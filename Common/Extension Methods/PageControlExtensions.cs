﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Media;
using System.Windows.Navigation;

namespace NS_Common.Extension_Methods
{
    public static class PageControlExtensions
    {
        public static NavigationService GetOwnerNavigationService(this System.Windows.Controls.Page page, Int32 ancestor_level= 1)
        {
            Int32  ancestorLevel  = 1;
            System.Windows.DependencyObject parent = page as System.Windows.DependencyObject;
            PropertyInfo pi = null;
            while (null == pi && null != parent)
            {
                parent = VisualTreeHelper.GetParent(parent);
                pi = parent.GetType().GetProperty("NavigationService");
                if (null != pi && ancestorLevel < ancestor_level)
                {
                    pi = null;
                    ancestorLevel++;
                }
            }
            if (null != pi && null != parent)
            {
                return pi.GetValue(parent) as NavigationService;
            }
            return null;
        }
    }
}
