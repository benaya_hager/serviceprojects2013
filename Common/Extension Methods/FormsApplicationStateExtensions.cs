﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Practices.Unity;

namespace NS_Common.ApplicationExtensions
{
    public static class FormsApplicationStateExtensions
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public static IUnityContainer m_Container;
        public static IUnityContainer GetGobalContainer(this Application application)
        {
            try
            { 
                 if (m_Container == null)
                {
                    m_Container = new UnityContainer();
                }
                 return m_Container;
            }
            finally
            {
            }
        }

        public static String ProductName(this Application application)
        {
            AssemblyProductAttribute myProduct =
                                       (AssemblyProductAttribute)AssemblyProductAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(),
                                        typeof(AssemblyProductAttribute));
            return myProduct.Product;

        }

     
    }
}

