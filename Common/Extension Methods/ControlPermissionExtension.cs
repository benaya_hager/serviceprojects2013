﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace NS_Common.Extension_Methods
{
    [MarkupExtensionReturnType(typeof(Boolean))]
    public class ControlPermission : MarkupExtension
    {
        [ConstructorArgument("permission_level")]
        public Int32 PermissionLevel { get; set; }

        /// <summary>
        /// Empty default constructor.
        /// </summary>
        public ControlPermission()
        { }

        /// <summary>
        /// Initialize the <see cref="Info"/> markup extension
        /// with title and body.
        /// </summary>
        public ControlPermission(Int32 permission_level)
        {
            PermissionLevel = permission_level;
        }



        private Boolean GetIsVisible()
        {
            if (System.Threading.Thread.CurrentPrincipal.GetType() == typeof(System.Security.Principal.GenericPrincipal)) 
                return true;
            
            Int32 val = 8;
            if (null != System.Threading.Thread.CurrentPrincipal)
            {
                PropertyInfo pi = System.Threading.Thread.CurrentPrincipal.GetType().GetProperty("UserPermissionsLevel");
                if (null != pi)
                {
                    val = (Int32)pi.GetValue(System.Threading.Thread.CurrentPrincipal, null);
                }
            }
            return  ((PermissionLevel & val) != 0);
        }


        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                return null;

            

            // get the target of the Extension from the IServiceProvider interface
            IProvideValueTarget ipvt =
                (IProvideValueTarget)serviceProvider.GetService(typeof(IProvideValueTarget));

            DependencyObject targetObject = ipvt.TargetObject as DependencyObject;

            object val = GetIsVisible();

            if (ipvt.TargetProperty is DependencyProperty)
            {
                // target property is a DP
                DependencyProperty dp = ipvt.TargetProperty as DependencyProperty;

                if (val is IConvertible)
                {
                    val = Convert.ChangeType(val, dp.PropertyType);
                }
            }
            else
            {
                // target property is not a DP, it's PropertyInfo instead.
                PropertyInfo info = ipvt.TargetProperty as PropertyInfo;

                if (val is IConvertible)
                {
                    val = Convert.ChangeType(val, info.PropertyType);
                }
            }

            return val;
        }
         


    }
}
