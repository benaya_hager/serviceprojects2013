﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;

namespace NS_Common.ApplicationExtensions
{
    public static class ApplicationStateExtensions
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public static IUnityContainer GetContainer(this Application application)
        {
            try
            {
                IUnityContainer container = application.Properties[GlobalContainerKey] as IUnityContainer;
                if (container == null)
                {
                    container = new UnityContainer();
                    application.Properties[GlobalContainerKey] = container;
                }
                return container;
            }
            finally
            {
            }
        }

        public static String ProductName(this Application application)
        {
            AssemblyProductAttribute myProduct =
                                       (AssemblyProductAttribute)AssemblyProductAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(),
                                        typeof(AssemblyProductAttribute));
            return myProduct.Product;

        }

        public static String ExecutableLocationPath(this Application application)
        {
            var wpfAssembly = (AppDomain.CurrentDomain
                    .GetAssemblies()
                    .Where(item => item.EntryPoint != null)
                    .Select(item =>
                        new { item, applicationType = item.GetType(item.GetName().Name + ".App", false) })
                    .Where(a => a.applicationType != null && typeof(System.Windows.Application)
                        .IsAssignableFrom(a.applicationType))
                        .Select(a => a.item))
                .FirstOrDefault();
            return wpfAssembly.Location;
        }
    }
}

