﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.NFW2_XYZ_Point
{
    public class XYPointFix
    {
        public XYPointFix(double xOffset, double yOffset, double angle)
        {
            m_XOffset = xOffset;
            m_YOffset = yOffset;
            m_Angle = angle;
        }

        public double XOffset
        {
            get { return m_XOffset; }
            set { m_XOffset = value; }
        }

        public double YOffset
        {
            get { return m_YOffset; }
            set { m_YOffset = value; }
        }

        public double Angle
        {
            get { return m_Angle; }
            set { m_Angle = value; }
        }

        private double m_XOffset;
        private double m_YOffset;
        private double m_Angle;
    }
}
