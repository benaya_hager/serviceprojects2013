﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Common.NFW2_XYZ_Point
{
    /// <summary>
    /// Represents a point in the 3D space
    /// </summary>
    public class XYZPoint
    {
        /// <summary>
        /// Copy C'tor - copies the coordinates of the given point. 
        /// if <paramref name="xyzPoint"/> is null no assignment is perfromed.
        /// </summary>
        /// <param name="xyzPoint"></param>
        public XYZPoint(XYZPoint xyzPoint)
        {
            if (xyzPoint == null)
                return;

            m_X = xyzPoint.X;
            m_Y = xyzPoint.Y;
            m_Z = xyzPoint.Z;
        }

        public XYZPoint(Single x, Single y, Single z)
        { 
            m_X = x;
            m_Y = y;
            m_Z = z;
        }

        public XYZPoint(CommaDelimitedStringCollection stringPoint)
        {
            m_X = Single.Parse(stringPoint[0]);
            m_Y = Single.Parse(stringPoint[1]);
            m_Z = Single.Parse(stringPoint[2]);
        }

        private Single m_X;
        public Single X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private Single m_Y;
        public Single Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        private Single m_Z;
        public Single Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }

        public static Boolean operator <(XYZPoint point1, XYZPoint point2)
        {
            return point1.m_X < point2.m_X || point1.m_Y < point2.m_Y || point1.m_Z < point2.m_Z;
        }

        public static Boolean operator >(XYZPoint point1, XYZPoint point2)
        {
            return point1.m_X > point2.m_X || point1.m_Y > point2.m_Y || point1.m_Z > point2.m_Z;
        }

        public override string ToString()
        {
            return "(" + X.ToString() + "," + Y.ToString() + "," + Z.ToString() + ")";
        }

        public XYZPoint Clone()
        {
            return (XYZPoint)this.MemberwiseClone();
        }
    }
}
