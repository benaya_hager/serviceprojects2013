﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Common.Service
{
    public class _ActionType
    {
        #region Public Properties

        private  Type m_ActionType;
        public  Type ActionType
        {
            get { return m_ActionType; }
            set { m_ActionType = value; }
        }

        private String  m_ActionNameShortDescription;
        public String  ActionNameShortDescription
        {
            get { return m_ActionNameShortDescription; }
            set { m_ActionNameShortDescription = value; }
        }


        private String m_ActionNameFullDescription;
        public String ActionNameFullDescription
        {
            get { return m_ActionNameFullDescription; }
            set { m_ActionNameFullDescription = value; }
        }

        #endregion

        #region Constructors

        public _ActionType()
        { }

        public _ActionType(Type action_type)
            :this()
        { m_ActionType = action_type; }

        public _ActionType(Type action_type, String action_name_short_description)
            : this(action_type)

        {
            m_ActionNameShortDescription = action_name_short_description;
        }

        public _ActionType(Type action_type, String action_name_short_description, String action_name_full_description)
            : this(action_type, action_name_short_description)
        {
            m_ActionNameFullDescription = action_name_full_description;
        }

        #endregion
    }
}
