﻿using System;

namespace NS_Common.Event_Arguments
{
    /// <summary>
    /// The event data, based on System.EventArgs is the base class for classes containing event data.
    /// </summary>
    public class UpdateSplashEventArgs : EventArgs
    {
        #region Public properties

        private String m_SplashString = "";
        /// <summary>
        /// Set/Get the string to display on splash window state
        /// </summary>
        public String SplashString
        {
            get { return m_SplashString; }
            set { m_SplashString = value; }
        }

        private Byte m_ProgressState = 100;
        /// <summary>
        /// Set/Get the progress bassed on 100% of total task to perform
        /// </summary>
        public Byte ProgressState
        {
            get { return m_ProgressState; }
            set { m_ProgressState = value; }
        }

        #endregion Public properties

        #region Constructors

        /// <summary>
        /// Base empty constructor, used to create empty reference to class
        /// </summary>
        public UpdateSplashEventArgs()
        { }

        /// <summary>
        /// Automatically Initialized reference to class
        /// </summary>
        /// <param name="inProgressState">The progress bassed on 100% of total task to perform</param>
        /// <param name="inSplashString">The string to display on splash window state</param>
        public UpdateSplashEventArgs(Byte inProgressState, String inSplashString)
            : this()
        {
            m_SplashString = inSplashString;
            m_ProgressState = inProgressState;
        }

        #endregion Constructors
    }
}