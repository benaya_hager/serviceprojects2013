﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;
 
namespace NS_Common.Service
{
    public class AsyncTrulyObservableCollection<T> : ObservableCollection<T>
          where T : INotifyPropertyChanged
    {
        private readonly AsyncContext _asyncContext = new AsyncContext();

        #region IAsyncContext Members
            public SynchronizationContext AsynchronizationContext { get { return _asyncContext.AsynchronizationContext; } }
            public bool IsAsyncCreatorThread { get { return _asyncContext.IsAsyncCreatorThread; } }
            public void AsyncPost(SendOrPostCallback callback, object state) { _asyncContext.AsyncPost(callback, state); }
        #endregion

         

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            AsyncPost(RaiseCollectionChanged, e);
        }

        private void RaiseCollectionChanged(object param)
        {
            base.OnCollectionChanged((NotifyCollectionChangedEventArgs)param);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            AsyncPost(RaisePropertyChanged, e);
        }

        private void RaisePropertyChanged(object param)
        {
            base.OnPropertyChanged((PropertyChangedEventArgs)param);
        }




        public AsyncTrulyObservableCollection()
        {
            CollectionChanged += TrulyObservableCollection_CollectionChanged;
        }

        public AsyncTrulyObservableCollection(IEnumerable list)
        {}

        void TrulyObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Object item in e.NewItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged += item_PropertyChanged;
                }
            }
            if (e.OldItems != null)
            {
                foreach (Object item in e.OldItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged -= item_PropertyChanged;
                }
            }
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var a = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            OnCollectionChanged(a);
        }
    }
}