﻿using System;
using System.Windows.Forms;

namespace NS_Common.Service
{
    public class TypeListViewItem : ListViewItem
    {
        private _ActionType m_ActionType;
        public _ActionType ActionType
        {
            get { return m_ActionType; }
            set { m_ActionType = value; }
        }
        
        
        private String m_FullTypeName;
        public String FullTypeName
        {
            get { return m_FullTypeName; }
            set { m_FullTypeName = value; }
        }


        private String m_Caption;
        public String Caption
        {
            get { return m_Caption; }
            set { m_Caption = value; }
        }

        public TypeListViewItem(String caption, String full_type_name, _ActionType action_type)
        {
            if (null != caption)
            {
                base.Text = caption;
                m_Caption = caption;
            }
            else
            {
                base.Text = "";
                m_Caption = "";
            }

            m_ActionType = action_type;
            m_FullTypeName = full_type_name;
        }

        public override string ToString()
        {
            return m_Caption;
        }
    }

}