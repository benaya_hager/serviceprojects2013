﻿using System;
using System.Drawing;

namespace NS_Common.Service
{
    public static  class SharedMembers
    {
        public static Brush NORMAL_BACKGROUND_COLOR = Brushes.White;
        public static Brush SEARCH_BACKGROUND_COLOR = Brushes.LemonChiffon;
        public static Brush DISABLED_BACK_COLOR = Brushes.Gray;
    }

    #region Public Type Definitions

    public enum _TEST_STATUS { IDLE = 0, RUNNING = 1, PAUSED = 2, STOPPED = 3, COMPLETED = 4, HOME_ROUTINE_PROGRESS = 5 }

    public enum _DB_FIELD_TYPE { TABLE_FIELD = 0, OTHER_TABLE_ID = 1, SINGLE_TO_MANY = 2 }

    public enum RunType
    {
        TEST = 0x1,
        LIVE = 0x2,
        DEV = 0x4
    };


    public enum _Internal_Lab_Status
    {
        [Tools.String_Enum.StringValue("Unknown")]
        ILS_Unknown = 1,
        [Tools.String_Enum.StringValue("Early")]
        ILS_Early = 2,
        [Tools.String_Enum.StringValue("Advanced")]
        ILS_Advanced = 3,
    }


    public enum _TEST_RESULT_CALCULATION_BY
    {
        [Tools.String_Enum.StringValue("RESULT BY PERCENST")]
        RESULT_BY_PERCENST = 0,
        [Tools.String_Enum.StringValue("RESULT BY REFERENCE POSITION")]
        RESULT_BY_REFERENCE_POSITION = 1,
        [Tools.String_Enum.StringValue("RESULT BY ABSOLUTE POSITION")]
        RESULT_BY_ABSOLUTE_POSITION = 2
    }

    /// <summary>
    /// Used for test machines types
    /// </summary>
    public enum _MachinesType
    {
        [Tools.String_Enum.StringValue("Unknown")]
        Machine_Unknown = 1,
        [Tools.String_Enum.StringValue("Compression Force Tester")]
        Compression_Force_Tester = 2,
        [Tools.String_Enum.StringValue("Deployment Test Device")]
        Deployment_Test_Device = 3,
        [Tools.String_Enum.StringValue("Securement Test")]
        Securement_Test = 4,
        [Tools.String_Enum.StringValue("Trackability Measurement Project")]
        Trackability_Measurement_Project = 5,
        [Tools.String_Enum.StringValue("InkJet")]
        InkJet = 6,
        [Tools.String_Enum.StringValue("Conformability")]
        Conformability =7,
        [Tools.String_Enum.StringValue("Flexibility II")]
        Flexibility_II = 8,
        [Tools.String_Enum.StringValue("Burst Test II")]
        BurstTest2 = 9,
        [Tools.String_Enum.StringValue("Transformation Measuring")]
        Reaction_Force_Tester = 10,
        [Tools.String_Enum.StringValue("Fuse Machine")]
        FUSE_MACHINE = 12,
        [Tools.String_Enum.StringValue("Transformation Measuring Tester")]
        TRANSFORMATION_MEASURING_TESTER = 12,
        [Tools.String_Enum.StringValue("Reaction Force Tester Test")]
        REATION_FORCE_TESTER = 13,
        [Tools.String_Enum.StringValue("Petite Reaction Force Tester Test")]
        PETIT_REATION_FORCE_TESTER = 14,
        [Tools.String_Enum.StringValue("Aneurysms NI Tester ")]
        ANEURYSM_NI_TESTER = 15,
        [Tools.String_Enum.StringValue("Debug Test Machine")]
        Debug_Test_Machine = 9999
    }




    /// <summary>
    /// Used to set user controls permission level
    /// </summary>
    public enum _UserPermissionsLevel
    {
        [Tools.String_Enum.StringValue("Unknown")]
        User_Unknown = 1,
        [Tools.String_Enum.StringValue("Operator")]
        User_Operator = 3,
        [Tools.String_Enum.StringValue("Power User")]
        User_Power_User = 7,
        [Tools.String_Enum.StringValue("Administrator")]
        User_Administrator = 15,
        [Tools.String_Enum.StringValue("Software Engineer")]
        User_Software_Engineer = 31
    }

    public enum _TestGroupType
    {
        [Tools.String_Enum.StringValue("Product")]
        TGT_Product = 1,
        [Tools.String_Enum.StringValue("Project")]
        TGT_Project = 2,
        [Tools.String_Enum.StringValue("C&F")]
        TGT_C_AND_F = 3,
    }
     

    #endregion Public Type Definitions

    
}