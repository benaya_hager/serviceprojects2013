﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Common.Service
{
    /// <summary>
    /// Supported data type String constants.
    /// </summary>
    public class Types
    {
        #region String constants

        public const String EMPTY = "Empty";
        public const String STRING = "String";
        public const String BOOLEAN = "Boolean";
        public const String BYTE = "Byte";
        public const String CHAR = "Char";
        public const String DATETIME = "DateTime";
        public const String DECIMAL = "Decimal";
        public const String DOUBLE = "Double";
        public const String INT16 = "Int16";
        public const String INT32 = "Int32";
        public const String INT64 = "Int64";
        public const String SINGLE = "Single";
        public const String UINT16 = "UInt16";
        public const String UINT32 = "UInt32";
        public const String UINT64 = "UInt64";
        public const String ARRAYLIST = "ArrayList";
        public const String COLOR = "Color";
        public const String FONT = "Font";
        public const String SIZE = "Size";
        public const String POINT = "Point";
        public const String INI_SETTINGS = "IniSettings";
        public const String INI_SECTION = "IniSection";
        public const String KEYS = "Keys";
        public const String CHECKED_STATE = "CheckState";
        public const String PAIR = "Pair";
        public const String RECTANGLE = "Rectangle";
       
        #endregion String constants
    }
}
