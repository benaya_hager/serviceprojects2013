﻿

namespace NS_Common.ViewModels.Common
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    public class PageClosingBehavior
    {
        public static ICommand GetUnloaded(DependencyObject obj)
        {
            if ( null != obj )
                return (ICommand)obj.GetValue(UnloadedProperty);
            return null;
        }

        public static void SetUnloaded(DependencyObject obj, ICommand value)
        {
            obj.SetValue(UnloadedProperty, value);
        }


        public static readonly DependencyProperty UnloadedProperty = DependencyProperty.RegisterAttached(
            "Unloaded", typeof(ICommand), typeof(PageClosingBehavior),
            new UIPropertyMetadata(new PropertyChangedCallback(UnloadedChanged)));

        private static void UnloadedChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            Page page = target as Page;

            if (page != null)
            {
                if (e.NewValue != null)
                {
                    page.Unloaded += Page_Unloaded;
                }
                else
                {
                    page.Unloaded -= Page_Unloaded;
                }
            }
        }
         
 
        static void Page_Unloaded(object sender, EventArgs e)
        {
            ICommand Unloaded = GetUnloaded(sender as Page);
            if (Unloaded != null)
            {
                Unloaded.Execute(null);
            }
        }
         
    }
}
