﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace NS_Common.ViewModels.Common
{
    public class MouseDoubleClick
    {
        public static DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command",
            typeof(ICommand),
            typeof(MouseDoubleClick),
            new UIPropertyMetadata(CommandChanged));

        public static DependencyProperty CommandParameterProperty =
            DependencyProperty.RegisterAttached("CommandParameter",
                                                typeof(object),
                                                typeof(MouseDoubleClick),
                                                new UIPropertyMetadata(null));

        public static void SetCommand(DependencyObject target, ICommand value)
        {
            target.SetValue(CommandProperty, value);
        }

        public static void SetCommandParameter(DependencyObject target, object value)
        {
            target.SetValue(CommandParameterProperty, value);
        }
        public static object GetCommandParameter(DependencyObject target)
        {
            return target.GetValue(CommandParameterProperty);
        }
        private static void CommandChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            Control control = target as Control;
            if (control != null)
            {
                if ((e.NewValue != null) && (e.OldValue == null))
                {
                    control.MouseDoubleClick += OnMouseDoubleClick;
                }
                else if ((e.NewValue == null) && (e.OldValue != null))
                {
                    control.MouseDoubleClick -= OnMouseDoubleClick;
                }
            }
        }
        private static void OnMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            Control control = sender as Control;
            ICommand command = (ICommand)control.GetValue(CommandProperty);
            object commandParameter = control.GetValue(CommandParameterProperty);
            command.Execute(commandParameter);
        }
    }

    //public class DataGridBehavior
    //{
    
    //    public static ICommand GetMouseDoubleClick(DependencyObject obj)
    //    {
    //        if (null != obj)
    //            return (ICommand)obj.GetValue(MouseDoubleClickProperty);
    //        return null;
    //    }

    //    public static void SetMouseDoubleClick(DependencyObject obj, ICommand value)
    //    {
    //        obj.SetValue(MouseDoubleClickProperty, value);
    //    }


    //    public static readonly DependencyProperty MouseDoubleClickProperty = DependencyProperty.RegisterAttached(
    //        "MouseDoubleClick", typeof(ICommand), typeof(PageClosingBehavior),
    //        new UIPropertyMetadata(new PropertyChangedCallback(MouseDoubleClickChanged)));

    //    private static void MouseDoubleClickChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    //    {
    //        DataGrid  grid = target as DataGrid;

    //        if (grid != null)
    //        {
    //            if (e.NewValue != null)
    //            {
    //                grid.MouseDoubleClick += Grid_MouseDoubleClick;
    //            }
    //            else
    //            {
    //                grid.MouseDoubleClick -= Grid_MouseDoubleClick;
    //            }
    //        }
    //    }

        

    //    static void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    //    {
    //        ICommand MouseDoubleClick = GetMouseDoubleClick(sender as Page);
    //        if (MouseDoubleClick != null)
    //        {
    //            MouseDoubleClick.Execute(e);
    //        }
    //    }

        
         
    //}
}
