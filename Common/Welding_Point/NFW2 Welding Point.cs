﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NS_Common.XYZ_Point;

namespace NS_Common.Welding_Point
{
    public class Welding_Point : ListViewItem
    {
        #region Public Types

        #endregion Public Types

        #region Constructors

        public Welding_Point(XYZPoint inWeldingPoint,
                                  Int32 inStepIndex,
                                  Boolean inPointEnabled,
                                  Boolean inStepPointCompleted) :
            base()
        {
            m_WeldingPoint = inWeldingPoint.Clone();
            m_StepIndex = inStepIndex;
            m_PointEnabled = inPointEnabled;
            m_StepPointCompleted = inStepPointCompleted;
            base.Text = this.ToString();
        }

        public Welding_Point(XYZPoint inWeldingPoint) :
            this(inWeldingPoint, -1, true, false)
        {
        }

        public Welding_Point(Point inPoint) :
            this(new XYZPoint(inPoint.X, inPoint.Y, 0))
        { }

        public Welding_Point() :
            this(new XYZPoint(0, 0, 0))
        { }

        #endregion Constructors

        #region Public Properties

        private XYZPoint m_WeldingPoint;
        /// <summary>
        /// Get/Set the coordinates for welding
        /// </summary>
        public XYZPoint WeldingPoint
        {
            get { return m_WeldingPoint; }
            set { m_WeldingPoint = value; }
        }

        private Boolean m_PointEnabled = true;
        /// <summary>
        /// Get/Set if current step point will be drawn as enabled or disabled
        /// </summary>
        public Boolean PointEnabled
        {
            get { return m_PointEnabled; }
            set { m_PointEnabled = value; }
        }

        private Boolean m_StepPointCompleted = false;
        /// <summary>
        /// Get/Set if current step point will be drawn as completed
        /// </summary>
        public Boolean StepPointCompleted
        {
            get { return m_StepPointCompleted; }
            set { m_StepPointCompleted = value; }
        }

        private Int32 m_StepIndex;
        /// <summary>
        /// Get/Set step index
        /// </summary>
        public Int32 StepIndex
        {
            get { return m_StepIndex; }
            set { m_StepIndex = value; }
        }

        public new String Text
        {
            get { return this.ToString(); }
            private set { }
        }

        #endregion Public Properties

        #region Public Functions

        public override object Clone()
        {
            return (Welding_Point)this.MemberwiseClone();
        }

        public override string ToString()
        {
            return String.Format("StepIndex  {0:D} | Step Position ( {1:F3},{2:F3},{3:F3} )", m_StepIndex, (float)m_WeldingPoint.X, (float)m_WeldingPoint.Y, (float)m_WeldingPoint.Z);
        }

        #endregion Public Functions
    }
}