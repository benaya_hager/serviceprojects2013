﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Common.Error_Handling
{
    public class ErrorEventArgument : EventArgs 
    {
        private Exception m_Exception;

        public Exception myEx
        {
            get { return m_Exception; }
            set { m_Exception = value; }
        }

        /// <summary>
        /// Creates a shallow copy of the current Object.
        /// </summary>
        /// <returns>A shallow copy of the current object.</returns>
        public ErrorEventArgument Clone()
        {
            return (ErrorEventArgument)this.MemberwiseClone();
        }

        public ErrorEventArgument(Exception ex)
            : base()
        {
            m_Exception = ex;
        }
    }
}
