﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Common.Error_Handling
{
    /// <summary>
    /// The global list of known errors(Exceptions) in the system
    /// </summary>
    public static class ExceptionsList
    {
        /// <summary>
        /// Exception that occurs in case system was unable to create connection to controller
        /// </summary>
        public static BaseException CommunicationError = new BaseException();
        /// <summary>
        /// Exception that occurs in case system was unable to create connection to controller
        /// </summary>
        public static BaseException InitializationError = new BaseException();
        /// <summary>
        /// Occurs during loading the Axis list, in case the file not exists
        /// </summary>
        public static BaseException AxisXMLFileNotFound = new BaseException();
        /// <summary>
        /// Occurs during when trying to move Axis and the power state is off
        /// </summary>
        public static BaseException AxisPowerStateIsOFF = new BaseException();
        /// <summary>
        /// Occurs when trying to copy Axis reference while Axis Status Monitoring Thread thread is running
        /// </summary>
        public static BaseException AxisPosMonitoringStarted = new BaseException();
        /// <summary>
        /// Occurs when system failed to initial data for axises
        /// </summary>
        public static BaseException FailedToImportAxisData = new BaseException();

        /// <summary>
        /// Occurs when system failed to initial data for axises
        /// </summary>
        public static BaseException FailedToInitXYZ = new BaseException();

        /// <summary>
        /// Occurs when the Easy motion studio failed to reach the target axis position for some time period
        /// </summary>
        public static BaseException FailedToReachTargetPositionException = new BaseException();
        
        /// <summary>
        /// Occurs when trying to confirm some action on Axis that not exist in the list of 
        /// axis's in SingleAxis main collection
        /// </summary>
        public static BaseException AxisNotFound = new BaseException();
        
        /// <summary>
        /// Occurs when trying to select routine using the lock but some other process still didn't released it
        /// </summary>
        public static BaseException FailedToEnterCriticalSectionException = new BaseException();

        /// <summary>
        ///Exception that occurs in case writing thread is running and user trying to change some of parameters
        /// </summary>
        public static BaseException UnableToChangeParameterError = new BaseException();

        /// <summary>
        /// Occurs when some not known error appeared
        /// </summary>
        public static BaseException UnhandledMotionControllerException = new BaseException();
 
        #region  Common  Exceptions 
            public static BaseException MissingValueException = new BaseException();
            public static BaseException ForeignKeyConstraintException = new BaseException();
            public static BaseException DeleteFailedException = new BaseException();
            public static BaseException NoConnectionToSQLServerException = new BaseException();
            public static BaseException DuplicateRecordException = new BaseException();
            public static BaseException ProductIDMissingException = new BaseException();
        #endregion

        #region  User Admin  Exceptions 
            public static BaseException DuplicateLoginNameException = new BaseException();
            public static BaseException LoginMissingException = new BaseException();
            public static BaseException PasswordNotSetException = new BaseException();
        #endregion

        #region  Routine Steps  Exceptions 
            public static BaseException StepNameNotSetException = new BaseException();
            public static BaseException StepIndexNotSetException = new BaseException();
            public static BaseException StepIndexOccupiedException = new BaseException();
            public static BaseException StepNameOccupiedException = new BaseException();
        #endregion

        #region  Routine  Exceptions 
            public static BaseException RoutineNameMissingException = new BaseException();
            public static BaseException RoutineNameOccupiedException = new BaseException();
            public static BaseException RoutineTypeExistsForProductException = new BaseException();
            public static BaseException FolderIDMissingException = new BaseException();
        #endregion

        #region  Folding & Welding Step Actions  Exceptions 
            public static BaseException ActionNameNotSetException = new BaseException();
            public static BaseException ActionTypeNotSetException = new BaseException();
            public static BaseException AccelerationRateNotSetException = new BaseException();
            public static BaseException SlewSpeedNotSetException = new BaseException();
            public static BaseException AxisNotSetException = new BaseException();
            public static BaseException WaitTimeoutNotSetException = new BaseException();
            public static BaseException HomeRoutineNameNotSetException = new BaseException();
            public static BaseException LaserMainPulsePeakPowerNotSetException = new BaseException();
            public static BaseException LaserMainPulseWidthNotSetException = new BaseException();
            public static BaseException MismatchInMovementAndLaserTargetPositionsException = new BaseException();
            public static BaseException ActionNameAlreadyExistsForStep = new BaseException();
        #endregion

        #region  Axis  Exception 
            public static BaseException AxisNameNotSetException = new BaseException();
            public static BaseException NetworkIdentifierNotSetException = new BaseException();
            public static BaseException SetupFileNotSetException = new BaseException();
            public static BaseException DisplayPositionNotSetBaseException = new BaseException();
            public static BaseException AxisConstraintFolder2AxisException = new BaseException();
            public static BaseException DuplicateAxisDisplayPositionBaseException = new BaseException();
        #endregion

        #region  Folder  Exceptions 
            public static BaseException FolderSerialNumberNotSetException = new BaseException();
            public static BaseException FolderTypeNumberNotSetException = new BaseException();
            public static BaseException FolderLocationNumberNotSetException = new BaseException();
            public static BaseException DuplicateFolderNameException = new BaseException();
            public static BaseException FolderAlreadyMappedToBayException = new BaseException();
            public static BaseException TargetFolderMissingAxisException = new BaseException();
        #endregion

        #region  Blades  Exceptions 
            public static BaseException BladeSerialNumberNotSetException = new BaseException();
            public static BaseException BladeTypeNumberNotSetException = new BaseException();
            public static BaseException BladeConstraintFolder2BladeException = new BaseException();
            public static BaseException DuplicateBladeSerialNumberException = new BaseException();
        #endregion

        #region  Folder 2 Blade & Axis & Callibration 
            public static BaseException DuplicateAxisNameException = new BaseException();
            public static BaseException BladeAlreadyExistOnCurrentFolderException = new BaseException();
            public static BaseException AxisAlreadyExistOnCurrentFolderException = new BaseException();
            public static BaseException CallibrationAlreadyExistOnCurrentFolderException = new BaseException();
            public static BaseException NoFolderSelectedException = new BaseException();
            public static BaseException AxisWithNetworkIDExistsOnFolder = new BaseException();
            public static BaseException AxisWithNetworkIDExistsOnMachine = new BaseException();
            public static BaseException AxisWithNetworkIDExistsInStep = new BaseException();
        #endregion

        #region  CriticalArea 
            public static BaseException CriticalAreaMinPointNotSetException = new BaseException();
            public static BaseException CriticalAreaMaxPointNotSetException = new BaseException();
            public static BaseException CriticalAreaFolderStateNotSetException = new BaseException();
            public static BaseException CriticalAreaBayIndexNotSetException = new BaseException();
            public static BaseException CriticalAreaMachineNameNotSetException = new BaseException();
            public static BaseException CriticalAreaExistsForMachineBayState = new BaseException();
        #endregion

        #region  Service Projects: SYSTEM Exceptions 
            public static BaseException SYSTEM_RECORD_UPDATE_Exception = new BaseException();
        #endregion

        #region  Service Projects: Machine Exceptions 
            public static BaseException MachineNameNotSetException = new BaseException();
            public static BaseException DuplicateMachineNameException = new BaseException();
            public static BaseException MachineLocationNotSetException = new BaseException();
        #endregion

        #region  Service Projects: Brand Exceptions 
            public static BaseException BrandNameNotSetException = new BaseException();
            public static BaseException DuplicateBrandNameException = new BaseException();
        #endregion

        #region  Service Projects: ProjectState Exceptions 
            public static BaseException ProjectStateNameNotSetException = new BaseException();
            public static BaseException DuplicateProjectStateNameException = new BaseException();
        #endregion

        #region  Service Projects: Protocol Exceptions 
            public static BaseException ProtocolNameNotSetException = new BaseException();
            public static BaseException DuplicateProtocolNameException = new BaseException();
        #endregion

        #region  Service Projects: STP Exceptions 
            public static BaseException STP_TitleNotSetException = new BaseException();
            public static BaseException Duplicate_STP_TitleException = new BaseException();
            public static BaseException STP_SerialNumberNotSetException = new BaseException();
            public static BaseException Duplicate_STP_SerialNumberException = new BaseException();
            public static BaseException STP_VersionNotSetException = new BaseException();
            public static BaseException Duplicate_STP_VersionException = new BaseException();
        #endregion

        #region  Service Projects: C&F Exceptions 
            public static BaseException C_AND_F_TitleNotSetException = new BaseException();
            public static BaseException Duplicate_C_AND__TitleException = new BaseException();
        #endregion

        #region  Service Projects: Project Manager/Owner Exceptions 
            public static BaseException ProjectManagerNameNotSetException = new BaseException();
            public static BaseException DuplicateProjectManagerNameException = new BaseException();
        #endregion

        #region  Service Projects: Project Exceptions 
            public static BaseException ProjectNumberNotSetException = new BaseException();
            public static BaseException DuplicateProjectNumberException = new BaseException();

            public static BaseException ProjectNameNotSetException = new BaseException();
            public static BaseException DuplicateProjectNameException = new BaseException();

            public static BaseException ProjectNicknameNameNotSetException = new BaseException();
            public static BaseException DuplicateProjectNicknameException = new BaseException();
        #endregion 

        #region  Service Projects: Product Exceptions 
            public static BaseException ProductBrandNotSetException = new BaseException();
             
            public static BaseException CatalogNumberNotSetException = new BaseException();
            public static BaseException DuplicateCatalogNumberException = new BaseException();
        #endregion 

        #region  Service Projects: Lot Type Exceptions
            public static BaseException LotTypeTitleNotSetException= new BaseException();
            public static BaseException DuplicateLotTypeTitleException = new BaseException();
        #endregion 

        #region  Service Projects: Test Requested By Exceptions 
            public static BaseException PersonNameNotSetException = new BaseException();
            public static BaseException DuplicatePersonNameException = new BaseException();
        #endregion



        #region  FEA Log: Product Exceptions
            public static BaseException FEA_ProductNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicateProductNameException = new BaseException();
        #endregion

        #region  FEA Log: Family Exceptions
            public static BaseException FEA_FamilyNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicateFamilyNameException = new BaseException();
        #endregion

        #region  FEA Log: Subject Exceptions
            public static BaseException FEA_SubjectNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicateSubjectNameException = new BaseException();
        #endregion

        #region  FEA Log: Object Exceptions
            public static BaseException FEA_ObjectNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicateObjectNameException = new BaseException();
        #endregion

        #region  FEA Log: Part Exceptions
            public static BaseException FEA_PartNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicatePartNameException = new BaseException();
        #endregion

            #region  FEA Log: Material Exceptions
            public static BaseException FEA_MaterialNameNotSetException = new BaseException();
            public static BaseException FEA_DuplicateMaterialNameException = new BaseException();
        #endregion
    }

 
}
