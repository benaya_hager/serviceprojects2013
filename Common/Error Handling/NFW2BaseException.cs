﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Error_Handling
{
    /// <summary>
    /// Single Exception based class
    /// </summary>
    public class NFW2BaseException : Exception
    {
#region Public Properties
        private Exception m_Exception;
        /// <summary>
        /// The Original Exception received
        /// </summary>
        public Exception myEx
        {
            get { return m_Exception; }
            set { m_Exception = value; }
        }

        private String m_MessageText;
        /// <summary>
        /// Set/Get the error description text
        /// </summary>
        public String MessageText
        {
            get { return m_MessageText; }
            set { m_MessageText = value; }
        }

        private int m_ErrorID;
        /// <summary>
        /// Set/Get the error code from Error Codes List class
        /// </summary>
        public int ErrorID
        {
            get { return m_ErrorID; }
            set { m_ErrorID = value; }
        }
#endregion

#region Public Functions
        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public NFW2BaseException Clone()
        {
            return (NFW2BaseException)this.MemberwiseClone();
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty Constructor
        /// </summary>
        public NFW2BaseException()
        {}
        /// <summary>
        /// Constructor that initialize the message string 
        /// </summary>
        /// <param name="MessageText">The system message string</param>
        public NFW2BaseException(String ErrorText)
        {
            m_MessageText = ErrorText;
        }

        public NFW2BaseException(Exception inException)
        {
            m_Exception = inException;
            m_MessageText = m_Exception.Message;
            m_ErrorID = 0;            
        }
#endregion
    }
}
