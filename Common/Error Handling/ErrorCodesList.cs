﻿namespace NS_Common
{
    public class ErrorCodesList
    {
        public const short CANCELED = -2;
        public const short FAILED = -1;
        public const short OK = 0;
        public const short COMMUNICATION_ERROR = 1;
        public const short INITIALIZATION_ERROR = 2;
        public const short AXIS_DEFINITION_FILE_NOT_FOUND_ERROR = 3;
        public const short AXIS_POWER_STATE_OFF = 4;
        public const short AXIS_LOGGER_THREAD_STARTED = 5;
        public const short FAILED_TO_LOAD_SETUP = 6;
        public const short FAILED_TO_SET_SETUP = 7;
        public const short FAILED_TO_SELECT_AXIS = 8;
        public const short FAILED_TO_INITIALIZE_DRIVE = 9;
        public const short FAILED_TO_SET_HOME_POSITION = 10;
        public const short FAILED_TO_INIT_AXES = 11;
         

        public const short FAILED_TO_SET_BROADCAST_SETUP = 14;
        public const short FAILED_TO_SELECT_BROADCAST = 15;
        public const short FAILED_TO_SET_POWER_ON = 16;
        public const short FAILED_TO_RECIVE_POWERON_OK_STATUS = 17;

        public const short FAILED_TO_SET_ZERO_POSITION = 19;
        public const short FAILED_TO_MOVE_TO_REFERENCE = 20;
        public const short FAILED_TO_IMPORT_AXIS_LIST_FROM_DATABASE = 21;
        public const short AXIS_DATA_NOT_SET_FAILED_TO_INIT = 22;
        public const short FAILED_TO_RUN_ROUTINE_LABEL = 23;
        public const short FAILED_TO_READ_CURRENT_STATE = 24;
        public const short FAILED_TO_GET_APOS = 26;
        public const short FAILED_TO_SET_POSITION = 27;
        public const short AXIS_NOT_FOUND = 28;
        public const short BRIDGES_NOT_FOUND = 29;
        public const short WELD_POINTS_NOT_FOUND = 30;
        public const short ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE = 31;
        public const short IMAGE_PROCESSING_FAILED = 32;
        public const short IMAGE_PROCESSING_SUCCEEDED = 33;
        public const short IMAGE_PROCESSING_MOVEMENT_FAILED = 34;
        public const short WRONG_ROUTINE_TYPE = 35;
        public const short PRODUCT_DEFINITION_NOT_FOUND = 36;
        public const short INVALID_PARAMETER_VALUE = 37;
        public const short UNABLE_2_CHANGE_PARAMETER_ERROR = 38;
        public const short FAILED_TO_REACH_TARGET_POSITION = 39;
        public const short FAILED_TO_ENTER_CRITICAL_SECTION = 40;
        public const short FAILED_TO_OPEN_COM_PORT = 41;
        public const short USB_LENGTH_VALUE_ERROR = 42;
        public const short FAILED_TO_START_MESUARMENT_THREAD_ERROR = 43;
        public const short FAILED_TO_OPEN_COMM_PORT = 44;
        public const short DEVICE_COMMUNICATION_ERROR = 45;

        public const short FAILED_TO_READ_REGISTRY_DATA = 46;
        public const short FAILED_TO_WRITE_REGISTRY_DATA = 47;

        public const short FAILED_TO_SET_PIXELINK_CAMERA_FEATURE = 48;
        public const short ROUTINE_ABORTED = 49;
        public const short METHOD_TIMEOUT = 50;
        public const short STENT_INDEX_NOT_SET_FAILED_TO_START_ACTION = 51;
        public const short JET_INDEX_NOT_SET_FAILED_TO_START_ACTION = 52;

        public const short BAY_ZERO_POINT_NOT_SET = 53;

        public const short FILE_NOT_FOUND = 54;

        #region Aerotech Device Errors  (200 -> 250)

        public const short FAILED_TO_FIND_DEVICE_BY_NAME = 200;
        public const short FAILED_AEROBASIC_FAIL_NOT_FOUND = 201;
        public const short FAILED_TO_RUN_AEROBASIC_FILE = 202;


        #endregion Aerotech Device Errors  (200 -> 250)

        #region AppSettings Errors  (700 -> 710)

        public const short FAILED_TO_SAVE_SETTINGS_LOW_PERRMITIONS = 700;

        #endregion AppSettings Errors  (700 -> 710)

        #region DB ERRORS

        public const short FOREIGN_CONSTRAINT_ERROR = 547;
        public const short INSERT_NULL_VALUES_ERROR = 515;
        public const short NO_CONNECTION_TO_SQL_SERVER_ERROR = 1231;
        public const short INSERT_DUPLICATE_VALUE_ERROR = 2601;
        public const short DUPLICATE_VALUE_ERROR = 2627;
        public const short CONNECTION_RESET_BY_PEER_ERROR = 10054;

        #endregion DB ERRORS

        #region Axis MER Errors (100 -> 150)

        public const short MER_ENABLE_INPUT_IS_INACTIVE = 115;
        public const short MER_COMMAND_ERROR = 114;
        public const short MER_UNDER_VOLTAGE = 113;
        public const short MER_OVER_VOLTAGE = 112;
        public const short MER_OVER_TEMP_DRIVE = 111;
        public const short MER_OVER_TEMP_MOTOR = 110;
        public const short MER_L_TO_T = 109;
        public const short MER_OVER_CURRENT = 108;
        public const short MER_LSN_LIMIT_ACTIVE = 107;
        public const short MER_LSP_LIMIT_ACTIVE = 106;
        public const short MER_POSITION_WRAPAROUND = 105;
        public const short MER_SERIAL_COMMUNICATION_ERROR = 104;
        public const short MER_CONTROL_ERROR = 103;
        public const short MER_INVALID_SETUP_DATA = 102;
        public const short MER_SHORT_CIRCUIT = 101;
        public const short MER_CANbus_ERROR = 100;

        #endregion Axis MER Errors (100 -> 150)

        public const short UNHANDLED_MOTION_CONTROLLER_ERROR = 10000;
        public const short UNKNOWN_ERROR = -10000;
    }
}