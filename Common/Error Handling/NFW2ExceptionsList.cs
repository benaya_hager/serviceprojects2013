﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Error_Handling
{
    /// <summary>
    /// The global list of knowen errors(Exceptions) in the system
    /// </summary>
    public static class NFW2ExceptionsList
    {
        /// <summary>
        /// Exception that occurs in case system was unable to create connection to controller
        /// </summary>
        public static NFW2BaseException CommunicationError = new NFW2BaseException();
        /// <summary>
        /// Exception that occurs in case system was unable to create connection to controller
        /// </summary>
        public static NFW2BaseException InitializationError = new NFW2BaseException();
        /// <summary>
        /// Occurs during loading the Axis list, in case the file not exists
        /// </summary>
        public static NFW2BaseException AxisXMLFileNotFound = new NFW2BaseException();
        /// <summary>
        /// Occurs during when trying to move Axis and the power state is off
        /// </summary>
        public static NFW2BaseException AxisPowerStateIsOFF = new NFW2BaseException();
        /// <summary>
        /// Occurs when trying to copy Axis reference while Logger watcher thread is running
        /// </summary>
        public static NFW2BaseException AxisLogWatcherStarted = new NFW2BaseException();
        /// <summary>
        /// Occurs when system failed to initial data for axises
        /// </summary>
        public static NFW2BaseException FailedToImportAxisData = new NFW2BaseException();

        /// <summary>
        /// Occurs when system failed to initial data for axises
        /// </summary>
        public static NFW2BaseException FailedToInitXYZ = new NFW2BaseException();

        /// <summary>
        /// Occurs when the Easy motion studio failed to reach the target axis position for some time period
        /// </summary>
        public static NFW2BaseException FailedToReachTargetPositionException = new NFW2BaseException();
        

        /// <summary>
        /// Occurs when trying to confirm some action on Axis that not exist in the list of 
        /// axis's in SingleAxis main collection
        /// </summary>
        public static NFW2BaseException AxisNotFound = new NFW2BaseException();


        /// <summary>
        /// Occurs when trying to select routine using the lock but some other process still did't released it
        /// </summary>
        public static NFW2BaseException FailedToEnterCriticalSectionException = new NFW2BaseException();

        /// <summary>
        ///Exception that occurs in case writing thread is running and user trying to change some of parameters
        /// </summary>
        public static NFW2BaseException UnableToChangeParameterError = new NFW2BaseException();

        /// <summary>
        /// Occurs when some not knowen error appeared
        /// </summary>
        public static NFW2BaseException UnhandledMotionControllerExeption = new NFW2BaseException();

    }
}
