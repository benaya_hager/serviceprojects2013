﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Tools.NFW2_XYZ_Point
{
    /// <summary>
    /// Represents a point in the 3D space
    /// </summary>
    public class XYZPoint
    {
        public XYZPoint(Double x, Double y, Double z)
        { 
            m_X = x;
            m_Y = y;
            m_Z = z;
        }

        public XYZPoint(CommaDelimitedStringCollection stringPoint)
        {
            m_X = Double.Parse(stringPoint[0]);
            m_Y = Double.Parse(stringPoint[1]);
            m_Z = Double.Parse(stringPoint[2]);
        }

        private Double m_X;
        public Double X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        private Double m_Y;
        public Double Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        private Double m_Z;
        public Double Z
        {
            get { return m_Z; }
            set { m_Z = value; }
        }

        public static Boolean operator <(XYZPoint point1, XYZPoint point2)
        {
            return point1.m_X < point2.m_X || point1.m_Y < point2.m_Y || point1.m_Z < point2.m_Z;
        }

        public static Boolean operator >(XYZPoint point1, XYZPoint point2)
        {
            return point1.m_X > point2.m_X || point1.m_Y > point2.m_Y || point1.m_Z > point2.m_Z;
        }

        public override string ToString()
        {
            return "(" + X.ToString() + "," + Y.ToString() + "," + Z.ToString() + ")";
        }
    }
}
