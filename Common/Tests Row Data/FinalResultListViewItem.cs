﻿using System;
using System.Windows.Forms;
using NS_Common.Tests_Row_Data.NS_MesuareData;

namespace NS_Common.Tests_Row_Data
{
    /// <summary>
    /// ListViewItem based class, used to store additional information about tests results
    /// </summary>
    public class FinalResultListViewItem : ListViewItem
    {
        //Store test name
        protected String m_TestName;
        /// <summary>
        /// Get/Set test name
        /// </summary>
        public String TestName
        {
            get { return m_TestName; }
            set
            {
                m_TestName = value;
                base.Text = m_TestName;
            }
        }

        //Store Lot Number
        protected String m_LotNumber;
        /// <summary>
        /// Get/Set Lot Number
        /// </summary>
        public String LotNumber
        {
            get { return m_LotNumber; }
            set
            {
                m_LotNumber = value;
                base.SubItems[FinalResultPositions.LOT_NUMBER_INDEX].Text = m_LotNumber;
            }
        }

        //Store Maximum peak data
        protected BaseMesuareData m_MaximumValue;
        /// <summary>
        /// Get/Set Maximum peak data
        /// </summary>
        public BaseMesuareData MaximumValue
        {
            get { return m_MaximumValue; }
            set
            {
                m_MaximumValue = value;
                base.SubItems[FinalResultPositions.MAXIMUM_VALUE_POSITION_INDEX].Text = ((MesuareData)m_MaximumValue).CurrentPosition.ToString();
                base.SubItems[FinalResultPositions.MAXIMUM_VALUE_INDEX].Text = ((MesuareData)m_MaximumValue).CurrentValue.ToString();
            }
        }

        //Store Minimum peak data
        protected BaseMesuareData m_MinimumValue;
        /// <summary>
        /// Get/Set Minimum peak data
        /// </summary>
        public BaseMesuareData MinimumValue
        {
            get { return m_MinimumValue; }
            set
            {
                m_MinimumValue = value;
                base.SubItems[FinalResultPositions.MINIMUM_VALUE_POSITION_INDEX].Text = ((MesuareData)m_MinimumValue).CurrentPosition.ToString();
                base.SubItems[FinalResultPositions.MINIMUM_VALUE_INDEX].Text = ((MesuareData)m_MinimumValue).CurrentValue.ToString();
            }
        }

        //Store Measured peak data
        protected BaseMesuareData m_MeasuredValue;
        /// <summary>
        /// Get/Set Measured peak data
        /// </summary>
        public BaseMesuareData MeasuredValue
        {
            get { return m_MeasuredValue; }
            set
            {
                m_MeasuredValue = value;
                base.SubItems[FinalResultPositions.MEASURED_VALUE_POSITION_INDEX].Text = ((MesuareData)m_MeasuredValue).CurrentPosition.ToString();
                base.SubItems[FinalResultPositions.MEASURED_VALUE_INDEX].Text = ((MesuareData)m_MeasuredValue).CurrentValue.ToString();
            }
        }

        protected String m_Specimen_Dislodgement_Mode;
        /// <summary>
        /// Get/Set Securement Specimen Dislodgement Mode
        /// </summary>
        public String Specimen_Dislodgement_Mode
        {
            get { return m_Specimen_Dislodgement_Mode; }
            set
            {
                m_Specimen_Dislodgement_Mode = value;
                base.SubItems[FinalResultPositions.SPECIMEN_DISLOGEMENT_MODE_INDEX].Text = m_Specimen_Dislodgement_Mode;
            }
        }

        public FinalResultListViewItem()
            : base()
        { }

        public FinalResultListViewItem(String inTestName, String inLotNumber)
            : this()
        {
            TestName = inTestName;
            m_LotNumber = inLotNumber;
            base.SubItems.Insert(FinalResultPositions.LOT_NUMBER_INDEX, new ListViewSubItem(this, inLotNumber));
        }

        /// <summary>
        /// Constructor of ListViewItem based class
        /// </summary>
        public FinalResultListViewItem(String inTestName, String inLotNumber, BaseMesuareData inMinimumValue, BaseMesuareData inMaximumValue)
            : this(inTestName, inLotNumber)
        {
            base.SubItems.Insert(FinalResultPositions.MAXIMUM_VALUE_POSITION_INDEX, new ListViewSubItem(this, ((MesuareData)inMaximumValue).CurrentPosition.ToString()));
            base.SubItems.Insert(FinalResultPositions.MAXIMUM_VALUE_INDEX, new ListViewSubItem(this, ((MesuareData)inMaximumValue).CurrentValue.ToString()));


            base.SubItems.Insert(FinalResultPositions.MINIMUM_VALUE_POSITION_INDEX, new ListViewSubItem(this, ((MesuareData)inMaximumValue).CurrentPosition.ToString()));
            base.SubItems.Insert(FinalResultPositions.MINIMUM_VALUE_INDEX, new ListViewSubItem(this, ((MesuareData)inMaximumValue).CurrentValue.ToString()));


            m_MaximumValue = inMaximumValue;
            m_MinimumValue = inMinimumValue;
        }


        /// <summary>
        /// Constructor of ListViewItem based class
        /// </summary>
        public FinalResultListViewItem(String inTestName, String inLotNumber, BaseMesuareData inMinimumValue, BaseMesuareData inMaximumValue, BaseMesuareData inMesuaredPoint)
            : this(inTestName, inLotNumber, inMinimumValue, inMaximumValue)
        {
            base.SubItems.Insert(FinalResultPositions.MEASURED_VALUE_POSITION_INDEX, new ListViewSubItem(this, ((MesuareData)inMesuaredPoint).CurrentPosition.ToString()));
            base.SubItems.Insert(FinalResultPositions.MEASURED_VALUE_INDEX, new ListViewSubItem(this, ((MesuareData)inMesuaredPoint).CurrentValue.ToString()));

            m_MeasuredValue = inMesuaredPoint;
        }




        /// <summary>
        /// Constructor of ListViewItem based class. Used in Securement project
        /// </summary>
        public FinalResultListViewItem(String inTestName, String inLotNumber, String inSpecimen_Dislodgement_Mode, MesuareData inMinimumValue, MesuareData inMaximumValue)
            : this(inTestName, inLotNumber, inMinimumValue, inMaximumValue)
        {
            base.SubItems.Insert(FinalResultPositions.SPECIMEN_DISLOGEMENT_MODE_INDEX, new ListViewSubItem(this, inSpecimen_Dislodgement_Mode));

            m_Specimen_Dislodgement_Mode = inSpecimen_Dislodgement_Mode;
        }

        public override string ToString()
        {
            return m_TestName;
        }
    }

    public static class FinalResultPositions
    {
        #region Private Members

        public const Int32 TEST_NAME_INDEX = 0;
        public const Int32 LOT_NUMBER_INDEX = 1;

        public const Int32 MAXIMUM_VALUE_POSITION_INDEX = 2;
        public const Int32 MAXIMUM_VALUE_INDEX = 3;

        public const Int32 MINIMUM_VALUE_POSITION_INDEX = 4;
        public const Int32 MINIMUM_VALUE_INDEX = 5;

        public const Int32 MEASURED_VALUE_POSITION_INDEX = 6;
        public const Int32 MEASURED_VALUE_INDEX = 7;

        public const Int32 SPECIMEN_DISLOGEMENT_MODE_INDEX = 6;

        public const Int32 BURST2_TEST_RESULT_INDEX = 6;

        #endregion Private Members
    }
}