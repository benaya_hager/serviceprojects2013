﻿using System;

namespace NS_Common.Tests_Row_Data
{
    public class Export2ExcelAtribute : Attribute
    {
        #region Public Properties

        private Boolean m_IsVisible4Export = true;
        /// <summary>
        /// Gets the indicate if field needs should be exported to file and excel.
        /// </summary>
        /// <value></value>
        public Boolean IsVisible4Export
        {
            get { return m_IsVisible4Export; }
        }

        private String m_FieldTitle;
        public String FieldTitle
        {
            get { return m_FieldTitle; }
            set { m_FieldTitle = value; }
        }


        #endregion Public Properties


        public Export2ExcelAtribute(Boolean is_visible_for_export)
        {
            m_IsVisible4Export = is_visible_for_export;
        }

        public Export2ExcelAtribute(Boolean is_visible_for_export, String field_title)
            : this(is_visible_for_export)
        {
            m_FieldTitle = field_title;
        }
    }
}