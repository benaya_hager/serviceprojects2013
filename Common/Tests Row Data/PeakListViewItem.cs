﻿using System;
using System.Windows.Forms;

namespace NS_Common.Service
{
    /// <summary>
    /// ListViewItem based class, used to store additional information about NI device Lines
    /// </summary>
    public class PeakListViewItem : ListViewItem
    {
        //Store current peak value
        private double m_PeackValue;
        /// <summary>
        /// Get/Set current peak value
        /// </summary>
        public double PeackValue
        {
            get { return m_PeackValue; }
            set
            {
                m_PeackValue = value;
                base.SubItems[(int)1].Text = m_PeackValue.ToString();
            }
        }

        /// <summary>
        /// Constructor of ListViewItem based class
        /// </summary>
        /// <param name="Caption"></param>
        /// <param name="inPeackValue"></param>
        public PeakListViewItem(String Caption, double inPeackValue, long inTime)
        {
            base.Text = Caption;
            m_PeackValue = inPeackValue;
            base.SubItems.Insert((int)1, new ListViewSubItem(this, inPeackValue.ToString()));
            base.SubItems.Insert((int)2, new ListViewSubItem(this, Convert.ToString(inTime)));
        }

        public override string ToString()
        {
            return base.Text;
        }
    }
}