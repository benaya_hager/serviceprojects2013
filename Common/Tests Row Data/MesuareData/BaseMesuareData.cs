﻿using System;
using System.Xml.Serialization;
using Tools.String_Enum;
using System.ComponentModel;

namespace NS_Common.Tests_Row_Data.NS_MesuareData
{
    #region Public Types & Definitions

    public enum MesurementUnits
    {
        [StringValue("Unknown")]
        _Unknown = 0,
        [StringValue("KG")]
        _KG = 1,
        [StringValue("LG")]
        _LG = 2,
        [StringValue("GR")]
        _GR = 3
    }

    public enum LevelType
    {
        Normal = 0,
        Warning = 32,
        NotRecognizedData = 64,
        Error = 1024
    }

    #endregion Public Types & Definitions

    public class BaseMesuareData : EventArgs
    {
        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion


        #region Public Properties

        [XmlIgnore, Export2ExcelAtribute(false)]
        protected static System.Reflection.PropertyInfo[] m_MeasureDataProperies = null;
        [XmlIgnore, Export2ExcelAtribute(false)]
        public static System.Reflection.PropertyInfo[] MeasureDataProperies
        {
            get { return m_MeasureDataProperies; }
            protected set { m_MeasureDataProperies = value; }
        }


        [XmlIgnore, Export2ExcelAtribute(false)]
        protected long m_TimeFromTestStart = 0;
        /// <summary>
        /// Time From Test Start
        /// </summary>
        [XmlElement("Time_From_Test_Start"), Export2ExcelAtribute(false)]
        public virtual long TimeFromTestStart
        {
            get { return m_TimeFromTestStart; }
            set { m_TimeFromTestStart = value; }
        }

        #endregion Public Properties

        #region Constructors

        public BaseMesuareData() : base() { }

        #endregion Constructors
 
        /// <summary>
        /// Creates a shallow copy of the current BaseMesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current BaseMesuareData object.</returns>
        public virtual BaseMesuareData Clone()
        {
            return (BaseMesuareData)this.MemberwiseClone();
        }

        protected virtual void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                MeasureDataProperies = typeof(BaseMesuareData).GetProperties();
            }
        }
         
        /// <summary>
        /// Implicit conversion to char.
        /// Conversion to original type only.
        /// </summary>
        /// <remarks>To return an Object type use the Value property.</remarks>
        /// <exception cref="System.Exception"> Thrown when no conversion is possible.</exception>
        public static implicit operator object[](BaseMesuareData v)
        {
            object[] arr = new object[m_MeasureDataProperies.Length];
            int iIndex = 0;
            foreach (System.Reflection.PropertyInfo pi in m_MeasureDataProperies)
            {
                arr[iIndex++] = pi.GetValue(v, null);
            }

            return arr;
        }
    }
}