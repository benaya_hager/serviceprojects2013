﻿using System;

namespace NS_Common.Tests_Row_Data.NS_MesuareData
{
    public class ErrorDesc : EventArgs
    {
        private Exception m_Exception;
        /// <summary>
        /// The copy of exception raised
        /// </summary>
        public Exception myEx
        {
            get { return m_Exception; }
            set { m_Exception = value; }
        }

        private short m_ErrorCode = ErrorCodesList.OK;
        /// <summary>
        /// Set/Get the error code number
        /// </summary>
        public short ErrorCode
        {
            get { return m_ErrorCode; }
            set { m_ErrorCode = value; }
        }

        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public ErrorDesc Clone()
        {
            return (ErrorDesc)this.MemberwiseClone();
        }

        public ErrorDesc(Exception ex, short inErrorCode)
            : base()
        {
            m_Exception = ex;
            m_ErrorCode = inErrorCode;
        }
    }
}