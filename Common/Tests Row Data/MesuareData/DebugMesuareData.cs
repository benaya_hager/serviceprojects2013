﻿namespace NS_Common.Tests_Row_Data.NS_MesuareData
{
    public class DebugMesuareData : MesuareData
    {
        public DebugMesuareData(double inCurrentValue, long inMilliseconds)
            : base(inCurrentValue, inMilliseconds)
        { }

        public DebugMesuareData(double inCurrentValue, double inCurrentPosition, long inMilliseconds)
            : base(inCurrentValue, inCurrentPosition, inMilliseconds)
        { }

        public DebugMesuareData(double inCurrentValue, LevelType inCurrentLevel, long inTime)
            : base(inCurrentValue, inCurrentLevel, inTime)
        { }
    }
}