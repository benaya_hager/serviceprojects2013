﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NS_Common.Tests_Row_Data.NS_MesuareData
{
    /// <summary>
    /// Transformation Measuring Measure data
    /// </summary>
    public class VCTMesuareData : MesuareData
    {
        [XmlIgnore, Export2ExcelAtribute(false)]
        private Int32 m_SamplesCount;
        [XmlIgnore, Export2ExcelAtribute(false)]
        public Int32 SamplesCount
        {
            get { return m_SamplesCount; }
            protected set { m_SamplesCount = value; }
        }



        //private Int32 m_MeasureIndex;
        //private Double m_SamplesRate;
        //private Double m_BlockTime;

        private double[,] m_Data;
        [XmlIgnore, Export2ExcelAtribute(false)]
        public double[,] Data
        {
            get
            {
                return m_Data;
            }
            private set
            {
                m_Data = value;
                //System.Buffer.BlockCopy(m_Data, 0, m_VoltageValues, 0, m_SamplesSount);
                //System.Buffer.BlockCopy(m_Data, m_SamplesSount, m_CurrentValues, 0, m_SamplesSount);
                //System.Buffer.BlockCopy(m_Data, m_SamplesSount * 2, m_TemperatureValues, 0, m_SamplesSount);


                //Array.Copy(m_Data, m_VoltageValues, 10) ;
                //Array.Copy(m_Data, m_CurrentValues, 10);
                //Array.Copy(m_Data, m_TemperatureValues, m_SamplesSount);
            }
        }

        private Boolean[] m_SaveValue;
        [XmlIgnore, Export2ExcelAtribute(false)]
        public Boolean[] SaveValue
        {
            get { return m_SaveValue; }
            private set { m_SaveValue = value; }
        }

        private double[] m_VoltageValues;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public double[] VoltageValues
        {
            get { return m_VoltageValues; }
            private set { m_VoltageValues = value; }
        }

        private double[] m_CurrentValues;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public double[] CurrentValues
        {
            get { return m_CurrentValues; }
            private set { m_CurrentValues = value; }
        }

        private double[] m_TemperatureValues;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public double[] TemperatureValues
        {
            get { return m_TemperatureValues; }
            private set { m_TemperatureValues = value; }
        }

        private double[] m_TimeValues;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public double[] TimeValues
        {
            get { return m_TimeValues; }
            private set { m_TimeValues = value; }
        }

        private Int32[] m_CycleNumber;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public Int32[] CycleNumber
        {
            get { return m_CycleNumber; }
            private set { m_CycleNumber = value; }
        }

        public VCTMesuareData() : base() { }

        public VCTMesuareData(Boolean set_active_properties) : base() { if (set_active_properties) SetActiveProperties(); }

        public VCTMesuareData(Double voltage_treshhold,
                              double[,] data,
                              Double block_time,
                              Double single_sample_time,
                              Int32 samples_count,
                              Int32 measure_index,
                              Int32 cycle_number,
                              Double voltage_coof,
                              Double current_coof,
                              Double temperature_coof)
            : base()
        {
            //SetActiveProperties();
            m_SamplesCount = samples_count;
            //m_SamplesRate = samples_rate;
            //m_MeasureIndex = measure_index;

            m_Data = data;



            m_VoltageValues = new double[samples_count];
            m_CurrentValues = new double[samples_count];
            m_TemperatureValues = new double[samples_count];
            m_TimeValues = new double[samples_count];
            m_SaveValue = new Boolean[samples_count];
            m_CycleNumber = new Int32[samples_count];

            for (int i = 0; i < samples_count; i++)
            {
                if (voltage_treshhold < m_Data[0, i])
                {
                    m_VoltageValues[i] = m_Data[0, i] * voltage_coof;
                    m_CurrentValues[i] = m_Data[1, i] * current_coof;
                    m_TemperatureValues[i] = m_Data[2, i] * temperature_coof;
                    m_CycleNumber[i] = cycle_number;
                    SaveValue[i] = true;
                }
                else
                    SaveValue[i] = false;
                m_TimeValues[i] = block_time * measure_index + single_sample_time * i;
            }
        }

        /// <summary>
        /// Creates a shallow copy of the current VCTMesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current VCTMesuareData object.</returns>
        public new VCTMesuareData Clone()
        {
            return (VCTMesuareData)this.MemberwiseClone();
        }

        protected override void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                // ;
                List<System.Reflection.PropertyInfo> tmpList = new List<System.Reflection.PropertyInfo>();
                foreach (System.Reflection.PropertyInfo prop in typeof(VCTMesuareData).GetProperties())
                {
                    Export2ExcelAtribute[] attrs = prop.GetCustomAttributes(typeof(Export2ExcelAtribute), false) as Export2ExcelAtribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsVisible4Export) continue;
                        tmpList.Add(prop);
                    }
                }
                MeasureDataProperies = new System.Reflection.PropertyInfo[tmpList.Count];
                Int32 iIndex = 0;
                foreach (System.Reflection.PropertyInfo prop in tmpList)
                {
                    MeasureDataProperies[iIndex++] = prop;
                }
            }
        }
    }
}