using System;
using System.Xml.Serialization;

namespace NS_Common.Tests_Row_Data.NS_MesuareData
{
    public class MesuareData : BaseMesuareData
    {
        #region Public Properties

        [XmlIgnore, Export2ExcelAtribute(false)]
        protected double m_CurrentValue = 0;
        /// <summary>
        /// Current value of LoadSail at measure time
        /// </summary>
        [XmlElement("Calculated_Value"), Export2ExcelAtribute(false)]
        public double CurrentValue
        {
            get { return m_CurrentValue; }
            set { m_CurrentValue = value; }
        }


        [XmlIgnore, Export2ExcelAtribute(false)]
        protected double m_CurrentPosition = 0;
        /// <summary>
        /// The encoder Position at measure time
        /// </summary>
        [XmlElement("Current_Position"), Export2ExcelAtribute(false)]
        public double CurrentPosition
        {
            get { return m_CurrentPosition; }
            set { m_CurrentPosition = value; }
        }


        [XmlIgnore, Export2ExcelAtribute(false)]
        protected long m_MesuareTime = 0;
        /// <summary>
        /// Time From Previous Measure
        /// </summary>
        [XmlElement("Time_From_Previous_Measure"), Export2ExcelAtribute(false)]
        public long MesuareTime
        {
            get { return m_MesuareTime; }
            set { m_MesuareTime = value; }
        }


        [XmlIgnore, Export2ExcelAtribute(false)]
        protected String m_OriginalDeviceString = "N\\A";
        /// <summary>
        /// The original string Received from VISHAY device
        /// </summary>
        [XmlElement("OriginalDeviceString"), Export2ExcelAtribute(false)]
        public String OriginalDeviceString
        {
            get { return m_OriginalDeviceString; }
            set { m_OriginalDeviceString = value; }
        }

        [XmlIgnore, Export2ExcelAtribute(false)]
        protected Boolean m_IsFailedToParse;
        /// <summary>
        /// Indicate if system failed to parse the received from Vishay device string.
        /// </summary>
        [XmlElement("IsFailedToParse"), Export2ExcelAtribute(false)]
        public Boolean IsFailedToParse { get { return m_IsFailedToParse; } set { m_IsFailedToParse = value; } }

        [XmlIgnore, Export2ExcelAtribute(false)]
        protected LevelType m_CurrentLevel = LevelType.Normal;
        /// <summary>
        /// One of the default value of level.
        /// </summary>
        [XmlIgnore, Export2ExcelAtribute(false)]
        public LevelType CurrentLevel
        {
            get { return m_CurrentLevel; }
            set { m_CurrentLevel = value; }
        }

        [XmlIgnore, Export2ExcelAtribute(false)]
        protected DateTime m_MeasuredTime = new DateTime();
        /// <summary>
        /// Actual Measure Time
        /// </summary>
        [XmlIgnore, Export2ExcelAtribute(false)]
        public DateTime MeasuredTime { get { return m_MeasuredTime; } set { m_MeasuredTime = value; } }


        #endregion Public Properties

        #region Constructors

        public MesuareData() { }

        public MesuareData(double inCurrentValue, long inMilliseconds)
            : base()
        {
            SetActiveProperties();

            m_CurrentValue = inCurrentValue;
            m_MesuareTime = inMilliseconds;
        }


        public MesuareData(double inCurrentValue, double inCurrentPosition, long inMilliseconds)
            : base()
        {
            SetActiveProperties();

            m_CurrentValue = inCurrentValue;
            m_MesuareTime = inMilliseconds;
            m_CurrentPosition = inCurrentPosition;
        }

        public MesuareData(double inCurrentValue, LevelType inCurrentLevel, long inTime)
            : base()
        {
            SetActiveProperties();

            m_CurrentValue = inCurrentValue;
            m_CurrentLevel = inCurrentLevel;
            m_MesuareTime = inTime;
        }

        public MesuareData(double inCurrentValue, LevelType inCurrentLevel, long inTime, long inTimeFromTestStart, String inOriginalVishayString = "N\\A", Boolean inIsFailedToParse = false)
            : base()
        {
            SetActiveProperties();

            m_MeasuredTime = DateTime.Now;
            m_CurrentValue = inCurrentValue;
            m_CurrentLevel = inCurrentLevel;
            m_MesuareTime = inTime;
            m_TimeFromTestStart = inTimeFromTestStart;
            m_OriginalDeviceString = inOriginalVishayString;
            m_IsFailedToParse = inIsFailedToParse;
        }

        public MesuareData(String inOriginalVishayString, long inMilliseconds)
            : base()
        {
            SetActiveProperties();

            m_OriginalDeviceString = inOriginalVishayString;
            m_MesuareTime = inMilliseconds;
        }

        public MesuareData(double inCurrentValue, double inCurrentPosition, long inMilliseconds, String inOriginalVishayString = "N\\A", Boolean inIsFailedToParse = false)
            : base()
        {
            SetActiveProperties();

            m_CurrentValue = inCurrentValue;
            m_MesuareTime = inMilliseconds;
            m_CurrentPosition = inCurrentPosition;
            m_OriginalDeviceString = inOriginalVishayString;
            m_IsFailedToParse = inIsFailedToParse;
        }

        public MesuareData(double inCurrentValue, DateTime in_measured_time)
            : base()
        {
            SetActiveProperties();
            m_CurrentValue = inCurrentValue;
            m_MeasuredTime = in_measured_time;
        }


        public MesuareData(double inCurrentValue, DateTime in_measured_time, String inOriginalString = "N\\A", long inMilliseconds = 0)
            : this(inCurrentValue, inMilliseconds)
        {
            m_OriginalDeviceString = inOriginalString;
            m_MesuareTime = inMilliseconds;
        }

        #endregion Constructors

        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public new MesuareData Clone()
        {
            return (MesuareData)this.MemberwiseClone();
        }

        protected override void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                MeasureDataProperies = typeof(MesuareData).GetProperties();
            }
        }

        /// <summary>
        /// Implicit conversion to char.
        /// Conversion to original type only.
        /// </summary>
        /// <remarks>To return an Object type use the Value property.</remarks>
        /// <exception cref="System.Exception"> Thrown when no conversion is possible.</exception>
        public static implicit operator object[](MesuareData v)
        {
            object[] arr = new object[m_MeasureDataProperies.Length];
            int iIndex = 0;
            foreach (System.Reflection.PropertyInfo pi in m_MeasureDataProperies)
            {
                arr[iIndex++] = pi.GetValue(v, null);
            }

            return arr;
        }
    }
}