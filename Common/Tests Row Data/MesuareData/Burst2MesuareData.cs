﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Common.Tests_Row_Data;
using Common.Tests_Row_Data.NS_MesuareData;

namespace Common.Common.Tests_Row_Data.NS_MesuareData
{
    public class Burst2MesuareData : MesuareData
    {
        [XmlIgnore, Export2ExcelAtribute(true)]
        public long B2_TimeFromTestStart { get { return m_TimeFromTestStart; } set { m_TimeFromTestStart = value; } }

        [XmlIgnore, Export2ExcelAtribute(true)]
        public long B2_TimeFromPreviousMeasure { get { return m_MesuareTime; } set { m_MesuareTime = value; } }

        [XmlIgnore, Export2ExcelAtribute(true)]
        public double B2_CurrentValue { get { return m_CurrentValue; } set { m_CurrentValue = value; } }

        [XmlIgnore, Export2ExcelAtribute(false)]
        public String B2_OriginalKellerString { get { return m_OriginalDeviceString; } set { m_OriginalDeviceString = value; } }

        [XmlIgnore, Export2ExcelAtribute(false)]
        public LevelType B2_CurrentLevel { get { return m_CurrentLevel; } set { m_CurrentLevel = value; } }


        public Burst2MesuareData() : base() { }

        public Burst2MesuareData(Boolean set_active_properties)
        { if (set_active_properties)SetActiveProperties(); }

        public Burst2MesuareData(long time_from_test_start, long time_from_previous_measure, double current_value)
            : base()
        {
            m_TimeFromTestStart = time_from_test_start;
            m_MesuareTime = time_from_previous_measure;
            m_CurrentValue = current_value;
        }


        public Burst2MesuareData(long time_from_test_start, long time_from_previous_measure, double current_value, LevelType level_type)
            : this(time_from_test_start, time_from_previous_measure, current_value)
        {
            m_CurrentLevel = level_type;
        }


        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public new Burst2MesuareData Clone()
        {
            return (Burst2MesuareData)this.MemberwiseClone();
        }

        protected override void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                // ;
                List<System.Reflection.PropertyInfo> tmpList = new List<System.Reflection.PropertyInfo>();
                foreach (System.Reflection.PropertyInfo prop in typeof(Burst2MesuareData).GetProperties())
                {
                    Export2ExcelAtribute[] attrs = prop.GetCustomAttributes(typeof(Export2ExcelAtribute), false) as Export2ExcelAtribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsVisible4Export) continue;
                        tmpList.Add(prop);
                    }
                }
                m_MeasureDataProperies = new System.Reflection.PropertyInfo[tmpList.Count];
                Int32 iIndex = 0;
                foreach (System.Reflection.PropertyInfo prop in tmpList)
                {
                    m_MeasureDataProperies[iIndex++] = prop;
                }
            }
        }


        /// <summary>
        /// Implicit conversion to char.
        /// Conversion to original type only.
        /// </summary>
        /// <remarks>To return an Object type use the Value property.</remarks>
        /// <exception cref="System.Exception">Thrown when no conversion is possible.</exception>
        public static implicit operator object[](Burst2MesuareData v)
        {
            //System.Reflection.PropertyInfo[] m_MeasureDataProperies = v.GetType().GetProperties();
            object[] arr = new object[m_MeasureDataProperies.Length];
            int iIndex = 0;
            foreach (System.Reflection.PropertyInfo pi in m_MeasureDataProperies)
            {
                arr[iIndex++] = pi.GetValue(v, null);
            }

            return arr;
        }



        ///// <summary>
        ///// Implicit conversion to char.
        ///// Conversion to original type only.
        ///// </summary>
        ///// <remarks>To return an Object type use the Value property.</remarks>
        ///// <exception cref="System.Exception">Thrown when no conversion is possible.</exception>
        //public static implicit operator MesuareData(Burst2MesuareData v)
        //{
        //    return new MesuareData(v.B2_CurrentValue, v.B2_CurrentLevel, v.B2_TimeFromPreviousMeasure, v.B2_TimeFromTestStart, v.OriginalDeviceString, v.IsFailedToParse);
        //}

        ///// <summary>
        ///// Implicit conversion to char.
        ///// Conversion to original type only.
        ///// </summary>
        ///// <remarks>To return an Object type use the Value property.</remarks>
        ///// <exception cref="System.Exception">Thrown when no conversion is possible.</exception>
        //public static implicit operator Burst2MesuareData(MesuareData v)
        //{
        //    return new Burst2MesuareData(v.TimeFromTestStart, v.MesuareTime, v.CurrentValue, v.CurrentLevel);
        //}
    }
}