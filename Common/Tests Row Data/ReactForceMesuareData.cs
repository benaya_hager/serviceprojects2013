﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Common.Tests_Row_Data;

namespace Common.Service
{
    public class ReactForceMesuareData : MesuareData
    {
        [XmlIgnore, Export2ExcelAtribute(true)]
        public long RF_TimeFromTestStart { get { return m_TimeFromTestStart; } set { m_TimeFromTestStart = value; } }

        [XmlIgnore, Export2ExcelAtribute(true)]
        public long RF_TimeFromPreviousMeasure { get { return m_MesuareTime; } set { m_MesuareTime = value; } }

        [XmlIgnore, Export2ExcelAtribute(true)]
        public double RF_CurrentValue { get { return m_CurrentValue; } set { m_CurrentValue = value; } }

        [XmlIgnore, Export2ExcelAtribute(false)]
        public LevelType RF_CurrentLevel { get { return m_CurrentLevel; } set { m_CurrentLevel = value; } }

        public ReactForceMesuareData() : base() { }

        public ReactForceMesuareData(long time_from_test_start, long time_from_previous_measure, double current_value, LevelType current_level)
            : base()
        {
            SetActiveProperties();

            m_TimeFromTestStart = time_from_test_start;
            m_MesuareTime = time_from_previous_measure;
            m_CurrentValue = current_value;
            m_CurrentLevel = current_level;
        }

        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public new ReactForceMesuareData Clone()
        {
            return (ReactForceMesuareData)this.MemberwiseClone();
        }

        protected override void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                // ;
                List<System.Reflection.PropertyInfo> tmpList = new List<System.Reflection.PropertyInfo>();
                foreach (System.Reflection.PropertyInfo prop in typeof(Burst2MesuareData).GetProperties())
                {
                    Export2ExcelAtribute[] attrs = prop.GetCustomAttributes(typeof(Export2ExcelAtribute), false) as Export2ExcelAtribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsVisible4Export) continue;
                        tmpList.Add(prop);
                    }
                }
                m_MeasureDataProperies = new System.Reflection.PropertyInfo[tmpList.Count];
                Int32 iIndex = 0;
                foreach (System.Reflection.PropertyInfo prop in tmpList)
                {
                    m_MeasureDataProperies[iIndex++] = prop;
                }
            }
        }


        /// <summary>
        /// Implicit conversion to char.
        /// Conversion to original type only.
        /// </summary>
        /// <remarks>To return an Object type use the Value property.</remarks>
        /// <exception cref="System.Exception">Thrown when no conversion is possible.</exception>
        public static implicit operator object[](ReactForceMesuareData v)
        {
            //System.Reflection.PropertyInfo[] m_MeasureDataProperies = v.GetType().GetProperties();
            object[] arr = new object[m_MeasureDataProperies.Length];
            int iIndex = 0;
            foreach (System.Reflection.PropertyInfo pi in m_MeasureDataProperies)
            {
                arr[iIndex++] = pi.GetValue(v, null);
            }

            return arr;
        }
    }
}