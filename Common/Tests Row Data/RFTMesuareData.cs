﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;

namespace Common.Tests_Row_Data.NS_MesuareData
{
    /// <summary>
    /// Reaction Force Tester Measure data
    /// </summary>
    public class RFTMesuareData : MesuareData
    {
        private DataRow m_DataRow;
        [XmlIgnore, Export2ExcelAtribute(false)]
        public DataRow RFT_DataRow
        {
            get
            {
                return m_DataRow;
            }
            set
            {
                m_DataRow = value;
                if (null != m_DataRow)
                {
                    m_EncoderValue = Convert.ToInt32(m_DataRow[0]);
                    m_LoadCell = Convert.ToDouble(m_DataRow[1]);
                }
            }
        }

        private int m_EncoderValue;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public int EncoderValue
        {
            get { return m_EncoderValue; }
            private set { m_EncoderValue = value; }
        }

        private double m_LoadCell;
        [XmlIgnore, Export2ExcelAtribute(true)]
        public double LoadCell
        {
            get { return m_LoadCell; }
            private set { m_LoadCell = value; }
        }

        public RFTMesuareData() : base() { }

        public RFTMesuareData(System.Data.DataRowChangeEventArgs data_row)
            : base()
        {
            SetActiveProperties();
            RFT_DataRow = data_row.Row;
        }

        /// <summary>
        /// Creates a shallow copy of the current MesuareData Object.
        /// </summary>
        /// <returns>A shallow copy of the current MesuareData object.</returns>
        public new RFTMesuareData Clone()
        {
            return (RFTMesuareData)this.MemberwiseClone();
        }

        protected override void SetActiveProperties()
        {
            if (null == m_MeasureDataProperies)
            {
                // ;
                List<System.Reflection.PropertyInfo> tmpList = new List<System.Reflection.PropertyInfo>();
                foreach (System.Reflection.PropertyInfo prop in typeof(RFTMesuareData).GetProperties())
                {
                    Export2ExcelAtribute[] attrs = prop.GetCustomAttributes(typeof(Export2ExcelAtribute), false) as Export2ExcelAtribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsVisible4Export) continue;
                        tmpList.Add(prop);
                    }
                }
                m_MeasureDataProperies = new System.Reflection.PropertyInfo[tmpList.Count];
                Int32 iIndex = 0;
                foreach (System.Reflection.PropertyInfo prop in tmpList)
                {
                    m_MeasureDataProperies[iIndex++] = prop;
                }
            }
        }


        /// <summary>
        /// Implicit conversion to char.
        /// Conversion to original type only.
        /// </summary>
        /// <remarks>To return an Object type use the Value property.</remarks>
        /// <exception cref="System.Exception">Thrown when no conversion is possible.</exception>
        public static implicit operator object[](RFTMesuareData v)
        {
            //System.Reflection.PropertyInfo[] m_MeasureDataProperies = v.GetType().GetProperties();
            object[] arr = new object[m_MeasureDataProperies.Length];
            int iIndex = 0;
            foreach (System.Reflection.PropertyInfo pi in m_MeasureDataProperies)
            {
                arr[iIndex++] = pi.GetValue(v, null);
            }

            return arr;
        }
    }
}