﻿using Microsoft.Practices.Unity;
using MVVM_Dialogs.Service;
using MVVM_Dialogs.Service.FrameworkDialogs.OpenFile;
using MVVM_Dialogs.Service.FrameworkDialogs.SaveAsFile;
using NS_Common;
using NS_Common.ViewModels.Common;
using NS_Routines_Controller_Common;
using NS_Routines_Controller_Common.Interfaces;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Routines_Controller_UI
{
	public class ManageRoutinesFilesViewModel : ViewModelBase, IManageRoutinesFilesViewModel
	{
		
		private string DEFAULT_ROUTINES_FILES_PATH = "C:\\Medinol\\" + Assembly.GetExecutingAssembly().FullName + "\\Routines\\";
		private readonly IDialogService dialogService;
		private readonly Func<IOpenFileDialog> openFileDialogFactory;
		private readonly Func<ISaveAsFileDialog> saveFileDialogFactory;
		
		private IUnityContainer m_Container;

		#region Constructor

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="container"></param>
			public ManageRoutinesFilesViewModel(IUnityContainer container)
			{
				ServiceLocator.RegisterSingleton<IDialogService, DialogService>();
				ServiceLocator.Register<IOpenFileDialog, OpenFileDialogViewModel>();
				ServiceLocator.Register<ISaveAsFileDialog, SaveAsFileDialogViewModel>();

				this.dialogService =  ServiceLocator.Resolve<IDialogService>();
				openFileDialogFactory = () => ServiceLocator.Resolve<IOpenFileDialog>();
				saveFileDialogFactory = () => ServiceLocator.Resolve<ISaveAsFileDialog>();
				m_Container = container;
			}

		#endregion

		#region Private Methods

			private IOpenFileDialog GetOpenFileDialog()
			{ return GetOpenFileDialog(DEFAULT_ROUTINES_FILES_PATH, GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN); }



			private IOpenFileDialog GetOpenFileDialog(string routines_files_path, string routines_file_pattern)
			{
				if (routines_files_path == String.Empty)
				{ routines_files_path = DEFAULT_ROUTINES_FILES_PATH; }
				// Let factory create the IOpenFileDialog
				IOpenFileDialog openFileDialog = openFileDialogFactory();
				openFileDialog.Filter = routines_file_pattern;
				openFileDialog.InitialDirectory = routines_files_path;
				openFileDialog.Title = "Load Routine File";
				return openFileDialog;
			}


			private ISaveAsFileDialog GetSaveAsFileDialog()
			{ return GetSaveAsFileDialog(DEFAULT_ROUTINES_FILES_PATH, GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN); }



			private ISaveAsFileDialog GetSaveAsFileDialog(string routines_files_path, string routines_file_pattern)
			{
				if (routines_files_path == String.Empty)
				{ routines_files_path = DEFAULT_ROUTINES_FILES_PATH; }
				// Let factory create the IOpenFileDialog
				ISaveAsFileDialog saveFileDialog = saveFileDialogFactory();
				saveFileDialog.Filter = routines_file_pattern;
				saveFileDialog.InitialDirectory = routines_files_path;
				saveFileDialog.Title = "Save as Routine File";
				return saveFileDialog;
			}

		#endregion



		/// <summary>
		/// Load the routine from XML file
		/// </summary>
		/// <param name="routine">Out parameter. The list of routine object </param>
		/// <returns>
		/// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
		/// errors codes from Common.ErrorCodesList
		/// </returns>
			public int LoadRoutineXmlFile(out String selected_file_name, string routines_files_path = "", string routines_file_pattern = GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN)
		{
			selected_file_name = "";
			IOpenFileDialog openFileDialog = GetOpenFileDialog(routines_files_path, routines_file_pattern);
			DialogResult result = dialogService.ShowOpenFileDialog(this, openFileDialog);
			if (result == DialogResult.OK)
			{
				selected_file_name = openFileDialog.FileName;
				return ErrorCodesList.OK; 
			}
			return ErrorCodesList.CANCELED;
		}
		 
		 
		public int SaveRoutineXmlFile(out String selected_file_name ,
									  string routines_files_path = "",
									  string routines_file_pattern = GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN)
		{
			selected_file_name = "";
			ISaveAsFileDialog saveFileDialog = GetSaveAsFileDialog(routines_files_path, routines_file_pattern);
			DialogResult result = dialogService.ShowSaveFileDialog(this, saveFileDialog);


			if (result == DialogResult.OK)
			{
				selected_file_name = saveFileDialog.FileName;
				return ErrorCodesList.OK;
			}
			else
				return ErrorCodesList.CANCELED;
		}
	}
}
