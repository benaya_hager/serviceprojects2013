﻿using Microsoft.Practices.Unity;
using NS_Routines_Controller_Common.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NS_Common.ApplicationExtensions;
using System.Collections.Specialized; 

namespace Routines_Controller_UI.View
{
    /// <summary>
    /// Interaction logic for RoutinesListPage.xaml
    /// </summary>
    public partial class RoutinesListPage : Page
    {
        #region Private Members

        private IUnityContainer m_Container;
        [Dependency]
        public IUnityContainer Container
        {
            get { return m_Container; }
            set
            {
                m_Container = value;
                if (!DesignerProperties.GetIsInDesignMode(this))
                {
                    if (null != m_Container)
                    {
                        this.DataContext = m_Container.Resolve<IRoutinesController>();
                    }
                }
            }
        }

        #endregion


        public RoutinesListPage()
        {
            if (null == m_Container)
            {
                Container = (IUnityContainer)Application.Current.GetContainer();
            }
 
            InitializeComponent();
 
            ((INotifyCollectionChanged)ListOfRoutinesPlayers.ItemsSource).CollectionChanged +=
                    new NotifyCollectionChangedEventHandler(List1CollectionChanged);


        }


        public void List1CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            Page_SizeChanged(sender, null);
        }


        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //if (ListOfRoutinesPlayers.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
            //{
                ListOfRoutinesPlayers.UpdateLayout();
                Size new_size;
                if (null == e)
                    new_size = new Size(this.ActualWidth, this.ActualHeight);
                else
                    new_size = e.NewSize;
 
                for (int i = 0; i < ListOfRoutinesPlayers.Items.Count; i++)
                {  
                    ListOfRoutinesPlayers.ScrollIntoView(ListOfRoutinesPlayers.Items[i]);
                    ListViewItem lbi1 = (ListViewItem)ListOfRoutinesPlayers.ItemContainerGenerator.ContainerFromIndex(i);
                    if (null != lbi1)
                    {
                        lbi1.Width = new_size.Width - 20;
                        lbi1.Height = new_size.Height / ListOfRoutinesPlayers.Items.Count - 20;
                    }

                }

                ListOfRoutinesPlayers.UpdateLayout();

              
           // }
        }

        private void ListViewItem_PreviewMouseRightButtonDown(object sender, MouseEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            if (item != null)
            {
                item.Focus();
                item.IsSelected = true;
                e.Handled = true;
            }
        }
    }
}
