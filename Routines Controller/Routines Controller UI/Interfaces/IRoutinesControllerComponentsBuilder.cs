﻿using System;
namespace Routines_Controller_UI.Interfaces
{
    public interface IRoutinesControllerComponentsBuilder
    {
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; set; }
        Microsoft.Practices.Unity.IUnityContainer Container { get; }
        GUIFactory GuiFactory { get; }
    }
}
