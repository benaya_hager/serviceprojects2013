﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Routine_Definitions.Routines;
using NS_Common;
using NS_Common.ViewModels.Common;
using System.Xml.Serialization;


namespace Routines_Controller_UI.Sample
{
    public class SampleRoutineControllerViewModel : ViewModelBase
    {

        [XmlIgnore]
        public RelayCommand<Object> UnloadMe { get; private set; }
        [XmlIgnore]
        public RelayCommand<Object> AddRoutine { get; private set; }


        private RoutinesCollection m_PerformanceRoutinesQueue = new RoutinesCollection();
        public RoutinesCollection PerformanceRoutinesQueue
        {
            get { return m_PerformanceRoutinesQueue; }
            set 
            { 
                m_PerformanceRoutinesQueue = value; 
            }
        }
        
        public SampleRoutineControllerViewModel()
        {
            UnloadMe = new RelayCommand<Object>(p => this.UnloadMeFromTheList(p), p => true);
            AddRoutine = new RelayCommand<Object>(p => this.AddRoutineToTheList(p), p => true);


            OnCollectionChange("en-US");                    // Default buttons from English

           
        }

        private void OnCollectionChange(object lang)
        {
            m_PerformanceRoutinesQueue.Add(new Routine("Test 1"));
            m_PerformanceRoutinesQueue.Add(new Routine("Test 2"));
            m_PerformanceRoutinesQueue.Add(new Routine("Test 6"));
            m_PerformanceRoutinesQueue.Add(new Routine("Test 24"));
            m_PerformanceRoutinesQueue.Add(new Routine("Test 44"));
        }


        protected virtual void AddRoutineToTheList(object routine)
        {
            m_PerformanceRoutinesQueue.Add(new Routine("Test 1"));
        }

        protected virtual void UnloadMeFromTheList(object routine)
        { 
            if (null != routine)
            {
                System.Collections.IList items = (System.Collections.IList)routine;
                if (items.Count > 0)
                    m_PerformanceRoutinesQueue.Remove((BaseRoutineDescriptor)items[0]); 
            }
        }
    }
}
