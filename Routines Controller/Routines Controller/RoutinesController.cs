using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization; 
using log4net;
using NS_RoutinesController.Routine_Definitions;  
using SFW;
using Tools.CustomRegistry;
using Tools.String_Enum;
using NS_Routines_Controller_Common;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Common;
using NS_Common.Event_Arguments;
using NS_Routines_Controller_Common.Routine_Definitions;
using Microsoft.Practices.Unity;
using NS_Routines_Controller_Common.Interfaces;
using System.Reflection;
using NS_Routines_Controller_Common.Routine_Definitions.Routines;
using NS_Common.ViewModels.Common;
using MVVM_Dialogs.Interfaces;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters; 


 

namespace NS_RoutinesController
{
    /// <summary>
    /// The routine manager class.
    /// </summary>
    public class RoutinesController : IIODevice, IRoutinesController
    {
        #region Public Types
 

        #endregion Public Types

        #region Public events delegate functions

        #region I'm Waiting

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when I'm Waiting state set.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ImWaitingHandler(Object Sender, RoutineEventArguments args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected ImWaitingHandler ImWaitingDelegate;

        protected void OnImWaiting(object Sender)
        {
            if (this.ImWaitingDelegate != null)
                this.ImWaitingDelegate(this, new RoutineEventArguments((BaseRoutineDescriptor)Sender));
        }

        /// <summary>
        /// re-define the ImWaiting event
        /// </summary>
        public event ImWaitingHandler ImWaiting
        {
            // this is the equivalent of ImWaiting += new EventHandler(...)
            add
            {
                if (this.ImWaitingDelegate == null || !this.ImWaitingDelegate.GetInvocationList().Contains(value))
                    this.ImWaitingDelegate += value;
            }
            // this is the equivalent of ImWaiting -= new EventHandler(...)
            remove
            {
                this.ImWaitingDelegate -= value;
            }
        }

        #endregion I'm Waiting
 
        #region Routine Finished

        /// <summary>
        /// The events delegate function. Occurs when routine is completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RoutineFinishedHandler(Object Sender, RoutineEventArguments args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected RoutineFinishedHandler RoutineFinishedDelegate;

        protected void OnRoutineFinished(object Sender, RoutineEventArguments args)
        {
            if (this.RoutineFinishedDelegate != null)
                this.RoutineFinishedDelegate(Sender, args);
        }

        /// <summary>
        /// re-define the RoutineFinished event
        /// </summary>
        public event RoutineFinishedHandler RoutineFinished
        {
            // this is the equivalent of RoutineFinished += new EventHandler(...)
            add
            {
                if (this.RoutineFinishedDelegate == null || !this.RoutineFinishedDelegate.GetInvocationList().Contains(value))
                    this.RoutineFinishedDelegate += value;
            }
            // this is the equivalent of RoutineFinished -= new EventHandler(...)
            remove
            {
                this.RoutineFinishedDelegate -= value;
            }
        }

        #endregion Routine Finished

        #region Before Routine Remove

        /// <summary>
        /// The events delegate function. Occurs when routine is going to be removed from performance loop.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void BeforeRoutineRemoveHandler(Object Sender, RoutineEventArguments args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected BeforeRoutineRemoveHandler BeforeRoutineRemoveDelegate;

        protected void OnBeforeRoutineRemove(object Sender, RoutineEventArguments args)
        {
            if (this.BeforeRoutineRemoveDelegate != null)
                this.BeforeRoutineRemoveDelegate(Sender, args);
        }

        /// <summary>
        /// re-define the BeforeRoutineRemove event
        /// </summary>
        public event BeforeRoutineRemoveHandler BeforeRoutineRemove
        {
            // this is the equivalent of BeforeRoutineRemove += new EventHandler(...)
            add
            {
                if (this.BeforeRoutineRemoveDelegate == null || !this.BeforeRoutineRemoveDelegate.GetInvocationList().Contains(value))
                    this.BeforeRoutineRemoveDelegate += value;
            }
            // this is the equivalent of BeforeRoutineRemove -= new EventHandler(...)
            remove
            {
                this.BeforeRoutineRemoveDelegate -= value;
            }
        }

        #endregion Before Routine Remove

        #region Change Routine State Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Routine state needs to be change.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ChangeRoutineStateHandler(Object Sender, Int32 RoutineIndex, _ROUTINE_STATE arg);

        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected ChangeRoutineStateHandler ChangeRoutineStateDelegate;

        protected void OnChangeRoutineState(object Sender, _ROUTINE_STATE arg)
        {
            if (ChangeRoutineStateDelegate != null)
            {
                if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
                {
                    ChangeRoutineStateDelegate(Sender, ((BaseRoutineDescriptor)Sender).RoutineIndex, arg);
                }
                else
                {
                    ChangeRoutineStateDelegate(Sender, -1, arg);
                }
            }
            else
            {
                //m_SystemLog.Error("The event change routine state was unhandled.");
                //throw new Exception("The event change routine state was unhandled.");
            }
        }

        /// <summary>
        /// re-define the ChangeRoutineState event
        /// </summary>
        public event ChangeRoutineStateHandler ChangeRoutineState
        {
            // this is the equivalent of ChangeRoutineState += new EventHandler(...)
            add
            {
                //if (ChangeRoutineStateDelegate != null)
                //{
                //    ChangeRoutineState -= ChangeRoutineStateDelegate;
                //    ChangeRoutineStateDelegate = null;
                //}
                if (this.ChangeRoutineStateDelegate == null || !this.ChangeRoutineStateDelegate.GetInvocationList().Contains(value))
                    ChangeRoutineStateDelegate += value;
            }
            // this is the equivalent of ChangeRoutineState -= new EventHandler(...)
            remove { ChangeRoutineStateDelegate -= value; }
        }

        #endregion Change Routine State Event

        #region Error Occurred during performance Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when there is an error during performance.
        /// The error description can be received from "ErrorMessage" and "ErrorCode"
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void PerformanceErrorOccuerdHandler(Object Sender, RoutineEventArguments args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected PerformanceErrorOccuerdHandler PerformanceErrorOccuerdDelegate;

        protected void OnPerformanceErrorOccuerd(object Sender, RoutineEventArguments args)
        {
            if (this.PerformanceErrorOccuerdDelegate != null)
                this.PerformanceErrorOccuerdDelegate(Sender, args);
        }

        /// <summary>
        /// re-define the PerformanceErrorOccuerd event
        /// </summary>
        public event PerformanceErrorOccuerdHandler PerformanceErrorOccuerd
        {
            // this is the equivalent of PerformanceErrorOccuerd += new EventHandler(...)
            add
            {
                //if (PerformanceErrorOccuerdDelegate != null)
                //{
                //    PerformanceErrorOccuerd -= PerformanceErrorOccuerdDelegate;
                //    PerformanceErrorOccuerdDelegate = null;
                //}
                if (this.PerformanceErrorOccuerdDelegate == null || !this.PerformanceErrorOccuerdDelegate.GetInvocationList().Contains(value))
                    PerformanceErrorOccuerdDelegate += value;
            }
            // this is the equivalent of PerformanceErrorOccuerd -= new EventHandler(...)
            remove { PerformanceErrorOccuerdDelegate -= value; }
        }

        #endregion Error Occurred during performance Event

        #region Update Splash Screen Event

        /// <summary>
        /// The events delegate function. Optional event, occurs when there is long operation and MotionController requests to update splash screen.
        /// </summary>
        /// <param name="Sender">The control that raised event handler</param>
        /// <param name="args">Not in use</param>
        public delegate void UpdateSplashScreenHandler(Object Sender, UpdateSplashEventArgs args);
        /// <summary>
        /// Optional event, occurs when there is long operation and MotionController requests to update splash screen.
        /// </summary>
        public static event UpdateSplashScreenHandler UpdateSplashScreen;

        /// <summary>
        /// Service function, used to check if there is handler for raised event declared.
        /// </summary>
        /// <param name="Sender">The Axis that raised the event</param>
        /// <param name="args"></param>
        public void OnUpdateSplashScreen(object sender, UpdateSplashEventArgs args)
        {
            if (UpdateSplashScreen != null)
                UpdateSplashScreen(sender, args);
        }

        #endregion Update Splash Screen Event

        #region Image Processing Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Image Processing Event state set.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void IPEventHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected IPEventHandler IPEventDelegate;

        protected void OnIPEvent(object Sender)
        {
            if (this.IPEventDelegate != null)
                this.IPEventDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the IPEvent event
        /// </summary>
        public event IPEventHandler IPEvent
        {
            // this is the equivalent of IPEvent += new EventHandler(...)
            add
            {
                //if (this.IPEventDelegate != null)
                //{
                //    IPEvent -= this.IPEventDelegate;
                //    this.IPEventDelegate = null;
                //}
                if (this.IPEventDelegate == null || !this.IPEventDelegate.GetInvocationList().Contains(value))
                {
                    this.IPEventDelegate += value;
                }
            }
            // this is the equivalent of IPEvent -= new EventHandler(...)
            remove
            {
                this.IPEventDelegate -= value;
            }
        }

        #endregion Image Processing Event

        #region Get Nevo Points List

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Get Nevo Points List set.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void NoPointsListAvailibleHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected NoPointsListAvailibleHandler NoPointsListAvailibleDelegate;

        protected void OnNoPointsListAvailible(object Sender)
        {
            if (this.NoPointsListAvailibleDelegate != null)
                this.NoPointsListAvailibleDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the NoPointsListAvailible event
        /// </summary>
        public event NoPointsListAvailibleHandler NoPointsListAvailible
        {
            // this is the equivalent of NoPointsListAvailible += new EventHandler(...)
            add
            {
                //if (this.NoPointsListAvailibleDelegate != null)
                //{
                //    NoPointsListAvailible -= this.NoPointsListAvailibleDelegate;
                //    this.NoPointsListAvailibleDelegate = null;
                //}
                if (this.NoPointsListAvailibleDelegate == null || !this.NoPointsListAvailibleDelegate.GetInvocationList().Contains(value))
                {
                    this.NoPointsListAvailibleDelegate += value;
                }
            }
            // this is the equivalent of NoPointsListAvailible -= new EventHandler(...)
            remove
            {
                this.NoPointsListAvailibleDelegate -= value;
            }
        }

        #endregion Get Nevo Points List

        #region Routine Step changed

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Routine Step changed.
        /// </summary>
        /// <param name="Sender">The Routine handler</param>
        /// <param name="Step">Current step handler</param>
        public delegate void RoutineStepChangedHandler(Object Sender, BaseStepDescriptor Step);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected RoutineStepChangedHandler RoutineStepChangedDelegate;

        protected void OnRoutineStepChanged(object Sender, BaseStepDescriptor Step)
        {
            if (this.RoutineStepChangedDelegate != null)
                this.RoutineStepChangedDelegate(Sender, Step);
        }

        /// <summary>
        /// re-define the RoutineStepChanged event
        /// </summary>
        public event RoutineStepChangedHandler RoutineStepChanged
        {
            // this is the equivalent of RoutineStepChanged += new EventHandler(...)
            add
            {
                //if (this.RoutineStepChangedDelegate != null)
                //{
                //    RoutineStepChanged -= this.RoutineStepChangedDelegate;
                //    this.RoutineStepChangedDelegate = null;
                //}
                if (this.RoutineStepChangedDelegate == null || !this.NoPointsListAvailibleDelegate.GetInvocationList().Contains(value))
                {
                    this.RoutineStepChangedDelegate += value;
                }
            }
            // this is the equivalent of RoutineStepChanged -= new EventHandler(...)
            remove
            {
                this.RoutineStepChangedDelegate -= value;
            }
        }

        #endregion Routine Step changed

        #region Display Image and wait for Confirmation Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Display Image and wait for Confirmation Event.
        /// </summary>
        /// <param name="sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void DisplayImageAndWait4ConfirmationHandler(Object sender, Int32 stent_index);

        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected DisplayImageAndWait4ConfirmationHandler DisplayImageAndWait4ConfirmationDelegate;

        protected void OnDisplayImageAndWait4Confirmation(object sender, Int32 stent_index)
        {
            if (DisplayImageAndWait4ConfirmationDelegate != null)
                DisplayImageAndWait4ConfirmationDelegate(sender, stent_index);
        }

        /// <summary>
        /// re-define the DisplayImageAndWait4Confirmation event
        /// </summary>
        public event DisplayImageAndWait4ConfirmationHandler DisplayImageAndWait4Confirmation
        {
            // this is the equivalent of DisplayImageAndWait4Confirmation += new EventHandler(...)
            add
            {
                //if (m_DisplayImageAndWait4ConfirmationDelegate != null)
                //{
                //    DisplayImageAndWait4Confirmation -= m_DisplayImageAndWait4ConfirmationDelegate;
                //    m_DisplayImageAndWait4ConfirmationDelegate = null;
                //}
                if (this.DisplayImageAndWait4ConfirmationDelegate == null || !this.DisplayImageAndWait4ConfirmationDelegate.GetInvocationList().Contains(value))
                {
                    this.DisplayImageAndWait4ConfirmationDelegate += value;
                }
            }
            // this is the equivalent of DisplayImageAndWait4Confirmation -= new EventHandler(...)
            remove { DisplayImageAndWait4ConfirmationDelegate -= value; }
        }

        #endregion Display Image and wait for Confirmation Event

        #region Video Action Clear Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs during action performance. Indicate the GUI level to perform clear on previous found points collection.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ClearPointsListHandler(Object Sender);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected ClearPointsListHandler ClearPointsListDelegate;

        protected virtual void OnClearPointsList(object Sender)
        {
            if (this.ClearPointsListDelegate != null)
                this.ClearPointsListDelegate(Sender);
        }

        /// <summary>
        /// re-define the ClearPointsList event
        /// </summary>
        public event ClearPointsListHandler ClearPointsList
        {
            // this is the equivalent of ClearPointsList += new EventHandler(...)
            add
            {
                //if (this.ClearPointsListDelegate != null)
                //{
                //    ClearPointsList -= this.ClearPointsListDelegate;
                //    this.ClearPointsListDelegate = null;
                //}
                if (this.ClearPointsListDelegate == null || !this.ClearPointsListDelegate.GetInvocationList().Contains(value))
                {
                    this.ClearPointsListDelegate += value;
                }
            }
            // this is the equivalent of ClearPointsList -= new EventHandler(...)
            remove
            {
                this.ClearPointsListDelegate -= value;
            }
        }

        #endregion Video Action Clear Event

        #region Video Capture Image Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs during action performance. Indicate the GUI level to perform Capture image.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void CaptureImageEventHandler(Object Sender, Int32 stent_index);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected CaptureImageEventHandler CaptureImageEventDelegate;

        protected virtual void OnCaptureImageEvent(object Sender, Int32 stent_index)
        {
            if (this.CaptureImageEventDelegate != null)
            {
                this.CaptureImageEventDelegate(Sender, stent_index);
            }
        }

        /// <summary>
        /// re-define the CaptureImageEvent event
        /// </summary>
        public event CaptureImageEventHandler CaptureImageEvent
        {
            // this is the equivalent of CaptureImageEvent += new EventHandler(...)
            add
            {
                //if (this.CaptureImageEventDelegate != null)
                //{
                //    CaptureImageEvent -= this.CaptureImageEventDelegate;
                //    this.CaptureImageEventDelegate = null;
                //}
                if (this.CaptureImageEventDelegate == null || !this.CaptureImageEventDelegate.GetInvocationList().Contains(value))
                {
                    this.CaptureImageEventDelegate += value;
                }
            }
            // this is the equivalent of CaptureImageEvent -= new EventHandler(...)
            remove
            {
                this.CaptureImageEventDelegate -= value;
            }
        }

        #endregion Video Capture Image Event

        #region Compute Points For Stent Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs during action performance. Indicate the GUI level to perform Compute Points For Stent procedure.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ComputePointsForStentEventHandler(Object Sender, Int32 stent_index, int num_of_models_for_over_lap, float min_over_lap_in_MM);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected ComputePointsForStentEventHandler ComputePointsForStentEventDelegate;

        protected virtual void OnComputePointsForStentEvent(object Sender, Int32 stent_index, int num_of_models_for_over_lap, float min_over_lap_in_MM)
        {
            if (this.ComputePointsForStentEventDelegate != null)
                this.ComputePointsForStentEventDelegate(Sender, stent_index, num_of_models_for_over_lap, min_over_lap_in_MM);
        }

        /// <summary>
        /// re-define the ComputePointsForStentEvent event
        /// </summary>
        public event ComputePointsForStentEventHandler ComputePointsForStentEvent
        {
            // this is the equivalent of ComputePointsForStentEvent += new EventHandler(...)
            add
            {
                //if (this.ComputePointsForStentEventDelegate != null)
                //{
                //    ComputePointsForStentEvent -= this.ComputePointsForStentEventDelegate;
                //    this.ComputePointsForStentEventDelegate = null;
                //}
                if (this.ComputePointsForStentEventDelegate == null || !this.ComputePointsForStentEventDelegate.GetInvocationList().Contains(value))
                {
                    this.ComputePointsForStentEventDelegate += value;
                }
            }
            // this is the equivalent of ComputePointsForStentEvent -= new EventHandler(...)
            remove
            {
                this.ComputePointsForStentEventDelegate -= value;
            }
        }

        #endregion Compute Points For Stent Event

        #region Go To Print Position Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs during action performance. Indicate the GUI level to perform Go To Print Position procedure.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void GoToPrintPositionEventHandler(Object Sender, Int32 stent_index, int jet_index);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected GoToPrintPositionEventHandler GoToPrintPositionEventDelegate;

        protected virtual void OnGoToPrintPositionEvent(object Sender, Int32 stent_index, int jet_index)
        {
            if (this.GoToPrintPositionEventDelegate != null)
                this.GoToPrintPositionEventDelegate(Sender, stent_index, jet_index);
        }

        /// <summary>
        /// re-define the GoToPrintPositionEvent event
        /// </summary>
        public event GoToPrintPositionEventHandler GoToPrintPositionEvent
        {
            // this is the equivalent of GoToPrintPositionEvent += new EventHandler(...)
            add
            {
                //if (this.GoToPrintPositionEventDelegate != null)
                //{
                //    GoToPrintPositionEvent -= this.GoToPrintPositionEventDelegate;
                //    this.GoToPrintPositionEventDelegate = null;
                //}
                if (this.GoToPrintPositionEventDelegate == null || !this.GoToPrintPositionEventDelegate.GetInvocationList().Contains(value))
                {
                    this.GoToPrintPositionEventDelegate += value;
                }
            }
            // this is the equivalent of GoToPrintPositionEvent -= new EventHandler(...)
            remove
            {
                this.GoToPrintPositionEventDelegate -= value;
            }
        }

        #endregion Go To Print Position Event

        #region New Routine Added To Performance Queue

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when New Routine Added To Performance Queue.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void NewRoutineAddedToPerformanceQueueHandler(Object Sender, RoutineEventArguments args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected NewRoutineAddedToPerformanceQueueHandler NewRoutineAddedToPerformanceQueueDelegate;

        protected void OnNewRoutineAddedToPerformanceQueue(object Sender, BaseRoutineDescriptor routine)
        {
            if (this.NewRoutineAddedToPerformanceQueueDelegate != null)
                this.NewRoutineAddedToPerformanceQueueDelegate(Sender, new RoutineEventArguments(routine));
        }

        /// <summary>
        /// re-define the NewRoutineAddedToPerformanceQueue event
        /// </summary>
        public event NewRoutineAddedToPerformanceQueueHandler NewRoutineAddedToPerformanceQueue
        {
            // this is the equivalent of NewRoutineAddedToPerformanceQueue += new EventHandler(...)
            add
            {
                if (this.NewRoutineAddedToPerformanceQueueDelegate == null || !this.NewRoutineAddedToPerformanceQueueDelegate.GetInvocationList().Contains(value))
                    this.NewRoutineAddedToPerformanceQueueDelegate += value;
            }
            // this is the equivalent of NewRoutineAddedToPerformanceQueue -= new EventHandler(...)
            remove
            {
                this.NewRoutineAddedToPerformanceQueueDelegate -= value;
            }
        }

        #endregion New Routine Added To Performance Queue

        #region New Routine For Editing Loaded

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when New Routine For Editing Loaded.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RoutineForEditingLoadedHandler(Object Sender, RoutineEventArguments args, String routine_file_name);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected RoutineForEditingLoadedHandler RoutineForEditingLoadedDelegate;

        protected void OnRoutineForEditingLoaded(object Sender, BaseRoutineDescriptor routine, String routine_file_name)
        {
            if (this.RoutineForEditingLoadedDelegate != null)
                this.RoutineForEditingLoadedDelegate(Sender, new RoutineEventArguments(routine), routine_file_name);
        }

        /// <summary>
        /// re-define the Routine For Editing Loaded event
        /// </summary>
        public event RoutineForEditingLoadedHandler RoutineForEditingLoaded
        {
            // this is the equivalent of Routine For Editing Loaded += new EventHandler(...)
            add
            {
                if (this.RoutineForEditingLoadedDelegate == null || !this.RoutineForEditingLoadedDelegate.GetInvocationList().Contains(value))
                    this.RoutineForEditingLoadedDelegate += value;
            }
            // this is the equivalent of Routine For Editing Loaded -= new EventHandler(...)
            remove
            {
                this.RoutineForEditingLoadedDelegate -= value;
            }
        }

        #endregion New Routine Added To Performance Queue

        #region Routine File Name Changed

        /// <summary>
        /// The events delegate function. Occurs when routine saved as succeeded
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RoutineFileSavedHandler(Object Sender, RoutineEventArguments args, String routine_file_name);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        protected RoutineFileSavedHandler RoutineFileSavedDelegate;

        protected void OnRoutineFileSaved(object Sender, RoutineEventArguments args, String routine_file_name)
        {
            if (this.RoutineFileSavedDelegate != null)
                this.RoutineFileSavedDelegate(Sender, args, routine_file_name);
        }

        /// <summary>
        /// re-define the RoutineFileSaved event
        /// </summary>
        public event RoutineFileSavedHandler RoutineFileSaved
        {
            // this is the equivalent of RoutineFileSaved += new EventHandler(...)
            add
            {
                if (this.RoutineFileSavedDelegate == null || !this.RoutineFileSavedDelegate.GetInvocationList().Contains(value))
                    this.RoutineFileSavedDelegate += value;
            }
            // this is the equivalent of RoutineFileSaved -= new EventHandler(...)
            remove
            {
                this.RoutineFileSavedDelegate -= value;
            }
        }

        #endregion #region Routine File Name Changed

        #region New Message

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when New Message received.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void NewMessageHandler(Object Sender, RCDataPrmNewMessage message);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private NewMessageHandler NewMessageDelegate;

        public void OnNewMessage(object Sender, RCDataPrmNewMessage message)
        {
            if (this.NewMessageDelegate != null)
                this.NewMessageDelegate(this, message);
        }

        /// <summary>
        /// re-define the NewMessage event
        /// </summary>
        public event NewMessageHandler NewMessage
        {
            // this is the equivalent of NewMessage += new EventHandler(...)
            add
            {
                if (this.NewMessageDelegate == null || !this.NewMessageDelegate.GetInvocationList().Contains(value))
                    this.NewMessageDelegate += value;
            }
            // this is the equivalent of NewMessage -= new EventHandler(...)
            remove
            {
                this.NewMessageDelegate -= value;
            }
        }

        #endregion New Message

        #endregion Public events delegate functions

        #region protected Members

        protected IUnityContainer m_Container;
        protected IManageFilesDialogsViewModel manageRoutinesFilesViewModel;

       


        /// <summary>
        /// The short descriptor of file.
        /// Used to create registry sub key to identify current type of settings
        /// </summary>
        protected const String m_DataShortDescriptor = "Routine";


        protected Thread m_RoutinesPerformanceThread = null;

        //protected Boolean m_Is_IO_Enable;

        //protected MotionController m_MotionCntr = null;

        protected ILog m_SystemLog = LogManager.GetLogger("RoutinesController");

        protected static Object m_ObjRoutineSynchronization = null;
        protected Queue m_ListOfHoldingThreads = new Queue();
        protected static Int32 m_RoutineSelectionCounter = 0;

        protected Dictionary<string, SFW.IComponent> m_ListOfDevices = new Dictionary<string, SFW.IComponent>();

        //protected Boolean m_StopRoutineThreadRequested = false;
        //protected Semaphore m_SemStopPerformaceQue = new Semaphore(1, 1);
        protected CancellationTokenSource m_StopRoutineThreadRequestedokenSource;

        #endregion protected Members

        #region     Constructors
    
        /// <summary>
        /// Base Empty constructor. protected for singleton implementation
        /// </summary>
        public RoutinesController(IUnityContainer container)
        {
            OnUpdateSplashScreen(this, new UpdateSplashEventArgs(25, "Starting Routines Controller initialization"));

            DataReceived = null;

            m_Container = container;

            manageRoutinesFilesViewModel =  m_Container.Resolve<IManageFilesDialogsViewModel>();

            OnUpdateSplashScreen(null, new UpdateSplashEventArgs(26, "Initialize registry data"));
            InitRegistryData();

            m_ObjRoutineSynchronization = new Object();
            m_PerformanceRoutinesQueue = new RoutinesCollection();


            UnloadMe = new RelayCommand<Object>(p => this.UnloadMeFromTheList(p), p => true);
            AddRoutine = new RelayCommand<Object>(p => this.AddRoutineToTheList(p), p => true);



            m_PerformanceRoutinesQueue.BeforeRoutineRemove +=
                new RoutinesCollection.BeforeRoutineRemoveHandler(m_PerformanceRoutinesQueue_BeforeRoutineRemove);


            //m_Is_IO_Enable = false;
            OnUpdateSplashScreen(this, new UpdateSplashEventArgs(30, "Finished Routines Controller initialization"));
        }
 

        #endregion SingleTone implimintation

        #region Public properties

        [XmlIgnore]
        public RelayCommand<Object> UnloadMe { get; protected set; }
        [XmlIgnore]
        public RelayCommand<Object> AddRoutine { get; protected set; }
 

        protected RoutinesCollection m_PerformanceRoutinesQueue = null;
        public RoutinesCollection PerformanceRoutinesQueue
        {
            get { return m_PerformanceRoutinesQueue; }
            set { m_PerformanceRoutinesQueue = value; }
        }
        

        public BaseRoutineDescriptor CurrentPerfromedRoutine
        {
            get { return m_CurrentPerfromedRoutine; }
        }


        protected Boolean m_IsRoutinesPerformanceThreadStarted = false;
        /// <summary>
        /// Get the state of Routines Performance Thread
        /// </summary>
        public Boolean IsRoutinesPerformanceThreadStarted
        {
            get { return m_IsRoutinesPerformanceThreadStarted; }
        }
 
        protected short m_ErrorCode = ErrorCodesList.OK;
        /// <summary>
        /// Get/Set the controller error state
        /// </summary>
        public short ErrorCode
        {
            get { return m_ErrorCode; }
            protected set { m_ErrorCode = value; }
        }

        protected String m_ErrorMessage = "";
        /// <summary>
        /// Get/Set the controller error message
        /// </summary>
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            protected set { m_ErrorMessage = value; }
        }

        protected int m_SleepIntervalBetweenRuns = 30;
        /// <summary>
        /// Sleep duration between each routine performance
        /// </summary>
        public int SleepIntervalBetweenRuns
        {
            get { return m_SleepIntervalBetweenRuns; }
            set { m_SleepIntervalBetweenRuns = value; }
        }

        protected Boolean m_UseDefaultFileManagementDialog = false;
        public Boolean UseDefaultFileManagementDialog
        {
            get { return m_UseDefaultFileManagementDialog; }
            set { m_UseDefaultFileManagementDialog = value; }
        }
        
        #region Module Settings Parameters

        protected String m_RoutinesFilePattern = "XML Files (*.xml)|*.xml";
        /// <summary>
        /// The pattern name of the configuration file
        /// </summary>
        [Category("Settings"), Description("The pattern name of the Routine file")]
        public String RoutinesFilePattern
        {
            get { return m_RoutinesFilePattern; }
            set { m_RoutinesFilePattern = value; }
        }

        protected String m_RoutinesFilesPath = "C:\\ES2\\Routines\\";  //"C:\\" + System.Windows.Forms.Application.ProductName + "\\Routines\\"; //Path.Combine(Path.GetTempPath(), Application.ProductName);
        /// <summary>
        /// Sets or returns the folder where file will be stored in.
        /// </summary>
        /// <remarks>Once set, you can just provide the file name without the path.
        /// The value can be overridden by passing a fully qualified path and file.</remarks>
        /// <exception cref="System.ApplicationException">Thrown when the path is not valid.</exception>
        [Category("Settings"), Description("The folder where file will be stored in.")]
        public String RoutinesFilesPath
        {
            get { return m_RoutinesFilesPath; }
            set
            {
                if (value == "")
                    m_RoutinesFilesPath = Path.Combine(Path.GetTempPath(), Assembly.GetExecutingAssembly().FullName);
                else
                {
                    m_RoutinesFilesPath = value;
                    if (Directory.Exists(m_RoutinesFilesPath))
                    {
                        if (!m_RoutinesFilesPath.EndsWith("\\"))
                        {
                            m_RoutinesFilesPath += "\\";
                        }
                    }
                    else
                    {
                        try
                        {
                            Directory.CreateDirectory(m_RoutinesFilesPath);
                        }
                        catch (Exception Ex)
                        {
                            throw new ApplicationException("Failed to create Folder \n\r" + Ex.Message);
                        }
                    }
                }
            }
        }

        #endregion Module Settings Parameters

        #endregion Public properties

        #region Public methods

        public short SetDeviceList(Dictionary<string, SFW.IComponent> device_list)
        {
            m_ListOfDevices = device_list;

            return ErrorCodesList.OK;
        }

        #region Add Routines Functions


        public Int32 AddRoutine2PerformanceQueue(BaseRoutineDescriptor RoutineObj,
                                                Object request_owner,
                                                Boolean with_copy = true,
                                                Boolean subscribe_to_events = true,
                                                Boolean set_devices = true) 
        {
            Int32 tmpRoutineIndex = -1;
            tmpRoutineIndex = AddRoutine2PerformanceQueue(RoutineObj, with_copy, subscribe_to_events, set_devices);
            OnNewRoutineAddedToPerformanceQueue(request_owner, m_PerformanceRoutinesQueue[tmpRoutineIndex]);
            return tmpRoutineIndex;
        }

        /// <summary>
        /// Add new Routine to performance Queue
        /// </summary>
        /// <param name="RoutineObj">The routine descriptor</param>
        /// <returns>The index of routine in performance queue</returns>
        /// <remarks>Important!!!!! Please remove the routine as soon you don't need it in the list</remarks>
        public Int32 AddRoutine2PerformanceQueue(BaseRoutineDescriptor RoutineObj,
                                                Boolean with_copy = true,
                                                Boolean subscribe_to_events = true,
                                                Boolean set_devices = true)
        {
            BaseRoutineDescriptor tmpRoutineObj;
            if (with_copy)
                tmpRoutineObj = RoutineObj.Clone();
            else
                tmpRoutineObj = RoutineObj;

            tmpRoutineObj.RoutineState = _ROUTINE_STATE.RS_READY_TO_START;
            if (subscribe_to_events)
                tmpRoutineObj.SetStepsEvents();

            Int32 tmpRoutineIndex = -1;
            //tmpRoutineObj.SetRoutineEvents(this);

            lock (m_PerformanceRoutinesQueue)
            {
                tmpRoutineIndex = m_PerformanceRoutinesQueue.Add(tmpRoutineObj);
            }

            if (set_devices)
            {
                m_ErrorCode = m_PerformanceRoutinesQueue[tmpRoutineIndex].SetActionsDevices(m_ListOfDevices, out m_ErrorMessage);
                if (NS_Common.ErrorCodesList.OK != m_ErrorCode)
                {
                    throw new Exception(m_ErrorMessage);
                }
            }

            //m_PerformanceRoutinesQueue[tmpRoutineIndex].SetStepsEvents();

            m_PerformanceRoutinesQueue[tmpRoutineIndex].RoutineIndex = tmpRoutineIndex;

            OnNewRoutineAddedToPerformanceQueue(this, m_PerformanceRoutinesQueue[tmpRoutineIndex]);

            return tmpRoutineIndex;
        }


        public void UpdateRoutineForEditing(BaseRoutineDescriptor m_Routine, String routine_file_name)
        {
            OnRoutineForEditingLoaded(this, m_Routine, routine_file_name); 
           // throw new NotImplementedException();
        }

        #endregion Add Routines Functions

        #region Remove Routine Functions

        /// <summary>
        /// Removes the routine by it's Index identifier in performance queue
        /// </summary>
        /// <param name="RoutineIndex">The routine index in performance queue</param>
        /// <returns>If found and removed return TRUE: Otherwise return FALSE</returns>
        public Boolean RemoveRoutineFormPerformanceQueue(Int32 RoutineIndex)
        {
            if (m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
            {
                lock (m_PerformanceRoutinesQueue)
                {
                    PauseRoutinePerformance(RoutineIndex, true);
                    m_PerformanceRoutinesQueue.Remove(RoutineIndex);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes all routines that their state differ the RS_IN_PROGRESS/RS_IM_WAITING/RS_PAUSED
        /// </summary>
        public void RemoveAllInactiveRoutinesFormPerformanceQueue()
        {
            do
            {
            } while (RemoveFirstInactiveRoutine());
        }

        public void RemoveAllRoutinesFromPerformanceQueue()
        {
            m_PerformanceRoutinesQueue.Clear();
        }

        #endregion Remove Routine Functions

        #region Routine Control functions

        public short AddSingleMovementAction2ExistingRoutine(Int32 RoutineIndex, Byte AxisNetworkIdentifier,
                                                         Double AxisTaretPositionMM)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            //if (m_PerformanceRoutinesQueue[RoutineIndex].GetType() == typeof(SingleWeldingRoutine))
            //{
            //    ((SingleWeldingRoutine)m_PerformanceRoutinesQueue[RoutineIndex]).AddSingleMovementStep(
            //        AxisNetworkIdentifier, AxisTaretPositionMM, true);
            //    return ErrorCodesList.OK;
            //}
            return ErrorCodesList.INITIALIZATION_ERROR;
        }

        public short AddSingleStep2ExistingRoutine(Int32 RoutineIndex, BaseStepDescriptor Step, Boolean bAddStepEvents)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (m_PerformanceRoutinesQueue[RoutineIndex].AddSingleStep(Step, bAddStepEvents))
                return ErrorCodesList.OK;

            return ErrorCodesList.INITIALIZATION_ERROR;
        }

        /// <summary>
        /// Get the state of stored routine
        /// </summary>
        /// <param name="RoutineIndex">The preloaded routine Index</param>
        /// <param name="RoutineState">Routine state</param>
        /// <returns></returns>
        public short GetRoutineStatus(Int32 RoutineIndex, out _ROUTINE_STATE RoutineState)
        {
            RoutineState = _ROUTINE_STATE.RS_NOT_SET;

            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            RoutineState = m_PerformanceRoutinesQueue[RoutineIndex].RoutineState;

            return ErrorCodesList.OK;
        }

      

        public short PauseRoutinePerformance(Int32 RoutineIndex, Boolean StopActiveActions)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_PAUSED;
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            if (StopActiveActions)
            {
                if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
                {
                    try
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].StopActionsPerformance();
                    }
                    catch
                    {
                    }
                    finally
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                    }
                }
                else
                    return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            }
            return ErrorCodesList.OK;
        }

        public short ContinueRoutine(Int32 RoutineIndex)
        {
            if (m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
            {
                ((BaseRoutineDescriptor)m_PerformanceRoutinesQueue[RoutineIndex]).ContinueRoutine();
                return ErrorCodesList.OK;
            }
            else
            {
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;
            }
        }

        public short PerformRoutine(Int32 RoutineIndex)
        {
            short result = ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;
            if (m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
            {
                if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
                {
                    try
                    {
                        Int32 iCount = 0;
                        while (iCount < 10 && m_PerformanceRoutinesQueue[RoutineIndex].RoutineState != _ROUTINE_STATE.RS_IN_PROGRESS)
                        {
                            m_PerformanceRoutinesQueue[RoutineIndex].StartRoutinePerformance();
                            iCount++;
                        }
                        if (iCount >= 10)
                            result = ErrorCodesList.FAILED;
                        else
                            result = ErrorCodesList.OK;
                    }
                    catch
                    {
                    }
                    finally
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                    }
                }
                else
                    result = ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            }
            return result;
        }

        /// <summary>
        /// Stops selected routine performance. The routine state sets to ready for start.
        /// All unFinished actions stopped. The step index set to first one.
        /// </summary>
        /// <param name="RoutineIndex">The routine index to stop</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short AbortRoutine(Int32 RoutineIndex)
        {
            short tmpResult = PauseRoutinePerformance(RoutineIndex, true);
            if (tmpResult != ErrorCodesList.OK)
                return tmpResult;

            ErrorCode = ErrorCodesList.ROUTINE_ABORTED;

            m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_COMPLETED;

            // take the error code back
            ErrorCode = ErrorCodesList.OK;

            return ErrorCodesList.ROUTINE_ABORTED;
        }


       

        /// <summary>
        /// Stops all routines performance. Each routine state sets to ready for start.
        /// All unFinished actions stopped. The step index set to first one.
        /// </summary>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short AbortAllRoutines()
        {
            foreach (BaseRoutineDescriptor routine in m_PerformanceRoutinesQueue) //.Values)
            {
                Int32 RoutineIndex = routine.RoutineIndex;
                short tmpResult = PauseRoutinePerformance(RoutineIndex, true);

                if (tmpResult != ErrorCodesList.OK)
                    return tmpResult;

                ErrorCode = ErrorCodesList.ROUTINE_ABORTED;
                if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
                {
                    try
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_COMPLETED;
                    }
                    catch
                    {
                    }
                    finally
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                    }
                }

                // take the error code back
                ErrorCode = ErrorCodesList.OK;
            }
            return ErrorCodesList.ROUTINE_ABORTED;
        }

        /// <summary>
        /// Move the step index of routine to next one.
        /// </summary>
        /// <param name="RoutineIndex">The routine Index in performance queue</param>
        /// <param name="StopActiveActions">In case the routine is performing previous step if this parameter is set:
        /// TRUE stops active actions and jump to next step
        /// FALSE wait till step completed and when jump to next step</param>
        /// <param name="SkipNotEnabled">If true will continue till the routine completed or
        /// till the first enable step in the list follows requested index</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short JumpToNextStep(Int32 RoutineIndex, Boolean StopActiveActions, Boolean SkipNotEnabled)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS &&
                        //  m_PerformanceRoutinesQueue[RoutineIndex].SelectedNode.GetType() != typeof(SingleWeldingStep) &&
                                            m_PerformanceRoutinesQueue[RoutineIndex].RoutineStepInProgress)
                    {
                        //m_PerformanceRoutinesQueue[RoutineIndex].
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                        return ErrorCodesList.FAILED;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            //if (m_PerformanceRoutinesQueue[RoutineIndex].SelectedNode.GetType() != typeof(SingleWeldingStep))
            //{
            //    int i;
            //    i = 0;
            //}
            short Result;
            Result = PauseRoutinePerformance(RoutineIndex, StopActiveActions);
            if (Result != ErrorCodesList.OK)
                return Result;

            //Wait while step complete
            Result = WaitStepComplete(m_PerformanceRoutinesQueue[RoutineIndex]);
            if (Result != ErrorCodesList.OK)
                return Result;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    m_PerformanceRoutinesQueue[RoutineIndex].SetStepIndex(
                        m_PerformanceRoutinesQueue[RoutineIndex].SelectedStepIndex + 1, 1, SkipNotEnabled);

                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_PAUSED)
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].IncreeseStepIndex = false;
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Move the step index of routine to previous one.
        /// </summary>
        /// <param name="RoutineIndex">The routine Index in performance queue</param>
        /// <param name="StopActiveActions">In case the routine is performing previous step if this parameter is set:
        /// TRUE stops active actions and jump to next step
        /// FALSE wait till step completed and when jump to next step</param>
        /// <param name="SkipNotEnabled">If true will continue till the routine first step or
        /// till the first enable step in the list follows requested index</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short JumpToPreviousStep(Int32 RoutineIndex, Boolean StopActiveActions, Boolean SkipNotEnabled)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS &&
                        // m_PerformanceRoutinesQueue[RoutineIndex].SelectedNode.GetType() != typeof(SingleWeldingStep) &&
                                            m_PerformanceRoutinesQueue[RoutineIndex].RoutineStepInProgress)
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                        return ErrorCodesList.FAILED;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }

            short Result = PauseRoutinePerformance(RoutineIndex, StopActiveActions);
            if (Result != ErrorCodesList.OK)
                return Result;

            //Wait while step complete
            Result = WaitStepComplete(m_PerformanceRoutinesQueue[RoutineIndex]);
            if (Result != ErrorCodesList.OK)
                return Result;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    //if (m_PerformanceRoutinesQueue[RoutineIndex].GetType() == typeof(SingleFoldingRoutine))
                    //    m_PerformanceRoutinesQueue[RoutineIndex].SetStepIndex(
                    //        m_PerformanceRoutinesQueue[RoutineIndex].SelectedIndex - 1, -1, SkipNotEnabled);
                    //else
                    m_PerformanceRoutinesQueue[RoutineIndex].SetStepIndex(
                        m_PerformanceRoutinesQueue[RoutineIndex].SelectedStepIndex - 3, -1, SkipNotEnabled);

                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_PAUSED)
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].IncreeseStepIndex = false;
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Move the step index of routine to specified index.
        /// </summary>
        /// <param name="RoutineIndex">The routine Index in performance queue</param>
        /// <param name="StepIndex">The required step Index. It has to be higher or equal to 0. And less then total number of steps in routine</param>
        /// <param name="StopActiveActions">In case the routine is performing previous step if this parameter is set:
        /// TRUE stops active actions and jump to next step
        /// FALSE wait till step completed and when jump to next step</param>
        /// <param name="SkipNotEnabled">If true will continue till the routine completed or
        /// till the first enable step in the list follows requested index</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short JumpToStep(Int32 RoutineIndex, Int32 StepIndex, Boolean StopActiveActions, Boolean SkipNotEnabled)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    if (StepIndex >= m_PerformanceRoutinesQueue[RoutineIndex].StepColl.Count || StepIndex < 0)
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                        return ErrorCodesList.INVALID_PARAMETER_VALUE;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            short Result = PauseRoutinePerformance(RoutineIndex, StopActiveActions);
            if (Result != ErrorCodesList.OK)
                return Result;

            //Wait while step complete
            Result = WaitStepComplete(m_PerformanceRoutinesQueue[RoutineIndex]);
            if (Result != ErrorCodesList.OK)
                return Result;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    m_PerformanceRoutinesQueue[RoutineIndex].SetStepIndex(StepIndex, 1, SkipNotEnabled);
                    if (m_SystemLog.IsDebugEnabled)
                    {
                        try
                        {
                            if (m_SystemLog.IsDebugEnabled)
                                m_SystemLog.Debug("Routine " + m_PerformanceRoutinesQueue[RoutineIndex].RoutineName + " --> selected step Index set to " + m_PerformanceRoutinesQueue[RoutineIndex].SelectedStepIndex.ToString());
                        }
                        catch { }
                    }
                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_PAUSED)
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].IncreeseStepIndex = false;
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            return ErrorCodesList.OK;
        }

        public short PauseAllStageRoutines(Boolean StopActiveActions)
        {
            lock (m_PerformanceRoutinesQueue)
            {
                foreach (BaseRoutineDescriptor tmpRoutine in m_PerformanceRoutinesQueue) //.Values)
                {
                    try
                    {
                        if (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                        //|| tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IM_WAITING)
                        {
                            //if (tmpRoutine.GetType() != typeof(SingleFoldingRoutine))
                            PauseRoutinePerformance(tmpRoutine.RoutineIndex, StopActiveActions);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return ErrorCodesList.OK;
        }

        public short PauseAllRoutines(Boolean StopActiveActions)
        {
            lock (m_PerformanceRoutinesQueue)
            {
                foreach (BaseRoutineDescriptor tmpRoutine in m_PerformanceRoutinesQueue) //.Values)
                {
                    try
                    {
                        if (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                        //||  tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IM_WAITING)
                        {
                            PauseRoutinePerformance(tmpRoutine.RoutineIndex, StopActiveActions);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return ErrorCodesList.OK;
        }

        public short ContinueAllPausedRoutines()
        {
            lock (m_PerformanceRoutinesQueue)
            {
                foreach (BaseRoutineDescriptor tmpRoutine in m_PerformanceRoutinesQueue) //.Values)
                {
                    try
                    {
                        if (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_PAUSED)
                        {
                            ContinueRoutine(tmpRoutine.RoutineIndex);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Move the step index of routine to the step with database step index.
        /// </summary>
        /// <param name="RoutineIndex">The routine Index in performance queue</param>
        /// <param name="DatabaseStepIndex">The required step Index. It has to be higher or equal to 0. And less then total number of steps in routine</param>
        /// <param name="StopActiveActions">In case the routine is performing previous step if this parameter is set:
        /// TRUE stops active actions and jump to next step
        /// FALSE wait till step completed and when jump to next step</param>
        /// <param name="SkipNotEnabled">If true will continue till the routine completed or
        /// till the first enable step in the list follows requested index</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short JumpToDatabaseStepIndex(Int32 RoutineIndex, Int32 DatabaseStepIndex, Boolean StopActiveActions,
                                             Boolean SkipNotEnabled)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS &&
                        //   m_PerformanceRoutinesQueue[RoutineIndex].SelectedNode.GetType() != typeof(SingleWeldingStep) &&
                                            m_PerformanceRoutinesQueue[RoutineIndex].RoutineStepInProgress)
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                        return ErrorCodesList.FAILED;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            short Result = PauseRoutinePerformance(RoutineIndex, StopActiveActions);
            if (Result != ErrorCodesList.OK)
                return Result;

            //Wait while step complete
            Result = WaitStepComplete(m_PerformanceRoutinesQueue[RoutineIndex]);
            if (Result != ErrorCodesList.OK)
                return Result;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    m_PerformanceRoutinesQueue[RoutineIndex].SetStepDatabaseIndex(DatabaseStepIndex, 1, SkipNotEnabled);
                    if (m_SystemLog.IsDebugEnabled)
                    {
                        try
                        {
                            if (m_SystemLog.IsDebugEnabled)
                                m_SystemLog.Debug("Routine " + m_PerformanceRoutinesQueue[RoutineIndex].RoutineName + " --> selected step Index set to " + m_PerformanceRoutinesQueue[RoutineIndex].SelectedStepIndex.ToString());
                        }
                        catch { }
                    }
                    if (m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_PAUSED ||
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState == _ROUTINE_STATE.RS_READY_TO_START)
                    {
                        m_PerformanceRoutinesQueue[RoutineIndex].IncreeseStepIndex = false;
                        m_PerformanceRoutinesQueue[RoutineIndex].RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;
                    }
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            return ErrorCodesList.OK;
        }

        #region Routine State Control Functions

        public void BaseRoutineDescriptor_Jump2NextStep(object Sender, EventArgs args)
        {
            if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
            {
                JumpToNextStep(((BaseRoutineDescriptor)Sender).RoutineIndex, true, true);
            }
        }

        public void BaseRoutineDescriptor_Jump2PreviousStep(object Sender, EventArgs args)
        {
            if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
            {
                JumpToPreviousStep(((BaseRoutineDescriptor)Sender).RoutineIndex, true, true);
            }
        }

        public void BaseRoutineDescriptor_StopRoutine(object Sender, EventArgs args)
        {
            if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
            {
                // PauseRoutinePerformance(((BaseRoutineDescriptor)Sender).RoutineIndex, true);
                AbortRoutine(((BaseRoutineDescriptor)Sender).RoutineIndex);
            }
        }

        public void BaseRoutineDescriptor_PauseRoutine(object Sender, EventArgs args)
        {
            if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
            {
                PauseRoutinePerformance(((BaseRoutineDescriptor)Sender).RoutineIndex, true);
            }
        }

        public void BaseRoutineDescriptor_PlayRoutine(object Sender, EventArgs args)
        {
            if (Sender.GetType().BaseType == typeof(BaseRoutineDescriptor))
            {
                PerformRoutine(((BaseRoutineDescriptor)Sender).RoutineIndex);
            }
        }

        #endregion Routine State Control Functions


        #endregion Routine Control functions

        #region Routine Controller Main thread control functions

        /// <summary>
        /// Start the main routines performance loop
        /// </summary>
        public void StartRoutinePerformance()
        {
            if (m_RoutinesPerformanceThread != null)
                if (m_RoutinesPerformanceThread.ThreadState == ThreadState.Running ||
                    m_RoutinesPerformanceThread.ThreadState == ThreadState.Background)
                {
                    return;
                }
            m_StopRoutineThreadRequestedokenSource = new CancellationTokenSource();
            m_RoutinesPerformanceThread = new Thread(new ParameterizedThreadStart(RoutinePerformance));
            m_RoutinesPerformanceThread.IsBackground = true;
            m_RoutinesPerformanceThread.Name = "Routines Performance main thread";
            m_RoutinesPerformanceThread.SetApartmentState(ApartmentState.STA);
            m_RoutinesPerformanceThread.Priority = ThreadPriority.Lowest;
            m_RoutinesPerformanceThread.Start(m_StopRoutineThreadRequestedokenSource.Token);
            m_IsRoutinesPerformanceThreadStarted = true;
        }

        /// <summary>
        /// Stop the Routine performance loop without waiting for complete loop.
        /// Please be careful for memory leaks while using this method.
        /// </summary>
        public void StopRoutinePerformance()
        {
            m_IsRoutinesPerformanceThreadStarted = false;
            if (m_RoutinesPerformanceThread != null)
            {
                try
                {
                    m_StopRoutineThreadRequestedokenSource.Cancel();
                    //m_StopRoutineThreadRequested = true;
                    //m_SemStopPerformaceQue = new Semaphore(1, 1);
                    //m_SemStopPerformaceQue.WaitOne();
                    //m_RoutinesPerformanceThread.Abort();
                }
                catch { }
                finally
                {
                    m_RoutinesPerformanceThread = null;
                }
            }
        }

        /// <summary>
        /// Runs through each of the routines in the queue and stops it.
        /// </summary>
        public void StopAllRoutines()
        {
            //foreach (KeyValuePair<Int32, BaseRoutineDescriptor> item in m_PerformanceRoutinesQueue)
            foreach (BaseRoutineDescriptor item in m_PerformanceRoutinesQueue)
            {
                AbortRoutine(item.RoutineIndex );
            }
        }

        #endregion Routine Controller Main thread control functions

        #region Routine Service functions

        /// <summary>
        /// Return the active step of routine with specified index
        /// </summary>
        /// <param name="RoutineIndex">The routine Index in performance queue</param>
        /// <param name="RoutineStep">Result class with active step data</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short GetRoutineActiveStep(Int32 RoutineIndex, out BaseStepDescriptor RoutineStep)
        {
            RoutineStep = null;

            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    if (m_PerformanceRoutinesQueue[RoutineIndex].StepColl.Count == 0 ||
                        m_PerformanceRoutinesQueue[RoutineIndex].SelectedStepIndex < 0)
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                        return ErrorCodesList.INVALID_PARAMETER_VALUE;
                    }
                    RoutineStep =
                        (BaseStepDescriptor)
                        m_PerformanceRoutinesQueue[RoutineIndex].SelectedNode.Clone(
                            (BaseRoutineDescriptor)m_PerformanceRoutinesQueue[RoutineIndex]);
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Return the Routine index by routine database Identifier
        /// </summary>
        /// <param name="RoutineDatabaseID">Routine database Identifier</param>
        /// <param name="RoutineStep">The routine Index in performance queue</param>
        /// <returns>Common.ErrorCodesList.OK if succeeded: otherwise return other error code</returns>
        public short GetRoutineIndexByRoutineDatabaseID(Int32 RoutineDatabaseID, out Int32 RoutineIndex)
        {
            RoutineIndex = -1;

            foreach (BaseRoutineDescriptor Routine in m_PerformanceRoutinesQueue) //.Values)
            {
                if (EnterCriticalSection(Routine.SynchronizationObject, ref  Routine.LockCounter))
                {
                    try
                    {
                        if (Routine.RoutineDataBaseID == RoutineDatabaseID)
                        {
                            RoutineIndex = Routine.RoutineIndex;
                            ExitCriticalSection(Routine.SynchronizationObject, ref Routine.LockCounter);
                            break;
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        ExitCriticalSection(Routine.SynchronizationObject, ref  Routine.LockCounter);
                    }
                }
                else
                    return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            }
            return ErrorCodesList.OK;
        }

    
        public short GetRoutineByRoutineIndex(Int32 RoutineIndex, out BaseRoutineDescriptor routine)
        {
            routine = null;
            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            routine = m_PerformanceRoutinesQueue[RoutineIndex];

            return ErrorCodesList.INVALID_PARAMETER_VALUE;
        }

        public short GetRoutineNameByRoutineIndex(Int32 RoutineIndex, out string routineName)
        {
            routineName = null;

            if (!m_PerformanceRoutinesQueue.ContainsKey(RoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter))
            {
                try
                {
                    //if (m_PerformanceRoutinesQueue[RoutineIndex].GetType() == typeof(SingleFoldingRoutine))
                    //{
                    //    routineName = ((SingleFoldingRoutine)m_PerformanceRoutinesQueue[RoutineIndex]).RoutineName;
                    //    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                    //    return ErrorCodesList.OK;
                    //}
                }
                catch
                {
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[RoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[RoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            return ErrorCodesList.INVALID_PARAMETER_VALUE;
        }

        #endregion Routine Service functions

        public short UpdateStepPosition(Int32 inRoutineIndex, Int32 stepIndex, Single XPositionMM, Single YPositionMM,
                                        Single ZPositionMM)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(inRoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[inRoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[inRoutineIndex].LockCounter))
            {
                try
                {
                    if (m_PerformanceRoutinesQueue[inRoutineIndex].RoutineState != _ROUTINE_STATE.RS_READY_TO_START)
                        // &&m_PerformanceRoutinesQueue[inRoutineIndex].RoutineState != _ROUTINE_STATE.RS_IM_WAITING
                        
                    {
                        ExitCriticalSection(m_PerformanceRoutinesQueue[inRoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[inRoutineIndex].LockCounter);
                        return ErrorCodesList.INVALID_PARAMETER_VALUE;
                    }

                    if ((m_PerformanceRoutinesQueue[inRoutineIndex].StepColl != null) &&
                        (m_PerformanceRoutinesQueue[inRoutineIndex].StepColl[stepIndex] != null))
                    {
                        //((SingleXYZMotionStep)m_PerformanceRoutinesQueue[inRoutineIndex].StepColl[stepIndex]).
                        //    UpdatePositionValues(XPositionMM, YPositionMM, ZPositionMM);
                    }
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[inRoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[inRoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;

            return ErrorCodesList.OK;
        }

        #region Save\Load Routine Functions

        public int SaveRoutineXmlFile(String FileName, BaseRoutineDescriptor Routine2Serialize, Type[] list = null)
        {
            string filePath = Path.Combine(new String[] { m_RoutinesFilesPath, Path.GetFileNameWithoutExtension(FileName) }) + "." + m_RoutinesFilePattern;
            try
            {
                Type[] list1 = list;
                if (null == list)
                {
                    list1 = Tools.Reflection.ReflectionTools.Concat(
                                          Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseRoutineDescriptor)), typeof(BaseRoutineDescriptor), true),
                                          Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseStepDescriptor)), typeof(BaseStepDescriptor), true),
                                          Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseActionDescriptor)), typeof(BaseActionDescriptor), true));
                }

                //Serialization
                XmlSerializer Serializer = new XmlSerializer(typeof(BaseRoutineDescriptor), list1);
                TextWriter Writer = new StreamWriter(filePath);
                Serializer.Serialize(Writer, Routine2Serialize);
                Writer.Close();

                OnRoutineFileSaved(this, new RoutineEventArguments(Routine2Serialize), FileName);
            }
            catch
            {
                return ErrorCodesList.FAILED;
            }

            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Save Axis list to XML data file
        /// </summary>
        /// <param name="AxisList">The list of axis descriptors</param>
        /// <param name="FileName">XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from MotionController.ErrorCodesList
        /// </returns>
        public int SaveRoutineXmlFile(String FileName, BaseRoutineDescriptor Routine2Serialize)
        {

            Type[] list = Tools.Reflection.ReflectionTools.Concat(
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseRoutineDescriptor)), typeof(BaseRoutineDescriptor), true),
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseStepDescriptor)), typeof(BaseStepDescriptor), true),
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseActionDescriptor)), typeof(BaseActionDescriptor), true));

            return SaveRoutineXmlFile(FileName, Routine2Serialize, list);
        }

        /// <summary>
        /// Save Axis list to XML data file
        /// </summary>
        /// <param name="AxisList">The list of axis descriptors</param>
        /// <param name="FileName">XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from MotionController.ErrorCodesList
        /// </returns>
        public int SaveRoutineXmlFile(BaseRoutineDescriptor Routine2Serialize, Type[] list = null)
        {
            String selected_file_name;
            int res = manageRoutinesFilesViewModel.SaveXmlFile(out selected_file_name, m_RoutinesFilesPath, m_RoutinesFilePattern, "Save as Routine File");

            if (res == ErrorCodesList.OK)
            {
                return SaveRoutineXmlFile(Path.Combine(RoutinesFilesPath, selected_file_name), Routine2Serialize, list);
            }
            else
                return ErrorCodesList.CANCELED;
        }

        public int SaveRoutineXmlFile(String FileName, Int32 inRoutineIndex, Type[] list = null)
        {
            if (!m_PerformanceRoutinesQueue.ContainsKey(inRoutineIndex))
                return ErrorCodesList.ROUTINE_NOT_FOUND_IN_PERFORMANCE_QUEUE;

            if (EnterCriticalSection(m_PerformanceRoutinesQueue[inRoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[inRoutineIndex].LockCounter))
            {
                try
                {
                    return SaveRoutineXmlFile(FileName, m_PerformanceRoutinesQueue[inRoutineIndex], list);
                }
                finally
                {
                    ExitCriticalSection(m_PerformanceRoutinesQueue[inRoutineIndex].SynchronizationObject, ref  m_PerformanceRoutinesQueue[inRoutineIndex].LockCounter);
                }
            }
            else
                return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
        }

        public int SaveRoutineXmlFile(Int32 inRoutineIndex, Type[] list = null)
        {
            String selected_file_name;
            int res = manageRoutinesFilesViewModel.SaveXmlFile(out selected_file_name, m_RoutinesFilesPath, m_RoutinesFilePattern, "Save as Routine File");

            if (res == ErrorCodesList.OK)
            {
                return SaveRoutineXmlFile(Path.Combine(RoutinesFilesPath, selected_file_name), inRoutineIndex, list);
            }
            else
                return ErrorCodesList.CANCELED;
        }


        /// <summary>
        /// Save Axis list to XML data file
        /// </summary>
        /// <param name="AxisList">The list of axis descriptors</param>
        /// <param name="FileName">XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from MotionController.ErrorCodesList
        /// </returns>
        public int SaveRoutineXmlFile(BaseRoutineDescriptor Routine2Serialize, out String routine_file_name, Type[] list = null)
        {
            int res = manageRoutinesFilesViewModel.SaveXmlFile(out routine_file_name, m_RoutinesFilesPath, m_RoutinesFilePattern, "Save as Routine File");

            if (res == ErrorCodesList.OK)
            {
                routine_file_name = Path.Combine(RoutinesFilesPath, routine_file_name);
                return SaveRoutineXmlFile(routine_file_name, Routine2Serialize, list);
            }
            else
                return ErrorCodesList.CANCELED;
        }



        /// <summary>
        /// Load the routine from XML file
        /// </summary>
        /// <param name="routine">Out parameter. The list of routine object</param>
        /// <param name="FileName">The XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public int LoadRoutineXmlFile(String file_name, out BaseRoutineDescriptor routine)
        {
            routine = null;

            if (!File.Exists(file_name))
            {
                //ExceptionsList.AxisXMLFileNotFound.MessageText = "The Routine file " + FileName + " not found";
                //ExceptionsList.AxisXMLFileNotFound.ErrorID = ErrorCodesList.AXIS_DEFINITION_FILE_NOT_FOUND_ERROR;
                //throw ExceptionsList.AxisXMLFileNotFound;
                return ErrorCodesList.FILE_NOT_FOUND;
            }

            //Load data from XML Axis File
            BaseRoutineDescriptor MyList;
            TextReader Reader = new StreamReader(file_name);

            Type[] list = Tools.Reflection.ReflectionTools.Concat(
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseRoutineDescriptor)), typeof(BaseRoutineDescriptor), true),
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseStepDescriptor)), typeof(BaseStepDescriptor), true),
                                  Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseActionDescriptor)), typeof(BaseActionDescriptor), true));
                                  //new List<System.Drawing.RectangleF> 
             
            //Serialization
            XmlSerializer Serializer = new XmlSerializer(typeof(BaseRoutineDescriptor), list);            //DeSerialization
            MyList = (BaseRoutineDescriptor)Serializer.Deserialize(Reader);
            Reader.Close();

            routine = MyList;

            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Load the routine from XML file
        /// </summary>
        /// <param name="routine">Out parameter. The list of routine object </param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public int LoadRoutineXmlFile(out String selected_file_name, out BaseRoutineDescriptor routine)
        {
            routine = null;
            selected_file_name = ""; 
           // dlg.InitialDirectory = m_RoutineCntrl.RoutinesFilesPath;
            String filter = "Routine Files (*." + m_RoutinesFilePattern + " )|*." + m_RoutinesFilePattern + "|All files (*.*)|*.*";
            //dlg.Multiselect = false;
            int res = manageRoutinesFilesViewModel.LoadXmlFile(out selected_file_name, m_RoutinesFilesPath,filter , "Open Routine File");
            if (res == ErrorCodesList.OK)
            {
                //Open file data
                return this.LoadRoutineXmlFile(Path.Combine(RoutinesFilesPath, selected_file_name), out routine);
            }
            return ErrorCodesList.CANCELED;
        }

        #endregion Save\Load Routine Functions

        
         

        #endregion Public methods

        #region Protected functions

        protected virtual void AddRoutineToTheList(object routine)
        {
            String routine_file_name;
            BaseRoutineDescriptor tmp_routine = null;
            Int32 result = LoadRoutineXmlFile(out routine_file_name, out tmp_routine);

            if (result == ErrorCodesList.OK )
                AddRoutine2PerformanceQueue(tmp_routine, false);
        }

        protected virtual void UnloadMeFromTheList(object routine)
        {
            if (null != routine)
            {
                m_PerformanceRoutinesQueue.Remove((BaseRoutineDescriptor)routine);
            }
        }

        /// <summary>
        /// Get base data from registry
        /// </summary>
        protected int InitRegistryData()
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc("Routines Controller",
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);


                this.RoutinesFilesPath = objRegistry.GetRegKeyValue("", "Routines Files Path", m_RoutinesFilesPath);
                objRegistry.SetRegKeyValue("", "Routines Files Path", m_RoutinesFilesPath);

                this.RoutinesFilePattern = objRegistry.GetRegKeyValue("", "Data File Pattern", m_RoutinesFilePattern);
                objRegistry.SetRegKeyValue("", "Routines Files Pattern", m_RoutinesFilePattern);

                m_UseDefaultFileManagementDialog = (objRegistry.GetRegKeyValue("", "Use Default File Management Dialog", m_UseDefaultFileManagementDialog == true ? 1 : 0) == 1 ? true : false);
                objRegistry.SetRegKeyValue("", "Use Default File Management Dialog", m_UseDefaultFileManagementDialog == true ? 1 : 0); 
                
            }
            catch { };
            return ErrorCodesList.OK;
        }

        //public static Int32 m_Counter = 0;
        protected BaseRoutineDescriptor m_CurrentPerfromedRoutine;

        protected bool EnterCriticalSection(Object inSynchronizationObject, ref Int32 LockCounter)
        {
            if (!System.Threading.Monitor.TryEnter(m_ObjRoutineSynchronization, 4000))
            {
                Object tmp = m_ListOfHoldingThreads.Peek();
                if (m_SystemLog.IsErrorEnabled)
                    m_SystemLog.Error("Failed to enter critical section - Routine controller");
                return false;
            }
            Interlocked.Increment(ref m_RoutineSelectionCounter);
            m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);
            return true;

            //if (Monitor.TryEnter(inSynchronizationObject, 4000))
            //{
            //    Interlocked.Increment(ref LockCounter);
            //    //if (Counter > 40)
            //    //{ Int32 I = 0; }
            //    return true;
            //}
            //else
            //    return false;
        }

        protected void ExitCriticalSection(Object inSynchronizationObject, ref  Int32 LockCounter)
        {
            try
            {
                while (m_ListOfHoldingThreads.Count > 0)
                {
                    Object tmp = m_ListOfHoldingThreads.Peek();
                    if (Convert.ToInt32(tmp) != Thread.CurrentThread.ManagedThreadId)
                        return;
                    Interlocked.Decrement(ref m_RoutineSelectionCounter);
                    m_ListOfHoldingThreads.Dequeue();
                    if (m_ListOfHoldingThreads.Count == 0)
                    {
                        System.Threading.Monitor.Exit(m_ObjRoutineSynchronization);
                        break;
                    }
                    else
                        System.Threading.Monitor.Exit(m_ObjRoutineSynchronization);
                }

                //if (LockCounter > 0)
                //{
                //    while (Interlocked.Decrement(ref LockCounter) >= 0)
                //    //while (m_AxisSelectionCounter-- > 0)
                //    {
                //        //Console.WriteLine("Deactivate Axis thread - " + System.Threading.Thread.CurrentThread.Name);
                //        if (LockCounter == 0)
                //        {
                //            System.Threading.Monitor.Exit(SynchronizationObject);
                //            return;
                //        }
                //        else
                //        {
                //            System.Threading.Monitor.Exit(SynchronizationObject);
                //        }
                //    }
                //}
                //while (m_Counter > 0)
                //{
                //    Monitor.Exit(SynchronizationObject);
                //    m_Counter--;
                //}
                //while (true)
                //{
                //    if (LockCounter > 0)
                //        Interlocked.Decrement(ref LockCounter);
                //    Monitor.Exit(inSynchronizationObject);
                //}
            }
            catch (SynchronizationLockException)
            {
            }
            //finally
            //{
            //    m_Counter = 0;
            //}
        }

        /// <summary>
        /// Routine performance thread Main function loop
        /// </summary>
        protected void RoutinePerformance(Object cancel_token)
        {
            try
            {
                while (true)
                {
                    try
                    {
                        lock (m_PerformanceRoutinesQueue)
                        {
                            foreach (BaseRoutineDescriptor tmpRoutine in m_PerformanceRoutinesQueue) //.Values)
                            {
                                if (EnterCriticalSection(tmpRoutine.SynchronizationObject, ref  tmpRoutine.LockCounter))
                                {
                                    try
                                    {
                                        if (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                                        {
                                            if ((tmpRoutine.RoutineStepInProgress) ||
                                                (tmpRoutine.StepColl.Count == 0))
                                            {
                                                /* The routine is currently performed by the system. Continue to next routine.*/
                                                ExitCriticalSection(tmpRoutine.SynchronizationObject, ref tmpRoutine.LockCounter);
                                                continue;
                                            }

                                            if (tmpRoutine.IncreeseStepIndex)
                                                tmpRoutine.SetStepIndex(tmpRoutine.SelectedStepIndex + 1, 1, true);
                                            else
                                                tmpRoutine.IncreeseStepIndex = true;

                                            if (tmpRoutine.SelectedStepIndex >= 0)
                                            {
                                                tmpRoutine.RoutineStepInProgress = true;
                                            }
                                            else
                                            {
                                                tmpRoutine.RoutineStepInProgress = false;
                                                continue;
                                            }

                                            if (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                                            {
                                                tmpRoutine.SelectedNode.PerformStep(tmpRoutine);

                                                m_CurrentPerfromedRoutine = tmpRoutine;
                                            }
                                        }
                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        ExitCriticalSection(tmpRoutine.SynchronizationObject, ref tmpRoutine.LockCounter);
                                    }
                                }
                                else
                                {
                                    Thread.Sleep(50);
                                }
                                if (((CancellationToken)cancel_token).IsCancellationRequested)
                                    return;
                                //if (true == m_StopRoutineThreadRequested)
                                //{
                                //    m_SemStopPerformaceQue.Release();
                                //    return;
                                //}
                            }
                            //Wait till next loop
                            Thread.Sleep(m_SleepIntervalBetweenRuns);
                            if (((CancellationToken)cancel_token).IsCancellationRequested)
                                return;
                            //if (true == m_StopRoutineThreadRequested)
                            //{
                            //    m_SemStopPerformaceQue.Release();
                            //    return;
                            //}
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (ThreadAbortException exc)
            {
                Console.WriteLine("Routine Controller t aborting, code is " + exc.ExceptionState);
            }
        }

        /// <summary>
        /// Remove first(single) not active routine from performance Queue
        /// </summary>
        /// <returns>If found not active routine it removed and returns true. otherwise return false</returns>
        protected Boolean RemoveFirstInactiveRoutine()
        {
            foreach (BaseRoutineDescriptor tmpRoutine in m_PerformanceRoutinesQueue) //.Values)
            {
                if ((tmpRoutine.RoutineState == _ROUTINE_STATE.RS_COMPLETED) ||
                    (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_NOT_SET) ||
                    (tmpRoutine.RoutineState == _ROUTINE_STATE.RS_READY_TO_START))
                {
                    m_PerformanceRoutinesQueue.Remove(tmpRoutine.RoutineIndex);
                    return true;
                }
            }
            return false;
        }

        protected void m_PerformanceRoutinesQueue_BeforeRoutineRemove(object Sender, RoutineEventArguments args)
        {
            OnBeforeRoutineRemove(this, args);
            String error_message = "";
            args.RoutineDescriptor.RemoveActionsDevices(out error_message);
        }

        /// <summary>
        /// Wait while step complete
        /// </summary>
        /// <param name="Routine"></param>
        /// <returns></returns>
        protected short WaitStepComplete(BaseRoutineDescriptor Routine)
        {
            while (true)
            {
                if (EnterCriticalSection(Routine.SynchronizationObject, ref Routine.LockCounter))
                {
                    try
                    {
                        if (!Routine.RoutineStepInProgress)
                        {
                            ExitCriticalSection(Routine.SynchronizationObject, ref  Routine.LockCounter);
                            break;
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        ExitCriticalSection(Routine.SynchronizationObject, ref Routine.LockCounter);
                    }
                    Thread.Sleep(10);
                    continue;
                }
                else
                    return ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
            }
            return ErrorCodesList.OK;
        }

        #endregion protected functions

        #region Routines Events

        /// <summary>
        /// Occurs when all steps in routine is finished.
        /// Removes the routine form performance queue
        /// </summary>
        /// <param name="Sender">The routine handle</param>
        /// <param name="args"></param>
        public void RoutineObj_RoutineCompleted(object Sender, EventArgs args)
        {
            Console.WriteLine("Routine Completed - " + ((BaseRoutineDescriptor)Sender).RoutineName);
            OnRoutineFinished(this, new RoutineEventArguments((BaseRoutineDescriptor)Sender));
        }

        /// <summary>
        /// Occurs when there is error during routine performance.
        /// </summary>
        /// <param name="Sender">The routine handle</param>
        /// <param name="args"></param>
        public void RoutineObj_PerformanceErrorOccuerd(object Sender, EventArgs args)
        {
            m_ErrorCode = ((BaseRoutineDescriptor)Sender).RoutineErrorCode;
            m_ErrorMessage = ((BaseRoutineDescriptor)Sender).RoutineErrorMessage;
            OnPerformanceErrorOccuerd(this, new RoutineEventArguments(((BaseRoutineDescriptor)Sender)));
        }

       

        /// <summary>
        /// Indicate to system that current routine is on "I'm waiting" state
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="args"></param>
        public void RoutineObj_ImWaiting(object Sender, EventArgs args)
        {
            OnImWaiting((BaseRoutineDescriptor)Sender);
        }

        public void RoutineObj_IPEvent(object Sender, EventArgs args)
        {
            OnIPEvent(this);
        }

        public void RoutineObj_NoPointsListAvailible(object Sender, EventArgs args)
        {
            OnNoPointsListAvailible(Sender);
        }

        public void RoutineObj_RoutineStepChanged(object Sender, BaseStepDescriptor Step)
        {
            OnRoutineStepChanged(Sender, Step);
        }

        public void RoutineObj_ChangeRoutineState(object Sender, _ROUTINE_STATE arg)
        {
            OnChangeRoutineState(Sender, arg);
        }

        public void RoutineObj_CaptureImage(object Sender, int stent_index)
        {
            OnCaptureImageEvent(Sender, stent_index);
        }

        public void RoutineObj_ClearPointsList(object Sender)
        {
            OnClearPointsList(Sender);
        }

        public void RoutineObj_DisplayImageAndWait4Confirmation(object sender, int stent_index)
        {
            OnDisplayImageAndWait4Confirmation(sender, stent_index);
        }

        public void RoutineObj_ComputePointsForStent(object Sender, int stent_index, int num_of_models_for_over_lap, float min_over_lap_in_MM)
        {
            OnComputePointsForStentEvent(Sender, stent_index, num_of_models_for_over_lap, min_over_lap_in_MM);
        }

        public void RoutineObj_GoToPrintPosition(object Sender, int stent_index, int jet_index)
        {
            OnGoToPrintPositionEvent(Sender, stent_index, jet_index);
        }


        #endregion Routines Events

        #region IComponent Members

        public string Name
        {
            get { return "Routines Controller"; }
        }

        #endregion IComponent Members

        #region IInputDevice Members
        
            public event DataEventHandler DataReceived = null;
        
        #endregion IInputDevice Members

        #region IOutputDevice Members

        public void Send(string data)
        {
            throw new NotImplementedException();
        }

        #endregion IOutputDevice Members

       
    }
} 