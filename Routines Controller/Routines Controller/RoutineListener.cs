﻿using SFW;
using System;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using NS_Routines_Controller_Common;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Interfaces;


namespace NS_Routines_Controller
{
    /// <summary>
    /// Base Routine Listener class. Can be overridden for custom use. 
    /// Please do not change the base class
    /// </summary> 
    public class RoutineListener : Listener, IRoutineControllerListener
    {
        public RoutineListener(NS_RoutinesController.RoutinesController device)
            : base(device)
        {
            m_Name = "Routines Controller Listener";

            device.BeforeRoutineRemove += new NS_RoutinesController.RoutinesController.BeforeRoutineRemoveHandler(device_BeforeRoutineRemove);
            device.ChangeRoutineState += new NS_RoutinesController.RoutinesController.ChangeRoutineStateHandler(device_ChangeRoutineState);

            device.DisplayImageAndWait4Confirmation += new NS_RoutinesController.RoutinesController.DisplayImageAndWait4ConfirmationHandler(device_DisplayImageAndWait4Confirmation);
            device.CaptureImageEvent += new NS_RoutinesController.RoutinesController.CaptureImageEventHandler(device_CaptureImageEvent);
            device.ClearPointsList += new NS_RoutinesController.RoutinesController.ClearPointsListHandler(device_ClearPointsList);
            device.ComputePointsForStentEvent += new NS_RoutinesController.RoutinesController.ComputePointsForStentEventHandler(device_ComputePointsForStentEvent);
            device.GoToPrintPositionEvent += new NS_RoutinesController.RoutinesController.GoToPrintPositionEventHandler(device_GoToPrintPositionEvent);

            device.ImWaiting += new NS_RoutinesController.RoutinesController.ImWaitingHandler(device_ImWaiting);
            device.IPEvent += new NS_RoutinesController.RoutinesController.IPEventHandler(device_IPEvent);
            device.NoPointsListAvailible += new NS_RoutinesController.RoutinesController.NoPointsListAvailibleHandler(device_NoPointsListAvailible);

            device.PerformanceErrorOccuerd += new NS_RoutinesController.RoutinesController.PerformanceErrorOccuerdHandler(device_PerformanceErrorOccuerd);

            device.RoutineFinished += new NS_RoutinesController.RoutinesController.RoutineFinishedHandler(device_RoutineFinished);
            device.RoutineStepChanged += new NS_RoutinesController.RoutinesController.RoutineStepChangedHandler(device_RoutineStepChanged);

          
            device.NewRoutineAddedToPerformanceQueue += new NS_RoutinesController.RoutinesController.NewRoutineAddedToPerformanceQueueHandler(device_NewRoutineAddedToPerformanceQueue);

            device.RoutineForEditingLoaded += new NS_RoutinesController.RoutinesController.RoutineForEditingLoadedHandler(device_RoutineForEditingLoaded);
            device.RoutineFileSaved += new NS_RoutinesController.RoutinesController.RoutineFileSavedHandler(device_RoutineFileSaved);

            device.NewMessage += new NS_RoutinesController.RoutinesController.NewMessageHandler(device_NewMessage);
        }

        private void device_NewMessage(object Sender, RCDataPrmNewMessage message)
        {
            SendUpdateEvent(new RCDataEvent(RCDataEventType.ET_NEW_UPDATE_MESSAGE_RECEIVED, message));
        }

        private void device_RoutineForEditingLoaded(object Sender, RoutineEventArguments args, string routine_file_name)
        {
            SendUpdateEvent(new RCDataEvent(RCDataEventType.ET_ROUTINE_FOR_EDITING_LOADED, new RCDataPrmNewRoutineForEditingLoaded(args.RoutineDescriptor, routine_file_name)));
        }

        private void device_RoutineFileSaved(object Sender, RoutineEventArguments args, string routine_file_name)
        {
            SendUpdateEvent(new RCDataEvent(RCDataEventType.ET_ROUTINE_FILE_NAME_CHANGED, new RCDataPrmRoutineFileNameChanged(routine_file_name)));
        }

        private void device_NewRoutineAddedToPerformanceQueue(object Sender, RoutineEventArguments args)
        {
            SendUpdateEvent(new RCDataEvent(RCDataEventType.ET_NEW_ROUTINE_LOADED_TO_PERFORMANCE_QUEUE, new RCDataPrmNewRoutineLoaded(Sender, args.RoutineDescriptor)));
        }

        private void device_RoutineStepChanged(object Sender, BaseStepDescriptor Step)
        {
            //throw new System.NotImplementedException();
        }

        private void device_RoutineFinished(object Sender, RoutineEventArguments args)
        {
            SendUpdateEvent(new RCDataEvent(RCDataEventType.ET_ROUTINE_FINISHED, new RCDataPrmRoutineStateChanged(args.RoutineDescriptor)));
        }

        private void device_PerformanceErrorOccuerd(object Sender, RoutineEventArguments args)
        {
            //throw new System.NotImplementedException();
        }

        private void device_NoPointsListAvailible(object Sender, System.EventArgs args)
        {
            //throw new System.NotImplementedException();
        }

        private void device_IPEvent(object Sender, System.EventArgs args)
        {
            //throw new System.NotImplementedException();
        }

        private void device_ImWaiting(object Sender, RoutineEventArguments args)
        {
            //throw new System.NotImplementedException();
        }

        private void device_GoToPrintPositionEvent(object Sender, int stent_index, int jet_index)
        {
            //throw new System.NotImplementedException();
        }

        private void device_DisplayImageAndWait4Confirmation(object sender, int stent_index)
        {
            //throw new System.NotImplementedException();
        }

        private void device_ComputePointsForStentEvent(object Sender, int stent_index, int num_of_models_for_over_lap, float min_over_lap_in_MM)
        {
            //throw new System.NotImplementedException();
        }

        private void device_ClearPointsList(object Sender)
        {
            //throw new System.NotImplementedException();
        }

        private void device_ChangeRoutineState(object Sender, int RoutineIndex, _ROUTINE_STATE arg)
        {
            //throw new System.NotImplementedException();
        }

        private void device_CaptureImageEvent(object Sender, int stent_index)
        {
            //throw new System.NotImplementedException();
        }

        private void device_BeforeRoutineRemove(object Sender, RoutineEventArguments args)
        {
            //throw new System.NotImplementedException();
        }

        private void SendUpdateEvent(RCDataEvent Event)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue(Event);

                // m_Log.Info("Sending " + m_LastMeasure);

                RaiseEventGroupReady();
            });
        }

        protected override void OnInputDeviceDataReceived(object sender, EventArgs<object> e)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue((IEventData)e.Value);

                // m_Log.Info("Sending " + m_LastMeasure);

                RaiseEventGroupReady();
            });
        }
    }

}
