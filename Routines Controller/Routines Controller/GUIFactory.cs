﻿using System;
using System.Collections.Generic; 
using SFW;
using System.Windows.Controls;
using System.Windows;
using NS_Common.ApplicationExtensions;
using System.ComponentModel;

namespace NS_RoutinesController
{
    public class GUIFactory : RuntimeObjectsFactory
    {
        public GUIFactory(Dictionary<string, SFW.IComponent> components)
            : base(components)
        {   
        }

        public override SFW.IComponent CreateItem(string type)
        {
            return CreateItem(type, null);
        }

        public SFW.IComponent CreateItem(string type, Page OwnerWindow)
        {
            SFW.IComponent res = null;
            if (m_Components.ContainsKey(type))
                res = m_Components[type];
            else
            {
                switch (type)
                {
                    #region User Controls Pages

                        #region Project Parameters Page

                        case "ProjectParametersPage":
                            {
                                m_Components.Add("ProjectParametersPage", new ProjectParametersPage(Application.Current.GetContainer()));
                                //((Page)m_Components["ProjectParametersPage"]).Unloaded += GUIFactory_Unloaded;
                                res = m_Components[type];
                                break;
                            }

                        #endregion Project Parameters Page

                        #region Product Parameters Page

                        case "ProductParametersPage":
                            {
                                m_Components.Add("ProductParametersPage", new ProductParametersPage(Application.Current.GetContainer()));
                                //((Page)m_Components["ProductParametersPage"]).Unloaded += GUIFactory_Unloaded;
                                res = m_Components[type];
                                break;
                            }

                        #endregion Product Parameters Page
                        
                        #region C And F Parameters Page

                        case "C_AND_F_ParametersPage":
                            {
                                m_Components.Add("C_AND_F_ParametersPage", new C_AND_F_ParametersPage(Application.Current.GetContainer()));
                                //((Page)m_Components["C_AND_F_ParametersPage"]).Unloaded += GUIFactory_Unloaded;
                                res = m_Components[type];
                                break;
                            }
                        #endregion C ANd F Parameters Page

                        #region Test Type Selection Page

                        case "TestTypeSelectionPage":
                            {
                                m_Components.Add("TestTypeSelectionPage", new TestTypeSelectionPage(Application.Current.GetContainer()));
                                //((Page)m_Components["TestTypeSelectionPage"]).Unloaded += GUIFactory_Unloaded;
                                res = m_Components[type];
                                break;
                            }

                        #endregion Test Type Selection Page

                    #endregion

                    #region Project Parameters Page

                        //case "BaseDBPage":
                        //    {
                        //        res = new BaseDBPage(Application.Current.GetContainer());
                        //        //res = m_Components[type];
                        //        break;
                        //    }

                    #endregion Project Parameters Page

                    default:
                        {
                            res = null;
                            break;
                        }
                }
            }
            return res;
        }

        void GUIFactory_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {  
            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }

        public override void ReleaseItem(string type)
        {
            switch (type)
            {
                case "frmSystemLog":
                    {
                        m_Components.Remove("frmSystemLog");
                        break;
                    }
            }
        }


        private void GUIFactory_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.Cancel == true) return;

            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }


        private void CreateChildWindows(SFW.IComponent component)
        {
            System.Reflection.MethodInfo mi = component.GetType().GetMethod("CreateChildWindows");
            if (mi != null)
                mi.Invoke(component, new object[] { this });
        }
    }
}