﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text; 
using System.Xml;
using System.Xml.Serialization;
using NS_Common; 
using SFW;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters; 
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using NS_Common.Service; 
using NS_Routines_Controller_Common.Routine_Definitions.Routines;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System.Windows;
using MVVM_Dialogs.Service.FrameworkDialogs.OpenFile;
using NS_Routines_Controller_Common.Routine_Definitions.Steps;
using NS_Routines_Controller_Common.Interfaces;
using NS_RoutinesController;


namespace NS_Routines_Controller
{
    /// <summary>
    /// Base Routine Logic class. Can be overridden for custom use. 
    /// Please do not change the base class
    /// </summary>
    public class RoutineLogic : Logic,  IRoutineControllerLogic
    {
        #region Protected Members

        protected NS_RoutinesController.RoutinesController m_RoutineCntrl;

        /// <summary>
        /// An event raised when the group of events which holds the data received from the input device are ready to be sent.
        /// </summary>
        public new event EventGroupHandler EventGroupReady;

        /// <summary>
        /// A group of events which holds the data received from the input device
        /// </summary>
        protected EventGroupData m_EventGroupData;

        protected Type[] m_PosibleTypesArray = null;

        #endregion protected Members

        #region Public Properties

        protected _ActionType[] m_PosibleActionsTypesArray = null;
        public _ActionType[] PosibleActionsTypesArray
        {
            get { return m_PosibleActionsTypesArray; }
            protected set { m_PosibleActionsTypesArray = value; }
        }

        private Int32 m_SubRoutinePerformanceCounter = 0;
        public Int32 SubRoutinePerformanceCounter
        {
            get { return m_SubRoutinePerformanceCounter; }
            set { m_SubRoutinePerformanceCounter = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RoutineLogic(RoutinesController routine_contrl)
        {
            Routine routine = new  Routine();


            m_RoutineCntrl = routine_contrl;
            m_PosibleTypesArray = GetRoutinesPosibleTypes();
            m_PosibleActionsTypesArray = GetActionsPosibleTypes();

            //  MapHandlers(); 
        }

        #endregion Constructors

        #region Public Methods

        #region Save\Load Routine Functions

        /// <summary>
        /// Load the routine from XML file
        /// </summary>
        /// <param name="routine">Out parameter. The list of routine object</param>
        /// <param name="FileName">The XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public virtual int LoadRoutineXmlFile(String file_name, out BaseRoutineDescriptor routine)
        {
            routine = null;
            TextReader Reader = null;
            try
            {


                if (!File.Exists(file_name))
                {
                    return ErrorCodesList.FILE_NOT_FOUND;
                }

                //Load data from XML Axis File
                BaseRoutineDescriptor MyList;
                Reader = new StreamReader(file_name);

                //Serialization
                XmlSerializer Serializer = new XmlSerializer(typeof(BaseRoutineDescriptor), m_PosibleTypesArray);            //DeSerialization
                MyList = (BaseRoutineDescriptor)Serializer.Deserialize(Reader);

                routine = MyList;

                return ErrorCodesList.OK;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error during open file");
                return ErrorCodesList.FAILED;
            }
            finally
            {
                if (null != Reader)
                    Reader.Close();
            }
        }

        /// <summary>
        /// Load the routine from XML file
        /// </summary>
        /// <param name="routine">Out parameter. The list of routine object </param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public virtual int LoadRoutineXmlFile(out BaseRoutineDescriptor routine)
        {
            routine = null;
            String routine_file_name = "";

            return LoadRoutineXmlFile(out routine, out routine_file_name);
        }

        /// <summary>
        /// Load the routine from XML file
        /// </summary>
        /// <param name="routine">Out parameter. The list of routine object </param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public virtual int LoadRoutineXmlFile(out BaseRoutineDescriptor routine, out String routine_file_name)
        {
           // routine = null;

            return m_RoutineCntrl.LoadRoutineXmlFile(out routine_file_name, out routine);


            //if (m_RoutineCntrl.UseDefaultFileManagementDialog)
            //{
            //    OpenFileDialog dlg = new OpenFileDialog();
            //    dlg.InitialDirectory = m_RoutineCntrl.RoutinesFilesPath;
            //    dlg.Filter = "Routine Files (*." + m_RoutineCntrl.RoutinesFilePattern + " )|*." + m_RoutineCntrl.RoutinesFilePattern + "|All files (*.*)|*.*";
            //    dlg.Multiselect = false;
            //    if (dlg.ShowDialog(null) == System.Windows.Forms.DialogResult.OK)
            //    {
            //        //Open file data
            //        routine_file_name = dlg.FileName; //System.IO.Path.Combine(m_RoutineCntrl.RoutinesFilesPath, dlg.SafeFileName);
            //        return this.LoadRoutineXmlFile(routine_file_name, out routine);
            //    }
            //}
            //else
            //{
            //    //Show load data dialog
            //    NS_RoutinesController.UI_Forms.Service.frmLoadDialog frmLoad = new NS_RoutinesController.UI_Forms.Service.frmLoadDialog();
            //    frmLoad.Init(m_RoutineCntrl.RoutinesFilesPath, m_RoutineCntrl.RoutinesFilePattern, "");
            //    if (frmLoad.ShowDialog() == DialogResult.OK)
            //    {
            //        //Open file data
            //        routine_file_name = System.IO.Path.Combine(m_RoutineCntrl.RoutinesFilesPath, frmLoad.SelectedFileName);
            //        return this.LoadRoutineXmlFile(System.IO.Path.Combine(m_RoutineCntrl.RoutinesFilesPath, frmLoad.SelectedFileName), out routine);
            //    }
            //}
            //routine_file_name = "";
            //return ErrorCodesList.CANCELED;
        }

        protected virtual void SaveRoutineAs(RCCommandPrmSaveRoutine rCCommandPrmSaveRoutine)
        {
            if (m_RoutineCntrl.UseDefaultFileManagementDialog)
            {
                //SaveFileDialog dlg = new SaveFileDialog();
                //dlg.InitialDirectory = m_RoutineCntrl.RoutinesFilesPath;
                //dlg.Filter = "Routine Files (*." + m_RoutineCntrl.RoutinesFilePattern + " )|*." + m_RoutineCntrl.RoutinesFilePattern + "|All files (*.*)|*.*";
                //if (dlg.ShowDialog() == DialogResult.OK)
                //{
                //    String file_name = Path.Combine(Path.GetDirectoryName(dlg.FileName), Path.GetFileNameWithoutExtension(dlg.FileName) + "." + m_RoutineCntrl.RoutinesFilePattern);
                    //Save file
                String file_name;
                m_RoutineCntrl.SaveRoutineXmlFile(((BaseRoutineDescriptor)rCCommandPrmSaveRoutine.Routine), out file_name, m_PosibleTypesArray);
               // }
            }
            else
            {
                m_RoutineCntrl.SaveRoutineXmlFile(((BaseRoutineDescriptor)rCCommandPrmSaveRoutine.Routine), m_PosibleTypesArray);
            }
        }

        protected virtual void SaveRoutine(RCCommandPrmSaveRoutine rCCommandPrmSaveRoutine)
        {

            m_RoutineCntrl.SaveRoutineXmlFile(rCCommandPrmSaveRoutine.RoutineFileName,
                                        ((BaseRoutineDescriptor)rCCommandPrmSaveRoutine.Routine),
                                        m_PosibleTypesArray);
        }

        /// <summary>
        /// Save Axis list to XML data file
        /// </summary>
        /// <param name="AxisList">The list of axis descriptors</param>
        /// <param name="FileName">XML file name</param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from MotionController.ErrorCodesList
        /// </returns>
        public virtual int SaveRoutineXmlFile(String FileName, BaseRoutineDescriptor Routine2Serialize)
        {
            string filePath = Path.Combine(new String[] { m_RoutineCntrl.RoutinesFilesPath, Path.GetFileNameWithoutExtension(FileName) }) + "." + m_RoutineCntrl.RoutinesFilePattern;
            try
            {
                //Serialization
                XmlSerializer Serializer = new XmlSerializer(typeof(BaseRoutineDescriptor), m_PosibleTypesArray);
                TextWriter Writer = new StreamWriter(filePath);
                Serializer.Serialize(Writer, Routine2Serialize);
                Writer.Close();
            }
            catch
            {
                return ErrorCodesList.FAILED;
            }

            return ErrorCodesList.OK;
        }

        public virtual string SerializeObj(Object pObject)
        {
            try
            {
                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(pObject.GetType(), m_PosibleTypesArray);
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

                xs.Serialize(xmlTextWriter, pObject);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());

                string tmpstr = XmlizedString.Substring(XmlizedString.IndexOf("<Action"));
                int iStartActionName = XmlizedString.IndexOf("<Action_Name>") + 13;
                int iEndActionName = XmlizedString.IndexOf("</Action_Name>");
                String actionName = "<!--" + XmlizedString.Substring(iStartActionName, iEndActionName - iStartActionName) + "-->";

                tmpstr = actionName + "<Step xsi:type=\"Step\">" + tmpstr;

                return tmpstr;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return null;
            }
        }

        #endregion Save\Load Routine Functions

        #endregion Public Methods

        protected override void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(RCCommandEvent), HandleRoutinesControllerCommandEventData);
            //m_Handlers.TryAdd(typeof(SpecialCommandEvent), HandleSpecialCommandEventData);
        }

        protected override void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
                m_Handlers.TryRemove(typeof(RCCommandEvent), out res);
        }

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        protected virtual String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        protected virtual Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        protected virtual void HandleRoutinesControllerCommandEventData(IEventData data)
        {
            if (data.GetType() != typeof(RCCommandEvent))
            {
                throw new ArgumentException("value must be of type RCCommandEvent.", "value");
            }

            switch (((RCCommandEvent)data).CmdType)
            {
                case RCCommandType.CT_START_ROUTINES_PERFORMANCE_THREAD:
                    {
                        m_RoutineCntrl.StartRoutinePerformance();
                        break;
                    }
                case RCCommandType.CT_STOP_ROUTINES_PERFORMANCE_THREAD:
                    {
                        m_RoutineCntrl.StopRoutinePerformance();
                        break;
                    }
                case RCCommandType.CT_LOAD_ROUTINE:
                    {
                        LoadRoutine(((RCCommandPrmLoadRoutine)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_LOAD_ROUTINE_FOR_EDITING:
                    {
                        LoadRoutineForEditing(((RCCommandPrmLoadRoutineForEditing)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_UNLOAD_ROUTINE:
                    {
                        UnLoadRoutine(((RCCommandPrmUnLoadRoutine)((RCCommandEvent)data).CommandParameters));
                        break;
                    }

                case RCCommandType.CT_STOP_ROUTINE:
                    {
                        StopRoutine(((RCCommandPrmStopRoutine)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_LOAD_ROUTINE_BY_NAME:
                    {
                        LoadRoutineByName(((RCCommandPrmLoadRoutineByName)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_GET_ACTION_XML_TEXT:
                    {
                        GetActionXML(((RCCommandPrmGetActionXML)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_SAVE_ROUTINE:
                    {
                        SaveRoutine(((RCCommandPrmSaveRoutine)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_SAVE_ROUTINE_AS:
                    {
                        SaveRoutineAs(((RCCommandPrmSaveRoutine)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_CREATE_NEW_ROUTINE_FOR_EDITING:
                    {
                        CreateRoutineForEditing(((RCCommandPrmCreateRoutineForEditing)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_SETTINGS_UPDATE:
                    {
                        UpdateSettingsParameters(((RCCommandPrmUpdateSettings)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.CT_ADD_ROUTINE_TO_QUEUE:
                    {
                        AddRoutineToPerformanceQueue(((RCCommandPrmAddRoutineToQueue)((RCCommandEvent)data).CommandParameters));
                        break; 
                    }
                case RCCommandType.CT_START_ROUTINE_BY_INDEX:
                    {
                        StartRoutineByIndex(((RCCommandPrmStartRoutineByIndex)((RCCommandEvent)data).CommandParameters));
                        break;
                    }
                case RCCommandType.ET_SEND_UPDATE_MESSAGE:
                    {
                        m_RoutineCntrl.OnNewMessage(this, new RCDataPrmNewMessage((((RCCommandPrmSendMessage)((RCCommandEvent)data).CommandParameters)).Message ));
                        break;
                    }
            }
        }

        private void AddRoutineToPerformanceQueue(RCCommandPrmAddRoutineToQueue rCCommandPrmAddRoutineToQueue)
        {
            m_RoutineCntrl.AddRoutine2PerformanceQueue(rCCommandPrmAddRoutineToQueue.UserRoutine, rCCommandPrmAddRoutineToQueue.OwnerForm ); 
        }

        private void StartRoutineByIndex(RCCommandPrmStartRoutineByIndex rCCommandPrmStartRoutineByIndex)
        {
            m_RoutineCntrl.PerformRoutine(rCCommandPrmStartRoutineByIndex.RoutineIndex);
        }


        private void UpdateSettingsParameters(RCCommandPrmUpdateSettings rCCommandPrmUpdateSettings)
        {
            m_SubRoutinePerformanceCounter = rCCommandPrmUpdateSettings.SubRoutinePerformanceCounter;
        }



        protected virtual void GetActionXML(RCCommandPrmGetActionXML rcCommandPrmGetActionXML)
        {
            Step step = new Step();
            BaseActionDescriptor action = null;

            try
            {
                action = (BaseActionDescriptor)Activator.CreateInstance(rcCommandPrmGetActionXML.ActionType.ActionType);
            }
            catch
            {
            }


            if (null == action) return;
            step.StepName = action.ActionName;
            step.AddAction(action);

            string res = SerializeObj(step);
            RCDataEvent Event;
            if (null != res)
            {
                Event = new RCDataEvent(RCDataEventType.ET_SEND_ACTION_XML_STRING,
                                                                new RCDataPrmActionXML(res));
            }
            else
            {
                Event = new RCDataEvent(RCDataEventType.ET_SEND_ACTION_XML_STRING,
                                                                new RCDataPrmActionXML(""));
            }

            SendUpdateEvent(Event, true);
        }


        protected virtual void LoadRoutineByName(RCCommandPrmLoadRoutineByName rCCommandPrmLoadRoutineByName)
        {
            BaseRoutineDescriptor m_Routine;
            Int32 m_RoutineIndex;
            if (LoadRoutineXmlFile(rCCommandPrmLoadRoutineByName.RoutineName, out m_Routine) != ErrorCodesList.OK)
            {
                return;
            }
            m_RoutineIndex = m_RoutineCntrl.AddRoutine2PerformanceQueue(m_Routine, false, true, true);
        }

        protected virtual void StopRoutine(RCCommandPrmStopRoutine rCCommandPrmStopRoutine)
        {
            m_RoutineCntrl.AbortRoutine(rCCommandPrmStopRoutine.RoutineIndex);
        }

        protected virtual void LoadRoutine(RCCommandPrmLoadRoutine rCCommandEventParameters)
        {
            BaseRoutineDescriptor m_Routine;
            Int32 m_RoutineIndex;
            if (LoadRoutineXmlFile(out m_Routine) != ErrorCodesList.OK)
            {
                return;
            }
            m_RoutineIndex = m_RoutineCntrl.AddRoutine2PerformanceQueue(m_Routine, false, true, true);
        }

        protected virtual void UnLoadRoutine(RCCommandPrmUnLoadRoutine rCCommandPrmUnLoadRoutine)
        {
            m_RoutineCntrl.RemoveRoutineFormPerformanceQueue(rCCommandPrmUnLoadRoutine.RoutineIndex);
        }

        protected virtual void LoadRoutineForEditing(RCCommandPrmLoadRoutineForEditing rCCommandPrmLoadRoutineForEditing)
        {
            BaseRoutineDescriptor m_Routine;
            String routine_file_name = "";
            if (LoadRoutineXmlFile(out m_Routine, out routine_file_name) != ErrorCodesList.OK)
            {
                return;
            }
            m_RoutineCntrl.UpdateRoutineForEditing(m_Routine, routine_file_name);
            //m_RoutineIndex = m_RoutineCntrl.AddRoutine2PerformanceQueue(m_Routine, false, true, true);
        }

        private void CreateRoutineForEditing(RCCommandPrmCreateRoutineForEditing rCCommandPrmCreateRoutineForEditing)
        {
            BaseRoutineDescriptor m_Routine = new Routine();
            String routine_file_name = "Unknown";

            m_RoutineCntrl.UpdateRoutineForEditing(m_Routine, routine_file_name);
        }


        protected virtual void SendUpdateEvent(RCDataEvent Event, Boolean isSync = false)
        {
            if (isSync)
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue(Event);

                if (EventGroupReady != null)
                    EventGroupReady(this, m_EventGroupData);
                // m_Log.Info("Sending " + m_LastMeasure);

                //RaiseEventGroupReady();
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    m_EventGroupData = new EventGroupData();

                    m_EventGroupData.Enqueue(Event);

                    if (EventGroupReady != null)
                        EventGroupReady(this, m_EventGroupData);
                });
            }
        }


        protected virtual Type[] GetRoutinesPosibleTypes()
        {
            List<Type[]> types = new List<Type[]>();
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetCallingAssembly(), typeof(BaseRoutineDescriptor), true));
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetCallingAssembly(), typeof(BaseStepDescriptor), true));
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetCallingAssembly(), typeof(BaseActionDescriptor), true));

            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseRoutineDescriptor)), typeof(BaseRoutineDescriptor), true));
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseStepDescriptor)), typeof(BaseStepDescriptor), true));
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseActionDescriptor)), typeof(BaseActionDescriptor), true));



            return Tools.Reflection.ReflectionTools.Concat(types);
        }

        protected virtual _ActionType[] GetActionsPosibleTypes()
        {
            List<Type[]> types = new List<Type[]>();
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetCallingAssembly(), typeof(BaseActionDescriptor), true));
            types.Add(Tools.Reflection.ReflectionTools.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseActionDescriptor)), typeof(BaseActionDescriptor), true));
            Type[] tmp = Tools.Reflection.ReflectionTools.Concat(types);
            _ActionType[] res = new _ActionType[tmp.Length];

            for (Int16 i = 0; i < tmp.Length; i++)
            {
                if (tmp[i] != typeof(BaseActionDescriptor))
                {
                    BaseActionDescriptor tmp_object = (BaseActionDescriptor)Tools.Reflection.ReflectionTools.GetNewObject(tmp[i]);
                    res[i] = new _ActionType(tmp[i], tmp_object.GetShortDescription(), tmp_object.GetFullDescription());
                }
                else
                    res[i] = new _ActionType(tmp[i]);

            }

            return res;

        }


    }
}
