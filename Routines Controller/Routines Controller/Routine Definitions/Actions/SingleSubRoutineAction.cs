﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using SFW;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;

namespace NS_RoutinesController.Routine_Definitions.Actions
{
    public class SingleSubRoutineAction : BaseActionDescriptor
    {
        #region Private members

            NS_RoutinesController.RoutinesController RoutineCntrl;
     
        #endregion Private members

        #region Constructors

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public SingleSubRoutineAction() :
            base()
        { }

        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inActionName">The Action name</param>
        /// <param name="RoutineIndex"> Routine Index </param>
        public SingleSubRoutineAction(String inActionName, Int32 inSubRoutineIndex)
            : this()
        {
            m_ActionName = inActionName;
            m_SubRoutineIndex = inSubRoutineIndex;
        }

        #endregion Constructors

        #region Public properties

        [XmlIgnore]
        private Int32 m_SubRoutineIndex;
        /// <summary>
        /// Get/Set the routine Index to run
        /// </summary>
        [XmlElement("SubRoutine_Index")]
        public Int32 SubRoutineIndex
        {
            get { return m_SubRoutineIndex; }
            set { m_SubRoutineIndex = value; }
        }

        #endregion Public properties

        #region Public functions

        #endregion Public functions

        #region Public overridden functions

        public override short SetMyDevices(Dictionary<string, IComponent> DeviceList, out String message)
        {
            message = "";
            Boolean bAllDevicesFound = false;
            foreach (object obj in DeviceList)
            {
                if (obj.GetType() == typeof(NS_RoutinesController.RoutinesController))
                {
                    RoutineCntrl = (NS_RoutinesController.RoutinesController)obj;
                    bAllDevicesFound = true;
                    break;
                }
            }

            if (true == bAllDevicesFound)
                return NS_Common.ErrorCodesList.OK;
            else
            {
                message = "If you want to use 'SingleSubRoutineAction' class, you have to pass 'NS_RoutinesController.RoutinesController' to routine controller";
                return NS_Common.ErrorCodesList.FAILED;
            }
        }

        public override short RemoveMyDevices(out string error_message)
        {
            error_message = "";
            RoutineCntrl = null;
            return NS_Common.ErrorCodesList.OK;
        }

        public override bool StopActionPerformance()
        {
            return (RoutineCntrl.AbortRoutine(m_SubRoutineIndex) == NS_Common.ErrorCodesList.OK);
        }

        /// <summary>
        /// Perform SubRoutine Action
        /// </summary>
        /// <returns>Return True if succeeded: Otherwise return false</returns>
        public override bool PerformAction()
        {
            ActionStateCompleted = false;
            return (RoutineCntrl.PerformRoutine(m_SubRoutineIndex) == NS_Common.ErrorCodesList.OK);
        }

        /// <summary>
        /// Set the callback event reference point for action completed event
        /// </summary>
        /// <param name="ParentStep"></param>
        /// <returns></returns>
        public override void SetActionCompletedHandler(BaseStepDescriptor ParentStep)
        {
            ActionStateCompleted = false;

            BaseRoutineDescriptor routine;
            RoutineCntrl.GetRoutineByRoutineIndex(m_SubRoutineIndex, out routine);
            routine.RoutineCompleted += Routine_RoutineCompleted;

            ActionCompleted += ParentStep.Action_ActionCompleted;
        }

        public override void RemoveEventsHandlers(BaseStepDescriptor ParentStep)
        {
            BaseRoutineDescriptor routine;
            RoutineCntrl.GetRoutineByRoutineIndex(m_SubRoutineIndex, out routine);
            routine.RoutineCompleted -= Routine_RoutineCompleted;

            ActionCompleted -= ParentStep.Action_ActionCompleted;
        }

         

        public override string GetShortDescription()
        {
            return "Perform Sub Routine";
        }

        public override string GetFullDescription()
        {
            return "Performs Sub Routine";
        }

        #endregion Public overridden functions

        #region Private functions

        private void Routine_RoutineCompleted(object Sender, EventArgs args)
        {
            OnActionCompleted(this);
        }

        #endregion Private functions
    }
}