using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using log4net; 
using NS_Routines_Controller_Common.Routine_Definitions.Actions;
using NS_Routines_Controller_Common.Service;
using NS_Common.Extension_Methods;
using NS_Common.ViewModels.Common;

namespace NS_Routines_Controller_Common.Routine_Definitions.Base_Classes
{
    public abstract class BaseStepDescriptor : ViewModelBase
    {
        #region Protected members

        [XmlIgnore]
        protected Thread m_StepPerformanceThread = null;
        [XmlIgnore]
        protected BaseRoutineDescriptor myParentRoutine;
        [XmlIgnore]
        protected Boolean m_StepInProgress = false;

        [XmlIgnore]
        protected ILog m_SystemLog = LogManager.GetLogger("RoutinesController");

        #endregion Protected members

        #region Public events delegate functions

        #region Step Completed

        /// <summary>
        /// The events delegate function. All step actions completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void StepCompletedHandler(Object Sender, StepEventArguments args);
        private event StepCompletedHandler StepCompletedDelegate;

        protected void OnStepCompleted(object Sender)
        {
            if (this.StepCompletedDelegate != null)
                this.StepCompletedDelegate(Sender, new StepEventArguments((BaseStepDescriptor)Sender));
        }

        /// <summary>
        /// re-define the StepCompleted event
        /// </summary>
        public event StepCompletedHandler StepCompleted
        {
            // this is the equivalent of ActionCompleted += new EventHandler(...)
            add
            {
                //if (this.StepCompletedDelegate != null)
                //{
                //    StepCompleted -= this.StepCompletedDelegate;
                //    this.StepCompletedDelegate = null;
                //}
                if (this.StepCompletedDelegate == null || !this.StepCompletedDelegate.GetInvocationList().Contains(value))
                {
                    this.StepCompletedDelegate += value;
                }
            }
            // this is the equivalent of ActionCompleted -= new EventHandler(...)
            remove
            {
                this.StepCompletedDelegate -= value;
            }
        }

        #endregion Step Completed
  
        #region Error Occurred during performance Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occuers when there is an error during performance.
        /// The eeror description can be received from "StepErrorMessage" and "StepErrorCode"
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void PerformanceErrorOccuerdHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private PerformanceErrorOccuerdHandler PerformanceErrorOccuerdDelegate;

        protected void OnPerformanceErrorOccuerd(object Sender, EventArgs args)
        {
            if (m_SystemLog.IsErrorEnabled)
                m_SystemLog.Error(m_StepErrorMessage);

            if (this.PerformanceErrorOccuerdDelegate != null)
                this.PerformanceErrorOccuerdDelegate(Sender, args);
        }

        /// <summary>
        /// re-define the PerformanceErrorOccuerd event
        /// </summary>
        public event PerformanceErrorOccuerdHandler PerformanceErrorOccuerd
        {
            // this is the equivalent of PerformanceErrorOccuerd += new EventHandler(...)
            add
            {
                //if (this.PerformanceErrorOccuerdDelegate != null)
                //{
                //    PerformanceErrorOccuerd -= this.PerformanceErrorOccuerdDelegate;
                //    this.PerformanceErrorOccuerdDelegate = null;
                //}
                if (this.PerformanceErrorOccuerdDelegate == null || !this.PerformanceErrorOccuerdDelegate.GetInvocationList().Contains(value))
                {
                    this.PerformanceErrorOccuerdDelegate += value;
                }
            }
            // this is the equivalent of PerformanceErrorOccuerd -= new EventHandler(...)
            remove
            {
                this.PerformanceErrorOccuerdDelegate -= value;
            }
        }

        #endregion Error Occurred during performance Event

       

        #endregion Public events delegate functions

        #region Public Types

        public enum _STEP_PRIORITY
        {
            SP_NORMAL = 0,
            SP_LOW = 1,
            SP_HIGHT = 2
        }

        #endregion Public Types

        #region Constructor

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public BaseStepDescriptor()
        {
            m_ActionsCol = new ActionsCollection();
            m_StepDataBaseID = -1;
            m_RoutineDatabaseStepIndex = -1;
            
        }

        /// <summary>
        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inStepName">The step name</param>
        /// <param name="inStepEnabled">Indicate if current step is enable</param>
        public BaseStepDescriptor(String inStepName, Boolean inStepEnabled)
            : this()
        {
            m_StepName = inStepName;
            m_StepEnabled = inStepEnabled;
        }

        #endregion Constructor

        #region Public properties

        [XmlIgnore]
        protected String m_StepName = "Step Name Not Set";
        /// <summary>
        /// Get/Set the step name
        /// </summary>
        [XmlElement("Step_Name")]
        public String StepName
        {
            get { return m_StepName; }
            set { m_StepName = value; }
        }

        [XmlIgnore]
        protected Int32 m_StepDataBaseID = -1;
        /// <summary>
        /// The Step Database ID
        /// </summary>
        [XmlIgnore]
        public Int32 StepDataBaseID
        {
            get { return m_StepDataBaseID; }
            set { m_StepDataBaseID = value; }
        }

        [XmlIgnore]
        protected ActionsCollection m_ActionsCol = new ActionsCollection() ;
        /// <summary>
        /// Get/Set the actions collection for current step
        /// </summary>
        [XmlElement("Action")]
        public ActionsCollection ActionsCol
        {
            get { return m_ActionsCol; }
            set { m_ActionsCol = value; }
        }

        [XmlIgnore]
        protected _STEP_PRIORITY m_StepPriorityLevel = _STEP_PRIORITY.SP_NORMAL;
        /// <summary>
        /// Set get the step priority level, the value can affect on other steps to be performed slower.
        /// </summary>
        [XmlIgnore]
        public _STEP_PRIORITY StepPriorityLevel
        {
            get { return m_StepPriorityLevel; }
            set { m_StepPriorityLevel = value; }
        }

        [XmlIgnore]
        protected int m_RoutineDatabaseStepIndex = -1;
        /// <summary>
        /// Store the step index for Routine, used to enumerate steps, and to keep the proper line of performance
        /// </summary>
        [XmlIgnore]
        public int RoutineDatabaseStepIndex
        {
            get { return m_RoutineDatabaseStepIndex; }
            set { m_RoutineDatabaseStepIndex = value; }
        }

        [XmlIgnore]
        protected short m_StepErrorCode = NS_Common.ErrorCodesList.OK;
        /// <summary>
        /// Indicate the Error code state of current Step performance
        /// </summary>
        [XmlIgnore]
        public short StepErrorCode
        {
            get { return m_StepErrorCode; }
            set { m_StepErrorCode = value; }
        }

        [XmlIgnore]
        protected Boolean m_StepEnabled = true;
        /// <summary>
        /// Indicate if current step is Enable. If not the system jumps to next step
        /// </summary>
        [XmlElement("Step_Enabled")]
        public Boolean StepEnabled
        {
            get { return m_StepEnabled; }
            set { m_StepEnabled = value; }
        }

        [XmlIgnore]
        protected String m_StepErrorMessage = "";
        /// <summary>
        /// Indicate the Error message of current Step performance
        /// </summary>
        [XmlIgnore]
        public String StepErrorMessage
        {
            get { return m_StepErrorMessage; }
            protected set { m_StepErrorMessage = value; }
        }

        [XmlIgnore]
        protected Boolean StepInProgress
        {
            get { return m_StepInProgress; }
            set { m_StepInProgress = value; }
        }

        #endregion Public properties

        #region Public Functions

        /// <summary>
        /// Add Base action description to step
        /// </summary>
        /// <param name="BaseAction">The single action definition the base</param>
        public virtual void AddAction(BaseActionDescriptor BaseAction)
        {
            m_ActionsCol.Add(BaseAction); 
        }
         
        #endregion Public Functions

        #region Public Virtual functions

        public virtual short SetRoutineIndex(Int32 parent_routine_index)
        {
           
            m_ActionsCol.ToList().ForEach(x => x.SetMyParentRoutineIndex(parent_routine_index));
            return NS_Common.ErrorCodesList.OK;
        }

        public virtual short SetActionsDevices(Dictionary<string, SFW.IComponent> DeviceList, out String error_message)
        {
            short result = NS_Common.ErrorCodesList.OK;
            String res_message = "";

            foreach (BaseActionDescriptor action in m_ActionsCol)
            {
                String local_res_message = "";
                short local_result = action.SetMyDevices(DeviceList, out local_res_message);
                if (NS_Common.ErrorCodesList.OK != local_result)
                {
                    res_message += local_res_message + "\n\r";
                    result = local_result;
                }
            }
            error_message = res_message;
            return result;
        }

        public virtual short RemoveActionsDevices(out String error_message)
        {
            short result = NS_Common.ErrorCodesList.OK;
            String res_message = "";
            foreach (BaseActionDescriptor action in m_ActionsCol)
            {
                String local_res_message = "";
                short local_result = action.RemoveMyDevices(out local_res_message);
                if (NS_Common.ErrorCodesList.OK != local_result)
                {
                    res_message += local_res_message + "\n\r";
                    result = local_result;
                }
            }
            error_message = res_message;
            return result;
        }

        /// <summary>
        /// Perform the member wise copy of class object to new reference
        /// </summary>
        /// <returns></returns>
        public virtual BaseStepDescriptor Clone(BaseRoutineDescriptor inParentRoutine)
        {
            BaseStepDescriptor tmpStep = (BaseStepDescriptor)this.MemberwiseClone();
            myParentRoutine = inParentRoutine;
            tmpStep.ActionsCol = this.ActionsCol.Clone();
            return tmpStep;
        }

        public virtual void PerformRoutineStoppedInMiddleMethods()
        { 

            foreach (BaseActionDescriptor action in m_ActionsCol)
            {
                action.RoutineStoppedInMiddle();
            } 
        }

        #endregion Public Virtual functions

        #region Public Abstract functions

        /// <summary>
        /// Stops the performance of all active tasks in this step
        /// </summary>
        /// <returns></returns>
        public abstract Boolean StopActionsPerformance();

        /// <summary>
        /// Perform step, if child class not override the class
        /// function reports about step finished immediately.
        /// </summary>
        public abstract void PerformStep(BaseRoutineDescriptor ParentRoutine);

        /// <summary>
        /// Step performance main thread
        /// </summary>
        protected abstract void StepPerformanceFunction();

        public abstract void RemoveEventsHandlers();

        /// <summary>
        /// Occurs when action is completed
        /// </summary>
        /// <param name="Sender">The Action</param>
        /// <param name="args"></param>
        public abstract void Action_ActionCompleted(object Sender, EventArgs args);

        ///// <summary>
        ///// Occurs when Axis failed to reach target Position
        ///// </summary>
        ///// <param name="Sender">The Action</param>
        ///// <param name="args"></param>
        //public abstract void Action_FailedToReachTargetPosition(object Sender, EventArgs args);

        public abstract void SetStepEventsHandlers(BaseRoutineDescriptor ParentRoutine);

        #endregion Public Abstract functions


    }

   
}