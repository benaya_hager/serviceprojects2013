using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using log4net; 
using SFW;
using System.Threading;
using System.ComponentModel;
using NS_Common.ViewModels.Common;

namespace NS_Routines_Controller_Common.Routine_Definitions.Base_Classes
{
    public abstract class BaseActionDescriptor : ViewModelBase
    {
        #region Public And Protected members

        public event PropertyChangedEventHandler PropertyChanged;

         
        [XmlIgnore]
        protected ILog m_SystemLog = LogManager.GetLogger("RoutinesController");

        [XmlIgnore]
        public EventGroupHandler EventGroupReady;

        [XmlIgnore]
        protected CancellationTokenSource m_ThreadCancellationSource;
        [XmlIgnore]
        protected Thread m_ActionPerformanceThread;
        [XmlIgnore]
        protected object m_SynchronizationObject = new System.Object();


        #endregion Public And Protected members

        #region Public events delegate functions

        #region Action completed Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when action completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ActionCompletedHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private ActionCompletedHandler ActionCompletedDelegate;

        protected virtual Boolean OnActionCompleted(object Sender)
        {
            if (m_SystemLog.IsDebugEnabled)
                m_SystemLog.Debug("Single Action Completed --> " + m_ActionName);

            Int32 iCount = 0;
            while (null == this.ActionCompletedDelegate && iCount++ < 20)
                System.Threading.Thread.Sleep(100);
            if (iCount >= 20)
                throw new Exception("The Handle to Action Complete not set");

            if (null != this.ActionCompletedDelegate)
            {
                this.ActionCompletedDelegate(Sender, new EventArgs());
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// re-define the ActionCompleted event
        /// </summary>
        public event ActionCompletedHandler ActionCompleted
        {
            // this is the equivalent of ActionCompleted += new EventHandler(...)
            add
            {
                //if (this.ActionCompletedDelegate != null)
                //{
                //    ActionCompleted -= this.ActionCompletedDelegate;
                //    this.ActionCompletedDelegate = null;
                //}
                if (this.ActionCompletedDelegate == null || !this.ActionCompletedDelegate.GetInvocationList().Contains(value))
                {
                    this.ActionCompletedDelegate += value;
                }
            }
            // this is the equivalent of ActionCompleted -= new EventHandler(...)
            remove
            {
                this.ActionCompletedDelegate -= value;
            }
        }

        #endregion Action completed Event

       

        #region Failed During Action Performance Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Action failed during performance.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void FailedDuringActionPerformanceHandler(Object Sender, short error_code, String error_message);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private FailedDuringActionPerformanceHandler FailedDuringActionPerformanceDelegate;

        protected void OnFailedDuringActionPerformance(object Sender, short error_code, String error_message)
        {
            if (m_SystemLog.IsErrorEnabled)
                m_SystemLog.Error(error_message);

            if (this.FailedDuringActionPerformanceDelegate != null)
                this.FailedDuringActionPerformanceDelegate(Sender, error_code, error_message);
        }

        /// <summary>
        /// re-define the FailedDuringActionPerformance event
        /// </summary>
        public event FailedDuringActionPerformanceHandler FailedDuringActionPerformance
        {
            // this is the equivalent of FailedDuringActionPerformance += new EventHandler(...)
            add
            {
                //if (this.FailedDuringActionPerformanceDelegate != null)
                //{
                //    FailedDuringActionPerformance -= this.FailedDuringActionPerformanceDelegate;
                //    this.FailedDuringActionPerformanceDelegate = null;
                //}
                if (this.FailedDuringActionPerformanceDelegate == null || !this.FailedDuringActionPerformanceDelegate.GetInvocationList().Contains(value))
                {
                    this.FailedDuringActionPerformanceDelegate += value;
                }
            }
            // this is the equivalent of FailedDuringActionPerformance -= new EventHandler(...)
            remove
            {
                this.FailedDuringActionPerformanceDelegate -= value;
            }
        }

        #endregion Failed During Action Performance Event

        #endregion Public events delegate functions

        #region Public Types

        /// <summary>
        /// The action groups type enumerator
        /// </summary>
        public enum _ACTION_GROUP_TYPES
        {
            AGT_NOT_SET = 0,
            AGT_MOTION = 1,
            AGT_WAIT = 2,
            AGT_HOME_ROUTINE = 3,
            AGT_LASER_ACTION = 4
        };

        #endregion Public Types

        #region Constructors

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public BaseActionDescriptor()
        {
            m_ActionDataBaseID = -1;
            m_ActionName = "Action";
        }

        #endregion Constructors

        #region Public properties

        [XmlIgnore]
        protected Boolean m_ActionStateCompleted = false;

        /// <summary>
        /// Indicate if action is completed
        /// </summary>
        [XmlIgnore]
        public Boolean ActionStateCompleted
        {
            get { return m_ActionStateCompleted; }
            set { m_ActionStateCompleted = value; }
        }

        [XmlIgnore]
        protected String m_ActionName = "Action";

        /// <summary>
        /// Additional information for Action (Optional)
        /// Get/Set the action name, by default is "Action"
        /// </summary>
        [XmlElement("Action_Name")]
        public String ActionName
        {
            get { return m_ActionName; }
            set { m_ActionName = value; }
        }

        [XmlIgnore]
        protected int m_ActionDataBaseID = -1;

        /// <summary>
        /// The Action Database ID
        /// </summary>
        [XmlIgnore]
        public Int32 ActionDataBaseID
        {
            get { return m_ActionDataBaseID; }
            set { m_ActionDataBaseID = value; }
        }

        [XmlIgnore]
        private Boolean m_ActionStartFailed = false;

        [XmlIgnore]
        public Boolean ActionStartFailed
        {
            get { return m_ActionStartFailed; }
            set { m_ActionStartFailed = value; }
        }

        [XmlIgnore]
        protected short m_ActionErrorCode = NS_Common.ErrorCodesList.OK;

        /// <summary>
        /// Indicate the Error code state of current action performance
        /// </summary>
        [XmlIgnore]
        public short ActionErrorCode
        {
            get { return m_ActionErrorCode; }
            protected set { m_ActionErrorCode = value; }
        }

        [XmlIgnore]
        protected String m_ActionErrorMessage = "";

        /// <summary>
        /// Indicate the Error message of current action performance.
        /// </summary>
        [XmlIgnore]
        public String ActionErrorMessage
        {
            get { return m_ActionErrorMessage; }
            protected set { m_ActionErrorMessage = value; }
        }

        [XmlIgnore]
        protected Int32 m_MyParentRoutineIndex;
        [XmlIgnore]
        public Int32 MyParentRoutineIndex
        {
            get { return m_MyParentRoutineIndex; }
            protected set { m_MyParentRoutineIndex = value; }
        }

        #endregion Public properties

        #region Public abstract functions

        /// <summary>
        /// Return the short display string. Used in Routine viewer actions display list
        /// </summary>
        /// <returns></returns>
        public abstract String GetShortDescription();

        /// <summary>
        ///  Return full  action description. Used in Routine viewer actions display list, on mouse over 
        /// </summary>
        /// <returns></returns>
        public abstract String GetFullDescription();

        /// <summary>
        /// Removes all associated devices logics and listeners from action
        /// </summary>
        /// <param name="error_message"></param>
        /// <returns></returns>
        public virtual short RemoveMyDevices(out String error_message)
        {
            error_message = "";
            return 0;
        }

        /// <summary>
        ///  Sets  devices logics and listeners necessary for action performance
        /// </summary>
        /// <param name="DeviceList"></param>
        /// <param name="error_message"></param>
        /// <returns></returns>
        public abstract short SetMyDevices(Dictionary<string, SFW.IComponent> DeviceList, out String error_message);
 
        #endregion Public abstract functions

        #region Virtual functions

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Set the action completed event handler for step
        /// </summary>
        /// <param name="ParentStep"></param>
        public virtual void SetActionCompletedHandler(BaseStepDescriptor ParentStep)
        {
            ActionCompleted += ParentStep.Action_ActionCompleted;
            return;
        }

        /// <summary>
        /// Removes all events handlers
        /// </summary>
        /// <param name="ParentStep"></param>
        public virtual void RemoveEventsHandlers(BaseStepDescriptor ParentStep)
        {
            try
            { 
                ActionCompleted -= ParentStep.Action_ActionCompleted;
            }
            catch
            {

            }
        }

        /// <summary>
        /// Sets all events handlers
        /// </summary>
        /// <param name="ParentRoutine"></param>
        /// <param name="ParentStep"></param>
        public virtual void SetActionEventsHandlers(BaseRoutineDescriptor ParentRoutine, BaseStepDescriptor ParentStep)
        {
            ActionStateCompleted = false;

            ActionCompleted += new BaseActionDescriptor.ActionCompletedHandler(ParentStep.Action_ActionCompleted);

        }

        /// <summary>
        /// Starts man action performance thread
        /// </summary>
        /// <returns></returns>
        public virtual Boolean PerformAction()
        {
            m_ThreadCancellationSource = new CancellationTokenSource();
            //Init Input channels watcher thread data
            ParameterizedThreadStart myThreadStart = new ParameterizedThreadStart(ThreadFunction);
            m_ActionPerformanceThread = new Thread(myThreadStart);
            m_ActionPerformanceThread.Name = m_ActionName;
            m_ActionPerformanceThread.IsBackground = true;
            m_ActionPerformanceThread.SetApartmentState(ApartmentState.STA);
            m_ActionPerformanceThread.Priority = ThreadPriority.Normal;
            //m_ActionPerformanceThread.Name = "TMT Action Wait Time/Diameter/Temperature Thread";
            m_ActionPerformanceThread.Start(m_ThreadCancellationSource.Token);

            return true;
        }

        /// <summary>
        /// Stop action performance
        /// </summary>
        /// <returns></returns>
        public virtual bool StopActionPerformance()
        {
            lock (m_SynchronizationObject) //myIOLogic)
            {
                if (null != m_ThreadCancellationSource)
                    m_ThreadCancellationSource.Cancel();
            }


            return true;
        }

        /// <summary>
        /// Used to stop all action started threads in case of routine stop in middle of process.
        /// Such as Start "Data Acquisition" threads "saving threads" etc.
        /// </summary>
        /// <returns></returns>
        public virtual short RoutineStoppedInMiddle()
        {
            return NS_Common.ErrorCodesList.OK;
        }

        /// <summary>
        /// Updates the Child steps with parent routine index. To enable the option to stop routine even from child step.
        /// </summary>
        /// <param name="parent_routine_index"></param>
        /// <returns></returns>
        public virtual short SetMyParentRoutineIndex(int parent_routine_index)
        {
            m_MyParentRoutineIndex = parent_routine_index;
            return NS_Common.ErrorCodesList.OK;
        }

        /// <summary>
        /// Member wise Clone action
        /// </summary>
        /// <returns></returns>
        public virtual BaseActionDescriptor Clone()
        {
            return (BaseActionDescriptor)this.MemberwiseClone();
        }


        /// <summary>
        /// Implements the base send event method
        /// </summary>
        /// <param name="Event"></param>
        protected virtual void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        /// <summary>
        /// Action performance main thread
        /// </summary>
        protected virtual void ThreadFunction(object token)
        {
            //try
            //{
            //    Int32 i = 0;
            //}
            //catch (Exception ex)
            //{
            //    OnFailedDuringActionPerformance(this, NS_Common.ErrorCodesList.FAILED, ex.Message);
            //}
            //finally
            //{
            //    OnActionCompleted(this);
            //}
            OnActionCompleted(this);
        }

        #endregion


       
    }
}