﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using log4net; 
using SFW;
using NS_Routines_Controller_Common.Service;
using NS_Common.Service;
using NS_Common;
using NS_Routines_Controller_Common.Routine_Definitions.Steps;
using NS_Routines_Controller_Common.Routine_Definitions.Actions;
using NS_Common.ViewModels.Common;

namespace NS_Routines_Controller_Common.Routine_Definitions.Base_Classes
{
    [XmlRoot("Routine_File")]
    public abstract class BaseRoutineDescriptor : ViewModelBase
    {
        #region Protected Members

        //[XmlIgnore]
        //protected ucFoldingRoutineProgress m_RoutineProgressControl = null;

        [XmlIgnore]
        protected ILog m_SystemLog = LogManager.GetLogger("RoutinesController");

        [XmlIgnore]
        protected ILog m_Log = LogManager.GetLogger("RoutinesStates");

        [XmlIgnore]
        private Tools.StopWatch.StopWatch GlobalRoutineStopWatch = new Tools.StopWatch.StopWatch();

        #endregion Protected Members

        #region Private Members

        [XmlIgnore]
        protected Boolean m_Is_IO_Enable = true;
        [XmlIgnore]
        public Boolean Is_IO_Enable
        {
            get { return m_Is_IO_Enable; }
            set { m_Is_IO_Enable = value; }
        }

        #endregion Private Members

        #region Public events delegate functions

        #region Routine Step completed Event

        /// <summary>
        /// The events delegate function. Routine Step performance completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void StepCompletedHandler(Object Sender, EventArgs args);

        private event StepCompletedHandler StepCompletedDelegate;

        protected void OnStepCompleted(object Sender)
        {
            if (StepCompletedDelegate != null)
                StepCompletedDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the StepCompleted event
        /// </summary>
        public event StepCompletedHandler StepCompleted
        {
            // this is the equivalent of ActionCompleted += new EventHandler(...)
            add
            {
                //if (StepCompletedDelegate != null)
                //{
                //    StepCompleted -= StepCompletedDelegate;
                //    StepCompletedDelegate = null;
                //}
                if (this.StepCompletedDelegate == null || !this.StepCompletedDelegate.GetInvocationList().Contains(value))
                    StepCompletedDelegate += value;
            }
            // this is the equivalent of StepCompleted -= new EventHandler(...)
            remove { StepCompletedDelegate -= value; }
        }

        #endregion Routine Step completed Event

        #region Routine completed Event

        /// <summary>
        /// The events delegate function. Routine performance completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RoutineCompletedHandler(Object Sender, EventArgs args);

        private event RoutineCompletedHandler RoutineCompletedDelegate;

        protected void OnRoutineCompleted(object Sender)
        {
            if (RoutineCompletedDelegate != null)
                RoutineCompletedDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the RoutineCompleted event
        /// </summary>
        public event RoutineCompletedHandler RoutineCompleted
        {
            // this is the equivalent of ActionCompleted += new EventHandler(...)
            add
            {
                //if (RoutineCompletedDelegate != null)
                //{
                //    RoutineCompleted -= RoutineCompletedDelegate;
                //    RoutineCompletedDelegate = null;
                //}
                if (this.RoutineCompletedDelegate == null || !this.RoutineCompletedDelegate.GetInvocationList().Contains(value))
                    RoutineCompletedDelegate += value;
            }
            // this is the equivalent of RoutineCompleted -= new EventHandler(...)
            remove { RoutineCompletedDelegate -= value; }
        }

        #endregion Routine completed Event

        #region Routine Failed Event

        /// <summary>
        /// The events delegate function. Routine performance Failed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RoutineFailedHandler(Object Sender, EventArgs args);

        private event RoutineFailedHandler RoutineFailedDelegate;

        protected void OnRoutineFailed(object Sender)
        {
            if (RoutineFailedDelegate != null)
                RoutineFailedDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the RoutineFailed event
        /// </summary>
        public event RoutineFailedHandler RoutineFailed
        {
            // this is the equivalent of ActionFailed += new EventHandler(...)
            add
            {
                //if (RoutineFailedDelegate != null)
                //{
                //    RoutineFailed -= RoutineFailedDelegate;
                //    RoutineFailedDelegate = null;
                //}
                if (this.RoutineFailedDelegate == null || !this.RoutineFailedDelegate.GetInvocationList().Contains(value))
                    RoutineFailedDelegate += value;
            }
            // this is the equivalent of RoutineFailed -= new EventHandler(...)
            remove { RoutineFailedDelegate -= value; }
        }

        #endregion Routine Failed Event
        
        #region Error Occurred during performance Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when there is an error during performance.
        /// The error description can be received from "RoutineErrorMessage" and "RoutineErrorCode"
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void PerformanceErrorOccuerdHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private PerformanceErrorOccuerdHandler PerformanceErrorOccuerdDelegate;

        protected void OnPerformanceErrorOccuerd(object Sender, EventArgs args)
        {
            if (this.PerformanceErrorOccuerdDelegate != null)
                this.PerformanceErrorOccuerdDelegate(Sender, args);
        }

        /// <summary>
        /// re-define the PerformanceErrorOccuerd event
        /// </summary>
        public event PerformanceErrorOccuerdHandler PerformanceErrorOccuerd
        {
            // this is the equivalent of PerformanceErrorOccuerd += new EventHandler(...)
            add
            {
                //if (this.PerformanceErrorOccuerdDelegate != null)
                //{
                //    PerformanceErrorOccuerd -= this.PerformanceErrorOccuerdDelegate;
                //    this.PerformanceErrorOccuerdDelegate = null;
                //}
                if (this.PerformanceErrorOccuerdDelegate == null || !this.PerformanceErrorOccuerdDelegate.GetInvocationList().Contains(value))
                    this.PerformanceErrorOccuerdDelegate += value;
            }
            // this is the equivalent of PerformanceErrorOccuerd -= new EventHandler(...)
            remove
            {
                this.PerformanceErrorOccuerdDelegate -= value;
            }
        }

        #endregion Error Occurred during performance Event

        #region Change Routine State Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Routine state needs to be change.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ChangeRoutineStateHandler(Object Sender, _ROUTINE_STATE arg);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private ChangeRoutineStateHandler ChangeRoutineStateDelegate;

        protected void OnChangeRoutineState(object Sender, _ROUTINE_STATE arg)
        {
            if (this.ChangeRoutineStateDelegate != null)
            {
                this.ChangeRoutineStateDelegate(this, arg);
                this.IncreeseStepIndex = false;
            }
            else
            {
                //m_Log.Error("The event change routine state was unhandeled.");
                //throw new Exception("The event change routine state was unhandeled.");
            }
        }

        /// <summary>
        /// re-define the ChangeRoutineState event
        /// </summary>
        public event ChangeRoutineStateHandler ChangeRoutineState
        {
            // this is the equivalent of ChangeRoutineState += new EventHandler(...)
            add
            {
                //if (this.ChangeRoutineStateDelegate != null)
                //{
                //    ChangeRoutineState -= this.ChangeRoutineStateDelegate;
                //    this.ChangeRoutineStateDelegate = null;
                //}
                if (this.ChangeRoutineStateDelegate == null || !this.ChangeRoutineStateDelegate.GetInvocationList().Contains(value))
                    this.ChangeRoutineStateDelegate += value;
            }
            // this is the equivalent of ChangeRoutineState -= new EventHandler(...)
            remove
            {
                this.ChangeRoutineStateDelegate -= value;
            }
        }

        #endregion Change Routine State Event

        #region Routine Step changed

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Routine Step changed.
        /// </summary>
        /// <param name="Sender">The Routine handler</param>
        /// <param name="Step">Current step handler</param>
        public delegate void RoutineStepChangedHandler(Object Sender, BaseStepDescriptor Step);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private RoutineStepChangedHandler RoutineStepChangedDelegate;

        public void OnRoutineStepChanged(object Sender, BaseStepDescriptor Step)
        {
            if (this.RoutineStepChangedDelegate != null)
                this.RoutineStepChangedDelegate(Sender, Step);
        }

        /// <summary>
        /// re-define the RoutineStepChanged event
        /// </summary>
        public event RoutineStepChangedHandler RoutineStepChanged
        {
            // this is the equivalent of RoutineStepChanged += new EventHandler(...)
            add
            {
                //if (this.RoutineStepChangedDelegate != null)
                //{
                //    RoutineStepChanged -= this.RoutineStepChangedDelegate;
                //    this.RoutineStepChangedDelegate = null;
                //}
                if (this.RoutineStepChangedDelegate == null || !this.RoutineStepChangedDelegate.GetInvocationList().Contains(value))
                {
                    this.RoutineStepChangedDelegate += value;
                }
            }
            // this is the equivalent of RoutineStepChanged -= new EventHandler(...)
            remove
            {
                this.RoutineStepChangedDelegate -= value;
            }
        }

        #endregion Routine Step changed
         
        #region Jump to next routine step

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when user press on Jump to next routine step button.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void Jump2NextStepHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private Jump2NextStepHandler Jump2NextStepDelegate;

        protected void OnJump2NextStep(object Sender, EventArgs args)
        {
            if (this.Jump2NextStepDelegate != null)
                this.Jump2NextStepDelegate(this, args);
        }

        /// <summary>
        /// re-define the Jump2NextStep event
        /// </summary>
        public event Jump2NextStepHandler Jump2NextStep
        {
            // this is the equivalent of Jump2NextStep += new EventHandler(...)
            add
            {
                if (this.Jump2NextStepDelegate == null || !this.Jump2NextStepDelegate.GetInvocationList().Contains(value))
                    this.Jump2NextStepDelegate += value;
            }
            // this is the equivalent of Jump2NextStep -= new EventHandler(...)
            remove
            {
                this.Jump2NextStepDelegate -= value;
            }
        }

        #endregion Jump to next routine step

        #region Remove me from list request Event

        /// <summary>
        /// The events delegate function. Remove this routine from the available list.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void RemoveMeFromListHandler(Object Sender, EventArgs args);

        private event RemoveMeFromListHandler RemoveMeFromListDelegate;

        protected void OnRemoveMeFromList(object Sender)
        {
            if (RemoveMeFromListDelegate != null)
                RemoveMeFromListDelegate(Sender, new EventArgs());
        }

        /// <summary>
        /// re-define the RemoveMeFromList event
        /// </summary>
        public event RemoveMeFromListHandler RemoveMeFromList
        {
            // this is the equivalent of ActionCompleted += new EventHandler(...)
            add
            {
                //if (RemoveMeFromListDelegate != null)
                //{
                //    RemoveMeFromList -= RemoveMeFromListDelegate;
                //    RemoveMeFromListDelegate = null;
                //}
                if (this.RemoveMeFromListDelegate == null || !this.RemoveMeFromListDelegate.GetInvocationList().Contains(value))
                    RemoveMeFromListDelegate += value;
            }
            // this is the equivalent of RemoveMeFromList -= new EventHandler(...)
            remove { RemoveMeFromListDelegate -= value; }
        }

        #endregion Routine completed Event

        #region Jump to Previous routine step

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when user press on Jump to Previous routine step button.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void Jump2PreviousStepHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private Jump2PreviousStepHandler Jump2PreviousStepDelegate;

        protected void OnJump2PreviousStep(object Sender, EventArgs args)
        {
            if (this.Jump2PreviousStepDelegate != null)
                this.Jump2PreviousStepDelegate(this, args);
        }

        /// <summary>
        /// re-define the Jump2PreviousStep event
        /// </summary>
        public event Jump2PreviousStepHandler Jump2PreviousStep
        {
            // this is the equivalent of Jump2PreviousStep += new EventHandler(...)
            add
            {
                if (this.Jump2PreviousStepDelegate == null || !this.Jump2PreviousStepDelegate.GetInvocationList().Contains(value))
                    this.Jump2PreviousStepDelegate += value;
            }
            // this is the equivalent of Jump2PreviousStep -= new EventHandler(...)
            remove
            {
                this.Jump2PreviousStepDelegate -= value;
            }
        }

        #endregion Jump to Previous routine step

        #region Stop Playing Selected Routine

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when user press on stop routine button.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void StopRoutineHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private StopRoutineHandler StopRoutineDelegate;

        protected void OnStopRoutine(object Sender, EventArgs args)
        {
            if (this.StopRoutineDelegate != null)
                this.StopRoutineDelegate(this, args);
        }

        /// <summary>
        /// re-define the StopRoutine event
        /// </summary>
        public event StopRoutineHandler StopRoutine
        {
            // this is the equivalent of StopRoutine += new EventHandler(...)
            add
            {
                if (this.StopRoutineDelegate == null || !this.StopRoutineDelegate.GetInvocationList().Contains(value))
                    this.StopRoutineDelegate += value;
            }
            // this is the equivalent of StopRoutine -= new EventHandler(...)
            remove
            {
                this.StopRoutineDelegate -= value;
            }
        }

        #endregion Stop Playing Selected Routine

        #region Pause Playing Selected Routine

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when user press on Pause routine button.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void PauseRoutineHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private PauseRoutineHandler PauseRoutineDelegate;

        protected void OnPauseRoutine(object Sender, EventArgs args)
        {
            if (this.PauseRoutineDelegate != null)
                this.PauseRoutineDelegate(this, args);
        }

        /// <summary>
        /// re-define the PauseRoutine event
        /// </summary>
        public event PauseRoutineHandler PauseRoutine
        {
            // this is the equivalent of PauseRoutine += new EventHandler(...)
            add
            {
                if (this.PauseRoutineDelegate == null || !this.PauseRoutineDelegate.GetInvocationList().Contains(value))
                    this.PauseRoutineDelegate += value;
            }
            // this is the equivalent of PauseRoutine -= new EventHandler(...)
            remove
            {
                this.PauseRoutineDelegate -= value;
            }
        }

        #endregion Pause Playing Selected Routine

        #region Start Playing Selected Routine

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when user press on stop routine button.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void PlayRoutineHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private PlayRoutineHandler PlayRoutineDelegate;

        protected void OnPlayRoutine(object Sender, EventArgs args)
        {
            if (this.PlayRoutineDelegate != null)
                this.PlayRoutineDelegate(this, args);
        }

        /// <summary>
        /// re-define the PlayRoutine event
        /// </summary>
        public event PlayRoutineHandler PlayRoutine
        {
            // this is the equivalent of PlayRoutine += new EventHandler(...)
            add
            {
                if (this.PlayRoutineDelegate == null || !this.PlayRoutineDelegate.GetInvocationList().Contains(value))
                    this.PlayRoutineDelegate += value;
            }
            // this is the equivalent of PlayRoutine -= new EventHandler(...)
            remove
            {
                this.PlayRoutineDelegate -= value;
            }
        }

        #endregion Start Playing Selected Routine

        #endregion Public events delegate functions

        #region Constructor

        protected BaseRoutineDescriptor()
        {
            m_RoutineDataBaseID = -1;

            SynchronizationObject = new Object();
            m_StepColl = new StepsCollection();
           
            RoutineName = "";




            PreviousStep = new RelayCommand<Object>(p => this.SetPreviousStep(p), p => true);
            ContinueRoutinePerformance = new RelayCommand<Object>(p => this.ContinueRoutinePerform(p), p => true);
            PauseRoutinePerformance = new RelayCommand<Object>(p => this.PauseRoutinePerform(p), p => true);
            StopRoutinePerformance = new RelayCommand<Object>(p => this.StopRoutinePerform(p), p => true);
            NextStep = new RelayCommand<Object>(p => this.SetNextStep(p), p => true);
            
            
        }

       
        
        #endregion Constructor
         
        #region Public Properties

        #region Binding Properties
           

            [XmlIgnore]
        public RelayCommand<Object> PreviousStep { get; private set; }
            [XmlIgnore]
            private Boolean m_ButtonPreviousStepVisible = false;
            [XmlIgnore]
            public Boolean ButtonPreviousStepVisible
            {
                get { return m_ButtonPreviousStepVisible; }
                set
                {
                    m_ButtonPreviousStepVisible = value;
                    RaisePropertyChanged("ButtonPreviousStepVisible");
                }
            }


            [XmlIgnore]
            public RelayCommand<Object> ContinueRoutinePerformance { get; private set; }
            [XmlIgnore]
            private Boolean m_ButtonContinueRoutinePerformanceVisible = true;
            [XmlIgnore]
            public Boolean ButtonContinueRoutinePerformanceVisible
            {
                get { return m_ButtonContinueRoutinePerformanceVisible; }
                set
                {
                    m_ButtonContinueRoutinePerformanceVisible = value;
                    RaisePropertyChanged("ButtonContinueRoutinePerformanceVisible");
                }
            }



            [XmlIgnore]
            public RelayCommand<Object> PauseRoutinePerformance { get; private set; }
            [XmlIgnore]
            private Boolean m_ButtonPauseRoutinePerformanceVisible = false;
            [XmlIgnore]
            public Boolean ButtonPauseRoutinePerformanceVisible
            {
                get { return m_ButtonPauseRoutinePerformanceVisible; }
                set
                {
                    m_ButtonPauseRoutinePerformanceVisible = value;
                    RaisePropertyChanged("ButtonPauseRoutinePerformanceVisible");
                }
            }



            [XmlIgnore]
            public RelayCommand<Object> StopRoutinePerformance { get; private set; }
            [XmlIgnore]
            private Boolean m_ButtonStopRoutinePerformanceVisible = false;
            [XmlIgnore]
            public Boolean ButtonStopRoutinePerformanceVisible
            {
                get { return m_ButtonStopRoutinePerformanceVisible; }
                set
                {
                    m_ButtonStopRoutinePerformanceVisible = value;
                    RaisePropertyChanged("ButtonStopRoutinePerformanceVisible");
                }
            }



            [XmlIgnore]
            public RelayCommand<Object> NextStep { get; private set; }
            [XmlIgnore]
            private Boolean m_ButtonNextStepVisible = false;
            [XmlIgnore]
            public Boolean ButtonNextStepVisible
            {
                get { return m_ButtonNextStepVisible; }
                set
                {
                    m_ButtonNextStepVisible = value;
                    RaisePropertyChanged("ButtonNextStepVisible");
                }
            }
        #endregion  

        #region Synchronization objects

        [XmlIgnore]
        public Object SynchronizationObject;

        [XmlIgnore]
        public Int32 LockCounter;

        #endregion Synchronization objects

        [XmlIgnore]
        protected _ROUTINE_STATE m_RoutineState = _ROUTINE_STATE.RS_NOT_SET;
        /// <summary>
        /// Get/Set the active routine state
        /// </summary>
        [XmlIgnore]
        public virtual _ROUTINE_STATE RoutineState
        {
            get { return m_RoutineState; }
            set
            {
                switch (value)
                {
                    case _ROUTINE_STATE.RS_COMPLETED:
                        {
                            if (m_Log.IsDebugEnabled)
                                m_Log.Debug("Routine " + m_RoutineName + " is completed");

                            if (m_RoutineState != value)
                            {
                                m_RoutineState = value;
                                OnChangeRoutineState(this, m_RoutineState);

                                PerformRoutineStoppedInMiddleMethods();
                            }

                            ButtonPreviousStepVisible = false;
                            ButtonContinueRoutinePerformanceVisible = false;
                            ButtonPauseRoutinePerformanceVisible = false;
                            ButtonStopRoutinePerformanceVisible = false;
                            ButtonNextStepVisible = false;

                            SelectedStepIndex = -1;

                            OnRoutineCompleted(this);
                            RoutineState = _ROUTINE_STATE.RS_READY_TO_START;
                            break;
                        }
                    case _ROUTINE_STATE.RS_READY_TO_START:
                        {
                            if (m_Log.IsDebugEnabled)
                                m_Log.Debug("Routine " + m_RoutineName + " state set to Ready to start");


                            if (m_RoutineState != value)
                            {
                                m_RoutineState = value;
                                OnChangeRoutineState(this, m_RoutineState);
                            }



                            SelectedStepIndex = -1;
                            RoutineStepInProgress = false;
                            m_RoutineErrorCode = ErrorCodesList.OK;


                            ButtonPreviousStepVisible = false;
                            ButtonContinueRoutinePerformanceVisible = true;
                            ButtonPauseRoutinePerformanceVisible = false;
                            ButtonStopRoutinePerformanceVisible = false;
                            ButtonNextStepVisible = false;

                            break;
                        }
                    case _ROUTINE_STATE.RS_NOT_SET:
                        {
                            if (m_RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                                RoutineState = _ROUTINE_STATE.RS_COMPLETED;
                            else
                            {
                                if (m_Log.IsDebugEnabled)
                                    m_Log.Debug("Routine " + m_RoutineName + " state set to Not Set");

                                if (m_RoutineState != value)
                                {
                                    m_RoutineState = value;
                                    OnChangeRoutineState(this, m_RoutineState);
                                }

                                ButtonPreviousStepVisible = false;
                                ButtonContinueRoutinePerformanceVisible = true;
                                ButtonPauseRoutinePerformanceVisible = false;
                                ButtonStopRoutinePerformanceVisible = false;
                                ButtonNextStepVisible = false;

                            }
                            break;
                        }
                    case _ROUTINE_STATE.RS_IN_PROGRESS:
                        {
                            m_RoutineErrorCode = ErrorCodesList.OK;

                            #region Check Previous State

                            switch (m_RoutineState)
                            {
                                case _ROUTINE_STATE.RS_PAUSED:
                                    {    
                                        if (SelectedNode.ActionsCol.ContainsType(typeof(SingleRoutineStateChangeAction)))
                                        {
                                            this.IncreeseStepIndex = false;
                                        }
                                        if (m_Log.IsDebugEnabled)
                                            m_Log.Debug("Routine " + m_RoutineName + " state set Continue in progress");
                                        break;
                                    }
                                //case _ROUTINE_STATE.RS_FAILED:
                                //    {
                                //        if (m_Log.IsDebugEnabled)
                                //            m_Log.Debug("Routine " + m_RoutineName + " Failed");


                                //        if (m_RoutineState != value)
                                //        {
                                //            m_RoutineState = value;
                                //            OnChangeRoutineState(this, m_RoutineState);
                                //        }

                                //        OnRoutineFailed(this);

                                //        SelectedStepIndex = -1;
                                //        RoutineStepInProgress = false;
                                //        RoutineState = _ROUTINE_STATE.RS_READY_TO_START;

                                //        if (m_RoutineState != value)
                                //        {
                                //            m_RoutineState = value;
                                //            OnChangeRoutineState(this, m_RoutineState);
                                //        }
                                //        return;
                                //    }
                                //case _ROUTINE_STATE.RS_IM_WAITING:
                                //    {
                                //        if (RoutineStepInProgress)
                                //        {
                                //            if (m_Log.IsDebugEnabled)
                                //                m_Log.Debug("Routine " + m_RoutineName + " set to progress next step (" + SelectedNode.StepName + ")");
                                //            SelectedNode.PerformStep(this);
                                //        }
                                //        break;
                                //    } 
                            }
                            #endregion


                            if (m_Log.IsDebugEnabled)
                                m_Log.Debug("Routine " + m_RoutineName + " state set to " + Tools.String_Enum.StringEnum.GetStringValue(value));

                            if (m_RoutineState != value)
                            {
                                m_RoutineState = value;
                                OnChangeRoutineState(this, m_RoutineState);
                            }

                            ButtonPreviousStepVisible = true;
                            ButtonContinueRoutinePerformanceVisible = false;
                            ButtonPauseRoutinePerformanceVisible = true;
                            ButtonStopRoutinePerformanceVisible = true;
                            ButtonNextStepVisible = true;

                            break;
                        }
                    case _ROUTINE_STATE.RS_PAUSED:
                        {
                            if (m_RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS)
                            //||m_RoutineState == _ROUTINE_STATE.RS_IM_WAITING)
                            {
                                if (m_Log.IsDebugEnabled)
                                    m_Log.Debug("Routine " + m_RoutineName + " state set to " + Tools.String_Enum.StringEnum.GetStringValue(value));

                                if (m_RoutineState != value)
                                {
                                    m_RoutineState = value;
                                    OnChangeRoutineState(this, m_RoutineState);
                                }


                                ButtonPreviousStepVisible = true;
                                ButtonContinueRoutinePerformanceVisible = true;
                                ButtonPauseRoutinePerformanceVisible = false;
                                ButtonStopRoutinePerformanceVisible = true;
                                ButtonNextStepVisible = true;

                                RoutineStepInProgress = false;
                            }
                            break;
                        }
                    //case _ROUTINE_STATE.RS_IM_WAITING:
                    //    {
                    //        if (m_RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS ||
                    //            m_RoutineState == _ROUTINE_STATE.RS_PAUSED)
                    //        {
                    //            if (m_Log.IsDebugEnabled)
                    //                m_Log.Debug("Routine " + m_RoutineName + " continue to\\from ImWaiting");
                                
                    //            //((SingleWeldingRoutine)this).OnImWaiting(this);

                    //            if (m_RoutineState != value)
                    //            {
                    //                m_RoutineState = value;
                    //                OnChangeRoutineState(this, m_RoutineState);
                    //            }
                    //        }
                    //        break;
                    //    }
                    case _ROUTINE_STATE.RS_FAILED:
                        {
                            if (m_Log.IsDebugEnabled)
                                m_Log.Debug("Routine " + m_RoutineName + " state set to " + Tools.String_Enum.StringEnum.GetStringValue(value));

                            if (m_RoutineState != value)
                            {
                                m_RoutineState = value;
                                OnChangeRoutineState(this, m_RoutineState);
                            }

                            ButtonPreviousStepVisible = false;
                            ButtonContinueRoutinePerformanceVisible = false;
                            ButtonPauseRoutinePerformanceVisible = false;
                            ButtonStopRoutinePerformanceVisible = false;
                            ButtonNextStepVisible = false;

                            /*
                            if (this.GetType() == typeof(SingleWeldingRoutine))
                            {
                                ((SingleWeldingRoutine)this).CloseArgonFlow();
                            }
                            */
                            OnRoutineCompleted(this);
                            break;
                        }
                }
            }
        }

       
        [XmlIgnore]
        protected int m_RoutineIndex = -1;
        /// <summary>
        /// The Routine Index in Performance QUEUE
        /// </summary>
        [XmlIgnore]
        public Int32 RoutineIndex
        {
            get { return m_RoutineIndex; }
            set
            {
                m_RoutineIndex = value;
                UpdateChildsRoutineID();
            }
        }

        [XmlIgnore]
        protected String m_RoutineName;
        /// <summary>
        /// Current routine name
        /// </summary>
        [XmlElement("Routine_Name")]
        public String RoutineName
        {
            get { return m_RoutineName; }
            set
            {  
                m_RoutineName = value;
                RaisePropertyChanged("RoutineName");
            }
        }

       
       


        [XmlIgnore]
        protected int m_RoutineDataBaseID = -1;
        /// <summary>
        /// The Routine Database ID
        /// </summary>
        [XmlIgnore]
        public Int32 RoutineDataBaseID
        {
            get { return m_RoutineDataBaseID; }
            set { m_RoutineDataBaseID = value; }
        }

        [XmlIgnore]
        protected StepsCollection m_StepColl;
        /// <summary>
        /// Store the step collection for current routine.
        /// The "Set" method used only for XML modules. Please do not use this method directly
        /// </summary>
        [XmlElement("Step")]
        public StepsCollection StepColl
        {
            get { return m_StepColl; }

            set
            {
                if (null == value)
                    return;
            }
        }

        [XmlIgnore]
        protected Boolean m_RoutineStepInProgress;
        /// <summary>
        /// Indicate that step is taken to performance and routine
        /// is waiting to step complete event
        /// </summary>
        [XmlIgnore]
        public Boolean RoutineStepInProgress
        {
            get { return m_RoutineStepInProgress; }
            set { m_RoutineStepInProgress = value; }
        }

        [XmlIgnore]
        protected int m_SelectedStepIndex = -1;
        /// <summary>
        /// Get/Set the currently performing step Index
        /// </summary>
        [XmlIgnore]
        public int SelectedStepIndex
        {
            get { return m_SelectedStepIndex; }
            private set
            {
                if (value < -1)
                    value = -1;
                
                m_SelectedStepIndex = value;
                RaisePropertyChanged("SelectedStepIndex");
                RaisePropertyChanged("ActualStepName");

                if (m_SelectedStepIndex >= StepColl.Count)
                    RoutineState = _ROUTINE_STATE.RS_COMPLETED;
                else
                    OnRoutineStepChanged(this, this.SelectedNode);
            }
        }

        /// <summary>
        /// Return the currently performing step
        /// </summary>
        [XmlIgnore]
        public  BaseStepDescriptor SelectedNode
        {
            get
            {
                if (m_SelectedStepIndex >= 0)
                    return (StepColl[m_SelectedStepIndex]);
                else
                    return null;
            }
        }

        /// <summary>
        /// Current routine name
        /// </summary>
        [XmlIgnore]
        public virtual String ActualStepName
        {
            get
            {
                if (m_SelectedStepIndex >= 0 && m_SelectedStepIndex < StepColl.Count )
                    return (StepColl[m_SelectedStepIndex].StepName);
                else
                    return "Routine Is Not Active";
            }
        }
        

        [XmlIgnore]
        protected short m_RoutineErrorCode = ErrorCodesList.OK;
        /// <summary>
        /// Indicate the Error state of current routine performance
        /// </summary>
        [XmlIgnore]
        public short RoutineErrorCode
        {
            get { return m_RoutineErrorCode; }
            set { m_RoutineErrorCode = value; }
        }

        [XmlIgnore]
        protected String m_RoutineErrorMessage = "";
        /// <summary>
        /// Indicate the Error message of current routine performance
        /// </summary>
        [XmlIgnore]
        public String RoutineErrorMessage
        {
            get { return m_RoutineErrorMessage; }
            set { m_RoutineErrorMessage = value; }
        }

        [XmlIgnore]
        private Boolean m_IncreeseStepIndex = true;
        /// <summary>
        /// Set/Get the value indicate if Routine performance thread needs to change the Step index
        /// </summary>
        [XmlIgnore]
        public Boolean IncreeseStepIndex
        {
            get { return m_IncreeseStepIndex; }
            set { m_IncreeseStepIndex = value; }
        }

        [XmlIgnore]
        private Boolean m_IsSubRoutine = false;
        [XmlIgnore]
        public Boolean IsSubRoutine
        {
            get { return m_IsSubRoutine; }
            set { m_IsSubRoutine = value; }
        }

        #endregion Public Properties

        #region Public Functions
 
        /// <summary>
        /// Add steps collection to routine
        /// </summary>
        /// <param name="inStepColl"></param>
        public void AddStepsList(StepsCollection inStepColl)
        {
            m_StepColl.Clear();
            m_StepColl = inStepColl;
        }

        /// <summary>
        /// Moves the selected step index to specified value
        /// </summary>
        /// <param name="inStepIndex">The required step index</param>
        /// <param name="IncrementValue">The direction for searching the first Enable step in the list</param>
        /// <param name="SkipNotEnabled">Do the routine has to skip the disabled steps</param>
        public void SetStepIndex(Int32 inStepIndex, Int32 IncrementValue, Boolean SkipNotEnabled)
        {
            Boolean bFound = false;
            if (inStepIndex < 0) inStepIndex = 0;
            if (inStepIndex > StepColl.Count) inStepIndex = StepColl.Count;

            SelectedStepIndex = inStepIndex;
            if (SkipNotEnabled)
            {
                while (((m_SelectedStepIndex < StepColl.Count && IncrementValue > 0) ||
                        (m_SelectedStepIndex > 0 && IncrementValue < 0)) && !bFound)
                {
                    if (m_RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS ||
                        m_RoutineState == _ROUTINE_STATE.RS_PAUSED)
                    //||m_RoutineState == _ROUTINE_STATE.RS_IM_WAITING)
                    {
                        if (SelectedNode.StepEnabled )
                            bFound = true;
                        else
                            SelectedStepIndex += IncrementValue;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public void SetStepDatabaseIndex(Int32 inStepDatabaseIndex, Int32 IncrementValue, Boolean SkipNotEnabled)
        {
            Boolean bFound = false;

            int i = 0;
            foreach (BaseStepDescriptor Step in m_StepColl)
            {
                if (Step.RoutineDatabaseStepIndex == inStepDatabaseIndex)
                {
                    SelectedStepIndex = i;
                    break;
                }
                i++;
            }

            if (SkipNotEnabled)
            {
                while (((m_SelectedStepIndex < StepColl.Count && IncrementValue > 0) ||
                        (m_SelectedStepIndex > 0 && IncrementValue < 0)) && !bFound)
                {
                    if (m_RoutineState == _ROUTINE_STATE.RS_IN_PROGRESS ||
                        m_RoutineState == _ROUTINE_STATE.RS_PAUSED )
                    //||m_RoutineState == _ROUTINE_STATE.RS_IM_WAITING)
                    {
                        if (SelectedNode.StepEnabled)
                            bFound = true;
                        else
                            SelectedStepIndex += IncrementValue;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public virtual Boolean AddSingleStep(BaseStepDescriptor step, Boolean bAddStepEvents)
        {
            try
            {
                if (bAddStepEvents)
                {
                    SetStepsEvents(step);
                }
                m_StepColl.Add(step);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void Step_StepCompleted(object Sender, StepEventArguments args)
        {
            //((BaseStepDescriptor)Sender).StepCompleted -= Step_StepCompleted;
            //((BaseStepDescriptor)Sender).FailedToReachTargetPosition -= Step_FailedToReachTargetPosition;

            //lock (SynchronizationObject)
            //{
            OnStepCompleted(this);

            if (args.StepDescriptor.StepErrorCode != ErrorCodesList.OK)
            {
                m_RoutineErrorCode = args.StepDescriptor.StepErrorCode;
                RoutineState = _ROUTINE_STATE.RS_FAILED;
                return;
            }

            RoutineStepInProgress = false;
            //}
        }

        protected virtual void Step_UpdateGUIActionCompleted(object Sender, StepEventArguments args)
        {
        }

        protected virtual void Step_UpdateGUIStepCompleted(object Sender, StepEventArguments args)
        {
        }
 
        protected virtual void Step_PerformanceErrorOccuerd(object Sender, EventArgs args)
        {
        }
         
        #region Routine State Control Functions

        public virtual void ucRoutinePlayer_PauseRoutine(object Sender, EventArgs args)
        {
            OnPauseRoutine(this, args);
        }

        public virtual void ucRoutinePlayer_PlayRoutine(object Sender, EventArgs args)
        {
            OnPlayRoutine(this, args);
        }

        public virtual void ucRoutinePlayer_StopRoutine(object Sender, EventArgs args)
        {
            OnStopRoutine(this, args);
        }

        public void ucRoutinePlayer_Jump2NextStep(object Sender, EventArgs args)
        {
            OnJump2NextStep(this, args);
        }

        public void ucRoutinePlayer_Jump2PreviousStep(object Sender, EventArgs args)
        {
            OnJump2PreviousStep(this, args);
        }

        #endregion

        #endregion Public Functions

        #region Public functions

        public virtual short UpdateChildsRoutineID()
        {
            foreach (BaseStepDescriptor step in m_StepColl)
            {
                short local_result = step.SetRoutineIndex(this.RoutineIndex);
            }
            return NS_Common.ErrorCodesList.OK;
        }

        public virtual short RemoveActionsDevices(out String error_message)
        {
            short result = NS_Common.ErrorCodesList.OK;
            String res_message = "";
            foreach (BaseStepDescriptor step in m_StepColl)
            {
                String local_res_message = "";
                short local_result = step.RemoveActionsDevices(out local_res_message);
                if (NS_Common.ErrorCodesList.OK != local_result)
                {
                    res_message += local_res_message + "\n\r";
                    result = local_result;
                }
            }

            error_message = res_message;
            return result;
        }
         
        public virtual short SetActionsDevices(Dictionary<string, IComponent> DeviceList, out String error_message)
        {
            short result = NS_Common.ErrorCodesList.OK;
            String res_message = "";
            
            foreach (BaseStepDescriptor step in m_StepColl)
            {
                String local_res_message = "";
                short local_result = step.SetActionsDevices(DeviceList, out local_res_message);
                if (NS_Common.ErrorCodesList.OK != local_result)
                {
                    res_message += local_res_message + "\n\r";
                    result = local_result;
                }
            }

            error_message = res_message;
            return result;
        }

        /// <summary>
        /// Stop all actions performance. In case it's movement stops throw motion controller.
        /// In case it's laser stop the laser action
        /// </summary>
        public virtual void StopActionsPerformance()
        {
            if (SelectedNode != null)
                SelectedNode.StopActionsPerformance();
        }
 
        public override string ToString()
        {
            return this.GetType() + " , Routine State --> " + Tools.String_Enum.StringEnum.GetStringValue(this.RoutineState);
        }
         
        /// <summary>
        /// Start routine performing
        /// </summary>
        /// <returns></returns>
        public virtual Boolean StartRoutinePerformance()
        {
            RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;

            return true;
        }
 
        #endregion Virtual and Abstract funcions

        #region Abstract methods
        public abstract Boolean ContinueRoutine();

        public abstract void SetStepsEvents();

        protected abstract void SetStepsEvents(BaseStepDescriptor SingleStep);
             
        public abstract BaseRoutineDescriptor Clone();

        public abstract void RemoveEventsHandlers();
        #endregion

        #region Protected virtual Methods

        protected virtual void PerformRoutineStoppedInMiddleMethods()
        {
            for (Int32 i = 0; i < m_SelectedStepIndex; i++)
            {
                StepColl[i].PerformRoutineStoppedInMiddleMethods();
            }
        }


        protected virtual void ContinueRoutinePerform(object routine)
        {
            RoutineState = _ROUTINE_STATE.RS_IN_PROGRESS;  
        }

        protected virtual void SetPreviousStep(object routine)
        {   
            SelectedStepIndex--;
        }

        protected virtual void PauseRoutinePerform(object routine)
        {
            RoutineState = _ROUTINE_STATE.RS_PAUSED;  
        }

        protected virtual void StopRoutinePerform(object routine)
        {
            RoutineState = _ROUTINE_STATE.RS_READY_TO_START;  
        }

        protected virtual void SetNextStep(object routine)
        {
            SelectedStepIndex++;
        }

       
        #endregion
    }
}