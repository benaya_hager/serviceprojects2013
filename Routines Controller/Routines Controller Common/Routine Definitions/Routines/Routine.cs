﻿using System;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Routine_Definitions;
using NS_Common.ViewModels.Common;
using System.Windows;

namespace NS_Routines_Controller_Common.Routine_Definitions.Routines
{
	public class Routine : BaseRoutineDescriptor
	{ 
		#region Constructors

		public Routine() : this("Undefined Type Routine")
		{}

		public Routine(String routine_name)
			: base()
		{  
			this.RoutineName = routine_name; 
		}

		#endregion Constructors

		#region Virtual & Overridden functions

		


		public override bool ContinueRoutine()
		{
			return base.StartRoutinePerformance();
		}

		public override void SetStepsEvents()
		{
			foreach (BaseStepDescriptor tmpStep in m_StepColl)
			{
				SetStepsEvents(tmpStep);
			}
		}

		protected override void SetStepsEvents(BaseStepDescriptor SingleStep)
		{
			SingleStep.StepCompleted += new BaseStepDescriptor.StepCompletedHandler(Step_StepCompleted);
			SingleStep.PerformanceErrorOccuerd += new BaseStepDescriptor.PerformanceErrorOccuerdHandler(Step_PerformanceErrorOccuerd);
			SingleStep.SetStepEventsHandlers(this);
		}



		public override BaseRoutineDescriptor Clone()
		{
			Routine tmp = new Routine();
			tmp = (Routine)this.MemberwiseClone();
			tmp.AddStepsList((StepsCollection)this.StepColl.Clone(this));
			return tmp;
		}

		public override void RemoveEventsHandlers()
		{
			foreach (BaseStepDescriptor tmpStep in m_StepColl)
			{
				tmpStep.RemoveEventsHandlers();
				tmpStep.StepCompleted -= Step_StepCompleted;                
				tmpStep.PerformanceErrorOccuerd -= Step_PerformanceErrorOccuerd;
			}
		}

	   


		#endregion Virtual & Overrided functions

		#region Public Functions

	  
		#endregion Public Functions
	}
}