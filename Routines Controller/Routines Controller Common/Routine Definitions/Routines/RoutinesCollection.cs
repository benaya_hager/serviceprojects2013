﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Common;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using NS_Common.Service;
using NS_Routines_Controller_Common.Service;

namespace NS_Routines_Controller_Common.Routine_Definitions.Routines
{
    public class RoutinesCollection : AsyncTrulyObservableCollection<BaseRoutineDescriptor>  // ObservableDictionary<Int32, BaseRoutineDescriptor>
    {
        #region Private members
            


            private BaseRoutineDescriptor GetRoutineByKey(int lIndex)
            {
                foreach (BaseRoutineDescriptor routine in this)
                    if (routine.RoutineIndex == lIndex)
                        return routine;
                return null;
            }
             

        #endregion

        #region Public Functions
            /// <summary>
            /// Return first free index from available in the list 
            /// </summary>
            /// <returns>First available position index in the list</returns>
            public int GetFreeIndex()
            {
                int lIndex = 1;
                while (true)
                {
                    if (!this.ContainsKey(lIndex))
                        break;
                    lIndex++;
                }
                return lIndex;
            }

            public bool ContainsKey(int lIndex)
            {
                foreach (BaseRoutineDescriptor routine in this)
                    if (routine.RoutineIndex == lIndex)
                        return true;
                return false;
            }


            //Get/Set the element at specific Index
            /// <summary>
            /// Get/Set the element at specific Index
            /// </summary>
            /// <param name="index">The zero-based index of the element</param>
            /// <returns>The element stored in the List</returns>
            public new BaseRoutineDescriptor this[int index]
            {
                get
                {
                    return GetRoutineByKey(index);
                }
                set
                {
                    //if (this.ContainsKey(index))
                        //this.Remove(index);
                    //var res = GetRoutineByKey(index);
                    //if (null != res)
                    Remove(GetRoutineByKey(index)); 
                    base.Add(value);
                }
            }

            /// <summary>
            /// Add Routine based on  SortedList collection, 
            /// and store direct access parameter
            /// </summary>
            /// <param name="item">Handle of a SingleRoutine to add</param>
            /// <returns>Return my direct access parameter</returns> 
            public new Int32 Add(BaseRoutineDescriptor item)
            {
                Int32 m_Index = GetFreeIndex();
                item.RoutineIndex = m_Index;
                base.Add(item);
                return m_Index;
            }

            /// <summary>
            /// Find item from  SortedList collection by RoutineIndex created in Add process
            /// </summary>
            /// <param name="item">Routine to remove</param>
            public new  void Remove(BaseRoutineDescriptor item)
            {
                if (this.ContainsKey(item.RoutineIndex))
                {
                    OnBeforeRoutineRemove(this, new RoutineEventArguments( item)); 

                    base.Remove(item);
                }
            }

            /// <summary>
            /// Find item from  SortedList collection by RoutineIndex created in Add process
            /// </summary>
            /// <param name="Index">Routine Index to remove</param>
            public void Remove(Int32 Index)
            {
                if (this.ContainsKey(Index))
                {
                    OnBeforeRoutineRemove(this, new RoutineEventArguments(this[Index]));
                    this[Index].RemoveEventsHandlers();

                    base.Remove(GetRoutineByKey(Index));
                    //base.Remove(Index);
                }
            }

            public new void Clear()
            {
                foreach (BaseRoutineDescriptor tmpRoutine in this)
                {
                    OnBeforeRoutineRemove(this, new RoutineEventArguments(tmpRoutine));
                }
                base.Clear(); 
            }
        #endregion
        
        #region Public events delegate functions
            #region Before Routine Remove
                /// <summary>
                /// The events delegate function. Occurs when routine is going to be removed from performance loop.
                /// </summary>
                /// <param name="Sender">The control that raised event handler </param>
                /// <param name="args"></param>
                public delegate void BeforeRoutineRemoveHandler(Object Sender, RoutineEventArguments args);
                public event BeforeRoutineRemoveHandler BeforeRoutineRemove;
                protected void OnBeforeRoutineRemove(object Sender, RoutineEventArguments args)
                {
                    if (BeforeRoutineRemove != null)
                        BeforeRoutineRemove(Sender, args);
                }
            #endregion
        #endregion
    }
}
