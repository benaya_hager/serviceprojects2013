﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Routine_Definitions.Steps;
using SFW;

namespace NS_Routines_Controller_Common.Routine_Definitions.Actions
{
    public class SingleRoutineStateChangeAction : BaseActionDescriptor
    {
        #region Private members

        /// <summary>
        /// Indicates if action first called during actual run of routine
        /// </summary>
        [XmlIgnore]
        private Boolean m_ChangeRoutineStateStateSet = false;

        #endregion Private members

        #region Public events delegate functions

        #region Change Routine State Event

        /// <summary>
        /// Define an event handler delegate
        /// The events delegate function. Occurs when Routine state needs to be change.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void ChangeRoutineStateHandler(Object Sender, _ROUTINE_STATE arg);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private ChangeRoutineStateHandler ChangeRoutineStateDelegate;

        protected void OnChangeRoutineState(object Sender, _ROUTINE_STATE arg)
        {
            if (this.ChangeRoutineStateDelegate != null)
                this.ChangeRoutineStateDelegate(Sender, arg);
        }

        /// <summary>
        /// re-define the ChangeRoutineState event
        /// </summary>
        public event ChangeRoutineStateHandler ChangeRoutineState
        {
            // this is the equivalent of ChangeRoutineState += new EventHandler(...)
            add
            {
                //if (this.ChangeRoutineStateDelegate != null)
                //{
                //    ChangeRoutineState -= this.ChangeRoutineStateDelegate;
                //    this.ChangeRoutineStateDelegate = null;
                //}
                if (this.ChangeRoutineStateDelegate == null || !this.ChangeRoutineStateDelegate.GetInvocationList().Contains(value))
                {
                    this.ChangeRoutineStateDelegate += value;
                }
            }
            // this is the equivalent of ChangeRoutineState -= new EventHandler(...)
            remove
            {
                this.ChangeRoutineStateDelegate -= value;
            }
        }

        #endregion Change Routine State Event

        #endregion Public events delegate functions

        #region Constructors

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public SingleRoutineStateChangeAction()
            : base()
        {
            m_ChangeRoutineStateStateSet = false;
        }

        /// <summary>
        /// Initialize the class with values
        /// </summary>
        /// <param name="inActionName">Action name</param>
        public SingleRoutineStateChangeAction(String inActionName)
            : this()
        {
            m_ActionName = inActionName;
        }

        public SingleRoutineStateChangeAction( _ROUTINE_STATE State)
            : this("Set Routine state to - " + Tools.String_Enum.StringEnum.GetStringValue(State))
        {
            m_RoutineState = State;
        }

        #endregion Constructors

        #region Public Properties

        [XmlIgnore]
        private  _ROUTINE_STATE m_RoutineState;

        [XmlIgnore]
        public  _ROUTINE_STATE RoutineState
        {
            get { return m_RoutineState; }
            set { m_RoutineState = value; }
        }

        #endregion Public Properties

        #region Public overridden functions

        public override short SetMyDevices(Dictionary<string, IComponent> DeviceList, out String message)
        {
            message = "";
            return NS_Common.ErrorCodesList.OK;
        }

        public override short RemoveMyDevices(out string error_message)
        {
            error_message = "";
            return NS_Common.ErrorCodesList.OK;
        }

        
        

        public override void SetActionEventsHandlers(BaseRoutineDescriptor parent_routine, Base_Classes.BaseStepDescriptor ParentStep)
        {
            return;
        }

        public override string GetShortDescription()
        {
            return "Routine State Change Action";
        }

        public override string GetFullDescription()
        {
            return "Routine State Change Action";
        }

        #endregion Public overridden functions

        #region Private Functions

        /// <summary>
        /// The Welding trigger thread ThreadStart entrance point.
        /// </summary>
        protected override void ThreadFunction(object token)
        {
            try
            {
                bool ChangeRoutineState = false;

                //Copy all necessary values to internal thread variables
                lock (m_SynchronizationObject)
                {
                    ChangeRoutineState = m_ChangeRoutineStateStateSet;
                }

                //If this is the first call to action performance generate the change action state event.
                //Otherwise perform the continue to next task
                if (!ChangeRoutineState)
                {
                    lock (m_SynchronizationObject)
                    {
                        m_ChangeRoutineStateStateSet = true;
                    }

                    OnChangeRoutineState(this, m_RoutineState);
                    return;
                }
                else
                {
                    lock (m_SynchronizationObject)
                    {
                        m_ChangeRoutineStateStateSet = false;
                    }
                }

            }
            catch (ThreadAbortException exc)
            {
                if (m_SystemLog.IsDebugEnabled)
                    m_SystemLog.Debug("Change routine state Action thread aborting, code is " + exc.ExceptionState);
            }
            finally
            {
                OnActionCompleted(this);
            }
        }

        #endregion Private Functions
    }
}