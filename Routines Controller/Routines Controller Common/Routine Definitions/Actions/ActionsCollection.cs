using System.Linq;
using System.Collections.ObjectModel;
using System;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Service;
using NS_Common.Service;

namespace NS_Routines_Controller_Common.Routine_Definitions.Actions
{
    /// <summary>
    /// Store the list of Actions for Single Step
    /// </summary>
    public class ActionsCollection : AsyncTrulyObservableCollection<BaseActionDescriptor> 
    {
        #region Public functions

        /// <summary>
        /// Copy all collection items to new reference
        /// </summary>
        /// <returns></returns>
        public ActionsCollection Clone()
        {
            ActionsCollection NewCollection = new ActionsCollection();
            foreach (BaseActionDescriptor tmpAction in this)
            {
                BaseActionDescriptor newAction = (BaseActionDescriptor)tmpAction.Clone();
                NewCollection.Add(newAction);
            }
            return NewCollection;
                

        }

        public bool ContainsType(Type type)
        {
            ObservableCollection<BaseActionDescriptor> col = this.Where(x => x.GetType() == type) as ObservableCollection<BaseActionDescriptor>;
            return ((null != col) && (col.Count > 0));
        }
         
        ///// Get/Set the element at specific Index
        ///// </summary>
        ///// <param name="index">The zero-based index of the element</param>
        ///// <returns>The element stored in the List</returns>
        //public BaseActionDescriptor this[int index]
        //{
        //    get { return ((BaseActionDescriptor)List[index]); }
        //    set { List[index] = value; }
        //}

        ////Adds an item to the ActionsCollection.
        ///// <summary>
        /////  Adds an item to the ActionsCollection.
        ///// </summary>
        ///// <param name="NewAction">Creates a shallow copy of the current NewAction.</param>
        ///// <returns>The position into which the new element was inserted.</returns>
        //public int Add(BaseActionDescriptor NewAction)
        //{
        //    return List.Add(NewAction);
        //}

        ////public BaseActionDescriptor FindActionByAxisID(int AxisNetworkID)
        ////{
        ////    foreach (BaseActionDescriptor Action in List)
        ////    {
        ////        if (Action.GetType() == typeof(SingleFoldingMotionAction))
        ////            if (((SingleFoldingMotionAction)Action).Axis.NetworkIdentifier == AxisNetworkID)
        ////                return Action;
        ////    }
        ////    return null;
        ////}

        ////Find BaseActionDescriptor in ActionsCollection list
        ///// <summary>
        ///// Determines the index of a specific item in the
        ///// </summary>
        ///// <param name="value">The BaseActionDescriptor to locate in the ActionsCollection.</param>
        ///// <returns></returns>
        //public int IndexOf(BaseActionDescriptor value)
        //{
        //    return (List.IndexOf(value));
        //}

        ////Insert New BaseActionDescriptor to BaseActionDescriptor List
        ///// <summary>
        ///// Inserts an BaseActionDescriptor item to the ActionsCollection at the specified index.
        ///// </summary>
        ///// <param name="index">The zero-based index at which value should be inserted.</param>
        ///// <param name="value">The BaseActionDescriptor to insert into the ActionsCollection.</param>
        //public void Insert(int index, BaseActionDescriptor value)
        //{
        //    List.Insert(index, value);
        //}

        ////Insert New BaseActionDescriptor to ActionsCollection List
        ///// <summary>
        /////  Replace the ActionsCollection item at the specified index.
        ///// </summary>
        ///// <param name="index">The zero-based index of the item to be replaced.</param>
        ///// <param name="value">The new BaseActionDescriptor to be placed over the old one in the ActionsCollection.</param>
        //public void Replace(int index, BaseActionDescriptor value)
        //{
        //    List.RemoveAt(index);
        //    List.Insert(index, value);
        //}

        ////Remove BaseActionDescriptor from BaseActionDescriptor list
        ///// <summary>
        ///// Removes the first occurrence of a specific BaseActionDescriptor object from the ActionsCollection.
        ///// </summary>
        ///// <param name="value">The BaseActionDescriptor Object to remove from the ActionsCollection.</param>
        //public void Remove(BaseActionDescriptor value)
        //{
        //    List.Remove(value);
        //}

        ////Check if tile exist in BaseActionDescriptor list array
        ///// <summary>
        ///// Determines whether the ActionsCollection contains a specific BaseActionDescriptor value.
        ///// </summary>
        ///// <param name="value"> The BaseActionDescriptor to locate in the ActionsCollection.</param>
        ///// <returns>
        ///// true if the BaseActionDescriptor Object is found in the ActionsCollection; otherwise,false.
        ///// </returns>
        //public bool Contains(BaseActionDescriptor value)
        //{
        //    // If value is not of type BaseActionDescriptor, this will return false.
        //    return (List.Contains((BaseActionDescriptor)value));
        //}

        #endregion Public functions

        #region Private/Protected Functions

        ////Check if item inserted is of right type
        ///// <summary>
        ///// Check if item inserted is of right type
        ///// </summary>
        ///// <param name="value">Added Item, if the type is wrong throw exception</param>
        //protected override void OnValidate(Object value)
        //{
        //    if (value.GetType().BaseType != typeof(BaseActionDescriptor))
        //        throw new ArgumentException("value must be of type BaseActionDescriptor.", "value");
        //}

        #endregion Private/Protected Functions

       
    }
}