using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using SFW;


namespace NS_Routines_Controller_Common.Routine_Definitions.Actions
{
    public class SingleWaitAction : BaseActionDescriptor
    {
        #region Private Members

        #endregion Private Members

        #region Constructors

        public SingleWaitAction() :
            base()
        {
            m_ActionName = "Wait Action";
        }

        /// <summary>
        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inActionName">The Action name</param>
        /// <param name="WaitingTimeout">The waiting time in milliseconds</param>
        public SingleWaitAction(Int32 inWaitingTimeout)
            : this()
        {
            WaitingTimeout = inWaitingTimeout;
        }

        /// <summary>
        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inActionName">The Action name</param>
        /// <param name="WaitingTimeout">The waiting time in milliseconds</param>
        public SingleWaitAction(String inActionName, Int32 inWaitingTimeout)
            : this(inWaitingTimeout)
        {
            ActionName = inActionName;
        }

        #endregion Constructors

        #region Public properties

        [XmlIgnore]
        private int m_WaitingTimeout;

        /// <summary>
        /// Get/Set the waiting timeout in milliseconds
        /// </summary>
        [XmlElement("Waiting_Timeout")]
        public int WaitingTimeout
        {
            get { return m_WaitingTimeout; }
            set
            {
                m_WaitingTimeout = value;
                m_ActionName = "Wait Action  " + m_WaitingTimeout.ToString() + " Milliseconds";
            }
        }

        #endregion Public properties

        #region Public overridden functions

        public override short SetMyDevices(Dictionary<string, IComponent> DeviceList, out String message)
        {
            message = "";
            return NS_Common.ErrorCodesList.OK;
        }

        public override short RemoveMyDevices(out string error_message)
        {
            error_message = "";
            return NS_Common.ErrorCodesList.OK;
        }

        //public override void SetActionCompletedHandler(BaseStepDescriptor ParentStep)
        //{
        //    ActionStateCompleted = false;

        //    ActionCompleted += new BaseActionDescriptor.ActionCompletedHandler(ParentStep.Action_ActionCompleted);
        //}

        //public override void RemoveEventsHandlers(BaseStepDescriptor ParentStep)
        //{
        //    // ActionCompleted -= ParentStep.Action_ActionCompleted;
        //}

        

        public override string GetShortDescription()
        {
            return "Wait Action";
        }

        public override string GetFullDescription()
        {
            return "Wait Preset timeout (ms) Action";
        }

        #endregion Public overridden functions

        #region Private Functions

        /// <summary>
        ///
        /// </summary>
        protected override void ThreadFunction(object token)
        {

            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();
            try
            {
                if (m_WaitingTimeout < 5000)
                {
                    System.Threading.Thread.Sleep(m_WaitingTimeout);
                }
                else
                {
                    while (true)
                    {
                        if (m_WaitingTimeout >= 0 && (sw.Peek() / 10.0)/*Milliseconds*/ < m_WaitingTimeout)
                        {
                            System.Threading.Thread.Sleep(10);
                            if (((CancellationToken)token).IsCancellationRequested) 
                                break;
                        }
                        else
                            break;
                    }

                }

            }
            catch (ThreadAbortException exc)
            {
                if (m_SystemLog.IsDebugEnabled)
                    m_SystemLog.Debug("Transformation Measuring Action thread aborting, code is " + exc.ExceptionState);
            }
            finally
            {
                if (!((CancellationToken)token).IsCancellationRequested) 
                    OnActionCompleted(this);
            }
        }

        #endregion Private Functions
    }
}