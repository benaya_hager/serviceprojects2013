﻿using System;
using System.Linq;
using System.Threading;
using NS_Routines_Controller_Common.Routine_Definitions.Actions;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Routines_Controller_Common.Service;

namespace NS_Routines_Controller_Common.Routine_Definitions.Steps
{
    public class Step : BaseStepDescriptor
    {
        #region Constructors

        public Step()
            : base()
        { }

        public Step(String inStepName)
            : base(inStepName, true)

        { }

        /// <summary>
        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inStepName">The step name</param>
        public Step(String inStepName, ActionsCollection inActionsCol)
            : this(inStepName)
        {
            //Copy received actions collection for this step, to local variable
            ActionsCol = inActionsCol;
        }


        #endregion Constructors

        #region Public Virtual functions

        /// <summary>
        /// Copy all Step members to new reference
        /// </summary>
        /// <returns></returns>
        public Step Clone()
        {
            return (Step)this.MemberwiseClone();
        }

        public override void PerformStep(BaseRoutineDescriptor ParentRoutine)
        {
            myParentRoutine = ParentRoutine;

            m_StepPerformanceThread = new Thread(new ThreadStart(StepPerformanceFunction));
            m_StepPerformanceThread.Name = "Step Performance thread( " + this.StepName + " )";
            m_StepPerformanceThread.IsBackground = true;
            m_StepPerformanceThread.SetApartmentState(ApartmentState.STA);
            m_StepPerformanceThread.Priority = ThreadPriority.Highest;
            m_StepPerformanceThread.Start();
        }

        public override bool StopActionsPerformance()
        {
            //base.StepInProgress = false; ;
            foreach (BaseActionDescriptor Action in m_ActionsCol)
            {
                Action.StopActionPerformance();
            }

            if (null != m_StepPerformanceThread)
                if (m_StepPerformanceThread.IsAlive)
                    if (m_StepPerformanceThread.IsBackground)
                        m_StepPerformanceThread.Abort();
            m_StepPerformanceThread = null;

            return true;
        }

        public override void Action_ActionCompleted(object Sender, EventArgs args)
        {
            //((BaseActionDescriptor)Sender).ActionCompleted -= Action_ActionCompleted;
            ((BaseActionDescriptor)Sender).ActionStateCompleted = true;


            Int32 CompletedActionsCounter = 0;
            Boolean AllStepActionsCompleted = true;
            foreach (BaseActionDescriptor Action in ActionsCol)
            {
                if (!Action.ActionStateCompleted)
                    AllStepActionsCompleted = false;
                else
                    CompletedActionsCounter++;
            }

            // OnUpdateGUIActionCompleted(this, CompletedActionsCounter);


            if (AllStepActionsCompleted && base.StepInProgress)
            {
                base.StepInProgress = false;
                if (m_SystemLog.IsDebugEnabled)
                    m_SystemLog.Debug("All Actions in step " + base.StepName + " are completed");
                m_StepErrorCode = NS_Common.ErrorCodesList.OK;
                //RemoveEventsHandlers();
                OnStepCompleted(this);
            }
        }

        public override void RemoveEventsHandlers()
        {
            foreach (BaseActionDescriptor Action in ActionsCol)
            {
                Action.RemoveEventsHandlers(this);
            }
        }

        public override void SetStepEventsHandlers(BaseRoutineDescriptor ParentRoutine)
        {
            foreach (BaseActionDescriptor Action in ActionsCol)
            {
                Action.SetActionEventsHandlers(ParentRoutine, this);
            }
        }
     
        #endregion Public Virtual functions

        #region Private Functions

        /// <summary>
        /// Step performance main thread
        /// </summary>
        protected override void StepPerformanceFunction()
        {
            try
            {
                base.StepInProgress = true;

                foreach (BaseActionDescriptor Action in ActionsCol)
                {
                    if (!Action.PerformAction())
                    {
                        Action.ActionStartFailed = true;
                        OnPerformanceErrorOccuerd(this, null);
                    }
                    else
                    {
                        Action.ActionStartFailed = false;
                    }
                }


                //foreach (BaseActionDescriptor Action in ActionsCol)
                //    if (!Action.ActionStartFailed)
                //        Action.SetActionCompletedHandler((BaseStepDescriptor)this);
            }
            catch (ThreadAbortException exc)
            {
                base.StepInProgress = false;
                StopActionsPerformance();
                Console.WriteLine("Thread aborting, code is " +
                 exc.ExceptionState);
            }
        }

        #endregion Private Functions

        #region Public events delegate functions

        #region Update GUI Step completed

        /// <summary>
        /// The events delegate function. All step actions completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void UpdateGUIStepCompletedHandler(Object Sender, StepEventArguments args);
        private event UpdateGUIStepCompletedHandler UpdateGUIStepCompletedDelegate;

        protected void OnUpdateGUIStepCompleted(object Sender)
        {
            if (this.UpdateGUIStepCompletedDelegate != null)
                this.UpdateGUIStepCompletedDelegate(Sender, new StepEventArguments((BaseStepDescriptor)Sender));
        }

        /// <summary>
        /// re-define the UpdateGUIStepCompleted event
        /// </summary>
        public event UpdateGUIStepCompletedHandler UpdateGUIStepCompleted
        {
            // this is the equivalent of UpdateGUIStepCompleted += new EventHandler(...)
            add
            {
                //if (this.UpdateGUIStepCompletedDelegate != null)
                //{
                //    UpdateGUIStepCompleted -= this.UpdateGUIStepCompletedDelegate;
                //    this.UpdateGUIStepCompletedDelegate = null;
                //}

                if (this.UpdateGUIStepCompletedDelegate == null || !this.UpdateGUIStepCompletedDelegate.GetInvocationList().Contains(value))
                {
                    this.UpdateGUIStepCompletedDelegate += value;
                }
            }
            // this is the equivalent of UpdateGUIStepCompleted -= new EventHandler(...)
            remove
            {
                this.UpdateGUIStepCompletedDelegate -= value;
            }
        }

        #endregion Update GUI Step completed

        #region Update GUI Action completed

        /// <summary>
        /// The events delegate function. All step actions completed.
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void UpdateGUIActionCompletedHandler(Object Sender, StepEventArguments args);
        private event UpdateGUIActionCompletedHandler UpdateGUIActionCompletedDelegate;

        protected void OnUpdateGUIActionCompleted(object Sender, Int32 inCompletedActionsCounter)
        {
            if (this.UpdateGUIActionCompletedDelegate != null)
                this.UpdateGUIActionCompletedDelegate(Sender, new StepEventArguments((BaseStepDescriptor)Sender, inCompletedActionsCounter));
        }

        /// <summary>
        /// re-define the UpdateGUIActionCompleted event
        /// </summary>
        public event UpdateGUIActionCompletedHandler UpdateGUIActionCompleted
        {
            // this is the equivalent of UpdateGUIActionCompleted += new EventHandler(...)
            add
            {
                //if (this.UpdateGUIActionCompletedDelegate != null)
                //{
                //    UpdateGUIActionCompleted -= this.UpdateGUIActionCompletedDelegate;
                //    this.UpdateGUIActionCompletedDelegate = null;
                //}
                if (this.UpdateGUIActionCompletedDelegate == null || !this.UpdateGUIActionCompletedDelegate.GetInvocationList().Contains(value))
                {
                    this.UpdateGUIActionCompletedDelegate += value;
                }
            }
            // this is the equivalent of UpdateGUIActionCompleted -= new EventHandler(...)
            remove
            {
                this.UpdateGUIActionCompletedDelegate -= value;
            }
        }

        #endregion Update GUI Action completed

        #endregion Public events delegate functions
    }
}