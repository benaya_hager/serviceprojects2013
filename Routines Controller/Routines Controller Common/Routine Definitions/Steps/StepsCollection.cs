using System;
using System.Collections;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System.Collections.ObjectModel;
using System.Linq;
using NS_Routines_Controller_Common.Service;
using NS_Common.Service;
using System.ComponentModel;

namespace NS_Routines_Controller_Common.Routine_Definitions
{
    public class StepsCollection : AsyncTrulyObservableCollection<BaseStepDescriptor> , INotifyPropertyChanged
          
    {
        #region Public functions

        /// <summary>
        /// Copy all collection items to new reference
        /// </summary>
        /// <returns></returns>
        public StepsCollection Clone(BaseRoutineDescriptor ParentRoutine)
        {
            //StepsCollection col = new StepsCollection();
            //col = (StepsCollection)this.Select(x => ((BaseStepDescriptor)x).Clone(ParentRoutine));
            //return col;
            StepsCollection NewCollection = new StepsCollection();
            foreach (BaseStepDescriptor tmpStep in this)
            {
                BaseStepDescriptor newStep = (BaseStepDescriptor)tmpStep.Clone(ParentRoutine);

                NewCollection.Add(newStep);
            }
            return NewCollection;
        }

        ////Get/Set the element at specific Index
        ///// <summary>
        ///// Get/Set the element at specific Index
        ///// </summary>
        ///// <param name="index">The zero-based index of the element</param>
        ///// <returns>The element stored in the List</returns>
        //public BaseStepDescriptor this[int index]
        //{
        //    get { return ((BaseStepDescriptor)List[index]); }
        //    set { List[index] = value; }
        //}

        ////Adds an item to the StepsCollection.
        ///// <summary>
        /////  Adds an item to the StepsCollection.
        ///// </summary>
        ///// <param name="NewAction">Creates a shallow copy of the current NewAction.</param>
        ///// <returns>The position into which the new element was inserted.</returns>
        //public int Add(BaseStepDescriptor NewStep)
        //{
        //    //BaseStepDescriptor newStep =  ;//.Clone();
        //    return List.Add((BaseStepDescriptor)NewStep);
        //}

        ////Find BaseStepDescriptor in StepsCollection list
        ///// <summary>
        ///// Determines the index of a specific item in the
        ///// </summary>
        ///// <param name="value">The BaseStepDescriptor to locate in the StepsCollection.</param>
        ///// <returns></returns>
        //public int IndexOf(BaseStepDescriptor value)
        //{
        //    return (List.IndexOf(value));
        //}

        ////Insert New BaseStepDescriptor to BaseStepDescriptor List
        ///// <summary>
        ///// Inserts an BaseStepDescriptor item to the StepsCollection at the specified index.
        ///// </summary>
        ///// <param name="index">The zero-based index at which value should be inserted.</param>
        ///// <param name="value">The BaseStepDescriptor to insert into the StepsCollection.</param>
        //public void Insert(int index, BaseStepDescriptor value)
        //{
        //    List.Insert(index, value);
        //}

        ///// <summary>
        /////  Replace the StepsCollection item at the specified index.
        ///// </summary>
        ///// <param name="index">The zero-based index of the item to be replaced.</param>
        ///// <param name="value">The new BaseStepDescriptor to be placed over the old one in the StepsCollection.</param>
        //public void Replace(int index, BaseStepDescriptor value)
        //{
        //    List.RemoveAt(index);
        //    List.Insert(index, value);
        //}

        ////Remove BaseStepDescriptor from BaseStepDescriptor list
        ///// <summary>
        ///// Removes the first occurrence of a specific BaseStepDescriptor object from the StepsCollection.
        ///// </summary>
        ///// <param name="value">The BaseStepDescriptor Object to remove from the StepsCollection.</param>
        //public void Remove(BaseStepDescriptor value)
        //{
        //    try
        //    {
        //        List.Remove(value);
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }
        //}

        //Check if tile exist in BaseStepDescriptor list array
        /// <summary>
        /// Determines whether the StepsCollection contains a specific BaseStepDescriptor value.
        /// </summary>
        /// <param name="value"> The BaseStepDescriptor to locate in the StepsCollection.</param>
        /// <returns>
        /// true if the BaseStepDescriptor Object is found in the StepsCollection; otherwise,false.
        /// </returns>
        public new bool Contains(BaseStepDescriptor value)
        {
            // If value is not of type BaseStepDescriptor, this will return false.
            return (base.Contains((BaseStepDescriptor)value));
        }

        #endregion Public functions

        #region Private/Protected Functions

        //Check if item inserted is of right type
        /// <summary>
        /// Check if item inserted is of right type
        /// </summary>
        /// <param name="value">Added Item, if the type is wrong throw exception</param>
        //protected override  void   OnValidate(Object value)
        //{
        //    if (value.GetType().BaseType != typeof(BaseStepDescriptor))
        //        throw new ArgumentException("value must be of type BaseStepDescriptor.", "value");
        //}

        #endregion Private/Protected Functions
    }
}