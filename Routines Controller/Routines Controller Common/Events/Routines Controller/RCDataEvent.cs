﻿using SFW;

namespace NS_Routines_Controller_Common.Events.Routines_Controller
{
    public class RCDataEvent : IEventData
    {
        #region Public Properties

        private RCDataEventType m_DataType;

        public RCDataEventType DataType
        {
            get { return m_DataType; }
            private set { m_DataType = value; }
        }

        private RCDataEventParameters.RCDataEventParameters m_DataParameters;
        public RCDataEventParameters.RCDataEventParameters DataParameters
        {
            get { return m_DataParameters; }
            private set { m_DataParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataEvent(RCDataEventType data_type, RCDataEventParameters.RCDataEventParameters prms)
        {
            m_DataType = data_type;
            m_DataParameters = prms;
        }

        #endregion Constructors
    }
}