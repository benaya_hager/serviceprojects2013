﻿using SFW;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters;

namespace NS_Routines_Controller_Common.Events.Routines_Controller
{
    /// <summary>
    /// Defines default command event structure
    /// </summary>
    public class RCCommandEvent : IEventData
    {
        #region Public Properties

        private RCCommandType m_cmdType;

        public RCCommandType CmdType
        {
            get { return m_cmdType; }
            private set { m_cmdType = value; }
        }

        private RCCommandEventParameters m_CommandParameters;
        public RCCommandEventParameters CommandParameters
        {
            get { return m_CommandParameters; }
            private set { m_CommandParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandEvent(RCCommandType incmdType, RCCommandEventParameters prms)
        {
            m_cmdType = incmdType;
            m_CommandParameters = prms;
        }

        #endregion Constructors
    }
}