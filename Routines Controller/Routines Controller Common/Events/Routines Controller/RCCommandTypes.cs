﻿namespace NS_Routines_Controller_Common.Events.Routines_Controller
{
    public enum RCCommandType
    {
        [Tools.String_Enum.StringValue("Start Routines Performance Thread")]
        CT_START_ROUTINES_PERFORMANCE_THREAD,
        [Tools.String_Enum.StringValue("Stop Routines Performance Thread")]
        CT_STOP_ROUTINES_PERFORMANCE_THREAD,
        [Tools.String_Enum.StringValue("Add Routine to queue")]
        CT_ADD_ROUTINE_TO_QUEUE,
        [Tools.String_Enum.StringValue("Load Routine")]
        CT_LOAD_ROUTINE,
        [Tools.String_Enum.StringValue("Load Routine For Editing")]
        CT_LOAD_ROUTINE_FOR_EDITING,
        [Tools.String_Enum.StringValue("Load Routine By Name")]
        CT_LOAD_ROUTINE_BY_NAME,
        [Tools.String_Enum.StringValue("Unload Routine")]
        CT_UNLOAD_ROUTINE,
        [Tools.String_Enum.StringValue("Stop Routine")]
        CT_STOP_ROUTINE,
        [Tools.String_Enum.StringValue("Get Action XML Text")]
        CT_GET_ACTION_XML_TEXT,
        [Tools.String_Enum.StringValue("Save Routine")]
        CT_SAVE_ROUTINE,
        [Tools.String_Enum.StringValue("Save Routine As")]
        CT_SAVE_ROUTINE_AS,
        [Tools.String_Enum.StringValue("Creates new Routine For Editing")]
        CT_CREATE_NEW_ROUTINE_FOR_EDITING,
        [Tools.String_Enum.StringValue("Update Settings")]
        CT_SETTINGS_UPDATE,
        [Tools.String_Enum.StringValue("Start Routine by Index")]
        CT_START_ROUTINE_BY_INDEX,
        [Tools.String_Enum.StringValue("Send Routine State Message")]
        ET_SEND_UPDATE_MESSAGE,
    }
}