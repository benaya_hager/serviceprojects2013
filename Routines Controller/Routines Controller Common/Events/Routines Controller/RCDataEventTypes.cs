﻿namespace NS_Routines_Controller_Common.Events.Routines_Controller
{
    public enum RCDataEventType
    {
        [Tools.String_Enum.StringValue("Data type not set")]
        ET_UNKNOWN,

        [Tools.String_Enum.StringValue("New Routine Loaded To Performance Queue")]
        ET_NEW_ROUTINE_LOADED_TO_PERFORMANCE_QUEUE,
        [Tools.String_Enum.StringValue("Routine Finished")]
        ET_ROUTINE_FINISHED,
        [Tools.String_Enum.StringValue("New Routine State Message Received")]
        ET_NEW_UPDATE_MESSAGE_RECEIVED,

        [Tools.String_Enum.StringValue("Routine for editing loaded")]
        ET_ROUTINE_FOR_EDITING_LOADED,
        [Tools.String_Enum.StringValue("Send Action XML String")]
        ET_SEND_ACTION_XML_STRING,

        [Tools.String_Enum.StringValue("Routine File Name Changed")]
        ET_ROUTINE_FILE_NAME_CHANGED



    }
}