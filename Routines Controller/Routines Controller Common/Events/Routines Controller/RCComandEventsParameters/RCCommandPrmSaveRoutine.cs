﻿using System;
namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmSaveRoutine : RCCommandEventParameters
    {
        #region Public Properties

        private Object m_Routine;
        public Object Routine
        {
            get { return m_Routine; }
            set { m_Routine = value; }
        }

        private String m_RoutineFileName;
        public String RoutineFileName
        {
            get { return m_RoutineFileName; }
            set { m_RoutineFileName = value; }
        }
        
        #endregion Public Properties

        #region Constructors

        public RCCommandPrmSaveRoutine(Object routine, String routine_file_name)
        {
            m_Routine = routine;
            m_RoutineFileName = routine_file_name;
        }

        #endregion Constructors
    }
}