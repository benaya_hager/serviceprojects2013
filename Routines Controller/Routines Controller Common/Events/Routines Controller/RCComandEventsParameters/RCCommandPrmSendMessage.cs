﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Common.Service;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmSendMessage : RCCommandEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private String m_Message;
        public String Message
        {
            get { return m_Message; }
            private set { m_Message = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmSendMessage(String  message) :
            base()
        {
            m_Message  = message;
        }

        #endregion Constructors
    }
}
