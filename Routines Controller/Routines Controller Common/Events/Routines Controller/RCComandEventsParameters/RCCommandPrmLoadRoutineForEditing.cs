﻿using System;
using System.Windows;
namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmLoadRoutineForEditing : RCCommandEventParameters
    {
        #region Public Properties

        private Window  m_OwnerForm;
        public Window OwnerForm
        {
            get { return m_OwnerForm; }
            set { m_OwnerForm = value; }
        }

        //private String m_RoutineFileName;
        //public String RoutineFileName
        //{
        //    get { return m_RoutineFileName; }
        //    set { m_RoutineFileName = value; }
        //}
        
        #endregion Public Properties

        #region Constructors

        public RCCommandPrmLoadRoutineForEditing(Window owner_form)
        {
            OwnerForm = owner_form;
           // m_RoutineFileName = routine_file_name; 
        }

        #endregion Constructors
    }
}