﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmUpdateSettings : RCCommandEventParameters
    {
        #region Public Properties

        private Int32 m_SubRoutinePerformanceCounter = 0;
        public Int32 SubRoutinePerformanceCounter
        {
            get { return m_SubRoutinePerformanceCounter; }
            set { m_SubRoutinePerformanceCounter = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmUpdateSettings(Int32 sub_routine_performance_counter)
        {
            m_SubRoutinePerformanceCounter = sub_routine_performance_counter;
        }

        #endregion Constructors
    }
}