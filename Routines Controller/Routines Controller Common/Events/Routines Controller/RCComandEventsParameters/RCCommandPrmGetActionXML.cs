﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Common.Service;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmGetActionXML : RCCommandEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private _ActionType m_ActionType;
        public _ActionType ActionType
        {
            get { return m_ActionType; }
            private set { m_ActionType = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmGetActionXML(_ActionType action_type) :
            base()
        {
            m_ActionType = action_type;
        }

        #endregion Constructors
    }
}
