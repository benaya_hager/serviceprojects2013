﻿using System;
using System.Windows;
namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmAddRoutineToQueue : RCCommandEventParameters
    {
        #region Public Properties

        private Object m_OwnerForm;
        public Object OwnerForm
        {
            get { return m_OwnerForm; }
            set { m_OwnerForm = value; }
        }

        private Routine_Definitions.Routines.Routine  m_UserRoutine;

        public Routine_Definitions.Routines.Routine  UserRoutine
        {
            get { return m_UserRoutine; }
            set { m_UserRoutine = value; }
        }
        
        private Guid m_OwnerUniqID;

	    public Guid OwnerUniqID
	    {
		    get { return m_OwnerUniqID;}
		    set { m_OwnerUniqID = value;}
	    }
	
        
        #endregion Public Properties

        #region Constructors

        public RCCommandPrmAddRoutineToQueue(Object owner_form, Guid owner_uniq_ID, Routine_Definitions.Routines.Routine  user_routine ) 
        {
            OwnerForm = owner_form;
            m_UserRoutine = user_routine;
            m_OwnerUniqID = owner_uniq_ID;
        }

        #endregion Constructors
    }
}