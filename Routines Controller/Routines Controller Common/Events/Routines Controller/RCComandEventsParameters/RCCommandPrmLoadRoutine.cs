﻿namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmLoadRoutine : RCCommandEventParameters
    {
        #region Public Properties

        private int m_PosIndex;
        public int PosIndex
        {
            get { return m_PosIndex; }
            set { m_PosIndex = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmLoadRoutine(int position_index)
        {
            m_PosIndex = position_index;
        }

        #endregion Constructors
    }
}