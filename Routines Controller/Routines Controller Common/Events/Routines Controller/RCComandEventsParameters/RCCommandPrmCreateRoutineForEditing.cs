﻿using System;
using System.Windows;
namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmCreateRoutineForEditing : RCCommandEventParameters
    {
        #region Public Properties

        private Window m_OwnerForm;
        public Window OwnerForm
        {
            get { return m_OwnerForm; }
            set { m_OwnerForm = value; }
        }

         
        
        #endregion Public Properties

        #region Constructors

        public RCCommandPrmCreateRoutineForEditing(Window owner_form) 
        {
            OwnerForm = owner_form; 
        }

        #endregion Constructors
    }
}