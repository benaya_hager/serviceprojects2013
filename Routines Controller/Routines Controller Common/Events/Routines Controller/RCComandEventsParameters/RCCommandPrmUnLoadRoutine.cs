﻿namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmUnLoadRoutine : RCCommandEventParameters
    {
        #region Public Properties

        private int m_RoutineIndex;
        public int RoutineIndex
        {
            get { return m_RoutineIndex; }
            set { m_RoutineIndex = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmUnLoadRoutine(int routine_index)
        {
            m_RoutineIndex = routine_index;
        }

        #endregion Constructors
    }
}