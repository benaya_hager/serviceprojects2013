﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmLoadRoutineByName : RCCommandEventParameters
    {
        #region Public Properties

        private int m_PosIndex;
        public int PosIndex
        {
            get { return m_PosIndex; }
            set { m_PosIndex = value; }
        }

        private String m_RoutineName = "Routine 1.XML";
        public String RoutineName
        {
            get { return m_RoutineName; }
            set { m_RoutineName = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCCommandPrmLoadRoutineByName(int position_index, String routine_name)
        {
            m_PosIndex = position_index;
            m_RoutineName = routine_name;
        }

        #endregion Constructors
    }
}