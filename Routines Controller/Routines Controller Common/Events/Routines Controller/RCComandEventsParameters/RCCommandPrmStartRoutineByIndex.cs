﻿using System;
namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters
{
    public class RCCommandPrmStartRoutineByIndex : RCCommandEventParameters
    {
        #region Public Properties

        

        private Int32 m_RoutineIndex;
        public Int32 RoutineIndex
        {
            get { return m_RoutineIndex; }
            set { m_RoutineIndex = value; }
        }
        
        #endregion Public Properties

        #region Constructors

        public RCCommandPrmStartRoutineByIndex(Int32 routine_index)
        { 
            m_RoutineIndex = routine_index;
        }

        #endregion Constructors
    }
}