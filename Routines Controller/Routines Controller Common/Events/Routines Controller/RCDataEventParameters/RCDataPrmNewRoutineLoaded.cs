﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmNewRoutineLoaded : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Object m_Routine = null;

        private Object m_Owner;

        public Object  Owner
        {
            get { return m_Owner; }
            set { m_Owner = value; }
        }
        

        /// <summary>
        ///
        /// </summary>
        public Object Routine
        {
            get { return m_Routine; }
            set
            {
                m_Routine = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmNewRoutineLoaded(Object owner,  Object routine)
            : base()
        {
            m_Owner = owner; 
            m_Routine = routine;
        }

        #endregion Constructors
    }
}