﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmActionXML : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private String m_ActionXmlString = "";
        public String ActionXmlString
        {
            get { return m_ActionXmlString; }
            private set { m_ActionXmlString = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmActionXML(String action_xml_string) :
            base()
        {
            m_ActionXmlString = action_xml_string;
        }

        #endregion Constructors
    }
}