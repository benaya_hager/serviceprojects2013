﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmRoutineFileNameChanged : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties
         
        private String m_RoutineFileName;
        public String RoutineFileName
        {
            get { return m_RoutineFileName; }
            set { m_RoutineFileName = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmRoutineFileNameChanged(  String routine_file_name)
            : base()
        { 
            m_RoutineFileName = routine_file_name;
        }

        #endregion Constructors
    }
}