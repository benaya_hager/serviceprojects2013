﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmNewRoutineForEditingLoaded : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Object m_Routine = null;
        /// <summary>
        ///
        /// </summary>
        public Object Routine
        {
            get { return m_Routine; }
            set
            {
                m_Routine = value;
            }
        }

        private String m_RoutineFileName;
        public String RoutineFileName
        {
            get { return m_RoutineFileName; }
            set { m_RoutineFileName = value; }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmNewRoutineForEditingLoaded(Object routine, String routine_file_name)
            : base()
        {
            m_Routine = routine;
            m_RoutineFileName = routine_file_name;
        }

        #endregion Constructors
    }
}