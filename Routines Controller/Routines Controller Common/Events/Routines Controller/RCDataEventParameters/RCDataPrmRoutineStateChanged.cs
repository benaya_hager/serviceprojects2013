﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmRoutineStateChanged : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Object m_Routine = null;

        /// <summary>
        ///
        /// </summary>
        public Object Routine
        {
            get { return m_Routine; }
            set
            {
                m_Routine = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmRoutineStateChanged(Object routine)
            : base()
        {
            m_Routine = routine;
        }

        #endregion Constructors
    }
}