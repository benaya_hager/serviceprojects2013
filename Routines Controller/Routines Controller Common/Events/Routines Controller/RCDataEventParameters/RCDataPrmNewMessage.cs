﻿using System;

namespace NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters
{
    public class RCDataPrmNewMessage : RCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private String m_Message = "";

        /// <summary>
        ///
        /// </summary>
        public String Message
        {
            get { return m_Message; }
            set
            {
                m_Message = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public RCDataPrmNewMessage(String message)
            : base()
        {
            m_Message = message;
        }

        #endregion Constructors
    }
}