﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;

namespace NS_Routines_Controller_Common.Service
{
    public class SingleActionTreeViewItem : TreeViewItem, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        #region Public Properties

        private BaseActionDescriptor m_Action;
        public BaseActionDescriptor Action
        {
            get { return m_Action; }
            set { m_Action = value; }
        }

         

        public System.Drawing.Font NodeFont
        {
            get { return (System.Drawing.Font)GetValue(NodeFontProperty); }
            set { SetValue(NodeFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NodeFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NodeFontProperty =
            DependencyProperty.Register("NodeFont", typeof(System.Drawing.Font), typeof(SingleActionTreeViewItem), new PropertyMetadata(new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)))));



        public System.Drawing.Color ForeColor
        {
            get { return (System.Drawing.Color)GetValue(ForeColorProperty); }
            set { SetValue(ForeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForeColorProperty =
            DependencyProperty.Register("ForeColor", typeof(System.Drawing.Color), typeof(SingleActionTreeViewItem), new PropertyMetadata(System.Drawing.Color.Red));

        

       

       

        #endregion

        #region Constructors

        public SingleActionTreeViewItem (BaseActionDescriptor action)
            : base()
        {
            Header = action.ActionName;  
            Action = action;

            System.Reflection.PropertyInfo[] pr = action.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo pi in pr)
            {
                System.Xml.Serialization.XmlElementAttribute[] attrs = pi.GetCustomAttributes(typeof(System.Xml.Serialization.XmlElementAttribute), false) as System.Xml.Serialization.XmlElementAttribute[];
                if (attrs.Length > 0)
                {
                    String res = GetPropertyToText(pi, attrs[0].ElementName, action);
                    if (null != res)
                    {
                        TreeViewItem  tmNode = new TreeViewItem ();
                        tmNode.Header = res;
                        NodeFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        this.Items.Add(tmNode);
                    }
                }
            }

        }

        private string GetPropertyToText(System.Reflection.PropertyInfo pi, string element_name, BaseActionDescriptor action)
        {
            String res = "";
            switch (pi.PropertyType.ToString())
            {
                #region String

                case "System.String":
                    {
                        res = element_name + " --> " + pi.GetValue(action, null).ToString(); 
                        break;
                    }

                #endregion

                #region Number

                case "System.Byte":
                case "System.Int32":
                case "System.Int64":
                case "System.UInt16":
                case "System.Decimal":
                case "System.Single":
                case "System.UInt32":
                case "System.Double":
                case "System.UInt64":
                case "System.Int16":
                    {
                        res = element_name + " --> " + (Convert.ToDecimal(pi.GetValue(action, null))).ToString();
                        break;
                    }

                #endregion

                #region Boolean

                case "System.Boolean":
                    {
                        res = element_name + " --> " + (Convert.ToBoolean(pi.GetValue(action, null)) == true ? "Checked" : "Unchecked");
                        break;
                    }

                #endregion

                default://Default to the Integer 0 value
                    {
                        if (null == pi.PropertyType.BaseType)
                            break;

                        #region Enum

                        if (pi.PropertyType.IsEnum)
                        {
                            res = element_name + " --> " +  Tools.String_Enum.StringEnum.GetStringValue((Enum)pi.GetValue(action, null));
                        }

                        #endregion

                        break;
                    }
            }
            return res;
        }

        #endregion

        #region Public And overridden Functions

        public override string ToString()
        {
            return m_Action.ActionName;
        }
         
        #endregion


        #region Virtual functions

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

      
    }
}
