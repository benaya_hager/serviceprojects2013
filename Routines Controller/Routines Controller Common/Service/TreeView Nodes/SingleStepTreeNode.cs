﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System.Windows.Controls;
using System.Windows;

namespace NS_Routines_Controller_Common.Service
{
    public class SingleStepTreeNode : TreeViewItem
    {
        #region Public Properties

        public System.Drawing.Font NodeFont
        {
            get { return (System.Drawing.Font)GetValue(NodeFontProperty); }
            set { SetValue(NodeFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NodeFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NodeFontProperty =
            DependencyProperty.Register("NodeFont", typeof(System.Drawing.Font), typeof(SingleStepTreeNode), new PropertyMetadata(new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)))));



        public System.Drawing.Color ForeColor
        {
            get { return (System.Drawing.Color)GetValue(ForeColorProperty); }
            set { SetValue(ForeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForeColorProperty =
            DependencyProperty.Register("ForeColor", typeof(System.Drawing.Color), typeof(SingleStepTreeNode), new PropertyMetadata(System.Drawing.Color.Blue));




        public Boolean  IsChecked
        {
            get { return (Boolean)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(Boolean), typeof(SingleStepTreeNode), new PropertyMetadata(false));

        

        private BaseStepDescriptor m_Step;
        public BaseStepDescriptor Step
        {
            get { return m_Step; }
            set 
            { 
                m_Step = value;
                foreach (BaseActionDescriptor action in m_Step.ActionsCol )
                {
                    this.Items.Add(new SingleActionTreeViewItem(action));
                }
            }
        }


        public bool Enabled
        {
            get { return Step.StepEnabled; }
            set
            {
                if (Step.StepEnabled)
                    ForeColor = System.Drawing.Color.Green;
                else
                    ForeColor = System.Drawing.Color.DarkGray;
            }
        }
        #endregion

        #region Constructors

        public SingleStepTreeNode(BaseStepDescriptor step)
            : base( )
        {
            Header = step.StepName;
            Step = step;
             
            this.Enabled = step.StepEnabled;
             
            IsChecked = step.StepEnabled;
           
        }
         
        #endregion

        #region Public And overridden Functions

        public override string ToString()
        {
            return m_Step.StepName;
        }

        #endregion

    }
}
