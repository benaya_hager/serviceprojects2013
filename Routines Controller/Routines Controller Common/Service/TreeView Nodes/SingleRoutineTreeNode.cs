﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;

namespace NS_Routines_Controller_Common.Service
{
    public class SingleRoutineTreeNode : TreeViewItem , INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;


        #region Public Properties

        private BaseRoutineDescriptor m_Routine;
        public BaseRoutineDescriptor Routine
        {
            get { return m_Routine; }
            private set 
            { 
                m_Routine = value;
                foreach (BaseStepDescriptor step in m_Routine.StepColl)
                {
                    this.Items.Add(new SingleStepTreeNode(step));  
                }
                RaisePropertyChanged("Routine");
            }
        }


        public String RoutineFileName
        {
            get { return (String)GetValue(RoutineFileNameProperty); }
            set { SetValue(RoutineFileNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RoutineFileName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RoutineFileNameProperty =
            DependencyProperty.Register("RoutineFileName", typeof(String), typeof(SingleRoutineTreeNode), new PropertyMetadata(""));

        


        public System.Drawing.Font NodeFont
        {
            get { return (System.Drawing.Font)GetValue(NodeFontProperty); }
            set { SetValue(NodeFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NodeFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NodeFontProperty =
            DependencyProperty.Register("NodeFont", typeof(System.Drawing.Font), typeof(SingleRoutineTreeNode), new PropertyMetadata(new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)))));



        public System.Drawing.Color ForeColor
        {
            get { return (System.Drawing.Color)GetValue(ForeColorProperty); }
            set { SetValue(ForeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForeColorProperty =
            DependencyProperty.Register("ForeColor", typeof(System.Drawing.Color), typeof(SingleRoutineTreeNode), new PropertyMetadata(System.Drawing.Color.Blue));

        

        #endregion

        #region Constructors

        public SingleRoutineTreeNode(BaseRoutineDescriptor routine, String routine_file_name)
            : base()
        {
            Header = routine.RoutineName;
            RoutineFileName = routine_file_name;
            Routine = routine;
        }

        #endregion

        #region Public And overridden Functions

        public override string ToString()
        {
            return m_Routine.RoutineName;
        }

 
         
        #endregion


        #region Virtual functions

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
