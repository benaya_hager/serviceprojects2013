﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;

namespace NS_Routines_Controller_Common.Service
{
 /// <summary>
    /// Event arguments based class
    /// </summary>
    public class StepEventArguments : EventArgs
    {
        #region Constructor

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public StepEventArguments()
            : base()
        {
        }

        /// <summary>
        /// Constructor that initialize EvetArgument object
        /// </summary>
        /// <param name="inStepDescriptor"></param>
        public StepEventArguments(BaseStepDescriptor inStepDescriptor)
            : this()
        {
            StepDescriptor = inStepDescriptor; //.Clone();
        }

        /// <summary>
        /// Constructor that initialize EvetArgument object
        /// </summary>
        /// <param name="inStepDescriptor"></param>
        public StepEventArguments(BaseStepDescriptor inStepDescriptor, Int32 inCompletedActionsCounter)
            : this(inStepDescriptor)
        {
            m_CompletedActionsCounter = inCompletedActionsCounter; //.Clone();
        }

        #endregion Constructor

        #region Public properties

        private BaseStepDescriptor m_StepDescriptor;
        /// <summary>
        /// The Completed Step descriptor
        /// </summary>
        public BaseStepDescriptor StepDescriptor
        {
            get { return m_StepDescriptor; }
            set { m_StepDescriptor = value; }
        }

        private Int32 m_CompletedActionsCounter = 0;
        /// <summary>
        /// Completed Actions Counter
        /// </summary>
        public Int32 CompletedActionsCounter
        {
            get { return m_CompletedActionsCounter; }
            set { m_CompletedActionsCounter = value; }
        }

        #endregion Public properties
    }
}
