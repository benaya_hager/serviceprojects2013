﻿using System;
using System.Drawing; 
using Tools.String_Enum;
using NS_Common.Service;


namespace NS_Routines_Controller_Common
{
    public static class GlobalVar
    {
        public const string DEFAULT_ROUTINES_FILES_PATTERN = "XML Files (*.xml)|*.xml";
    }


    #region Public class types

    #region Public Types

    public enum _ROUTINE_STATE
    {
        [StringValue("Not set")]
        RS_NOT_SET = 0,
        [StringValue("Ready to start")]
        RS_READY_TO_START = 1,
        [StringValue("In progress")]
        RS_IN_PROGRESS = 2,
        //[StringValue("I'm waiting")]
        //RS_IM_WAITING = 4,
        [StringValue("Paused")]
        RS_PAUSED = 8,
        [StringValue("Completed")]
        RS_COMPLETED = 16,
        [StringValue("Failed")]
        RS_FAILED = 32
    }

    #endregion Public Types

    public enum _MOTION_TYPES
    {
        MT_TRAPEZOIDAL = 0,
        MT_S_CURVE = 1,
        MT_PT = 2,
        MT_PVT = 3
    };

    #endregion Public class types

  
}