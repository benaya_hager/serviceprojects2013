﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;

namespace NS_Routines_Controller_Common
{

    /// <summary>
    /// Event arguments based class
    /// </summary>
    public class RoutineEventArguments : EventArgs
    {
        #region Constructor

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public RoutineEventArguments()
            : base()
        {
        }

        /// <summary>
        /// Constructor that initialize EvetArgument object
        /// </summary>
        /// <param name="inRoutineDescriptor"></param>
        public RoutineEventArguments(BaseRoutineDescriptor inRoutineDescriptor)
            : this()
        {
            RoutineDescriptor = inRoutineDescriptor; //.Clone();
        }

        #endregion Constructor

        #region Public properties

        private BaseRoutineDescriptor m_RoutineDescriptor;

        /// <summary>
        /// The completed routine descriptor
        /// </summary>
        public BaseRoutineDescriptor RoutineDescriptor
        {
            get { return m_RoutineDescriptor; }
            set { m_RoutineDescriptor = value; }
        }

        #endregion Public properties
    }
}
