﻿using System;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
namespace NS_Routines_Controller_Common.Interfaces
{
    public interface IRoutineControllerLogic
    {
        event SFW.EventGroupHandler EventGroupReady;
        int LoadRoutineXmlFile(out BaseRoutineDescriptor routine);
        int LoadRoutineXmlFile(out BaseRoutineDescriptor routine, out string routine_file_name);
        int LoadRoutineXmlFile(string file_name, out BaseRoutineDescriptor routine);
        NS_Common.Service._ActionType[] PosibleActionsTypesArray { get; }
        int SaveRoutineXmlFile(string FileName, BaseRoutineDescriptor Routine2Serialize);
        string SerializeObj(object pObject);
        int SubRoutinePerformanceCounter { get; set; }

        
    }
}
