﻿using System;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
namespace NS_Routines_Controller_Common.Interfaces
{
    public interface IRoutinesController
    {
        short AbortAllRoutines();
        short AbortRoutine(int RoutineIndex);
        int AddRoutine2PerformanceQueue(NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor RoutineObj, bool with_copy = true, bool subscribe_to_events = true, bool set_devices = true);
        short AddSingleMovementAction2ExistingRoutine(int RoutineIndex, byte AxisNetworkIdentifier, double AxisTaretPositionMM);
        short AddSingleStep2ExistingRoutine(int RoutineIndex, NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseStepDescriptor Step, bool bAddStepEvents);
        void BaseRoutineDescriptor_Jump2NextStep(object Sender, EventArgs args);
        void BaseRoutineDescriptor_Jump2PreviousStep(object Sender, EventArgs args);
        void BaseRoutineDescriptor_PauseRoutine(object Sender, EventArgs args);
        void BaseRoutineDescriptor_PlayRoutine(object Sender, EventArgs args);
        void BaseRoutineDescriptor_StopRoutine(object Sender, EventArgs args);
        short ContinueAllPausedRoutines();
        short ContinueRoutine(int RoutineIndex);
        NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor CurrentPerfromedRoutine { get; }
        event SFW.DataEventHandler DataReceived;
        short ErrorCode { get; }
        string ErrorMessage { get; }
        short GetRoutineActiveStep(int RoutineIndex, out NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseStepDescriptor RoutineStep);
        short GetRoutineByRoutineIndex(int RoutineIndex, out NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor routine);
        short GetRoutineIndexByRoutineDatabaseID(int RoutineDatabaseID, out int RoutineIndex);
        short GetRoutineNameByRoutineIndex(int RoutineIndex, out string routineName);
        short GetRoutineStatus(int RoutineIndex, out NS_Routines_Controller_Common._ROUTINE_STATE RoutineState);
        bool IsRoutinesPerformanceThreadStarted { get; }
        short JumpToDatabaseStepIndex(int RoutineIndex, int DatabaseStepIndex, bool StopActiveActions, bool SkipNotEnabled);
        short JumpToNextStep(int RoutineIndex, bool StopActiveActions, bool SkipNotEnabled);
        short JumpToPreviousStep(int RoutineIndex, bool StopActiveActions, bool SkipNotEnabled);
        short JumpToStep(int RoutineIndex, int StepIndex, bool StopActiveActions, bool SkipNotEnabled);
        int LoadRoutineXmlFile(String file_name, out BaseRoutineDescriptor routine);
        int LoadRoutineXmlFile(out String file_name,out BaseRoutineDescriptor routine);
        string Name { get; }
        void OnUpdateSplashScreen(object sender, NS_Common.Event_Arguments.UpdateSplashEventArgs args);
        short PauseAllRoutines(bool StopActiveActions);
        short PauseAllStageRoutines(bool StopActiveActions);
        short PauseRoutinePerformance(int RoutineIndex, bool StopActiveActions);
        short PerformRoutine(int RoutineIndex);
        void RemoveAllInactiveRoutinesFormPerformanceQueue();
        void RemoveAllRoutinesFromPerformanceQueue();
        bool RemoveRoutineFormPerformanceQueue(int RoutineIndex);
        void RoutineObj_CaptureImage(object Sender, int stent_index);
        void RoutineObj_ChangeRoutineState(object Sender, NS_Routines_Controller_Common._ROUTINE_STATE arg);
        void RoutineObj_ClearPointsList(object Sender);
        void RoutineObj_ComputePointsForStent(object Sender, int stent_index, int num_of_models_for_over_lap, float min_over_lap_in_MM);
        void RoutineObj_DisplayImageAndWait4Confirmation(object sender, int stent_index);
        void RoutineObj_GoToPrintPosition(object Sender, int stent_index, int jet_index);
        void RoutineObj_ImWaiting(object Sender, EventArgs args);
        void RoutineObj_IPEvent(object Sender, EventArgs args);
        void RoutineObj_NoPointsListAvailible(object Sender, EventArgs args);
        void RoutineObj_PerformanceErrorOccuerd(object Sender, EventArgs args);
        void RoutineObj_RoutineCompleted(object Sender, EventArgs args);
        void RoutineObj_RoutineStepChanged(object Sender, NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseStepDescriptor Step);
        string RoutinesFilePattern { get; set; }
        string RoutinesFilesPath { get; set; }
        int SaveRoutineXmlFile(NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor Routine2Serialize, out string routine_file_name, Type[] list = null);
        int SaveRoutineXmlFile(NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor Routine2Serialize, Type[] list = null);
        int SaveRoutineXmlFile(int inRoutineIndex, Type[] list = null);
        int SaveRoutineXmlFile(string FileName, NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor Routine2Serialize);
        int SaveRoutineXmlFile(string FileName, NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor Routine2Serialize, Type[] list = null);
        int SaveRoutineXmlFile(string FileName, int inRoutineIndex, Type[] list = null);
        void Send(string data);
        short SetDeviceList(System.Collections.Generic.Dictionary<string, SFW.IComponent> device_list);
        int SleepIntervalBetweenRuns { get; set; }
        void StartRoutinePerformance();
        void StopAllRoutines();
        void StopRoutinePerformance();
        void UpdateRoutineForEditing(NS_Routines_Controller_Common.Routine_Definitions.Base_Classes.BaseRoutineDescriptor m_Routine, string routine_file_name);
        short UpdateStepPosition(int inRoutineIndex, int stepIndex, float XPositionMM, float YPositionMM, float ZPositionMM);
        bool UseDefaultFileManagementDialog { get; set; }
    }
}
