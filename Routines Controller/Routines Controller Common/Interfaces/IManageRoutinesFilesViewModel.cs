﻿using System;
namespace NS_Routines_Controller_Common.Interfaces
{
   

    public interface IManageRoutinesFilesViewModel
    {
        int LoadRoutineXmlFile(out string selected_file_name, string routines_files_path = "", string routines_file_pattern = GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN);
        int SaveRoutineXmlFile(out String selected_file_name, string routines_files_path = "", string routines_file_pattern = GlobalVar.DEFAULT_ROUTINES_FILES_PATTERN);
    }
}
