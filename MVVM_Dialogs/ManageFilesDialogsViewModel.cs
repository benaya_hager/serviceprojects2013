﻿using Microsoft.Practices.Unity;
using MVVM_Dialogs.Interfaces;
using MVVM_Dialogs.Service;
using MVVM_Dialogs.Service.FrameworkDialogs.OpenFile;
using MVVM_Dialogs.Service.FrameworkDialogs.SaveAsFile;
using NS_Common;
using NS_Common.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace MVVM_Dialogs
{
    public class ManageFilesDialogsViewModel : ViewModelBase, IManageFilesDialogsViewModel
    {
        protected readonly IDialogService dialogService;
        protected readonly Func<IOpenFileDialog> openFileDialogFactory;
        protected readonly Func<ISaveAsFileDialog> saveFileDialogFactory;

        protected IUnityContainer m_Container;

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public ManageFilesDialogsViewModel(IUnityContainer container)
        {
            ServiceLocator.RegisterSingleton<IDialogService, DialogService>();
            ServiceLocator.Register<IOpenFileDialog, OpenFileDialogViewModel>();
            ServiceLocator.Register<ISaveAsFileDialog, SaveAsFileDialogViewModel>();

            this.dialogService = ServiceLocator.Resolve<IDialogService>();
            openFileDialogFactory = () => ServiceLocator.Resolve<IOpenFileDialog>();
            saveFileDialogFactory = () => ServiceLocator.Resolve<ISaveAsFileDialog>();
            m_Container = container;
        }

        #endregion

        #region Protected Methods

        protected virtual IDialogService GetMessageDialog()
        {
            return dialogService;
        }


        protected virtual IOpenFileDialog GetOpenFileDialog()
        { return GetOpenFileDialog( "", GlobalDef.DEFAULT_TARGETS_FILES_PATH, GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, "Open File"); }



        protected virtual IOpenFileDialog GetOpenFileDialog(string default_file_name , string targets_files_path, string targets_file_pattern, string sDialogTitle = "Open File")
        {
            if (targets_files_path == String.Empty)
            { targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH; }
            // Let factory create the IOpenFileDialog
            IOpenFileDialog openFileDialog = openFileDialogFactory();
            openFileDialog.Filter = targets_file_pattern;
            openFileDialog.InitialDirectory = targets_files_path;
            openFileDialog.Title = sDialogTitle; // "Load Target File";
            openFileDialog.FileName = default_file_name;
            return openFileDialog;
        }


        protected virtual ISaveAsFileDialog GetSaveAsFileDialog()
        { return GetSaveAsFileDialog("", GlobalDef.DEFAULT_TARGETS_FILES_PATH, GlobalDef.DEFAULT_TARGETS_FILES_PATTERN); }



        protected virtual ISaveAsFileDialog GetSaveAsFileDialog(string default_file_name , string targets_files_path, string targets_file_pattern, string sDialogTitle = "Save as File")
        {
            if (targets_files_path == String.Empty)
            { targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH; }
            // Let factory create the IOpenFileDialog
            ISaveAsFileDialog saveFileDialog = saveFileDialogFactory();
            saveFileDialog.Filter = targets_file_pattern;
            saveFileDialog.InitialDirectory = targets_files_path;
            saveFileDialog.Title = sDialogTitle;
            saveFileDialog.FileName = default_file_name;
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = true;
            
            return saveFileDialog;
        }

        #endregion


        public virtual int LoadXmlFile(out String selected_file_name,
                                        string default_file_name = "",
                                        string target_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH,
                                        string target_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN,
                                        string sDialogTitle = "Open File")
        {
            selected_file_name = "";
            IOpenFileDialog openFileDialog = GetOpenFileDialog(default_file_name, target_files_path, target_file_pattern, sDialogTitle);
            DialogResult result = dialogService.ShowOpenFileDialog(this, openFileDialog);
            if (result == DialogResult.OK)
            {
                selected_file_name = openFileDialog.FileName;
                return ErrorCodesList.OK;
            }
            return ErrorCodesList.CANCELED;
        }


        public virtual int SaveXmlFile(out String selected_file_name,
                                      string default_file_name = "",
                                      string targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH,
                                      string targets_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN,
                                      string sDialogTitle = "Save as File")
        {
            selected_file_name = "";
            ISaveAsFileDialog saveFileDialog = GetSaveAsFileDialog(default_file_name, targets_files_path, targets_file_pattern, sDialogTitle);
            DialogResult result = dialogService.ShowSaveFileDialog(this, saveFileDialog);


            if (result == DialogResult.OK)
            {
                selected_file_name = saveFileDialog.FileName;
                return ErrorCodesList.OK;
            }
            else
                return ErrorCodesList.CANCELED;
        }



        /// <summary>
        /// Load the Target from XML file
        /// </summary>
        /// <param name="Target">Out parameter. The list of Target object </param>
        /// <returns>
        /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
        /// errors codes from Common.ErrorCodesList
        /// </returns>
        public virtual int LoadXmlFile(out String selected_file_name, string target_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH, string target_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, string sDialogTitle = "Open File")
		{
            return LoadXmlFile(out selected_file_name, "", target_files_path, target_file_pattern, sDialogTitle);
		}


        public virtual int SaveXmlFile(out String selected_file_name,
                                      string targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH,
                                      string targets_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN,
                                      string sDialogTitle = "Save as File")
        {
            return SaveXmlFile(out selected_file_name, "", targets_files_path, targets_file_pattern, sDialogTitle);
        }



        public virtual MessageBoxResult DisplayMessageBox(object ownerViewModel,
                                                            string messageBoxText,
                                                            string caption,
                                                            MessageBoxButton button,
                                                            MessageBoxImage icon)
        {
            return GetMessageDialog().ShowMessageBox(ownerViewModel,
                                                     messageBoxText,
                                                     caption,
                                                     button,
                                                     icon);
        }
    }
}
