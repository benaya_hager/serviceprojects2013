﻿using System;
using System.Diagnostics.Contracts;
using System.Windows.Forms;
using WinFormsSaveAsFileDialog = System.Windows.Forms.SaveFileDialog;

namespace MVVM_Dialogs.Service.FrameworkDialogs.SaveAsFile
{
	/// <summary>
	/// Class wrapping System.Windows.Forms.SaveAsFileDialog, making it accept a ISaveAsFileDialog.
	/// </summary>
	public class SaveAsFileDialog : IDisposable
	{
		private readonly ISaveAsFileDialog saveAsFileDialog;
		private WinFormsSaveAsFileDialog concreteSaveAsFileDialog;
		

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveAsFileDialog"/> class.
		/// </summary>
		/// <param name="SaveAsFileDialog">The interface of a SaveAs file dialog.</param>
		public SaveAsFileDialog(ISaveAsFileDialog SaveAsFileDialog)
		{
			Contract.Requires(SaveAsFileDialog != null);

			this.saveAsFileDialog = SaveAsFileDialog;

			// Create concrete SaveAsFileDialog
			concreteSaveAsFileDialog = new WinFormsSaveAsFileDialog
			{
				AddExtension = SaveAsFileDialog.AddExtension,
				CheckFileExists = SaveAsFileDialog.CheckFileExists,
				CheckPathExists = SaveAsFileDialog.CheckPathExists,
				DefaultExt = SaveAsFileDialog.DefaultExt,
				FileName = SaveAsFileDialog.FileName,
				Filter = SaveAsFileDialog.Filter,
				InitialDirectory = SaveAsFileDialog.InitialDirectory,
				Title = SaveAsFileDialog.Title
			};
		}


		/// <summary>
		/// Runs a common dialog box with the specified owner.
		/// </summary>
		/// <param name="owner">
		/// Any object that implements System.Windows.Forms.IWin32Window that represents the top-level
		/// window that will own the modal dialog box.
		/// </param>
		/// <returns>
		/// System.Windows.Forms.DialogResult.OK if the user clicks OK in the dialog box; otherwise,
		/// System.Windows.Forms.DialogResult.Cancel.
		/// </returns>
		public DialogResult ShowDialog(IWin32Window owner)
		{
			Contract.Requires(owner != null);

			DialogResult result = concreteSaveAsFileDialog.ShowDialog(owner);

			// Update ViewModel
			saveAsFileDialog.FileName = concreteSaveAsFileDialog.FileName;

			return result;
		}


		#region IDisposable Members

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting
		/// unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}


		~SaveAsFileDialog()
		{
			Dispose(false);
		}


		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (concreteSaveAsFileDialog != null)
				{
					concreteSaveAsFileDialog.Dispose();
					concreteSaveAsFileDialog = null;
				}
			}
		}

		#endregion
	}
}
