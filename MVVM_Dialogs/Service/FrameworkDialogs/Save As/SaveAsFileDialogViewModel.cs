﻿namespace MVVM_Dialogs.Service.FrameworkDialogs.SaveAsFile
{
	/// <summary>
	/// ViewModel of the OpenFileDialog.
	/// </summary>
    public class SaveAsFileDialogViewModel : FileDialogViewModel, ISaveAsFileDialog
	{
		 
		public bool Enable { get; set; }
	}
}
