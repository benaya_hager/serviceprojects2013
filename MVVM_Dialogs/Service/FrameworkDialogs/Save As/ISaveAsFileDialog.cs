﻿namespace MVVM_Dialogs.Service.FrameworkDialogs.SaveAsFile
{
	/// <summary>
    /// Interface describing the SaveAsFileDialog.
	/// </summary>
    public interface ISaveAsFileDialog : IFileDialog
	{
		/// <summary>
		/// Gets or sets a value indicating whether the dialog box allows multiple files to be
		/// selected.
		/// </summary>
		bool Enable { get; set; }
	}
}
