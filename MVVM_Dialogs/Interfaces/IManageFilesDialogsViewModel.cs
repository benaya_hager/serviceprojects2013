﻿using System;
using System.Windows;
namespace MVVM_Dialogs.Interfaces
{
    public interface IManageFilesDialogsViewModel
    {
        int LoadXmlFile(out string selected_file_name, string target_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH, string target_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, string sDialogTitle = "Open File");
        int SaveXmlFile(out string selected_file_name, string targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH, string targets_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, string sDialogTitle = "Save As File");

        int LoadXmlFile(out string selected_file_name, string default_file_name ="", string target_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH, string target_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, string sDialogTitle = "Open File");
        int SaveXmlFile(out string selected_file_name, string default_file_name = "",  string targets_files_path = GlobalDef.DEFAULT_TARGETS_FILES_PATH, string targets_file_pattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN, string sDialogTitle = "Save As File");

        MessageBoxResult DisplayMessageBox(object ownerViewModel,
                                            string messageBoxText,
                                            string caption,
                                            MessageBoxButton button,
                                            MessageBoxImage icon);
    
    }
}
