﻿using DBManagerBase;
using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using Routines_Controller_UI; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using NS_Routines_Controller_Common.Interfaces;

namespace TestApplication.Component_Builders
{
    public class ComponentsBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public void Bulid(IUnityContainer container)
        {
             
            Container = container;

            Container.RegisterInstance<IUnityContainer>(Container);

            Container.RegisterType<IDBManagerComponentsBuilder, DBManagerComponentsBuilder>(); //"DBManagerComponentsBuilder", new InjectionConstructor(Container));
            Container.RegisterInstance<IDBManagerComponentsBuilder>(Container.Resolve<IDBManagerComponentsBuilder>());


            Container.RegisterType<IRoutinesControllerComponentsBuilder, RoutinesControllerComponentsBuilder>(); //"DBManagerComponentsBuilder", new InjectionConstructor(Container));
            Container.RegisterInstance<IRoutinesControllerComponentsBuilder>(Container.Resolve<IRoutinesControllerComponentsBuilder>());


        }

        public IUnityContainer Container { set; get; }
    }
}
