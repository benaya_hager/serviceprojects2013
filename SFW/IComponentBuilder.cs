﻿
namespace SFW
{
    /// <summary>
    /// Builds the application components and registers them to one another.
    /// </summary>
    public interface IComponentBuilder : IComponent
    {
        /// <summary>
        /// Creates the application components
        /// </summary>
        void CreateComponents(string Mode ="");
        /// <summary>
        /// Registers the application components to each other.
        /// </summary>
        void RegisterComponents();
    }
}
