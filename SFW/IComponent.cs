﻿
namespace SFW
{
    /// <summary>
    /// The most basic interface for all the application components
    /// </summary>
    public interface IComponent
    {
        /// <summary>
        /// A unique name that describes the component behavior and responsibility
        /// </summary>
        string Name
        {
            get;
        }
    }
}
