﻿
namespace SFW
{
    public interface IEventGroupPublisher 
    {
        event EventGroupHandler EventGroupReady;

         //if (EventGroupReady != null)
         //       EventGroupReady(this, data);
        void RaiseEventGroup(EventGroupData data);

        //if (EventGroupReady != null)
        //    {
        //        EventGroupData groupData = new EventGroupData();

        //        groupData.Enqueue(data);

        //        EventGroupReady(this, groupData);
        //    }
        void RaiseSingleEvent(IEventData data);
        
    }
}
