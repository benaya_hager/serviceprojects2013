﻿
namespace SFW
{
    public delegate void DataEventHandler(object sender, EventArgs<object> data);

    /// <summary>
    /// The basic interface for all input devices of the application
    /// </summary>
    public interface IInputDevice : IComponent
    {
        /// <summary>
        /// An event raised when new data arrives from the device
        /// </summary>
        event DataEventHandler DataReceived; 
    }
}
