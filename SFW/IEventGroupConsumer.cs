﻿namespace SFW
{
    /// <summary>
    /// TODO : adiel
    /// </summary>
    public interface IEventGroupConsumer
    {
        /// <summary>
        /// A public event handler to handle the EventGroupData processed by the Component.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <example>
        /// foreach (IEventData singleEvent in data)
        ///    {
        ///        if (singleEvent != null)
        ///        {
        ///            if (m_Handlers == null || m_Handlers[singleEvent.GetType()] == null)
        ///            {
        ///                throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
        ///            }
        ///            else
        ///            {
        ///                m_Handlers[singleEvent.GetType()](singleEvent);
        ///            }
        ///        }
        ///    }
        /// </example>
        void OnProcessEventGroup(object sender, EventGroupData data);

        /// <summary>
        /// Maps handlers to each of the consumed EventGroup.
        /// </summary>
        void MapHandlers();

        void ClearHandlers();
    }
}