﻿
namespace SFW
{
    /// <summary>
    /// The basic interface for all devices that gets the the application's output as input.
    /// </summary>
    public interface IOutputDevice : IComponent
    {
        void Send(string data);
    }
}
