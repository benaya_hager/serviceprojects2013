﻿using System.Threading;
namespace SFW
{
    public interface IActionSyncManager
    {
        string Name { get; }
        AutoResetEvent GetSyncObject(string syncObjectName);
        
    }
}
