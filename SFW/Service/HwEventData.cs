﻿
namespace SFW.Service
{
    public class HwEventData : IEventData
    {
        protected long m_TimeStamp;

        public long TimeStamp
        {
            get { return m_TimeStamp; }
            set { m_TimeStamp = value; }
        }

        protected object m_Data;

        public object Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        protected long m_MsgId;

        public long MsgId
        {
            get { return m_MsgId; }
            set { m_MsgId = value; }
        }

        public HwEventData(long timeStamp, long msgId, object data)
        {
            m_TimeStamp = timeStamp;
            m_Data = data;
            m_MsgId = msgId;
        }

        public HwEventData(long timeStamp, object data)
        {
            m_TimeStamp = timeStamp;
            m_Data = data;
            m_MsgId = -1;
        }
    }
}
