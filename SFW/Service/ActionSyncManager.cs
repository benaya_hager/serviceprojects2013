﻿using System.Collections.Generic;
using System.Threading;

namespace SFW
{
    /// <summary>
    /// this class is a synchronization service to sync actions between different objects of the solution
    /// the class uses designated AutoResetEvent objects
    /// </summary>
    public class ActionSyncManager: IComponent, IActionSyncManager
    {
       
        private Dictionary<string,AutoResetEvent> _syncObjectDictionary;

       
        /// <summary>
        /// use this function to get a syncobject by name 
        /// </summary>
        /// <param name="syncObjectName"> name of syncobject: should specify sync purpose</param>
        /// <returns></returns>
        public AutoResetEvent GetSyncObject(string syncObjectName)
        {
            lock (this)
            {
                
            
            if (!_syncObjectDictionary.ContainsKey(syncObjectName))
                _syncObjectDictionary.Add(syncObjectName, new AutoResetEvent(false));
            return _syncObjectDictionary[syncObjectName];
            }
        }


        public ActionSyncManager()
        {
            _syncObjectDictionary = new Dictionary<string, AutoResetEvent>();
        }

        public string Name
        {
            get { return "ActionSyncManager"; }
        }
    }
}
