﻿
namespace SFW
{
    /// <summary>
    /// An interface for all <see cref="IInputDevice"/> of the application.
    /// </summary>
    /// <remarks>
    /// Note : it is advised to use the Listener basic implementation of IListener.
    /// </remarks>
    public interface IListener : IComponent
    {
    }
}
