﻿using System.Collections.Generic;

namespace SFW
{
    public abstract class ComponentBuilder : IComponentBuilder
    {
        public ComponentBuilder()
        {
        }

        #region Private Data Members

        protected Dictionary<string, IComponent> m_Components;
        public Dictionary<string, IComponent> ComponentsList
        {
            get { return m_Components; }
            set { m_Components = value; }
        }

        #endregion

        #region ICompBuilder Members

        public virtual void Build(string Mode = "")
        {
            CreateComponents(Mode);
            RegisterComponents();
        }

        public virtual void Release()
        {
            ReleaseComponents();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <example>
        /// {
        ///     using (IUnityContainer m_Container = new UnityContainer())
        ///     {
        ///     m_Container.RegisterType<IInputDevice, MockInputDevice>("MockInputDevice",
        ///                                     new InjectionConstructor(2000));
        /// 
        ///     m_Container.RegisterType<Listener, SampleListener>("SampleListener",
        ///                                 new InjectionConstructor(
        ///                                 new ResolvedParameter<IInputDevice>("MockInputDevice")));
        /// 
        ///     Listener sampleListener = m_Container.Resolve<Listener>("SampleListener");
        /// 
        ///     SampleLogic logic1 = m_Container.Resolve<SampleLogic>();
        /// 
        ///     SampleLogic2 logic2 = m_Container.Resolve<SampleLogic2>();
        /// 
        ///     sampleListener.EventGroupReady += logic1.OnProcessEventGroup;
        /// 
        ///     logic1.EventGroupReady += logic2.OnProcessEventGroup;
        ///     }
        /// 
        ///     m_Container.RegisterType<IInputDevice, MockInputDevice>("MockInputDevice",
        ///                                     new InjectionConstructor(2000));
        /// 
        ///     m_Container.RegisterType<Listener, SampleListener>("SampleListener",
        ///                                 new InjectionConstructor(
        ///                                 new ResolvedParameter<IInputDevice>("MockInputDevice")));
        /// 
        ///     Listener sampleListener = m_Container.Resolve<Listener>("SampleListener");
        /// 
        ///     SampleLogic logic1 = m_Container.Resolve<SampleLogic>();
        /// 
        ///     SampleLogic2 logic2 = m_Container.Resolve<SampleLogic2>();
        /// 
        ///     sampleListener.EventGroupReady += logic1.OnProcessEventGroup;
        /// 
        ///     logic1.EventGroupReady += logic2.OnProcessEventGroup;
        /// }
        /// </example>
        public abstract void CreateComponents(string Mode = "");

        public abstract void CreateMainWindow();

        public virtual void RegisterComponents()
        {
            RegisterDevices();
            
            RegisterListeners();
            
            RegisterLogics();

            return;
        }

        protected abstract void RegisterLogics();

        protected abstract void RegisterListeners();

        protected abstract void RegisterDevices();
        
        public virtual void  ReleaseComponents(){}

        #endregion

        #region IComponent Members

        public string Name
        {
            get { return "ComponentBuilder"; }
        }

        #endregion
   }
}
