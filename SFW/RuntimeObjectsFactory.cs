﻿using System.Collections.Generic;

namespace SFW
{
    /// <summary>
    /// A base class for all factories that creates objects in runtime after the initialization time. Most of the components are created
    /// in the ComponentBuilder while others are created upon request or according to the user actions.
    /// </summary>
    public abstract class RuntimeObjectsFactory : IComponent
    {
        protected Dictionary<string, IComponent> m_Components;

        public RuntimeObjectsFactory(Dictionary<string, IComponent> components)
        {
            m_Components = components;
        }

        public abstract IComponent CreateItem(string type);

        public abstract void ReleaseItem(string type);

        public virtual void ReleaseItem(IComponent item)
        { }

        public string Name
        {
            get { return "RuntimeObjectsFactory"; }
        }
    }
}