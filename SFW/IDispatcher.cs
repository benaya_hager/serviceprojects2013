﻿
namespace SFW
{
    /// <summary>
    /// An interface intended for use for components that dispatch data (Notifications) to clients of the application.
    /// </summary>
    public interface IDispatcher
    {
    }
}
