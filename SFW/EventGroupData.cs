﻿using System.Collections.Concurrent;

namespace SFW
{
    public delegate void EventGroupHandler(object sender, EventGroupData data);

    /// <summary>
    /// A Collection of queued event's data, used as the basic communication between two application components.
    /// </summary>
    public class EventGroupData : ConcurrentQueue<IEventData> 
    {
        
    }
}
