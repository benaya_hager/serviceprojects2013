﻿
namespace SFW
{
    /// <summary>
    /// An interface for a device that sends input to the application and gets output from the application.
    /// </summary>
    public interface IIODevice : IComponent, IInputDevice, IOutputDevice
    {
    }
}
