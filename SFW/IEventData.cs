﻿
namespace SFW
{
    /// <summary>
    /// The basic interface for a single event data that is raised from one component to the other
    /// </summary>
    public interface IEventData
    {
    }
}
