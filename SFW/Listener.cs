﻿
namespace SFW
{
    public abstract class Listener : IListener
    {
        /// <summary>
        /// An event raised when the group of events which holds the data received from the input device are ready to be sent.
        /// </summary>
        public event EventGroupHandler EventGroupReady;

        /// <summary>
        /// A group of events which holds the data received from the input device
        /// </summary>
        protected EventGroupData m_EventGroupData;

        /// <summary>
        /// The listener's unique name
        /// </summary>
        protected string m_Name;

        protected IInputDevice m_Device;

        public Listener(IInputDevice device)
        {
            m_Name = "Listener";

            m_Device = device;

            device.DataReceived += new DataEventHandler(OnInputDeviceDataReceived);
        }

        /// <summary>
        /// An abstract method that needs to be implemented by the user.
        /// Processing of the device events can result in an <see cref="EventGroupReady"/> event raised.
        /// in order to pass processed data the user can use the <see cref="m_EventGroupData"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected abstract void OnInputDeviceDataReceived(object sender, SFW.EventArgs<object> e);

        /// <summary>
        /// Raises the <see cref="EventGroupReady"/> with <see cref="m_EventGroupData"/> as data.
        /// </summary>
        protected virtual void RaiseEventGroupReady()
        {
            if (EventGroupReady != null)
                EventGroupReady(this, m_EventGroupData);
        }

        /// <summary>
        /// Raises the <see cref="EventGroupReady"/> with <paramref name="data"/>.
        /// </summary>
        /// <param name="data">the event group data to be passed forward</param>
        protected virtual void RaiseEventGroupReady(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        /// <summary>
        /// Raises the <see cref="EventGroupReady"/> with <paramref name="data"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data">the event group data to be passed forward</param>
        protected virtual void RaiseEventGroupReady(object sender, EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(sender, data);
        }

        protected virtual void RaiseSingleEvent(IEventData data)
        {
            if (EventGroupReady != null)
            {
                EventGroupData groupData = new EventGroupData();

                groupData.Enqueue(data);

                EventGroupReady(this, groupData);
            }
        }

        #region IComponent Members

        public string Name
        {
            get
            {
                return m_Name;
            }
        }

        #endregion
    }
}
