﻿using log4net;
using System;
using System.Collections.Concurrent;

namespace SFW
{
    public abstract class Logic : BaseNotificationObject, IComponent
    {
        #region Protected Data Members

        protected ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        protected ILog m_Logger;

        protected string m_Name;

        #endregion Protected Data Members

        #region Public Events

        /// <summary>
        /// An event raised when the group of events which holds the data received from the input device are ready to be sent.
        /// </summary>
        public event EventGroupHandler EventGroupReady;

        #endregion Public Events

        #region Constructor

        public Logic()
        {
            m_Name = GetType().Name;

            m_Logger = LogManager.GetLogger(m_Name);

            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapHandlers();
        }

        ~Logic() { ClearHandlers(); }

        #endregion Constructor

        #region Event Group Data Handler

        /// <summary>
        /// A public event handler to handle the EventGroupData processed by the Logic. The event handler has a simple implementation
        /// in which each event invokes (in the current thread) the handler from m_Handlers mapped by the user to the
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        public virtual void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
            // Caused too much problems - for now obsolete user can always do it by himself ...
            //RaiseEventGroup(data);
        }

        #endregion Event Group Data Handler

        #region Event Raising

        protected void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        protected virtual void RaiseSingleEvent(IEventData data)
        {
            if (EventGroupReady != null)
            {
                EventGroupData groupData = new EventGroupData();

                groupData.Enqueue(data);

                EventGroupReady(this, groupData);
            }
        }

        protected abstract void MapHandlers();


        protected virtual void ClearHandlers()
        { 

        }


        #endregion Event Raising

        #region IComponent Members

        public string Name
        {
            get { return m_Name; }
        }

        #endregion IComponent Members
    }
}