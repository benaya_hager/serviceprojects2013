﻿using System;
namespace NS_BLL
{
    public interface IBLL
    {
        bool CreateRule(Type FieldType);
        event SFW.DataEventHandler DataReceived;
        int MachineID { get; }
        NS_BLL.Logics.Service.MachineType2BrandsLogic MachineType2Brands { get; }
        string Name { get; }
        NS_BLL.Logics.C_AND_F_Logic Selected_C_And_F { get; set; }
        NS_BLL.Logics.ProtocolLogic Selected_Protocol { get; set; }
        NS_BLL.Logics.STP_Logic Selected_STP { get; set; }
        NS_BLL.Logics.Product.ProductLogic SelectedProduct { get; set; }
        NS_BLL.Logics.Project.ProjectLogic SelectedProject { get; set; }
        NS_Common.Service._TestGroupType SelectedTestGroupType { get; set; }
        void Send(string data);
        MachineLogic TestMachine { get; }
    }
}
