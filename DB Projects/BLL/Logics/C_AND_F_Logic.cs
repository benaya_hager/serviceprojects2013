﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using NS_Common.Service;

namespace NS_BLL.Logics
{
    /// <summary>
    /// The table that holds C&F test parameters list
    /// </summary>
    public class C_AND_F_Logic: Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "C And F";
#endregion

#region Public properties
        private String m_TestName = "";
        /// <summary>
        /// C&F name\Title
        /// </summary>
        [DBColumn("Name/Title", 255, false, true, false, false, 0, 0)]
        public String TestName
        {
            get { return m_TestName; }
            set { m_TestName = value; RaisePropertyChanged("TestName"); }
        }       

        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current STP user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; RaisePropertyChanged("UserComments"); }
        }

        ///<summary>
        ///Used for automatic fields Binding
        ///</summary>
        [DBColumn(false)]
        private System.Data.DataTable m_ShimsDiameters;
        [DBColumn(_DB_FIELD_TYPE.SINGLE_TO_MANY, "SelectFromProductDistinctShimpsDiameters",
                                                  "", "", "")]
        public System.Data.DataTable ShimsDiameters
        {
            get
            {
                if (null == m_ShimsDiameters)
                {
                    NS_DAL.DAL.GetInstance().SelectByParamsToField<C_AND_F_Logic>(this, "ShimsDiameters", out m_ShimsDiameters);
                }
                return m_ShimsDiameters;
            }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }

#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public C_AND_F_Logic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<C_AND_F_Logic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with C&F description parameters. 
        /// </summary>
        public C_AND_F_Logic(Int32 inC_AND_F_ID) :
            this()
        {
            this.ID = inC_AND_F_ID;
            LoadData();
        }

        /// <summary>
        /// Constructor with S&F description parameters. 
        /// </summary>
        public C_AND_F_Logic(Int32 inC_AND_F_ID ,String inTitle) :
            this(inC_AND_F_ID)
        { 
            m_TestName  = inTitle;
        }

        public C_AND_F_Logic(String inTitle) :
            this(-1, inTitle)
        {
        }
#endregion

#region Public overridden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "C&F Details";
        }

        /// <summary>
        /// Return the selected C&F name
        /// </summary>
        /// <returns>Return the selected STP name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            if (null == m_TestName || "" == m_TestName)
            {
                throw NS_Common.Error_Handling.ExceptionsList.C_AND_F_TitleNotSetException;
            }  

            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<C_AND_F_Logic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.C_AND_F_TitleNotSetException;
                        }                        
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.Duplicate_C_AND__TitleException;
                        }                       
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected C&F ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            if (ID >= 0)
            {
                C_AND_F_Logic logic = new C_AND_F_Logic();
                NS_DAL.DAL.GetInstance().LoadDataTable<C_AND_F_Logic>(ref logic, ID);
                Clone2This(logic);
            }
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<C_AND_F_Logic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            if (null == m_TestName || "" == m_TestName)
            {
                throw NS_Common.Error_Handling.ExceptionsList.C_AND_F_TitleNotSetException;
            }  
            
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<C_AND_F_Logic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.C_AND_F_TitleNotSetException;
                        }       
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.Duplicate_C_AND__TitleException;
                        }  
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<C_AND_F_Logic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            C_AND_F_Logic Lgc = new C_AND_F_Logic();
            NS_DAL.DAL.GetInstance().SelectAllToDataTable<C_AND_F_Logic>(Lgc, out DT);
            return DT;  
        }
#endregion
    }
}
