﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using System.Data;
using NS_Common.Service;
using NS_BLL.Logics;

namespace NS_BLL.Logics.Project
{
    public class ProjectLogic : Base_Classes.BaseLogicClass
    {
        #region Constructors
            /// <summary>
            /// Base Empty constructor. 
            /// </summary>
            public ProjectLogic() : 
                base()
            {
                m_TableName = TABLE_NAME; 
                NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProjectLogic>(this, false, true);
                InitValues();
            }

            /// <summary>
            /// Constructor with Project  description parameters. 
            /// </summary>
            public ProjectLogic(Int32 inProjectID) :
                this()
            {
                this.ID = inProjectID;
                LoadData();
            }
        #endregion

        #region Private Const
            private const String TABLE_NAME = "Projects";
        #endregion

        #region Public properties
            [DBColumn(false)]
            public override string TableName
            {
                get
                {
                    return m_TableName;
                }
            }
            
            [DBColumn(false)]
            private String m_Project_Number = "";
            /// <summary>
            /// Set\Get the Project serial number (Project Num/code)
            /// </summary>
            [DBColumn("Num/Code", 255, false,true, false, false, 0, 0)]
            public String Project_Number
            {
                get { return m_Project_Number; }
                set { m_Project_Number = value; RaisePropertyChanged("Project_Number"); }
            }
        
            [DBColumn(false)]
            private String m_Project_Name = "";
            /// <summary>
            /// Set\Get the Project name/title
            /// </summary>
            [DBColumn("Name/Title", 500, false, true, false, false, 0, 0)]
            public String Project_Name
            {
                get { return m_Project_Name; }
                set { m_Project_Name = value; RaisePropertyChanged("Project_Name"); }
            }

            [DBColumn(false)]
            private String m_Project_Nickname = "";
            /// <summary>
            /// Set\Get the Project Nickname
            /// </summary>
            [DBColumn("Nickname", 255, false, true, false, false, 0, 0)]
            public String Project_Nickname
            {
                get { return m_Project_Nickname; }
                set { m_Project_Nickname = value; RaisePropertyChanged("Project_Nickname"); }
            }

            [DBColumn(false)]
            private Int32 m_Project_ManagerID = -1;
            /// <summary>
            /// Set\Get the Project Manager ID from table "Projects Managers/Owners"
            /// </summary>
            [System.ComponentModel.DefaultValue(1), 
            DBColumn("Projects Managers/Owners IDs", _DB_FIELD_TYPE.OTHER_TABLE_ID, "Projects Managers/Owners", "Person Name/Title", "ProductManagerName")]
            public Int32 Project_ManagerID
            {
                get { return m_Project_ManagerID; }
                set { m_Project_ManagerID = value; RaisePropertyChanged("Project_ManagerID"); }
            }

            [DBColumn(false)]
            private String m_ProductManagerName = "";
            /// <summary>
            /// Set/Get value Product Manager Name
            /// </summary>
            [DBColumn(false)]
            public String ProductManagerName
            {
                get { return m_ProductManagerName; }
                set { m_ProductManagerName = value; RaisePropertyChanged("ProductManagerName"); }
            }

            [DBColumn(false)]
            private Int32 m_Development_Status =  -1;
            /// <summary>
            /// Set\Get the Project development status
            /// </summary>
            [System.ComponentModel.DefaultValue(1), 
            DBColumn("Development Status ID", _DB_FIELD_TYPE.OTHER_TABLE_ID, "Project States", "Project State Name", "DevelopmentStatusCaption")]
            public Int32 Development_Status
            {
                get { return m_Development_Status; }
                set { m_Development_Status = value; RaisePropertyChanged("Development_Status"); }
            }

            [DBColumn(false)]
            private String m_DevelopmentStatusCaption = "";
            /// <summary>
            /// Set/Get value Development Status Caption
            /// </summary>
            [DBColumn(false)]
            public String DevelopmentStatusCaption
            {
                get { return m_DevelopmentStatusCaption; }
                set { m_DevelopmentStatusCaption = value; RaisePropertyChanged("DevelopmentStatusCaption"); }
            }

            [DBColumn(false)]
            private _Internal_Lab_Status m_Internal_Lab_Status = _Internal_Lab_Status.ILS_Unknown;
            /// <summary>
            /// Set\Get the Project Internal Laboratory development status
            /// </summary>
            [System.ComponentModel.DefaultValue(_Internal_Lab_Status.ILS_Unknown), 
            DBColumn("Internal Lab Status", -1, false, false, true, false, 0, 0)]
            public _Internal_Lab_Status Internal_Lab_Status
            {
                get { return m_Internal_Lab_Status; }
                set { m_Internal_Lab_Status = value; RaisePropertyChanged("Internal_Lab_Status"); }
            }
            /// <summary>
            /// Used for automatic fields Binding
            /// </summary>
            [DBColumn(false)]
            public String strInternal_Lab_Status
            {
                get { return Tools.String_Enum.StringEnum.GetStringValue(m_Internal_Lab_Status); }
                set
                {
                    if ((string.IsNullOrEmpty(value) | value == null)) return;
                    m_Internal_Lab_Status = (NS_Common.Service._Internal_Lab_Status)Tools.String_Enum.StringEnum.Parse(typeof(NS_Common.Service._Internal_Lab_Status), value);
                    RaisePropertyChanged("strInternal_Lab_Status"); 
                }
            }

            [DBColumn(false)]
            private Boolean m_isOpenOrClosed = true;
            /// <summary>
            /// Set\Get the Project development status (is open or closed state)
            /// </summary>
            [System.ComponentModel.DefaultValue(true), 
            DBColumn("Open/Closed", -1, false, false, false, false, 0, 0)]
            public Boolean isOpenOrClosed
            {
                get { return m_isOpenOrClosed; }
                set { m_isOpenOrClosed = value; RaisePropertyChanged("isOpenOrClosed"); }
            }

            [DBColumn(false)]
            private String m_UserComments = "";
            /// <summary>
            /// Any comments about current project user wants to add
            /// </summary>
            [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
            public String UserComments
            {
                get { return m_UserComments; }
                set { m_UserComments = value; RaisePropertyChanged("UserComments"); }
            }

            ///<summary>
            ///Used for automatic fields Binding
            ///</summary>
            [DBColumn(false)]
            private System.Data.DataTable m_ShimsDiameters;
            [DBColumn(_DB_FIELD_TYPE.SINGLE_TO_MANY, "SelectFromProductDistinctShimpsDiameters",
                                                      "", "", "")]
            public System.Data.DataTable ShimsDiameters
            {
                get
                {
                    if (null == m_ShimsDiameters)
                    {
                        NS_DAL.DAL.GetInstance().SelectByParamsToField<ProjectLogic>(this, "ShimsDiameters", out m_ShimsDiameters);
                    }
                    return m_ShimsDiameters;
                }
            }
        #endregion

        #region overridden Functions
            /// <summary>
            /// Return the class description string
            /// </summary>
            /// <returns>Class description text</returns>
            /// <remarks></remarks>
            public override string ClassDescription()
            {
                return "Project Details";
            }

            /// <summary>
            /// Return the selected Project name
            /// </summary>
            /// <returns>Return the selected Project name, or "..." in case no item selected</returns>
            /// <remarks></remarks>
            public override string DataDescription()
            {
                if (m_ID == 0)
                    return "...";
                return m_Name;
            }

            /// <summary>
            /// Updates data in database
            /// </summary>
            /// <remarks></remarks>
            public override void UpdateData()
            {
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                    
                    NS_DAL.DAL.GetInstance().Update<ProjectLogic>(this, false);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Name/Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNameNotSetException;
                            }
                            if (ex.Message.Contains("Num/Code"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNumberNotSetException;
                            }
                            if (ex.Message.Contains("Nickname"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNicknameNameNotSetException;
                            } 
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Name/Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNameException;
                            }
                            if (ex.Message.Contains("Num/Code"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNumberException;
                            }
                            if (ex.Message.Contains("Nickname"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNicknameException;
                            } 
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                }
            }

            /// <summary>
            /// Load data for selected Project ID
            /// </summary>
            /// <remarks></remarks>
            public override void LoadData()
            {
                ProjectLogic logic = new ProjectLogic();
                NS_DAL.DAL.GetInstance().LoadDataTable<ProjectLogic>(ref logic, ID);
                Clone2This(logic);
            }

            /// <summary>
            /// Delete row from database
            /// </summary>
            /// <remarks></remarks>
            public override void DeleteData()
            {
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                    NS_DAL.DAL.GetInstance().Delete<ProjectLogic>(this);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                            throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                    }
                }
            }

            /// <summary>
            /// Insert new row with data, to database
            /// </summary>
            /// <remarks></remarks>
            public override void InsertData()
            {
                try
                {
                    Object RecordID;
                    NS_DAL.DAL.GetInstance().Insert<ProjectLogic>(this, true, out RecordID);
                    this.ID = Convert.ToInt32(RecordID);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Name/Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNameNotSetException;
                            }
                            if (ex.Message.Contains("Num/Code"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNumberNotSetException;
                            }
                            if (ex.Message.Contains("Nickname"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProjectNicknameNameNotSetException;
                            } 
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Name/Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNameException;
                            }
                            if (ex.Message.Contains("Num/Code"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNumberException;
                            }
                            if (ex.Message.Contains("Nickname"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNicknameException;
                            } 
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw new Exception(ex.Message);
                    }
                }
            }

            /// <summary>
            /// Base initialization function. Set to 0/"" all values
            /// </summary>
            /// <remarks></remarks>
            public override void InitValues()
            {
                InitValuesToDefaults();
            }

            /// <summary>
            /// Get search result by one or more parameters
            /// </summary>
            /// <returns>All found rows corresponding to the search request</returns>
            /// <remarks></remarks>
            public override DataView GetSearchResults()
            {
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProjectLogic>(this, out DT);
                return DT.DefaultView;
            }

            /// <summary>
            /// Return the full list of Projects from table Projects
            /// </summary>
            /// <returns></returns>
            /// <remarks>Return the full list of Projects from table Projects</remarks>
            public override DataTable GetComboSource()
            {
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProjectLogic>(this, out DT);
                return DT;
            }
        #endregion

        #region Public Methods
            /// <summary>
            /// Return the full list of Product states from table "Product States"
            /// </summary>
            /// <returns></returns>
            public DataTable GetProjectStatesListTable()
            {
                System.Data.DataTable DT;
                ProjectStatesLogic BrndsLgc = new ProjectStatesLogic();
                NS_DAL.DAL.GetInstance().SelectAllToDataTable<ProjectStatesLogic>(BrndsLgc, out DT);
                return DT;  
            }
        #endregion
    }
}
