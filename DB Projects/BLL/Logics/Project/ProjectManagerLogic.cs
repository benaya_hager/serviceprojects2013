﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics.Project
{
    public class ProjectManagerLogic: Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Projects Managers/Owners";
#endregion

#region Public properties
        private String m_PersonName = "";
        /// <summary>
        /// Project manager Name\Title
        /// </summary>
        [DBColumn("Person Name/Title", 255, false, true, false, false, 0, 0)]
        public String PersonName
        {
            get { return m_PersonName; }
            set { m_PersonName = value; RaisePropertyChanged("PersonName"); }
        }       

        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current the person user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; RaisePropertyChanged("UserComments"); }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public ProjectManagerLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProjectManagerLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Person description parameters. 
        /// </summary>
        public ProjectManagerLogic(Int32 inPersonID) :
            this()
        {
            this.ID = inPersonID;
            LoadData();
        }

        /// <summary>
        /// Constructor with  PersonID description parameters. 
        /// </summary>
        public ProjectManagerLogic(Int32 inPersonID ,String inPersonName) :
            this(inPersonID)
        { 
            m_PersonName  = inPersonName;
        }

        public ProjectManagerLogic(String inPersonName) :
            this(-1, inPersonName)
        {
        }
#endregion

#region Public overridden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Project Manager/Owner Details";
        }

        /// <summary>
        /// Return the selected Project Manager/Owner Name
        /// </summary>
        /// <returns>Return the selected Project Manager/Owner Name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            if (null == m_PersonName || "" == m_PersonName)
                throw NS_Common.Error_Handling.ExceptionsList.ProjectManagerNameNotSetException;

            try
            {
                if ((this.ID == 1)) // The Unknown
                     throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<ProjectManagerLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Person Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProjectManagerNameNotSetException;
                        }                        
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Person Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectManagerNameException;
                        }                       
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Project Manager/Owner ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            if (ID >= 0)
            {
                ProjectManagerLogic logic = new ProjectManagerLogic();
                NS_DAL.DAL.GetInstance().LoadDataTable<ProjectManagerLogic>(ref logic, ID);
                Clone2This(logic);
            }
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1))
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<ProjectManagerLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            if (null == m_PersonName || "" == m_PersonName)
                throw NS_Common.Error_Handling.ExceptionsList.ProjectManagerNameNotSetException;

            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<ProjectManagerLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {                    
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Person Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProjectManagerNameNotSetException;
                        }                        
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Person Name/Title"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectManagerNameException;
                        }                       
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProjectManagerLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        ///Return the full list of Project Manager/Owner from table Project managers
        /// </summary>
        /// <returns>Return the full list of Project Manager/Owner from table Project managers</returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectAllToDataTable<ProjectManagerLogic>(this, out DT);
            return DT;
        }
#endregion
    }
}
