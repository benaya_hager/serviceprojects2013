﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using System.Data;

namespace NS_BLL.Logics.Project
{
    public class ProjectStatesLogic: Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Project States";
#endregion

#region Public properties
        private String m_ProjectState_Name = "";
        /// <summary>
        /// ProjectState name
        /// </summary>
        [DBColumn("Project State Name", 255, false,true, false, false, 0, 0)]
        public String ProjectState_Name
        {
            get { return m_ProjectState_Name; }
            set { m_ProjectState_Name = value; RaisePropertyChanged("ProjectState_Name"); }
        }
         
        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current brand user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; RaisePropertyChanged("UserComments"); }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public ProjectStatesLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProjectStatesLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with ProjectState description parameters. 
        /// </summary>
        public ProjectStatesLogic(Int32 inProjectStateID) :
            this()
        {
            this.ID = inProjectStateID;
            LoadData();
        }

        /// <summary>
        /// Constructor with ProjectState description parameters. 
        /// </summary>
        public ProjectStatesLogic(Int32 inProjectStateID ,String inProjectStateName) :
            this()
        { 
            ID = inProjectStateID;
            ProjectState_Name = inProjectStateName;
        }

        public ProjectStatesLogic(String inProjectStateName) :
            this(-1, inProjectStateName)
        {
        }
#endregion

#region Public overridden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Project State Details";
        }

        /// <summary>
        /// Return the selected ProjectState name
        /// </summary>
        /// <returns>Return the selected ProjectState name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<ProjectStatesLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Project State Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProjectStateNameNotSetException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Project State Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectStateNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Project State
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            ProjectStatesLogic logic = new ProjectStatesLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<ProjectStatesLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<ProjectStatesLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<ProjectStatesLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Project State Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProjectStateNameNotSetException; 
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Project State Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProjectStateNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProjectStatesLogic>(this, out DT);
            return DT.DefaultView;
        }


        /// <summary>
        /// Return Full table results
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProjectStatesLogic>(this, out DT);
            return DT;
        }
 

#endregion
    }
}
