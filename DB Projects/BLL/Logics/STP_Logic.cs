﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics
{
    /// <summary>
    /// The table that holds STP list
    /// </summary>
    public class STP_Logic : Base_Classes.BaseLogicClass
    {  
        #region Private Constants 
            private const String TABLE_NAME = "STPs";
        #endregion

        #region Public properties
            private String m_SerialNumber = "";
            /// <summary>
            /// STP Serial number
            /// </summary>
            [DBColumn("#Num", 255, false, true, false, false, 0, 0)]
            public String SerialNumber
            {
                get { return m_SerialNumber; }
                set
                {  
                    if (m_SerialNumber != value)
                    {
                        m_SerialNumber = value;
                        RaisePropertyChanged("SerialNumber");
                    }
                }
            }

            private String m_Title = "";
            /// <summary>
            /// STP Title
            /// </summary>
            [DBColumn("Title", 255, false,true, false, false, 0, 0)]
            public String Title
            {
                get { return m_Title; }
                set 
                {
                    if (m_Title != value)
                    {
                        m_Title = value;
                        RaisePropertyChanged("Title");
                    }
                }
            }

            private String m_Version = "";
            /// <summary>
            /// STP Version
            /// </summary>
            [DBColumn("STP Version", 255, false, true, false, false, 0, 0)]
            public String Version
            {
                get { return m_Version; }
                set 
                {
                    if (m_Version != value)
                    {
                        m_Version = value;
                        RaisePropertyChanged("Version");
                    }
                }
            }

            [DBColumn(false)]
            private DateTime m_dtCreatedDate = new DateTime();
            [DBColumn("Created Date", -1, false, false, false, false, 0, 0)]
            public DateTime CreatedDate
            {
                get { return m_dtCreatedDate; }
                set { m_dtCreatedDate = value; }
            }

            private String m_UserComments = "";
            /// <summary>
            /// Any comments about current STP user wants to add
            /// </summary>
            [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
            public String UserComments
            {
                get { return m_UserComments; }
                set
                {
                    if (m_UserComments != value)
                    {
                        m_UserComments = value;
                        RaisePropertyChanged("UserComments");
                    }
                }
            }

            [DBColumn(false)]
            public override string TableName
            {
                get
                {
                    return m_TableName;
                }          
            }
        #endregion

        #region Constructors
            /// <summary>
            /// Base Empty constructor. 
            /// </summary>
            public STP_Logic() : 
                base()
            {
                m_TableName = TABLE_NAME;
                NS_DAL.DAL.GetInstance().CheckDataBaseColumns<STP_Logic>(this, false, true);
                InitValues();
            }

            /// <summary>
            /// Constructor with STP description parameters. 
            /// </summary>
            public STP_Logic(Int32 inSTP_ID) :
                this()
            {
                this.ID = inSTP_ID;
                LoadData();
            }

            /// <summary>
            /// Constructor with STP description parameters. 
            /// </summary>
            public STP_Logic(Int32 inSTP_ID ,String inTitle) :
                this()
            { 
                ID = inSTP_ID;
                m_Title  = inTitle;
            }

            public STP_Logic(String inTitle) :
                this(-1, inTitle)
            {
            }
        #endregion

        #region Public overridden methods
            /// <summary>
            /// Return the class description string
            /// </summary>
            /// <returns>Class description text</returns>
            /// <remarks></remarks>
            public override string ClassDescription()
            {
                return "STP Details";
            }

            /// <summary>
            /// Return the selected STP name
            /// </summary>
            /// <returns>Return the selected STP name, or "..." in case no item selected</returns>
            /// <remarks></remarks>
            public override string DataDescription()
            {
                if (m_ID == 0 )
                    return "...";
                return m_Name;
            }

            /// <summary>
            /// Updates data in database
            /// </summary>
            /// <remarks></remarks>
            public override void UpdateData()
            {
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                    NS_DAL.DAL.GetInstance().Update<STP_Logic>(this, false);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_TitleNotSetException;
                            }
                            if (ex.Message.Contains("#Num"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_SerialNumberNotSetException;
                            }
                            if (ex.Message.Contains("STP Version"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_VersionNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_TitleException;
                            }
                            if (ex.Message.Contains("#Num"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_SerialNumberException;
                            }
                            if (ex.Message.Contains("STP Version"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_VersionException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                }
            }

            /// <summary>
            /// Load data for selected STP ID
            /// </summary>
            /// <remarks></remarks>
            public override void LoadData()
            {
                STP_Logic logic = new STP_Logic();
                NS_DAL.DAL.GetInstance().LoadDataTable<STP_Logic>(ref logic, ID);
                Clone2This(logic);
            }

            /// <summary>
            /// Delete row from database
            /// </summary>
            /// <remarks></remarks>
            public override void DeleteData()
            {
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                    NS_DAL.DAL.GetInstance().Delete<STP_Logic>(this);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                            throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                    }
                }
            }

            /// <summary>
            /// Insert new row with data, to database
            /// </summary>
            /// <remarks></remarks>
            public override void InsertData()
            {
            
                try
                {
                    Object RecordID;
                    NS_DAL.DAL.GetInstance().Insert<STP_Logic>(this, true, out RecordID);
                    this.ID = Convert.ToInt32(RecordID);
                }
                catch (System.Data.SqlClient.SqlException ex) 
                {
                    switch (ex.Number) 
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_TitleNotSetException; 
                            }
                            if (ex.Message.Contains("#Num"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_SerialNumberNotSetException;
                            }
                            if (ex.Message.Contains("STP Version"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.STP_VersionNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Title"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_TitleException;
                            }
                            if (ex.Message.Contains("#Num"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_SerialNumberException;
                            }
                            if (ex.Message.Contains("STP Version"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_VersionException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw new Exception(ex.Message);
                    }
                }
            }

            /// <summary>
            /// Base initialization function. Set to 0/"" all values
            /// </summary>
            /// <remarks></remarks>
            public override void InitValues()
            {
                InitValuesToDefaults(); 
            }

            /// <summary>
            /// Get search result by one or more parameters
            /// </summary>
            /// <returns>All found rows corresponding to the search request</returns>
            /// <remarks></remarks>
            public override System.Data.DataView GetSearchResults()
            {
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<STP_Logic>(this, out DT);
                return DT.DefaultView;
            }

            /// <summary>
            /// Return null :)
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public override System.Data.DataTable GetComboSource()
            {
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectAllToDataTable<STP_Logic>(this, out DT);
                return DT;
            }

            public override void SetDefaultValuesToNotSetProperties()
            {
               //throw new NotImplementedException();
            }

            public override void ClearLastSearchResults()
            {
                m_ID = 0;
                m_dtCreatedDate = default(DateTime);
            }
        #endregion
    }
}
