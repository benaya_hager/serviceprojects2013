﻿using System;
using System.Data;
using System.Linq;
using NS_Common.Service;
using NS_DAL.Service;

namespace NS_BLL
{
    /// <summary>
    /// User descriptor
    /// </summary>
    public class UserLogic : Base_Classes.BaseLogicClass,
                              System.Security.Principal.IPrincipal,
                              System.Security.Principal.IIdentity
    {
        #region Private Const

        private const String TABLE_NAME = "Users";
        private const String ENCRIPTION_KEY = "I!f# *``t^h&ese components are already installed, you can launch the application now. Otherwise, click the button below to install the prerequisites and run the applica33$t%i-o+n.";

        private String m_EncryptedPassword = "";//Used to store encrypted value of password
        //private String m_RetypedPassword = ""; // Used to store the value written in confirmation password area

        #endregion Private Const

        #region Public Events

        #region User Changed Event

        /// <summary>
        /// The events delegate function. Occurs when new UserLogic loaded.
        /// </summary>
        /// <param name="Sender">The control that raised event handler</param>
        /// <param name="args">Not in use</param>
        public delegate void UserChangedHandler(NS_BLL.UserLogic ul);
        /// <summary>
        /// Occurs when new UserLogic loaded
        /// </summary>
        private event UserChangedHandler UserChangedDelegate;

        /// <summary>
        /// Service function, used to check if there is handler for raised event declared.
        /// </summary>
        /// <param name="Sender">UserLogic class</param>
        private void OnUserChanged()
        {
            if (UserChangedDelegate != null)
                UserChangedDelegate(this);
        }

        public event UserChangedHandler UserChanged
        {
            // this is the equivalent of CurrentPositionChanged += new EventHandler(...)
            add
            {
                if (this.UserChangedDelegate == null || !this.UserChangedDelegate.GetInvocationList().Contains(value))
                {
                    UserChangedDelegate += value;
                }
            }
            // this is the equivalent of CurrentPositionChanged -= new EventHandler(...)
            remove
            {
                UserChangedDelegate -= value;
            }
        }

        #endregion User Changed Event

        #endregion Public Events

        #region Public properties

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }
        }

        #region IPrincipal Members

        [DBColumn(false)]
        public System.Security.Principal.IIdentity Identity
        {
            get { return this; }
        }


        #endregion IPrincipal Members

        #region IIdentity Members

        [DBColumn(false)]
        string System.Security.Principal.IIdentity.AuthenticationType
        {
            get { return "Custom Authentication"; }
        }

        [DBColumn(false)]
        public string AuthenticationType
        {
            get { return "Custom Authentication"; }
        }

        [DBColumn(false)]
        private Boolean m_AuthenticatedValue = false;

        [DBColumn(false)]
        public bool IsAuthenticated
        {
            get { return m_AuthenticatedValue; }
            set { m_AuthenticatedValue = value; }
        }

        [DBColumn(false)]
        string System.Security.Principal.IIdentity.Name
        {
            get { return (String.IsNullOrEmpty(m_Name) ? this.DataDescription() : m_Name); }
        }

        [DBColumn(false)]
        public new String Name
        {
            get { return (String.IsNullOrEmpty(m_Name) ? this.DataDescription() : m_Name); }
        }

        #endregion IIdentity Members

        [DBColumn(false)]
        private Boolean m_isActive = true;
        [DBColumn("Is Active", -1, false, false, false, false, 0, 0)]
        public Boolean isActive
        {
            get { return m_isActive; }
            set { m_isActive = value; }
        }

        /// <summary>
        /// Used to store local value of user Login name
        /// </summary>
        [DBColumn(false)]
        private String m_Login = "";
        /// <summary>
        /// Get/Set the user Login name
        /// </summary>
        [DBColumn("User Login", 255, false, true, false, false, 0, 0)]
        public String Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }

        /// <summary>
        /// Used to store local value of user Password
        /// </summary>
        [DBColumn(false)]
        private String m_Password = "";
        /// <summary>
        /// Get/Set the user password
        /// </summary>
        [DBColumn("User Password", 255, false, false, true, false, 0, 0)]
        public String Password
        {
            get { return m_EncryptedPassword; }
            set
            {
                m_Password = value;
                m_EncryptedPassword = EncryptDecrypt(m_Password);
                //m_Password = EncryptDecrypt(m_EncryptedPassword);
            }
        }

        /// <summary>
        /// Used to store local value of user last name
        /// </summary>
        [DBColumn(false)]
        private String m_LastName = "";
        /// <summary>
        /// Get/Set user last name
        /// </summary>
        [DBColumn("Last Name", 255, false, false, true, false, 0, 0)]
        public String Last_Name
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        /// <summary>
        /// Used to store local value of user first name
        /// </summary>
        [DBColumn(false)]
        private String m_FirstName = "";
        /// <summary>
        /// Get/Set user first name
        /// </summary>
        [DBColumn("First Name", 255, false, false, true, false, 0, 0)]
        public String First_Name
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        /// <summary>
        /// Used to store local value of user description text
        /// </summary>
        [DBColumn(false)]
        private String m_Comments = "";
        /// <summary>
        /// Get/Set user description text
        /// </summary>
        [DBColumn("User Comments", 255, false, false, true, false, 0, 0)]
        public String Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        [DBColumn(false)]
        private DateTime m_dtChangeDate = new DateTime();
        [DBColumn("Last Change Date", -1, false, false, true, false, 0, 0)]
        public DateTime ChangeDate
        {
            get { return m_dtChangeDate; }
            set { m_dtChangeDate = value; }
        }


        [DBColumn(false)]
        public String strUserPermissionsLevel
        {
            get { return Tools.String_Enum.StringEnum.GetStringValue(m_UserPermissionsLevel); }
            set
            {
                if ((string.IsNullOrEmpty(value) | value == null)) return;
                m_UserPermissionsLevel = (NS_Common.Service._UserPermissionsLevel)Tools.String_Enum.StringEnum.Parse(typeof(NS_Common.Service._UserPermissionsLevel), value);
            }
        }

        [DBColumn(false)]
        private _UserPermissionsLevel m_UserPermissionsLevel = _UserPermissionsLevel.User_Unknown;
        [System.ComponentModel.DefaultValue(_UserPermissionsLevel.User_Unknown),
        DBColumn("User Permissions Level", -1, false, false, false, false, 0, 0)]
        public _UserPermissionsLevel UserPermissionsLevel
        {
            get { return m_UserPermissionsLevel; }
            set { m_UserPermissionsLevel = value; }
        }


        [DBColumn(false)]
        private _MachinesType m_MachinesType = _MachinesType.Machine_Unknown;
        [System.ComponentModel.DefaultValue(_MachinesType.Machine_Unknown),
        DBColumn(false)]
        public _MachinesType MachinesType
        {
            get { return m_MachinesType; }
            set { m_MachinesType = value; }
        }


        #endregion Public properties

        #region Constructors

        /// <summary>
        /// Base Empty constructor.
        /// </summary>
        public UserLogic() :
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<UserLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with User description parameters.
        /// </summary>
        public UserLogic(Int32 inUserID) :
            this()
        {
            this.ID = inUserID;
            LoadData();
        }

        /// <summary>
        /// Constructor with User description parameters.
        /// </summary>
        public UserLogic(String inName)
            : this()
        {
            m_Name = inName;
        }

        public UserLogic(String inName, String inPassword)
            : this()
        {
            m_Name = inName;
            Password = inPassword;

            m_AuthenticatedValue = IsValidNameAndPassword(m_Name, Password);
            if (m_AuthenticatedValue)
            {
                LoadData();
                OnUserChanged();
                m_AuthenticatedValue = true;
            }
        }

        #endregion Constructors

        #region Private Methods

        private bool IsValidNameAndPassword(string username, string password)
        {
            DataRow myUserRow = default(DataRow);
            try
            {
                UserLogic LogicParams = new UserLogic();
                DataTable Result;
                LogicParams.Login = username.Trim();
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<UserLogic>(LogicParams, out Result);

                if (Result == null && NS_DAL.DAL.GetInstance().WorkDisconnected)
                {
                    this.First_Name = "Unknown";
                    this.Last_Name = "Unknown";
                    this.UserPermissionsLevel = _UserPermissionsLevel.User_Unknown;
                    this.m_ID = 1;
                    return true;
                }
                else if (Result == null)
                    return false;

                if (Result.Rows.Count > 0)
                    myUserRow = Result.Rows[0];
                else
                    return false;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.NO_CONNECTION_TO_SQL_SERVER_ERROR: 
                        throw NS_Common.Error_Handling.ExceptionsList.NoConnectionToSQLServerException;
                    default:
                        throw ex;
                }
            }


            if (myUserRow == null) return false;
            string myPass = (myUserRow["User Password"] == System.DBNull.Value ? string.Empty : myUserRow["User Password"]).ToString();
            if (string.IsNullOrEmpty(myPass)) throw NS_Common.Error_Handling.ExceptionsList.PasswordNotSetException;
            this.ID = (Int32)myUserRow["ID"];
            // MessageBox.Show("user: " & username & " right password : " & EncryptDecrypt(myPass) & " typed password : " & password & " , " & myPass.Equals(EncryptDecrypt(password)))
            return myPass.Equals(password);
        }

        //private void OnUserChanged()
        //{
        //    if (UserChangedEvent != null)
        //        UserChangedEvent(this, null);
        //}

        #endregion Private Methods

        #region Public Functions

        public void Validate()
        {
            m_AuthenticatedValue = IsValidNameAndPassword(m_Name, m_Password);
        }

        public string EncryptDecrypt(string passWord)
        {
            char[] PassWordCharArray = passWord.ToCharArray();
            char[] EncriptionKeyCharArray = ENCRIPTION_KEY.ToCharArray();
            string EncriptedPassWord = "";

            for (Int32 x = 0; x <= PassWordCharArray.Length - 1; x++)
            {
                EncriptedPassWord += Convert.ToChar(PassWordCharArray[x] ^ EncriptionKeyCharArray[x]).ToString();
            }

            return EncriptedPassWord;
        }

        #endregion Public Functions

        #region overridden Functions

        #region IPrincipal Members

        [DBColumn(false)]
        public bool IsInRole(string role)
        {
            return role == Tools.String_Enum.StringEnum.GetStringValue(m_UserPermissionsLevel);
        }

        #endregion IPrincipal Members

        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "User Details";
        }

        /// <summary>
        /// Return the selected Login name
        /// </summary>
        /// <returns>Return the selected Product name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            return this.m_Login;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                this.ChangeDate = DateTime.Now;

                NS_DAL.DAL.GetInstance().Update<UserLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Login"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.LoginMissingException;
                            }
                            if (ex.Message.Contains("Password"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.PasswordNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("CK_Users_User_Login"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateLoginNameException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Product ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            UserLogic logic = new UserLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<UserLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                NS_DAL.DAL.GetInstance().Delete<UserLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            try
            {
                Object RecordID;
                this.ChangeDate = DateTime.Now;
                NS_DAL.DAL.GetInstance().Insert<UserLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Login"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.LoginMissingException;
                            }
                            if (ex.Message.Contains("Password"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.PasswordNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("CK_Users_User_Login"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateLoginNameException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults();
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<UserLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override DataTable GetComboSource()
        {
            return null;
        }

        #endregion overridden Functions
    }
}