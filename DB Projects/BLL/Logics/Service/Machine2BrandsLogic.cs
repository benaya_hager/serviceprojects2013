﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using Common.Service;

namespace NS_BLL.Logics.Service
{
    class MachineType2BrandsLogic : Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Machine Type To Brands";
#endregion

#region Public properties
        private Common.Service._MachinesType m_Machine_Type = Common.Service._MachinesType.Machine_Unknown;
        [DBColumn("Machine Type", -1, false, false, false, false, 0, 0)]
        public Common.Service._MachinesType Machine_Type
        {
            get { return m_Machine_Type; }
            set { m_Machine_Type = value; }
        }

        /// <summary>
        /// Used for automatic fields Binding
        /// </summary>
        [DBColumn(false)]
        public String strMachine_Type
        {
            get { return Tools.String_Enum.StringEnum.GetStringValue(m_Machine_Type); }
            set
            {
                if ((string.IsNullOrEmpty(value) | value == null)) return;
                m_Machine_Type = (Common.Service._MachinesType)Tools.String_Enum.StringEnum.Parse(typeof(Common.Service._MachinesType), value);
            }
        }

        [DBColumn(false)]
        private Int32 m_BrandID = -1;
        /// <summary>
        /// Set/Get value product Class/Brand. The value indicate ID in a table of Brands
        /// </summary>
        [DBColumn("Class/Brand ID", _DB_FIELD_TYPE.OTHER_TABLE_ID, "Brands", "Brand Name", "BrandName")]
        public Int32 BrandID
        {
            get { return m_BrandID; }
            set { m_BrandID = value; }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public MachineType2BrandsLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<MachineType2BrandsLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Machine Type To Brand description parameters. 
        /// </summary>
        public MachineType2BrandsLogic(Int32 inMachineType2BrandID) :
            this()
        {
            this.ID = inMachineType2BrandID;
            LoadData();
        }        
#endregion

#region Public overriden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Machine Type To Brand Details";
        }

        /// <summary>
        /// Return the selected Machine name
        /// </summary>
        /// <returns>Return the selected Machine name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Update <MachineType2BrandsLogic>(this,false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {                        
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {                       
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Product ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            MachineType2BrandsLogic logic = new MachineType2BrandsLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<MachineType2BrandsLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<MachineType2BrandsLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {   
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<MachineType2BrandsLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<MachineType2BrandsLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            return null;
        }
#endregion
    }
}
