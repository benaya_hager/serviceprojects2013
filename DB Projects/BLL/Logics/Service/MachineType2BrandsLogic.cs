﻿using System;
using System.Linq;
using NS_Common.Service;
using NS_DAL.Service;

namespace NS_BLL.Logics.Service
{
    public class MachineType2BrandsLogic : Base_Classes.BaseLogicClass
    {
        #region Private Const

        private const String TABLE_NAME = "Machine Type To Brands";

        #endregion Private Const

        #region Public properties

        private NS_Common.Service._MachinesType m_Machine_Type = NS_Common.Service._MachinesType.Machine_Unknown;
        [DBColumn("Machine Type", -1, false, false, false, false, 0, 0)]
        public NS_Common.Service._MachinesType Machine_Type
        {
            get { return m_Machine_Type; }
            set
            {
                m_Machine_Type = value;
                NS_DAL.DAL.GetInstance().SelectByParamsToField<MachineType2BrandsLogic>(this, "AvailibleBrandsList", out m_AvailibleBrandsList);
                OnCheckedItemsChanged(this);
            }
        }

        /// <summary>
        /// Used for automatic fields Binding
        /// </summary>
        [DBColumn(false)]
        public String strMachine_Type
        {
            get { return Tools.String_Enum.StringEnum.GetStringValue(m_Machine_Type); }
            set
            {
                if ((string.IsNullOrEmpty(value) | value == null)) return;
                Machine_Type = (NS_Common.Service._MachinesType)Tools.String_Enum.StringEnum.Parse(typeof(NS_Common.Service._MachinesType), value);
            }
        }

        [DBColumn(false)]
        private Int32 m_BrandID = -1;
        /// <summary>
        /// Set/Get value product Class/Brand. The value indicate ID in a table of Brands
        /// </summary>
        [DBColumn("Class/Brand ID", _DB_FIELD_TYPE.OTHER_TABLE_ID, "Brands", "Brand Name", "BrandName")]
        public Int32 BrandID
        {
            get { return m_BrandID; }
            set { m_BrandID = value; }
        }

        [DBColumn(false)]
        private String m_BrandName = "";
        /// <summary>
        /// Set/Get value for catalog number of product
        /// </summary>
        [DBColumn(false)]
        public String BrandName
        {
            get { return m_BrandName; }
            set { m_BrandName = value; }
        }

        [DBColumn(false)]
        private System.Data.DataTable m_AvailibleBrandsList = null;
        /// <summary>
        /// Get the list of Brands that set as usable for current machine.
        /// </summary>
        [DBColumn(_DB_FIELD_TYPE.SINGLE_TO_MANY,
                  "SelectBrandsByMachineType", "", "", "",
                  "Machine_Type")]
        public System.Data.DataTable AvailibleBrandsList
        {
            get
            {
                if (null == m_AvailibleBrandsList)
                {
                    NS_DAL.DAL.GetInstance().SelectByParamsToField<MachineType2BrandsLogic>(this, "AvailibleBrandsList", out m_AvailibleBrandsList);
                }
                return m_AvailibleBrandsList;
            }
            //private set { m_AvailibleBrandsList = value; }
        }


        [DBColumn(false)]
        private System.Data.DataSet m_Brands = null;
        /// <summary>
        /// Set/Get value product Class/Brand. The value indicate ID in a table of Brands
        /// </summary>
        [DBColumn(false)]
        public System.Data.DataSet BrandsList
        {
            get
            {
                if (null == m_Brands)
                {
                    NS_BLL.Logics.Product.BrandsLogic lgc = new NS_BLL.Logics.Product.BrandsLogic();
                    m_Brands = lgc.GetComboSourceDS();
                }
                return m_Brands;
            }
            //private set { m_AvailibleBrandsList = value; }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }
        }

        #endregion Public properties

        #region Constructors

        /// <summary>
        /// Base Empty constructor.
        /// </summary>
        public MachineType2BrandsLogic() :
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<MachineType2BrandsLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Machine Type To Brand description parameters.
        /// </summary>
        public MachineType2BrandsLogic(Int32 inMachineType2BrandID) :
            this()
        {
            this.ID = inMachineType2BrandID;
            LoadData();
        }

        #endregion Constructors

        #region Public overridden methods

        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Machine Type To Brand Details";
        }

        /// <summary>
        /// Return the selected Machine name
        /// </summary>
        /// <returns>Return the selected Machine name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0)
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;


                NS_DAL.DAL.GetInstance().Update<MachineType2BrandsLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Product ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            MachineType2BrandsLogic logic = new MachineType2BrandsLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<MachineType2BrandsLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<MachineType2BrandsLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<MachineType2BrandsLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults();
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<MachineType2BrandsLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            return null;
        }

        #endregion Public overridden methods

        #region Public Events

        #region Checked Items changed event

        /// <summary>
        /// Define an event handler delegate
        ///     The events delegate function. Occurs when a binding operation is complete, such as when data is pushed
        /// to the control property from the data source or vice versa
        /// </summary>
        /// <param name="Sender">The control that raised event handler </param>
        /// <param name="args"></param>
        public delegate void CheckedItemsChangedHandler(Object Sender, EventArgs args);
        /// <summary>
        /// Declare an event handler delegate
        /// </summary>
        private CheckedItemsChangedHandler CheckedItemsChangedDelegate;

        protected void OnCheckedItemsChanged(object Sender)
        {
            if (this.CheckedItemsChangedDelegate != null)
            {
                this.CheckedItemsChangedDelegate(Sender, new EventArgs());
            }
        }

        /// <summary>
        /// re-define the CheckedItemsChanged event
        /// </summary>
        public event CheckedItemsChangedHandler CheckedItemsChanged
        {
            // this is the equivalent of CheckedItemsChanged += new EventHandler(...)
            add
            {
                //if (this.CheckedItemsChangedDelegate != null)
                //{
                //    CheckedItemsChanged -= this.CheckedItemsChangedDelegate;
                //    this.CheckedItemsChangedDelegate = null;
                //}
                if (this.CheckedItemsChangedDelegate == null || !this.CheckedItemsChangedDelegate.GetInvocationList().Contains(value))
                {
                    this.CheckedItemsChangedDelegate += value;
                }
            }
            // this is the equivalent of CheckedItemsChanged -= new EventHandler(...)
            remove
            {
                this.CheckedItemsChangedDelegate -= value;
            }
        }

        #endregion Checked Items changed event

        #endregion Public Events

        #region Public Methods

        public void ResetAvailibleBrandsList()
        {
            m_AvailibleBrandsList = null;
        }

        #endregion Public Methods
    }
}