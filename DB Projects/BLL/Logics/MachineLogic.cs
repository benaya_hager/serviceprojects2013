﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools.String_Enum;
using NS_DAL.Service;

namespace NS_BLL 
{
    /// <summary>
    /// Machine descriptor
    /// </summary>
    public class MachineLogic : Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Machines";
#endregion

#region Public properties
        private String m_Machine_Name = "";
        /// <summary>
        /// The Test unit name (Machine name)
        /// </summary>
        [DBColumn("Machine Name", 255, false,false, false, false, 0, 0)]
        public String Machine_Name
        {
            get { return m_Machine_Name; }
            set { m_Machine_Name = value; }
        }
                
        private String m_Machine_Location = "";
        /// <summary>
        /// The Test unit Location (Machine location - R&D Lab/Production/etc.)
        /// </summary>
        [DBColumn("Machine Location", 255, false, false, true, false, 0, 0)]
        public String Machine_Location
        {
            get { return m_Machine_Location; }
            set { m_Machine_Location = value; }
        }

        private Common.Service._MachinesType m_Machine_Type = Common.Service._MachinesType.Machine_Unknown;
        [DBColumn("Machine Type", -1, false, false, true, false, 0, 0)]
        public Common.Service._MachinesType Machine_Type
        {
            get { return m_Machine_Type; }
            set { m_Machine_Type = value; }
        }

        /// <summary>
        /// Used for automatic fields Binding
        /// </summary>
        [DBColumn(false)]
        public String strMachine_Type
        {
            get { return Tools.String_Enum.StringEnum.GetStringValue(m_Machine_Type); }
            set
            {
                if ((string.IsNullOrEmpty(value) | value == null)) return;
                m_Machine_Type = (Common.Service._MachinesType)Tools.String_Enum.StringEnum.Parse(typeof(Common.Service._MachinesType), value);
            }
        }

        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current machine user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public MachineLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<MachineLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with machine description parameters. 
        /// </summary>
        public MachineLogic(Int32 inMachineID) :
            this()
        {
            this.ID = inMachineID;
            LoadData();
        }


        /// <summary>
        /// Constructor with machine description parameters. 
        /// </summary>
        public MachineLogic(Int32 inMachineID ,String inMachineName, String inMachineLocation) :
            this()
        { 
            ID = inMachineID;
            Machine_Location = inMachineLocation;
            Machine_Name = inMachineName;
        }

        public MachineLogic(String inMachineName, String inMachineLocation) :
            this(-1, inMachineName, inMachineLocation)
        {
        }
#endregion

#region Public overriden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Machine Details";
        }

        /// <summary>
        /// Return the selected Machine name
        /// </summary>
        /// <returns>Return the selected Machine name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Update < MachineLogic>(this,false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Machine Name"))
                        {
                            throw Common.Error_Handling.ExceptionsList.MachineNameNotSetException;
                        }
                        if (ex.Message.Contains("Machine Location"))
                        {
                            throw Common.Error_Handling.ExceptionsList.MachineLocationNotSetException;
                        }
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Machine Name"))
                        {
                            throw Common.Error_Handling.ExceptionsList.DuplicateMachineNameException;
                        }
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected machine ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            MachineLogic logic = new MachineLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<MachineLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<MachineLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {   
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<MachineLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Machine Name"))
                        {
                            throw Common.Error_Handling.ExceptionsList.MachineNameNotSetException; 
                        }
                        if (ex.Message.Contains("Machine Location"))
                        {
                            throw Common.Error_Handling.ExceptionsList.MachineLocationNotSetException;
                        }
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Machine Name"))
                        {
                            throw Common.Error_Handling.ExceptionsList.DuplicateMachineNameException;
                        }
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<MachineLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            return null;
        }
#endregion
    }
}
