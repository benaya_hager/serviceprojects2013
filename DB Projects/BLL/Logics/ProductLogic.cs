﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using System.Data;
using Common.Service;

namespace NS_BLL
{
    public class ProductLogic : Base_Classes.BaseLogicClass
    {
        #region Private Const
            private const String TABLE_NAME = "Products";
        #endregion

        #region Constructors
            /// <summary>
            /// Base Empty constructor. 
            /// </summary>
            public ProductLogic() : 
                base()
            {
                m_TableName = TABLE_NAME; 
                NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProductLogic>(this, false, true);
                InitValues();
            }

            /// <summary>
            /// Constructor with Product description parameters. 
            /// </summary>
            public ProductLogic(Int32 inProductID) :
                this()
            {
                this.ID = inProductID;
                LoadData();
            }
        #endregion

        #region Public properties
            [DBColumn(false)]
            public override string TableName
            {
                get
                {
                    return m_TableName;
                }
            }        
           
            private _ProductBrand m_Product_Brand = _ProductBrand.Product_Unknown ;
            /// <summary>
            /// Set/Get value product Class/Brand
            /// </summary>
            [DBColumn("Product Class/Brand", -1, false, false, false, false, 0, 0)]
            public _ProductBrand Product_Brand
            {
                get { return m_Product_Brand; }
                set { m_Product_Brand = value; }
            }
            /// <summary>
            /// Used for automatic fields Binding
            /// </summary>
            [DBColumn(false)]
            public String strProduct_Brand
            {
                get { return Tools.String_Enum.StringEnum.GetStringValue(m_Product_Brand); }
                set
                {
                    if ((string.IsNullOrEmpty(value) | value == null)) return;
                    m_Product_Brand = (Common.Service._ProductBrand)Tools.String_Enum.StringEnum.Parse(typeof(Common.Service._ProductBrand), value);
                }
            }

            private String m_Catalog_Number = "";
            /// <summary>
            /// Set/Get value for catalog number of product
            /// </summary>
            [DBColumn("Catalog Number", 255, false, true, false, false, 0, 0)]
            public String Catalog_Number
            {
                get { return m_Catalog_Number; }
                set { m_Catalog_Number = value; }
            }

            private String m_SizeOrType = "";
            /// <summary>
            ///Set/Get value for product Size Or Type
            /// </summary>
            [DBColumn("Size/Type", 255, false, false, true, false, 0, 0)]
            public String SizeOrType
            {
                get { return m_SizeOrType; }
                set { m_SizeOrType = value; }
            }

            private String m_Description = "";
            /// <summary>
            ///Set/Get value for product description. Such as Covered\Clean\etc.
            /// </summary>
            [DBColumn("Description", 1000, false, false, true, false, 0, 0)]
            public String Description
            {
                get { return m_Description; }
                set { m_Description = value; }
            }

            private String m_UserComments = "";
            /// <summary>
            ///Set/Get value for user description, or comments
            /// </summary>
            [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
            public String UserComments
            {
                get { return m_UserComments; }
                set { m_UserComments = value; }
            }
        #endregion

        #region Overriden Functions
            /// <summary>
            /// Return the class description string
            /// </summary>
            /// <returns>Class description text</returns>
            /// <remarks></remarks>
            public override string ClassDescription()
            {
                return "Product Details";
            }

            /// <summary>
            /// Return the selected Product name
            /// </summary>
            /// <returns>Return the selected Machine name, or "..." in case no item selected</returns>
            /// <remarks></remarks>
            public override string DataDescription()
            {
                if (m_ID == 0)
                    return "...";
                return m_Name;
            }

            /// <summary>
            /// Updates data in database
            /// </summary>
            /// <remarks></remarks>
            public override void UpdateData()
            {
                try
                {
                    NS_DAL.DAL.GetInstance().Update<ProductLogic>(this, false);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            //if (ex.Message.Contains("Login"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.LoginMissingException;
                            //}
                            //if (ex.Message.Contains("Password"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.PasswordNotSetException;
                            //}
                            throw Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            //if (ex.Message.Contains("CK_Users_User_Login"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.DuplicateLoginNameException;
                            //}
                            throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                }
            }

            /// <summary>
            /// Load data for selected Product ID
            /// </summary>
            /// <remarks></remarks>
            public override void LoadData()
            {
                ProductLogic logic = new ProductLogic();
                NS_DAL.DAL.GetInstance().LoadDataTable<ProductLogic>(ref logic, ID);
                Clone2This(logic);
            }

            /// <summary>
            /// Delete row from database
            /// </summary>
            /// <remarks></remarks>
            public override void DeleteData()
            {
                try
                {
                    NS_DAL.DAL.GetInstance().Delete<ProductLogic>(this);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                            throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                        case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw Common.Error_Handling.ExceptionsList.DeleteFailedException;
                    }
                }
            }

            /// <summary>
            /// Insert new row with data, to database
            /// </summary>
            /// <remarks></remarks>
            public override void InsertData()
            {
                try
                {
                    Object RecordID;
                    NS_DAL.DAL.GetInstance().Insert<ProductLogic>(this, true, out RecordID);
                    this.ID = Convert.ToInt32(RecordID);
                }
                catch (System.Data.SqlClient.SqlException ex) 
                {
                    switch (ex.Number) 
                    {
                        case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            //if (ex.Message.Contains("Login"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.LoginMissingException; 
                            //}
                            //if (ex.Message.Contains("Password"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.PasswordNotSetException; 
                            //}
                            throw Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            //if (ex.Message.Contains("CK_Users_User_Login"))
                            //{
                            //    throw Common.Error_Handling.ExceptionsList.DuplicateLoginNameException;
                            //}
                            throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw new Exception(ex.Message);
                    }
                }
            }

            /// <summary>
            /// Base initialization function. Set to 0/"" all values
            /// </summary>
            /// <remarks></remarks>
            public override void InitValues()
            {
                InitValuesToDefaults(); 
            }

            /// <summary>
            /// Get search result by one or more parameters
            /// </summary>
            /// <returns>All found rows corresponding to the search request</returns>
            /// <remarks></remarks>
            public override DataView GetSearchResults()
            {
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProductLogic>(this, out DT);
                return DT.DefaultView;
            }

            /// <summary>
            /// Return null :)
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public override DataTable GetComboSource()
            {
                return null;
            }
        #endregion
    }
}
