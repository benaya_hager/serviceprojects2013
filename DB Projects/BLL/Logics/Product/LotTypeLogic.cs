﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics.Product
{
    /// <summary>
    /// The table that holds availible Lots list
    /// </summary>
    public class CatalogTypeLogic: Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Catalog Types";
#endregion

#region Public properties
        private String m_CatalogTypeCaption = "";
        /// <summary>
        /// Brand name
        /// </summary>
        [DBColumn("Catalog Type Title", 255, false, true, false, false, 0, 0)]
        public String CatalogTypeCaption
        {
            get { return m_CatalogTypeCaption; }
            set { m_CatalogTypeCaption = value; }
        }

        private Common.Service._MachinesType m_Machine_Type = Common.Service._MachinesType.Machine_Unknown;
        [DBColumn("Machine Type", -1, false, false,false, false, 0, 0)]
        public Common.Service._MachinesType Machine_Type
        {
            get { return m_Machine_Type; }
            set { m_Machine_Type = value; }
        }

        /// <summary>
        /// Used for automatic fields Binding
        /// </summary>
        [DBColumn(false)]
        public String strMachine_Type
        {
            get { return Tools.String_Enum.StringEnum.GetStringValue(m_Machine_Type); }
            set
            {
                if ((string.IsNullOrEmpty(value) | value == null)) return;
                m_Machine_Type = (Common.Service._MachinesType)Tools.String_Enum.StringEnum.Parse(typeof(Common.Service._MachinesType), value);
            }
        }

        private Single m_SecurementShimsDiameter = 0.0F;
        /// <summary>
        /// The value used in Securement test's. Store the shims diameter for current lot.
        /// </summary>
        [DBColumn("Securement Shims Diameter", -1, false, false, false, false, 0, 0)]
        public Single SecurementShimsDiameter
        {
            get { return m_SecurementShimsDiameter; }
            set { m_SecurementShimsDiameter = value; }
        }

        private Single m_DeploymentTestLength = 0.0F;
        /// <summary>
        /// The value used in Deployment test's. Store the test length for current lot.
        /// </summary>
        [DBColumn("Deployment Test Length", -1, false, false, true, false, 0, 0)]
        public Single DeploymentTestLength
        {
            get { return m_DeploymentTestLength; }
            set { m_DeploymentTestLength = value; }
        }

        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current brand user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public CatalogTypeLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<CatalogTypeLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Lot Type description parameters. 
        /// </summary>
        public CatalogTypeLogic(Int32 inLotTypeID) :
            this()
        {
            this.ID = inLotTypeID;
            LoadData();
        }

        /// <summary>
        /// Constructor with  Lot Type description parameters. 
        /// </summary>
        public CatalogTypeLogic(Int32 inLotTypeID ,String inLotTitle) :
            this()
        {
            ID = inLotTypeID;
            CatalogTypeCaption = inLotTitle;
        }

        public CatalogTypeLogic(String inLotTitle) :
            this(-1, inLotTitle)
        {
        }
#endregion

#region Public overriden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Lot Details";
        }

        /// <summary>
        /// Return the selected Lot name
        /// </summary>
        /// <returns>Return the selected Brand name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Update<CatalogTypeLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Catalog Type Title"))
                        {
                            throw Common.Error_Handling.ExceptionsList.LotTypeTitleNotSetException;
                        }
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Catalog Type Title"))
                        {
                            throw Common.Error_Handling.ExceptionsList.DuplicateLotTypeTitleException;
                        }
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Brand ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            CatalogTypeLogic logic = new CatalogTypeLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<CatalogTypeLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<CatalogTypeLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {   
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<CatalogTypeLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Catalog Type Title"))
                        {
                            throw Common.Error_Handling.ExceptionsList.LotTypeTitleNotSetException;
                        }
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Catalog Type Title"))
                        {
                            throw Common.Error_Handling.ExceptionsList.DuplicateLotTypeTitleException;
                        }
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<CatalogTypeLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return the full list of brands from table Lot types
        /// </summary>
        /// Return the full list of brands from table Lot types
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<CatalogTypeLogic>(this, out DT);
            return DT;
        }
#endregion
    }
}
