﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics.Product
{
    /// <summary>
    /// The table that holds brands list
    /// </summary>
    public class BrandsLogic : Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Brands";
#endregion

#region Public properties
        private String m_Brand_Name = "";
        /// <summary>
        /// Brand name
        /// </summary>
        [DBColumn("Brand Name", 255, false,true, false, false, 0, 0)]
        public String Brand_Name
        {
            get { return m_Brand_Name; }
            set 
            { 
                m_Brand_Name = value;
                RaisePropertyChanged("Brand_Name");
            }
        }
        
 

        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current brand user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set 
            { 
                m_UserComments = value;
                RaisePropertyChanged("UserComments");
            }
        }


        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public BrandsLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<BrandsLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Brand description parameters. 
        /// </summary>
        public BrandsLogic(Int32 inBrandID) :
            this()
        {
            this.ID = inBrandID;
            LoadData();
        }

        /// <summary>
        /// Constructor with Brand description parameters. 
        /// </summary>
        public BrandsLogic(Int32 inBrandID ,String inBrandName) :
            this()
        { 
            ID = inBrandID;
            Brand_Name = inBrandName;
        }

        public BrandsLogic(String inBrandName) :
            this(-1, inBrandName)
        {
        }
#endregion

#region Public overridden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Brand Details";
        }

        /// <summary>
        /// Return the selected Brand name
        /// </summary>
        /// <returns>Return the selected Brand name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<BrandsLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Brand Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.BrandNameNotSetException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Brand Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateBrandNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Brand ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            BrandsLogic logic = new BrandsLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<BrandsLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<BrandsLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<BrandsLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Brand Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.BrandNameNotSetException; 
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Brand Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateBrandNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<BrandsLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return the full list of brands from table Brands
        /// </summary>
        /// Return the full list of brands from table Brands
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectAllToDataTable<BrandsLogic>(this, out DT);
            return DT;
        }

        public   System.Data.DataSet GetComboSourceDS()
        {
            System.Data.DataSet DS;
            NS_DAL.DAL.GetInstance().SelectAllToDataTableDS<BrandsLogic>(this, out DS);
            return DS;
        }
#endregion
    }
}
