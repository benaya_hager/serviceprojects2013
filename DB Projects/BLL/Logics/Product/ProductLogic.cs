﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;
using System.Data;
using NS_Common.Service;
using NS_BLL.Logics;

namespace NS_BLL.Logics.Product
{
    public class ProductLogic : Base_Classes.BaseLogicClass
    {
        #region Private Const
            private const String TABLE_NAME = "Products";
        #endregion

        #region Constructors
            /// <summary>
            /// Base Empty constructor. 
            /// </summary>
            public ProductLogic() : 
                base()
            {
                m_TableName = TABLE_NAME;

                m_BrandsList = new BrandsLogic().GetComboSource();  

                NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProductLogic>(this, false, true);
                InitValues();
            }

            /// <summary>
            /// Constructor with Product description parameters. 
            /// </summary>
            public ProductLogic(Int32 inProductID) :
                this()
            {
                this.ID = inProductID;
                LoadData();
            }
        #endregion

        #region Public properties
            [DBColumn(false)]
            public override string TableName
            {
                get
                {
                    return m_TableName;
                }
            }
            
            
            [DBColumn(false)]
            private DataTable m_BrandsList = null;
            /// <summary>
            
            /// Return the list of Brands
            /// </summary>
            [DBColumn(false)]
            public DataTable BrandsList
            {
                get { return m_BrandsList; }
                set
                { 
                    m_BrandsList = value;
                    RaisePropertyChanged("BrandsList");
                }
            }
            
            

            [DBColumn(false)]
            private Int32 m_BrandID = -1;
            /// <summary>
            /// Set/Get value product Class/Brand. The value indicate ID in a table of Brands
            /// </summary>
            [System.ComponentModel.DefaultValue(1), 
            DBColumn("Class/Brand ID", _DB_FIELD_TYPE.OTHER_TABLE_ID, "Brands", "Brand Name", "BrandName")]
            public Int32 BrandID
            {
                get { return m_BrandID; }
                set 
                { 
                    m_BrandID = value;
                    RaisePropertyChanged("BrandID");
                }
            }

            [DBColumn(false)]
            private String m_BrandName= "";
            /// <summary>
            /// Set/Get value for catalog number of product
            /// </summary>
            [DBColumn(false)]
            public String BrandName
            {
                get { return m_BrandName; }
                set 
                { 
                    m_BrandName = value;
                    RaisePropertyChanged("BrandName");
                }
            }

            ///<summary>
            ///Used for automatic fields Binding
            ///</summary>
            [DBColumn(false)]
            private Single m_ShimsDiameter = 0.8F;
            [DBColumn("Sec: Shims Diameter",  -1, false,false,true,false,0,0)] 
            public Single ShimsDiameter
            {
                get { return m_ShimsDiameter; }
                set
                { 
                    m_ShimsDiameter = value;
                    RaisePropertyChanged("ShimsDiameter");
                }
            }

            [DBColumn(false)]
            private String m_Catalog_Number = "";
            /// <summary>
            /// Set/Get value for catalog number of product
            /// </summary>
            [DBColumn("Catalog Number", 255, false, true, false, false, 0, 0)]
            public String Catalog_Number
            {
                get { return m_Catalog_Number; }
                set
                { 
                    m_Catalog_Number = value;
                    RaisePropertyChanged("Catalog_Number");
                }
            }

            [DBColumn(false)]
            private String m_SizeOrType = "";
            /// <summary>
            ///Set/Get value for product Size Or Type
            /// </summary>
            [DBColumn("Size/Type", 255, false, false, true, false, 0, 0)]
            public String SizeOrType
            {
                get { return m_SizeOrType; }
                set 
                { 
                    m_SizeOrType = value;
                    RaisePropertyChanged("SizeOrType");
                }
            }

            [DBColumn(false)]
            private Boolean m_isProductProductionActive = true;
            /// <summary>
            /// Set\Get the Product production status (is open or closed state)
            /// </summary>
            [DBColumn("Is Product Production Active", -1, false, false, false, false, 0, 0)]
            public Boolean isProductProductionActive
            {
                get { return m_isProductProductionActive; }
                set 
                { 
                    m_isProductProductionActive = value;
                    RaisePropertyChanged("isProductProductionActive");
                }
            }

            [DBColumn(false)]
            private DateTime m_dtLastStateChangeDate = default(DateTime);
            [DBColumn("Last State Change Date", -1, false, false,false , false, 0, 0)]
            public DateTime LastStateChangeDate
            {
                get { return m_dtLastStateChangeDate; }
                set
                { 
                    m_dtLastStateChangeDate = value;
                    RaisePropertyChanged("LastStateChangeDate");
                }
            }

            [DBColumn(false)]
            private String m_Description = "";
            /// <summary>
            ///Set/Get value for product description. Such as Covered\Clean\etc.
            /// </summary>
            [DBColumn("Description", 1000, false, false, true, false, 0, 0)]
            public String Description
            {
                get { return m_Description; }
                set
                { 
                    m_Description = value;
                    RaisePropertyChanged("Description");
                }
            }

            [DBColumn(false)]
            private String m_UserComments = "";
            /// <summary>
            ///Set/Get value for user description, or comments
            /// </summary>
            [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
            public String UserComments
            {
                get { return m_UserComments; }
                set
                { 
                    m_UserComments = value;
                    RaisePropertyChanged("UserComments");
                }
            }
        #endregion

        #region overridden Functions
            /// <summary>
            /// Return the class description string
            /// </summary>
            /// <returns>Class description text</returns>
            /// <remarks></remarks>
            public override string ClassDescription()
            {
                return "Product Details";
            }

            /// <summary>
            /// Return the selected Product name
            /// </summary>
            /// <returns>Return the selected Machine name, or "..." in case no item selected</returns>
            /// <remarks></remarks>
            public override string DataDescription()
            {
                if (m_ID == 0)
                    return "...";
                return m_Name;
            }

            /// <summary>
            /// Updates data in database
            /// </summary>
            /// <remarks></remarks>
            public override void UpdateData()
            {                
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;                    

                    NS_DAL.DAL.GetInstance().Update<ProductLogic>(this, false);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Class/Brand ID"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProductBrandNotSetException;
                            }
                            if (ex.Message.Contains("Catalog Number"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.CatalogNumberNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Catalog Number"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateCatalogNumberException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                }
            }

            /// <summary>
            /// Load data for selected Product ID
            /// </summary>
            /// <remarks></remarks>
            public override void LoadData()
            {
                ProductLogic logic = new ProductLogic();
                NS_DAL.DAL.GetInstance().LoadDataTable<ProductLogic>(ref logic, ID);
                Clone2This(logic);
            }

            /// <summary>
            /// Delete row from database
            /// </summary>
            /// <remarks></remarks>
            public override void DeleteData()
            {
                try
                {
                    if ((this.ID == 1)) // The Unknown
                        throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                    NS_DAL.DAL.GetInstance().Delete<ProductLogic>(this);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    switch (ex.Number)
                    {
                        case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                            throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                    }
                }
            }

            /// <summary>
            /// Insert new row with data, to database
            /// </summary>
            /// <remarks></remarks>
            public override void InsertData()
            {
                
                try
                {
                    Object RecordID;
                    NS_DAL.DAL.GetInstance().Insert<ProductLogic>(this, true, out RecordID);
                    this.ID = Convert.ToInt32(RecordID);
                }
                catch (System.Data.SqlClient.SqlException ex) 
                {
                    switch (ex.Number) 
                    {
                        case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            if (ex.Message.Contains("Class/Brand ID"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.ProductBrandNotSetException;
                            }
                            if (ex.Message.Contains("Catalog Number"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.CatalogNumberNotSetException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                        case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            if (ex.Message.Contains("Catalog Number"))
                            {
                                throw NS_Common.Error_Handling.ExceptionsList.DuplicateCatalogNumberException;
                            }
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                        case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                            throw new Exception("Could not perform desired action due to network problems! Please try again");
                        default:
                            throw new Exception(ex.Message);
                    }
                }
            }

            /// <summary>
            /// Base initialization function. Set to 0/"" all values
            /// </summary>
            /// <remarks></remarks>
            public override void InitValues()
            {
                InitValuesToDefaults(); 
            }

            /// <summary>
            /// Get search result by one or more parameters
            /// </summary>
            /// <returns>All found rows corresponding to the search request</returns>
            /// <remarks></remarks>
            public override DataView GetSearchResults()
            {
                DateTime tmpVal = m_dtLastStateChangeDate;
                m_dtLastStateChangeDate = default(DateTime);  
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProductLogic>(this, out DT);
                m_dtLastStateChangeDate = tmpVal;
                return DT.DefaultView;
            }

            /// <summary>
            /// ReturnFull table results
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public override DataTable GetComboSource()
            {
                DateTime tmpVal = m_dtLastStateChangeDate;
                m_dtLastStateChangeDate = default(DateTime); 
                System.Data.DataTable DT;
                NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProductLogic>(this, out DT);
                m_dtLastStateChangeDate = tmpVal;
                 
                return DT;
            }
 
        #endregion

        #region Public Methods

        #endregion
    }
}
