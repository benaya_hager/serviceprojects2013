﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics
{
    /// <summary>
    /// The table that holds Protocol list
    /// </summary>
    public class ProtocolLogic : Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Protocols";
#endregion

#region Public properties
        private String m_Protocol_Name = "";
        /// <summary>
        /// Protocol name
        /// </summary>
        [DBColumn("Protocol Name", 255, false,true, false, false, 0, 0)]
        public String Protocol_Name
        {
            get { return m_Protocol_Name; }
            set { m_Protocol_Name = value; RaisePropertyChanged("Protocol_Name"); }
        }
         
        private String m_UserComments = "";
        /// <summary>
        /// Any comments about current Protocol user wants to add
        /// </summary>
        [DBColumn("User Comments", 1000, false, false, true, false, 0, 0)]
        public String UserComments
        {
            get { return m_UserComments; }
            set { m_UserComments = value; RaisePropertyChanged("UserComments"); }
        }

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public ProtocolLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<ProtocolLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with Protocol description parameters. 
        /// </summary>
        public ProtocolLogic(Int32 inProtocolID) :
            this()
        {
            this.ID = inProtocolID;
            LoadData();
        }

        /// <summary>
        /// Constructor with Protocol description parameters. 
        /// </summary>
        public ProtocolLogic(Int32 inProtocolID ,String inProtocolName) :
            this()
        { 
            ID = inProtocolID;
            Protocol_Name = inProtocolName;
        }

        public ProtocolLogic(String inProtocolName) :
            this(-1, inProtocolName)
        {
        }
#endregion

#region Public overridden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Protocol Details";
        }

        /// <summary>
        /// Return the selected Protocol name
        /// </summary>
        /// <returns>Return the selected Protocol name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            if (null == m_Protocol_Name || "" == m_Protocol_Name)
            {
                throw NS_Common.Error_Handling.ExceptionsList.ProtocolNameNotSetException;
            } 

            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<ProtocolLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Protocol Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProtocolNameNotSetException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Protocol Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProtocolNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Protocol ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            ProtocolLogic logic = new ProtocolLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<ProtocolLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<ProtocolLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {

            if (null == m_Protocol_Name || "" == m_Protocol_Name)
            {
                throw NS_Common.Error_Handling.ExceptionsList.ProtocolNameNotSetException;
            } 
            
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<ProtocolLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        if (ex.Message.Contains("Protocol Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.ProtocolNameNotSetException; 
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {
                        if (ex.Message.Contains("Protocol Name"))
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateProtocolNameException;
                        }
                        throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<ProtocolLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            ProtocolLogic Lgc = new ProtocolLogic();
            NS_DAL.DAL.GetInstance().SelectAllToDataTable<ProtocolLogic>(Lgc, out DT);
            return DT;  
        }
#endregion
    }
}
