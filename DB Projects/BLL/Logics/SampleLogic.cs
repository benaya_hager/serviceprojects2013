﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_DAL.Service;

namespace NS_BLL.Logics
{
    class SampleLogic: Base_Classes.BaseLogicClass
    {  
#region Private Const 
        private const String TABLE_NAME = "Sample Table";
#endregion

#region Public properties
        private String m_SampleItem = "";
        /// <summary>
        /// SampleItem
        /// </summary>
        [DBColumn("Any Text 4 Field name", 255, false, true, false, false, 0, 0)]
        public String SampleItem
        {
            get { return m_SampleItem; }
            set { m_SampleItem = value; }
        }
 

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }          
        }
#endregion

#region Constructors
        /// <summary>
        /// Base Empty constructor. 
        /// </summary>
        public SampleLogic() : 
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<SampleLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with  Sample ID description parameters. 
        /// </summary>
        public SampleLogic(Int32 inSample_ID) :
            this()
        {
            this.ID = inSample_ID;
            LoadData();
        }
 
#endregion

#region Public overriden methods
        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Sample Details";
        }

        /// <summary>
        /// Return the selected Sample name
        /// </summary>
        /// <returns>Return the selected Sample name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0 )
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                
                NS_DAL.DAL.GetInstance().Update<SampleLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    {                        
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Sample ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            SampleLogic logic = new SampleLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<SampleLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                if ((this.ID == 1)) // The Unknown
                    throw Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception;

                NS_DAL.DAL.GetInstance().Delete<SampleLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw  Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<SampleLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex) 
            {
                switch (ex.Number) 
                {
                    case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                    {                        
                        throw Common.Error_Handling.ExceptionsList.MissingValueException;
                    }
                    case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    {                        
                        throw Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                    }
                    case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults(); 
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<SampleLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectAllToDataTable<SampleLogic>(this, out DT);
            return DT;
        }
#endregion
    }
}
