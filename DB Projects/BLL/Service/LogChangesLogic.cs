﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using NS_DAL.Service;

namespace NS_BLL.Base_Classes.Service
{
    public class LogChangesLogic : Base_Classes.BaseLogicClass
    {

        #region   Public Data Types  

            //Public Enum ActionTypes
            //    Insert = 10
            //    Update = 20
            //    Delete = 30
            //End Enum

        #endregion

        #region   Private Data Types  

            private struct Field
            {
                public string Name;
                public string Value;
            }

        #endregion

        #region   Private Data Members  
            private string _form_name;
            private int _action_type_id;
            //private int action_unique_id;
            //private string _field_name;
            //private string _field_value;
            // Private _change_date As Date
            //private int _user_id;
            //private string _user_name;
            private System.Collections.Queue _fields;
            private string _affected_record_description;
            private int _affected_record_id;
            //changed record id
        #endregion

        #region   Public Properties 

            public int Affected_Record_ID
            {
                get { return _affected_record_id; }
                set { _affected_record_id = value; }
            }

            public string Form_Name
            {
                get { return _form_name; }
                set { _form_name = value; }
            }

            public int Action_ID
            {
                get { return _action_type_id; }
                set { _action_type_id = value; }
            }

            public string Affected_Record_Description
            {
                get { return _affected_record_description; }
                set { _affected_record_description = value; }
            }

        #endregion

        #region  overridden Methods 

            public override string ClassDescription()
            {
                return "Log Changes List";
            }

            public override string DataDescription()
            {
                if (this.ID == 0) return "...";
                // Return Me.Form_Name & " :: " & Action_Description
                return "Should not see this!";
            }

            public override void LoadData()
            {
                //
            }

            public override void UpdateData()
            {
                
            }

            public override void DeleteData()
            {
                //
            }

            public override void InitValues()
            {
                this.ID = 0;
                this.Action_ID = 0;
                this.Affected_Record_Description = string.Empty;
                this.Affected_Record_ID = 0;
                this.Form_Name = string.Empty;
                this._fields = new Queue();
                //Me._change_date = Now
                try
                {
                    //this._user_id = ((object)My.User.CurrentPrincipal).id;
                    //this._user_name = My.User.Name;
                    //this.action_unique_id = Now.Ticks.GetHashCode;
                }
                catch //(Exception ex)
                {


                }
            }

            public override void InsertData()
            {
                //if (_fields.Count > 0)
                //{
                //    IEnumerator myIterator = _fields.GetEnumerator;
                //    while (myIterator.MoveNext)
                //    {
                //        try
                //        {
                //            //NS_DAL.LogChangesDataAccess.Insert(this.action_unique_id, this.Form_Name, ((Field)myIterator.Current).Name, this._action_type_id, this.Affected_Record_ID, this.Affected_Record_Description, ((Field)myIterator.Current).Value);
                //        }
                //        catch (Exception ex)
                //        {

                //        }
                //    }
                //}
                //Else
                //Try
                //    NS_DAL.LogChangesDataAccess.Insert(Me.action_unique_id, _
                //                                                 Me.Form_Name, _
                //                                                 String.Empty, _
                //                                                 Me._action_type_id, _
                //                                                 Me.Affected_Record_ID, _
                //                                                 Me.Affected_Record_Description, _
                //                                                 String.Empty)
                //Catch ex As Exception


                //End Try
            }

            public override DataView GetSearchResults()
            {
                return null;
            }

            public override DataTable GetComboSource()
            {
                return null;
            }

        #endregion

        #region  Constructor Methods  

            //Public Sub New()
            //    InitValues()
            //End Sub

            //Public Sub New(ByVal id As Integer)
            //    Me.ID = id
            //    LoadData()
            //End Sub

            public LogChangesLogic(string form_name, int action_id, int record_id, string record_description)
            {
                InitValues();
                this.Form_Name = form_name;
                this.Action_ID = action_id;
                this.Affected_Record_Description = record_description;
                this.Affected_Record_ID = record_id;
            }

        #endregion

        #region   Public Methods 

            public void AddAffectedField(string field_name, string field_value)
            {
                Field newCouple = new Field();
                if ((string.IsNullOrEmpty(field_name)))
                {
                    return;
                }
                newCouple.Name = field_name;
                newCouple.Value = field_value;
                this._fields.Enqueue(newCouple);
            }

            //public static DataView GetDeletedRecords(string form_name)
            //{
            //    int Action_Type_ID = GlobalService.SystemDictionariesSingletone.GetInstance().dicActionTypes(ActionTypesSingletone.DeleteName.ToLower());
            //    return GetFormattedLogChangesTable(NS_DAL.LogChangesDataAccess.GetChangesList(form_name, Action_Type_ID).Table).DefaultView;
            //}

            //public static DataView GetAddedRecords(string form_name)
            //{
            //    int Action_Type_ID = GlobalService.SystemDictionariesSingletone.GetInstance().dicActionTypes(ActionTypesSingletone.InsertName.ToLower());
            //    return GetFormattedLogChangesTable(NS_DAL.LogChangesDataAccess.GetChangesList(form_name, -1, Action_Type_ID).Table).DefaultView;
            //}

            //public static DataView GetUpdatedRecords(string form_name, int ID)
            //{
            //    int Action_Type_ID = GlobalService.SystemDictionariesSingletone.GetInstance().dicActionTypes(ActionTypesSingletone.UpdateName.ToLower());
            //    return GetFormattedLogChangesTable(NS_DAL.LogChangesDataAccess.GetChangesList(form_name, ID, Action_Type_ID).Table).DefaultView;
            //}

            //public static DataView GetFieldChanges(int recordID, string form_name)
            //{
            //    int Action_Type_ID = GlobalService.SystemDictionariesSingletone.GetInstance().dicActionTypes(ActionTypesSingletone.InsertName.ToLower());
            //    return GetFormattedLogChangesTable(NS_DAL.LogChangesDataAccess.GetChangesList(form_name, -1, Action_Type_ID, recordID).Table).DefaultView;
            //}

        #endregion

        #region   Private Methods  

            private static void FillRecordsLogTable(DataTable verticalTable, ref DataTable resTable)
            {
                //DataRow mySourceRow = default(DataRow);
                DataRow myDestRow = default(DataRow);
                string colName = null;
                foreach (DataRow mySourceRow in verticalTable.Rows)
                {
                    myDestRow = resTable.Rows.Find(mySourceRow["Action_Unique_ID"]);
                    if (myDestRow == null)
                    {
                        colName = isNull(mySourceRow["Field_Name"]).ToString() ;
                        if (!string.IsNullOrEmpty(colName))
                        {
                            DataColumn colFieldName = new DataColumn(colName, typeof(string));
                            if (resTable.Columns[mySourceRow["Field_Name"].ToString()] == null)
                            {
                                resTable.Columns.Add(colFieldName);
                            }
                            DataRow newRow = resTable.NewRow();
                            newRow["ID"] = mySourceRow["ID"];
                            newRow["Affected Record Description"] = mySourceRow["Affected_Record_Description"];
                            newRow["User"] = mySourceRow["Login"];
                            newRow["Date"] = mySourceRow["Date"];

                            //newRow.Item("Action_Unique_ID") = mySourceRow.Item("Action_Unique_ID")
                            //newRow.Item("Action_Type_ID") = mySourceRow.Item("Action_Type_ID")


                            //newRow.Item("Affected_Record_ID") = mySourceRow.Item("Affected_Record_ID")
                            newRow["Form Name"] = mySourceRow["Form_Name"];
                            newRow[mySourceRow["Field_Name"].ToString()] = mySourceRow["Field_Value"];
                            resTable.Rows.Add(newRow);

                        }
                    }
                    else
                    {
                        try
                        {
                            myDestRow[mySourceRow["Field_Name"].ToString()] = mySourceRow["Field_Value"];
                        }
                        catch //(Exception ex)
                        {
                            DataColumn colFieldName = new DataColumn(mySourceRow["Field_Name"].ToString(), typeof(string));
                            resTable.Columns.Add(colFieldName);
                            myDestRow[mySourceRow["Field_Name"].ToString()] = mySourceRow["Field_Value"];
                        }
                    }
                }
            }

            private static DataTable PrepareRecordsLogTable(DataTable verTable)
            {
                DataTable horTable = new DataTable();
                DataColumn colUnique = new DataColumn("ID", typeof(int));
                DataColumn recIDAction = new DataColumn("Action_Unique_ID", typeof(int));
                DataColumn recFormName = new DataColumn("Form Name", typeof(string));
                DataColumn recFieldName = new DataColumn("Field_Name", typeof(string));
                DataColumn recIDActionType = new DataColumn("Action_Type_ID", typeof(string));
                DataColumn recIDUser = new DataColumn("User", typeof(string));
                DataColumn recDateCol = new DataColumn("Date", typeof(DateTime));
                DataColumn recIDAffectedRecord = new DataColumn("Affected_Record_ID", typeof(int));
                DataColumn recAffectedRecord = new DataColumn("Affected Record Description", typeof(string));
                DataColumn recFieldValue = new DataColumn("Field_Value", typeof(string));






                horTable.Columns.Add(colUnique);
                horTable.Columns.Add(recAffectedRecord);
                horTable.Columns.Add(recIDUser);
                horTable.Columns.Add(recDateCol);
                //horTable.Columns.Add(recIDAction)
                horTable.Columns.Add(recFormName);
                //horTable.Columns.Add(recFieldName)
                //horTable.Columns.Add(recIDActionType)


                //horTable.Columns.Add(recIDAffectedRecord)

                //horTable.Columns.Add(recFieldValue)




                DataColumn[] keys = new DataColumn[2];
                keys[0] = colUnique;
                horTable.PrimaryKey = keys;
                return horTable;
            }

            public static DataTable GetFormattedLogChangesTable(DataTable verticalTable)
            {
                DataTable tbl = PrepareRecordsLogTable(verticalTable);
                //FillRecordsLogTable(verticalTable, tbl);
                return tbl;
            }

        #endregion

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return "LogChangesLogic";
            }           
        }

       
    }

}
