﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Common.Service;
using System.Data;
using NS_DAL.Service;

namespace NS_BLL.Motion_BLL
{
    public abstract class BaseRoutineClass : Base_Classes.BaseLogicClass
    {
        #region Public Properties
            [DBColumn(false)]
            protected string m_RoutineName = "";
            /// <summary> 
            /// Store selected routine name 
            /// </summary> 
            /// <value>The new value for routine name</value> 
            /// <returns>Routine name</returns> 
            /// <remarks></remarks> 
            [DBColumn("Routine Name", 255, false, true, false, false, 0, 0)]
            public string RoutineName
            {
                get { return m_RoutineName; }
                set { m_RoutineName = value; }
            }
          

            [DBColumn(false)]
            protected int m_ProductID = 0;
            /// <summary> 
            /// The uniq identifier for product 
            /// </summary> 
            /// <value>The new value for product ID</value> 
            /// <returns>Product ID</returns> 
            /// <remarks></remarks> 
            [DBColumn("Product ID", -1, false, false, false, false, 0, 0)]
            public int ProductID
            {
                get { return m_ProductID; }
                set { m_ProductID = value; }
            }

            [DBColumn(false)]
            protected bool m_isRoutineActive = false;
            /// <summary> 
            ///Indicate is current routine is selected to be active for it's product 
            /// </summary> 
            /// <value>The new value Active state</value> 
            /// <returns>Is active</returns> 
            /// <remarks></remarks> 
            [DBColumn("Is Routine Active", -1, false, false, false, false, 0, 0)]
            public bool isRoutineActive
            {
                get { return m_isRoutineActive; }
                set { m_isRoutineActive = value; }
            }


            [DBColumn(false)]
            protected string m_Description = "";
            /// <summary> 
            /// Any description text 
            /// </summary> 
            /// <value>New description</value> 
            /// <returns>Stored description text</returns> 
            /// <remarks></remarks> 
            [DBColumn("Description", 255, false, false, true, false, 0, 0)]
            public string Description
            {
                get { return m_Description; }
                set { m_Description = value; }
            }

            [DBColumn(false)]
            protected DataTable m_StepsList;
            /// <summary> 
            /// Get/Set the database table included all routine steps 
            /// </summary> 
            /// <value></value> 
            /// <returns></returns> 
            /// <remarks></remarks> 
            [DBColumn(false)]
            public DataTable StepsList
            {
                get { return m_StepsList; }
                set { m_StepsList = value; }
            }

            [DBColumn(false)]
            protected int m_FolderID;
            /// <summary> 
            /// Get/Set the database table included all routine steps 
            /// </summary> 
            /// <value></value> 
            /// <returns></returns> 
            /// <remarks></remarks> 
            [DBColumn("Folder ID", -1, false, false, false, false, 0, 0)]
            public int FolderID
            {
                get { return m_FolderID; }
                set { m_FolderID = value; }
            }

            [DBColumn(false)]
            public override string TableName
            {
                get
                {
                    return m_TableName;
                }
            }
        #endregion

        #region Constructors
            public BaseRoutineClass()
            {
               
            }

          
        #endregion
    }
} 

