﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NS_BLL.Motion_BLL
{
    public class RoutineLogic : Base_Classes.BaseLogicClass
    {

        //#region "Private Members"
        //#endregion

        //private static RoutineLogic _Instance;

        //#region " Public Properties "
        //private string m_RoutineName = "";
        ///// <summary>
        ///// Store selected routine name
        ///// </summary>
        ///// <value>The new value for routine name</value>
        ///// <returns>Routine name</returns>
        ///// <remarks></remarks>
        //public string RoutineName
        //{
        //    get { return m_RoutineName; }
        //    set { m_RoutineName = value; }
        //}

        //private int m_ProductID = 0;
        ///// <summary>
        ///// The uniq identifier for product
        ///// </summary>
        ///// <value>The new value for product ID</value>
        ///// <returns>Product ID</returns>
        ///// <remarks></remarks>
        //public int ProductID
        //{
        //    get { return m_ProductID; }
        //    set { m_ProductID = value; }
        //}

        //private int m_Type = 0;
        ///// <summary>
        ///// Some type variable
        ///// </summary>
        ///// <value>New type value</value>
        ///// <returns>Stored type value</returns>
        ///// <remarks></remarks>
        //public int Type
        //{
        //    get { return m_Type; }
        //    set { m_Type = value; }
        //}

        //private string m_Description = "";
        ///// <summary>
        ///// Any description text
        ///// </summary>
        ///// <value>New description</value>
        ///// <returns>Stored description text</returns>
        ///// <remarks></remarks>
        //public string Description
        //{
        //    get { return m_Description; }
        //    set { m_Description = value; }
        //}

        //private int m_FolderID = 0;
        ///// <summary>
        ///// Set/Get FolderID for which current routine was created
        ///// </summary>
        ///// <value>New Folder ID value</value>
        ///// <returns>Stored Folder ID value</returns>
        ///// <remarks></remarks>
        //public int FolderID
        //{
        //    get { return m_FolderID; }
        //    set { m_FolderID = value; }
        //}

        //private int m_Folder2BladeID = 0;
        ///// <summary>
        ///// Connection identifier from base folder table, to blade
        ///// </summary>
        ///// <value></value>
        ///// <returns></returns>
        ///// <remarks></remarks>
        //public int Folder2BladeID
        //{
        //    get { return m_Folder2BladeID; }
        //    set { m_Folder2BladeID = value; }
        //}

        //private DataTable m_StepsList;
        //public DataTable StepsList
        //{
        //    get { return m_StepsList; }
        //    set { m_StepsList = value; }
        //}

        //private Motion_BLL.Service.StepCollection m_Step_And_Actions_Data;
        ///// <summary>
        ///// Get/Set the routine steps with it's Actions arranged in [List of Steps[List Actions]]
        ///// </summary>
        ///// <value></value>
        ///// <returns></returns>
        ///// <remarks></remarks>
        //public Motion_BLL.Service.StepCollection Step_And_Actions_Data
        //{
        //    get { return m_Step_And_Actions_Data; }
        //    private set { m_Step_And_Actions_Data = value; }
        //}


        ///// <summary>
        ///// Returns the step item with the given record ID.
        ///// </summary>
        ///// <param name="recordID">The record i</param>
        ///// <returns></returns>
        ///// <remarks></remarks>
        //public Service.StepItem GetStepItem(int recordID)
        //{
        //    if ((Step_And_Actions_Data == null))
        //    {
        //        return null;
        //    }

        //    foreach (KeyValuePair<byte, Service.StepItem> pair in Step_And_Actions_Data)
        //    {
        //        if (pair.Value.StepRecordID == recordID)
        //        {
        //            return pair.Value;
        //        }
        //    }

        //    //For Each item As Service.StepItem In (Motion_BLL.Service.StepCollection)Step_And_Actions_Data
        //    //    If item.StepRecordID = recordID Then
        //    //        Return item
        //    //    End If
        //    //Next
        //    return null;
        //}
        //#endregion

        //#region " Constructor Methods "
        ///// <summary>
        ///// Base empty constructor. Init all values to the default
        ///// </summary>
        ///// <remarks></remarks>
        //public RoutineLogic()
        //{
        //    InitValues();
        //    _Instance = this;
        //}

        ///// <summary>
        ///// Selected Row constructor. Upload from database all data for recived ID
        ///// </summary>
        ///// <param name="inId">The row identifier</param>
        ///// <remarks></remarks>
        //public RoutineLogic(int inId)
        //    : this()
        //{
        //    this.ID = inId;
        //    LoadData();
        //}

        //#endregion

        //#region " Public & overridden Methods "
        ///// <summary>
        ///// Return the class description string
        ///// </summary>
        ///// <returns>Class description text</returns>
        ///// <remarks></remarks>
        //public override string ClassDescription()
        //{
        //    return "Folding Routine Details";
        //}

        ///// <summary>
        ///// Return the selected routine name
        ///// </summary>
        ///// <returns>Return the selected routine name, or "..." in case no item selected</returns>
        ///// <remarks></remarks>
        //public override string DataDescription()
        //{
        //    if (this.ID == null)
        //        return "...";
        //    return this.m_RoutineName;
        //    //Return Me.Name
        //}

        ///// <summary>
        ///// Load data for selected Routine ID
        ///// </summary>
        ///// <remarks></remarks>
        //public override void LoadData()
        //{
        //    DataRow myRow = NS_DAL.RoutineDataAccess.GetRow(ID);
        //    m_Type = Convert.ToInt32(isNull(myRow["iType"]));
        //    m_ProductID =Convert.ToInt32( isNull(myRow["ProductID"]));
        //    m_RoutineName = Convert.ToString (isNull(myRow["txtRoutineName"]));
        //    m_Folder2BladeID = Convert.ToInt32(isNull(myRow["iFolder2BladeID"]));
        //    m_Description = Convert.ToString (isNull(myRow["txtDescription"]));
        //    m_StepsList = NS_DAL.RoutineDataAccess.GetRoutineStepsList(ID);
        //    RefreshDirectList();
        //}


        ///// <summary>
        ///// Updates data in database
        ///// </summary>
        ///// <remarks></remarks>
        //public override void UpdateData()
        //{
        //    try
        //    {
        //        NS_DAL.RoutineDataAccess.UpdateRow(ID, m_Type, m_ProductID, m_RoutineName, m_FolderID, m_Folder2BladeID, m_Description);

        //        NS_DAL.RoutineDataAccess.UpdateRoutineStepsList(m_StepsList.GetChanges(), ID);
        //        m_Step_And_Actions_Data = new Motion_BLL.Service.StepCollection(NS_DAL.RoutineDataAccess.GetRoutineStepAndActionsDataList(ID));
        //        RefreshDirectList();
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        switch (ex.Number)
        //        {
        //            case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
        //                if (ex.Message.ToLower().Contains("productid")) // ERROR: Unknown binary operator Like
 
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.ProductIDMissingException;
        //                }
        //                if (ex.Message.ToLower().Contains("txtroutinename"))
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.RoutineNameMissingException;
        //                }
        //                break;
        //            case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
        //            case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
        //                throw Common.Error_Handling.ExceptionsList.RoutineNameOccupiedException;
        //             case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
        //                throw new Exception("Could not perform desired action due to network problems! Please try again");
        //            default:
        //                throw Common.Error_Handling.ExceptionsList.MissingValueException;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Delete row from database
        ///// </summary>
        ///// <remarks></remarks>
        //public override void DeleteData()
        //{
        //    try
        //    {
        //        NS_DAL.RoutineDataAccess.DeleteRow(ID);
        //        RefreshDirectList();
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        switch (ex.Number)
        //        {
        //            case Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
        //                throw Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
        //             case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
        //                throw new Exception("Could not perform desired action due to network problems! Please try again");
        //            default:
        //                throw Common.Error_Handling.ExceptionsList.DeleteFailedException;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Base initialization function. Set to 0/"" all values
        ///// </summary>
        ///// <remarks></remarks>
        //public override void InitValues()
        //{
        //    this.ID = -1;
        //    this.m_RoutineName = string.Empty;
        //    this.m_ProductID = 0;
        //    this.m_Type = 0;
        //    this.m_FolderID = 0;
        //    this.m_Description = string.Empty;
        //    this.m_Folder2BladeID = 0;
        //    this.m_StepsList = null;
        //    this.m_Step_And_Actions_Data = null;
        //}

        ///// <summary>
        ///// Insert new row with data, to database
        ///// </summary>
        ///// <remarks></remarks>
        //public override void InsertData()
        //{
        //    try
        //    {
        //        ID = NS_DAL.RoutineDataAccess.Insert(m_Type, m_ProductID, m_RoutineName, m_FolderID, m_Folder2BladeID, m_Description);
        //        RefreshDirectList();
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        switch (ex.Number)
        //        {
        //            case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
        //                if (ex.Message.ToLower().Contains("productid"))
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.ProductIDMissingException;
        //                }
        //                if (ex.Message.ToLower().Contains("txtroutinename"))
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.RoutineNameMissingException;
        //                }
        //                break;
        //            case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
        //            case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
        //                throw Common.Error_Handling.ExceptionsList.RoutineNameOccupiedException;
        //             case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
        //                throw new Exception("Could not perform desired action due to network problems! Please try again");                    
        //            //Throw DuplicateLoginNameException
        //            default:
        //                throw Common.Error_Handling.ExceptionsList.MissingValueException;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Get search result by one or more parameters
        ///// </summary>
        ///// <returns>All found rows corresponding to the search request</returns>
        ///// <remarks></remarks>
        //public override DataView GetSearchResults()
        //{
        //    return NS_DAL.RoutineDataAccess.GetSearchResults(m_RoutineName, m_ProductID, m_Type);
        //}

        ///// <summary>
        ///// Not in use
        ///// </summary>
        ///// <returns></returns>
        ///// <remarks></remarks>
        //public override DataTable GetComboSource()
        //{
        //    return null;
        //}

        //public void RefreshDirectList()
        //{
        //    Step_And_Actions_Data = new Motion_BLL.Service.StepCollection(NS_DAL.RoutineDataAccess.GetRoutineStepAndActionsDataList(_Instance.ID));
        //}

        ///// <summary>
        ///// Return the last step index. Used to select the new index number for new step
        ///// </summary>
        ///// <returns>Last step index</returns>
        ///// <remarks></remarks>
        //public byte GetLastStepIndex()
        //{
        //    if ((m_StepsList == null))
        //        return 0;
        //    DataRow myRow = m_StepsList.Rows[m_StepsList.Rows.Count - 1];
        //    return Convert.ToByte ( isNull(myRow["Index"]));
        //}

        //public float RecalculateStartPosition(Int32 inAxisID, Int32 inStepIndex)
        //{
        //    return Step_And_Actions_Data.GetLastAxisPosition4Step(inAxisID, inStepIndex);
        //}

        //public override void SaveAs(string name)
        //{
        //    try
        //    {
        //        //FolderID and Product ID have to be NULL to prevent problems with constraints
        //        //First add the routine
        //        dynamic routineID = NS_DAL.RoutineDataAccess.Insert(m_Type, 0, name, 0, m_Folder2BladeID, m_Description);
        //        //Second add the steps and for each step add its actions
        //        foreach (KeyValuePair<byte, Service.StepItem> pair in Step_And_Actions_Data)
        //        {
        //            // Add the step details 
        //            dynamic stepID = NS_DAL.RoutineStepsDataAccess.Insert(routineID, pair.Value.StepName, pair.Value.StepIndex, true);
        //            foreach (Service.Action actionValue in pair.Value.ListOfActions)
        //            {
        //                NS_DAL.StepActionsDataAccess.Insert(stepID, actionValue.ActionName, actionValue.ActionType, actionValue.AxisID, actionValue.DestinationPosition, actionValue.AccelerationRate, actionValue.AcclerationType, actionValue.DefaultAccelerationSelection, actionValue.SlewSpeed, actionValue.DestinationPosition,
        //                actionValue.DefaultSlewSelection, name, actionValue.WaitTimeout);
        //            }
        //        }
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        switch (ex.Number)
        //        {
        //            case Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
        //                if (ex.Message.ToLower().Contains("productid"))
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.ProductIDMissingException;
        //                }
        //                if (ex.Message.ToLower().Contains("txtroutinename"))
        //                {
        //                    throw Common.Error_Handling.ExceptionsList.RoutineNameMissingException;
        //                }
        //                break;
        //            case Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
        //            case Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
        //                throw Common.Error_Handling.ExceptionsList.RoutineNameOccupiedException;
        //             case Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
        //                throw new Exception("Could not perform desired action due to network problems! Please try again");
        //            default:
        //                throw Common.Error_Handling.ExceptionsList.MissingValueException;
        //        }
        //    }
        //}

        //#endregion

        public override string ClassDescription()
        {
            throw new NotImplementedException();
        }

        public override string DataDescription()
        {
            throw new NotImplementedException();
        }

        public override void UpdateData()
        {
            throw new NotImplementedException();
        }

        public override void LoadData()
        {
            throw new NotImplementedException();
        }

        public override void DeleteData()
        {
            throw new NotImplementedException();
        }

        public override void InsertData()
        {
            throw new NotImplementedException();
        }

        public override void InitValues()
        {
            throw new NotImplementedException();
        }

        public override DataView GetSearchResults()
        {
            throw new NotImplementedException();
        }

        public override DataTable GetComboSource()
        {
            throw new NotImplementedException();
        }

        public override string TableName
        {
            get { throw new NotImplementedException(); }
        }
    }
}
