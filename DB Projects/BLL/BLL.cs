﻿using NS_Common.Service;
using NS_BLL.Logics.Service;
using SFW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NS_BLL
{
    public class BLL : IIODevice, IBLL
    {
        #region Constructor
            public BLL()
            {
                ReloadMachineData();
            }
        #endregion

        #region Public Methods
            public Boolean CreateRule(Type FieldType)
            {
                return NS_DAL.DAL.GetInstance().CreateRule(FieldType);     
            }

            public void ReloadMachineData()
            {
                m_TestMachine = new MachineLogic(m_MachineID);
                m_MachineType2Brands = new MachineType2BrandsLogic();
                m_MachineType2Brands.Machine_Type = m_TestMachine.Machine_Type;
                DataTable tbl = m_MachineType2Brands.AvailibleBrandsList;
            }
        #endregion

        #region Public Properties

            private Int32 m_MachineID = -1;
            /// <summary>
            /// Get current machine DataBase Identifier
            /// </summary>
            public Int32 MachineID
            {
                get { return m_MachineID; }
            }

            private MachineLogic m_TestMachine;
            /// <summary>
            /// The current machine database Row
            /// </summary>
            public MachineLogic TestMachine
            {
                get { return m_TestMachine; }
                private set { m_TestMachine = value; }
            }

            private MachineType2BrandsLogic m_MachineType2Brands;
            /// <summary>
            /// The current MachineType2Brands database Row
            /// </summary>
            public MachineType2BrandsLogic MachineType2Brands
            {
                get { return m_MachineType2Brands; }
                private set { m_MachineType2Brands = value; }
            }


            private _TestGroupType m_SelectedTestGroupType;
            /// <summary>
            /// Get the selected test group type
            /// </summary>
            public _TestGroupType SelectedTestGroupType
            {
                get { return m_SelectedTestGroupType; }
                set { m_SelectedTestGroupType = value; }
            }

            private NS_BLL.Logics.Product.ProductLogic m_SelectedProduct = new NS_BLL.Logics.Product.ProductLogic(1/*Unknown*/);
            /// <summary>
            /// Get  the Product Logic item
            /// </summary>
            public NS_BLL.Logics.Product.ProductLogic SelectedProduct
            {
                get { return m_SelectedProduct; }
                set { m_SelectedProduct = value; }
            }

            private NS_BLL.Logics.Project.ProjectLogic m_SelectedProject = new NS_BLL.Logics.Project.ProjectLogic(1/*Unknown*/);
            /// <summary>
            /// Get  the Project Logic item
            /// </summary>
            public NS_BLL.Logics.Project.ProjectLogic SelectedProject
            {
                get { return m_SelectedProject; }
                set { m_SelectedProject = value; }
            }

            private NS_BLL.Logics.C_AND_F_Logic m_Selected_C_And_F = new NS_BLL.Logics.C_AND_F_Logic(1/*Unknown*/);
            /// <summary>
            /// Get  the C&F Logic item
            /// </summary>
            public NS_BLL.Logics.C_AND_F_Logic Selected_C_And_F
            {
                get { return m_Selected_C_And_F; }
                set { m_Selected_C_And_F = value; }
            }

            private NS_BLL.Logics.STP_Logic m_Selected_STP = new NS_BLL.Logics.STP_Logic(1/*Unknown*/);
            /// <summary>
            /// Get  the STP Logic item
            /// </summary>
            public NS_BLL.Logics.STP_Logic Selected_STP
            {
                get { return m_Selected_STP; }
                set { m_Selected_STP = value; }
            }

            private NS_BLL.Logics.ProtocolLogic m_Selected_Protocol = new NS_BLL.Logics.ProtocolLogic(1/*Unknown*/);
            /// <summary>
            /// Get  the Protocol Logic item
            /// </summary>
            public NS_BLL.Logics.ProtocolLogic Selected_Protocol
            {
                get { return m_Selected_Protocol; }
                set { m_Selected_Protocol = value; }
            }

        #endregion Public Properties

        #region IInput Device Members

            public event DataEventHandler DataReceived;

        #endregion

        #region IOutput Device Members

            public string Name
            {
                get { return "Business Layer Logic Object"; }
            }
         
            public void Send(string data)
            {
                throw new NotImplementedException();
            }

        #endregion

    }
}
