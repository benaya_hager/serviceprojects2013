﻿using System;
using NS_Common.Service;
using NS_DAL.Service;

namespace NS_BLL.Base_Classes
{
    /// <summary>
    /// Base Test Result
    /// </summary>
    public class BaseTestResultLogic : Base_Classes.BaseLogicClass
    {
        #region Protected Const

        private const String TABLE_NAME = "Test Results";

        #endregion Protected Const

        #region Public properties

        [DBColumn(false)]
        public override string TableName
        {
            get
            {
                return m_TableName;
            }
        }

        #region Machine INFO

        /// <summary>
        /// Local copy of Computer Network Name
        /// </summary>
        [DBColumn(false)]
        protected String m_ComputerNetworkName;
        /// <summary>
        /// Get/Set the machine name, that current test performed
        /// </summary>
        [DBColumn("Computer Network Name", 500, false, false, true, false, 0, 0)]
        public String ComputerNetworkName
        {
            get { return m_ComputerNetworkName; }
            set { m_ComputerNetworkName = value; }
        }

        /// <summary>
        /// Local copy of first of Processor ID, From machine that current test performed
        /// </summary>
        [DBColumn(false)]
        protected String m_FirstProcessID;
        /// <summary>
        /// Get\Set the first of Processor ID, From machine that current test performed
        /// </summary>
        [DBColumn("First Process ID", 255, false, false, true, false, 0, 0)]
        public String FirstProcessID
        {
            get { return m_FirstProcessID; }
            set { m_FirstProcessID = value; }
        }

        /// <summary>
        ///  Local copy of Active machine ID (BLL.MachineLogic.ID)
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_MachineID;
        /// <summary>
        ///  Get\Set the Active machine ID (BLL.MachineLogic.ID)
        /// </summary>
        [DBColumn("Machine ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "Machines", "Machine Name", "MachineName")]
        public Int32 MachineID
        {
            get { return m_MachineID; }
            set { m_MachineID = value; }
        }

        private String m_MachineName = "";
        /// <summary>
        /// Set/Get value Machine Name
        /// </summary>
        [DBColumn(false)]
        public String MachineName
        {
            get { return m_MachineName; }
            set { m_MachineName = value; }
        }

        #endregion Machine INFO

        #region User Info

        /// <summary>
        /// Local copy of Active windows login, that was logged in during the test performance
        /// </summary>
        [DBColumn(false)]
        protected String m_SystemUserLoginName;
        /// <summary>
        /// Get\Set the Active windows login, that was logged in during the test performance
        /// </summary>
        [DBColumn("System User Login Name", 255, false, false, true, false, 0, 0)]
        public String SystemUserLoginName
        {
            get { return m_SystemUserLoginName; }
            set { m_SystemUserLoginName = value; }
        }

        /// <summary>
        /// Local copy of Active User ID (BLL.UserLogic.ID)
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_UserID;
        /// <summary>
        ///  Get\Set the Active User ID (BLL.UserLogic.ID)
        /// </summary>
        [DBColumn("User ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "Users", "First Name", "User_First_Name")]
        public Int32 UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }

        private String m_User_First_Name = "";
        /// <summary>
        /// Set/Get value User First Name
        /// </summary>
        [DBColumn(false)]
        public String User_First_Name
        {
            get { return m_User_First_Name; }
            set { m_User_First_Name = value; }
        }

        #endregion User Info

        #region DB Data fields

        /// <summary>
        ///  Local copy of Active Project ID (BLL.ProjectLogic.ID)
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_ProjectID;
        /// <summary>
        ///  Get\Set the Active Project ID (BLL.ProjectLogic.ID)
        /// </summary>
        [DBColumn("Project ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "Projects", "Name/Title", "ProjectName")]
        public Int32 ProjectID
        {
            get { return m_ProjectID; }
            set { m_ProjectID = value; }
        }

        private String m_ProjectName = "";
        /// <summary>
        /// Set/Get value Project Name
        /// </summary>
        [DBColumn(false)]
        public String ProjectName
        {
            get { return m_ProjectName; }
            set { m_ProjectName = value; }
        }

        /// <summary>
        /// Local copy of Active Product ID (BLL.ProductLogic.ID)
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_ProductID;
        /// <summary>
        ///  Get\Set the Active Product ID (BLL.ProductLogic.ID)
        /// </summary>
        [DBColumn("Product ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "Products", "Catalog Number", "ProductName")]
        public Int32 ProductID
        {
            get { return m_ProductID; }
            set { m_ProductID = value; }
        }

        private String m_ProductName = "";
        /// <summary>
        /// Set/Get value Product Name
        /// </summary>
        [DBColumn(false)]
        public String ProductName
        {
            get { return m_ProductName; }
            set { m_ProductName = value; }
        }

        /// <summary>
        /// Local copy of Protocol ID
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_ProtocolID;
        /// <summary>
        /// Set/Get Protocol
        /// </summary>
        [DBColumn("Protocol ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "Protocols", "Protocol Name", "ProtocolName")]
        public Int32 ProtocolID
        {
            get { return m_ProtocolID; }
            set { m_ProtocolID = value; }
        }

        private String m_ProtocolName = "";
        /// <summary>
        /// Set/Get value Protocol Name
        /// </summary>
        [DBColumn(false)]
        public String ProtocolName
        {
            get { return m_ProtocolName; }
            set { m_ProtocolName = value; }
        }

        /// <summary>
        /// Local copy of C&F ID
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_C_AND_F_ID;
        /// <summary>
        /// Set/Get  C&F ID
        /// </summary>
        [DBColumn("C AND F ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "C And F", "Name/Title", "C_AND_F_Name")]
        public Int32 C_AND_F_ID
        {
            get { return m_C_AND_F_ID; }
            set { m_C_AND_F_ID = value; }
        }

        private String m_C_AND_F_Name = "";
        /// <summary>
        /// Set/Get value C&F Name
        /// </summary>
        [DBColumn(false)]
        public String C_AND_F_Name
        {
            get { return m_C_AND_F_Name; }
            set { m_C_AND_F_Name = value; }
        }

        /// <summary>
        /// Local copy of STP ID
        /// </summary>
        [DBColumn(false)]
        protected Int32 m_STP_ID;
        /// <summary>
        /// Set/Get Protocol
        /// </summary>
        [DBColumn("STP ID", NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID, "STPs", "Title", "STP_Title")]
        public Int32 STP_ID
        {
            get { return m_STP_ID; }
            set { m_STP_ID = value; }
        }

        private String m_STP_Title = "";
        /// <summary>
        /// Set/Get value STP Title
        /// </summary>
        [DBColumn(false)]
        public String STP_Title
        {
            get { return m_STP_Title; }
            set { m_STP_Title = value; }
        }

        #endregion DB Data fields

        #region Test Parameters

        /// <summary>
        /// Local copy of test name
        /// </summary>
        [DBColumn(false)]
        protected String m_TestName;
        /// <summary>
        /// Set/Get the test name
        /// </summary>
        [DBColumn("Test Name", 255, false, false, true, false, 0, 0)]
        public String TestName
        {
            get { return m_TestName; }
            set { m_TestName = value; }
        }

        /// <summary>
        /// Local copy of Lot number
        /// </summary>
        [DBColumn(false)]
        protected String m_LotNumber;
        /// <summary>
        /// Set/Get Lot number
        /// </summary>
        [DBColumn("Lot Number", 255, false, false, true, false, 0, 0)]
        public String LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        /// <summary>
        /// Local copy of Serial Number
        /// </summary>
        [DBColumn(false)]
        protected String m_SerialNumber;
        /// <summary>
        /// Set/Get Serial Number
        /// </summary>
        [DBColumn("Serial Number", 255, false, false, true, false, 0, 0)]
        public String SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        /// <summary>
        /// Local copy of Test date
        /// </summary>
        [DBColumn(false)]
        protected DateTime m_TestDate;
        /// <summary>
        /// Set/Get the date test was made
        /// </summary>
        [DBColumn("Test Date", -1, false, false, false, false, 0, 0)]
        public DateTime TestDate
        {
            get { return m_TestDate; }
            set { m_TestDate = value; }
        }

        /// <summary>
        /// Local copy of the full path and name of results row data file
        /// </summary>
        [DBColumn(false)]
        protected String m_RowDataResultFilePath;
        /// <summary>
        /// Set/Get the full path and name of results row data file
        /// </summary>
        [DBColumn("Row Data Result File Path", 1024, false, false, false, false, 0, 0)]
        public String RowDataResultFilePath
        {
            get { return m_RowDataResultFilePath; }
            set { m_RowDataResultFilePath = value; }
        }

        #endregion Test Parameters

        #region Test setup

        [DBColumn(false)]
        protected _TEST_RESULT_CALCULATION_BY m_TestResultsCalculateBy = _TEST_RESULT_CALCULATION_BY.RESULT_BY_PERCENST;
        [DBColumn("Test Results Calculate By", -1, false, false, true, false, 0, 0)]
        public _TEST_RESULT_CALCULATION_BY TestResultsCalculateBy
        {
            get { return m_TestResultsCalculateBy; }
            set
            {
                m_TestResultsCalculateBy = value;
            }
        }

        [DBColumn(false)]
        protected Double m_ResultByAbsolutePositionValue = -1;
        /// <summary>
        /// Get/Set value indicate the absolute position that measure
        /// data has to be taken there for result.
        /// </summary>
        /// <remarks>Used in case m_TestResultsCalculateBy = _TEST_RESULT_CALCULATION_BY.RESULT_BY_ABSOLUTE_POSITION</remarks>
        [DBColumn("Result By Absolute Position Value", -1, false, false, true, false, 0, 0)]
        public Double ResultByAbsolutePositionValue
        {
            get { return m_ResultByAbsolutePositionValue; }
            set { m_ResultByAbsolutePositionValue = value; }
        }

        [DBColumn(false)]
        protected Double m_ResultByReferencePositionValue = -1;
        /// <summary>
        /// Get/Set value indicate the reference position that measure
        /// data has to be taken there for result.
        /// </summary>
        /// <remarks>Used in case m_TestResultsCalculateBy = _TEST_RESULT_CALCULATION_BY.RESULT_BY_REFERENCE_POSITION</remarks>
        [DBColumn("Result By Reference Position Value", -1, false, false, true, false, 0, 0)]
        public Double ResultByReferencePositionValue
        {
            get { return m_ResultByReferencePositionValue; }
            set { m_ResultByReferencePositionValue = value; }
        }

        [DBColumn(false)]
        protected Double m_ResultByPercentValue = -1;
        /// <summary>
        /// Get/Set value indicate the absolute position calculated by (totatal motion * nudResultByPercent.udValue %) that measure
        /// data has to be taken there for result.
        /// </summary>
        /// <remarks>Used in case m_TestResultsCalculateBy = _TEST_RESULT_CALCULATION_BY.RESULT_BY_PERCENST</remarks>
        [DBColumn("Result By Percent Value", -1, false, false, true, false, 0, 0)]
        public Double ResultByPercentValue
        {
            get { return m_ResultByPercentValue; }
            set { m_ResultByPercentValue = value; }
        }

        [DBColumn(false)]
        protected Double m_TestDistanceMM = -1;
        /// <summary>
        /// Get/Set the the test length MM.
        /// </summary>
        [DBColumn("Test Distance (MM)", -1, false, false, true, false, 0, 0)]
        public Double TestDistanceMM
        {
            get { return m_TestDistanceMM; }
            set { m_TestDistanceMM = value; }
        }

        [DBColumn(false)]
        protected Double m_MotorSlewSpeed = -1;
        /// <summary>
        /// Get/Set the Motor test speed value
        /// </summary>
        [DBColumn("Motor Slew Speed", -1, false, false, true, false, 0, 0)]
        public Double MotorSlewSpeed
        {
            get { return m_MotorSlewSpeed; }
            set { m_MotorSlewSpeed = value; }
        }

        [DBColumn(false)]
        protected Double m_MotorAcceleration = -1;
        /// <summary>
        /// Get/Set the  Motor  test Acceleration value
        /// </summary>
        [DBColumn("Motor Acceleration", -1, false, false, true, false, 0, 0)]
        public Double MotorAcceleration
        {
            get { return m_MotorAcceleration; }
            set { m_MotorAcceleration = value; }
        }

        #endregion Test setup

        #region Additional Info Fields

        /// <summary>
        /// Local copy of Additional test information
        /// </summary>
        [DBColumn(false)]
        protected String m_Info;
        /// <summary>
        /// Set/Get Additional test information
        /// </summary>
        [DBColumn("Info", 1000, false, false, true, false, 0, 0)]
        public String Info
        {
            get { return m_Info; }
            set { m_Info = value; }
        }

        /// <summary>
        /// Local copy of user comments Before test was started
        /// </summary>
        [DBColumn(false)]
        protected String m_UserHeaderComments;
        /// <summary>
        /// Set/Get user comments Before test was started
        /// </summary>
        [DBColumn("User Pre-Test Comments", 1000, false, false, true, false, 0, 0)]
        public String UserHeaderComments
        {
            get { return m_UserHeaderComments; }
            set { m_UserHeaderComments = value; }
        }

        /// <summary>
        /// Local copy of user comments After test was completed
        /// </summary>
        [DBColumn(false)]
        protected String m_UserFooterComments;
        /// <summary>
        /// Set/Get user comments After test was completed
        /// </summary>
        [DBColumn("User Post-Test Comments", 1000, false, false, true, false, 0, 0)]
        public String UserFooterComments
        {
            get { return m_UserFooterComments; }
            set { m_UserFooterComments = value; }
        }

        #endregion Additional Info Fields

        #region Result values

        #region Maximum Weight

        /// <summary>
        /// Local copy of position where maximum result was Measured
        /// </summary>
        [DBColumn(false)]
        protected Double m_MaximumValueMotorPositionMM;
        /// <summary>
        ///  Get\Set the position where maximum result was Measured
        /// </summary>
        [DBColumn("Maximum Value Motor Position(MM)", -1, false, false, true, false, 0, 0)]
        public Double MaximumValueMotorPositionMM
        {
            get { return m_MaximumValueMotorPositionMM; }
            set { m_MaximumValueMotorPositionMM = value; }
        }

        /// <summary>
        /// Local copy of Maximum Measured Weight (Kg)
        /// </summary>
        [DBColumn(false)]
        protected Double m_MaximumMesuaredWeight;
        /// <summary>
        ///  Get\Set the Maximum Measured Weight (Kg)
        /// </summary>
        [DBColumn("Maximum Measured Weight (Kg)", -1, false, false, true, false, 0, 0)]
        public Double MaximumMeasuredWeight
        {
            get { return m_MaximumMesuaredWeight; }
            set { m_MaximumMesuaredWeight = value; }
        }

        #endregion Maximum Value

        #region Minimum Weight

        /// <summary>
        /// Local copy of position where Minimum result was Measured
        /// </summary>
        [DBColumn(false)]
        protected Double m_MinimumValueMotorPositionMM;
        /// <summary>
        ///  Get\Set the position where Minimum result was Measured
        /// </summary>
        [DBColumn("Minimum Value Motor Position(MM)", -1, false, false, true, false, 0, 0)]
        public Double MinimumValueMotorPositionMM
        {
            get { return m_MinimumValueMotorPositionMM; }
            set { m_MinimumValueMotorPositionMM = value; }
        }

        /// <summary>
        /// Local copy of Minimum Measured Value (Kg)
        /// </summary>
        [DBColumn(false)]
        protected Double m_MinimumMeasuredWeight;
        /// <summary>
        ///  Get\Set the Minimum Measured Weight (Kg)
        /// </summary>
        [DBColumn("Minimum Measured Weight (Kg)", -1, false, false, true, false, 0, 0)]
        public Double MinimumMeasuredWeight
        {
            get { return m_MinimumMeasuredWeight; }
            set { m_MinimumMeasuredWeight = value; }
        }

        #endregion Minimum Value

        #endregion Result values

        #endregion Public properties

        #region Constructors

        /// <summary>
        /// Base Empty constructor.
        /// </summary>
        public BaseTestResultLogic() :
            base()
        {
            m_TableName = TABLE_NAME;
            NS_DAL.DAL.GetInstance().CheckDataBaseColumns<BaseTestResultLogic>(this, false, true);
            InitValues();
        }

        /// <summary>
        /// Constructor with result to load ID.
        /// </summary>
        public BaseTestResultLogic(Int32 inResultID) :
            this()
        {
            this.ID = inResultID;
            LoadData();
        }

        #endregion Constructors

        #region Public overridden methods

        /// <summary>
        /// Return the class description string
        /// </summary>
        /// <returns>Class description text</returns>
        /// <remarks></remarks>
        public override string ClassDescription()
        {
            return "Test Results Details";
        }

        /// <summary>
        /// Return the selected Test name
        /// </summary>
        /// <returns>Return the selected Test name, or "..." in case no item selected</returns>
        /// <remarks></remarks>
        public override string DataDescription()
        {
            if (m_ID == 0)
                return "...";
            return m_Name;
        }

        /// <summary>
        /// Updates data in database
        /// </summary>
        /// <remarks></remarks>
        public override void UpdateData()
        {
            try
            {
                NS_DAL.DAL.GetInstance().Update<BaseTestResultLogic>(this, false);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                }
            }
        }

        /// <summary>
        /// Load data for selected Test ID
        /// </summary>
        /// <remarks></remarks>
        public override void LoadData()
        {
            BaseTestResultLogic logic = new BaseTestResultLogic();
            NS_DAL.DAL.GetInstance().LoadDataTable<BaseTestResultLogic>(ref logic, ID);
            Clone2This(logic);
        }

        /// <summary>
        /// Delete row from database
        /// </summary>
        /// <remarks></remarks>
        public override void DeleteData()
        {
            try
            {
                NS_DAL.DAL.GetInstance().Delete<BaseTestResultLogic>(this);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.FOREIGN_CONSTRAINT_ERROR:
                        throw NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException;
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw NS_Common.Error_Handling.ExceptionsList.DeleteFailedException;
                }
            }
        }

        /// <summary>
        /// Insert new row with data, to database
        /// </summary>
        /// <remarks></remarks>
        public override void InsertData()
        {
            try
            {
                Object RecordID;
                NS_DAL.DAL.GetInstance().Insert<BaseTestResultLogic>(this, true, out RecordID);
                this.ID = Convert.ToInt32(RecordID);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {
                    case NS_Common.ErrorCodesList.INSERT_NULL_VALUES_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.MissingValueException;
                        }
                    case NS_Common.ErrorCodesList.INSERT_DUPLICATE_VALUE_ERROR:
                    case NS_Common.ErrorCodesList.DUPLICATE_VALUE_ERROR:
                        {
                            throw NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException;
                        }
                    case NS_Common.ErrorCodesList.CONNECTION_RESET_BY_PEER_ERROR:
                        throw new Exception("Could not perform desired action due to network problems! Please try again");
                    default:
                        throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Base initialization function. Set to 0/"" all values
        /// </summary>
        /// <remarks></remarks>
        public override void InitValues()
        {
            InitValuesToDefaults();
        }

        /// <summary>
        /// Get search result by one or more parameters
        /// </summary>
        /// <returns>All found rows corresponding to the search request</returns>
        /// <remarks></remarks>
        public override System.Data.DataView GetSearchResults()
        {
            System.Data.DataTable DT;
            NS_DAL.DAL.GetInstance().SelectByParamsToDataTable<BaseTestResultLogic>(this, out DT);
            return DT.DefaultView;
        }

        /// <summary>
        /// Return null :)
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override System.Data.DataTable GetComboSource()
        {
            return null;
        }

        #endregion Public overridden methods
    }
}