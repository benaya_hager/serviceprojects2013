﻿using NS_DAL.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NS_BLL.Base
{
    public class BLL_ViewModelBase : INotifyPropertyChanged
    {
        [DBColumn(false)]
        public event PropertyChangedEventHandler PropertyChanged;
        [DBColumn(false)]
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
    
}
