﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NS_DAL.Service;
using NS_BLL.Base_Classes.Service;
using System.ComponentModel;
using NS_Common.ViewModels.Common;
using NS_BLL.Base;
using System.Windows.Media;
using NS_Common;
using NS_Common.Service;

namespace NS_BLL.Base_Classes
{
        

    public abstract class BaseLogicClass : BLL_ViewModelBase
    {
        public event Record_ID_Value_ChangedEventHandler Record_ID_Value_Changed;
        public delegate void Record_ID_Value_ChangedEventHandler(int newID);
        public event Data_UpdatedEventHandler Data_Updated;
        public delegate void Data_UpdatedEventHandler(int UpdatedRecordID);

        #region   Private Data Members
            [DBColumn(false)] 
            protected LogChangesLogic m_LogChangesClass;
            [DBColumn(false)]
            public LogChangesLogic LogChangesClass
            {
                get { return m_LogChangesClass; }
            }
        #endregion

        #region  Public Must Override Methods
            /// <summary>
            /// Return the class description string
            /// </summary>
            /// <returns>Class description text</returns>
            /// <remarks></remarks>
            public abstract string ClassDescription();

            /// <summary>
            /// Return the selected motion name
            /// </summary>
            /// <returns>Return the selected motion name, or "..." in case no item selected</returns>
            /// <remarks></remarks>
            public abstract string DataDescription();

            /// <summary>
            /// Updates data in database
            /// </summary>
            /// <remarks></remarks>
            public abstract void UpdateData();

            /// <summary>
            /// Load data for selected Product ID
            /// </summary>
            /// <remarks></remarks>
            public abstract void LoadData();



            /// <summary>
            /// Delete row from database
            /// </summary>
            /// <remarks></remarks>
            public abstract void DeleteData();

            /// <summary>
            /// Insert new row with data, to database
            /// </summary>
            /// <remarks></remarks>
            public abstract void InsertData();

            /// <summary>
            /// Base initialization function. Set to 0/"" all values
            /// </summary>
            /// <remarks></remarks>
            public abstract void InitValues();


            public abstract DataView GetSearchResults();

            /// <summary>
            /// Return nothing :)
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public abstract DataTable GetComboSource();

            /// <summary>
            /// Set default values to not set properties.
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public virtual void SetDefaultValuesToNotSetProperties()
            {}


            /// <summary>
            /// Load data for selected Product ID
            /// </summary>
            /// <remarks></remarks>
            public virtual void LoadData(Int32 id_toUse4load)
            {
                this.ID = id_toUse4load;
                this.LoadData(); 
            }
        #endregion

        #region  Protected Methods
            protected static object isNull(object value)
            {
                if (value == System.DBNull.Value) return null;
                return value;
            }

            protected static object isNull(object value, System.TypeCode RequiredType)
            {
                if (value == System.DBNull.Value)
                {
                    switch (RequiredType)
                    {
                        case TypeCode.String:
                            return "";
                        case TypeCode.Int16:
                            break;
                        case TypeCode.Int32:
                            break;
                        case TypeCode.Int64:
                            break;
                        case TypeCode.Single:
                            return 0;
                        default:
                            return (null);
                    }
                }
                return value;
            }

            /// <summary>
            /// Used to update the calling process that the data was updated
            /// </summary>
            /// <param name="UpdatedID"></param>
            /// <remarks></remarks>
            protected void DataUpdated(int UpdatedID)
            {
                if (Data_Updated != null)
                {
                    Data_Updated(UpdatedID);
                }
            }

            protected virtual void InitValuesToDefaults()
            {     
                
                System.Reflection.PropertyInfo[] pr = this.GetType().GetProperties();
                Object Value;
                foreach (System.Reflection.PropertyInfo pi in pr)
                {
                    System.ComponentModel.DefaultValueAttribute[] attrs =  pi.GetCustomAttributes(typeof(System.ComponentModel.DefaultValueAttribute), false)  as System.ComponentModel.DefaultValueAttribute[];
                    if (attrs.Length > 0)
                    {
                        Value = attrs[0].Value;
                    }
                    else
                    {
                        switch (pi.PropertyType.ToString())
                        {

                            case "System.String":
                            {
                                Value = "";
                                break;
                            }
                            //Localized settings
                            case "System.DateTime":
                            {
                                Value = DateTime.Now; // default(DateTime);
                                break;
                            }
                            case "System.Char":
                            {
                                Value = default(char);
                                break;
                            }
                            case "NS_BLL.Service.LogChangesLogic":
                            {
                                continue;
                                //Value = (Object)new LogChangesLogic("",0,0,"");
                                // break;
                            }

                            case "System.Byte": 
                            {
                                Value = (Byte)0;
                                break;
                            }
                            case "System.Int32":
                            {
                                Value = (System.Int32)0;
                                break;
                            }
                            case "System.Int64":
                            {
                                Value = (System.Int64)0;
                                break;
                            }
                            case "System.UInt16":
                            {
                                Value = (System.Int16)0;
                                break;
                            }
                            case "System.Decimal":
                            {
                                Value = (System.Decimal)0;
                                break;
                            }
                            case "System.Single":
                            {
                                Value = (System.Single)0;
                                break;
                            }
                            case "System.UInt32":
                            {
                                Value = (System.UInt32)0;
                                break;
                            }
                            case "System.Double":
                            {
                                Value = (System.Double)0;
                                break;
                            }
                            case "System.UInt64":
                            {
                                Value = (System.UInt64)0;
                                break;
                            }
                            case "System.Int16":
                            {
                                Value = (System.Int16)0;
                                break;
                            }
                            case "System.Boolean":
                            {
                                Value = false;
                                break;
                            }
                            default://Default to the Integer 0 value
                            {
                                //if (null == pi.PropertyType.BaseType) continue;

                                if (pi.PropertyType.IsEnum)
                                {
                                    if (Tools.String_Enum.StringEnum.IsStringDefined(pi.PropertyType, "Unknown"))
                                    {
                                        Value = 1; //Set to  Unknown state
                                        break;
                                    }
                                    continue;
                                }
                                continue;
                                //Value = 0;
                                //break;
                            }
                        }
                    }
                    if (pi.CanWrite) 
                        pi.SetValue(this, Value, null);  
                }                 
            }
        #endregion

        #region Public Properties
            [DBColumn(false)]
            protected String m_TableName = "Machines";
            
            [DBColumn(false)]
            public abstract String TableName
            {
                get;               
            }

            [DBColumn(false)]
            protected String m_Name = "";
            [DBColumn(false)]
            public virtual string Name
            {
                protected set { m_Name = value; }
                get
                {
                    if (m_ID == 0) return ClassDescription();
                    return ClassDescription() + " (" + DataDescription() + ")";
                }
            }

            [DBColumn(false)]
            private FormStateType m_StateType = FormStateType.Search;
            [DBColumn(false)]    
            public FormStateType StateType
            {
                get { return m_StateType; }
                set
                {
                    if (m_StateType != value)
                    {
                        m_StateType = value;
                        RaisePropertyChanged("ControlsBackColor");
                    }
                }
            }
                

            [DBColumn(false)]
            public virtual System.Drawing.Brush ControlsBackColor
            {
                get 
                {
                    switch (m_StateType)
                    {
                        case FormStateType.Search:
                            {
                                return SharedMembers.SEARCH_BACKGROUND_COLOR;
                            }
                        case FormStateType.New:
                        case FormStateType.Edit:
                            {
                                return SharedMembers.NORMAL_BACKGROUND_COLOR;
                            }
                        default:
                            return SharedMembers.DISABLED_BACK_COLOR;
                    }
                }
            }
            

            protected int m_ID;
            /// <summary>
            /// ///Get/Set the DB identifier of current Row 
            /// </summary>
            [DBColumn("ID", -1, true, true, false, true, 1, 1)]            
            public virtual int ID
            {
                get { return m_ID; }
                set
                {
                    m_ID = value;                     
                    if (Record_ID_Value_Changed != null)
                    {
                        Record_ID_Value_Changed(value);
                    }
                }
            }             


        #endregion

        #region   Public Methods
            public void Clone2This(BaseLogicClass logic)
            {
                System.Reflection.PropertyInfo[] pr = logic.GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo pi in pr)
                {
                    if (pi.CanWrite) 
                        pi.SetValue(this, pi.GetValue(logic, null) , null);
                }
            }

            public virtual void ClearLastSearchResults()
            {
                m_ID = 0;
            }

            public virtual DataTable GetComboSource(String DisplayMemberName, params String[] CollumnNames)
            {
                System.Data.DataTable DT = GetComboSource();
                if (null == DT) return DT;

                System.Type myDataType =
                       System.Type.GetType("System.String");

                // The expression multiplies the "Price" column value 
                // by the "Quantity" to create the "Total" column.
                string expression = "";
                foreach (String CollumnName in CollumnNames)
                {
                    if (expression != "") expression += " + '  ' + ";
                    expression += "[" + CollumnName + "]";
                }


                // Create the column, setting the type to Attribute.
                DataColumn column = new DataColumn(DisplayMemberName, myDataType,
                    expression, MappingType.Attribute);

                // Set various properties.
                column.AutoIncrement = false;
                column.ReadOnly = true;

                // Add the column to a DataTable object's to DataColumnCollection.
                DT.Columns.Add(column);
                return DT;
            }
        #endregion
    }
}

