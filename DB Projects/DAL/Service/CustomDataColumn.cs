﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NS_DAL
{
    /// <summary>
    ///     The CustomDataColumn class represents the definition of
    ///a data column. The dataCollector class needs a collection of this
    ///class. It describes them the columns of DataTable object, and the
    ///columns of the database table.
    ///  
    ///     There are many constructors of this class. These help you to
    ///define column easily.
    /// </summary>
    public class CustomDataColumn : DataColumn
    {          
        private Boolean m_IsPrimaryKey = false;
        /// <summary>
        ///     If this column is the primary key. Only one PK column
        ///will be added, the last one.
        /// </summary>
        public Boolean  IsPrimaryKey { get { return m_IsPrimaryKey; } }

        private Boolean m_IsAccessDatabase = false;
        public Boolean IsAccessDatabase
        {
            get { return m_IsAccessDatabase; }
            set { m_IsAccessDatabase = value; }
        }


        private SqlDbType m_SQLColumnType;
        /// <summary>
        /// Specifies SQL Server-specific data type of a field, property, for use in
        ///     a System.Data.SqlClient.SqlParameter.
        /// </summary>
        public SqlDbType SQLColumnType
        {
            get { return m_SQLColumnType; }
            set { m_SQLColumnType = value; }
        }

        public System.Type m_ColumnType;
        /// <summary>
        /// .NET Framework type - type in DataTable
        /// </summary>
        public System.Type ColumnType
        {
            get { return m_ColumnType; }
            set { m_ColumnType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inColumnName">Name of the column in the database, and in the temporary table (DataTable).</param>
        /// <param name="inSQLColumnType">Specifies SQL Server-specific data type of a field, property, for use in a System.Data.SqlClient.SqlParameter.</param>
        /// <param name="ColumnLength">If current type is VCHAR indicate the Maximum length value</param> 
        /// <param name="IsPrimaryKey">If this column is the primary key. Only one PK column will be added, the last one.</param>
        /// <param name="isUnique"></param>
        /// <param name="isAllowDBNull">If NULL value is accepted in SQL or not.</param>
        /// <param name="isAutoIncrement">If this column is an identity column in the SQL table.
        ///                               If it is, the SQL server gives an incremental ID when  the row inserted.</param>
        /// <param name="inAutoIncrementSeed">The seed of increment.</param>
        /// <param name="inAutoIncrementStep">The ammount of increment.</param>
        public CustomDataColumn(String inColumnName,                                                              
                                System.Type inColumnType,
                                Int32 ColumnLength, 
                                Boolean IsPrimaryKey,
                                Boolean isUnique,
                                Boolean isAllowDBNull,
                                Boolean isAutoIncrement,
                                long inAutoIncrementSeed,
                                long inAutoIncrementStep)
        {

            Initialize(inColumnName,  inColumnType, ColumnLength, IsPrimaryKey,
                        isUnique, isAllowDBNull, isAutoIncrement, inAutoIncrementSeed, inAutoIncrementStep);
        }



        public CustomDataColumn(String inColumnName,
                               System.Type inColumnType,
                                Int32 ColumnLength) :
            this(inColumnName,  inColumnType,ColumnLength, false, false, true, false, 0, 1)

        { }

        

        public CustomDataColumn(String inColumnName
                                ,System.Type inColumnType) :
            this(inColumnName,  inColumnType, -1,  false, false, true, false, 0, 1)

        { }


        //public CustomDataColumn(String inColumnName, System.Type inColumnType)
        //{
        //    SqlDbType tmpType = GetSqlType(inColumnType);
        //    Int32 ColumnLength = GetDefaultSize(inColumnType);
        //    Initialize(inColumnName, inColumnType , ColumnLength,  false, false, true, false, 0, 1);
        //}

        private int GetDefaultSize(Type inColumnType)
        {
            switch (inColumnType.ToString())
            {
                case "System.String":
                    return 255;

                case "System.Decimal":
                case "System.Double":
                case "System.Single":
                case "System.Int64":
                case "System.Int16":
                case "System.Int32":
                case "System.DateTime":
                    return -1;

                default:
                    throw new Exception(inColumnType.ToString() + " not implemented.");
            }    
        }

        private SqlDbType GetSqlType(Type inColumnType)
        {
            switch (inColumnType.ToString())
            {
                case "System.String":
                    return SqlDbType.VarChar;

                case "System.Decimal":
                    return SqlDbType.Decimal;

                case "System.Double":
                case "System.Single":
                    return SqlDbType.Real;

                case "System.Int64":
                    return SqlDbType.BigInt;

                case "System.Int16":
                case "System.Int32":
                    return SqlDbType.Int;

                case "System.DateTime":
                    return SqlDbType.DateTime;

                default:
                    throw new Exception(inColumnType.ToString() + " not implemented.");
            }
        }

        public override String ToString()
        {
            String sResult = "";

            // adds the column definition to the Create Table command

            if (m_IsPrimaryKey)
            {
                if (m_IsAccessDatabase)
                {
                    sResult += "[" + this.ColumnName + "]";
                    //COUNTER(1,1) 
                    sResult += " COUNTER(" + this.AutoIncrementSeed + "," +
                        this.AutoIncrementStep + ")";
                }
                else
                {
                    sResult += "[" + this.ColumnName + "] " + SQLGetType(this);
                   
                    //IDENTITY(1,1) NOT NULL
                    sResult += " IDENTITY(" + this.AutoIncrementSeed + "," +
                        this.AutoIncrementStep + ") NOT NULL";
                }
            }
            else
            {
                sResult += "[" + this.ColumnName + "] " + SQLGetType(this);
                if (this.AllowDBNull)
                {
                    sResult += " NULL";
                }
                else
                {
                    sResult += " NOT NULL";
                }
            }
            sResult +=  Environment.NewLine;

            return sResult;
        }

        public String CreatePrimaryKey(String fullTableName, String tableName)
        {
            String sResult = "";
            // primary key producing command
            if (this.m_IsPrimaryKey)
            {
                sResult = "ALTER TABLE " + fullTableName + Environment.NewLine +
                    " ADD CONSTRAINT [PK_" + tableName + "] PRIMARY KEY CLUSTERED" + Environment.NewLine +
                    "([" + this.ColumnName + "])";
            }
            return sResult; 
        }


        // Return T-SQL data type definition, based on schema definition for a column
        private   string SQLGetType(object type, int columnSize, int numericPrecision, int numericScale)
        {
            if (((Type)type).BaseType.ToString() == "System.Enum")
            {
                return ((Type)type).Name;  
            }
            else
            {
                switch (type.ToString())
                {
                    case "System.String":
                        return "VARCHAR(" + ((columnSize == -1) ? 255 : columnSize) + ")";

                    case "System.Decimal":
                        if (numericScale > 0)
                            return "REAL";
                        else if (numericPrecision > 10)
                            return "BIGINT";
                        else
                            return "INT";

                    case "System.Double":
                    case "System.Single":
                        return "REAL";

                    case "System.Int64":
                        return "BIGINT";

                    case "System.Int16":
                    case "System.Int32":
                        return "INT";

                    case "System.DateTime":
                        return "DATETIME";

                    case "System.Boolean":
                        return "BIT";
                    case "System.Windows.DependencyObjectType":
                        {
                            return null;
                        }
                    default:
                        throw new Exception(type.ToString() + " not implemented.");
                }
            }
        }

        // Overload based on row from schema table
        private   string SQLGetType(DataRow schemaRow)
        {
            return SQLGetType(schemaRow["DataType"],
                                int.Parse(schemaRow["ColumnSize"].ToString()),
                                int.Parse(schemaRow["NumericPrecision"].ToString()),
                                int.Parse(schemaRow["NumericScale"].ToString()));
        }

        // Overload based on DataColumn from DataTable type
        private   string SQLGetType(DataColumn column)
        {
            return SQLGetType(column.DataType, column.MaxLength, 10, 2);
        }

        private void Initialize(String inColumnName,                                
                                System.Type inColumnType,
                                Int32 ColumnLength,
                                Boolean IsPrimaryKey,
                                Boolean isUnique,
                                Boolean isAllowDBNull,
                                Boolean isAutoIncrement,
                                long inAutoIncrementSeed,
                                long inAutoIncrementStep)
        {
            if (inColumnType.BaseType.ToString()  ==  "System.Enum" )
                m_SQLColumnType = SqlDbType.Udt;
            else
                m_SQLColumnType = GetDBType( inColumnType);
            m_ColumnType = inColumnType;
            base.DataType = inColumnType;
            base.ColumnName = inColumnName;
            base.AllowDBNull = isAllowDBNull;
            base.Unique = isUnique;
            if (inAutoIncrementSeed != 0)
                base.AutoIncrementSeed = inAutoIncrementSeed;
            if (inAutoIncrementStep != 0)
                base.AutoIncrementStep = inAutoIncrementStep;
            base.AutoIncrement = isAutoIncrement;
            if (ColumnLength>=0)
                base.MaxLength = ColumnLength;
            m_IsPrimaryKey = IsPrimaryKey; 
            
        }


        private SqlDbType GetDBType(System.Type theType)
        {
            System.Data.SqlClient.SqlParameter p1 = default(System.Data.SqlClient.SqlParameter);
            System.ComponentModel.TypeConverter tc = default(System.ComponentModel.TypeConverter);
            p1 = new System.Data.SqlClient.SqlParameter();
            tc = System.ComponentModel.TypeDescriptor.GetConverter(p1.DbType);
            if (tc.CanConvertFrom(theType))
            {
                p1.DbType =(DbType) tc.ConvertFrom((Object)theType.Name);
            }
            else
            {
                //Try brute force
                try
                {
                    p1.DbType = (DbType)tc.ConvertFrom((Object)theType.Name);
                }
                catch  
                {
                }
                //Do Nothing
            }
            return p1.SqlDbType;
        }
    }
}
