﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NS_DAL.Service;

namespace NS_DAL
{
    /// <summary>
    /// Convert all property fields of class to CustomDataColumns
    /// </summary>
    public class Class2CustomDataColumns
    {
        private List<CustomDataColumn> m_dataColumns;
        public List<CustomDataColumn>  dataColumns
        { get { return m_dataColumns; } }


        public Class2CustomDataColumns(Object SourceClass, Boolean AddAutoNumberIdField) 
        {
            m_dataColumns  = new List<CustomDataColumn>();
            if (AddAutoNumberIdField)
                m_dataColumns.Add(new CustomDataColumn("ID",  System.Type.GetType("System.Int32"),  -1, true, true, false, true, 1, 1));
            
            System.Reflection.PropertyInfo[] pr = SourceClass.GetType().GetProperties(); 
            
            foreach (System.Reflection.PropertyInfo pi in pr)
            {                 
                DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                if (attrs.Length > 0)
                {
                    if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                    CustomDataColumn tmp  = new CustomDataColumn(attrs[0].ColumnName,
                                                                pi.PropertyType,
                                                                attrs[0].ColumnLength,
                                                                attrs[0].IsPrimaryKey,
                                                                attrs[0].Unique,
                                                                attrs[0].AllowDBNull,
                                                                attrs[0].AutoIncrement,
                                                                attrs[0].AutoIncrementSeed,
                                                                attrs[0].AutoIncrementStep);
                    m_dataColumns.Add(tmp);
                }
                else
                    m_dataColumns.Add(new CustomDataColumn(pi.Name, pi.PropertyType));

                    
            }
        }
        public Class2CustomDataColumns(Object SourceClass)
            : this(SourceClass, true)
        { }
    }
}
