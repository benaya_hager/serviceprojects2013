﻿using System;
using NS_Common.Service;

namespace NS_DAL.Service
{
    #region Class DBColumnAttribute

    /// <summary>
    /// Simple attribute class for storing the Boolean value indicate if field will be visible for
    /// DBmanager database creator.
    /// In complex version store all the information about current column.
    /// </summary>
    public class DBColumnAttribute : Attribute
    {
        #region Public Properties

        private Boolean m_IsDBVisibleAttribute = true;
        /// <summary>
        /// Gets the indicate if field needs to be created in new database.
        /// </summary>
        /// <value></value>
        public Boolean IsDBVisibleAttribute
        {
            get { return m_IsDBVisibleAttribute; }
        }

        private _DB_FIELD_TYPE m_FieldType = _DB_FIELD_TYPE.TABLE_FIELD;
        /// <summary>
        /// Gets the value indicates if current field is local db field or an ID of other table entrance.
        /// </summary>
        /// <value></value>
        public _DB_FIELD_TYPE FieldType
        {
            get { return m_FieldType; }
        }

        #region System Fields

        private String m_ColumnName = "";
        /// <summary>
        /// Gets the database name of current field.
        /// </summary>
        /// <value></value>
        public String ColumnName
        {
            get { return m_ColumnName; }
        }

        private Boolean m_IsPrimaryKey = false;
        /// <summary>
        /// Gets value indicate if this column is the primary key.
        /// </summary>
        /// <remarks>Only one PK column will be added, the last one.</remarks>
        public Boolean IsPrimaryKey
        {
            get { return m_IsPrimaryKey; }
        }

        private Boolean m_Unique = false;
        /// <summary>
        ///
        /// </summary>
        public Boolean Unique
        {
            get { return m_Unique; }
        }

        private Boolean m_AllowDBNull = true;
        /// <summary>
        /// Gets value indicate If NULL value is accepted in SQL or not..
        /// </summary>
        public Boolean AllowDBNull
        {
            get { return m_AllowDBNull; }
        }

        #endregion System Fields

        #region String Fields

        private Int32 m_ColumnLength = -1;
        /// <summary>
        /// If current type is VCHAR(String) indicate the Maximum length value
        /// </summary>
        /// <remarks>if this value is -1, the default size will be set</remarks>
        public Int32 ColumnLength
        {
            get { return m_ColumnLength; }
        }

        #endregion String Fields

        #region Numbers Fields

        private Boolean m_AutoIncrement = false;
        /// <summary>
        /// If this column is an identity column in the SQL table,
        /// if it is, the SQL server gives an incremental ID when the row inserted.
        /// </summary>
        public Boolean AutoIncrement
        {
            get { return m_AutoIncrement; }
        }

        private long m_AutoIncrementSeed = 0;
        /// <summary>
        ///The seed of increment.
        /// </summary></remarks>
        public long AutoIncrementSeed
        {
            get { return m_AutoIncrementSeed; }
        }

        private long m_AutoIncrementStep = 0;
        /// <summary>
        ///The ammount of increment.
        /// </summary></remarks>
        public long AutoIncrementStep
        {
            get { return m_AutoIncrementStep; }
        }

        #endregion Numbers Fields

        #region Additional Table Fields

        private String m_AditionalTableName = "";
        /// <summary>
        /// Gets the data table name. Used only in case FieldType == OTHER_TABLE_ID.
        /// </summary>
        /// <value></value>
        public String AditionalTableName
        {
            get { return m_AditionalTableName; }
        }

        private String m_AditionalFieldDisplayTitleName = "";
        /// <summary>
        /// Gets the data field name in the other table, that will be displayed in datagrid viewer .
        /// Used only in case FieldType == OTHER_TABLE_ID.
        /// </summary>
        /// <value></value>
        public String AditionalFieldDisplayTitleName
        {
            get { return m_AditionalFieldDisplayTitleName; }
        }

        private String m_LocalPropertyName = "";
        /// <summary>
        /// Gets the local class property name, where to set the data from additional table.
        /// Used only in case FieldType == OTHER_TABLE_ID.
        /// </summary>
        /// <value></value>
        public String LocalPropertyName
        {
            get { return m_LocalPropertyName; }
        }

        #endregion Additional Table Fields

        #region (Store Precedures & Single To Many) Fields

        private String m_SelectStoreProcName = "";
        /// <summary>
        /// Gets the select data store procedure name. Used only in case FieldType == SINGLE_TO_MANY.
        /// </summary>
        /// <value></value>
        public String SelectStoreProcName
        {
            get { return m_SelectStoreProcName; }
        }

        private String[] m_SelectFieldNames = null;
        /// <summary>
        /// Gets the list of fields used as parameters in select store procedure . Used only in case FieldType == SINGLE_TO_MANY.
        /// </summary>
        /// <value></value>
        public String[] SelectFieldNames
        {
            get { return m_SelectFieldNames; }
        }


        private String m_DeleteStoreProcName = "";
        /// <summary>
        /// Gets the Delete data store procedure name. Used only in case FieldType == SINGLE_TO_MANY.
        /// </summary>
        /// <value></value>
        public String DeleteStoreProcName
        {
            get { return m_DeleteStoreProcName; }
        }

        private String m_InsertStoreProcName = "";
        /// <summary>
        /// Gets the Insert data store procedure name. Used only in case FieldType == SINGLE_TO_MANY.
        /// </summary>
        /// <value></value>
        public String InsertStoreProcName
        {
            get { return m_InsertStoreProcName; }
        }

        private String m_UpdateStoreProcName = "";
        /// <summary>
        /// Gets the Update data store procedure name. Used only in case FieldType == SINGLE_TO_MANY.
        /// </summary>
        /// <value></value>
        public String UpdateStoreProcName
        {
            get { return m_UpdateStoreProcName; }
        }

        #endregion (Store Precedures & Single To Many) Fields

        #endregion Public Properties

        #region Constructors

        /// <summary>
        /// Creates a new <see cref="DBColumnAttribute"/> instance.
        /// </summary>
        /// <param name="inIsDBVisibleAttribute">Indicate if current field will be created in data base.</param>
        public DBColumnAttribute(Boolean inIsDBVisibleAttribute)
        {
            m_IsDBVisibleAttribute = inIsDBVisibleAttribute;
        }

        public DBColumnAttribute(String inColumnName,
                                 Int32 inColumnLength,
                                 Boolean inIsPrimaryKey,
                                 Boolean inUnique,
                                 Boolean inAllowDBNull,
                                 Boolean inAutoIncrement,
                                 long inAutoIncrementSeed,
                                 long inAutoIncrementStep) :
            this(true)
        {
            m_ColumnName = inColumnName;
            m_IsPrimaryKey = inIsPrimaryKey;
            m_Unique = inUnique;
            m_ColumnLength = inColumnLength;
            m_AllowDBNull = inAllowDBNull;
            m_AutoIncrement = inAutoIncrement;
            m_AutoIncrementSeed = inAutoIncrementSeed;
            m_AutoIncrementStep = inAutoIncrementStep;
        }

        public DBColumnAttribute(String inColumnName,
                                 _DB_FIELD_TYPE inFieldType,
                                 String inAditionalTableName,
                                 String inAditionalFieldDisplayTitleName,
                                 String inLocalPorpertyName)
            : this(inColumnName, -1, false, false, false, false, 0, 0)
        {
            m_FieldType = inFieldType;
            m_AditionalTableName = inAditionalTableName;
            m_AditionalFieldDisplayTitleName = inAditionalFieldDisplayTitleName;
            m_LocalPropertyName = inLocalPorpertyName;
        }

        /// <summary>
        /// Used only with _DB_FIELD_TYPE.SINGLE_TO_MANY type
        /// </summary>
        /// <param name="inFieldType">Have to be _DB_FIELD_TYPE.SINGLE_TO_MANY</param>
        ///<param name="inSelectStoreProcName">Select store procedure name</param>
        ///<param name="inDeleteStoreProcName">Delete store procedure name</param>
        ///<param name="inInsertStoreProcName">Insert store procedure name</param>
        ///<param name="inUpdateStoreProcName">Update store procedure name</param>
        ///<param name="inSelectFieldNames">The list of fields used as parameters in select store procedure</param>
        public DBColumnAttribute(_DB_FIELD_TYPE inFieldType,
                                String inSelectStoreProcName,
                                String inDeleteStoreProcName,
                                String inInsertStoreProcName,
                                String inUpdateStoreProcName,
                                params String[] inSelectFieldNames)
            : this(false)
        {
            if (inFieldType != _DB_FIELD_TYPE.SINGLE_TO_MANY)
                throw new Exception("This constructor has to receive only  _DB_FIELD_TYPE.SINGLE_TO_MANY value");
            m_FieldType = inFieldType;
            m_SelectStoreProcName = inSelectStoreProcName;
            m_DeleteStoreProcName = inDeleteStoreProcName;
            m_InsertStoreProcName = inInsertStoreProcName;
            m_UpdateStoreProcName = inUpdateStoreProcName;
            m_SelectFieldNames = inSelectFieldNames;
        }

        #endregion Constructors
    }

    #endregion Class DBColumnAttribute
}