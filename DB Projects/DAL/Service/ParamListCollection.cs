﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Data;

namespace NS_DAL.Service
{
    public class ParamListCollection 
    { 
        #region Private data members 
            //protected int size; 
            //protected Collection List = new Collection(); 
            protected List<System.Data.SqlClient.SqlParameter> sqlParamList; 
        #endregion 
        
        #region Constructors 
            public ParamListCollection() 
            {
                sqlParamList = new List<System.Data.SqlClient.SqlParameter>(); 
            } 

            //public ParamListCollection(int Psize) 
            //{ 
            //    //this.size = Psize; 
            //    //sqlParamList.Initialize(); 
            //} 
        #endregion 
        
        #region Public functions 
            /// <summary> 
            /// Add object to local list. 
            /// </summary> 
            /// <param name="paramValue">Object to add</param> 
            /// <remarks></remarks> 
            public void Add(System.Data.SqlClient.SqlParameter paramValue) 
            {
                sqlParamList.Add(paramValue); 
            } 

            /// <summary> 
            /// Add object to local list with specific name to map the parametr in the list. 
            /// </summary> 
            /// <param name="paramName">The name of parametr to map</param> 
            /// <param name="paramValue">An System.Object that is the value of the System.Data.SqlClient.SqlParameter.</param> 
            /// <remarks></remarks> 
            public void Add(string paramName, object paramValue) 
            {
                System.Data.SqlClient.SqlParameter tmpParam = null;

                foreach (System.Data.SqlClient.SqlParameter param in sqlParamList)
                {
                    if (param.ParameterName == paramName)
                    {
                        tmpParam = param;
                        break;
                    }
                }
                if (null != tmpParam)
                {
                    sqlParamList.Remove(tmpParam);  
                }

                sqlParamList.Add(new System.Data.SqlClient.SqlParameter(paramName, paramValue)); 
            } 

            /// <summary> 
            /// Add object to local list with specific name to map the parametr in the list. 
            /// </summary> 
            /// <param name="paramName">The name of the System.Data.SqlClient.SqlParameter. The default is an empty string.</param> 
            /// <param name="type">One of the System.Data.SqlDbType values. The default is NVarChar.</param> 
            /// <param name="direction">One of the System.Data.ParameterDirection values. The default is Input.</param> 
            /// <param name="paramValue">An System.Object that is the value of the parameter. The default value is null.</param> 
            /// <remarks></remarks> 
            public void Add(string paramName, SqlDbType type, ParameterDirection direction,Object paramValue ) 
            {
                System.Data.SqlClient.SqlParameter tmpParam = null;

                foreach (System.Data.SqlClient.SqlParameter param in sqlParamList)
                {
                    if (param.ParameterName == paramName)
                    {
                        tmpParam = param;
                        break;
                    }
                }
                if (null != tmpParam)
                {
                    sqlParamList.Remove(tmpParam);
                }

                System.Data.SqlClient.SqlParameter VAL = new System.Data.SqlClient.SqlParameter(paramName,paramValue);

                VAL.Direction  = direction;
                VAL.SqlDbType =  type;
                sqlParamList.Add(VAL); 
            } 
        
            /// <summary> 
            /// Get parametr object by String name identifier 
            /// </summary> 
            /// <param name="paramName">The name of the System.Data.SqlClient.SqlParameter.</param> 
            /// <returns>Return the local copy of parametr</returns> 
            /// <remarks></remarks> 
            public object GetParamValue(string paramName) 
            {
                foreach (System.Data.SqlClient.SqlParameter param in sqlParamList)
                {
                    if (param.ParameterName == paramName)
                    {
                        return param;
                    }
                }
                return null;
            } 

            /// <summary> 
            /// Get parametr object by position index 
            /// </summary> 
            /// <param name="index">List position index</param> 
            /// <returns>Return the local copy of parameter</returns> 
            /// <remarks></remarks> 
            public object GetParamValue(int index) 
            {
                if (sqlParamList.Count <= index)
                {
                    return sqlParamList[index];
                }
                return null; 
            } 
            
            /// <summary> 
            /// Removes the first occurrence of a specific object from the System.Collections.Generic.List 
            /// </summary> 
            /// <param name="paramName">The object to remove from the System.Collections.Generic.List. The value can be null for reference types.</param> 
            /// <remarks></remarks> 
            public void Remove(string paramName) 
            {
                System.Data.SqlClient.SqlParameter tmpParam = null;

                foreach (System.Data.SqlClient.SqlParameter param in sqlParamList)
                {
                    if (param.ParameterName == paramName)
                    {
                        tmpParam = param;
                        break;
                    }
                }
                if (null != tmpParam)
                {
                    sqlParamList.Remove(tmpParam);
                }
            } 
            
            /// <summary> 
            /// Removes all elements from the array 
            /// </summary> 
            /// <remarks></remarks> 
            public void clear() 
            {
                sqlParamList.Clear(); 
            } 
            
            /// <summary> 
            /// Return the copy of list with parrametrs array 
            /// </summary> 
            /// <returns>Copy of list with parrametrs array</returns> 
            /// <remarks></remarks> 
            public object[] GetParamArray() 
            {
                object[] retParamArray = new object[sqlParamList.Count]; 
                int x = 0;
                for (x = 0; x <= sqlParamList.Count - 1; x++)
                {
                    retParamArray[x] = sqlParamList[x+1]; 
                } 
                return retParamArray; 
            } 
            
            /// <summary> 
            /// Return the local copy of stored sqlParamList 
            /// </summary> 
            /// <returns>Copy of stored sqlParamList</returns> 
            /// <remarks></remarks> 
            public System.Data.SqlClient.SqlParameter[] GetSQLParamArray() 
            {
                System.Data.SqlClient.SqlParameter[] retParamArray = new System.Data.SqlClient.SqlParameter[sqlParamList.Count];
                for ( int x = 0; x <= sqlParamList.Count-1; x++)
                {
                    retParamArray[x] = sqlParamList[x];
                }
                return retParamArray; 
            } 
        #endregion 
    } 
}
