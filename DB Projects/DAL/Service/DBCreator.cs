﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Tools.String_Enum;

namespace NS_DAL.Service
{
    internal class DBCreator
    {
        #region Data Managment Functions

        /// <summary>
        /// Check if table exists
        /// </summary>
        /// <param name="table_name">Table name to look for</param>
        /// <param name="m_Conn">Connection to server database</param>
        /// <param name="TableColumns">Expected Table columns list </param>
        /// <param name="CreateIfNotExists">If not exist and the value is TRUE create table on the server</param>
        /// <returns>Return true if table exists or successfully created, otherwise return false</returns>
        public static Boolean IsTableExists(String table_name, System.Data.SqlClient.SqlConnection m_Conn, List<CustomDataColumn> TableColumns, Boolean CreateIfNotExists)
        {
            Boolean bResult = false;
            try
            {
                if (m_Conn.State != ConnectionState.Open)
                    m_Conn.Open();
                bResult = m_Conn.GetSchema("Tables").Select("Table_Name = '" + table_name + "'").Length > 0;
                if (!bResult && CreateIfNotExists)
                {
                    try
                    {
                        CreateTableWithColumns("dbo", table_name, m_Conn, TableColumns);
                        bResult = true;
                    }
                    finally
                    {
                    }
                }
            }
            catch { }
            finally
            {
                if (m_Conn.State == ConnectionState.Open)
                    m_Conn.Close();
            }
            return bResult;
        }

        /// <summary>
        /// The constructor of class, the only way to create an instance
        /// of dataCollector class. Using space or special characters in
        /// table or owner name is not recommended.
        /// </summary>
        /// <param name="tableOwner">Table Creator user name</param>
        /// <param name="tableName">Table name to look for</param>
        /// <param name="m_Conn">Connection to server database</param>
        /// <param name="TableColumns">Expected Table columns list </param>
        /// <returns>Return true if table exists or successfully created, otherwise return false</returns>
        public static Boolean CreateTableWithColumns(String tableOwner,
                                                    String tableName,
                                                    System.Data.SqlClient.SqlConnection m_Conn,
                                                    List<CustomDataColumn> TableColumns)
        {
            Boolean bResult = false;
            try
            {
                // This DataTable will store the data.
                DataTable dataTable = new DataTable(tableName);

                List<String> createCKCommandText_i = new List<string>();
                List<String> createPKCommandText_i = new List<String>();
                String createTableCommandText_i = "CREATE TABLE [" + tableOwner + "].[" + tableName + "]" + " (" + Environment.NewLine;
                // Checking the column definition collection:
                //  1. Generating Column objects in dt.
                //  2. Adding column definition text to create command text.
                //  3. Making primary key generator command text, if necessary.
                // Warning! This class can add only one primary key to a single column.
                // If you defined more, only the last one will added.
                foreach (CustomDataColumn colDef in TableColumns)
                {
                    // adds the column to the DataTable
                    dataTable.Columns.Add(colDef);

                    if (colDef.SQLColumnType == SqlDbType.Udt)
                    {
                        CreateRule(m_Conn, colDef.ColumnType);
                        CreateType(new SqlCommand("", m_Conn), tableName, colDef.ColumnType);
                    }

                    createTableCommandText_i += colDef.ToString();
                    createTableCommandText_i += "," + Environment.NewLine;


                    // primary key producing command
                    if (colDef.IsPrimaryKey)
                    {
                        createPKCommandText_i.Add(GetPrimaryKeySQLString(colDef, tableName));
                    }
                    else if (colDef.Unique)// Unique producing command
                    {
                        createCKCommandText_i.Add(GetUniqueConstrainSQLString(colDef, tableName));
                    }
                }
                // end of create table command
                createTableCommandText_i = createTableCommandText_i.Remove
                    (createTableCommandText_i.Length - Environment.NewLine.Length - 1);
                createTableCommandText_i += ")";

                if (m_Conn.State != ConnectionState.Open)
                    m_Conn.Open();

                // Running create table
                SqlCommand sqlComm = new SqlCommand(createTableCommandText_i, m_Conn);

                sqlComm.ExecuteNonQuery();
                // Adding primary key constraint if needed.
                foreach (String str in createPKCommandText_i)
                {
                    SqlCommand sqlCommPK = new SqlCommand(str, m_Conn);
                    sqlCommPK.ExecuteNonQuery();
                }

                // Adding Other constraint if needed.
                foreach (String str in createCKCommandText_i)
                {
                    SqlCommand sqlCommPK = new SqlCommand(str, m_Conn);
                    sqlCommPK.ExecuteNonQuery();
                }
                bResult = true;
            }
            catch
            {
                bResult = false;
            }
            finally
            {
                if (m_Conn.State == ConnectionState.Open)
                    m_Conn.Close();
            }
            return bResult;
        }

        /// <summary>
        /// Check if Column exists in specific table
        /// </summary>
        /// <param name="table_name">Table name to look for</param>
        /// <param name="m_Conn">Connection to server database</param>
        /// <param name="TableColumns">Expected Table columns list </param>
        /// <param name="CreateIfNotExists">If not exist and the value is TRUE create Column at specific table on the server</param>
        /// <returns>>Return true if table exists or successfully created, otherwise return false</returns>
        public static bool CheckTableColumnsExists(String table_name,
                                            System.Data.SqlClient.SqlConnection m_Conn,
                                            List<CustomDataColumn> TableColumns,
                                            Boolean CreateIfNotExists)
        {
            Boolean bResult = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                    m_Conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = new SqlCommand("Select * From [" + table_name + "]", m_Conn);
                dataAdapter.Fill(ds, table_name);
                DataTable tbl = ds.Tables[0];

                foreach (CustomDataColumn Colmn in TableColumns)
                {
                    bResult = ColumnExists(tbl, Colmn.ColumnName);

                    if (!bResult)
                    {
                        if (CreateIfNotExists)
                        {
                            //tbl.Columns.Add(Colmn);
                            SqlCommand cmd = m_Conn.CreateCommand();

                            if (Colmn.SQLColumnType == SqlDbType.Udt)
                            {
                                CreateRule(m_Conn, Colmn.ColumnType);
                                CreateType(cmd, table_name, Colmn.ColumnType);
                            }

                            if (m_Conn.State != ConnectionState.Open)
                                m_Conn.Open();

                            cmd.CommandText = "ALTER TABLE [" + table_name + "] " + "ADD " + Colmn.ToString();
                            cmd.ExecuteNonQuery();

                            // primary key producing command
                            if (Colmn.IsPrimaryKey)
                            {
                                cmd.CommandText = GetPrimaryKeySQLString(Colmn, table_name);
                                cmd.ExecuteNonQuery();
                            }
                            else if (Colmn.Unique)// Unique producing command
                            {
                                cmd.CommandText = GetUniqueConstrainSQLString(Colmn, table_name);
                                cmd.ExecuteNonQuery();
                            }

                            bResult = true;
                        }
                        else
                        {
                            bResult = false;
                            break;
                        }
                    }
                }
            }
            catch
            {
                bResult = false;
            }
            finally
            {
                if (m_Conn.State == ConnectionState.Open)
                    m_Conn.Close();
            }

            return bResult;
        }

        private static Boolean CreateType(SqlCommand cmd, String table_name, Type FieldType)
        {
            Boolean bResult = false;
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();
                cmd.CommandText = " CREATE TYPE " + FieldType.Name;
                cmd.CommandText += " FROM [nvarchar](100) NOT NULL";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "EXEC sys.sp_bindrule @rulename=N'[";
                cmd.CommandText += FieldType.Name + "Rule]' , @objname=N'[";
                cmd.CommandText += FieldType.Name + "]' , @futureonly='futureonly'";
                cmd.ExecuteNonQuery();
                bResult = true;
            }
            catch
            {
                bResult = false;
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                    cmd.Connection.Close();
            }
            return bResult;
        }



        /// <summary>
        /// Detect the table name from class.
        /// If found property "TableName" return it's value Otherwise use class name as table name
        /// </summary>
        /// <typeparam name="T">The class type user wants to use</typeparam>
        /// <param name="ClassToRead">Class reference</param>
        /// <returns></returns>
        public static String GetTableNameFromClass<T>(T ClassToRead)
        {
            System.Reflection.PropertyInfo[] pr = ClassToRead.GetType().GetProperties();

            String sTableName = ClassToRead.GetType().Name;
            foreach (System.Reflection.PropertyInfo pi in pr)
            {
                if (pi.Name == "TableName")
                {
                    sTableName = pi.GetValue(ClassToRead, null).ToString();
                    break;
                }
            }


            return sTableName;
        }

        /// <summary>
        /// Detect the table Private Key name from class.
        /// If found property with DBColumnAttribute IsPrimaryKey property set to true return it's name
        /// Otherwise use default Key name "ID"
        /// </summary>
        /// <typeparam name="T">The class type user wants to use</typeparam>
        /// <param name="ClassToRead">Class reference</param>
        /// <returns>Return the Primary Key name for current class</returns>
        public static String GetTable_PK_Name_FromClass<T>(T ClassToRead)
        {
            String sPK = "ID";
            System.Reflection.PropertyInfo[] pr = ClassToRead.GetType().GetProperties();

            foreach (System.Reflection.PropertyInfo pi in pr)
            {
                DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                if (attrs.Length > 0)
                {
                    if (attrs[0].IsPrimaryKey)
                    {
                        sPK = attrs[0].ColumnName;
                        break;
                    }
                }
            }
            return sPK;
        }

        public static Object GetTable_PK_Value_FromClass<T>(T ClassToRead)
        {
            Object vPK = 0;
            System.Reflection.PropertyInfo[] pr = ClassToRead.GetType().GetProperties();

            foreach (System.Reflection.PropertyInfo pi in pr)
            {
                DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                if (attrs.Length > 0)
                {
                    if (attrs[0].IsPrimaryKey)
                    {
                        vPK = pi.GetValue(ClassToRead, null);
                        break;
                    }
                }
            }
            return vPK;
        }

        public static bool CreateRule(System.Data.SqlClient.SqlConnection m_Conn,
                                      Type FieldType)
        {
            Boolean bResult = false;
            try
            {
                String RuleName = FieldType.Name + "Rule";
                String RuleProperty = FieldType.Name;

                Boolean bFirstTime = true;

                Type underlyingType = Enum.GetUnderlyingType(FieldType);

                String SQL_Rule = "Create RULE " + RuleName + " as @" + RuleProperty;
                //Look for our string value associated with fields in this enum
                foreach (FieldInfo fi in FieldType.GetFields())
                {
                    //Check for our custom attribute
                    StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                    if (attrs.Length > 0)
                    {
                        if (!bFirstTime)
                            SQL_Rule += " OR @" + RuleProperty;
                        SQL_Rule += "='" + attrs[0].Value + "'";
                        bFirstTime = false;
                    }
                }




                if (m_Conn.State != ConnectionState.Open)
                    m_Conn.Open();

                // Running create table
                SqlCommand sqlComm = new SqlCommand(SQL_Rule, m_Conn);

                sqlComm.ExecuteNonQuery();
                bResult = true;
            }
            catch
            {
                bResult = false;
            }
            finally
            {
                if (m_Conn.State == ConnectionState.Open)
                    m_Conn.Close();
            }
            return bResult;
        }

        #endregion Data Managment Functions

        #region Private functions

        private static bool ColumnExists(DataRow tbl, string p)
        {
            return tbl.Table.Columns.Contains("[" + p + "]");
        }

        private static bool ColumnExists(DataTable Table, string ColumnName)
        {
            bool bRet = false;
            foreach (DataColumn col in Table.Columns)
            {
                if (col.ColumnName == ColumnName)
                {
                    bRet = true;
                    break;
                }
            }

            return bRet;
        }

        private static String GetPrimaryKeySQLString(CustomDataColumn colDef, String tableName)
        {
            String ColumnName = "";

            for (int i = 0; i < colDef.ColumnName.Length; i++)
                if (colDef.ColumnName[i] != ' ')
                    ColumnName += colDef.ColumnName[i];
                else
                    ColumnName += '_';

            return "ALTER TABLE [" + tableName + "]" + Environment.NewLine +
                    " ADD CONSTRAINT [PK_" + tableName + "_" + ColumnName + "] PRIMARY KEY CLUSTERED" + Environment.NewLine +
                    "([" + colDef.ColumnName + "])";
        }

        private static String GetUniqueConstrainSQLString(CustomDataColumn colDef, String tableName)
        {
            String ColumnName = "";

            for (int i = 0; i < colDef.ColumnName.Length; i++)
                if (colDef.ColumnName[i] != ' ')
                    ColumnName += colDef.ColumnName[i];
                else
                    ColumnName += '_';

            return "ALTER TABLE [" + tableName + "]" + Environment.NewLine +
                    " ADD CONSTRAINT [CK_" + tableName + "_" + ColumnName + "] " + Environment.NewLine +
                    "UNIQUE ([" + colDef.ColumnName + "])";
        }

        #endregion Private functions
    }
}