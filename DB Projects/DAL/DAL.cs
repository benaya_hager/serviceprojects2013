﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using NS_Common;
using NS_Common.Event_Arguments;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.VisualBasic;
using NS_DAL.Service;
using Tools.CustomRegistry;
using Tools.String_Enum;

//using Microsoft.ApplicationBlocks.Data;

namespace NS_DAL
{
    public class DAL : IDisposable
    {
        #region Public events & delegate functions

        #region Update Splash Screen Event

        /// <summary>
        /// The events delegate function. Optional event, occurs when there is long operation and MotionController requests to update splash screen.
        /// </summary>
        /// <param name="Sender">The control that raised event handler</param>
        /// <param name="args">Not in use</param>
        public delegate void UpdateSplashScreenHandler(Object Sender, UpdateSplashEventArgs args);
        /// <summary>
        /// Optional event, occurs when there is long operation and MotionController requests to update splash screen.
        /// </summary>
        public static event UpdateSplashScreenHandler UpdateSplashScreen;

        /// <summary>
        /// Service function, used to check if there is handler for raised event declared.
        /// </summary>
        /// <param name="Sender">The Axis that raised the event</param>
        /// <param name="args"></param>
        public void OnUpdateSplashScreen(object sender, UpdateSplashEventArgs args)
        {
            if (UpdateSplashScreen != null)
                UpdateSplashScreen(sender, args);
        }

        #endregion Update Splash Screen Event

        #endregion Public events & delegate functions

        #region Singleton implementation

        static DAL m_Instance = null;

        /// <summary>
        /// Private member Constructor
        /// Initialize all the parameters for DBManager
        /// </summary>
        private DAL()
        {
            OnUpdateSplashScreen(this, new UpdateSplashEventArgs(10, "DAL -> Initialize registry data"));
            try { InitRegistryData();}
            catch 
            {

            }
            
            //Application.ThreadExit += new EventHandler(Application_ThreadExit);

            m_Conn = new SqlConnection(ConnectionString);

            OnUpdateSplashScreen(this, new UpdateSplashEventArgs(100, "DAL Initialization Completed"));
        }

        /// <summary>
        /// Simple class destructor
        /// </summary>
        ~DAL()
        {
            if (null != m_Conn)
            {
                if (m_Conn.State != ConnectionState.Closed)
                    m_Conn.Close();
                m_Conn.Dispose();
                m_Conn = null;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (null != m_Conn)
            {
                if (m_Conn.State != ConnectionState.Closed)
                    m_Conn.Close();
                m_Conn.Dispose();
                m_Conn = null;
            }
        }

        #endregion IDisposable Members

        /// <summary>
        /// Occurs on application exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_ThreadExit(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Get the reference to the DBManager object.
        /// </summary>
        /// <returns>The reference to the DBManager object. There is only one Instance in the system </returns>
        public static DAL GetInstance()
        {
            if (m_Instance == null)
                m_Instance = new DAL();
            return m_Instance;
        }

        /// <summary>
        /// Release the DAL
        /// </summary>
        public static void Release()
        {
            if (m_Instance != null)
            {
                m_Instance = null;
            }
        }

        #endregion Singleton implementation

        #region Public members

        private System.Data.SqlClient.SqlConnection m_Conn = null;

        private String m_ConnectionString = "packet size=4096; " +
                                            "user id=igalk;" +
                                            "Password='12345';" +
                                            "data source='tlvsql';" +
                                            "Database=r&d_results";

        /// Get the database connection string
        /// </summary>
        public String ConnectionString
        {
            get { return m_ConnectionString; }
            private set { m_ConnectionString = value; }
        }

        private Int32 m_WorkDisconnected = 0;

        /// <summary>
        /// Work in simulator mode. Disconnected from database
        /// </summary>
        public Boolean WorkDisconnected { get { return (m_WorkDisconnected == 0 ? false : true); } private set { m_WorkDisconnected = (value == false ? 0 : 1); } }

        #region Global Settings

        private Boolean m_isSilent = true;

        /// <summary>
        /// Set/Get the value indicate if control displays error messages
        /// </summary>
        public Boolean isSilent
        {
            get { return m_isSilent; }
            set { m_isSilent = value; }
        }

        private String m_ErrorMessage = "";

        /// <summary>
        /// Get last Error message
        /// </summary>
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            private set { m_ErrorMessage = value; }
        }

        private int m_ErrorStatus = ErrorCodesList.OK;

        /// <summary>
        /// Get/Set the error status for cont
        /// </summary>
        public int ErrorStatus
        {
            get { return m_ErrorStatus; }
            set { m_ErrorStatus = value; }
        }

        #endregion Global Settings

        #endregion Public members

        #region Public Methods

        public short LoadDataTable<T>(ref T Logic, Int32 RowID) where T : class, new()
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            if (WorkDisconnected) return ErrorCodesList.OK;

            if (CheckDataBaseColumns<T>(Logic, false, true))
                return SelectByPK<T>(RowID.ToString(), out Logic);
            else
            {
                return ErrorCodesList.FAILED;
            }
        }

        public short LoadDataTable<T>(T Logic, Int32 RowID) where T : class, new()
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            if (WorkDisconnected) return ErrorCodesList.OK;

            if (CheckDataBaseColumns<T>(Logic, false, true))
                return SelectByPK<T>(RowID.ToString(), out Logic);
            else
                return ErrorCodesList.FAILED;
        }

        public short SelectAllToDataTable<T>(T Logic, out DataTable result) where T : class
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            Exception ExceptionRaised = null;

            String sTableName = DBCreator.GetTableNameFromClass(Logic);
            short iSucceded = ErrorCodesList.FAILED;
            result = null;
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                        SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                        break;
                                    }
                            }
                        }
                    }
                    //String SQL = "SELECT [" + sTableName + "].* [" +  SQL_AditionalTables + "] FROM [" +sTableName + "]";
                    //String SQL = ;
                    DataSet ds = SqlHelper.ExecuteDataset(m_Conn, CommandType.Text, String.Format("SELECT [{0}].* {1} FROM [{0}] {2}", sTableName, SQL_AditionalTables, SQL_AditionalTablesINNERJOIN));
                    result = ds.Tables[0];
                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short SelectAllToDataTableDS<T>(T Logic, out DataSet result) where T : class
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            Exception ExceptionRaised = null;

            String sTableName = DBCreator.GetTableNameFromClass(Logic);
            short iSucceded = ErrorCodesList.FAILED;
            result = null;
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                        SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                        break;
                                    }
                            }
                        }
                    }
                    //String SQL = "SELECT [" + sTableName + "].* [" +  SQL_AditionalTables + "] FROM [" +sTableName + "]";
                    //String SQL = ;
                    result = SqlHelper.ExecuteDataset(m_Conn, CommandType.Text, String.Format("SELECT [{0}].* {1} FROM [{0}] {2}", sTableName, SQL_AditionalTables, SQL_AditionalTablesINNERJOIN));

                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short SelectAllToClass<T>(out List<T> result) where T : class, new()
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            Exception ExceptionRaised = null;

            String sTableName = DBCreator.GetTableNameFromClass(new T());
            short iSucceded = ErrorCodesList.FAILED;
            result = new List<T>();
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                        SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                        break;
                                    }
                            }
                        }
                    }

                    DataSet ds = new DataSet();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = new SqlCommand(String.Format("SELECT [{0}].* {1} FROM [{0}] {2}", sTableName, SQL_AditionalTables, SQL_AditionalTablesINNERJOIN), m_Conn);
                    dataAdapter.Fill(ds, sTableName);
                    DataTable tbl = ds.Tables[0];

                    foreach (DataRow myRow in tbl.Rows)
                    {
                        T SingleRow = new T();
                        foreach (System.Reflection.PropertyInfo pi in pr)
                        {
                            DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                            if (attrs.Length > 0)
                            {
                                if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[attrs[0].ColumnName].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                {
                                    switch (attrs[0].FieldType)
                                    {
                                        case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                            {
                                                if (attrs[0].LocalPropertyName != "")
                                                {
                                                    System.Reflection.PropertyInfo LocalPi = typeof(T).GetProperty(attrs[0].LocalPropertyName);
                                                    LocalPi.SetValue(SingleRow, isNull(myRow[attrs[0].AditionalFieldDisplayTitleName]), null);
                                                }
                                                break;
                                            }
                                    }
                                    pi.SetValue(SingleRow, isNull(myRow[attrs[0].ColumnName]), null);
                                }
                            }
                            else
                            {
                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[pi.Name].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                    pi.SetValue(SingleRow, isNull(myRow[pi.Name]), null);
                            }
                        }
                        result.Add(SingleRow);
                    }
                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }

            return iSucceded;
        }

        public short SelectByParamsToClass<T>(T Params, out T result) where T : class, new()
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;

            Exception ExceptionRaised = null;
            bool whereExist = false;
            String WhereString = "";

            result = new T();
            String sTableName = DBCreator.GetTableNameFromClass(Params);
            String sPrimaryKey = DBCreator.GetTable_PK_Name_FromClass(Params);
            short iSucceded = ErrorCodesList.FAILED;
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                        SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                        break;
                                    }
                            }
                        }
                    }

                    DataSet ds = new DataSet();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    String StrSQL = String.Format("SELECT [{0}].* {1} FROM [{0}] {2}", sTableName, SQL_AditionalTables, SQL_AditionalTablesINNERJOIN);

                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            if (Usable2Search(pi.GetValue(Params, null)))
                            {
                                switch (attrs[0].FieldType)
                                {
                                    case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                        {
                                            if (GetStringValue(pi, Params) == "1") //In case of Unknown
                                                continue;
                                            break;
                                        }
                                }
                                //WhereString += (whereExist ? " AND [" : " WHERE [") + attrs[0].ColumnName + "] like '" + GetStringValue(pi, Params) + "'";
                                WhereString += (whereExist ? " AND [" : " WHERE [") + sTableName + "].[" + attrs[0].ColumnName + "] like '" + GetStringValue(pi, Params) + "'";
                                whereExist = true;
                            }
                        }
                        else
                        {
                            if (Usable2Search(pi.GetValue(Params, null)))
                            {
                                WhereString += (whereExist ? " AND [" : " WHERE [") + pi.Name + "] like '" + GetStringValue(pi, Params) + "'";
                                whereExist = true;
                            }
                        }
                    }

                    StrSQL += WhereString;
                    dataAdapter.SelectCommand = new SqlCommand(StrSQL, m_Conn);
                    dataAdapter.Fill(ds, sTableName);
                    DataTable tbl = ds.Tables[0];

                    foreach (DataRow myRow in tbl.Rows)
                    {
                        T SingleRow = new T();
                        foreach (System.Reflection.PropertyInfo pi in pr)
                        {
                            DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                            if (attrs.Length > 0)
                            {
                                if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[attrs[0].ColumnName].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                {
                                    switch (attrs[0].FieldType)
                                    {
                                        case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                            {
                                                if (attrs[0].LocalPropertyName != "")
                                                {
                                                    System.Reflection.PropertyInfo LocalPi = typeof(T).GetProperty(attrs[0].LocalPropertyName);
                                                    LocalPi.SetValue(SingleRow, isNull(myRow[attrs[0].AditionalFieldDisplayTitleName]), null);
                                                }
                                                break;
                                            }
                                    }
                                    pi.SetValue(SingleRow, isNull(myRow[attrs[0].ColumnName]), null);
                                }
                            }
                            else
                            {
                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[pi.Name].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                    pi.SetValue(SingleRow, isNull(myRow[pi.Name]), null);
                            }
                        }
                        result = SingleRow;
                    }
                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short SelectByParamsToDataTable<T>(T Params, out DataTable result)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;
            bool whereExist = false;
            String WhereString = "";
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            String sTableName = DBCreator.GetTableNameFromClass(Params);
            String sPrimaryKey = DBCreator.GetTable_PK_Name_FromClass(Params);
            short iSucceded = ErrorCodesList.FAILED;
            result = null;

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                foreach (System.Reflection.PropertyInfo pi in pr)
                {
                    DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                        switch (attrs[0].FieldType)
                        {
                            case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                {
                                    SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                    SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                    break;
                                }
                        }
                    }
                }

                String StrSQL = String.Format("SELECT [{0}].* {1} FROM [{0}]", sTableName, SQL_AditionalTables);
                foreach (System.Reflection.PropertyInfo pi in pr)
                {
                    DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                    if (attrs.Length > 0)
                    {
                        if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                        if (Usable2Search(pi.GetValue(Params, null)))
                        {
                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        if (GetStringValue(pi, Params) == "1") //In case of Unknown
                                            continue;
                                        break;
                                    }
                            }

                            Object val = pi.GetValue(Params, null);

                            if ((pi.GetValue(Params, null).GetType().BaseType.ToString() == "System.Enum") &&
                                (GetStringValue(pi, Params) == "Unknown")) //In case of Unknown
                                continue;

                            //WhereString += (whereExist ? " AND [" : " WHERE [") + attrs[0].ColumnName + "] like '" + GetStringValue(pi, Params) + "'";
                            WhereString += (whereExist ? " AND [" : " WHERE [") + sTableName + "].[" + attrs[0].ColumnName + "] like '" + GetStringValue(pi, Params) + "'";
                            whereExist = true;
                        }
                    }
                    else
                    {
                        if (Usable2Search(pi.GetValue(Params, null)))
                        {
                            WhereString += (whereExist ? " AND [" : " WHERE [") + pi.Name + "] like '" + GetStringValue(pi, Params) + "'";
                            whereExist = true;
                        }
                    }
                }

                StrSQL += SQL_AditionalTablesINNERJOIN + WhereString;
                DataSet ds = new DataSet();
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();
                    ds = SqlHelper.ExecuteDataset(m_Conn, CommandType.Text, StrSQL);
                    //SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    //dataAdapter.SelectCommand = new SqlCommand(StrSQL, m_Conn);
                    //dataAdapter.Fill(ds, sTableName);
                    result = ds.Tables[0];
                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short SelectByPK<T>(String PK_Value, out T result) where T : class, new()
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;

            result = new T();
            String sTableName = DBCreator.GetTableNameFromClass(result);
            String sPrimaryKey = DBCreator.GetTable_PK_Name_FromClass(result);
            short iSucceded = ErrorCodesList.FAILED;
            String SQL_AditionalTables = "";
            String SQL_AditionalTablesINNERJOIN = "";

            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    System.Reflection.PropertyInfo[] pr = typeof(T).GetProperties();
                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                    {
                                        SQL_AditionalTables += String.Format(", [{0}].[{1}] AS [{1}]", attrs[0].AditionalTableName, attrs[0].AditionalFieldDisplayTitleName);
                                        SQL_AditionalTablesINNERJOIN += String.Format(" INNER JOIN [{0}] ON ([{1}].[{2}] = [{0}].ID)", attrs[0].AditionalTableName, sTableName, attrs[0].ColumnName);
                                        break;
                                    }
                            }
                        }
                    }

                    DataSet ds = new DataSet();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    //String.Format("SELECT [{0}].* {1} FROM [{0}] {2}", sTableName, SQL_AditionalTables, SQL_AditionalTablesINNERJOIN)
                    String Str = String.Format("SELECT [{0}].* {1} FROM [{0}] {4} Where ([{0}].[{2}] like '{3}')", sTableName, SQL_AditionalTables, sPrimaryKey, PK_Value, SQL_AditionalTablesINNERJOIN);
                    dataAdapter.SelectCommand = new SqlCommand(Str, m_Conn);
                    dataAdapter.Fill(ds, sTableName);
                    DataTable tbl = ds.Tables[0];

                    foreach (DataRow myRow in tbl.Rows)
                    {
                        T SingleRow = new T();
                        foreach (System.Reflection.PropertyInfo pi in pr)
                        {
                            DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                            if (attrs.Length > 0)
                            {
                                if (!(Boolean)attrs[0].IsDBVisibleAttribute) continue;

                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[attrs[0].ColumnName].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                {
                                    switch (attrs[0].FieldType)
                                    {
                                        case NS_Common.Service._DB_FIELD_TYPE.OTHER_TABLE_ID:
                                            {
                                                if (attrs[0].LocalPropertyName != "")
                                                {
                                                    System.Reflection.PropertyInfo LocalPi = typeof(T).GetProperty(attrs[0].LocalPropertyName);
                                                    LocalPi.SetValue(SingleRow, isNull(myRow[attrs[0].AditionalFieldDisplayTitleName]), null);
                                                }
                                                break;
                                            }
                                    }
                                    pi.SetValue(SingleRow, isNull(myRow[attrs[0].ColumnName]), null);
                                }
                            }
                            else
                            {
                                if (pi.GetValue(SingleRow, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    Object EnumValue = Tools.String_Enum.StringEnum.Parse(pi.GetValue(SingleRow, null).GetType(), myRow[pi.Name].ToString());
                                    pi.SetValue(SingleRow, EnumValue, null);
                                }
                                else
                                    pi.SetValue(SingleRow, isNull(myRow[pi.Name]), null);
                            }
                        }
                        result = SingleRow;
                    }
                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }

            return iSucceded;
        }

        public short Insert<T>(T ClassToInsert, Boolean bCheckDataBaseColumnsExists, out Object RecordID)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;

            RecordID = -1;
            short iSucceded = ErrorCodesList.FAILED;
            if (WorkDisconnected) return ErrorCodesList.OK;

            if (bCheckDataBaseColumnsExists)
            {
                if (!CheckDataBaseColumns<T>(ClassToInsert, true, false))
                {
                    return ErrorCodesList.FAILED;
                }
            }

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    String sTableName = DBCreator.GetTableNameFromClass(ClassToInsert);
                    String sqlString = "INSERT INTO [" + sTableName + "] (";
                    String sqlString2 = ") VALUES (";

                    System.Reflection.PropertyInfo[] pr = ClassToInsert.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if ((!(Boolean)attrs[0].IsDBVisibleAttribute) ||
                                (Boolean)attrs[0].AutoIncrement) continue;

                            if (null != pi.GetValue(ClassToInsert, null))
                            {
                                if (pi.GetValue(ClassToInsert, null).GetType().BaseType.ToString() == "System.Enum")
                                {
                                    String EnumStringValue = Tools.String_Enum.StringEnum.GetStringValue((Enum)pi.GetValue(ClassToInsert, null));
                                    sqlString += "[" + attrs[0].ColumnName + "]";
                                    sqlString2 += "'" + Nothing2DBNull(EnumStringValue) + "'";
                                    sqlString += ", ";
                                    sqlString2 += ", ";
                                }
                                else
                                {
                                    if (Nothing2DBNull(pi.GetValue(ClassToInsert, null)) != "")
                                    {
                                        sqlString += "[" + attrs[0].ColumnName + "]";
                                        sqlString2 += "'" + Nothing2DBNull(pi.GetValue(ClassToInsert, null)) + "'";
                                        sqlString += ", ";
                                        sqlString2 += ", ";
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (null != pi.GetValue(ClassToInsert, null))
                            {
                                if (pi.GetValue(ClassToInsert, null).GetType().BaseType.ToString() == "System.Enum")
                                { }
                                else
                                {
                                    sqlString += "[" + pi.Name + "]";
                                    sqlString2 += "'" + Nothing2DBNull(pi.GetValue(ClassToInsert, null)) + "'";
                                    sqlString += ", ";
                                    sqlString2 += ", ";
                                }
                            }
                        }
                    }
                    try
                    {
                        sqlString = sqlString.Substring(0, sqlString.LastIndexOf(", "));
                    }
                    catch { }

                    try
                    {
                        sqlString2 = sqlString2.Substring(0, sqlString2.LastIndexOf(", "));
                    }
                    catch { }

                    sqlString += sqlString2 + "); \n\r" + "SELECT @ID = SCOPE_IDENTITY()";

                    SqlCommand cmd = new SqlCommand(sqlString, m_Conn);

                    cmd.Parameters.Add("@ID", SqlDbType.Variant, 0, "ID");
                    cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();
                    RecordID = cmd.Parameters["@ID"].Value;

                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short Delete<T>(T ClassToDelete)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;
            short iSucceded = ErrorCodesList.FAILED;
            if (WorkDisconnected) return ErrorCodesList.OK;

            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    String sTableName = DBCreator.GetTableNameFromClass(ClassToDelete);
                    String sPrimaryKeyname = DBCreator.GetTable_PK_Name_FromClass(ClassToDelete);
                    Object vPrimaryKeyValue = DBCreator.GetTable_PK_Value_FromClass(ClassToDelete);

                    String sqlString = "DELETE [" + sTableName +
                                              "] \n\r WHERE [" +
                                                sPrimaryKeyname + "] = '" + vPrimaryKeyValue.ToString() + "'";

                    SqlCommand cmd = new SqlCommand(sqlString, m_Conn);

                    cmd.ExecuteNonQuery();

                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }

            return iSucceded;
        }

        public short Update<T>(T ClassToUpdate, Boolean bCheckDataBaseColumnsExists)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;
            short iSucceded = ErrorCodesList.FAILED;
            if (WorkDisconnected) return ErrorCodesList.OK;

            if (bCheckDataBaseColumnsExists)
            {
                if (!CheckDataBaseColumns<T>(ClassToUpdate, true, false))
                {
                    return ErrorCodesList.FAILED;
                }
            }
            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();

                    String sTableName = DBCreator.GetTableNameFromClass(ClassToUpdate);
                    String sqlString = "UPDATE [" + sTableName + "] SET \n\r";
                    String sPrimaryKeyname = DBCreator.GetTable_PK_Name_FromClass(ClassToUpdate);
                    Object vPrimaryKeyValue = DBCreator.GetTable_PK_Value_FromClass(ClassToUpdate);

                    System.Reflection.PropertyInfo[] pr = ClassToUpdate.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo pi in pr)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            if ((!(Boolean)attrs[0].IsDBVisibleAttribute) ||
                                (Boolean)attrs[0].AutoIncrement) continue;

                            if (null == pi.GetValue(ClassToUpdate, null))
                            {
                                pi.SetValue(ClassToUpdate, "", null);
                            }
                            if (pi.GetValue(ClassToUpdate, null).GetType().BaseType.ToString() == "System.Enum")
                            {
                                String EnumStringValue = Tools.String_Enum.StringEnum.GetStringValue((Enum)pi.GetValue(ClassToUpdate, null));
                                sqlString += "[" + attrs[0].ColumnName + "] = '" + Nothing2DBNull(EnumStringValue) + "'";
                            }
                            else
                            {
                                sqlString += "[" + attrs[0].ColumnName + "] = '" + Nothing2DBNull(pi.GetValue(ClassToUpdate, null)) + "'";
                            }
                        }
                        else
                        {
                            if (pi.GetValue(ClassToUpdate, null).GetType().BaseType.ToString() == "System.Enum")
                            {
                                String EnumStringValue = Tools.String_Enum.StringEnum.GetStringValue((Enum)pi.GetValue(ClassToUpdate, null));
                                sqlString += "[" + pi.Name + "] = '" + Nothing2DBNull(EnumStringValue) + "'";
                            }
                            else
                            {
                                sqlString += "[" + pi.Name + "] = '" + Nothing2DBNull(pi.GetValue(ClassToUpdate, null)) + "'";
                            }
                        }
                        sqlString += ", ";
                    }
                    sqlString = sqlString.Substring(0, sqlString.LastIndexOf(", "));

                    sqlString += "WHERE \n\r ([" +
                         sPrimaryKeyname + "] = '" + vPrimaryKeyValue.ToString() + "' )";

                    SqlCommand cmd = new SqlCommand(sqlString, m_Conn);

                    cmd.ExecuteNonQuery();

                    iSucceded = ErrorCodesList.OK;
                }
            }
            catch (Exception ex)
            {
                iSucceded = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }


            return iSucceded;
        }

        public short SelectByParamsToField<T>(T ClassToSelect, String PropertyName, out DataTable resultTable) where T : class
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;
            resultTable = null;

            if (WorkDisconnected) return ErrorCodesList.OK;
            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();
                    DataSet myDS = null;

                    System.Reflection.PropertyInfo pi = typeof(T).GetProperty(PropertyName);
                    if (null != pi)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.SINGLE_TO_MANY:
                                    {
                                        if ("" != attrs[0].SelectStoreProcName)
                                        {
                                            ParamListCollection pArray = new ParamListCollection();
                                            if (null != attrs[0].SelectFieldNames)
                                            {
                                                foreach (String FieldName in attrs[0].SelectFieldNames)
                                                {
                                                    System.Reflection.PropertyInfo LocalPi = typeof(T).GetProperty(FieldName);
                                                    if (null != LocalPi)
                                                    {
                                                        if (LocalPi.GetValue(ClassToSelect, null).GetType().BaseType.ToString() == "System.Enum")
                                                        {
                                                            String EnumStringValue = Tools.String_Enum.StringEnum.GetStringValue((Enum)LocalPi.GetValue(ClassToSelect, null));
                                                            pArray.Add(FieldName, Nothing2DBNull(EnumStringValue));
                                                        }
                                                        else
                                                        {
                                                            pArray.Add(FieldName, Nothing2DBNull(LocalPi.GetValue(ClassToSelect, null)));
                                                        }
                                                    }
                                                }
                                            }
                                            myDS = SqlHelper.ExecuteDataset(m_Conn, attrs[0].SelectStoreProcName, pArray.GetSQLParamArray());
                                            if (null != myDS)
                                            {
                                                if ((myDS.Tables.Count > 0) && (myDS.Tables[0].Rows.Count > 0))
                                                    resultTable = myDS.Tables[0];
                                            }
                                            else
                                            {
                                                m_ErrorMessage = String.Format("The select procedure found nothing!");
                                                m_ErrorStatus = ErrorCodesList.FAILED;
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            m_ErrorMessage = String.Format("The 'Select' Store procedure not set!");
                                            m_ErrorStatus = ErrorCodesList.FAILED;
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        m_ErrorMessage = "The FieldType of DBColumnAttribute has to be SINGLE_TO_MANY!";
                                        m_ErrorStatus = ErrorCodesList.FAILED;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            m_ErrorMessage = String.Format("The DBColumnAttribute for field '{0}' not found!", PropertyName);
                            m_ErrorStatus = ErrorCodesList.FAILED;
                        }
                    }
                    else
                    {
                        m_ErrorMessage = String.Format("The property '{0}' not found!", PropertyName);
                        m_ErrorStatus = ErrorCodesList.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                m_ErrorStatus = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }

            return (short)m_ErrorStatus;
        }

        public short SelectByParamsToField<T>(T ClassToSelect, String PropertyName, out DataSet resultDS) where T : class
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            Exception ExceptionRaised = null;
            resultDS = null;
            if (WorkDisconnected) return ErrorCodesList.OK;
            try
            {
                lock (m_Conn)
                {
                    if (m_Conn.State != ConnectionState.Open)
                        m_Conn.Open();
                    //DataSet myDS = null;

                    System.Reflection.PropertyInfo pi = typeof(T).GetProperty(PropertyName);
                    if (null != pi)
                    {
                        DBColumnAttribute[] attrs = pi.GetCustomAttributes(typeof(DBColumnAttribute), false) as DBColumnAttribute[];
                        if (attrs.Length > 0)
                        {
                            switch (attrs[0].FieldType)
                            {
                                case NS_Common.Service._DB_FIELD_TYPE.SINGLE_TO_MANY:
                                    {
                                        if ("" != attrs[0].SelectStoreProcName)
                                        {
                                            ParamListCollection pArray = new ParamListCollection();
                                            if (null != attrs[0].SelectFieldNames)
                                            {
                                                foreach (String FieldName in attrs[0].SelectFieldNames)
                                                {
                                                    System.Reflection.PropertyInfo LocalPi = typeof(T).GetProperty(FieldName);
                                                    if (null != LocalPi)
                                                    {
                                                        if (LocalPi.GetValue(ClassToSelect, null).GetType().BaseType.ToString() == "System.Enum")
                                                        {
                                                            String EnumStringValue = Tools.String_Enum.StringEnum.GetStringValue((Enum)LocalPi.GetValue(ClassToSelect, null));
                                                            pArray.Add(FieldName, Nothing2DBNull(EnumStringValue));
                                                        }
                                                        else
                                                        {
                                                            pArray.Add(FieldName, Nothing2DBNull(LocalPi.GetValue(ClassToSelect, null)));
                                                        }
                                                    }
                                                }
                                            }
                                            resultDS = SqlHelper.ExecuteDataset(m_Conn, attrs[0].SelectStoreProcName, pArray.GetSQLParamArray());
                                            break;
                                        }
                                        else
                                        {
                                            m_ErrorMessage = String.Format("The 'Select' Store procedure not set!");
                                            m_ErrorStatus = ErrorCodesList.FAILED;
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        m_ErrorMessage = "The FieldType of DBColumnAttribute has to be SINGLE_TO_MANY!";
                                        m_ErrorStatus = ErrorCodesList.FAILED;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            m_ErrorMessage = String.Format("The DBColumnAttribute for field '{0}' not found!", PropertyName);
                            m_ErrorStatus = ErrorCodesList.FAILED;
                        }
                    }
                    else
                    {
                        m_ErrorMessage = String.Format("The property '{0}' not found!", PropertyName);
                        m_ErrorStatus = ErrorCodesList.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                m_ErrorStatus = ErrorCodesList.FAILED;
                ExceptionRaised = ex;
            }
            finally
            {
                lock (m_Conn)
                {
                    if (m_Conn.State == ConnectionState.Open)
                        m_Conn.Close();
                }
            }

            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                if (ExceptionRaised != null)
                    throw ExceptionRaised;
            }

            return (short)m_ErrorStatus;
        }

        public Boolean CheckDataBaseColumns<T>(T ClassToRead, Boolean AddAutoNumberIdField, Boolean CreateIfNotExists)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            if (WorkDisconnected) return true;
            Class2CustomDataColumns TableColumns = new Class2CustomDataColumns(ClassToRead, AddAutoNumberIdField);
            String sTableName = DBCreator.GetTableNameFromClass(ClassToRead);

            lock (m_Conn)
            {
                if (DBCreator.IsTableExists(sTableName, m_Conn, TableColumns.dataColumns, CreateIfNotExists))
                {
                    if (DBCreator.CheckTableColumnsExists(sTableName, m_Conn, TableColumns.dataColumns, CreateIfNotExists))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Boolean CreateRule(Type FieldType)
        {
            m_ErrorMessage = "";
            m_ErrorStatus = ErrorCodesList.OK;
            if (WorkDisconnected) return true;
            return DBCreator.CreateRule(m_Conn, FieldType);
        }

        #endregion Public Methods

        #region Private functions

        #region Service functions

        /// <summary>
        /// Get base data from registry
        /// </summary>
        private int InitRegistryData()
        {
            try
            {
                cRegistryFunc objRegistry = new cRegistryFunc("Service Projects",
                                                              "DAL",
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

                m_ConnectionString = objRegistry.GetRegKeyValue("", "Connection String", m_ConnectionString);
                objRegistry.SetRegKeyValue("", "Connection String", m_ConnectionString);

                m_WorkDisconnected = objRegistry.GetRegKeyValue("", "Work Disconnected", m_WorkDisconnected);
                objRegistry.SetRegKeyValue("", "Work Disconnected", m_WorkDisconnected);
            }
            catch { };
            return ErrorCodesList.OK;
        }

        private object isNull(object value)
        {
            if (value == System.DBNull.Value) return null;
            return value;
        }

        private object isNull(object value, System.TypeCode RequiredType)
        {
            if (value == System.DBNull.Value)
            {
                switch (RequiredType)
                {
                    case TypeCode.String:
                        return "";
                    case TypeCode.Int16:
                        break;
                    case TypeCode.Int32:
                        break;
                    case TypeCode.Int64:
                        break;
                    case TypeCode.Single:
                        return 0;
                    default:
                        return (null);
                }
            }
            return value;
        }

        /// <summary>
        /// Check the Single value is not Null and valid to be written in database
        /// </summary>
        /// <param name="sngl">Single value to be checked</param>
        /// <returns>Return DBNull.Null in case the Single is not valid</returns>
        /// <remarks></remarks>
        private String Nothing2DBNull(Object obj)
        {
            switch (obj.GetType().ToString())
            {
                case "System.String":
                    {
                        if (obj == null) return " ";
                        return Convert.ToString(obj);
                    }
                case "System.Byte":
                    {
                        if (obj == null) return Convert.ToString(default(Byte));
                        return Convert.ToString(obj);
                    }
                case "System.Char":
                    {
                        if (obj == null) return Convert.ToString(default(Char));
                        return Convert.ToString(obj);
                    }
                //Localized settings
                case "System.DateTime":
                    {
                        if (obj == null) return Convert.ToString(default(DateTime));
                        return Convert.ToString(obj, DateTimeFormatInfo.InvariantInfo);
                    }
                case "System.Int32":
                case "System.Int64":
                case "System.UInt16":
                case "System.Decimal":
                case "System.Single":
                case "System.UInt32":
                case "System.Double":
                case "System.UInt64":
                case "System.Int16":
                    {
                        if (obj == null) return Convert.ToString(0, NumberFormatInfo.InvariantInfo);
                        return Convert.ToString(obj, NumberFormatInfo.InvariantInfo);
                    }
                default://Default to the String value
                    return Convert.ToString(obj);
            }
        }

        /// <summary>
        /// Returns true only if the value of str is legal string value
        /// </summary>
        /// <param name="str">Value to check</param>
        /// <returns>False in case the value of str is not a valid string value, otherwise true</returns>
        /// <remarks></remarks>
        protected static bool Usable2SearchString(object str)
        {
            //returns true only if the value of str is legal string value
            if (str == System.DBNull.Value) return false;
            if (str == null) return false;
            if (string.IsNullOrEmpty((String)str)) return false;
            if (((String)str).Trim().Length == 0) return false;
            return true;
        }

        /// <summary>
        /// Returns true only if the value of Value is legal integer value greater then 0
        /// </summary>
        /// <param name="str">Value to check</param>
        /// <returns>False in case the value of str is not a valid integer value, otherwise true</returns>
        /// <remarks></remarks>
        protected static bool Usable2SearchInt(object str)
        {
            //returns true only if the value of str is legal integer value greater then 0
            if (str == System.DBNull.Value) return false;
            if (!Information.IsNumeric(str)) return false;
            if ((int)str == 0) return false;
            return true;
        }

        /// <summary>
        /// Returns true only if the value of str is legal date value
        /// </summary>
        /// <param name="str">The date to check</param>
        /// <returns>False in case the value of str is not a valid Date value, otherwise true</returns>
        /// <remarks></remarks>
        protected static bool Usable2SearchDate(object str)
        {
            //returns true only if the value of str is legal date value
            if (ValidDate((System.DateTime)str) == System.DBNull.Value) return false;
            //if (ValidDate(DateTime.Now) == System.DBNull.Value) return false;
            //if (IsDate((String) str)) return false;
            return true;
        }

        /// <summary>
        /// Returns true only if the value of obj is differ than 0 (Usualy Unknown type)
        /// </summary>
        /// <param name="obj">The value to check</param>
        /// <returns>False in case the value of obj is equal to 0, otherwise true</returns>
        /// <remarks></remarks>
        private static bool Usable2SearchEnum(object obj)
        {
            return Usable2SearchInt((Int32)obj);
        }

        protected static bool Usable2Search(object obj)
        {
            if (obj == null) return false;

            switch (obj.GetType().ToString())
            {
                case "System.Boolean":
                    return false;
                case "System.String":
                    return Usable2SearchString(obj);
                case "System.Byte":
                case "System.Char":
                    return Usable2SearchString(Convert.ToString(obj));
                case "System.DateTime":
                    return (Usable2SearchDate(obj));
                case "System.Int32":
                case "System.Int64":
                case "System.UInt16":
                case "System.UInt32":
                case "System.UInt64":
                case "System.Int16":
                    return (((Int32)obj) != 0);
                case "System.Decimal":
                case "System.Double":
                    return (((Decimal)obj) != 0);
                case "System.Single":
                    return (((Single)obj) != 0);
                case "System.Enum":
                    return Usable2SearchEnum(obj);
                default://Default to the String value
                    {
                        if (obj.GetType().BaseType.Equals(typeof(Enum)))
                        {
                            return Usable2SearchEnum(obj);
                        }
                        return false;
                    }
            }
        }

        /// <summary>
        /// Return the string value of current property in object
        /// </summary>
        /// <param name="pi">The property to read</param>
        /// <param name="Obj">that object to get the property of</param>
        /// <returns>Property to String value</returns>
        protected String GetStringValue(System.Reflection.PropertyInfo pi, object Obj)
        {
            Object val = pi.GetValue(Obj, null);
            System.Reflection.FieldInfo fi = val.GetType().GetField(val.ToString());

            if (null == fi) return val.ToString();

            StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
            if (attrs.Length > 0)
                return Tools.String_Enum.StringEnum.GetStringValue((Enum)val);
            else
                return val.ToString();
        }

        /// <summary>
        /// Function that checks the validity of dateTime object
        /// </summary>
        /// <param name="str">Value to check</param>
        /// <returns>Return DBNull.Value in case the date is not valid</returns>
        /// <remarks></remarks>
        public static object ValidDate(System.DateTime dt)
        {
            if (dt.Equals(default(DateTime)) | !Information.IsDate(dt)) return DBNull.Value;
            return dt;
        }

        #endregion Service functions

        #endregion Private functions
    }
}