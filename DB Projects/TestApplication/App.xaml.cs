﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

using System.Windows;
using Microsoft.Practices.Unity;
using DBManagerBase;
using NS_Common.ApplicationExtensions;
using All_Projects_Test_Application.Component_Builders;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application 
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            ComponentsBuilder builder = new ComponentsBuilder();

            builder.Bulid(this.GetContainer());

            builder.Container.Resolve<MainWindow>().Show(); 
        } 
        //public new static App Current { get { return (App)Application.Current; } } 
    }

  
}


