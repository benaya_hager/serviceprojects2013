﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Members

        private IUnityContainer m_Container;
        //[Dependency]
        public IUnityContainer Container
        {
            get { return m_Container; }
            set { m_Container = value; }
        }


        #endregion

        public MainWindow(IUnityContainer container)
        {
            m_Container = container;
            InitializeComponent();
        }
    }
}
