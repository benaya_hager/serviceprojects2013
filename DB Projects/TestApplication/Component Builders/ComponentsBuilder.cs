﻿using DBManagerBase;
using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;

namespace All_Projects_Test_Application.Component_Builders
{
    public class ComponentsBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public void Bulid(IUnityContainer container)
        {
             
            Container = container;

            Container.RegisterInstance<IUnityContainer>(Container);

            Container.RegisterType<IDBManagerComponentsBuilder, DBManagerComponentsBuilder>(); //"DBManagerComponentsBuilder", new InjectionConstructor(Container));

            Container.RegisterInstance<IDBManagerComponentsBuilder>(Container.Resolve<IDBManagerComponentsBuilder>());
        }

        public IUnityContainer Container { set; get; }
    }
}
