﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManagerBase.Views.User_Controls;
using Xceed.Wpf.Toolkit;
using DBManagerBase.Interfaces.Parameters_Pages.Product;
using Microsoft.Practices.Unity;

namespace DBManagerBase.Views.Parameters_Pages.Product
{
    /// <summary>
    /// Interaction logic for pgSelect_STP_Data.xaml
    /// </summary>
    public partial class pgSelect_Product_Data : Page
    {
        public pgSelect_Product_Data(IUnityContainer container)
        {
            InitializeComponent();

            this.DataContext = container.Resolve<ISelect_Product_Data_ViewModel>();
        }


        public bool ParseExceptions(Exception ex, System.Windows.Controls.Page page)
        {

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception))
            {
                ((System.Windows.Controls.Control)txtProductCatNumber).ShowWarningBaloonMessage("System Record update required",  "This is System record. Please do not try to change or modify it");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.CatalogNumberNotSetException))
            {
                ((System.Windows.Controls.Control)txtProductCatNumber).ShowWarningBaloonMessage("Mandatory Field Not Set", "Catalog Number not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.DuplicateCatalogNumberException))
            {
                ((System.Windows.Controls.Control)txtProductCatNumber).ShowWarningBaloonMessage("Duplicate Value", "Catalog Number is already in use!");
                return true;
            }
            ///Exception not found in predefined list
            return false;
        }
    }
}
