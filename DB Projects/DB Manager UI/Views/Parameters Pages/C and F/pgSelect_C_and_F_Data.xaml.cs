﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManagerBase.Views.User_Controls;
using DBManagerBase.Interfaces.Parameters_Pages.C_and_F;
using Microsoft.Practices.Unity;

namespace DBManagerBase.Views.Parameters_Pages.Projects
{
    /// <summary>
    /// Interaction logic for pgSelect_C_and_F_Data.xaml
    /// </summary>
    public partial class pgSelect_C_and_F_Data : Page
    {
        public pgSelect_C_and_F_Data(IUnityContainer container)
        {
            InitializeComponent();

            this.DataContext = container.Resolve<ISelect_C_and_F_Data_ViewModel>();
        }

        public bool ParseExceptions(Exception ex, System.Windows.Controls.Page page)
        {
            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception))
            {
                ((System.Windows.Controls.Control)txtName).ShowWarningBaloonMessage("System Record update required", "This is System record. Please do not try to change or modify it");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.C_AND_F_TitleNotSetException))
            {
                ((System.Windows.Controls.Control)txtName).ShowWarningBaloonMessage("Mandatory Field Not Set", "C && F Title/Name not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.Duplicate_C_AND__TitleException))
            {
                ((System.Windows.Controls.Control)txtName).ShowWarningBaloonMessage("Duplicate Value", "C && F Title/Name is already in use!");
                return true;
            }
            ///Exception not found in predefined list
            return false;
        }
    }
}
