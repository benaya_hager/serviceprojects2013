﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManagerBase.Views.User_Controls;
using Xceed.Wpf.Toolkit;
using Microsoft.Practices.Unity;
using DBManagerBase.Interfaces.Parameters_Pages.Project;

namespace DBManagerBase.Views.Parameters_Pages.Project
{
    /// <summary>
    /// Interaction logic for pgSelect_STP_Data.xaml
    /// </summary>
    public partial class pgSelect_Project_Data : Page
    {
        public pgSelect_Project_Data(IUnityContainer container) //, NS_BLL.Logics.Project.ProjectLogic project_Logic)
        {
            InitializeComponent();

            this.DataContext = container.Resolve<ISelect_Project_Data_ViewModel>();
           // this.DataContext = project_Logic;
        }


        public bool ParseExceptions(Exception ex, System.Windows.Controls.Page page)
        {

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.ProjectNumberNotSetException))
            {
                ((System.Windows.Controls.Control)txtProjectCatalogNumber).ShowWarningBaloonMessage("Mandatory Field Not Set", "Project Catalog Number not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNumberException))
            {
                ((System.Windows.Controls.Control)txtProjectCatalogNumber).ShowWarningBaloonMessage("Duplicate Value", "Project Catalog Number is already in use!");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.ProjectNameNotSetException))
            {
                ((System.Windows.Controls.Control)txtProjectName).ShowWarningBaloonMessage("Mandatory Field Not Set", "Project Name not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNameException))
            {
                ((System.Windows.Controls.Control)txtProjectName).ShowWarningBaloonMessage("Duplicate Value", "Project Name is already in use!");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.ProjectNicknameNameNotSetException))
            {
                ((System.Windows.Controls.Control)txtProjectNickname).ShowWarningBaloonMessage("Mandatory Field Not Set", "Project Nickname Name not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.DuplicateProjectNicknameException))
            {
                ((System.Windows.Controls.Control)txtProjectNickname).ShowWarningBaloonMessage("Duplicate Value", "Project Nickname is already in use!");
                return true;
            } 


            ///Exception not found in predefined list
            return false;
        }
    }
}
