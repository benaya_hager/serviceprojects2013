﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManagerBase.Views.User_Controls;
using Microsoft.Practices.Unity;
using DBManagerBase.Interfaces.Parameters_Pages;

namespace DBManagerBase.Views.Parameters_Pages
{
    /// <summary>
    /// Interaction logic for pgSelect_STP_Data.xaml
    /// </summary>
    public partial class pgSelect_STP_Data : Page
    {
        public pgSelect_STP_Data(IUnityContainer container)
        {
            InitializeComponent();

            this.DataContext = container.Resolve<ISelect_STP_Data_ViewModel>();
        }


        public bool ParseExceptions(Exception ex, System.Windows.Controls.Page page)
        {
            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.SYSTEM_RECORD_UPDATE_Exception))
            {
                ((System.Windows.Controls.Control)txtNUM).ShowWarningBaloonMessage("System Record update required", "This is System record. Please do not try to change or modify it");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.STP_SerialNumberNotSetException))
            {
                ((System.Windows.Controls.Control)txtNUM).ShowWarningBaloonMessage("Mandatory Field Not Set", "STP #NUM Field not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_SerialNumberException))
            {
                ((System.Windows.Controls.Control)txtNUM).ShowWarningBaloonMessage("Duplicate Value", "Such STP #NUM already in use!");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.STP_TitleNotSetException))
            {
                ((System.Windows.Controls.Control)txtTitle).ShowWarningBaloonMessage("Mandatory Field Not Set", "STP Title Field not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_TitleException))
            {
                ((System.Windows.Controls.Control)txtTitle).ShowWarningBaloonMessage("Duplicate Value", "Such STP Title already in use!");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.STP_VersionNotSetException))
            {
                ((System.Windows.Controls.Control)txtVersion).ShowWarningBaloonMessage("Mandatory Field Not Set", "STP Version not set !");
                return true;
            }

            if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.Duplicate_STP_VersionException))
            {
                ((System.Windows.Controls.Control)txtVersion).ShowWarningBaloonMessage("Duplicate Value", "Such STP Version already in use!");
                return true;
            }
            ///Exception not found in predefined list
            return false;
        }
    }
}
