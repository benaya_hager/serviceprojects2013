﻿using DBManagerBase.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Unity;
using System.ComponentModel;
using NS_Common.ApplicationExtensions ; 

namespace DBManagerBase.Views.User_Controls
{
    /// <summary>
    /// Interaction logic for ucWin8DBFieldSelectorView.xaml
    /// </summary>
    public partial class ucWin8DBFieldSelectorView : UserControl
    {

        #region Private Members

        private IUnityContainer m_Container;
        [Dependency]
        public IUnityContainer Container
        {
            get { return m_Container; }
            set
            { 
                m_Container = value;
                if (!DesignerProperties.GetIsInDesignMode(this))
                {
                    if (null != m_Container)
                    { 
                        m_Container.RegisterType<IWin8DBFieldSelectorViewModel, ucWin8DBFieldSelectorViewModel>();
                        m_Container.RegisterInstance<IWin8DBFieldSelectorViewModel>(m_Container.Resolve<IWin8DBFieldSelectorViewModel>());  //builder.Container.Resolve<ucWin8DBFieldSelectorViewModel>("ucWin8DBFieldSelectorViewModel");
                        this.DataContext = m_Container.Resolve<IWin8DBFieldSelectorViewModel>();
                    }
                }
            }
        }
     
        #endregion

        public ucWin8DBFieldSelectorView() //IUnityContainer container) //: this()
        {   
            InitializeComponent();
            if (null == m_Container)
            {
                //if (null == container)
                    Container = Application.Current.GetContainer();
                //else
                //    Container = container; 
            }
        }
     
    }
}
