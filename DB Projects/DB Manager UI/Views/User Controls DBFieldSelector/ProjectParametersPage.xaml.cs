﻿using Components;
using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DBManagerBase.Views.User_Controls
{
    /// <summary>
    /// Interaction logic for ProductParametersPage.xaml
    /// </summary>
    public partial class ProjectParametersPage : Page, SFW.IComponent // IBasePage, 
    {
        #region Private Members
            //[Dependency]
            IUnityContainer m_Container = null;
        #endregion

        #region Constructors

            public ProjectParametersPage(IUnityContainer container)
            {
                InitializeComponent();

                m_Container = container;
                this.DataContext = container.Resolve<IProjectParametersViewModel>();
            }

        #endregion


            string IComponent.Name
            {
                get { return "Project Parameters Page"; }
            }
    }
}
