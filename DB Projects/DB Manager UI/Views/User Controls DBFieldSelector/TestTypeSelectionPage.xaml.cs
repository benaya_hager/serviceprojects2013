﻿using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DBManagerBase.Views.User_Controls
{
    /// <summary>
    /// Interaction logic for TestTypeSelectionPage.xaml
    /// </summary>
    public partial class TestTypeSelectionPage : Page, SFW.IComponent  //IBasePage,
    {
        #region Private Members

        //[Dependency]
        IUnityContainer m_Container = null; 

        #endregion


        #region Constructors

            public TestTypeSelectionPage(IUnityContainer container)
            {
                InitializeComponent();

                m_Container = container;
                this.DataContext = container.Resolve<ITestTypeSelectionViewModel>();
            }

        #endregion


    }
}
