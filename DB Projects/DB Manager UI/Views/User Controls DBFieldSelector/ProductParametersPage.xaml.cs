﻿using Components;
using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NS_Common.ViewModels.Common;

namespace DBManagerBase.Views.User_Controls
{
    /// <summary>
    /// Interaction logic for ProductParametersPage.xaml
    /// </summary>
    public partial class ProductParametersPage : Page, SFW.IComponent // IBasePage, 
    {
        #region Private Members
            //[Dependency]
            IUnityContainer m_Container = null;
        #endregion

        #region Constructors

            public ProductParametersPage(IUnityContainer container)
            {
                InitializeComponent();

                m_Container = container;
                this.DataContext = container.Resolve<IProductParametersViewModel>();
            }

        #endregion


            string IComponent.Name
            {
                get { return "Product Parameters Page"; }
            }
    }
}
