﻿using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DBManagerBase.Views
{
    /// <summary>
    /// Interaction logic for WarningBalloonWindow.xaml
    /// </summary>
    public partial class WarningBalloonWindow : Window 
    {
     
        

        #region Private Members

        private IUnityContainer m_Container;
       
        //[Dependency]
        public IUnityContainer Container
        {
            get { return m_Container; }
            set { m_Container = value; }
        }


        #endregion



        public WarningBalloonWindow(string title, string message)
        {
            
            InitializeComponent();
            this.DataContext = new WarningBalloonWindowViewModel(title, message);
        } 
    }
}

