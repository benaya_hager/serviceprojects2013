﻿using DBManagerBase.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NS_Common.ApplicationExtensions;
using NS_Common; 

namespace DBManagerBase.Views
{
    /// <summary>
    /// Interaction logic for BaseDBPage.xaml
    /// </summary>
    public partial class BaseDBPage : Page, SFW.IComponent
    {

        #region Private Members
        
        private IUnityContainer m_Container;
        [Dependency]
        public IUnityContainer Container
        {
            get { return m_Container; }
            set
            {
                m_Container = value;

            }
        }

        #endregion

        #region Constructors

        public BaseDBPage(IUnityContainer unityContainer,
                          String window_caption,
                          FormStateType form_state_type,
                          Page user_area_target_page)
        {
            InitializeComponent();

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                m_Container = unityContainer;
                if (null != m_Container)
                {
                    IBaseDBViewModel dbView = m_Container.Resolve<IBaseDBViewModel>();
                    dbView.FormStateType = form_state_type;
                    dbView.ActiveContentPage = user_area_target_page;
                    dbView.WindowCaption = window_caption;
                    //dbView.ChildDataContext = user_area_target_page.DataContext;

                    this.DataContext = dbView;
                }
            }
        }

        #endregion
  
    }
}
