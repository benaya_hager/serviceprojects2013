﻿using System;
namespace DBManagerBase.ViewModels
{
    public interface IWin8DBFieldSelectorViewModel
    {
        System.Windows.Controls.Page ActiveContentPage { get; }
    }
}
