﻿using System;
namespace DBManagerBase
{
    public interface IDBManagerComponentsBuilder
    {
        NS_BLL.BLL Bll { get; set; }
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; set; }
        Microsoft.Practices.Unity.IUnityContainer Container { get; }
        Components.GUIFactory GuiFactory { get; }
    }
}
