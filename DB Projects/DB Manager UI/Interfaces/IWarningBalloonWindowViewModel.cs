﻿using System;
namespace DBManagerBase.ViewModels
{
    interface IWarningBalloonWindowViewModel
    {
        string WarningMessage { get; set; }
        string WarningTitle { get; set; }
    }
}
