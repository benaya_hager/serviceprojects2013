﻿using NS_Common;
using System;
using System.Windows.Controls;
namespace DBManagerBase.ViewModels
{
    public interface IBaseDBViewModel
    {
        System.Windows.Controls.Page ActiveContentPage { get; set; }
        String WindowCaption  { get; set; }
        FormStateType FormStateType { get; set; }
        //Object ChildDataContext { get; set; } 
    }
}
