﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.Unity;   

namespace DBManagerBase
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ComponentsBuilder builder = new ComponentsBuilder();

            builder.Build(); 
            builder.Container.Resolve<MainWindow>().Show();
        }
    }
}
