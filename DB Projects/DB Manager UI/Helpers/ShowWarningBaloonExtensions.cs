﻿using DBManagerBase.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;


namespace DBManagerBase.Views.User_Controls
{
    public enum MessageTitleType
    {
        ErrorMessage = 0x1,
        QiestionMessage = 0x2,
        InfoMessage = 0x4,
        WarningMessage = 0x8
    }

    public static class ShowWarningBaloonExtensions
    {

        /// <summary>
        /// Method to test if the control or it's parent is in design mode
        /// </summary>
        /// <param name="control">Control to examine</param>
        /// <returns>True if in design 12mode, otherwise false</returns>
        private static bool ResolveDesignMode(System.Windows.Controls.UserControl control)
        {
            System.Reflection.PropertyInfo designModeProperty;
            bool designMode;


            // Get the protected property
            designModeProperty = control.GetType().GetProperty(
                                    "DesignMode",
                                    System.Reflection.BindingFlags.Instance
                                    | System.Reflection.BindingFlags.NonPublic);


            // Get the controls DesignMode value
            designMode = (bool)designModeProperty.GetValue(control, null);


            // Test the parent if it exists
            //if (control.Parent != null)
            //{
            //    designMode |= ResolveDesignMode(control.Parent);
            //}


            return designMode;
        }


        public static void ShowWarningBaloonMessage(this System.Windows.Controls.Control control, String title, String message)
        {
            Point location;
            WarningBalloonWindow Balloon = new WarningBalloonWindow(title, message);

            location = GetControlPosition(control);
            Balloon.Left = location.X;
            Balloon.Top = location.Y;



            Balloon.Show();
            control.Focus();

            System.Threading.Thread thread = System.Threading.Thread.CurrentThread; 

            Task task = Task.Delay(2000)
                .ContinueWith(t => HideBallon(thread, Balloon));
        }

        private static Point GetControlPosition(System.Windows.Controls.Control myControl)
        {
            Point locationToScreen = myControl.PointToScreen(new Point(0, 0));
            PresentationSource source = PresentationSource.FromVisual(myControl);
            return source.CompositionTarget.TransformFromDevice.Transform(locationToScreen);
        }


        private static void HideBallon(System.Threading.Thread thread, WarningBalloonWindow Balloon)
        {
            Dispatcher disp = Dispatcher.FromThread(thread);
            disp.Invoke(() => Balloon.Close());
        }
    }
}
