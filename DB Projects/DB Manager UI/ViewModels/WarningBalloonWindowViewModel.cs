﻿using NS_Common.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DBManagerBase.ViewModels
{
    public class WarningBalloonWindowViewModel : ViewModelBase, DBManagerBase.ViewModels.IWarningBalloonWindowViewModel
    {
        #region Public Properties

        private String m_WarningMessage;
        public String WarningMessage
        {
            get { return m_WarningMessage; }
            set { m_WarningMessage = value; RaisePropertyChanged("WarningMessage"); }
        }

        private String m_MessageTitle;
        public String WarningTitle
        {
            get { return m_MessageTitle; }
            set
            {
                m_MessageTitle = value;
                RaisePropertyChanged("WarningTitle");
            }
        }
        #endregion

        public WarningBalloonWindowViewModel(string title, string message)
        {
            this.WarningTitle = title;
            this.WarningMessage = message;
        }
    }
}
