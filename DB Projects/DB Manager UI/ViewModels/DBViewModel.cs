﻿using NS_Common.ViewModels.Common;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NS_Common;
using System.Collections.ObjectModel;
using System.Collections;
using DBManagerBase.ViewModels.Parameters_Pages;
using MVVM_Dialogs.Interfaces;

namespace DBManagerBase.ViewModels
{
    public class DBViewModel : ViewModelBase, IBaseDBViewModel
    {
        public RelayCommand<Object> DoSearch { get; private set; }
        public RelayCommand<Object> SetSearchMode { get; private set; }
        public RelayCommand<Object> SetNewRecordMode { get; private set; }
        public RelayCommand<Object> DeleteRecord { get; private set; }
        public RelayCommand<Object> SaveRecord { get; private set; }


        public RelayCommand<Object> GridMouseDoublClicked { get; private set; }

        #region Private Members

        protected DBManagerComponentsBuilder m_Builder;
        protected IUnityContainer m_Container;
        protected IManageFilesDialogsViewModel manageRoutinesFilesViewModel;

        #endregion

        #region Public Properties

            protected System.Windows.Controls.Page m_ActiveContentPage;
            public System.Windows.Controls.Page ActiveContentPage
            {
                get { return m_ActiveContentPage; }
                set
                {
                    m_ActiveContentPage = value;
                    RaisePropertyChanged("ActiveContentPage");
                }
            }

            protected String m_WindowCaption;
            public string WindowCaption
            {
                get{return m_WindowCaption;}
                set
                {
                    if (m_WindowCaption != value)
                    {
                        m_WindowCaption = value;
                        RaisePropertyChanged("WindowCaption");
                    }
                }
            }

            protected FormStateType m_FormStateType;
            public FormStateType FormStateType
            {
                get{return m_FormStateType;}
                set
                {
                    m_FormStateType = value;
                    SetButtonsVisibleStates();
                    if (null != m_ActiveContentPage)
                    {
                        NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                        logic.StateType = m_FormStateType;
                       // ((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).StateType = m_FormStateType;
                        SearchResultsDataSource = null;
                    }
                    RaisePropertyChanged("FormStateType");
                    
                }
            }

            private DataView m_SearchResultsDataSource;
            public DataView SearchResultsDataSource
            {
                get { return m_SearchResultsDataSource; }
                set 
                {
                    m_SearchResultsDataSource = RemoveSlashSignFromColumnsNames(value);
                    RaisePropertyChanged("SearchResultsDataSource");
                }
            } 

            #region Buttons Visible States
                private System.Windows.Visibility m_ButtonSearchModeVisible;
                public System.Windows.Visibility ButtonSearchModeVisible { get { return m_ButtonSearchModeVisible; } set { m_ButtonSearchModeVisible = value; RaisePropertyChanged("ButtonSearchModeVisible"); } }

                private System.Windows.Visibility m_ButtonNewRecordVisible;
                public System.Windows.Visibility ButtonNewRecordVisible { get { return m_ButtonNewRecordVisible; } set { m_ButtonNewRecordVisible = value; RaisePropertyChanged("ButtonNewRecordVisible"); } }

                private System.Windows.Visibility m_ButtonDeleteDetailsRecordVisible;
                public System.Windows.Visibility ButtonDeleteDetailsRecordVisible { get { return m_ButtonDeleteDetailsRecordVisible; } set { m_ButtonDeleteDetailsRecordVisible = value; RaisePropertyChanged("ButtonDeleteDetailsRecordVisible"); } }

                private System.Windows.Visibility m_ButtonSaveVisible;
                public System.Windows.Visibility ButtonSaveVisible { get { return m_ButtonSaveVisible; } set { m_ButtonSaveVisible = value; RaisePropertyChanged("ButtonSaveVisible"); } }

                private System.Windows.Visibility m_ButtonResetVisible;
                public System.Windows.Visibility ButtonResetVisible { get { return m_ButtonResetVisible; } set { m_ButtonResetVisible = value; RaisePropertyChanged("ButtonResetVisible"); } }

                private System.Windows.Visibility m_ButtonClearVisible;
                public System.Windows.Visibility ButtonClearVisible { get { return m_ButtonClearVisible; } set { m_ButtonClearVisible = value; RaisePropertyChanged("ButtonClearVisible"); } }

                private System.Windows.Visibility m_ButtonDoSearchVisible;
                public System.Windows.Visibility ButtonDoSearchVisible { get { return m_ButtonDoSearchVisible; } set { m_ButtonDoSearchVisible = value; RaisePropertyChanged("ButtonDoSearchVisible"); } }
            #endregion

        #endregion

        #region Constructors

            public DBViewModel(IUnityContainer  container) //, String target_page_name)
            {

                DoSearch = new RelayCommand<Object>(p => this.Search(p), p => true);
                DeleteRecord = new RelayCommand<Object>(p => this.DeleteSelectedRecord(p), p => true);
                SaveRecord = new RelayCommand<Object>(p => this.SaveSelectedRecord(p), p => true);

                SetSearchMode = new RelayCommand<Object>(p => this.SearchMode(p), p => true);
                SetNewRecordMode = new RelayCommand<Object>(p => this.NewRecordMode(p), p => true);

                GridMouseDoublClicked = new RelayCommand<Object>(p => this.GridDoubleClicked(p), p => true);


                manageRoutinesFilesViewModel = m_Container.Resolve<IManageFilesDialogsViewModel>();

                //m_Builder = (DBManagerComponentsBuilder)container.Resolve<IDBManagerComponentsBuilder>();

                //ActiveContentPage = (System.Windows.Controls.Page)m_Builder.GuiFactory.CreateItem(target_page_name);

            }



        #endregion

        #region Private Functions

            private DataView RemoveSlashSignFromColumnsNames(DataView value)
            {

                DataView res = value;
                if (null != res)
                {
                    foreach (DataColumn column in res.Table.Columns)
                    {
                        if (column.ColumnName.Contains('/'))
                        {
                            column.ColumnName = column.ColumnName.Replace('/', '_');
                        }
                    }
                }
                return res;
            }

            private void GridDoubleClicked(object p)
            {
                System.Data.DataRowView data_row = p as System.Data.DataRowView;
                if (null != data_row)
                {
                    if (!data_row.Row.Table.Columns.Contains("ID")) return;
                    Int32 row_id = Convert.ToInt32(data_row["ID"].ToString());

                    NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                    logic.LoadData(row_id);
                    //((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).LoadData(row_id);
                    FormStateType = NS_Common.FormStateType.Edit;  
                }
            }

            private void Search(object parameter)
            {
                FormStateType = NS_Common.FormStateType.Search;
                NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;

                logic.ClearLastSearchResults();
                SearchResultsDataSource = logic.GetSearchResults();

                

            }

            private void SearchMode(object p)
            {
                FormStateType = NS_Common.FormStateType.Search;
                NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                logic.InitValues(); 
                //((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).InitValues(); 
            }

            private void NewRecordMode(object p)
            {
                FormStateType = NS_Common.FormStateType.New;
                NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                logic.InitValues(); 
                //((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).InitValues();
            }


            private void DeleteSelectedRecord(object p)
            {
                NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                logic.DeleteData();
                //((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).DeleteData();
                NewRecordMode(p);
            }

            private Boolean SaveSelectedRecord(object p)
            {
                NS_BLL.Base_Classes.BaseLogicClass logic = (m_ActiveContentPage.DataContext as BaseParametersPageViewModel).MyLogic;
                //NS_BLL.Base_Classes.BaseLogicClass activeLogic = ((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext) as NS_BLL.Base_Classes.BaseLogicClass ;
                try
                {
                    switch (m_FormStateType)
                    {
                        case FormStateType.Edit:
                            {
                                logic.UpdateData();
                                if (logic.LogChangesClass != null)
                                {
                                    logic.LogChangesClass.InsertData(); //Enable if u wish to save log for each forms
                                }
                                manageRoutinesFilesViewModel.DisplayMessageBox(null, "Record has been updated", "Record Info", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                                //TopHeaderText = activeLogic.Name + " :: Edit";
                                break;
                            }
                        case FormStateType.New:
                            {
                                //FillLogChangesLogic(NFW2_BLL.Base_Classes.ActionTypesSingletone.GetInstance().InsertActionID);
                                logic.InsertData();
                                if (logic.LogChangesClass != null)
                                {
                                    logic.LogChangesClass.Affected_Record_ID = ((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).ID;
                                    logic.LogChangesClass.Affected_Record_Description = ((NS_BLL.Base_Classes.BaseLogicClass)m_ActiveContentPage.DataContext).DataDescription();
                                    logic.LogChangesClass.InsertData(); //enable if u wish to save log for each forms
                                }
                                manageRoutinesFilesViewModel.DisplayMessageBox(null, "New record has been created", "Record Info", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                                FormStateType = NS_Common.FormStateType.Edit;
                                break;
                            }
                    }
                    //InitChangesMade(this);
                }
                catch (Exception ex)
                {
                    if (!ParseExceptions(ex))
                    {
                        if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.ForeignKeyConstraintException))
                        {
                            //OnForeignKeyConstraintException(this, null);
                            manageRoutinesFilesViewModel.DisplayMessageBox(null, "Cannot change value that used for identification in other parts of the system!", "Record Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                            return false;
                        }

                        if (ex.Equals(NS_Common.Error_Handling.ExceptionsList.DuplicateRecordException))
                        {
                            //OnAttemptToSaveDuplicateRecordErrorAccured(this, null);
                            return false;
                        }
                        manageRoutinesFilesViewModel.DisplayMessageBox(null, "Error: " + "\n\r" + ex.Message, "Unhandled Error Occurred" , System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        return false;
                    }
                    else
                        return false;
                }
                return true;


            }


            private bool ParseExceptions(Exception ex)
            {
                if (null == m_ActiveContentPage) return false;

                System.Reflection.MethodInfo mi = m_ActiveContentPage.GetType().GetMethod("ParseExceptions");
                if (mi != null)
                {
                    return (bool)mi.Invoke(m_ActiveContentPage, new object[] { ex, m_ActiveContentPage });
                }
                return false;
            }


            private void SetButtonsVisibleStates()
            {
                switch (m_FormStateType)
                {
                    case FormStateType.Edit:
                        {
                            ButtonSearchModeVisible = System.Windows.Visibility.Visible;
                            ButtonNewRecordVisible = System.Windows.Visibility.Visible;
                            ButtonDeleteDetailsRecordVisible = System.Windows.Visibility.Visible;
                            ButtonSaveVisible = System.Windows.Visibility.Visible;
                            ButtonResetVisible = System.Windows.Visibility.Collapsed;
                            ButtonClearVisible = System.Windows.Visibility.Collapsed;
                            ButtonDoSearchVisible = System.Windows.Visibility.Collapsed ;
                            break;
                        }
                    case FormStateType.New:
                        {
                            ButtonSearchModeVisible = System.Windows.Visibility.Visible;
                            ButtonSaveVisible = System.Windows.Visibility.Visible;
                            ButtonClearVisible = System.Windows.Visibility.Visible;
                            ButtonDoSearchVisible = System.Windows.Visibility.Collapsed ;
                            ButtonNewRecordVisible = System.Windows.Visibility.Collapsed  ;
                            ButtonDeleteDetailsRecordVisible = System.Windows.Visibility.Collapsed ;
                            ButtonResetVisible = System.Windows.Visibility.Collapsed ;
                            break;
                        }
                    case FormStateType.Search:
                        {
                            ButtonDoSearchVisible = System.Windows.Visibility.Visible;
                            ButtonNewRecordVisible = System.Windows.Visibility.Visible;
                            ButtonClearVisible = System.Windows.Visibility.Visible;
                            ButtonDeleteDetailsRecordVisible = System.Windows.Visibility.Collapsed ;
                            ButtonSaveVisible = System.Windows.Visibility.Collapsed ;
                            ButtonResetVisible = System.Windows.Visibility.Collapsed ;
                            ButtonSearchModeVisible = System.Windows.Visibility.Collapsed ;
                            //this.btnSend2Excel.Visible = false;
                            break;
                        }
                }
            }

        #endregion 
    }
}
