﻿using NS_Common.ViewModels.Common;
using Components;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Controls;
using System.Windows.Input;

namespace DBManagerBase.ViewModels
{
    public class TestTypeSelectionViewModel : ViewModelBase, DBManagerBase.ViewModels.ITestTypeSelectionViewModel
    {

        public RelayCommand<Object> NavigateToProducts { get; private set; }
        public RelayCommand<Object> NavigateToProjects { get; private set; }
        public RelayCommand<Object> NavigateToC_And_F { get; private set; }


        IUnityContainer m_Container = null;
        DBManagerComponentsBuilder m_Builder;


        private int myVar;
        public int MyProperty
        {
            get { return myVar; }
            set { myVar = value; }
        }


        public TestTypeSelectionViewModel(IUnityContainer container)
        {
            m_Container = container;
            m_Builder = (DBManagerComponentsBuilder)m_Container.Resolve<IDBManagerComponentsBuilder>();


            NavigateToProducts = new RelayCommand<Object>(p => this.GoToProducts(p), p => true);
            NavigateToProjects = new RelayCommand<Object>(p => this.GoToProjects(p), p => true);
            NavigateToC_And_F = new RelayCommand<Object>(p => this.GoToC_And_F(p), p => true);
        }

        private void GoToC_And_F(object param)
        {
            ((System.Windows.Controls.Page)param).NavigationService.Navigate((Page)m_Builder.GuiFactory.CreateItem("C_AND_F_ParametersPage"));
        }

        private void GoToProjects(object param)
        {
            ((System.Windows.Controls.Page)param).NavigationService.Navigate ((Page)m_Builder.GuiFactory.CreateItem("ProjectParametersPage"));
        }

        private void GoToProducts(object param)
        {
            ((System.Windows.Controls.Page)param).NavigationService.Navigate((Page)m_Builder.GuiFactory.CreateItem("ProductParametersPage"));
        }
        
    }


   
}
