﻿using NS_Common;
using NS_Common.ViewModels.Common;
using DBManagerBase.Views;
using DBManagerBase.Views.Parameters_Pages.Project;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using System.Windows.Controls;
using System.Windows.Navigation;
using NS_Common.Extension_Methods;

namespace DBManagerBase.ViewModels
{
    public class ProjectParametersViewModel : ViewModelBase, DBManagerBase.ViewModels.IProjectParametersViewModel
    {

        private IUnityContainer m_Container;

        public RelayCommand<Object> ProjectPageUnloaded { get; private set; }
        public RelayCommand<Object> ManageProjectsDatabase { get; private set; }
        


        #region Public Properties 

        private NS_BLL.IBLL m_BLL = null;
        public NS_BLL.IBLL BLL
        {
            get { return m_BLL; }
            set { m_BLL = value; }
        }


        #region Projects

        private String m_ValueMember = "ID";
        /// <summary>
        /// Gets or sets the path of the property to use as the actual value for the
        /// items in the System.Windows.Forms.ListControl.
        /// </summary>
        public String ValueMember
        {
            get { return m_ValueMember; }
            private set
            {
                m_ValueMember = value;
                RaisePropertyChanged("ValueMember");
            }
        }

        private String m_DisplayMember = "Display Member"; //"Name/Title";
        /// <summary>
        /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
        /// </summary>
        public String DisplayMember
        {
            get { return m_DisplayMember; }
            private set
            {
                m_DisplayMember = value;
                RaisePropertyChanged("DisplayMember");
            }
        }

        private DataTable m_myDataSource;
        /// <summary>
        /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
        /// </summary>
        public DataTable myDataSource
        {
            get { return m_myDataSource; }
            set
            {
                m_myDataSource = value;
                RaisePropertyChanged("myDataSource");
            }
        }

        private int m_SelectedProjectIndex = 0;
        /// <summary>
        /// Gets or sets the index specifying the currently selected item.
        /// </summary>
        /// <Returns>
        /// A zero-based index of the currently selected item.
        /// A value of negative one (-1) is returned if no item is selected.
        /// </Returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     The specified index is less than or equal to -2.-or- The specified index
        ///    is greater than or equal to the number of items in the combo box.
        /// </exception>
        public int SelectedProjectIndex
        {
            get { return m_SelectedProjectIndex; }
            set
            {
                if (m_SelectedProjectIndex != value)
                {
                    m_SelectedProjectIndex = value;
                    RaisePropertyChanged("SelectedProjectIndex");
                }
            }
        }

        private System.Data.DataRowView m_SelectedProjectItem = null;
        public System.Data.DataRowView SelectedProjectItem
        {
            get { return m_SelectedProjectItem; }
            set
            {
                if (m_SelectedProjectItem != value)
                {
                    m_SelectedProjectItem = value;
                    if (null == m_SelectedProjectItem)
                    {
                        m_BLL.SelectedProject = new NS_BLL.Logics.Project.ProjectLogic(1/*Unknown*/);
                    }
                    else
                    {
                        m_BLL.SelectedProject = new NS_BLL.Logics.Project.ProjectLogic(Convert.ToInt32(m_SelectedProjectItem["ID"]));
                    }
                    RaisePropertyChanged("SelectedProjectItem");
                }
            }
        }

        #endregion


        #region Project Managers

        private String m_PrjManValueMember = "ID";
        /// <summary>
        /// Gets or sets the path of the property to use as the actual value for the
        /// items in the System.Windows.Forms.ListControl.
        /// </summary>
        public String PrjManValueMember
        {
            get { return m_PrjManValueMember; }
            private set
            {
                m_PrjManValueMember = value;
                RaisePropertyChanged("PrjManValueMember");
            }
        }

        private String m_PrjManDisplayMember = "Name"; //"Person Name/Title"; //"Name/Title";
        /// <summary>
        /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
        /// </summary>
        public String PrjManDisplayMember
        {
            get { return m_PrjManDisplayMember; }
            private set
            {
                m_PrjManDisplayMember = value;
                RaisePropertyChanged("PrjManDisplayMember");
            }
        }

        private DataTable m_PrjManDataSource;
        /// <summary>
        /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
        /// </summary>
        public DataTable PrjManDataSource
        {
            get { return m_PrjManDataSource; }
            set
            {
                m_PrjManDataSource = value;
                RaisePropertyChanged("PrjManDataSource");
            }
        }

        private int m_SelectedPrjManIndex = 0;
        /// <summary>
        /// Gets or sets the index specifying the currently selected item.
        /// </summary>
        /// <Returns>
        /// A zero-based index of the currently selected item.
        /// A value of negative one (-1) is returned if no item is selected.
        /// </Returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     The specified index is less than or equal to -2.-or- The specified index
        ///    is greater than or equal to the number of items in the combo box.
        /// </exception>
        public int SelectedPrjManIndex
        {
            get { return m_SelectedPrjManIndex; }
            set
            {
                if (m_SelectedPrjManIndex != value)
                {
                    m_SelectedPrjManIndex = value;
                    RaisePropertyChanged("SelectedPrjManIndex");
                }
            }
        }


        private Boolean m_UseProjectManagersFilters;
        /// <summary>
        /// Set/Get Value that indicates if user control will use Project managers filter on Projects list
        /// </summary>
        public Boolean UseProjectManagersFilters
        {
            get { return m_UseProjectManagersFilters; }
            set 
            {
                if (m_UseProjectManagersFilters != value)
                {
                    m_UseProjectManagersFilters = value;
                    LoadProjectsData();
                    RaisePropertyChanged("UseProjectManagersFilters");
                }
            }
        }


        private System.Data.DataRowView m_SelectedPrjManItem = null;
        public System.Data.DataRowView SelectedPrjManItem
        {
            get { return m_SelectedPrjManItem; }
            set
            {
                if (m_SelectedPrjManItem != value)
                {
                    
                    m_SelectedPrjManItem = value;
                    LoadProjectsData();
                    RaisePropertyChanged("SelectedPrjManItem");
                }
            }
        }

        #endregion

        #endregion


        public ProjectParametersViewModel(IUnityContainer container)
        {
            m_Container = container;
 
            m_BLL = container.Resolve<NS_BLL.IBLL>();

            ProjectPageUnloaded = new RelayCommand<Object>(p => this.UnloadedProjectsPage(p), p => true);
            ManageProjectsDatabase = new RelayCommand<Object>(param => this.ManageProducts(param), param => true);


            LoadProjectManagers();
            LoadProjectsData();
        }



        private void ManageProducts(object param)
        {
            if (null != (param as Page))
            {
                Page target = new BaseDBPage(m_Container, "Projects Database Management", FormStateType.Search, new pgSelect_Project_Data(m_Container));

                NavigationService service = (param as Page).GetOwnerNavigationService(2);
                if (null != service) service.Navigate(target);
            }
        }

        private void UnloadedProjectsPage(object param)
        {
            SelectedProjectIndex = 0;
            SelectedPrjManIndex = 0;
        }

        private void LoadProjectManagers()
        {
            NS_BLL.Logics.Project.ProjectManagerLogic lgc = new NS_BLL.Logics.Project.ProjectManagerLogic();
            PrjManDataSource = lgc.GetComboSource();
            if (null!=PrjManDataSource)
                PrjManDataSource.Columns["Person Name/Title"].ColumnName = "Name";
        }

        private void LoadProjectsData()
        {
            NS_BLL.Logics.Project.ProjectLogic lgc = new NS_BLL.Logics.Project.ProjectLogic();


            if (m_UseProjectManagersFilters && null != m_SelectedPrjManItem)
                ((NS_BLL.Logics.Project.ProjectLogic)lgc).Project_ManagerID = Convert.ToInt32(m_SelectedPrjManItem["ID"]);

             
            myDataSource = (((NS_BLL.Logics.Project.ProjectLogic)lgc).GetComboSource("Display Member", "Name/Title", "Nickname"));
            

            //if (cmbxListOfProjects.Items.Count > 1 && cmbxListOfProjects.SelectedIndex < 0)
            //    cmbxListOfProjects.SelectByValueMember(cmbxListOfProjects.Items[1]);
        }
    }
}
