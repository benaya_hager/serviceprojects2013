﻿using NS_Common;
using NS_Common.ViewModels.Common;
using DBManagerBase.Views;
using DBManagerBase.Views.Parameters_Pages;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using System.Windows.Controls;
using System.Windows.Navigation;

namespace DBManagerBase.ViewModels
{
    public class ucWin8DBFieldSelectorViewModel : ViewModelBase, DBManagerBase.ViewModels.IWin8DBFieldSelectorViewModel
    {
        public RelayCommand<Object> ManageSTPsDatabase { get; private set; }
        public RelayCommand<Object> ManageProtocolsDatabase { get; private set; }
           
        #region Private Members

            DBManagerComponentsBuilder m_Builder;
            private IUnityContainer m_Container; 
        #endregion

        #region Public Properties
        
            private System.Windows.Controls.Page m_ActiveContentPage;
            public System.Windows.Controls.Page ActiveContentPage
            {
                get { return m_ActiveContentPage; }
                set
                {
                    m_ActiveContentPage = value;
                    RaisePropertyChanged("ActiveContentPage");
                }
            }

            #region STP Members

                private String m_STPValueMember = "ID";
                /// <summary>
                /// Gets or sets the path of the property to use as the actual value for the
                /// items in the System.Windows.Forms.ListControl.
                /// </summary>
                public String STPValueMember
                {
                    get { return m_STPValueMember; }
                    private set
                    {
                        m_STPValueMember = value;
                        RaisePropertyChanged("ValueMember");
                    }
                }

                private String m_STPDisplayMember = "Title"; //"Name/Title";
                /// <summary>
                /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
                /// </summary>
                public String STPDisplayMember
                {
                    get { return m_STPDisplayMember; }
                    private set
                    {
                        m_STPDisplayMember = value;
                        RaisePropertyChanged("DisplayMember");
                    }
                }

                private DataTable m_STPDataSource;
                /// <summary>
                /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
                /// </summary>
                public DataTable STPDataSource
                {
                    get { return m_STPDataSource; }
                    set
                    {
                        m_STPDataSource = value;
                        RaisePropertyChanged("STPDataSource");
                    }
                }

                private int m_STPSelectedIndex = 0;
                /// <summary>
                /// Gets or sets the index specifying the currently selected item.
                /// </summary>
                /// <Returns>
                /// A zero-based index of the currently selected item.
                /// A value of negative one (-1) is returned if no item is selected.
                /// </Returns>
                /// <exception cref="System.ArgumentOutOfRangeException">
                ///     The specified index is less than or equal to -2.-or- The specified index
                ///    is greater than or equal to the number of items in the combo box.
                /// </exception>
                public int STPSelectedIndex
                {
                    get { return m_STPSelectedIndex; }
                    set
                    {
                        if (m_STPSelectedIndex != value)
                        {
                            m_STPSelectedIndex = value;
                            RaisePropertyChanged("STPSelectedIndex");
                        }
                    }
                }

            #endregion

            #region Protocol Members

                private String m_ProtocolValueMember = "ID";
                /// <summary>
                /// Gets or sets the path of the property to use as the actual value for the
                /// items in the System.Windows.Forms.ListControl.
                /// </summary>
                public String ProtocolValueMember
                {
                    get { return m_ProtocolValueMember; }
                    private set
                    {
                        m_ProtocolValueMember = value;
                        RaisePropertyChanged("ValueMember");
                    }
                }

                private String m_ProtocolDisplayMember = "Protocol Name"; //"Name/Title";
                /// <summary>
                /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
                /// </summary>
                public String ProtocolDisplayMember
                {
                    get { return m_ProtocolDisplayMember; }
                    private set
                    {
                        m_ProtocolDisplayMember = value;
                        RaisePropertyChanged("DisplayMember");
                    }
                }

                private DataTable m_ProtocolDataSource;
                /// <summary>
                /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
                /// </summary>
                public DataTable ProtocolDataSource
                {
                    get { return m_ProtocolDataSource; }
                    set
                    {
                        m_ProtocolDataSource = value;
                        RaisePropertyChanged("ProtocolDataSource");
                    }
                }

                private int m_ProtocolSelectedIndex = 0;
                /// <summary>
                /// Gets or sets the index specifying the currently selected item.
                /// </summary>
                /// <Returns>
                /// A zero-based index of the currently selected item.
                /// A value of negative one (-1) is returned if no item is selected.
                /// </Returns>
                /// <exception cref="System.ArgumentOutOfRangeException">
                ///     The specified index is less than or equal to -2.-or- The specified index
                ///    is greater than or equal to the number of items in the combo box.
                /// </exception>
                public int ProtocolSelectedIndex
                {
                    get { return m_ProtocolSelectedIndex; }
                    set
                    {
                        if (m_ProtocolSelectedIndex != value)
                        {
                            m_ProtocolSelectedIndex = value;
                            RaisePropertyChanged("ProtocolSelectedIndex");
                        }
                    }
                }
            
            #endregion

        #endregion 

        #region Constructors

            public ucWin8DBFieldSelectorViewModel(IUnityContainer  container)
            {
                m_Container = container;

                m_Builder = (DBManagerComponentsBuilder)container.Resolve<IDBManagerComponentsBuilder>();

                ManageSTPsDatabase = new RelayCommand<Object>(p => this.STPsDatabaseManagement(p), p => true);
                ManageProtocolsDatabase = new RelayCommand<Object>(p => this.ProtocolssDatabaseManagement(p), p => true);

                LoadSTP_Data();
                LoadProtocolsData();

                ActiveContentPage = (Page)m_Builder.GuiFactory.CreateItem("TestTypeSelectionPage");

            }

        #endregion

        #region Private Methods

            private void STPsDatabaseManagement(object param)
            {
                Page target = new BaseDBPage(m_Builder.Container, "STP Database Management", FormStateType.Search, new  pgSelect_STP_Data(m_Container ));
               
                ((System.Windows.Controls.Page)param).NavigationService.Navigate(target);
            }

            private void ProtocolssDatabaseManagement(object param)
            {
                Page target = new BaseDBPage(m_Builder.Container, "Protocols Database Management", FormStateType.Search, new pgSelect_Protocol_Data(m_Container));

                ((System.Windows.Controls.Page)param).NavigationService.Navigate(target);
            }

            private void LoadSTP_Data()
            {
                NS_BLL.Logics.STP_Logic lgc = new NS_BLL.Logics.STP_Logic();
                STPDataSource = ((NS_BLL.Logics.STP_Logic)lgc).GetComboSource();
                //cmbListOfSTPs.FillAndSetValue(myTbl, "ID", "Title", true, 1);
                //if (cmbListOfSTPs.Items.Count > 0 && cmbListOfSTPs.SelectedIndex < 0)
                //    cmbListOfSTPs.SelectByValueMember(cmbListOfSTPs.Items[0]);
            }

            private void LoadProtocolsData()
            {
                NS_BLL.Logics.ProtocolLogic lgc = new NS_BLL.Logics.ProtocolLogic();
                ProtocolDataSource = ((NS_BLL.Logics.ProtocolLogic)lgc).GetComboSource();
                //cmbxListOfProtocols.FillAndSetValue(myTbl, "ID", "Protocol Name", true, 1);
                //if (cmbxListOfProtocols.Items.Count > 0 && cmbxListOfProtocols.SelectedIndex < 0)
                //    cmbxListOfProtocols.SelectByValueMember(cmbxListOfProtocols.Items[1]);
            }

        #endregion
    }
}
