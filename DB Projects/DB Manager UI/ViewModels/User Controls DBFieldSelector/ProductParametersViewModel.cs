﻿using NS_Common;
using NS_Common.ViewModels.Common;
using DBManagerBase.Views;
using DBManagerBase.Views.Parameters_Pages.Product;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using NS_Common.Extension_Methods;


namespace DBManagerBase.ViewModels
{
    public class ProductParametersViewModel : ViewModelBase, DBManagerBase.ViewModels.IProductParametersViewModel
    {
        #region Private Members
        private IUnityContainer m_Container;
        #endregion

        public RelayCommand<Object> ProductPageUnloaded { get; private set; }
        public RelayCommand<Object> ManageProductsDatabase { get; private set; }
        public RelayCommand<Object> ManageBrandsDatabase { get; private set; }

        #region Public Properties


        private NS_BLL.IBLL m_BLL = null;
        public NS_BLL.IBLL BLL
        {
            get { return m_BLL; }
            set { m_BLL = value; }
        }

        private String m_ValueMember = "ID";
        /// <summary>
        /// Gets or sets the path of the property to use as the actual value for the
        /// items in the System.Windows.Forms.ListControl.
        /// </summary>
        public String ValueMember
        {
            get { return m_ValueMember; }
            private set
            {
                m_ValueMember = value;
                RaisePropertyChanged("ValueMember");
            }
        }

        private String m_DisplayMember = "Display Member"; //"Name/Title";
        /// <summary>
        /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
        /// </summary>
        public String DisplayMember
        {
            get { return m_DisplayMember; }
            private set
            {
                m_DisplayMember = value;
                RaisePropertyChanged("DisplayMember");
            }
        }

        private DataTable m_myDataSource;
        /// <summary>
        /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
        /// </summary>
        public DataTable myDataSource
        {
            get { return m_myDataSource; }
            set
            {
                m_myDataSource = value;
                RaisePropertyChanged("myDataSource");
            }
        }

        private int m_SelectedProductIndex = 0;
        /// <summary>
        /// Gets or sets the index specifying the currently selected item.
        /// </summary>
        /// <Returns>
        /// A zero-based index of the currently selected item.
        /// A value of negative one (-1) is returned if no item is selected.
        /// </Returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     The specified index is less than or equal to -2.-or- The specified index
        ///    is greater than or equal to the number of items in the combo box.
        /// </exception>
        public int SelectedProductIndex
        {
            get { return m_SelectedProductIndex; }
            set
            {
                m_SelectedProductIndex = value;
                RaisePropertyChanged("SelectedProductIndex");
            }
        }


        private System.Data.DataRowView m_SelectedProductItem = null;
        public System.Data.DataRowView SelectedProductItem
        {
            get { return m_SelectedProductItem; }
            set
            {
                if (m_SelectedProductItem != value)
                {   
                    m_SelectedProductItem = value;
                    if (null == m_SelectedProductItem)
                    {
                        m_BLL.SelectedProduct = new NS_BLL.Logics.Product.ProductLogic(1/*Unknown*/);
                    }
                    else
                    {
                        m_BLL.SelectedProduct = new NS_BLL.Logics.Product.ProductLogic(Convert.ToInt32(m_SelectedProductItem["ID"]));
                    }
                    RaisePropertyChanged("SelectedProductItem");
                }
            }
        }

        #region Brands

        private int m_SelectedBrndIndex = 0;
        /// <summary>
        /// Gets or sets the index specifying the currently selected item.
        /// </summary>
        /// <Returns>
        /// A zero-based index of the currently selected item.
        /// A value of negative one (-1) is returned if no item is selected.
        /// </Returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     The specified index is less than or equal to -2.-or- The specified index
        ///    is greater than or equal to the number of items in the combo box.
        /// </exception>
        public int SelectedBrndIndex
        {
            get { return m_SelectedBrndIndex; }
            set
            {
                if (m_SelectedBrndIndex != value)
                {
                    m_SelectedBrndIndex = value;
                    RaisePropertyChanged("SelectedBrndIndex");
                }
            }
        }
        
        private String m_BrndValueMember = "Class/Brand ID";
        /// <summary>
        /// Gets or sets the path of the property to use as the actual value for the
        /// items in the System.Windows.Forms.ListControl.
        /// </summary>
        public String BrndValueMember
        {
            get { return m_BrndValueMember; }
            private set
            {
                m_BrndValueMember = value;
                RaisePropertyChanged("BrndValueMember");
            }
        }

        private String m_BrndDisplayMember = "Brand Name"; //"Name/Title";
        /// <summary>
        /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
        /// </summary>
        public String BrndDisplayMember
        {
            get { return m_BrndDisplayMember; }
            private set
            {
                m_BrndDisplayMember = value;
                RaisePropertyChanged("BrndDisplayMember");
            }
        }

        private DataTable m_BrndDataSource;
        /// <summary>
        /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
        /// </summary>
        public DataTable BrndDataSource
        {
            get { return m_BrndDataSource; }
            set
            {
                m_BrndDataSource = value;
                RaisePropertyChanged("BrndDataSource");
            }
        }

        private Boolean m_UseBrandsFilters = true;
        /// <summary>
        /// Set/Get Value that indicates if user control will use Brands filter on Products list
        /// </summary>
        public Boolean UseBrandsFilters 
        {
            get { return m_UseBrandsFilters; }
            set 
            {
                if (m_UseBrandsFilters != value)
                {
                    m_UseBrandsFilters = value;
                    LoadProductsData(); 
                    RaisePropertyChanged("UseBrandsFilters");
                }
                
            }
        }

        private System.Data.DataRowView m_SelectedBrandItem = null;
        public System.Data.DataRowView SelectedBrandItem
        {
            get { return m_SelectedBrandItem; }
            set
            {
                if (m_SelectedBrandItem != value)
                {
                    m_SelectedBrandItem = value;
                    LoadProductsData(); 
                    RaisePropertyChanged("SelectedBrandItem");
                }
            }
        }

        #endregion

        #endregion

        #region Constructors

        public ProductParametersViewModel(IUnityContainer container)
        {
            m_Container = container;
            m_BLL = m_Container.Resolve<NS_BLL.IBLL>();

            ProductPageUnloaded = new RelayCommand<Object>(param => this.UnloadedProductsPage(param), param => true);
            ManageProductsDatabase = new RelayCommand<Object>(param => this.ManageProducts(param), param => true);
            ManageBrandsDatabase = new RelayCommand<Object>(param => this.ManageBrands(param), param => true);

            LoadProductsData();

            LoadBrands();
        }

       

        #endregion

        private void ManageProducts(object param)
        {
            if (null != (param as Page))
            {
                Page target = new BaseDBPage(m_Container, "Products Database Management", FormStateType.Search, new pgSelect_Product_Data(m_Container ));

                NavigationService service = (param as Page).GetOwnerNavigationService(2);
                if (null != service) service.Navigate(target);
            }
        }

        private void ManageBrands(object param)
        {
            if (null != (param as Page))
            {
                Page target = new BaseDBPage(m_Container, "Brands Database Management", FormStateType.Search, new pgSelect_Brand_Data(m_Container));

                NavigationService service = (param as Page).GetOwnerNavigationService(2);
                if (null != service) service.Navigate(target);
            }
        }

        private void UnloadedProductsPage(object param)
        {
            SelectedProductIndex =0;
            SelectedBrndIndex = 0;
            //SelectedProductItem = null;
        }
 

        private void LoadProductsData()
        {
            NS_BLL.Logics.Product.ProductLogic lgc = new NS_BLL.Logics.Product.ProductLogic();

            if (m_UseBrandsFilters && null != m_SelectedBrandItem)
                ((NS_BLL.Logics.Product.ProductLogic)lgc).BrandID = Convert.ToInt32(m_SelectedBrandItem["ID"]);
  
            myDataSource = lgc.GetComboSource("Display Member", "Catalog Number", "Size/Type");

        }

        private void LoadBrands()
        {

            if (null != m_BLL.MachineType2Brands)
            {
                m_BLL.MachineType2Brands.ResetAvailibleBrandsList();
                BrndDataSource = m_BLL.MachineType2Brands.AvailibleBrandsList;
                //cmbxBrandsFilter.FillAndSetValue(m_BLL.MachineType2Brands.AvailibleBrandsList, "Class/Brand ID", "Brand Name", true, 1);
            }

            //if (cmbxBrandsFilter.Items.Count > 0 && cmbxBrandsFilter.SelectedIndex < 0)
            //    cmbxBrandsFilter.SelectByValueMember(cmbxBrandsFilter.Items[1]);
        }
    }
}
