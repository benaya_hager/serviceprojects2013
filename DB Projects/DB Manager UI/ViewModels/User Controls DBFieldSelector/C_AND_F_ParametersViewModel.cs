﻿using NS_Common.ViewModels.Common;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SFW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace DBManagerBase.ViewModels
{
    public class C_AND_F_ParametersViewModel : ViewModelBase, DBManagerBase.ViewModels.IC_AND_F_ParametersViewModel
    {
        public RelayCommand<Object> C_And_F_PageUnloaded { get; private set; }

        #region Private Members

        private NS_BLL.IBLL m_BLL = null;
        public NS_BLL.IBLL BLL
        {
            get { return m_BLL; }
            set { m_BLL = value; }
        }

        private String m_ValueMember = "ID";
        /// <summary>
        /// Gets or sets the path of the property to use as the actual value for the
        /// items in the System.Windows.Forms.ListControl.
        /// </summary>
        public String ValueMember
        {
            get { return m_ValueMember; }
            private set 
            { 
                m_ValueMember = value;
                RaisePropertyChanged("ValueMember");
            }
        }

        private String m_DisplayMember = "Name"; //"Name/Title";
        /// <summary>
        /// Gets or sets the property to display for this System.Windows.Forms.ListControl.
        /// </summary>
        public String DisplayMember
        {
            get { return m_DisplayMember; }
            private set 
            { 
                m_DisplayMember = value;
                RaisePropertyChanged("DisplayMember");
            }
        }

        private DataTable m_myDataSource;
        /// <summary>
        /// Gets or sets the data source for this System.Windows.Forms.ComboBox.
        /// </summary>
        public DataTable myDataSource
        {
            get { return m_myDataSource; }
            set
            {
                m_myDataSource = value ;
                RaisePropertyChanged("myDataSource");
            }
        }

        private int m_SelectedIndex = 0;
        /// <summary>
        /// Gets or sets the index specifying the currently selected item.
        /// </summary>
        /// <Returns>
        /// A zero-based index of the currently selected item.
        /// A value of negative one (-1) is returned if no item is selected.
        /// </Returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     The specified index is less than or equal to -2.-or- The specified index
        ///    is greater than or equal to the number of items in the combo box.
        /// </exception>
        public int SelectedIndex
        {
            get { return m_SelectedIndex; }
            set 
            {
                if (m_SelectedIndex != value)
                {
                    m_SelectedIndex = value;
                    RaisePropertyChanged("SelectedIndex");
                }
            }
        }



        private System.Data.DataRowView m_Selected_C_And_F_Item = null;
        public System.Data.DataRowView Selected_C_And_F_Item
        {
            get { return m_Selected_C_And_F_Item; }
            set
            {
                if (m_Selected_C_And_F_Item != value)
                {
                    m_Selected_C_And_F_Item = value;
                    if (null == m_Selected_C_And_F_Item)
                    {
                        m_BLL.Selected_C_And_F = new NS_BLL.Logics.C_AND_F_Logic(1/*Unknown*/);
                    }
                    else
                    {
                        m_BLL.Selected_C_And_F = new NS_BLL.Logics.C_AND_F_Logic(Convert.ToInt32(m_Selected_C_And_F_Item["ID"]));
                    }
                }
            }
        }
        #endregion


        public C_AND_F_ParametersViewModel(IUnityContainer container)
        {
            m_BLL = container.Resolve<NS_BLL.IBLL>();

            C_And_F_PageUnloaded = new RelayCommand<Object>(param => this.Unloaded_C_And_F_sPage(param), param => true);

            Load_C_And_F_Data();
            
        }

        private void Unloaded_C_And_F_sPage(object param)
        {
            SelectedIndex = 0;
        }

        //private void sample()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("Name");
        //    dt.Columns.Add("Age");
        //    dt.Columns.Add("City");

        //    DataRow dr = dt.NewRow();
        //    dr["Name"] = "Jacob";
        //    dr["Age"] = 25;
        //    dr["City"] = "France";
        //    dt.Rows.Add(dr);

        //    DataRow dr1 = dt.NewRow();
        //    dr1["Name"] = "Julia Martin";
        //    dr1["Age"] = 26;
        //    dr1["City"] = "France";
        //    dt.Rows.Add(dr1);

        //    DataRow dr2 = dt.NewRow();
        //    dr2["Name"] = "Brandon";
        //    dr2["Age"] = 24;
        //    dr2["City"] = "London";
        //    dt.Rows.Add(dr2);

        //    myDataSource = dt;
        //    DisplayMember  = dt.Columns[0].ToString();
        //}

        private void Load_C_And_F_Data()
        {
            NS_BLL.Logics.C_AND_F_Logic lgc = new NS_BLL.Logics.C_AND_F_Logic();
            myDataSource = ((NS_BLL.Logics.C_AND_F_Logic)lgc).GetComboSource();
            if (null != myDataSource)
                myDataSource.Columns["Name/Title"].ColumnName = "Name";
            //DisplayMember = myDataSource.Columns[0].ToString();

            //cmbxListOf_C_And_F_Tests.FillAndSetValue(myTbl, "ID", "Name/Title", true, 1);
            //if (cmbxListOf_C_And_F_Tests.Items.Count > 0 && cmbxListOf_C_And_F_Tests.SelectedIndex < 0)
            //    cmbxListOf_C_And_F_Tests.SelectByValueMember(cmbxListOf_C_And_F_Tests.Items[1]);
        }


       
    }
}
