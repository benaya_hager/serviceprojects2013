﻿using NS_Common.ViewModels.Common;
using DBManagerBase.Interfaces.Parameters_Pages.Project;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NS_BLL.Logics.Project;
using System.Windows.Controls;
using DBManagerBase.Views;
using System.Windows.Navigation;
using NS_Common.Extension_Methods;
using NS_Common;
using DBManagerBase.Views.Parameters_Pages.Projects;
using NS_Common.Service;
using Tools.String_Enum;
using System.Collections;

namespace DBManagerBase.ViewModels.Parameters_Pages.Project
{
    public class Select_Project_Data_ViewModel : BaseParametersPageViewModel, ISelect_Project_Data_ViewModel
    {

        public RelayCommand<Object> ManageTestTypesDatabase { get; private set; }
        public RelayCommand<Object> ManageProjectManagersDatabase { get; private set; }
        
        #region  Private Members
            private IUnityContainer m_Container;
        #endregion

        #region Public Properties

            private NS_BLL.IBLL m_BLL = null;
            public NS_BLL.IBLL BLL
            {
                get { return m_BLL; }
                set { m_BLL = value; }
            }

            private DataTable m_ProjectStatesList;
            public DataTable ProjectStatesList
            {
                get { return m_ProjectStatesList; }
                set { m_ProjectStatesList = value; RaisePropertyChanged("ProjectStatesList"); }
            }

            private DataTable m_ProjectManagerList;
            public DataTable ProjectManagerList
            {
                get { return m_ProjectManagerList; }
                set
                { 
                    m_ProjectManagerList = value; 
                    if (null != m_ProjectManagerList)
                        m_ProjectManagerList.Columns["Person Name/Title"].ColumnName = "Person Name";

                    RaisePropertyChanged("ProjectManagerList");  
                }
            }
            

            //private var  m_InternalLabStatusItemsList;
            //public _Internal_Lab_Status InternalLabStatusItemsList
            //{
            //    get 
            //    {
            //        return m_InternalLabStatusItemsList; 
            //    }

            //    set
            //    {
            //        var stringEnum = new StringEnum(typeof(Common.Service._Internal_Lab_Status));
            //        var tmpList = new ArrayList();
            //        foreach (DictionaryEntry str in stringEnum.GetListValues())
            //        {
            //            tmpList.Add(str);
            //        }

            //        //= value; RaisePropertyChanged("InternalLabStatusItemsList"); 
            //    }
            //}
            
        #endregion

        #region Constructors

            public Select_Project_Data_ViewModel(IUnityContainer container, NS_BLL.Logics.Project.ProjectLogic logic)
            {
                m_Container = container;
                m_BLL = container.Resolve<NS_BLL.IBLL>();

                ProjectStatesList = new ProjectStatesLogic().GetComboSource();
                ProjectManagerList = new ProjectManagerLogic().GetComboSource(); 

                m_MyLogic = logic;

                ManageTestTypesDatabase = new RelayCommand<Object>(param => this.ManageTestTypes(param), param => true);
                ManageProjectManagersDatabase = new RelayCommand<Object>(param => this.ManageProjectManagers(param), param => true);
            }

            

        #endregion

        private void ManageTestTypes(object param)
        {
            if (null != (param as Page))
            {
                Page target = new BaseDBPage(m_Container, "Project States Database Management", FormStateType.Search, new pgSelect_ProjectState_Data(m_Container));

                NavigationService service = (param as Page).GetOwnerNavigationService(2);
                if (null != service) service.Navigate(target);
            }
        }

        private void ManageProjectManagers(object param)
        {
            if (null != (param as Page))
            {
                Page target = new BaseDBPage(m_Container, "Project Managers Database Management", FormStateType.Search, new pgSelect_ProjectManager_Data(m_Container));

                NavigationService service = (param as Page).GetOwnerNavigationService(2);
                if (null != service) service.Navigate(target);
            }
        } 

           
    }
}
