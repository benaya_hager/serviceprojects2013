﻿using NS_Common.ViewModels.Common;
using DBManagerBase.Interfaces.Parameters_Pages.Project;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DBManagerBase.ViewModels.Parameters_Pages.Project
{
    public class Select_ProjectState_Data_ViewModel : BaseParametersPageViewModel, ISelect_ProjectState_Data_ViewModel
    {
        #region Private Members
            private IUnityContainer m_Container;
        #endregion

        #region Public Properties

            private NS_BLL.IBLL m_BLL = null;
            public NS_BLL.IBLL BLL
            {
                get { return m_BLL; }
                set { m_BLL = value; }
            }

        #endregion

        #region Constructors

            public Select_ProjectState_Data_ViewModel(IUnityContainer container, NS_BLL.Logics.Project.ProjectStatesLogic  logic)
            {
                m_Container = container;
                m_BLL = container.Resolve<NS_BLL.IBLL>();
                m_MyLogic = logic;
            }

        #endregion
    }
}
