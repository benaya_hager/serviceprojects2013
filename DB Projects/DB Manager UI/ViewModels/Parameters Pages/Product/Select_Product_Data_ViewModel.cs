﻿using NS_Common.ViewModels.Common;
using DBManagerBase.Interfaces.Parameters_Pages.Product;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBManagerBase.ViewModels.Parameters_Pages.Product
{
    public class Select_Product_Data_ViewModel : BaseParametersPageViewModel, ISelect_Product_Data_ViewModel
    {
        #region  Private Members
            private IUnityContainer m_Container;
        #endregion

        #region Public Properties

            private NS_BLL.IBLL m_BLL = null;
            public NS_BLL.IBLL BLL
            {
                get { return m_BLL; }
                set { m_BLL = value; }
            }

        #endregion

        #region Constructors

            public Select_Product_Data_ViewModel(IUnityContainer container, NS_BLL.Logics.Product.ProductLogic  logic)
            {
                m_Container = container;
                m_BLL = container.Resolve<NS_BLL.IBLL>();
                m_MyLogic = logic;
            }

        #endregion


           
    }
}
