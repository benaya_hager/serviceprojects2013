﻿using NS_Common.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBManagerBase.ViewModels.Parameters_Pages
{
    public class BaseParametersPageViewModel : ViewModelBase
    {

        protected NS_BLL.Base_Classes.BaseLogicClass m_MyLogic;
        public NS_BLL.Base_Classes.BaseLogicClass MyLogic
        {
            get { return m_MyLogic; }
            protected set { m_MyLogic = value; }
        }
    }
}
