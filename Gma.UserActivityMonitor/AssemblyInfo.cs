using System.Reflection;

// Information about this assembly is defined by the following
// attributes.
//
// change them to the information which is associated with the assembly
// you compile.

[assembly: AssemblyTitle("Medinol.UserActivity")]
[assembly: AssemblyDescription("This class library contains components which monitor all mouse and keyboard activities globally (also outside of the application) and provides appropriate events.")]
[assembly: AssemblyCompany("Medinol")]
[assembly: AssemblyProduct("Medinol.UserActivity")]
[assembly: AssemblyCopyright("Copyright � Medinol 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has following format :
//
// Major.Minor.Build.Revision
//
// You can specify all values by your own or you can build default build and revision
// numbers with the '*' character (the default):

[assembly: AssemblyVersion("3.0.*")]

// The following attributes specify the key for the sign of your assembly. See the
// .NET Framework documentation for more information about signing.
// This is not required, if you don't want signing let these attributes like they're.
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyFileVersionAttribute("3.0.0.0")]
