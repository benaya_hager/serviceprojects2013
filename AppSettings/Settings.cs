﻿using NS_Common.ApplicationExtensions;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text; 
using System.Xml;
using AppSettings.Service; 
using log4net;
using Tools.CustomRegistry;
using System.Windows;
using System.Windows.Shapes;
using NS_Common.Service;
using NS_Common;
using MVVM_Dialogs.Interfaces;
using Microsoft.Practices.Unity;
using System.Reflection;
using MVVM_Dialogs; 

namespace AppSettings
{
    /// <summary>
    /// Top-level class.
    /// Provides a way to save application settings to XML file format.
    /// </summary>
    /// <remarks>
    /// Used to replace the standard Windows INI file format.
    /// Sections are named categories to hold collections of settings.
    /// Settings are name-value pairs able to store many different data types.
    /// </remarks>
    /// <example>
    /// Create an instance of the XMLSettings class.
    /// <code>
    /// MySettings = new XMLSettings("MySettings.xml")
    /// MySettings = new XMLSettings("C:\\MySettings[.xml]")</code>
    /// Load the file.
    /// <code>
    /// bool loaded = MySettings.Load([File]);</code>
    /// Return a setting passing a default value.
    /// If the value exists then the existing setting is returned.
    /// <code>
    /// BackColor = MySettings.GetVal("FormSettings","BackColor",BackColor);</code>
    /// Set a value passing a section, name and value.
    /// Value and section will be created if needed.
    /// <code>
    /// MySettings.SetVal("FormSettings","Left",Left);</code>
    /// Return the same value with 2 different notations.
    /// <code>
    /// ret = MySettings.GetVal("FormSettings","Left");
    /// ret = MySettings.Sections["FormSettings"]["Left"];</code>
    /// Add a collection of Settings.
    /// <code>
    /// IniSettings SubSettings1 = new IniSettings(MySettings);
    /// SubSettings1.Add("S1","SubSettings1 test");
    /// SubSettings1.Add("S2",1234);
    /// SubSettings1.Add("S3",true);
    /// MySettings.SetVal("Coll","SubSettings1",SubSettings1);</code>
    /// Access a sub-setting in that section.
    /// <code>
    /// Bret = MySettings.Sections["Coll"]["SubSettings1"]["S3"];</code>
    /// Use as a Data source.
    /// <code>
    /// list1.DisplayMember = "Name";
    ///	list1.DataSource = MySettings.Sections["Coll"]["SubSettings1"].ToDataSource();</code>
    /// </example>
    public class Settings
    {
        #region protected  members & definitions

        //protected KeysConverter m_KeyConv = new KeysConverter();

        protected DateTime m_FileDate;

        /// <summary>
        /// The short descriptor of file.
        /// Used to create registry sub key to identify current type of settings
        /// </summary>
        protected String m_DataShortDescriptor = "";

        protected ILog m_AppSettingsLog;

        protected Boolean m_bInitRegistyInProgress = false;
         
        protected IUnityContainer m_Container;
        protected IManageFilesDialogsViewModel manageSettingsFilesViewModel;

        #endregion protected  members & definitions

        #region Public Properties

        protected String m_ConfigFileName = "Default";
        /// <summary>
        /// The name of the configuration file (with or without path)"
        /// </summary>
        [Category("Settings"), Description("The name of the configuration file (without path)")]
        public String ConfigFileName
        {
            get { return m_ConfigFileName; }
            set
            {
                if (m_ConfigFileName != value)
                {
                    m_ConfigFileName = value;
                    if (System.IO.Path.HasExtension(m_ConfigFileName))
                    {
                        if (m_ConfigFileName.EndsWith(ConfigFilePattern))
                        {
                            m_ConfigFileName = m_ConfigFileName.Substring(0, m_ConfigFileName.Length - ConfigFilePattern.Length - 1);
                        }
                    }
                    //UpdateRegistyVariables();
                }
            }
        }

        protected String m_ConfigFilePattern = GlobalDef.DEFAULT_TARGETS_FILES_PATTERN;
        /// <summary>
        /// The pattern name of the configuration file
        /// </summary>
        [Category("Settings"), Description("The pattern name of the configuration file")]
        public String ConfigFilePattern
        {
            get { return m_ConfigFilePattern; }
            set 
            {
                if (m_ConfigFilePattern != value)
                {
                    m_ConfigFilePattern = value;
                    //UpdateRegistyVariables();
                }
            }
        }

        protected String m_ConfigFilesPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Application.Current.ProductName());
        /// <summary>
        /// Sets or returns the folder where file will be stored in.
        /// </summary>
        /// <remarks>Once set, you can just provide the file name without the path.
        /// The value can be overridden by passing a fully qualified path and file.</remarks>
        /// <exception cref="System.ApplicationException">Thrown when the path is not valid.</exception>
        [Category("Settings"), Description("The folder where file will be stored in.")]
        public String ConfigFilesPath
        {
            get { return m_ConfigFilesPath; }
            set
            {
                if (value != m_ConfigFilesPath)
                {
                    if (value == "")
                        m_ConfigFilesPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Application.Current.ProductName());
                    else
                    {
                        m_ConfigFilesPath = value;
                        if (Directory.Exists(m_ConfigFilesPath))
                        {
                            if (!m_ConfigFilesPath.EndsWith("\\"))
                            {
                                m_ConfigFilesPath += "\\";
                            }
                        }
                        else
                        {
                            try
                            {
                                Directory.CreateDirectory(m_ConfigFilesPath);
                            }
                            catch (Exception Ex)
                            {
                                throw new ApplicationException("Failed to create Folder \n\r" + Ex.Message);
                            }
                        }
                        //UpdateRegistyVariables();
                    }
                }
            }
        }
 

        protected String m_LastXmlErrorMessage = "";
        /// <summary>
        /// Returns a user description if there is one.
        /// </summary>
        [Category("Settings"), Description("Returns a last error text description if there is one.")]
        public String LastXmlErrorMessage
        {
            get
            {
                return m_LastXmlErrorMessage;
            }
        }

        protected Int32 m_LastXmlErrorCode = ErrorCodesList.OK;
        /// <summary>
        /// Returns a user description if there is one.
        /// </summary>
        [Category("Settings"), Description("Returns a last error code number")]
        public Int32 LastXmlErrorCode
        {
            get
            {
                return m_LastXmlErrorCode;
            }
        }


        protected String m_XmlComments = "";
        /// <summary>
        /// Returns the comments at the top of the XML
        /// after having read the file.
        /// </summary>
        [Category("Settings"), Description("Returns the comments at the top of the XML after having read the file.")]
        public String XmlComments
        {
            get
            {
                return m_XmlComments;
            }
        }

        private bool m_Dirty;
        /// <summary>
        /// Indicates when the settings have changed and need to saved or re-read.
        /// </summary>
        [Category("Settings"), Description("Indicates when the settings have changed and need to saved or re-read.")]
        public bool Dirty
        {
            get
            {
                return m_Dirty;
            }
            set
            {
                if (m_Dirty != value)
                    m_Dirty = value;
            }
        }

        protected IniSections m_TopLevelSections;//Section names
        /// <summary>
        /// Access to the collection of Sections.
        /// </summary>
        [Category("Settings"), Description("Access to the collection of Sections.")]
        public IniSections Sections
        {
            get
            {
                return m_TopLevelSections;
            }
            set
            {
                m_TopLevelSections = value;
            }
        }

        /// <summary>
        /// Has the data file changed since the last read or write?
        /// </summary>
        [Category("Settings"), Description("Has the data file changed since the last read or write?")]
        public bool FileChanged
        {
            get
            {
                try
                {
                    return m_FileDate != File.GetLastWriteTime(BuildFileName());
                }
                catch { return true; }
            }
        }

        protected Boolean m_PreventDefaultOverride = true;
        /// <summary>
        /// Has the data file changed since the last read or write?
        /// </summary>
        [Category("Settings"), Description("Prevent user to change default setup file")]
        public bool PreventDefaultOverride
        {
            get { return m_PreventDefaultOverride; }
            set { m_PreventDefaultOverride = value; }
        }

        #endregion Public Properties

        #region Constructors & Destructor

        /// <summary>
        /// Constructor with no file specified. Initialize all values to it's default
        /// </summary>
        /// <param name="inDataShortDescriptor">
        /// The short descriptor of file.
        /// Used to create registry sub key to identify current type of settings
        /// </param>
        public Settings(IUnityContainer container, String inDataShortDescriptor)
        {
            m_AppSettingsLog = LogManager.GetLogger("AppSettingsLog");

            m_Container = container; 

            manageSettingsFilesViewModel = m_Container.Resolve<IManageFilesDialogsViewModel>();

            m_DataShortDescriptor = inDataShortDescriptor;
            InitRegistryData();

            try
            {
                m_TopLevelSections = new IniSections(this);//Section names
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage += "AppSettings constructor failed\n" + ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                throw ex;
            }
        }

        /// <summary>
        /// Use as default path the application location path
        /// </summary>
        /// <param name="FileName">The name of the configuration file (without path)</param>
        /// <remarks> Passing only a file name without a path will result
        ///  in the file being saved in the same folder as this DLL.
        /// </remarks>
        /// <param name="FilePattern">The pattern of the configuration file</param>
        public Settings(IUnityContainer container,  String FileName, String FilePattern)
            : this(container, "", System.IO.Path.GetDirectoryName(Application.Current.ExecutableLocationPath()), FileName, FilePattern)
        {
        }


        /// <summary>
        /// Constructor
        /// </summary>
        ///  <param name="inDataShortDescriptor">
        /// The short descriptor of file.
        /// Used to create registry sub key to identify current type of settings</param>
        /// <param name="FilePath">The data file path/parameter>
        /// <remarks> Passing only a file name without a path will result
        ///  in the file being loaded from same folder as this DLL.
        /// </remarks>
        /// <param name="FileName">The name of the configuration file (without path)</param>
        /// <param name="FilePattern">The pattern of the configuration file</param>
        /// <exception cref="System.ApplicationException">Thrown when the specified
        /// folder does not exist.</exception>
        public Settings(IUnityContainer container,String inDataShortDescriptor, String FilesPath, String FileName, String FilePattern)
            : this(container,inDataShortDescriptor)
        {
            try
            {
                if (FilesPath != "")
                    this.ConfigFilesPath = FilesPath;
                if (FileName != "")
                    this.ConfigFileName = FileName;
                if (FilePattern != "")
                    this.ConfigFilePattern = FilePattern;
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage += "AppSettings(String fileName) constructor failed\n" + ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                throw ex;
            }
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~Settings()
        {
            //cRegistryFunc RegistryAccess = new cRegistryFunc(Application.ProductName,
            //                                                 System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
            //                                                 cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

            //RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Name", this.ConfigFileName);
            //RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Path", this.ConfigFilesPath);
            //RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Pattern", this.ConfigFilePattern);
        }

        #endregion Constructors & Destructor

        #region Public Functions

        public void UpdateRegistyVariables()
        {
            if (!m_bInitRegistyInProgress)
            {
                cRegistryFunc RegistryAccess = new cRegistryFunc(Application.Current.ProductName(),
                                                                 System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                                 cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Name", ConfigFileName);
                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Path", ConfigFilesPath);
                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Pattern", ConfigFilePattern);
            }
        }

        public void ResetLastError()
        {
            m_LastXmlErrorMessage = "";
        }

        #region Remove Items/Sections

        /// <summary>
        /// Removes a Setting with a specific name.
        /// </summary>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <exception cref="System.NullReferenceException">Thrown when the specified
        /// section or setting does not exist.</exception>///
        public void Remove(String sectionName, String settingName)
        {
            try
            {
                //If the value exists then remove it.
                Sections[sectionName].Settings.Remove(settingName);
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage += "Remove(String sectionName, String settingName) failed\n" + ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                throw ex;
            }
        }

        /// <summary>
        /// Removes a Section with a specific name.
        /// </summary>
        /// <param name="sectionName">The name of the section</param>
        /// <exception cref="System.NullReferenceException">Thrown when the specified
        /// section does not exist.</exception>///
        public void Remove(String sectionName)
        {
            try
            {
                //If the section exists then remove it.
                Sections.Remove(sectionName);
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage += "Remove(String sectionName) failed\n" + ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                throw ex;
            }
        }

        #endregion Remove Items/Sections

        #region Get/Set Values

        /// <summary>
        /// Creates a section and setting as needed then assigns a value.
        /// </summary>
        /// <param name="settingName">Setting name</param>
        /// <param name="oValue">Object supporting many data types</param>
        /// <returns>The setting object.</returns>
        public IniSetting SetVal(String settingName, object oValue)
        {
            return this.SetVal(m_DataShortDescriptor, settingName, oValue);
        }

        /// <summary>
        /// Creates a section and setting as needed then assigns a value.
        /// </summary>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <param name="oValue">Object supporting many data types</param>
        /// <returns>The setting object.</returns>
        public IniSetting SetVal(String sectionName, String settingName, object oValue)
        {
            //If the section exists then just update the value.
            IniSetting setting = Sections.Add(sectionName).Settings.Add(settingName);
            if (null != setting)
            {
                if (null == setting.settingValue)
                {
                    setting.Value = oValue;
                }
                if (!setting.Value.Equals(oValue))
                    setting.Value = oValue;
            }
            return setting;
        }

        /// <summary>
        /// Creates a section and setting as needed then assigns a value.
        /// </summary>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <param name="oValue">Object supporting many data types</param>
        /// <param name="description">A description of the setting</param>
        /// <returns>The setting object.</returns>
        public IniSetting SetVal(String sectionName, String settingName, object oValue, String description)
        {
            //If the section exists then update the value.
            IniSetting setting = SetVal(sectionName, settingName, oValue);
            setting.Description = description;
            return setting;
        }

        /// <summary>
        /// Gets the IniSetting object by name.
        /// </summary>
        /// <param name="settingName">Setting name</param>
        /// <returns>The setting object if succeeded : Otherwise null.</returns>
        public IniSetting GetVal(String settingName)
        {
            return this.GetVal(m_DataShortDescriptor, settingName);
        }

        /// <summary>
        /// Gets the IniSetting object by name.
        /// </summary>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <returns>The setting object if succeeded : Otherwise null.</returns>
        public IniSetting GetVal(String sectionName, String settingName, Boolean NoDefault)
        {
            try
            {
                return Sections[sectionName].Settings[settingName];
            }
            catch (Exception Ex)
            {
                m_LastXmlErrorMessage = Ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                return null;
            }
        }

        /// <summary>
        /// Gets the IniSetting object by name.
        /// </summary>
        /// <param name="settingName">Setting name</param>
        /// <param name="oDefault">Default value</param>
        /// <returns>The setting object if succeeded : Otherwise default value.</returns>
        public virtual IniSetting GetVal(String settingName, object oDefault)
        {
            return this.GetVal(m_DataShortDescriptor, settingName, oDefault);
        }

        /// <summary>
        /// Gets the IniSetting object by name passing a default.
        /// </summary>
        /// <remarks>Default value will only be assigned if the setting does not exist.</remarks>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <param name="oDefault">Default value</param>
        /// <returns>The setting object if succeeded: Otherwise default value.</returns>
        public virtual IniSetting GetVal(String sectionName, String settingName, object oDefault)
        {
            try
            {
                IniSection sec = m_TopLevelSections.Add(sectionName);
                IniSetting setting = sec.Settings[settingName];
                //Make a value if needed and assign the default value
                if (setting == null)
                {
                    setting = sec.Settings.Add(settingName);
                    setting.TopObject = this;
                    setting.Value = oDefault;
                }
                return setting;
            }
            catch (Exception Ex)
            {
                m_LastXmlErrorMessage = Ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                return null;
            }
        }

        /// <summary>
        /// Gets the IniSetting object by name passing a default.
        /// </summary>
        /// <remarks>Default value will only be assigned if the setting does not exist.</remarks>
        /// <param name="sectionName">Section name</param>
        /// <param name="settingName">Setting name</param>
        /// <param name="oDefault">Default value</param>
        /// <param name="description">A description of the setting</param>
        /// <returns>The setting object if succeeded: Otherwise default value.</returns>
        public IniSetting GetVal(String sectionName, String settingName, object oDefault, String description)
        {
            try
            {
                IniSection sec = m_TopLevelSections.Add(sectionName);
                IniSetting setting = sec.Settings[settingName];
                //Make a value if needed and assign the default value
                if (setting == null)
                {
                    setting = sec.Settings.Add(settingName);
                    setting.TopObject = this;
                    setting.Value = oDefault;
                }
                setting.Description = description;
                return setting;
            }
            catch (Exception Ex)
            {
                m_LastXmlErrorMessage = Ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                return null;
            }
        }

        #endregion Get/Set Values

        private Int32 GetPermitions()
        {
            if (System.Threading.Thread.CurrentPrincipal.GetType() == typeof(System.Security.Principal.GenericPrincipal))
                return 8;

            Int32 val = 1;
            if (null != System.Threading.Thread.CurrentPrincipal)
            {
                PropertyInfo pi = System.Threading.Thread.CurrentPrincipal.GetType().GetProperty("UserPermissionsLevel");
                if (null != pi)
                {
                    val = (Int32)pi.GetValue(System.Threading.Thread.CurrentPrincipal, null);
                }
            }
            return val;
        }

        #region Save file functions

        /// <summary>
        /// Save the settings to the specified path and file.
        /// </summary>
        /// <remarks>Using the file name specified during construction.</remarks>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16 Save()
        {
            Int16 res = ErrorCodesList.FAILED; 
            try
            {
                m_LastXmlErrorMessage = "";
                m_LastXmlErrorCode = ErrorCodesList.OK;

                if (ConfigFileName.ToLower() == "default" && m_PreventDefaultOverride)
                {
                    Boolean DontHavePermissions = true;
                    
                    if ((GetPermitions() & 8) != 0)
                    {
                        DontHavePermissions = false;
                    }
                    
                    if (DontHavePermissions)
                    {
                        m_LastXmlErrorCode = ErrorCodesList.FAILED_TO_SAVE_SETTINGS_LOW_PERRMITIONS;
                        m_LastXmlErrorMessage = "You don't have right Permissions to override Default setup!!!! \n\r " +
                                        "Please use ''Save As'' option. Or contact your system administrator to get right permissions.";

                        if (m_AppSettingsLog.IsErrorEnabled)
                            m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                        return  ErrorCodesList.FAILED;
                    }
                }
                WriteXMLSettings(BuildFileName());
                m_Dirty = false;
                m_FileDate = File.GetLastWriteTime(BuildFileName());
                res = ErrorCodesList.OK; 
            }
            catch (Exception ex)
            {
                m_LastXmlErrorCode = ErrorCodesList.FAILED;
                m_LastXmlErrorMessage = ex.Message;

                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                res = ErrorCodesList.FAILED;  
            }
            return res;
        }

        /// <summary>
        /// Saves the settings to specified file. Pass only a file name without a path.
        /// The file being saved to same folder as this DLL.
        /// Using default file extension.
        /// </summary>
        /// <param name="FileName">The File Name. Without extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16 Save(String FileName)
        {
            return this.Save(FileName, this.ConfigFilePattern);
        }

        /// <summary>
        /// Saves the settings from the specified file. Pass only a file name without a path.
        /// The file being saved to the same folder as this DLL.
        /// </summary>
        /// <param name="FileName">The File Name. Without extension</param>
        /// <param name="FilePattern">File Extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16 Save(String FileName, String FilePattern)
        {
            return this.Save(System.IO.Path.GetDirectoryName(Application.Current.ExecutableLocationPath()), FileName, FilePattern);
        }

        /// <summary>
        /// Save the settings to specified file. Pass only a file name without a path.
        /// The file being loaded from the same folder as this DLL.
        /// </summary>
        /// <param name="FilesPath">The full file path</param>
        /// <param name="FileName">File Name to load</param>
        /// <param name="FilePattern">File extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16 Save(String FilesPath, String FileName, String FilePattern)
        {
            String OldPattern = this.ConfigFilePattern,
                   OldFileName = this.ConfigFileName,
                   OldPath = this.ConfigFilesPath;

            try
            {
                this.ConfigFilesPath = FilesPath;
                this.ConfigFileName = FileName;
                this.ConfigFilePattern = FilePattern;
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage = ex.Message;
                this.ConfigFilePattern = OldPattern;
                this.ConfigFileName = OldFileName;
                this.ConfigFilesPath = OldPath;

                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);

                return ErrorCodesList.FAILED;
            }

            UpdateRegistyVariables();

            try
            {
                return this.Save();
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage = ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                return ErrorCodesList.FAILED;
            }
        }

        /// <summary>
        /// Shows "Save As" data file dialog
        /// </summary>
        /// <returns>TRUE if no error; FALSE if error or Cancel pressed</returns>
        public Int16  SaveFileDialog()
        {
            m_LastXmlErrorCode = ErrorCodesList.OK;
            m_LastXmlErrorMessage = "";
             

            String selected_file_name;
            int res = manageSettingsFilesViewModel.SaveXmlFile(out selected_file_name, System.IO.Path.GetFileName(ConfigFileName), m_ConfigFilesPath, ConfigFilePattern, "Save Settings File AS");

            if (res == ErrorCodesList.OK)
            {
                if (System.IO.Path.HasExtension(selected_file_name))
                {
                    if (selected_file_name.EndsWith(ConfigFilePattern))
                        selected_file_name = selected_file_name.Substring(0, selected_file_name.Length - ConfigFilePattern.Length - 1);
                }
                ConfigFilesPath = System.IO.Path.GetDirectoryName(selected_file_name);
                ConfigFileName = System.IO.Path.GetFileName(selected_file_name);
                return this.Save();
            }
            else
            {
                m_LastXmlErrorCode = ErrorCodesList.CANCELED;
                m_LastXmlErrorMessage = "";
                return ErrorCodesList.CANCELED;
            }  
        }

        #endregion Save file functions

        #region Load file functions

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <remarks>Using the file name specified during construction.</remarks>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16  Load()
        {
            Int16 res = ErrorCodesList.OK; 
            m_LastXmlErrorMessage = "";
            m_LastXmlErrorCode = ErrorCodesList.OK;
            try
            {
                res = ReadXMLSettings(BuildFileName());
                if (res == ErrorCodesList.OK )
                {
                    this.Dirty = false;
                    m_FileDate = File.GetLastWriteTime(BuildFileName());
                }
                UpdateRegistyVariables();
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage = ex.Message;
                m_LastXmlErrorCode = ErrorCodesList.FAILED;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                res = ErrorCodesList.FAILED;
            }
            return res;
        }

        /// <summary>
        /// Loads the settings from the specified file. Pass only a file name without a path.
        /// The file being loaded from the same folder as this DLL.
        /// Using default file extension.
        /// </summary>
        /// <param name="FileName">The File Name. Without extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16  Load(String FileName)
        {
            return this.Load(FileName, this.ConfigFilePattern);
              
        }

        /// <summary>
        /// Loads the settings from the specified file. Pass only a file name without a path.
        /// The file being loaded from the same folder as this DLL.
        /// </summary>
        /// <param name="FileName">The File Name. Without extension</param>
        /// <param name="FilePattern">File Extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16  Load(String file_name, String file_pattern)
        {
            return this.Load(m_ConfigFilesPath, file_name, file_pattern);
        }

        /// <summary>
        /// Loads the settings from the specified file. Pass only a file name without a path.
        /// The file being loaded from the same folder as this DLL.
        /// </summary>
        /// <param name="FilesPath">The full file path</param>
        /// <param name="FileName">File Name to load</param>
        /// <param name="FilePattern">File extension</param>
        /// <returns>TRUE if no error; FALSE if error</returns>
        public Int16 Load(String FilesPath, String FileName, String FilePattern)
        {
            String OldPattern = this.ConfigFilePattern,
                   OldFileName = this.ConfigFileName,
                   OldPath = this.ConfigFilesPath;
            Int16 res = ErrorCodesList.OK;
 
            try
            {
                this.ConfigFilesPath = FilesPath;
                this.ConfigFileName = FileName;
                this.ConfigFilePattern = FilePattern;
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage = ex.Message;
                this.ConfigFilePattern = OldPattern;
                this.ConfigFileName = OldFileName;
                this.ConfigFilesPath = OldPath;

                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                throw;
            }

            UpdateRegistyVariables();

            try
            {
                res = this.Load();
            }
            catch (Exception ex)
            {
                m_LastXmlErrorMessage = ex.Message;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                res = ErrorCodesList.FAILED;  
            }
            return res;
        }

        /// <summary>
        /// Opens the data file with user selection dialog
        /// </summary>
        /// <returns>TRUE if no error; FALSE if error, or cancel pressed</returns>
        public Int16  LoadFileDialog()
        {


            String selected_file_name;
            int res = manageSettingsFilesViewModel.LoadXmlFile(out selected_file_name, ConfigFilesPath , ConfigFilePattern, "Open Routine File");
            if (res == ErrorCodesList.OK)
            {
                ConfigFilesPath = System.IO.Path.GetDirectoryName(selected_file_name);
                ConfigFileName = System.IO.Path.GetFileName(selected_file_name);
               //ConfigFileName = selected_file_name;
                //Open file data
                return this.Load();
            }
            return ErrorCodesList.CANCELED; 
           
        }

        #endregion Load file functions

        #endregion Public Functions

        #region protected  Functions

        /// <summary>
        /// Writes all sections and settings to the xml file.
        /// </summary>
        protected void WriteXMLSettings(String FileName)
        {
            m_LastXmlErrorMessage = "";
            XmlTextWriter xWriter = new XmlTextWriter(FileName, null);
            try
            {
                xWriter.Formatting = Formatting.Indented;
                xWriter.WriteStartDocument(true);
                //Style sheet to make it look like an INI file
                //String sXsl = "type='text/xsl' href='INI.xsl'";
                //xWriter.WriteProcessingInstruction("xml-stylesheet", sXsl);
                //Get started writing
                xWriter.WriteComment("MesuareData Programming www.Tactile-World.com");
                xWriter.WriteComment("XML Version=2.0");
                //The sections
                xWriter.WriteStartElement(Tags.SECTIONS);
                foreach (IniSection TopLevelSection in Sections)
                {
                    xWriter.WriteStartElement(Tags.SECTION);
                    xWriter.WriteAttributeString(Tags.NAME, TopLevelSection.Name);
                    WriteXMLSection(TopLevelSection.Settings, ref xWriter);//Pass the settings down
                    xWriter.WriteEndElement();//End of the section
                }
                xWriter.WriteEndElement();
            }
            catch (Exception e)
            {
                m_LastXmlErrorMessage += "Cannot write settings. " + e.Message + "\n";
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                throw;
            }
            finally
            {
                //Write the XML to file and close
                if (xWriter != null)
                {
                    xWriter.Close();
                }
            }
        }

        /// <summary>Writes each sections settings.</summary>
        protected virtual void WriteXMLSection(IniSettings currSettings, ref XmlTextWriter xWriter)
        {
            foreach (IniSetting iniSetting in currSettings)
            {
                if (iniSetting == null) continue;
                xWriter.WriteStartElement(Tags.SETTING);
                xWriter.WriteAttributeString(Tags.NAME, iniSetting.Name);
                xWriter.WriteAttributeString(Tags.TYPE, iniSetting.TypeName);
                //Make sure the tag and description are not null and have a value.
                if (iniSetting.Tag != null)
                {
                    if (iniSetting.Tag.Length > 0)
                        xWriter.WriteAttributeString(Tags.TAG, iniSetting.Tag);
                }

                if (iniSetting.Description != null)
                {
                    if (iniSetting.Description.Length > 0)
                        xWriter.WriteAttributeString(Tags.DESCRIPTION, iniSetting.Description);
                }

                switch (iniSetting.TypeName)
                {
                    //If this value is a collection of settings then call this function again.
                    case Types.INI_SETTINGS:
                        WriteXMLSection(iniSetting, ref xWriter);
                        break;

                    //If this value is a section then call this function again.
                    case Types.INI_SECTION:
                        xWriter.WriteStartElement(Tags.SECTION);
                        xWriter.WriteAttributeString(Tags.NAME, ((IniSection)iniSetting).Name);
                        WriteXMLSection(((IniSection)iniSetting).Settings, ref xWriter);//Pass the settings down
                        xWriter.WriteEndElement();//End of the section
                        break;
                    case Types.ARRAYLIST:
                        WriteArrayListItems(iniSetting, ref xWriter);
                        break;
                    case Types.SIZE:
                        xWriter.WriteElementString(Tags.VALUE, SizeToString(iniSetting));
                        break;
                    case Types.POINT:
                        xWriter.WriteElementString(Tags.VALUE, PointToString(iniSetting));
                        break;
                    case Types.DATETIME:
                        xWriter.WriteElementString(Tags.VALUE, ((DateTime)iniSetting).ToString(DateTimeFormatInfo.InvariantInfo));
                        break;
                    case Types.DECIMAL:
                        xWriter.WriteElementString(Tags.VALUE, ((decimal)iniSetting).ToString(NumberFormatInfo.InvariantInfo));
                        break;
                    case Types.DOUBLE:
                        xWriter.WriteElementString(Tags.VALUE, ((double)iniSetting).ToString(NumberFormatInfo.InvariantInfo));
                        break;
                    case Types.SINGLE:
                        xWriter.WriteElementString(Tags.VALUE, ((Single)iniSetting).ToString(NumberFormatInfo.InvariantInfo));
                        break;
                    case Types.PAIR:
                        xWriter.WriteElementString(Tags.VALUE, (iniSetting.ToString()).ToString());
                        break;

                    //case Types.LIST_VIEW_ITEMS_COLLECTION:
                    //    WriteListViewItems(iniSetting, ref xWriter);
                    //    break;
                    default:
                        if (iniSetting != null)
                            xWriter.WriteElementString(Tags.VALUE, iniSetting);
                        break;
                }
                xWriter.WriteEndElement();//End of the settings
            }
        }

        //protected  void WriteListViewItems(System.Windows.Forms.ListView.ListViewItemCollection itemsList, ref XmlTextWriter xWriter)
        //{
        //    foreach (ListViewItem Item in itemsList)
        //    {
        //        System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(Item.GetType());
        //        x.Serialize(xWriter, Item);
        //        //throw new NotImplementedException();
        //    }
        //}

        /// <summary>Writes array list to xml.</summary>
        protected void WriteArrayListItems(ArrayList arrLst, ref XmlTextWriter xWriter)
        {
            ArrayList arr = arrLst;
            int ct = arr.Count;
            if (ct == 0) return;

            xWriter.WriteStartElement(Tags.ARRAYLIST);
            if (arr[0].GetType().Name == "String")
            {
                xWriter.WriteAttributeString(Tags.TYPE, arr[0].GetType().Name);

                for (int r = 0; r <= ct - 1; r++)
                {
                    xWriter.WriteElementString(Tags.ITEM, arr[r].ToString());
                }
            }
            else
            {
                Type tmptype = arr[0].GetType();
                if (tmptype == typeof(Int32) || (tmptype == typeof(Int16)) ||
                    tmptype == typeof(Single) || tmptype == typeof(Double) ||
                    tmptype == typeof(Decimal))
                {
                    xWriter.WriteAttributeString(Tags.TYPE, arr[0].GetType().Name);
                    for (int r = 0; r <= ct - 1; r++)
                    {
                        xWriter.WriteElementString(Tags.ITEM, Convert.ToString(arr[r], NumberFormatInfo.InvariantInfo));
                    }
                    //xWriter.WriteEndElement();
                }
                else
                    WriteArrayListItems(arrLst, arr[0].GetType(), xWriter);
            }
            xWriter.WriteEndElement();
        }

        protected void WriteArrayListItems(ArrayList arrLst, Type type, XmlTextWriter xWriter)
        {
            System.Reflection.MemberInfo[] myMemberInfo = type.GetProperties();
            ArrayList arr = arrLst;
            int ct = arr.Count;

            xWriter.WriteAttributeString(Tags.TYPE, type.FullName + "," + type.Assembly.GetName());
            for (int r = 0; r <= ct - 1; r++)
            {
                xWriter.WriteStartElement(type.ToString());

                for (int i = 0; i < myMemberInfo.Length; i++)
                {
                    Object result = type.InvokeMember(myMemberInfo[i].Name.ToString(), System.Reflection.BindingFlags.GetProperty, null, arr[r], new object[] { });

                    xWriter.WriteStartElement(myMemberInfo[i].Name.ToString());
                    xWriter.WriteAttributeString(Tags.TYPE, result.GetType().ToString());
                    xWriter.WriteElementString(myMemberInfo[i].Name.ToString(), result.ToString());
                    xWriter.WriteEndElement();
                }

                xWriter.WriteEndElement();
            }
        }


        /// <summary>Reads data into objects from xml.</summary>
        protected Int16 ReadXMLSettings(String pathAndFileName)
        { 
            Int16 res = ErrorCodesList.OK;
            if (!File.Exists(pathAndFileName))
            {
                m_LastXmlErrorMessage = "File " + pathAndFileName + " Not found";
                m_LastXmlErrorCode = ErrorCodesList.FILE_NOT_FOUND;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                return  ErrorCodesList.FAILED ;
            }

            XmlTextReader xReader = new XmlTextReader(pathAndFileName);
            m_LastXmlErrorMessage = "";
            m_LastXmlErrorCode = ErrorCodesList.OK;
            m_XmlComments = "";
            try
            {
                //Load the the document.
                xReader.WhitespaceHandling = WhitespaceHandling.None;
                xReader.NameTable.Add(Tags.SECTION);
                xReader.NameTable.Add(Tags.SECTIONS);
                xReader.NameTable.Add(Tags.SETTING);
                xReader.NameTable.Add(Tags.SETTINGS);
                xReader.NameTable.Add(Tags.NAME);
                xReader.NameTable.Add(Tags.TYPE);
                xReader.NameTable.Add(Tags.VALUE);
                xReader.NameTable.Add(Tags.ARRAYLIST);
                xReader.NameTable.Add(Tags.ITEM);
                xReader.NameTable.Add(Tags.SETTINGS);
                while (xReader.Read())
                {
                    if (xReader.NodeType == XmlNodeType.Comment)
                    {
                        //We might check the version here
                        m_XmlComments += xReader.Value + "\n";
                    }

                    if (xReader.NodeType == XmlNodeType.Element)
                    {
                        if (xReader.Name == Tags.SECTIONS)
                        {
                            //This is the top section. Sections will follow
                            ReadEachSectionInSectons(null, ref xReader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                m_LastXmlErrorMessage += "Cannot read settings\n. " + e.Message + "\n" + "Line " + xReader.LineNumber + "\n";
                m_LastXmlErrorCode = ErrorCodesList.FAILED;
                if (m_AppSettingsLog.IsErrorEnabled)
                    m_AppSettingsLog.Error(m_LastXmlErrorMessage);
                res = ErrorCodesList.FAILED;
            }
            finally
            {
                //Close
                if (xReader != null)
                    xReader.Close();
            }
            return res;
        }

        /// <summary>
        /// Creates each section object from xml.
        ///</summary>
        protected void ReadEachSectionInSectons(IniSetting parentSetting, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            nodeDepth = xReader.Depth;
            //We must be at the top sections node
            while (xReader.Read())
            {
                if (nodeDepth == xReader.Depth) return;
                if (xReader.NodeType == XmlNodeType.Element && xReader.Name == Tags.SECTION)
                {
                    String SecName = xReader.GetAttribute(Tags.NAME);//"Name"
                    if (SecName.Length == 0) return;//Get out. No name.
                    IniSection thisSec;
                    if (parentSetting == null)//No parent. Must be a top section
                    {
                        thisSec = Sections.Add(SecName);
                    }
                    else
                    {
                        //Create a section
                        thisSec = Sections.Add(SecName);
                        parentSetting.Value = thisSec;
                    }
                    //If this is an empty node we need to not read any more
                    if (!xReader.IsEmptyElement)
                    {
                        ReadEachSettingInSection(thisSec, ref xReader);//Each section
                    }
                }
            }
        }

        /// <summary>
        /// Creates each sub-setting object from xml.
        ///</summary>
        protected void ReadEachSettingInSubSetting(IniSetting parentSetting, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            //Create a collection of sub-settings
            IniSettings subSettings = new IniSettings(this);
            //Assign it to the parentSetting
            parentSetting.Value = subSettings;

            //We must be at the beginning of a section of settings
            nodeDepth = xReader.Depth;
            while (xReader.Read())
            {
                //If after reading the settings we have hit the end then get out.
                if (nodeDepth == xReader.Depth) return;
                if (xReader.NodeType == XmlNodeType.Element && xReader.Name == Tags.SETTING)
                {
                    ReadSettingAndGetType(subSettings, ref xReader);
                }
            }
        }

        /// <summary>
        /// Reads all the array list items
        ///</summary>
        protected void ReadEachArrayListItem(IniSetting parentSetting, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            String subType;
            ArrayList alst = new ArrayList();
            //Move to ArrayList node
            while (xReader.Read())
            {
                if (xReader.NodeType == XmlNodeType.Element && xReader.Name == Tags.ARRAYLIST)
                {
                    //We must be at the beginning of a section of settings
                    nodeDepth = xReader.Depth;
                    subType = xReader.GetAttribute(Tags.TYPE);
                    xReader.Read();//Move into the items
                    do
                    {
                        if (xReader.NodeType == XmlNodeType.Element)
                        {
                            if (xReader.Name == Tags.ITEM)
                            {
                                alst.Add(ConvertFromXmlToType(xReader.ReadElementString(), subType));
                            }
                            else
                            {
                                try
                                {
                                    if (null != subType)
                                    {
                                        Type tmpType = Type.GetType(subType);
                                        System.Reflection.Assembly SampleAssembly = System.Reflection.Assembly.GetAssembly(tmpType);
                                        System.Reflection.MemberInfo[] myMemberInfo = tmpType.GetProperties();

                                        Object result = Activator.CreateInstance(tmpType);
                                        for (int i = 0; i < myMemberInfo.Length; i++)
                                        {
                                            xReader.Read();//Move into the items

                                            String ItemType = xReader.GetAttribute(Tags.TYPE);
                                            String tmp = xReader.ReadElementString();

                                            tmpType.InvokeMember(myMemberInfo[i].Name.ToString(),
                                                                                    System.Reflection.BindingFlags.SetProperty,
                                                                                    null, result, new object[] { Convert.ChangeType(tmp, Type.GetType(ItemType)) });
                                        }
                                        alst.Add(result);
                                        while (xReader.NodeType == XmlNodeType.EndElement && nodeDepth != xReader.Depth)
                                            xReader.Read();
                                    }
                                }
                                catch //(Exception Ex)
                                {
                                    while (xReader.NodeType == XmlNodeType.EndElement && nodeDepth != xReader.Depth)
                                        xReader.Read();
                                }
                            }
                        }
                    }
                    while (nodeDepth != xReader.Depth);
                }
                parentSetting.Value = alst;
                return;
            }
        }

        /// <summary>
        /// Creates each sub-setting object that has a setting parent.
        ///</summary>
        protected void ReadEachSubSettingInSettings(IniSettings parentSettings, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            //We must be at the beginning of a section of settings
            nodeDepth = xReader.Depth;
            while (xReader.Read())
            {
                if (nodeDepth == xReader.Depth) return;
                if (xReader.NodeType == XmlNodeType.Element && xReader.Name == Tags.SETTING)
                {
                    ReadSettingAndGetType(parentSettings, ref xReader);//In this section
                }
            }
        }

        /// <summary>
        /// Creates each setting object from xml.
        ///</summary>
        protected void ReadEachSettingInSection(IniSection curSection, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            //We must be at the beginning of a section of settings
            nodeDepth = xReader.Depth;
            while (xReader.Read())
            {
                if (nodeDepth == xReader.Depth) return;
                if (xReader.NodeType == XmlNodeType.Element && xReader.Name == Tags.SETTING)
                {
                    ReadSettingAndGetType(curSection.Settings, ref xReader);//In this section
                }
            }
        }

        /// <summary>
        /// Reads all the possible data in a setting.
        /// This is the smallest unit of data.
        ///</summary>
        protected void ReadAllSettingData(IniSetting curSetting, String sType, ref XmlTextReader xReader)
        {
            int nodeDepth = 0;
            //We must be at the beginning of a section of settings
            nodeDepth = xReader.Depth;
            while (xReader.Read())
            {
                if (xReader.NodeType == XmlNodeType.Element)
                {
                    if (xReader.Name == Tags.VALUE)
                    {
                        curSetting.Value = ConvertFromXmlToType(xReader.ReadElementString(), sType);
                        if (xReader.NodeType != XmlNodeType.Element) xReader.MoveToContent();
                        if (nodeDepth == xReader.Depth) return;
                    }
                    if (xReader.Name == Tags.TAG)
                    {
                        curSetting.Tag = xReader.ReadElementString();
                        if (xReader.NodeType != XmlNodeType.Element) xReader.MoveToContent();
                        if (nodeDepth == xReader.Depth) return;
                    }
                    if (xReader.Name == Tags.DESCRIPTION)
                    {
                        curSetting.Description = xReader.ReadElementString();
                        if (xReader.NodeType != XmlNodeType.Element) xReader.MoveToContent();
                        if (nodeDepth == xReader.Depth) return;
                    }
                }
            }
        }

        /// <summary>
        /// Create setting and value from xml.
        /// Cuold be a section or more settings.
        /// </summary>
        protected void ReadSettingAndGetType(IniSettings collectionOfSettings, ref XmlTextReader xReader)
        {
            String settingName = "";
            String sType;
            IniSetting iniSetting = null;

            //Add a	new	item
            settingName = xReader.GetAttribute(Tags.NAME);
            sType = xReader.GetAttribute(Tags.TYPE);
            iniSetting = collectionOfSettings.Add(settingName);

            switch (sType)
            {
                //If this value is a collection of settings then call this function again
                //after moving to the node.
                case Types.INI_SETTINGS:
                    ReadEachSettingInSubSetting(iniSetting, ref xReader);
                    break;

                //If this value is a collection of settings then call this function again.
                case Types.INI_SECTION:
                    //Create a section
                    ReadEachSectionInSectons(iniSetting, ref xReader);
                    break;

                case Types.ARRAYLIST:
                    ReadEachArrayListItem(iniSetting, ref xReader);
                    break;

                default:
                    ReadAllSettingData(iniSetting, sType, ref xReader);
                    break;
            }

            //			if(iniSetting!=null)
            //			{
            //				collectionOfSettings.Remove(settingName);
            //			}
        }


        /// <summary>
        /// Convert from an XML String to the proper type of object
        /// </summary>
        protected object ConvertFromXmlToType(String valueFromXML, String typeFromXML)
        {
            switch (typeFromXML)
            {
                case Types.BOOLEAN:
                    return Convert.ToBoolean(valueFromXML);
                case Types.BYTE:
                    return Convert.ToByte(valueFromXML);
                case Types.CHAR:
                    return Convert.ToChar(valueFromXML);

                //Localized settings
                case Types.DATETIME:
                    return Convert.ToDateTime(valueFromXML, DateTimeFormatInfo.InvariantInfo);
                case Types.DECIMAL:
                    return Convert.ToDecimal(valueFromXML, NumberFormatInfo.InvariantInfo);
                case Types.DOUBLE:
                    return Convert.ToDouble(valueFromXML, NumberFormatInfo.InvariantInfo);
                case Types.SINGLE:
                    return Convert.ToSingle(valueFromXML, NumberFormatInfo.InvariantInfo);
                //case Types.KEYS:
                //    return (Keys)m_KeyConv.ConvertFromString(valueFromXML);
                case Types.INT16:
                    return Convert.ToInt16(valueFromXML);
                case Types.INT32:
                    return Convert.ToInt32(valueFromXML);
                case Types.INT64:
                    return Convert.ToInt64(valueFromXML);
                case Types.UINT16:
                    return Convert.ToUInt16(valueFromXML);
                case Types.UINT32:
                    return Convert.ToUInt32(valueFromXML);
                case Types.UINT64:
                    return Convert.ToUInt64(valueFromXML); 
                case Types.SIZE:
                    return StringToSize(valueFromXML);
                case Types.POINT:
                    return StringToPoint(valueFromXML);  
                default://Default to the String value
                    return valueFromXML;
            }
        }

        /// <summary>
        /// Get base data from registry
        /// </summary>
        protected int InitRegistryData()
        {
            try
            {
                m_bInitRegistyInProgress = true;
                //Initialize application registry data
                cRegistryFunc RegistryAccess = new cRegistryFunc(Application.Current.ProductName(),
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);


                this.ConfigFileName = RegistryAccess.GetRegKeyValue(m_DataShortDescriptor, "Data File Name", this.ConfigFileName);
                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Name", this.ConfigFileName);

                this.ConfigFilesPath = RegistryAccess.GetRegKeyValue(m_DataShortDescriptor, "Data File Path", this.ConfigFilesPath);
                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Path", this.ConfigFilesPath);

                this.ConfigFilePattern = RegistryAccess.GetRegKeyValue(m_DataShortDescriptor, "Data File Pattern", this.ConfigFilePattern);
                RegistryAccess.SetRegKeyValue(m_DataShortDescriptor, "Data File Pattern", this.ConfigFilePattern);
            }
            catch { }
            finally {m_bInitRegistyInProgress = false;};
            return ErrorCodesList.OK;
        }

        #endregion protected  Functions

        #region Helpers (Misc)
 
        protected String PointToString(Point sz)
        {
            return sz.X + "," + sz.Y;
        }


        protected Point StringToPoint(String st)
        {
            char sep = ',';
            return new Point(Int32.Parse(st.Split(sep)[0]), Int32.Parse(st.Split(sep)[1]));
        }

        protected String SizeToString(Size sz)
        {
            return sz.Width + "," + sz.Height;
        }

        protected Size StringToSize(String st)
        {
            char sep = ',';
            return new Size(Int32.Parse(st.Split(sep)[0]), Int32.Parse(st.Split(sep)[1]));
        }
         
       

        protected String GetDllPath()
        {
            String workingpath = "";
            try
            {
                workingpath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                if (workingpath.IndexOf("ile:\\") >= 0)
                {
                    return workingpath.Substring(6) + "\\";
                }
                return workingpath + "\\";
            }
            catch { throw; }
        }

        protected String EnsurePathIsIncluded(String fileName)
        {
            String ret = "";
            try
            {
                if (fileName.IndexOf("\\") >= 0)//A path was specified
                {
                    if (Directory.Exists(fileName.Substring(0, fileName.LastIndexOf("\\"))))
                    {
                        //Make sure we have an extension.
                        if (!ret.EndsWith(".xml"))
                        {
                            ret += ".xml";
                        }
                        ret = fileName;
                    }
                    else
                    {
                        throw new ApplicationException("Folder does not exist.");
                    }
                }
                else//No path specified.
                {
                    if (m_ConfigFilesPath != "")
                    {
                        ret = m_ConfigFilesPath + fileName;
                    }
                    else
                    {
                        ret = GetDllPath() + fileName;
                    }
                    //Make sure we have an extension.
                    if (!ret.EndsWith(".xml"))
                    {
                        ret += ".xml";
                    }
                }
                return ret;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Combine the file name and pattern
        /// </summary>
        /// <returns></returns>
        protected String BuildFileName()
        {
            //if (File.Exists(ConfigFileName))
            //    return ConfigFileName;
            //else
            return Tools.Misc.UtilsClass.EnsureTrailingSlash(m_ConfigFilesPath) + ConfigFileName;// +"." + ConfigFilePattern;
        }

        #endregion Helpers (Misc)
    }
}