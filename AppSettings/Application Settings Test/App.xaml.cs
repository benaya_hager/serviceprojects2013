﻿using Application_Settings_Test.Component_Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

using System.Windows;
using NS_Common.ApplicationExtensions;
using Microsoft.Practices.Unity;


namespace Application_Settings_Test
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            ComponentsBuilder builder = new ComponentsBuilder();

            builder.Bulid(this.GetContainer());


            AppSettings.Settings ste = new AppSettings.Settings(this.GetContainer(), "Test");

            ste.LoadFileDialog();

            ste.SetVal("Hello", 100);

            ste.SaveFileDialog(); 

            builder.Container.Resolve<MainWindow>().Show();

           


        }
    }
}
