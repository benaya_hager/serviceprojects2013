﻿using Microsoft.Practices.Unity;
using MVVM_Dialogs;
using MVVM_Dialogs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Application_Settings_Test.Component_Builders
{
    public class ComponentsBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public void Bulid(IUnityContainer container)
        {

            Container = container;

            Container.RegisterInstance<IUnityContainer>(Container);

            Container.RegisterType<IManageFilesDialogsViewModel, ManageFilesDialogsViewModel>();


        }

        public IUnityContainer Container { set; get; }
    }
}
