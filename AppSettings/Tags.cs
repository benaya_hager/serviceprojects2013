﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AppSettings
{
    /// <summary>
    /// XML tag names.
    /// </summary>
    public class Tags
    {
        #region String constants

        public const String SECTIONS = "Sections";
        public const String SECTION = "Section";
        public const String SETTINGS = "Settings";
        public const String SETTING = "Setting";
        public const String VALUE = "Value";
        public const String ARRAYLIST = "ArrayList";
        public const String ITEM = "Item";
        public const String NAME = "Name";
        public const String TYPE = "Type";
        public const String TAG = "Tag";
        public const String DESCRIPTION = "Desc";

        #endregion String constants
    }

}
