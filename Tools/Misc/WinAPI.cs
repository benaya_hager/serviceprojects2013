/*
 * WinAPI.cs - Utility structures and functions for accessing Windows API,
 * including GDI and User libraries
 * 
 * Note: this class includes some definitions and methods already present
 * in the internal class System.Drawing.Win32.Api - it should be made public
 * instead, adding new definitions from this file.
 *
 * Copyright (C) 2004 by Maciek Plewa (http://mil-sim.net/contact)
 *
 * Licence: refer to the Readme file
 */

using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace Tools.Misc
{
    /// <summary>
    /// Utility structures and functions for accessing Windows API
    /// </summary>
    public static class WinAPI
    {
#region Public types and Constants
        public static readonly int WS_POPUP = unchecked((int)0x80000000);

        public static readonly int SWP_NOSIZE = 0x0001;
        public static readonly int SWP_NOMOVE = 0x0002;
        public static readonly int SWP_NOACTIVATE = 0x0010;
        //Tooltip Window Constants

        public static readonly int SW_HIDE = 0;
#endregion
        
        public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        public static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        public static readonly IntPtr HWND_TOP = new IntPtr(0);
        public static readonly IntPtr HWND_BOTTOM = new IntPtr(1);



        /// <summary>
        /// Defines the possible window style flags in Microsoft's API. These flags can be OR'd together to make more complex styles.
        /// </summary>
        [Flags]
        public enum WindowStyles : uint
        {
            WS_OVERLAPPED       = 0x00000000,
            WS_POPUP        = 0x80000000,
            WS_CHILD        = 0x40000000,
            WS_MINIMIZE     = 0x20000000,
            WS_VISIBLE      = 0x10000000,
            WS_DISABLED     = 0x08000000,
            WS_CLIPSIBLINGS     = 0x04000000,
            WS_CLIPCHILDREN     = 0x02000000,
            WS_MAXIMIZE     = 0x01000000,
            WS_BORDER       = 0x00800000,
            WS_DLGFRAME     = 0x00400000,
            WS_VSCROLL      = 0x00200000,
            WS_HSCROLL      = 0x00100000,
            WS_SYSMENU      = 0x00080000,
            WS_THICKFRAME       = 0x00040000,
            WS_GROUP        = 0x00020000,
            WS_TABSTOP      = 0x00010000,

            WS_MINIMIZEBOX      = 0x00020000,
            WS_MAXIMIZEBOX      = 0x00010000,

            WS_CAPTION      = WS_BORDER | WS_DLGFRAME,
            WS_TILED        = WS_OVERLAPPED,
            WS_ICONIC       = WS_MINIMIZE,
            WS_SIZEBOX      = WS_THICKFRAME,
            WS_TILEDWINDOW      = WS_OVERLAPPEDWINDOW,

            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            WS_POPUPWINDOW      = WS_POPUP | WS_BORDER | WS_SYSMENU,
            WS_CHILDWINDOW      = WS_CHILD,
        }

        
        public const int S_OK = 0;
        public const int HWND_DESKTOP = 0;

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.
        LayoutKind.Sequential, Pack = 2)]
        public struct MAKELPARAM
        {
            public uint wLow;
            public uint wHigh;
        }

#region TOOLINFO
        [StructLayout(LayoutKind.Sequential)]
        public struct TOOLINFO
        {
            public int cbSize;
            public int uFlags;
            public IntPtr hwnd;
            public IntPtr uId;
            public RECT rect;
            public IntPtr hinst;

            //[MarshalAs(UnmanagedType.LPTStr)]
            public string lpszText;

            public IntPtr lParam;
        }
        #endregion

#region SIZEAPI
        [StructLayout(LayoutKind.Sequential)]
        public struct SIZEAPI
        {
            public int cx;
            public int cy;

            public override string ToString()
            {
                return String.Format("{0} x {1}", cx, cy);
            }
        }
#endregion

#region WINDOWINFO
        [StructLayout(LayoutKind.Sequential)]
        public struct WINDOWINFO
        {
            public UInt32 cbSize;
            public RECT rcWindow;
            public RECT rcClient;
            public UInt32 dwStyle;
            public UInt32 dwExStyle;
            public UInt32 dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public byte atomWindowType;
            public UInt16 wCreatorVersion;
        }
        #endregion

#region POINT
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;

            public POINT(Point point)
            {
                x = point.X;
                y = point.Y;
            }

            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
        #endregion

#region RECT
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            #region "Variables."
                /// <summary> 
                /// Left position of the rectangle. 
                /// </summary> 
                public int Left;
                /// <summary> 
                /// Top position of the rectangle. 
                /// </summary> 
                public int Top;
                /// <summary> 
                /// Right position of the rectangle. 
                /// </summary> 
                public int Right;
                /// <summary> 
                /// Bottom position of the rectangle. 
                /// </summary> 
                public int Bottom;
            #endregion

            #region "Operators."
                /// <summary> 
                /// Operator to convert a RECT to Drawing.Rectangle. 
                /// </summary> 
                /// <param name="rect">Rectangle to convert.</param> 
                /// <returns>A Drawing.Rectangle</returns> 
                public static implicit operator Rectangle(RECT rect)
                {
                    return Rectangle.FromLTRB(rect.Left, rect.Top, rect.Right, rect.Bottom);
                }

                /// <summary> 
                /// Operator to convert Drawing.Rectangle to a RECT. 
                /// </summary> 
                /// <param name="rect">Rectangle to convert.</param> 
                /// <returns>RECT rectangle.</returns> 
                public static implicit operator RECT(Rectangle rect)
                {
                    return new RECT(rect.Left, rect.Top, rect.Right, rect.Bottom);
                }
            #endregion

            #region "Constructor."
                /// <summary> 
                /// Constructor. 
                /// </summary> 
                /// <param name="inLeft">Horizontal position.</param> 
                /// <param name="inTop">Vertical position.</param> 
                /// <param name="inRight">Right most side.</param> 
                /// <param name="inBottom">Bottom most side.</param> 
                public RECT(int inLeft, int inTop, int inRight, int inBottom)
                {
                    Left = inLeft;
                    Top = inTop;
                    Right = inRight;
                    Bottom = inBottom;
                }
            #endregion
        }
#endregion

#region TEXTMETRIC
        /// <summary>
        /// Describes a physical font. The size is specified in logical units.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct TEXTMETRIC
        {
            int Height;
            int Ascent;
            int Descent;
            int InternalLeading;
            int ExternalLeading;
            int AveCharWidth;
            int MaxCharWidth;
            int Weight;
            int Overhang;
            int DigitizedAspectX;
            int DigitizedAspectY;
            Char FirstChar;
            Char LastChar;
            Char DefaultChar;
            Char BreakChar;
            byte Italic;
            byte Underlined;
            byte StruckOut;
            byte PitchAndFamily;
            byte CharSet;
        }
        #endregion

#region Pen styles
        public enum PenStyle : int
        {
            PS_SOLID = 0,
            PS_DASH = 1,
            PS_DOT = 2,
            PS_DASHDOT = 3,
            PS_DASHDOTDOT = 4,
            PS_NULL = 5,
            PS_INSIDEFRAME = 6,
            PS_USERSTYLE = 7,
            PS_ALTERNATE = 8,
            PS_STYLE_MASK = 0x0000000F
        }
#endregion

#region User32 library bindings

    /// <summary>
    /// Obtains information about an existing window
    /// </summary>
    /// <param name="hWnd">Window handle</param>
    /// <param name="pwi">Pointer to WINDOWINFO structure
    /// receiving the information [out]</param>
    /// <returns>Returns true if function succeeds or false if it fails</returns>
    [DllImport("user32")]
    public extern static bool GetWindowInfo(
        IntPtr hWnd,
        out WINDOWINFO pwi);

    /// <summary>
    /// Returns a handle to the window associated with the
    /// specified display device context
    /// </summary>
    /// <param name="hDC">Display device context</param>
    /// <returns>Window handle</returns>
    [DllImport("user32")]
    public extern static IntPtr WindowFromDC(IntPtr hDC);

    /// <summary>
    /// Determines whether the specified window handle
    /// identifies an existing window
    /// </summary>
    /// <param name="hWnd">Handle to the window to test</param>
    /// <returns></returns>
    [DllImport("user32")]
    public extern static bool IsWindow(IntPtr hWnd);

    /// <summary>
    /// Retrieves the desktop window handle
    /// </summary>
    /// <returns>Handle to desktop window</returns>
    [DllImport("user32")]
    public extern static IntPtr GetDesktopWindow();

    /// <summary>
    /// Retrieves a handle to specified window's parent window
    /// </summary>
    /// <param name="hWnd">Window handle</param>
    /// <returns>Handle to parent window</returns>
    [DllImport("user32")]
    public extern static IntPtr GetParent(IntPtr hWnd);

    [DllImport("user32.dll")]
    public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("User32", SetLastError = true)]
    public static extern int GetClientRect(IntPtr hWnd, ref RECT lpRect);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRECT);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern bool GetWindowRect(int hwnd, ref RECT lpRECT);



    [DllImport("user32.dll")]
    public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X,
                                             int Y, int cx, int cy, uint uFlags);

    [DllImport("User32", SetLastError = true)]
    public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

    [DllImport("User32", SetLastError = true)]
    public static extern bool SetWindowPos(Int32 hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

    [DllImport("user32.dll")]
    public static extern bool GetCursorPos(out Point lpPoint);


    /// <summary>
    /// Instantiate a System.Windows.Forms.Form and set its properties to control its style.
    ///The System.Windows.Forms.NativeWindow class can be used to encapsulate a window handle. It provides properties and methods that can be used to perform the same behavior as the CreateWindowEx method.
    /// </summary>
    /// <param name="dwExStyle"></param>
    /// <param name="lpClassName"></param>
    /// <param name="lpWindowName"></param>
    /// <param name="dwStyle"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="nWidth"></param>
    /// <param name="nHeight"></param>
    /// <param name="hWndParent"></param>
    /// <param name="hMenu"></param>
    /// <param name="hInstance"></param>
    /// <param name="lpParam"></param>
    /// <returns></returns>
    [DllImport("user32.dll")]
    public static extern IntPtr CreateWindowEx(
       uint dwExStyle,
       string lpClassName,
       string lpWindowName,
       uint dwStyle,
       int x,
       int y,
       int nWidth,
       int nHeight,
       IntPtr hWndParent,
       IntPtr hMenu,
       IntPtr hInstance,
       IntPtr lpParam);

    [DllImport("User32", SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref TOOLINFO lParam);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern int SendMessage(IntPtr hWndControl, int Msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string pszTitle);

    [DllImport("User32", SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("User32", SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessage(int handl, Tools.WindowMessage.MessageFilterDefinitions.WindowsMessages windowsMessages, int p, ref POINTAPI lp);
    
    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessage(int hwnd, int wMsg, int wParam, ref RECT lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessage(int hwnd, int wMsg, int wParam, ref POINTAPI lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessage(int hwnd, int wMsg, int wParam, ref IntPtr lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessageBynum(int hwnd, int wMsg, int wParam, int lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessageBystring(int hwnd, int wMsg, int wParam, string lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageW", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessageByString1(int hwnd, int wMsg, int wParam, string lParam);
    
    [DllImport("user32", EntryPoint = "SendMessageW", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SendMessageByByte(int hwnd, int wMsg, int wParam, ref short lParam); 
        



    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool PostMessage(IntPtr hWnd, int Msg, int wParam, Byte[] lParam);

    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("User32", CharSet = CharSet.Auto)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Portability", "CA1901:PInvokeDeclarationsShouldBePortable")]
    public static extern IntPtr PostMessage(IntPtr hWnd, int msg, string wParam, string lParam);

    [DllImport("User32", CharSet = CharSet.Auto)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Portability", "CA1901:PInvokeDeclarationsShouldBePortable")]
    public static extern IntPtr PostMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

    public static readonly int LOG_MESSAGE = RegisterWindowMessage("LOG_MESSAGE");
    [DllImport("user32")]
    public static extern int RegisterWindowMessage(string message);

    [DllImport("user32", SetLastError = true, EntryPoint = "BroadcastSystemMessage")]
    public static extern int BroadcastSystemMessageRecipients(MessageBroadcastFlags dwFlags, ref MessageBroadcastRecipients lpdwRecipients, uint uiMessage, StringBuilder wParamlpBuffer, StringBuilder lParamlpBuffer);

    [DllImport("user32", SetLastError = true)]
    public static extern int BroadcastSystemMessage(MessageBroadcastFlags dwFlags, IntPtr lpdwRecipients, uint uiMessage, StringBuilder wParamlpBuffer, StringBuilder lParamlpBuffer);

    [DllImport("user32", SetLastError = true, EntryPoint = "BroadcastSystemMessage")]
    public static extern int BroadcastSystemMessageRecipients(MessageBroadcastFlags dwFlags, ref MessageBroadcastRecipients lpdwRecipients, uint uiMessage, IntPtr wParam, IntPtr lParam);

    [DllImport("user32", SetLastError = true)]
    public static extern int BroadcastSystemMessage(MessageBroadcastFlags dwFlags, IntPtr lpdwRecipients, uint uiMessage, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll")]
    public static extern bool DestroyWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    public static extern bool ClientToScreen(IntPtr hWnd, ref POINT lpPoint);

    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int SetCursorPos(int x, int y);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int GetCursorPos(ref POINTAPI lpPoint);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int ScreenToClient(int hwnd, ref POINTAPI lpPoint);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int ClientToScreen(int hwnd, ref POINTAPI lpPoint);
    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
    public static extern int ShowCursor(int bShow);

    [DllImport("user32.dll")]
    public static extern IntPtr GetActiveWindow();

    /// <summary>The GetForegroundWindow function returns a handle to the foreground window.</summary>
    [DllImport("user32.dll")]
    public static extern IntPtr GetForegroundWindow();

    // For Windows Mobile, replace user32.dll with coredll.dll 
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool SetForegroundWindow(IntPtr hWnd);
#endregion

#region Kernel and CommCtl library bindings
        [Flags]
        public enum MessageBroadcastFlags : uint
        {
            BSF_QUERY = 0x00000001,
            BSF_IGNORECURRENTTASK = 0x00000002,
            BSF_FLUSHDISK = 0x00000004,
            BSF_NOHANG = 0x00000008,
            BSF_POSTMESSAGE = 0x00000010,
            BSF_FORCEIFHUNG = 0x00000020,
            BSF_NOTIMEOUTIFNOTHUNG = 0x00000040,
            BSF_ALLOWSFW = 0x00000080,
            BSF_SENDNOTIFYMESSAGE = 0x00000100,
            BSF_RETURNHDESK = 0x00000200,
            BSF_LUID = 0x00000400,
        }

        [Flags]
        public enum MessageBroadcastRecipients : uint
        {
            BSM_ALLCOMPONENTS = 0x00000000,
            BSM_VXDS = 0x00000001,
            BSM_NETDRIVER = 0x00000002,
            BSM_INSTALLABLEDRIVERS = 0x00000004,
            BSM_APPLICATIONS = 0x00000008,
            BSM_ALLDESKTOPS = 0x00000010,
        }

        [DllImport("kernel32.dll")]
        public extern static uint GetVersion();

        /// <summary>
        /// Enables Windows common controls to use XP visual styles
        /// </summary>
        [DllImport("comctl32")]
        public extern static void InitCommonControls();

        [System.Runtime.InteropServices.DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public unsafe static extern bool ZeroMemory(byte* destination, int length);

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
        public static extern void CopyMemory(IntPtr dest, IntPtr src, int size);

        //[DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
        //public static extern void CopyMemory(ref Single dest, IntPtr src, int size);


        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory", SetLastError = false)]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);
#endregion

#region GDI

        /// <summary>
        /// Selects an object into the specified device context
        /// </summary>
        /// <param name="hDc">Handle of the device context</param>
        /// <param name="hObject">Object handle</param>
        /// <returns>Returns IntPtr.Zero if function fails</returns>
        [DllImport("gdi32")]
        public extern static IntPtr SelectObject(
            IntPtr hDc,
            IntPtr hObject);

        [DllImport("gdi32")]
        public static extern IntPtr CreatePen(PenStyle fnPenStyle, int nWidth, int crColor);

        [DllImport("gdi32", EntryPoint = "Rectangle")]
        public static extern bool DrawRectangle(
            IntPtr hDc,
            int left,
            int top,
            int right,
            int bottom);

        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateDIBSection(IntPtr hdc, [In] ref BITMAPINFO pbmi,
           uint pila, out IntPtr ppvBits, IntPtr hSection, uint dwOffset);

        [DllImport("gdi32.dll")]
        public static extern int SetBkMode(IntPtr hdc, int iBkMode);
         

        public const int TRANSPARENT = 1;
        public const int OPAQUE = 2;

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BITMAPINFOHEADER
        {
            public uint biSize;
            public int biWidth;
            public int biHeight;
            public ushort biPlanes;
            public ushort biBitCount;
            public uint biCompression;
            public uint biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public uint biClrUsed;
            public uint biClrImportant;

            public void Init()
            {
                biSize = (uint)Marshal.SizeOf(this);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public class BITMAPINFO
        {
            public Int32 biSize;
            public Int32 biWidth;
            public Int32 biHeight;
            public Int16 biPlanes;
            public Int16 biBitCount;
            public Int32 biCompression;
            public Int32 biSizeImage;
            public Int32 biXPelsPerMeter;
            public Int32 biYPelsPerMeter;
            public Int32 biClrUsed;
            public Int32 biClrImportant;
            public Int32 colors;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RGBQUAD
        {
            public byte rgbBlue;
            public byte rgbGreen;
            public byte rgbRed;
            public byte rgbReserved;
        }
#endregion
        
        [DllImport("msvcrt.dll", SetLastError = false)]
        public static extern IntPtr memcpy(IntPtr dest, IntPtr src, int count);

        [DllImport("user32.dll")]
        public static extern Boolean GetKeyState(Tools.WindowMessage.MessageFilterDefinitions.VirtualKeyStates nVirtKey);

        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey); 

    }	// WinAPI class
};	 

