using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Web;


namespace Tools.Misc
{
    /// <summary>
    /// List of static functions
    /// </summary>
    public static class UtilsClass
    {
        public static Rectangle ScaleToFit(Bitmap SrcBitmap, Size DestSize)
        {
            // Assume Source is wider then Dest
            Single ratio = (Single)DestSize.Width / SrcBitmap.Width;

            // Check if source is toller then dest
            if (ratio * SrcBitmap.Height > DestSize.Height)
                ratio = (Single)DestSize.Height / SrcBitmap.Height;

            return new Rectangle((int)(DestSize.Width - ratio * SrcBitmap.Width) / 2,
                                 (int)(DestSize.Height - ratio * SrcBitmap.Height) / 2,
                                 (int)(ratio * SrcBitmap.Width),
                                 (int)(ratio * SrcBitmap.Height));
        }

        public static string EnsureTrailingSlash(string dir)
        {
            if (dir == null) return "";
            if (dir[dir.Length - 1] != '\\') dir += @"\";
            return dir;
        }

        public static Bitmap LoadBitmapFormFile(string FileName)
        {
            // This function loads a bitmap, copy it to a local bitmap and release the original one.
            // This is done to make sure that the file (FileName) does not stay locked.
            // See MSDN Article ID 814675 for detials

            Bitmap srcBM = new Bitmap(FileName);
            Bitmap cpyBM = new Bitmap(srcBM.Size.Width, srcBM.Size.Height, srcBM.PixelFormat);

            BitmapData srcData = srcBM.LockBits(new Rectangle(new Point(0, 0), srcBM.Size), ImageLockMode.ReadOnly, srcBM.PixelFormat);
            BitmapData cpyData = cpyBM.LockBits(new Rectangle(new Point(0, 0), srcBM.Size), ImageLockMode.WriteOnly, srcBM.PixelFormat);
            unsafe { WinAPI.CopyMemory(cpyData.Scan0, srcData.Scan0, srcData.Stride * srcData.Height); };
            srcBM.UnlockBits(srcData);
            cpyBM.UnlockBits(cpyData);

            srcBM.Dispose();
            return cpyBM;
        }

        /// <summary>
        /// Custom drawing function, used to draw on every type of controls.
        /// </summary>
        /// <param name="Sender">The control on wich wanted to draw</param>
        /// <param name="e">Provides data for the System.Windows.Forms.Control.Paint event.</param>
        /// <param name="StartColor">The collor from wich to start drawing</param>
        /// <param name="EndColor">The collor that system have to finish with.</param>
        public static void CustomPaint(object Sender, System.Windows.Forms.PaintEventArgs e, Color StartColor, Color EndColor)
        {
            try
            {


                Brush m_Brush; // Demonstration brush    
                //Color m_Color2  = System.Drawing.SystemColors.ControlLight //System.Drawing.SystemColors.GradientActiveCaption 'Color.FromArgb(255, 204, 204, 204) 'Color.DeepSkyBlue ' 'Color.FromArgb(168, 212, 248) 'Color.FromArgb(168, 212, 248) 'Color.White ' Color.FromArgb(50, 157, 237)    // Mainly acts as foreground color
                //Color  m_Color1 = Color.WhiteSmoke    //Color.Orange // Mainly acts as background color
                Color m_Color1 = StartColor;//System.Drawing.SystemColors.ControlLight; // System.Drawing.SystemColors.GradientActiveCaption 'Color.FromArgb(255, 204, 204, 204) 'Color.DeepSkyBlue ' 'Color.FromArgb(168, 212, 248) 'Color.FromArgb(168, 212, 248) 'Color.White ' Color.FromArgb(50, 157, 237)    // Mainly acts as foreground color
                Color m_Color2 = EndColor;//System.Drawing.SystemColors.ControlDark;
                Rectangle m_BrushSize; // Rectangle used when tiling brushes
                Graphics myGraphics; // Graphics object used to draw brushes

                if (((Control)Sender).Width <= 0 || ((Control)Sender).Height <= 0) return;
                m_BrushSize = new Rectangle(0, -1, ((Control)Sender).Width, ((Control)Sender).Height);

                LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush(m_BrushSize, m_Color2, m_Color1, LinearGradientMode.Vertical);
                myLinearGradientBrush.WrapMode = WrapMode.Tile;

                ColorBlend color_blend = new ColorBlend();
                color_blend.Colors = new Color[] { m_Color2, m_Color1, m_Color2 };
                color_blend.Positions = new float[] { .0F, .5F, 1 };
                myLinearGradientBrush.InterpolationColors = color_blend;


                myLinearGradientBrush.WrapMode = WrapMode.Tile;
                // The RotateTransform method rotates the brush by the user
                // specified amount
                myLinearGradientBrush.RotateTransform(0);

                // You can also use a ScaleTransform to deform the brush
                //   The following cuts the width of brush in half, and
                //   doubles the height.
                //.ScaleTransform(0.5F, 2.0F)

                // Set the point where the blending will focus.  Any single 
                // between 0 and 1 is allowed. The default is one.
                // .SetBlendTriangularShape(BlendFocus)


                // For more advanced uses, you can use the SetSiMedinolBellShape
                //   method to set where the center of the gradient occurs.
                //.SetSiMedinolBellShape(1)

                // Set m_Brush equal to the newly created brush
                m_Brush = myLinearGradientBrush;

                // Use the brush to draw the appropriate Drawing in the Gph
                myGraphics = e.Graphics;// '.CreateGraphics()

                // Select the Type of drawing based on user input
                // "Fill" fills the entire PictureBox
                myGraphics.FillRectangle(m_Brush, 0, -1, ((Control)Sender).Width, ((Control)Sender).Height);
            }
            catch {}
        }

        /// <summary>
        /// Retrieves the position and dimensions of a specified window
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns a rectangle object describing the client area</returns>
        public static Rectangle GetWindowClientRect(IntPtr hwnd)
        {
            if (hwnd == IntPtr.Zero)
                return System.Drawing.Rectangle.Empty;

            WinAPI.WINDOWINFO wi = new WinAPI.WINDOWINFO();

            if (WinAPI.GetWindowInfo(hwnd, out wi))
            {
                Rectangle rect = new Rectangle(
                    wi.rcClient.Left,
                    wi.rcClient.Top,
                    wi.rcClient.Right - wi.rcClient.Left,
                    wi.rcClient.Bottom - wi.rcClient.Top);

                return rect;
            }

            return System.Drawing.Rectangle.Empty;
        }

        /// <summary>
        /// Retrieves Windows OS version
        /// </summary>
        /// <returns>
        /// System.Version object representing Windows version
        /// </returns>
        public static System.Version GetSystemVersion()
        {
            uint version = WinAPI.GetVersion();

            int major = (int)version & 0xFF;
            int minor = ((int)version & 0xFF00) >> 8;
            int build = ((int)version & 0xFFF0000) >> 16;

            return new Version(major, minor, build);
        }

        public static IntPtr MakeLParam(int lo, int hi)
        {
            return (IntPtr)(((short)hi << 16) | (lo & 0xffff));
        }

        #region Sliding Const and functions
            /// <summary>
            /// Animates the window from left to right. This flag can be used with roll or slide animation.
            /// </summary>
            public const int AW_HOR_POSITIVE = 0X1;
            /// <summary>
            /// Animates the window from right to left. This flag can be used with roll or slide animation.
            /// </summary>
            public const int AW_HOR_NEGATIVE = 0X2;
            /// <summary>
            /// Animates the window from top to bottom. This flag can be used with roll or slide animation.
            /// </summary>
            public const int AW_VER_POSITIVE = 0X4;
            /// <summary>
            /// Animates the window from bottom to top. This flag can be used with roll or slide animation.
            /// </summary>
            public const int AW_VER_NEGATIVE = 0X8;
            /// <summary>
            /// Makes the window appear to collapse inward if AW_HIDE is used or expand outward if the AW_HIDE is not used.
            /// </summary>
            public const int AW_CENTER = 0X10;
            /// <summary>
            /// Hides the window. By default, the window is shown.
            /// </summary>
            public const int AW_HIDE = 0X10000;
            /// <summary>
            /// Activates the window.
            /// </summary>
            public const int AW_ACTIVATE = 0X20000;
            /// <summary>
            /// Uses slide animation. By default, roll animation is used.
            /// </summary>
            public const int AW_SLIDE = 0X40000;
            /// <summary>
            /// Uses a fade effect. This flag can be used only if hwnd is a top-level window.
            /// </summary>
            public const int AW_BLEND = 0X80000;

            /// <summary>
            /// Animates a window.
            /// </summary>
            [DllImport("user32.dll", CharSet = CharSet.Auto)]
            public static extern int AnimateWindow(IntPtr hwand, int dwTime, int dwFlags);
        #endregion

        public static Int32 GetLoWord(int DWord)
        {
            if ((DWord & 32768) != 0)
            {
                // &H8000& = &H00008000 
                return DWord | -65536;
            }
            else
            {
                return DWord & 65535;
            }
        }

        public static Int32 GetHiWord(int DWord)
        {
            return (DWord & -65536) / 65536;
        } 
    }
} 


