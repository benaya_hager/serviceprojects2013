﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Tools.Misc
{
    [StructLayout(LayoutKind.Sequential)]
    public struct POINTAPI
    {
        public int X;
        public int Y;

        public POINTAPI(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static implicit operator System.Drawing.Point(POINTAPI p)
        {
            return new System.Drawing.Point(p.X, p.Y);
        }

        public static implicit operator POINTAPI(System.Drawing.Point p)
        {
            return new POINTAPI(p.X, p.Y);
        }
    }
}
