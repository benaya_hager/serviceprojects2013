﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Tools.Reflection
{
    public class ReflectionTools
    {
        public static Type[] FindDerivedTypesFromAssembly(Assembly assembly, Type baseType, bool classOnly)
        {
            List<Type> res = new List<Type>();

            if (assembly == null)
                throw new ArgumentNullException("assembly", "Assembly must be defined");
            if (baseType == null)
                throw new ArgumentNullException("baseType", "Parent Type must be defined");

            // get all the types
            var types = assembly.GetTypes();

            // works out the derived types
            foreach (var type in types)
            {
                // if classOnly, it must be a class
                // useful when you want to create instance
                if (classOnly && !type.IsClass)
                    continue;

                if (baseType.IsInterface)
                {
                    var it = type.GetInterface(baseType.FullName);

                    if (it != null)
                        // add it to result list
                        res.Add(type);
                }
                else if (type.IsSubclassOf(baseType))
                {
                    // add it to result list
                    res.Add(type);
                }
            }
            Type[] lstOfTypes = new Type[res.Count + 1];
            lstOfTypes[0] = baseType;
            int i = 1;
            foreach (var type in res)
                lstOfTypes[i++] = type;
            return lstOfTypes;
        }

        public static Type[] Concat(Type[] list1, Type[] list2, Type[] list3)
        {
            Int32 iIndex = 0, j = 0;
            Type[] res = new Type[list1.Length + list2.Length + list3.Length];

            for (j = 0; j < list1.Length; j++)
            {
                res[iIndex++] = list1[j];
            }
            for (j = 0; j < list2.Length; j++)
            {
                res[iIndex++] = list2[j];
            }
            for (j = 0; j < list3.Length; j++)
            {
                res[iIndex++] = list3[j];
            }
            return res;
        }


        public static Type[] Concat(List<Type[]> lst_to_concat)
        {
            List<Type> tmplst = new List<Type>();

            foreach (Type[] tp_array in lst_to_concat)
            {
                for (int j = 0; j < tp_array.Length; j++)
                {
                    if (!tmplst.Contains(tp_array[j]))
                    {
                        tmplst.Add(tp_array[j]);
                    }
                }
            }
            Type[] res = new Type[tmplst.Count];
            tmplst.CopyTo(res);
            return res;
        }

        public static object GetNewObject(Type t)
        {
            try
            {
                return t.GetConstructor(new Type[] { }).Invoke(new object[] { });
            }
            catch
            {
                return null;
            }
        }

        public static T GetNewObject<T>()
        {
            try
            {
                return (T)typeof(T).GetConstructor(new Type[] { }).Invoke(new object[] { });
            }
            catch
            {
                return default(T);
            }
        }
    }
}