﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web; 

namespace Tools.StringUtils
{
    /// <summary>
    /// Static class that exposes many string methods, 
    /// useful for string evaluation and edits
    /// </summary>
    public static class StringTool
    {
        #region String Constants To Help With Comparisons
            private const string m_Letters = "abcdefghijklmnopqrstuvwxyz";
            private const string m_Digits = "0123456789";
            private const string m_ForwardSlash = "/";
            private const string m_BackSlash = "\\";
            private const string m_Period = ".";
            private const string m_DollarSign = "$";
            private const string m_PercentSign = "%";
            private const string m_Comma = ",";
            private const string m_Yes = "yes";
            private const string m_No = "no";
            private const string m_True = "true";
            private const string m_False = "false";
            private const string m_1 = "1";
            private const string m_0 = "0";
            private const string m_y = "y";
            private const string m_n = "n";
        #endregion
        
        #region Data Type String Constants
            public const string m_GUID = "guid";
            public const string m_Boolean1 = "boolean";
            public const string m_Boolean2 = "bool";
            public const string m_Byte = "byte";
            public const string m_Char = "char";
            public const string m_DateTime = "datetime";
            public const string m_DBNull = "dbnull";
            public const string m_Decimal = "decimal";
            public const string m_Double = "double";
            public const string m_Empty = "empty";
            public const string m_Int16_1 = "int16";
            public const string m_Int16_2 = "short";
            public const string m_Int32_1 = "int32";
            public const string m_Int32_2 = "int";
            public const string m_Int32_3 = "integer";
            public const string m_Int64_1 = "int64";
            public const string m_Int64_2 = "long";
            public const string m_Object = "object";
            public const string m_SByte = "sbyte";
            public const string m_Single = "single";
            public const string m_String = "string";
            public const string m_UInt16 = "uint16";
            public const string m_UInt32 = "uint32";
            public const string m_UInt64 = "uint64";
        #endregion
        
        #region Methods That Check Data Type
            /// <summary>
            /// Evaluates whether passed-in string can be converted to a bool
            /// </summary>
            /// <param name="stream">string to check</param>
            /// <returns>
            /// bool indicating whether stream is a bool (0, 1, true/True, 
            /// false/False)
            /// </returns>
            public static bool IsStandardBool(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;
                    stream = stream.Trim().ToLower();
                    switch (stream)
                    {
                        case m_0:
                            return true;
                        case m_1:
                            return true;
                        case m_True:
                            return true;
                        case m_False:
                            return true;
                        default:
                            return false;
                    }
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Evaluates whether string can can be COERCED to a bool
            /// </summary>
            /// <param name="stream">string to check</param>
            /// <returns>
            /// bool indicating whether argument is a standard or custom bool 
            /// (0, 1, true/True, false/False) OR (y/Y, yes/Yes, n/N, no/NO) 
            /// </returns>
            public static bool IsFriendlyBool(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;
                    stream = stream.Trim().ToLower();
                    switch (stream)
                    {
                        case m_0:
                            return true;
                        case m_1:
                            return true;
                        case m_True:
                            return true;
                        case m_False:
                            return true;
                        case m_n:
                            return true;
                        case m_y:
                            return true;
                        case m_No:
                            return true;
                        case m_Yes:
                            return true;
                        default:
                            return false;
                    }
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Returns a bool conversion of the passed in string
            /// </summary>
            /// <param name="stream">string to convert/coerce</param>
            /// <returns>
            /// bool representation of passed-in string
            /// </returns>
            public static bool CoerceToBool(string stream)
            {
                try
                {
                    stream = stream.Trim().ToLower();
                    switch (stream)
                    {
                        case m_0:
                            return true;
                        case m_1:
                            return true;
                        case m_True:
                            return true;
                        case m_False:
                            return false;
                        case m_n:
                            return false;
                        case m_y:
                            return true;
                        case m_No:
                            return false;
                        case m_Yes:
                            return true;
                        default:
                            return false;
                    }
                }
                catch //  (Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Evaluates whether passed-in string contains any characters/
            /// digits/symbols. Trims spaces before checking.
            /// </summary>
            /// <param name="stream">string to check</param>
            /// <returns>
            /// bool indicating whether argument is void of characters/
            /// digits/symbols
            ///</returns>
            public static bool IsEmpty(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty
                        || stream.Trim() == string.Empty)
                        return true;
                    else
                        return false;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Checks each character of the string for any character other 
            /// than a digit, or a dollar sign or a percentage sign. If any
            /// are found, returns false indicating that the stream is NOT
            /// a number
            /// </summary>
            /// <param name="stream">
            /// The stream of characters (string) to check
            /// </param>
            /// <returns>
            /// True/False value indicating whether the string can be 
            /// coerced to a number
            /// </returns>
            public static bool IsNumber(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;

                    string character = string.Empty;
                    //set a string up of all characters that may indicate
                    //that the stream is a number, or a formatted number:
                    string validCharacters = m_Digits + m_Period +
                        m_DollarSign + m_Comma;
                    for (int i = 0; i < stream.Length; i++)
                    {
                        character = stream.Substring(i, 1);
                        if (!validCharacters.Contains(character))
                            return false;
                    }
                    return true;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Checks the string to see whether it is a number & if it is, 
            /// then it checks whether there is formatting applied to that #
            /// </summary>
            /// <param name="stream">
            /// The stream of characters (string) to check
            /// </param>
            /// <returns>
            /// True/False value indicating whether the string is a number
            /// that is formatted (contains digits and number formatting)
            /// </returns>
            public static bool IsFormattedNumber(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;

                    string character = string.Empty;
                    //set a string up of all characters that may indicate that 
                    //the stream is a number, or a formatted number:
                    string validCharacters = m_Digits + m_Period +
                        m_DollarSign + m_PercentSign + m_Comma;


                    for (int i = 0; i < stream.Length; i++)
                    {
                        character = stream.Substring(i, 1);
                        if (!validCharacters.Contains(character))
                            //the stream contains non-numeric characters:
                            return false;
                    }
                    //at this point, each single character is a number OR an 
                    //allowable symbol, but we must see whether those 
                    //characters contain a formatting character:
                    string formattingCharacters = m_DollarSign +
                        m_PercentSign + m_Comma;
                    for (int i = 0; i < stream.Length; i++)
                    {
                        if (formattingCharacters.Contains(character))
                            return true;
                    }
                    //still here? then the stream is a number, but NOT a 
                    //formatted number
                    return false;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Checks whether string can be coerced into a DateTime value
            /// </summary>
            /// <param name="stream">The string to check/// </param>
            /// <returns>
            /// bool indicating whether stream can be converted to a date
            /// </returns>
            public static bool IsDate(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;
                    DateTime checkDate = new DateTime();
                    bool dateType = true;
                    try
                    {
                        checkDate = DateTime.Parse(stream);
                    }
                    catch
                    {
                        dateType = false;
                    }
                    return dateType;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Checks the string to see whether it is a number and if it is, 
            /// then it checks whether there is a decimal in that number.
            /// </summary>
            /// <param name="stream">
            /// The stream of characters (string) to check
            /// </param>
            /// <returns>
            /// True/False value indicating whether the string is a
            /// double---must be a number, and include a decimal 
            /// in order to pass the test
            /// </returns>
            public static bool IsDouble(string stream)
            {
                try
                {
                    if (stream == null || stream == string.Empty)
                        return false;


                    if (!IsNumber(stream))
                        return false;


                    //at this point each character is a number OR an allowable
                    //symbol; we must see whether the string holds the decimal:
                    if (stream.Contains(m_Period))
                        return true;


                    //still here? the stream is a #, but does NOT have a decimal
                    return false;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return false;
                }
            }


            /// <summary>
            /// Checks string to see if it matches a TypeCode string and returns
            /// that TypeCode, or returns TypeCode.Empty if there is no match
            /// </summary>
            /// <param name="dataTypeString">
            /// String representation of a TypeCode (string, int, bool...)
            /// </param>
            /// <returns>TypeCode that maps to the dataTypeString</returns>
            public static TypeCode GetDataType(string dataTypeString)
            {
                try
                {
                    switch (dataTypeString.ToLower())
                    {
                        // todo: isn't there a better way for guid?
                        case m_GUID:
                            return TypeCode.Object;
                        case m_Boolean1:
                            return TypeCode.Boolean;
                        case m_Boolean2:
                            return TypeCode.Boolean;
                        case m_Byte:
                            return TypeCode.Byte;
                        case m_Char:
                            return TypeCode.Char;
                        case m_DateTime:
                            return TypeCode.DateTime;
                        case m_DBNull:
                            return TypeCode.DBNull;
                        case m_Decimal:
                            return TypeCode.Decimal;
                        case m_Double:
                            return TypeCode.Double;
                        case m_Empty:
                            return TypeCode.Empty;
                        case m_Int16_1:
                            return TypeCode.Int16;
                        case m_Int16_2:
                            return TypeCode.Int16;
                        case m_Int32_1:
                            return TypeCode.Int32;
                        case m_Int32_2:
                            return TypeCode.Int32;
                        case m_Int32_3:
                            return TypeCode.Int32;
                        case m_Int64_1:
                            return TypeCode.Int64;
                        case m_Int64_2:
                            return TypeCode.Int64;
                        case m_Object:
                            return TypeCode.Object;
                        case m_SByte:
                            return TypeCode.SByte;
                        case m_Single:
                            return TypeCode.Single;
                        case m_String:
                            return TypeCode.String;
                        case m_UInt16:
                            return TypeCode.UInt16;
                        case m_UInt32:
                            return TypeCode.UInt32;
                        case m_UInt64:
                            return TypeCode.UInt64;
                        default:
                            return TypeCode.Empty;
                    }
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return TypeCode.Empty;
                }
            }

            /// <summary>
            /// Checks to see if a string is a valid email address. A valid email address is at least 5 characters, and has an '@' and a '.' in it
            /// </summary>
            /// <param Name="email">String containing email address to check</param>
            /// <returns>Returns boolean indicating whether this is a valid email address or not</returns>
            public static bool IsValidEmailAddress(string email)
            {
                email += "";
                if (email.Length < 5 || email.IndexOf("@") == -1 || email.IndexOf(".") == -1)
                    return false;
                else
                    return true;
            }
        #endregion
        
        #region String Conversions
            /// <summary>
            /// Returns a date, as coerced from the string argument. Will raise
            /// an error, if the string cannot be coerced 
            /// ----so, use IsDate in this same class FIRST
            /// </summary>
            /// <param name="stream">string to get date value from</param>
            /// <returns>a DateTime object</returns>
            public static DateTime GetDate(string stream)
            {
                DateTime dateValue = new DateTime();
                try
                {
                    dateValue = DateTime.Parse(stream);
                    return dateValue;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return dateValue;
                }
            }


            /// <summary>
            /// Returns an int, as coerced from the string argument. 
            /// Will raise an error, if the string cannot be coerced
            /// ----so, use IsNumber in this same class FIRST
            /// </summary>
            /// <param name="stream">string to get int value from</param>
            /// <returns>an int object</returns>
            public static int GetInteger(string stream)
            {
                try
                {
                    int number = 0;
                    if (!IsNumber(stream))
                        return number;
                    //still here? check to see if it is formatted:
                    if (IsFormattedNumber(stream))
                    {
                        //it's formatted; replace the format characters
                        //with nothing (retain the decimal so as not to change 
                        //the intended value
                        stream = stream.Replace(m_Comma, string.Empty);
                        stream = stream.Replace(m_DollarSign, string.Empty);
                        stream = stream.Replace(m_PercentSign, string.Empty);
                    }
                    //we've removed superfluous formatting characters, if they 
                    //did exist, now let's round it/convert it, and return it:
                    number = Convert.ToInt32(stream);
                    return number;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return 0;
                }
            }


            /// <summary>
            /// Returns a double, as coerced from the string argument. 
            /// Will raise an error, if the string cannot be coerced
            /// ----so, use IsNumber in this same class FIRST
            /// </summary>
            /// <param name="stream">string to get double value from</param>
            /// <returns>a double object</returns>
            public static double GetDouble(string stream)
            {
                try
                {
                    double number = 0;
                    if (!IsNumber(stream))
                        return number;
                    //still here? check to see if it is formatted:
                    if (IsFormattedNumber(stream))
                    {
                        //it's formatted; replace the format characters
                        //with nothing (retain the decimal so as not to change 
                        //the intended value)
                        stream = stream.Replace(m_Comma, string.Empty);
                        stream = stream.Replace(m_DollarSign, string.Empty);
                        stream = stream.Replace(m_PercentSign, string.Empty);
                    }


                    //we've removed superfluous formatting characters, if they 
                    //did exist, now let's round it/convert it, and return it:
                    number = Convert.ToDouble(stream);
                    return number;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return 0;
                }
            }
        #endregion
        
        #region String Edits
            /// <summary>
            /// Gets a number as an ordered number, if you know what I mean. For example, converts 1 to "1st" or 2 to "2nd"
            /// </summary>
            public static String GetOrdinalSuffix(int number)
            {
                if ((number % 100) > 3 && (number % 100) < 21)
                {
                    return "th";
                }
                switch (number % 10)
                {
                    case 1:
                        return "st";
                    case 2:
                        return "nd";
                    case 3:
                        return "rd";
                    default:
                        return "th";
                }
            }

            /// <summary>
            /// Iterates thru an entire string, and sets the first letter of 
            /// each word to uppercase, and all ensuing letters to lowercase
            /// </summary>
            /// <param name="stream">The string to alter the case of</param>
            /// <returns>
            /// Same string w/uppercase initial letters & others as lowercase
            /// </returns>
            public static string MixCase(string stream)
            {
                try
                {
                    string newString = string.Empty;
                    string character = string.Empty;
                    string preceder = string.Empty;
                    for (int i = 0; i < stream.Length; i++)
                    {
                        character = stream.Substring(i, 1);
                        if (i > 0)
                        {
                            //look at the character immediately before current
                            preceder = stream.Substring(i - 1, 1);
                            //remove white space character from predecessor
                            if (preceder.Trim() == string.Empty)
                                //the preceding character WAS white space, so
                                //we'll change the current character to UPPER
                                character = character.ToUpper();
                            else
                                //the preceding character was NOT white space,
                                //we'll force the current character to LOWER
                                character = character.ToLower();
                        }
                        else
                            //index is 0, thus we are at the first character
                            character = character.ToUpper();
                        //add the altered character to the new string:
                        newString += character;
                    }
                    return newString;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }

            /// <summary>
            /// Iterates thru a string, and removes anything set to clean.
            /// Except---Does NOT remove anything in exceptionsToAllow
            /// </summary>
            /// <param name="stream">
            /// The string to clean</param>
            /// <returns>
            /// The same string, missing all elements that were set to clean
            /// (except when a character was listed in exceptionsToAllow)
            /// </returns>
            public static string Clean(string stream, bool cleanWhiteSpace,
                bool cleanDigits, bool cleanLetters, string exceptionsToAllow)
            {
                try
                {
                    string newString = string.Empty;
                    string character = string.Empty;
                    string blessed = string.Empty;
                    if (!cleanDigits)
                        blessed += m_Digits;
                    if (!cleanLetters)
                        blessed += m_Letters;
                    blessed += exceptionsToAllow;
                    //we set the comparison string to lower
                    //and will compare each character's lower case version
                    //against the comparison string, without
                    //altering the original case of the character
                    blessed = blessed.ToLower();
                    for (int i = 0; i < stream.Length; i++)
                    {
                        character = stream.Substring(i, 1);
                        if (blessed.Contains(character.ToLower()))
                            //add the altered character to the new string:
                            newString += character;
                        else if (character.Trim() == string.Empty &&
                            !cleanWhiteSpace)
                            newString += character;
                    }
                    return newString;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }

            /// <summary>
            /// Converts a string in camel format suchAsThis into a string Such As This
            /// </summary>
            /// <param name="s">String to parse</param>
            /// <returns>camel format string</returns>
            public static string MakeFriendly(string s)
            {
                char[] chars = s.ToCharArray();
                string output = chars[0].ToString().ToUpper();
                for (int i = 1; i < chars.Length; i++)
                {
                    char c = chars[i];
                    if (c >= 'A' && c <= 'Z')
                    {
                        output += " " + c.ToString();
                    }
                    else
                    {
                        output += c.ToString();
                    }
                }
                return output;

            }

            /// <summary>
            /// Parses a string so that it is suitable for a CONTAINSTABLE query
            /// </summary>
            /// <param Name="searchQuery">The query to parse</param>
            /// <param Name="noiseWords">The noisewords for this database - all lowercase</param>
            /// <param Name="searchType">The type of search to do; either <i>AND</i> or <i>OR</i></param>
            /// <returns>The string ready to be queried</returns>
            public static string ParseStringForFullTextSearch(string searchQuery, Dictionary<string, bool> noiseWords, string searchType)
            {
                string query = (" " + searchQuery + " ").Replace("'", "''").ToLower();
                query = query.Replace("\"", "");
                string[] words = query.Split(new char[] { ' ' });
                foreach (string word in words)
                {
                    if (noiseWords.ContainsKey(word))
                    {
                        query = query.Replace(" " + word + " ", " ");
                    }
                }
                while (query.IndexOf("  ") != -1)
                {
                    query = query.Replace("  ", " ");
                }
                query = query.Trim();
                query = "\"" + query.Replace(" ", "\" " + searchType + " \"") + "\"";
                return query;
            }

            /// <summary>
            /// Parses a string so that it is suitable for a CONTAINSTABLE query
            /// </summary>
            /// <param Name="searchQuery">The query to parse</param>
            /// <param Name="noiseWords">The noisewords for this database - all lowercase</param>
            /// <param Name="searchType">The type of search to do; either <i>AND</i> or <i>OR</i></param>
            /// <returns>The string ready to be queried</returns>
            public static string ParseStringForFullTextSearch(string searchQuery, string[] noiseWords, string searchType)
            {
                string query = (" " + searchQuery + " ").Replace("'", "''").ToLower();
                query = query.Replace("\"", "");
                for (int i = 0; i < noiseWords.Length; i++)
                {
                    while (query.IndexOf(" " + noiseWords[i] + " ") != -1)
                        query = query.Replace(" " + noiseWords[i] + " ", " ");
                }
                while (query.IndexOf("  ") != -1)
                {
                    query = query.Replace("  ", " ");
                }
                query = query.Trim();
                query = "\"" + query.Replace(" ", "\" " + searchType + " \"") + "\"";
                return query;
            }

            /// <summary>
            /// Appends a Name and Value pair to a URL
            /// </summary>
            /// <remarks>Automatically adds the '?' or '&amp;' as necessary, and URLEncodes the Value</remarks>
            public static String AppendNameValueToUrl(string url, string name, string value)
            {
                if (url.IndexOf("?") > 0)
                {
                    url += "&";
                }
                else
                {
                    url += "?";
                }

                return url + name + "=" + HttpUtility.UrlEncode(value);
            }

            /// <summary>
            /// Strips a string of all but the alpha-numeric characters
            /// </summary>
            /// <param Name="input">The string you wish to parse</param>
            /// <returns>Returns just the alpha-numeric characters of the input string</returns>
            public static string RemoveNonAlphaNumeric(string input)
            {
                char[] chars = input.ToCharArray();
                string output = "";
                for (int i = 0; i < chars.Length; i++)
                {
                    char c = chars[i];
                    if (Char.IsLetterOrDigit(c))
                    {
                        output += c.ToString();
                    }
                }
                return output;
            }

            /// <summary>
            /// Strips a string of all but the numeric characters
            /// </summary>
            /// <param Name="input">The string you wish to parse</param>
            /// <returns>Returns just the numeric characters of the input string</returns>
            public static string RemoveNonNumeric(string input)
            {
                char[] chars = input.ToCharArray();
                string output = "";
                for (int i = 0; i < chars.Length; i++)
                {
                    char c = chars[i];
                    if (Char.IsDigit(c))
                    {
                        output += c.ToString();
                    }
                }
                return output;
            }

        #endregion

        #region Exception Parsing
            /// <summary>Gets a diagnostic report of an exception</summary>
            public static string GetExceptionReportAsHtml(Exception ex, HttpRequest request, string extraInformation)
            {

                // don't report ThreadAbortException exceptions as these are thrown by Response.Redirect
                if (ex is System.Threading.ThreadAbortException)
                {
                    throw new Tools.ExceptionNS.CustomException("Thread abort exception - not important");
                }

                string url = "";
                string referrer = "";
                string userAgent = "";
                if (request != null)
                {
                    url = request.Url.AbsoluteUri;
                    referrer = (request.UrlReferrer != null) ? request.UrlReferrer.AbsoluteUri : "null";
                    userAgent = request.UserAgent;
                }


                string summary = "<b>" + ex.GetType().FullName + "</b> details follow:<br><br>" +
                    "<b>URL:</b> <a href=\"" + url + "\">" + url + "</a><br><br>" +
                    "<b>Referrer:</b> <a href=\"" + referrer + "\">" + referrer + "</a><br><br>" +
                    "<i>" + extraInformation + "</i><br><br>" + ex.Message + "<br><br>";

                // if it is an SqlException, send details, including SQL statement
                if (ex is Tools.ExceptionNS.CustomSqlException)
                {
                    summary += ((Tools.ExceptionNS.CustomSqlException)ex).ToHtml();
                }

                summary += "<b>Stack Trace :</b><br><br>" + ex.StackTrace.Replace("\n", "<br>") + "<br><br><br><B>User Agent:</B><br>" + userAgent;

                return summary;

            }
        #endregion
        
        #region String Locators
            /// <summary>
            /// Parses a file system or url path, and locates the file name
            /// </summary>
            /// <param name="fullPath">
            /// String indicating a file system or url path to a file
            /// </param>
            /// <param name="includeFileExtension">
            /// Whether to return file extension in addition to file name
            /// </param>
            /// <returns>
            /// File name, if found, and extension if requested, and located
            /// </returns>
            public static string GetFileNameFromPath(string fullPath,
                bool includeFileExtension)
            {
                try
                {
                    bool url = fullPath.Contains(m_ForwardSlash);
                    string search = string.Empty;
                    if (url)
                        search = m_ForwardSlash;
                    else
                        search = m_BackSlash;
                    string portion = string.Empty;

                    int decimals = GetKeyCharCount(fullPath, m_Period);
                    if (decimals >= 1)
                        //get all text to the RIGHT of the LAST slash:
                        portion = GetExactPartOfString(fullPath, search, false,
                            false, false);
                    else
                        return string.Empty;

                    if (includeFileExtension)
                        return portion;
                    search = m_Period;
                    portion = GetExactPartOfString(portion, search, false,
                        true, false);
                    return portion;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }


            /// <summary>
            /// Parses a url or file stream string, to get and return the 
            /// path portion (sans the file name and extension)
            /// </summary>
            /// <param name="fullPath">
            /// A string indicating a file system path or a url. Can 
            /// contain a file name/extension.
            /// </param>
            /// <returns>
            /// The original path minus the file name and extension, 
            /// if it had existed, with no extension will return
            /// the original string, plus an optional slash
            /// </returns>
            public static string GetFolderPath(string fullPath)
            {
                try
                {
                    bool url = fullPath.Contains(m_ForwardSlash);
                    string slash = string.Empty;
                    if (url)
                        slash = m_ForwardSlash;
                    else
                        slash = m_BackSlash;

                    string fileName = GetFileNameFromPath(fullPath, true);
                    //use tool to return all text to the LEFT of the file name
                    string portion = GetStringBetween(fullPath, string.Empty,
                        fileName);

                    //add the pertinent slash to the end of the string;
                    if (portion.Length > 0 && portion.Substring(
                        portion.Length - 1, 1) != slash)
                        portion += slash;
                    return portion;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }


            /// <summary>
            /// Useful to pinpoint exact string between whatever 
            /// characters/string you wish to grab text from
            /// </summary>
            /// <param name="stream">
            /// string from which to cull subtext from
            /// </param>
            /// <param name="from">
            /// string that precedes the text you are looking for
            /// </param>
            /// <param name="to">
            /// string that follows the text you are looking for
            /// </param>
            /// <returns>
            /// The string between point x and point y
            /// </returns>
            public static string GetStringBetween(string stream, string from,
                string to)
            {
                try
                {
                    string subField = string.Empty;
                    subField = RightOf(stream, from);
                    subField = LeftOf(subField, to);
                    return subField;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }

            /// <summary>
            /// Will return the text to the LEFT of indicated substring 
            /// </summary>
            /// <param name="stream">
            /// string from which to cull a portion of text
            /// </param>
            /// <param name="stringToStopAt">
            /// string that indicates what char or string to stop at
            /// </param>
            /// <returns>
            /// The string to the left of point x (stringToStopAt)
            /// </returns>
            public static string LeftOf(string stream, string stringToStopAt)
            {
                try
                {
                    if (stringToStopAt == null || stringToStopAt == string.Empty)
                        return stream;


                    int stringLength = stream.Length;
                    int findLength = stringToStopAt.Length;


                    stringToStopAt = stringToStopAt.ToLower();
                    string temp = stream.ToLower();
                    int i = temp.IndexOf(stringToStopAt);


                    if ((i <= -1) && (stringToStopAt != temp.Substring(0, findLength))
                        || (i == -1))
                        return stream;


                    string result = stream.Substring(0, i);
                    return result;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }


            /// <summary>
            /// Will return the text to the RIGHT of whatever substring you indicate
            /// </summary>
            /// <param name="stream">
            /// string from which to cull a portion of text
            /// </param>
            /// <param name="stringToStartAfter">
            /// string that indicates what char or string to start after
            /// </param>
            /// <returns>
            /// The string to the right of point x (stringToStartAfter)
            /// </returns>
            public static string RightOf(string stream, string stringToStartAfter)
            {
                try
                {
                    if (stringToStartAfter == null || stringToStartAfter == string.Empty)
                        return stream;
                    stringToStartAfter = stringToStartAfter.ToLower();
                    string temp = stream.ToLower();
                    int findLength = stringToStartAfter.Length;
                    int i = temp.IndexOf(stringToStartAfter);
                    if ((i <= -1) && (stringToStartAfter != temp.Substring(0, findLength))
                        || (i == -1))
                        return stream;

                    string result =
                        stream.Substring(i + findLength, stream.Length - (i + findLength));
                    return result;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }


            /// <summary>
            /// Searches a string for every single instance of the passed-in
            /// field delimiters, and returns all the values between those
            /// delimiters, as a List object
            /// </summary>
            /// <param name="streamToSearch">string to search</param>
            /// <param name="leftFieldDelimiter">string to start at</param>
            /// <param name="rightFieldDelimiter">string to stop at</param>
            /// <returns>A List object of strings</returns>
            public static List<string> GetEachFieldValue(string streamToSearch,
                string leftFieldDelimiter, string rightFieldDelimiter)
            {
                string search = streamToSearch;
                string field = string.Empty;
                List<string> fields = new List<string>();
                while (!string.IsNullOrEmpty(search)
                    && search.Contains(leftFieldDelimiter)
                    && search.Contains(rightFieldDelimiter))
                {
                    //get the val and add to list
                    field = GetStringBetween(search, leftFieldDelimiter,
                        rightFieldDelimiter);
                    if (!string.IsNullOrEmpty(field))
                        fields.Add(field);
                    //shorten the search string and continue
                    search = RightOf(search, field + rightFieldDelimiter);
                }
                return fields;
            }


            /// <summary>
            /// Instructions on using arguments:
            /// Set firstInstance = true, to stop at first instance of locateChar
            /// If firstInstance = false, then the LAST instance of locateChar will be used
            /// Set fromLeft = true, to return string from the left of locateChar
            /// If fromLeft = false, then the string from the right of locateChar 
            /// will be returned.
            /// Set caseSensitive to true/false for case-sensitivity
            /// EXAMPLES:
            /// GetPartOfString('aunt jemima', 'm', 'true', 'true')
            /// will return 'aunt je'
            /// GetPartOfString('aunt jemima', 'm', 'true', 'false')
            /// </summary>
            /// <param name="stream">
            /// The string from which to cull a portion of text
            /// </param>
            /// <param name="locateChar">
            /// The character or string that is the marker
            /// for which to grab text (from left or right depending
            /// on other argument)
            /// </param>
            /// <param name="firstInstance">
            /// Whether or not to get the substring from the first
            /// encountered instance of the locateChar argument
            /// </param>
            /// <param name="fromLeft">
            /// Whether to search from the left. If set to false,
            /// then the string will be searched from the right.
            /// </param>
            /// <param name="caseSensitive">
            /// Whether to consider case (upper/lower)
            /// </param>
            /// <returns>
            /// A portion of the input string, based on ensuing arguments
            /// </returns>
            public static string GetExactPartOfString(string stream, string locateChar,
                      bool firstInstance, bool fromLeft, bool caseSensitive)
            {
                try
                {
                    stream = stream.ToString();
                    string tempStream = string.Empty;
                    string tempLocateChar = string.Empty;
                    if (!caseSensitive)
                    { //case doesn't matter, convert to lower:
                        tempStream = stream.ToLower();
                        tempLocateChar = locateChar.ToLower();
                    }
                    //default charCnt to 1; for first inst of locateChar:
                    int charCount = 1;
                    if (firstInstance == false)
                        //get number of times char exists in string:
                        if (caseSensitive)
                            charCount = GetKeyCharCount(stream, locateChar);
                        else
                            charCount = GetKeyCharCount(tempStream, tempLocateChar);
                    //get position of first/last inst of char in str:
                    int position = 0;
                    if (caseSensitive)
                        position = GetCharPosition(stream, locateChar, charCount);
                    else
                        position = GetCharPosition(tempStream, tempLocateChar, charCount);
                    string result = string.Empty;
                    //chk that character exists in str:
                    if (position == -1)
                        result = string.Empty;
                    else
                    {
                        //char exists, proceed:
                        int streamLength = stream.Length;
                        if (fromLeft == true)
                            //return string from left:
                            result = stream.Substring(0, position);
                        else
                        {
                            //return string from right:
                            position += 1;
                            result = stream.Substring(position, streamLength - position);
                        }
                    }
                    return result;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return null;
                }
            }

            /// <summary>
            /// Returns the number of times, that the key character is found 
            /// in the stream string
            /// </summary>
            /// <param name="stream">
            /// string in which to locate key character
            /// </param>
            /// <param name="keyChar">
            /// key character: the string or char to count inside the stream
            /// </param>
            /// <returns>
            /// The number of times the string or char was located
            /// </returns>
            public static int GetKeyCharCount(string stream, string keyChar)
            {
                try
                {
                    string current;
                    int keyCount = 0;
                    for (int i = 0; i < stream.Length; i++)
                    {
                        current = stream.Substring(i, 1);
                        if (current == keyChar)
                            keyCount += 1;
                    }
                    if (keyCount <= 0)
                        return -1;
                    else
                        return keyCount;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return -1;
                }
            }


            /// <summary>
            /// Is CASE-SENSITIVE
            /// Returns x position of sChar in sstream, where x = iCharInst. 
            /// If: getCharPos('pineapple', 'p', 3) Then: 6 is returned
            /// </summary>
            /// <param name="stream">
            /// string in which to pinpoint the character (or string) position
            /// </param>
            /// <param name="charToPinpoint">character or string to locate</param>
            /// <param name="whichCharInstance">
            /// Number indicating WHICH instance of the character/string to locate
            /// </param>
            /// <returns>
            /// The index of the character or string found inside the input string.
            /// Will return -1 if the string/character is not found, or if the 
            /// instance number is not found
            /// </returns>
            public static int GetCharPosition(string stream, string charToPinpoint, int whichCharInstance)
            {
                try
                {
                    string current;
                    int keyCharCount = 0;
                    for (int i = 0; i < stream.Length; i++)
                    {
                        current = stream.Substring(i, 1);
                        //was BLOCKED SCRIPT sCurr = sstream.charAt(i);
                        if (current == charToPinpoint)
                        {
                            keyCharCount += 1;
                            if (keyCharCount == whichCharInstance)
                                return i;
                        }
                    }
                    return -1;
                }
                catch //(Exception ex)
                {
                    //ErrorTool.ProcessError(ex);
                    return -1;
                }
            }
        #endregion

        #region String And File
            /// <summary>
            /// Creates a dictionary containing all the words in the noise file
            /// </summary>
            /// <param Name="filePath">The full path of the noise file</param>
            /// <returns>Returns a hash Table</returns>
            public static Dictionary<string, bool> GetNoiseWords(string filePath)
            {

                System.IO.StreamReader noiseFile = System.IO.File.OpenText(filePath);
                string noiseWords = noiseFile.ReadToEnd();
                noiseFile.Close();
                noiseWords = noiseWords.Replace("\r\n", " ");
                while (noiseWords.IndexOf("  ") != -1)
                {
                    noiseWords = noiseWords.Replace("  ", " ");
                }
                noiseWords = noiseWords.Trim();
                noiseWords = noiseWords.Replace(" ", "|").ToLower();

                string[] noiseWordsArray = noiseWords.Split(new Char[] { '|' });

                Dictionary<string, bool> dict = new Dictionary<string, bool>();
                foreach (string word in noiseWordsArray)
                {
                    dict.Add(word, true);
                }

                return dict;

            }

            /// <summary>
            /// Removes illegal characters, such as ':' and '\' from filenames
            /// </summary>
            /// <param Name="filename">The original filename, excluding the path (can include file extension)</param>
            /// <returns>The filename where any illegal characters are replaced with underscores</returns>
            public static string RemoveIllegalCharactersFromFilename(string filename)
            {
                char[] invalidChars = new char[System.IO.Path.GetInvalidFileNameChars().Length + 4];
                invalidChars[0] = System.IO.Path.PathSeparator;
                invalidChars[1] = System.IO.Path.VolumeSeparatorChar;
                invalidChars[2] = System.IO.Path.AltDirectorySeparatorChar;
                invalidChars[3] = System.IO.Path.DirectorySeparatorChar;
                Array.Copy(System.IO.Path.GetInvalidFileNameChars(), 0, invalidChars, 4, System.IO.Path.GetInvalidFileNameChars().Length);

                foreach (char c in invalidChars)
                {
                    filename = filename.Replace(c, '_');
                }

                return filename;

            }

            /// <summary>
            /// Converts a number of bytes into a friendly Name, 
            /// such as 603 bytes, 14 KB, 1,048 MB etc
            /// </summary>
            public static string GetFriendlyFileSize(long sizeInBytes)
            {
                if (sizeInBytes < 1000)
                {
                    return sizeInBytes + " bytes";
                }
                if (sizeInBytes < 1000000)
                {
                    return (sizeInBytes / 1000) + " KB";
                }

                long temp = sizeInBytes / 10000;
                double megs = temp / 100.0;

                return megs.ToString("n") + " MB";

            }
        #endregion

        #region String And DateTime
            /// <summary>
            /// Gets the textual representation of a date in a "friendly" format, using the specified month and year formats. If the time is not midnight, 
            /// then the time is included
            /// </summary>
            public static string GetFriendlyDate(DateTime date, String monthFormat, String yearFormat)
            {
                string format = "d " + monthFormat + " " + yearFormat;
                if (!IsMidnight(date))
                {
                    format = "h:mmt\\m, " + format;
                }
                return date.ToString(format);
            }

            /// <summary>
            /// Gets, as text, the start and end date and time for a start date and end date 
            /// using the format strings for the month and year
            /// </summary>
            public static String GetFriendlyDateRange(DateTime? startDate, DateTime? endDate, string monthFormat, string yearFormat)
            {

                if (startDate == null)
                {
                    return "";
                }

                DateTime start = startDate.Value;
                DateTime end = endDate.Value;

                if (AreSameDay(start, end) || end == null)
                {

                    if (AreSameTime(start, end) || end == null)
                    {
                        if (IsMidnight(start))
                        {
                            return start.ToString("d " + monthFormat + " " + yearFormat);
                        }
                        else
                        {
                            return start.ToString("h:mmt\\m, d " + monthFormat + " " + yearFormat);
                        }
                    }
                    else
                    {
                        return start.ToString("h:mmt\\m") + " - " + end.ToString("h:mmt\\m") + ", " + start.ToString("d " + monthFormat + " " + yearFormat);
                    }

                }


                string t;

                if (IsMidnight(start))
                {
                    t = start.Day.ToString();
                    // if the start date is midnight, then we may be able to make the date more compact
                    // for example, 10 Dec 03 - 14 Dec 03 could be 10 - 14 Dec 03
                    if (start.Year == end.Year)
                    {
                        if (start.Month == end.Month)
                        {
                            // month and year are the same, so don't print anything
                        }
                        else
                        {
                            // just print the month as the years of the two dates are the same
                            t += start.ToString(" " + monthFormat);
                        }
                    }
                    else
                    {
                        // the years are different
                        t += start.ToString(" " + monthFormat + " " + yearFormat);
                    }
                }
                else
                {
                    t = start.ToString("h:mmt\\m, d " + monthFormat + " " + yearFormat);
                }

                t += " - ";

                if (!IsMidnight(end))
                {
                    t += end.ToString("h:mmt\\m") + ", ";
                }

                t += end.ToString("d " + monthFormat + " " + yearFormat);



                return t;

            }

            /// <summary>
            /// Check if date time is midnight
            /// </summary>
            /// <returns>Returns true if the time is midnight</returns> 
            /// <remarks>Ignores the seconds and milliseconds</remarks>
            public static bool IsMidnight(DateTime? date)
            {
                if (date == null)
                {
                    return false;
                }
                DateTime d = date.Value;
                return (d.Hour == 0 && d.Minute == 0);
            }

            /// <summary>
            /// Check if Same Day
            /// </summary>
            /// <returns >Returns true if 2 date times are on the same day</returns>
            public static bool AreSameDay(DateTime? date1, DateTime? date2)
            {
                if (date1 == null || date2 == null)
                {
                    return false;
                }
                DateTime d1 = date1.Value;
                DateTime d2 = date2.Value;
                return (d1.Year == d2.Year && d1.Month == d2.Month && d1.Day == d2.Day);
            }

            /// <summary>
            /// Check if same time
            /// </summary>
            /// <returns>Returns true if 2 date times have the same Hour and Minute values</returns> 
            public static bool AreSameTime(DateTime? date1, DateTime? date2)
            {
                if (date1 == null || date2 == null)
                {
                    return false;
                }
                DateTime d1 = date1.Value;
                DateTime d2 = date2.Value;
                return (d1.Hour == d2.Hour && d1.Minute == d2.Minute);
            }
        #endregion
    }
}
