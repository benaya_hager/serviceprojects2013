using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;


namespace Tools.CustomRegistry
{
    /// <summary>
    /// The class to perform all the read write methods in registry area
    /// </summary>
    public class cRegistryFunc
    {
#region Public Enums & Data types definitions
        public enum _REG_BASE_KEY_TYPES
        {
            HKEY_LOCAL_MACHINE = 0,
            HKEY_CURRENT_USER
        }
#endregion

#region Private data members & constants
        const String sBASEPATH = @"SOFTWARE\MEDINOL\";

        RegistryKey m_WorkingBaseKey = Registry.LocalMachine;
#endregion

#region Constructor & Destructor
        /// <summary>
        /// Base empty constructor. 
        /// Please do not use it directly from your code.
        /// </summary>
        public cRegistryFunc()
            : this("","", _REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE )
        {
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public cRegistryFunc(String inApplicationName, String inModuleName, _REG_BASE_KEY_TYPES inBaseRegistryKey)
        {
            ApplicationName = inApplicationName;
            BaseRegistryKey = inBaseRegistryKey; 
            String []tmp   = inModuleName.Split(',');
            ModuleName = tmp[0];

            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open base Tiles Registry Key
            try
            {
                //try to open key
                m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }

            //In case the key not opened try to create new key
            if (m_RegistryObject == null)
            {
                try
                {
                    m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath(), RegistryKeyPermissionCheck.Default);
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            //release object
            m_RegistryObject.Close();
            m_RegistryObject = null;

            //Create the sub Key if not exists
           //CreateSubKey(ModuleName);
        }
        
        /// <summary>
        /// Destructor
        /// </summary>
        ~cRegistryFunc()
        {
        }
#endregion

#region Private Functions
        /// <summary>
        /// Build the base key for current application
        /// </summary>
        /// <returns></returns>
        private String GetBaseKeyPath()
        {
            String Result = "";
            //Set the Application Identifier
            if (m_ApplicationName != "")
                Result = Tools.Misc.UtilsClass.EnsureTrailingSlash(sBASEPATH) + m_ApplicationName + "\\";
            else
                Result = sBASEPATH;

            //Set Module Identifier
            if (m_ModuleName != "")
                Result = Tools.Misc.UtilsClass.EnsureTrailingSlash(Result) + m_ModuleName + "\\";

            return Result;            
        }


        /// <summary>
        /// Create the sub key
        /// </summary>
        /// <param name="SubKeyName">The sub key name to create</param>
        /// <returns></returns>
        private void CreateSubKey(String SubKeyName)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open Registry key
            try
            {
                m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubKeyName + "\\", true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }
            //In case the key not opened try to create new key
            if (m_RegistryObject == null)
            {
                try
                {
                    m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath() + SubKeyName + "\\", RegistryKeyPermissionCheck.Default);
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            //release object
            m_RegistryObject.Close();
            m_RegistryObject = null;
        }
#endregion

#region Public functions
        /// <summary>
        /// Return STRING registry key value
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="sDefaultValue">Default return value</param>
        /// <returns>If successful return value is the value from registry, otherwise return the default value</returns>
        public String GetRegKeyValue(String SubCategory, String KeyName, String sDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;
            //Set return value to default value
            String sReturValue = sDefaultValue;
            
            //Open Registry key
            try
            {
                if (SubCategory !="")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory + "\\", true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() , true);

            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                if (m_RegistryObject.GetValue(KeyName) != null) // Key opend and data exist
                {
                    //Get key data                
                    sReturValue = m_RegistryObject.GetValue(KeyName  ).ToString();
                    //release object
                    m_RegistryObject.Close();
                }
                m_RegistryObject = null;
            }
            return sReturValue;

        }

        /// <summary>
        /// Set the registry STRING key value, if the key not exist the system automatically create new key
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="sNewDefaultValue">New value to store</param>
        public void SetRegKeyValue(String SubCategory, String KeyName, String sNewDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }
            if (m_RegistryObject == null)
                try
                {
                    if (SubCategory != "")
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath() + SubCategory);
                    else
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath());
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                m_RegistryObject.SetValue(KeyName, sNewDefaultValue);
                m_RegistryObject.Close();
            }
            m_RegistryObject = null;
        }

        /// <summary>
        /// Return DWORD registry key value
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lDefaultValue">Default return value</param>
        /// <returns>If successful return value is the value from registry, otherwise return the default value</returns>
        public Int32 GetRegKeyValue(String SubCategory, String KeyName, Int32 lDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;
            //Set return value to default value
            Int32  lReturValue = lDefaultValue;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                if (m_RegistryObject.GetValue(KeyName) != null) // Key opend and data exist
                {
                    //Get key data                
                    lReturValue = Convert.ToInt32(m_RegistryObject.GetValue(KeyName));
                    //release object
                    m_RegistryObject.Close();
                }
                m_RegistryObject = null;
            }
            return lReturValue;

        }

        /// <summary>
        /// Set the registry DWORD key value, if the key not exist the system automatically create new key
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lNewDefaultValue">New value to store</param>
        public void SetRegKeyValue(String SubCategory, String KeyName, Int32 lNewDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }
            if (m_RegistryObject == null)
                try
                {
                    if (SubCategory != "")
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath() + SubCategory);
                    else
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath());
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                m_RegistryObject.SetValue(KeyName, lNewDefaultValue);
                m_RegistryObject.Close();
            }
            m_RegistryObject = null;
        }

        /// <summary>
        /// Return DWORD registry key value
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lDefaultValue">Default return value</param>
        /// <returns>If successful return value is the value from registry, otherwise return the default value</returns>
        public Byte GetRegKeyValue(String SubCategory, String KeyName, Byte lDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;
            //Set return value to default value
            byte lReturValue = lDefaultValue;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                if (m_RegistryObject.GetValue(KeyName) != null) // Key opend and data exist
                {
                    //Get key data                
                    lReturValue = Convert.ToByte(m_RegistryObject.GetValue(KeyName));
                    //release object
                    m_RegistryObject.Close();
                }
                m_RegistryObject = null;
            }
            return lReturValue;

        }

   
        /// <summary>
        /// Set the registry DWORD key value, if the key not exist the system automatically create new key
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lNewDefaultValue">New value to store</param>
        public void SetRegKeyValue(String SubCategory, String KeyName, Byte lNewDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }
            if (m_RegistryObject == null)
                try
                {
                    if (SubCategory != "")
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath() + SubCategory);
                    else
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath());
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                m_RegistryObject.SetValue(KeyName, lNewDefaultValue);
                m_RegistryObject.Close();
            }
            m_RegistryObject = null;
        }




        /// <summary>
        /// Return DWORD registry key value
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lDefaultValue">Default return value</param>
        /// <returns>If successful return value is the value from registry, otherwise return the default value</returns>
        public Double GetRegKeyValue(String SubCategory, String KeyName, Double lDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;
            //Set return value to default value
            Double  lReturValue = lDefaultValue;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                if (m_RegistryObject.GetValue(KeyName) != null) // Key opened and data exist
                {
                    //Get key data                
                    lReturValue = Convert.ToDouble(m_RegistryObject.GetValue(KeyName, lDefaultValue ));
                    //release object
                    m_RegistryObject.Close();
                }
                m_RegistryObject = null;
            }
            return lReturValue;

        }

        /// <summary>
        /// Set the registry DWORD key value, if the key not exist the system automatically create new key
        /// </summary>
        /// <param name="SubCategory">SubKey Name</param>
        /// <param name="KeyName">Key name</param>
        /// <param name="lNewDefaultValue">New value to store</param>
        public void SetRegKeyValue(String SubCategory, String KeyName, Double lNewDefaultValue)
        {
            //The reference to object of Microsoft registry
            RegistryKey m_RegistryObject = null;

            //Open Registry key
            try
            {
                if (SubCategory != "")
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath() + SubCategory, true);
                else
                    m_RegistryObject = m_WorkingBaseKey.OpenSubKey(GetBaseKeyPath(), true);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught.", e);
            }
            if (m_RegistryObject == null)
                try
                {
                    if (SubCategory != "")
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath() + SubCategory);
                    else
                        m_RegistryObject = m_WorkingBaseKey.CreateSubKey(GetBaseKeyPath());
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, e.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("{0} Exception caught.", e);
                }

            //If current key exist in registry
            if (m_RegistryObject != null)
            {
                m_RegistryObject.SetValue(KeyName, lNewDefaultValue);
                m_RegistryObject.Close();
            }
            m_RegistryObject = null;
        }


        public Boolean GetRegKeyValue(String SubCategory, String KeyName, Boolean lDefaultValue)
        {
            Int32 res = (Int32)GetRegKeyValue(SubCategory, KeyName, (Int32)(lDefaultValue == true ? 1 : 0));
            return res == 1 ? true : false;
        }

        public void SetRegKeyValue(String SubCategory, String KeyName, Boolean lNewDefaultValue)
        {
            SetRegKeyValue(SubCategory, KeyName, (Int32)(lNewDefaultValue == true ? 1 : 0));
        }









        public static String BuildPropertyName(String BasePropertyName, String m_MyComponentName)
        {
            return BasePropertyName + " % " + m_MyComponentName + " % ";
        }

        


        /// <summary>
        /// Get base data from registry
        /// </summary>
        public static cRegistryFunc GetRoootRegistryKey()
        {
            try
            {
                //Initialize application registry data
                return new cRegistryFunc(System.Windows.Forms.Application.ProductName,
                                         System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                         cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);
            }
            catch { return null; }

        }




        public static  float GetRegistryFields(cRegistryFunc regRootKey,String component_name, string sub_key_name, float default_value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            return Convert.ToSingle(regRootKey.GetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), default_value));
        }

        public static  void SetRegistryFields(cRegistryFunc regRootKey, String component_name, string sub_key_name, float value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            regRootKey.SetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), value);
        }


        public static  String GetRegistryFields(cRegistryFunc regRootKey, String component_name, string sub_key_name, String default_value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            return Convert.ToString(regRootKey.GetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), default_value));
        }

        public static  void SetRegistryFields(cRegistryFunc regRootKey, String component_name, string sub_key_name, String value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            regRootKey.SetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), value);
        }


        public static long GetRegistryFields(cRegistryFunc regRootKey, String component_name, string sub_key_name, long default_value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            return Convert.ToInt64(regRootKey.GetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), default_value));
        }

        public static void SetRegistryFields(cRegistryFunc regRootKey, String component_name, string sub_key_name, long value)
        {
            if (null == regRootKey)
                regRootKey = GetRoootRegistryKey();
            regRootKey.SetRegKeyValue("", BuildPropertyName(sub_key_name, component_name), value);
        }



        public static  void UpdateRegistryValue(string Key_Name, String Value)
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc(System.Windows.Forms.Application.ProductName,
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

                objRegistry.SetRegKeyValue("", Key_Name, Value);
            }
            catch { }

        }

        public static  void UpdateRegistryValue(string Key_Name, double Value)
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc(System.Windows.Forms.Application.ProductName,
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

                objRegistry.SetRegKeyValue("", Key_Name, Value);
            }
            catch { }

        }

        public static  String GetRegistryValue(string Key_Name, String default_value)
        {
            String result = "";
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc(System.Windows.Forms.Application.ProductName,
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);


                result = objRegistry.GetRegKeyValue("", Key_Name, default_value);
            }
            catch { }
            return result;
        }

        public static  decimal GetRegistryValue(string Key_Name, float default_value)
        {
            Double result = 0;
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc(System.Windows.Forms.Application.ProductName,
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);


                result = objRegistry.GetRegKeyValue("", Key_Name, default_value);
            }
            catch { }
            return (Decimal)result;
        }



#endregion

#region Public Properties
        private String m_ApplicationName = "";
        /// <summary>
        /// Store whitch different SubKey name for each application
        /// </summary>
        public String  ApplicationName
        {
            get { return m_ApplicationName; }
            set { m_ApplicationName = value;}
        }

        _REG_BASE_KEY_TYPES m_BaseRegistryKey;
        public _REG_BASE_KEY_TYPES BaseRegistryKey 
        { 
            get { return m_BaseRegistryKey; }
            set 
            {
                m_BaseRegistryKey = value;
                switch (m_BaseRegistryKey)
                {                    
                    case _REG_BASE_KEY_TYPES.HKEY_CURRENT_USER:
                    {
                        m_WorkingBaseKey = Registry.CurrentUser;
                        break;
                    }
                    case _REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE:
                    {
                        m_WorkingBaseKey = Registry.LocalMachine;
                        break;
                    }
                }
                
            }
        }

        String m_ModuleName = "";
        public String ModuleName 
        {
            get { return m_ModuleName;   }
            set { m_ModuleName = value;  } 
        }
#endregion
    }
}
