﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace Tools.Excel
{
    class ExportToExcel
    {
    }


    /// <summary>
    /// Vertical alignment styles used for Spreadsheet.CellStyle.VerticalAlignment.
    /// </summary>   
    public enum VerticalAlignmentStyle
    {
        /// <summary>
        /// Top alignment.
        /// </summary>   
        Top = 0,
        /// <summary>
        /// Center alignment.
        /// </summary>    
        Center = 1,
        /// <summary>
        /// Bottom alignment.
        /// </summary>     
        Bottom = 2,
        /// <summary>
        /// Justify alignment.
        /// </summary>    
        Justify = 3,
        /// <summary>
        /// Distributed alignment.
        /// </summary>
        Distributed = 4,
    }

    /// <summary>
    /// Horizontal alignment styles used for Spreadsheet.CellStyle.HorizontalAlignment.
    /// </summary>
    public enum HorizontalAlignmentStyle
    {
        /// <summary>
        /// Aligns data depending on the data type (text, number, etc.). 
        /// Consult Microsoft Excel documentation.
        /// </summary>   
        General = 0,
        /// <summary>
        ///  Left alignment.
        /// </summary>
        Left = 1,
        /// <summary>
        /// Center alignment.
        /// </summary>
        Center = 2,
        /// <summary>
        /// Right alignment.
        /// </summary>   
        Right = 3,
        /// <summary>
        /// Fill alignment repeats cell data to fill the whole cell.
        /// </summary>
        Fill = 4,
        /// <summary>
        /// Justify alignment.
        /// </summary>
        Justify = 5,
        /// <summary>
        /// Centered across selection. Multiple cells can be selected but only one should 
        /// have value for this alignment to have effect.
        /// </summary>  
        CenterAcross = 6,
        /// <summary>
        /// Distributed alignment.
        /// </summary>
        Distributed = 7,
    }
}
