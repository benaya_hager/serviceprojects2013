﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tools.UI_Messages
{
    public class ApplicationMessage
    {

        protected enum MessageType
        {
            ERROR_MESSAGE = 0x1,
            WARNING_MESSAGE = 0x2,
            QUESTION_MESSAGE = 0x4,
            QUESTION_WITH_CANCEL_MESSAGE = 0x8,
            INFO_MESSAGE = 0x10
        }

        protected class Msg
        {
            //protected MessageType m_MessageType;
            protected String m_MessageText;
            protected System.Windows.Forms.MessageBoxButtons m_MessageButtons;
            protected System.Windows.Forms.MessageBoxIcon m_MessageIcon;
            protected String m_MessageCaption;

#region Constructors
            /// Base constructor, create message with default caption text
            /// </summary>
            /// <param name="msgText">The message text</param>
            /// <param name="msgType">The message display type</param>
            public Msg(String msgText, MessageType msgType) :
                this(msgText, msgType, "Message")
            {
                
            }

            /// <summary>
            /// Base message constructor
            /// </summary>
            /// <param name="msgText">The message text</param>
            /// <param name="msgType">The message display type</param>
            /// <param name="msgCaption">The message caption text</param>
            public Msg(String msgText, MessageType msgType, String msgCaption)
            {
                m_MessageText = msgText;
                switch (msgType)
                {
                    case MessageType.ERROR_MESSAGE:
                    {
                        this.m_MessageButtons = MessageBoxButtons.OK;
                        this.m_MessageIcon = MessageBoxIcon.Error;
                        this.m_MessageCaption = "Error";
                        break;
                    }
                    case MessageType.QUESTION_MESSAGE:
                    {
                        this.m_MessageButtons  = MessageBoxButtons.YesNo;
                        this.m_MessageIcon = MessageBoxIcon.Question;
                        this.m_MessageCaption = "Question";
                        break;
                    }
                    case MessageType.QUESTION_WITH_CANCEL_MESSAGE:
                    {
                        this.m_MessageButtons  = MessageBoxButtons.YesNoCancel;
                        this.m_MessageIcon = MessageBoxIcon.Question;
                        this.m_MessageCaption = "Question";
                        break;
                    }
                    case MessageType.WARNING_MESSAGE:
                    {
                        this.m_MessageButtons  = MessageBoxButtons.OK;
                        this.m_MessageIcon = MessageBoxIcon.Exclamation;
                        this.m_MessageCaption = "Warning";
                        break;
                    }
                    case MessageType.INFO_MESSAGE:
                    {
                        this.m_MessageButtons = MessageBoxButtons.OK;
                        this.m_MessageIcon = MessageBoxIcon.Information;
                        this.m_MessageCaption = "Information";
                        break;
                    }
                }            
                if(!msgCaption.Equals("Message")) m_MessageCaption = msgCaption;
            }
#endregion


            public System.Windows.Forms.DialogResult Show()
            {
                return this.Show(null);
            }

            public System.Windows.Forms.DialogResult Show(String messageUserValue)
            {
                String myText = this.m_MessageText;
                if (messageUserValue != null)
                    myText = myText + " " + messageUserValue;


                return MessageBox.Show(myText, m_MessageCaption, m_MessageButtons, m_MessageIcon);
            }

            /// <summary>
            /// Replaces the format item in a specified String with the text equivalent of the value of a 
            /// corresponding Object instance in a specified array. 
            /// </summary>
            /// <param name="args">An Object array containing zero or more objects to format. </param>
            /// <returns></returns>
            public System.Windows.Forms.DialogResult ShowFormated(params Object[] args)
            {
                String myText = String.Format(this.m_MessageText, args); 
                return MessageBox.Show(myText, m_MessageCaption, m_MessageButtons, m_MessageIcon);
            }            
        }

        protected  static ApplicationMessage m_Instance;
        /// <summary>
        /// Store the list of preloaded messages
        /// </summary>
        protected Dictionary<String, Msg> m_MessagesList; 

        /// <summary>
        /// protected base constructor. The class simulates the single tone structure
        /// </summary>
        protected ApplicationMessage()
        { 
            m_MessagesList = new Dictionary<String, Msg>();
            LoadMessages();
        }

        virtual public void LoadMessages()
        {
            m_MessagesList.Add("DELETE_THIS_RECORD", new Msg("Are you sure you want to delete this record?", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("DELETE_THOUS_RECORDS", new Msg("Are you sure you want to delete thous records?", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("RECORD_DELETED", new Msg("Record has been deleted", MessageType.INFO_MESSAGE));
            m_MessagesList.Add("RECORD_UPDATED", new Msg("Record has been updated", MessageType.INFO_MESSAGE));
            m_MessagesList.Add("RECORD_CREATED", new Msg("New record has been created", MessageType.INFO_MESSAGE));
            m_MessagesList.Add("USER_INACTIVE", new Msg("Cannot log in the system, user is inactive! ", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("VALUE_!EXIST", new Msg("Value does not exist! Would you like to add it now?", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("SAVE_CHANGES", new Msg("Save changes?", MessageType.QUESTION_WITH_CANCEL_MESSAGE));
            m_MessagesList.Add("USE_SELECTED", new Msg("Use Selected Value ", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("WRONG_VALUE", new Msg("Wrong value ", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("DELETE_RECORD", new Msg("Are you sure you want to delete the record?", MessageType.QUESTION_MESSAGE ));
            m_MessagesList.Add("ERROR_RECORD_DELETING", new Msg("Record cannot be deleted!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("DUPLICATED_VALUE", new Msg("Cannot save duplicate value!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("ERROR_UPDATE", new Msg("Cannot update!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("PASSWORD_CHANGED", new Msg("Password succesfully changed", MessageType.INFO_MESSAGE ));
            m_MessagesList.Add("PASSWORD_!MATCH", new Msg("Password and confirm password do not match!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("ERROR_LOGIN", new Msg("The username and password pair is incorrect!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("FOLDER_!EXIST", new Msg("Folder does not exist!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("ERROR_CHANGING_KEY", new Msg("Cannot change value that used for identification in other parts of the system!", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("USE_SELECTION?", new Msg("Use selection?", MessageType.QUESTION_MESSAGE ));
            m_MessagesList.Add("DATA_CLASS_MISSING", new Msg("Contact your application administrator, data class is missing - error! ", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("NO_RECORD_SELECTED", new Msg("No record selected!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("PRIORITY_LOAD_FAILED", new Msg("Loading from Priority failed!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("SQL_CONNECTION_FAILED", new Msg("Could not connect to SQL server!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("ERROR_FORM_CLOSING", new Msg("Cannot close form while in progress!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("CHANGES_SAVED", new Msg("Changes succesfully saved", MessageType.INFO_MESSAGE ));
            m_MessagesList.Add("FILE_EXIST", new Msg("The file allready exist, do you want to overwrite?", MessageType.QUESTION_MESSAGE , "Warning!!!"));
            m_MessagesList.Add("RECORD_REMOVED", new Msg("Record succesfully removed", MessageType.INFO_MESSAGE ));
            m_MessagesList.Add("SEARCH_RESULT_ERROR", new Msg("Could not get search results! Possible reason is network problem. Please try again.", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("ADD_ITEM_TO_FOLDER", new Msg("Do you wish to use selected value?", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("AXIS_CONSTRAINT_FOLDER2AXIS_ERROR", new Msg("Cannot remove axis number {0:D} because it's placed on folder {0:D}", MessageType.WARNING_MESSAGE));
            m_MessagesList.Add("BLADE_CONSTRAINT_FOLDER2BLADES_ERROR", new Msg("Cannot remove blade number {0:D} because it's placed on folder {1:D}", MessageType.WARNING_MESSAGE));
            m_MessagesList.Add("NO_FOLDER_SELECTED_ERROR", new Msg("Cannot Add/Remove Blade/Axis/Configurations while Folder not selected!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("FOLDER_BLADES_OR_AXIS_MISSING_ERROR", new Msg("Axis or Blade is not installed on current folder!", MessageType.ERROR_MESSAGE ));
            m_MessagesList.Add("BLADE_OR_AXIS_SELECTION_MISSING", new Msg("Blade or Axis selection is missing!", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("APPLY_SELECTED_CONFIGURATION", new Msg("Selected Axis is {0:D} and selected Blade is {1:D}. \n\r Press Yes to Apply selection No to continue. \n\r", MessageType.QUESTION_MESSAGE, "Warning!!!"));
            m_MessagesList.Add("NO_FREE_BLADES_OR_AXISES_FOUND_ERROR", new Msg("No axes or blades are available to create a new pair", MessageType.INFO_MESSAGE, "Warning!!!"));
            m_MessagesList.Add("CALLIBRATION_GROUP_ALLREADY_EXISTS", new Msg("Can't add new callibration group. \n\rCallibration group allready exists, for callibration please select from the list.", MessageType.ERROR_MESSAGE , "Warning!!!"));
            m_MessagesList.Add("NO_ITEM_SELECTED_4_CONFIGURATION", new Msg("No item selected 4 configuration!", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("AXIS_POSITION_NOT_SET_TO_ZERO", new Msg("Please move axis position to home position before you start new test.", MessageType.ERROR_MESSAGE));
            m_MessagesList.Add("IO_INTERRUPT_OCCURRED", new Msg("A Severe security failure has occurred. Continue ?", MessageType.QUESTION_MESSAGE));
            m_MessagesList.Add("ABORT_ALL_ROUTINES", new Msg("Abort all routines ?", MessageType.QUESTION_MESSAGE));
        }

        public static System.Windows.Forms.DialogResult Show(String msgKey)
        {
            return Show(msgKey,null);
        }         
        public static System.Windows.Forms.DialogResult Show(String msgKey ,String messageUserValue) 
        {
            if (m_Instance==null) 
                m_Instance = new ApplicationMessage();
            
            return m_Instance.m_MessagesList[msgKey].Show(messageUserValue);
        }

        /// <summary>
        /// Show message as it
        /// </summary>
        /// <param name="msgText"></param>
        /// <param name="bShowAsIt"></param>
        /// <returns></returns>
        public static System.Windows.Forms.DialogResult Show(String msgText, Boolean bShowAsIt)
        {
            if (bShowAsIt)
            {
                return (new Msg(msgText, MessageType.ERROR_MESSAGE)).Show();  
            }
            else
                return Show(msgText, null);
        }         


        /// <summary>
        /// Shows formated message based on predefined string
        /// </summary>
        /// <param name="msgKey">Predefined key reference</param>
        /// <param name="args">An Object array containing zero or more objects to format.</param>
        /// <returns></returns>
        public static System.Windows.Forms.DialogResult ShowFormated(String msgKey, params Object[] args)
        {
            if (m_Instance == null)
                m_Instance = new ApplicationMessage();

            return m_Instance.m_MessagesList[msgKey].ShowFormated(args);
        }

        public static System.Windows.Forms.DialogResult ShowMessage(String text , String title)
        {
            return Show( text, title,MessageBoxIcon.Error);
        }

        public static System.Windows.Forms.DialogResult ShowMessage(String text, String title, MessageBoxButtons buttonsType, MessageBoxIcon iconType)
        {
            return Show(text, title, buttonsType, iconType );
        }

        public static System.Windows.Forms.DialogResult Show(String text, String title,  System.Windows.Forms.MessageBoxIcon iconType)
        {
            if (m_Instance == null) m_Instance = new ApplicationMessage();
            return MessageBox.Show(text, title, MessageBoxButtons.OK, iconType);
        }

        public static System.Windows.Forms.DialogResult Show(String text, String title, MessageBoxButtons buttonsType, System.Windows.Forms.MessageBoxIcon iconType)
        {
            if (m_Instance == null) m_Instance = new ApplicationMessage();
            return MessageBox.Show(text, title,  buttonsType, iconType);
        }
    }
}
