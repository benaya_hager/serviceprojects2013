﻿using System;
using System.Linq;

namespace ExtensionMethods
{
    public static class StringExtensions
    {
        public static string ExtractNumber(this string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }


        /// <summary>
        /// Extracts the number in a string of the format "string number" like "g200 1.878"
        /// </summary>
        /// <param name="expr"></param>
        /// <returns>The Extracted number</returns>
        public static string ExtractZumbachMeasure(this string expr)
        {
            string[] stringArray;

            stringArray = expr.Split(' ');

            if (stringArray.Count() >= 2)
            {
                return stringArray[1];
            }

            return "0";
        }

        /// <summary>
        /// Cuts <paramref name="numOfDigitsAfterThePoint"/> from the given number in a string format.
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="numOfDigitsAfterThePoint"></param>
        /// <returns></returns>
        public static string RemoveTrailingDigits(this string expr, int numOfDigitsAfterThePoint)
        {
            return Double.Parse(expr).ToString("f" + numOfDigitsAfterThePoint);
        }

        public static string ReverseString(this string expr)
        {
            string tempstr = "";

            char[] tempAr = expr.ToCharArray();

            for (int i = tempAr.Length - 1; i >= 0; i--)
                tempstr += tempAr[i].ToString();

            return tempstr;
        }

        /// <summary> Convert a string of hex digits (ex: E4 CA B2) to a byte array. </summary>
        /// <param name="s"> The string containing the hex digits (with or without spaces). </param>
        /// <returns> Returns an array of bytes. </returns>
        public static byte[] HexStringToByteArray(this string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        /// <summary> Convert a string of hex digits (ex: E4 CA B2) to a byte array. </summary>
        /// <param name="s"> The string containing the hex digits (with or without spaces). </param>
        /// <returns> Returns an array of bytes. </returns>
        public static byte[] HexStringToByteArrayWithEndLine(this string s)
        {
            int i;
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2 + 2];
            for (i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            buffer[i / 2] = (byte)'\r';
            buffer[i / 2 + 1] = (byte)'\n';
            return buffer;
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {

            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                double res;
                if (Expression is string)
                {
                    if (!Double.TryParse(Expression as string, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out res))
                        return false;
                    else
                        if (!Double.TryParse(Expression.ToString(), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out res))
                            return false;
                    return true;
                }
            }
            catch { } // just dismiss errors but return false
            return false;
        }
    }

}