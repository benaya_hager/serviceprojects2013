﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

using System.Windows;
using Microsoft.Practices.Unity;
using NS_Common.ApplicationExtensions;
using Routines_Controller_Test_Application.Component_Builders;
using Routines_Controller_UI.View;
using Routines_Controller_UI;

namespace Routines_Controller_Test_Application
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            ComponentsBuilder builder = new ComponentsBuilder();

            builder.Bulid(this.GetContainer());


            //builder.Container.Resolve<RoutinesListPage>().Show();

            builder.Container.Resolve<MainWindow>().Show();
        } 
    }
}
