#if _WIN32
	using Motion_Controller_Common.TML._32_BIT;
#else
	using Motion_Controller_Common.TML._64_BIT;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NS_Common.ViewModels.Common;
using NS_Common.Error_Handling;
using SFW;
using NS_Common;
using Motion_Controller_Common.Interfaces;
using Motion_Controller_Common.Service;

namespace NS_MotionController.Axis
{
	//#region Single Axis description class

	/// <summary>
	/// Single axes description class
	/// </summary>
    //public class cAxis : ViewModelBase, IIODevice, IBaseAxis
    //{

        //    #region Private members

        //    private IMotionController m_MotionController;

        //    private const int NO_CHANGE_WAITING_TIME = 15;
        //    private const int FAILED_TO_SELECT_AXIS_TIMES = 15;
        //    private const int POSITION_CHECK_LOOP_TIME = 50;
        //    private const int SIMULATION_STEPS_COUNT = 5;
        //    private const int CONTINUOUS_MOVE_STEP = 10000;

        //    private Thread m_PosMonitoringThread = null;

        //    #region Simulator Definitions

        //        private System.Timers.Timer tmrMovementSimulation = null;
        //        private Int32 m_SimulatorDestinationPosition = 0;
        //        private Int32 m_SimulatorStep = 0;
        //        private Int32 m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;
        //        private Dictionary<String, Int16> m_SimulatorINT16VariablesList = null;
        //        private Dictionary<String, Double> m_SimulatorDoubleVariablesList = null;

        //    #endregion


        //    private long m_LastMotorPosition = 0;

        //    private Boolean m_ReadPositionThreadActive = false;
        //    private Int32 m_SuspendPosMonitoringThreadTimeout = 100;

        //    #endregion Private members

        //    #region Public properties

        //    #region Axis Setup Parameters

        //    private int m_LoadedSetupID = -1;
        //    public int LoadedSetupID
        //    {
        //        get { return m_LoadedSetupID; }
        //        set { m_LoadedSetupID = value; }
        //    }

        //    private String m_SetupFileName = "";
        //    /// <summary>
        //    /// File name created in EasyMotion Studio, used to initialize the Axis basic information
        //    /// </summary>
        //    public String SetupFileName
        //    {
        //        get { return m_SetupFileName; }
        //        set { m_SetupFileName = value; }
        //    }

        //    /// <summary>
        //    /// Stores the value of the Status Register Low from Axis
        //    /// </summary>
        //    private UInt16 m_AxisOn_flag = 0;
        //    /// <summary>
        //    /// Used to determinate if the power is on for current Axes
        //    /// </summary>
        //    public UInt16 AxisOn_flag
        //    {
        //        get { return m_AxisOn_flag; }
        //        set { m_AxisOn_flag = value; }
        //    }

        //    private double m_ResetWaitPeriod = 0.1;
        //    /// <summary>
        //    /// Store the timeout for how long to keep the Axis power off state on reset event
        //    /// </summary>
        //    public double ResetWaitPeriod
        //    {
        //        get { return m_ResetWaitPeriod; }
        //        set { m_ResetWaitPeriod = value; }
        //    }

        //    private Int32 m_Axis_StartPosition;
        //    /// <summary>
        //    /// Set/Get the home position for current Axis.
        //    /// May not be in use if the Home routine is working properly.
        //    /// </summary>
        //    public Int32 Axis_StartPosition
        //    {
        //        get { return m_Axis_StartPosition; }
        //        set { m_Axis_StartPosition = value; }
        //    }

        //    private Byte m_NetworkIdentifier;
        //    /// <summary>
        //    /// The Axis Network Identifier
        //    /// </summary>
        //    public Byte NetworkIdentifier
        //    {
        //        get { return m_NetworkIdentifier; }
        //        set { m_NetworkIdentifier = value; }
        //    }

        //    private String m_Target_Position_Variable_Name = "";
        //    /// <summary>
        //    /// Set/Get the value indicate if current motor target position parameter name
        //    /// </summary>
        //    public String Target_Position_Variable_Name
        //    {
        //        get { return m_Target_Position_Variable_Name; }
        //        set { m_Target_Position_Variable_Name = value; }
        //    }

        //    private String m_ActualPositionVariableName = "APOS";
        //    /// <summary>
        //    /// Get/Set the name of technosoft variable name for current position check
        //    /// </summary>
        //    public String ActualPositionVariableName
        //    {
        //        get { return m_ActualPositionVariableName; }
        //        set { m_ActualPositionVariableName = value; }
        //    }

        //    #endregion

        //    #region Units Conversation Properties

        //    private double m_Millimeter_To_IU_Coeff; // = TMLLibDefs.MILLIMETER_TO_IO_COEF;
        //    /// <summary>
        //    /// Get multiplier for current Axes translation from mm to drive/motor Internal Units.
        //    /// Used to calculate encoder index position.
        //    /// </summary>
        //    public double Millimeter_To_IU_Coeff
        //    {
        //        get { return m_Millimeter_To_IU_Coeff; }
        //        set { m_Millimeter_To_IU_Coeff = value; }
        //    }

        //    private double m_MillimeterSecond_To_IU_SlewCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_SLEW_COEF;
        //    /// <summary>
        //    /// Get multiplier for current Axes translation from mm/s to drive/motor Internal Units.
        //    /// Used to calculate drive/motor slew speed.
        //    /// </summary>
        //    public double MillimeterSecond_To_IU_SlewCoeff
        //    {
        //        get { return m_MillimeterSecond_To_IU_SlewCoeff; }
        //        set { m_MillimeterSecond_To_IU_SlewCoeff = value; }
        //    }

        //    private double m_MillimeterSecond_To_IU_AccelerationCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_ACCELERATION_COEF;
        //    /// <summary>
        //    /// Get multiplier for current Axes translation from mm/s^2 to drive/motor Internal Units.
        //    /// Used to calculate drive/motor Acceleration speed.
        //    /// </summary>
        //    public double MillimeterSecond_To_IU_AccelerationCoeff
        //    {
        //        get { return m_MillimeterSecond_To_IU_AccelerationCoeff; }
        //        set { m_MillimeterSecond_To_IU_AccelerationCoeff = value; }
        //    }

        //    private double m_MillimeterSecond_To_IU_VelocityCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_VELOCITY_COEF;
        //    /// <summary>
        //    /// Get multiplier for current Axes translation from mm/s to drive/motor Internal Units.
        //    /// Used to calculate drive/motor velocity speed.
        //    /// </summary>
        //    public double MillimeterSecond_To_IU_VelocityCoeff
        //    {
        //        get { return m_MillimeterSecond_To_IU_VelocityCoeff; }
        //        set { m_MillimeterSecond_To_IU_VelocityCoeff = value; }
        //    }

        //    #endregion

        //    private String m_AxisName = "";
        //    /// <summary>
        //    /// Optional parameter, that makes able to give names to each Axis
        //    /// By default is Empty String value
        //    /// </summary>
        //    public String AxisName
        //    {
        //        get { return m_AxisName; }
        //        set { m_AxisName = value; }
        //    }

        //    private int m_ActualPositionIU = 0;
        //    /// <summary>
        //    /// Store the actual position of Axis in Technosoft Units
        //    /// </summary>
        //    public int ActualPositionIU
        //    {
        //        get
        //        {
        //            if (!this.IsAxisInSimulatorMode)
        //            {
        //                if (m_MotionController.ActivateAxis(this))
        //                {
        //                    try
        //                    {
        //                        TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out m_ActualPositionIU);
        //                    }
        //                    catch { }
        //                    finally
        //                    {
        //                        m_MotionController.DeactivateAxis();
        //                    }
        //                }
        //                return m_ActualPositionIU;
        //            }
        //            else
        //                return m_ActualPositionIU;
        //        }
        //        set
        //        {
        //            if (!this.IsAxisInSimulatorMode)
        //            {
        //                throw new Exception("The usage of Set Actual position enabled only in simulation mode.");
        //            }
        //            m_ActualPositionIU = value;
        //        }
        //    }

        //    public Double ActualPositionMM
        //    {
        //        get { return this.IU2MM(this.ActualPositionIU); }
        //    }

        //    /// <summary>
        //    /// Return last taken motor position.
        //    /// Based on last call to get ActualPositionIU.
        //    /// The return value is in Technosoft Units.
        //    /// </summary>
        //    public int LastMotorPosition
        //    {
        //        get
        //        {
        //            return m_ActualPositionIU;
        //        }
        //    }

        //    /// <summary>
        //    /// Indicate that currently the axis is in motion
        //    /// </summary>
        //    public bool InMotion
        //    {
        //        get { return ; }

        //    }

        //    private Boolean m_IsAxisInSimulatorMode = false;
        //    /// <summary>
        //    /// Get/Set the value indicate if current axis was initialized in Simulation mode.
        //    /// </summary>
        //    public Boolean IsAxisInSimulatorMode
        //    {
        //        get { return m_IsAxisInSimulatorMode; }
        //        set { m_IsAxisInSimulatorMode = value; }
        //    }

        //    private Boolean m_AxisMemoryIncreased = false;
        //    /// <summary>
        //    /// Get/Set the value indicate if "execute_before_movement" function was started,
        //    /// but the appositive function "execute_after_movement" is still not used
        //    /// </summary>
        //    public Boolean AxisMemoryIncreased
        //    {
        //        get { return m_AxisMemoryIncreased; }
        //        set
        //        {
        //            if (m_MotionController.ActivateAxis(this))
        //            {
        //                try
        //                {
        //                    if (value == true && m_AxisMemoryIncreased == false)
        //                    {
        //                        if (TMLLib.TS_CALL_Label("execute_before_movement")) //(0x406A))
        //                        {
        //                            m_AxisMemoryIncreased = true;
        //                        }
        //                    }
        //                    else if (m_AxisMemoryIncreased == true)
        //                    {
        //                        if (TMLLib.TS_CALL_Label("execute_after_movement"))
        //                        {
        //                            m_AxisMemoryIncreased = false;
        //                        }
        //                    }
        //                }
        //                catch
        //                {
        //                }
        //                finally
        //                {
        //                    m_MotionController.DeactivateAxis();
        //                }
        //            }
        //        }
        //    }

        //    public Int32 SimulatorDestinationPosition
        //    { get { return m_SimulatorDestinationPosition; } }

        //    private Single m_LastDestinationPositionIU = 0;
        //    public Single LastDestinationPositionIU
        //    {
        //        get { return m_LastDestinationPositionIU; }
        //        set { m_LastDestinationPositionIU = value; }
        //    }

        //    #endregion Public properties

        //    #region Constructors

        //    /// <summary>
        //    /// Base empty constructor, all the members initialize to their base values
        //    /// </summary>
        //    public cAxis(IMotionController motion_cntrl)
        //    {
        //        m_MotionController = motion_cntrl;

        //        //	Register the callback function with the TML_lib.DLL
        //        //TMLLib.TS_RegisterHandlerForUnrequestedDriveMessages(UnregEventsHandler);
        //        //	Register the callback function with the TML_lib.DLL
        //    }

        //    #endregion Constructors

        //    #region Public Methods

        //    public override string ToString()
        //    {
        //        return m_AxisName;
        //    }

        //    /// <summary>
        //    /// Create new copy of SingleAxis object
        //    /// </summary>
        //    /// <param name="NewSingleAxis">
        //    /// Base SingleAxis object, that copy all data members to newly created object
        //    /// </param>
        //    public IBaseAxis Clone(IBaseAxis NewSingleAxis)
        //    {
        //        if (m_PosMonitoringThread != null)
        //        {
        //            if ((m_PosMonitoringThread.ThreadState & (ThreadState.Unstarted | ThreadState.Stopped | ThreadState.Suspended)) == 0)
        //            {
        //                ExceptionsList.AxisPosMonitoringStarted.MessageText = "The logging watcher thread is currently running for Axis " + m_AxisName + ", to copy you have to stop watching thread using StopLogging function";
        //                ExceptionsList.AxisPosMonitoringStarted.ErrorID = ErrorCodesList.AXIS_LOGGER_THREAD_STARTED;
        //                throw ExceptionsList.AxisPosMonitoringStarted;
        //            }
        //        }

        //        if (m_AxisPositionWatcherThread != null)
        //        {
        //            if ((m_AxisPositionWatcherThread.ThreadState & (ThreadState.Unstarted | ThreadState.Stopped | ThreadState.Suspended)) == 0)
        //            {
        //                ExceptionsList.AxisPosMonitoringStarted.MessageText = "The Axis Position Watcher Thread is currently running for Axis " + m_AxisName + ", to copy you have to stop watching thread using StopLogging function";
        //                ExceptionsList.AxisPosMonitoringStarted.ErrorID = ErrorCodesList.AXIS_LOGGER_THREAD_STARTED;
        //                throw ExceptionsList.AxisPosMonitoringStarted;
        //            }
        //        }

        //        return (IBaseAxis)this.MemberwiseClone();
        //    }

        //    /// <summary>
        //    /// Create new copy of SingleAxis object
        //    /// </summary>
        //    public IBaseAxis Clone()
        //    {
        //        if (m_PosMonitoringThread != null)
        //        {
        //            if ((m_PosMonitoringThread.ThreadState & (ThreadState.Unstarted | ThreadState.Stopped | ThreadState.Suspended)) == 0)
        //            {
        //                ExceptionsList.AxisPosMonitoringStarted.MessageText = "The logging watcher thread is currently running for Axis " + m_AxisName + ", to copy you have to stop watching thread using StopLogging function";
        //                ExceptionsList.AxisPosMonitoringStarted.ErrorID = ErrorCodesList.AXIS_LOGGER_THREAD_STARTED;
        //                throw ExceptionsList.AxisPosMonitoringStarted;
        //            }
        //        }

        //        if (m_AxisPositionWatcherThread != null)
        //        {
        //            if ((m_AxisPositionWatcherThread.ThreadState & (ThreadState.Unstarted | ThreadState.Stopped | ThreadState.Suspended)) == 0)
        //            {
        //                ExceptionsList.AxisPosMonitoringStarted.MessageText = "The Axis Position Watcher Thread is currently running for Axis " + m_AxisName + ", to copy you have to stop watching thread using StopLogging function";
        //                ExceptionsList.AxisPosMonitoringStarted.ErrorID = ErrorCodesList.AXIS_LOGGER_THREAD_STARTED;
        //                throw ExceptionsList.AxisPosMonitoringStarted;
        //            }
        //        }

        //        return (IBaseAxis)this.MemberwiseClone();
        //    }



        //    public Boolean SetTargetPositionAsActual(Boolean do_memmory_increase = true)
        //    {
        //        m_MotionController.StopAxisMotion(this, do_memmory_increase);

        //        //System.Threading.Thread.Sleep(100);
        //        return m_MotionController.SetTargetPositionToActual(this, do_memmory_increase);
        //    }

        //    /// <summary>
        //    /// Start the Axis Axis Status Monitoring Thread thread
        //    /// </summary>
        //    public void StartPosMonitoring()
        //    {
        //        try
        //        {
        //            StopPosMonitoring();

        //            m_SuspendPosMonitoringThreadTimeout = 100;
        //            m_LastMotorPosition = ActualPositionIU;

        //            //Initialize Input channels watcher thread data
        //            ThreadStart myThreadStart = new ThreadStart(AxisPositionMonitoringThread);
        //            m_PosMonitoringThread = new Thread(myThreadStart);
        //            m_PosMonitoringThread.IsBackground = true;
        //            m_PosMonitoringThread.SetApartmentState(ApartmentState.STA);
        //            m_PosMonitoringThread.Priority = ThreadPriority.Lowest;
        //            m_PosMonitoringThread.Name = "Axis Status Monitoring Thread";
        //            m_PosMonitoringThread.Start();
        //        }
        //        catch //(Exception e)
        //        {
        //        }
        //    }

        //    /// <summary>
        //    /// Start the Axis Axis Status Monitoring Thread thread
        //    /// </summary>
        //    public void SuspendedStartPosMonitoring(Int32 WaitTimeOut)
        //    {
        //        StopPosMonitoring();
        //        m_InMotion = true;
        //        m_SuspendPosMonitoringThreadTimeout = WaitTimeOut;

        //        //Initialize Input channels watcher thread data
        //        ThreadStart myThreadStart = new ThreadStart(AxisPositionMonitoringThread);
        //        m_PosMonitoringThread = new Thread(myThreadStart);
        //        m_PosMonitoringThread.IsBackground = true;
        //        m_PosMonitoringThread.SetApartmentState(ApartmentState.STA);
        //        m_PosMonitoringThread.Priority = ThreadPriority.Lowest;
        //        m_PosMonitoringThread.Name = "Axis Status Monitoring Thread";
        //        m_PosMonitoringThread.Start();
        //    }

        //    /// <summary>
        //    /// Stop the Axis Axis Status Monitoring Thread thread
        //    /// </summary>
        //    public void StopPosMonitoring()
        //    {
        //        if (m_PosMonitoringThread != null)
        //        {
        //            lock (m_PosMonitoringThread)
        //            {
        //                if (null != m_PosMonitoringThread)
        //                {
        //                    if (m_PosMonitoringThread.IsAlive)
        //                        if (m_PosMonitoringThread.IsBackground)
        //                        {
        //                            m_PosMonitoringThread.Abort();

        //                            //  m_PosMonitoringThread.Join();
        //                        }
        //                    m_PosMonitoringThread = null;
        //                }
        //            }
        //            m_ReadPositionThreadActive = false;

        //            // Wait for foreground thread to end.
        //            System.Threading.Thread.Sleep(50);
        //            m_InMotion = false;
        //        }
        //    }

        //    /// <summary>
        //    /// Start the Axis Position watcher thread with default value of thread priority "ThreadPriority.Lowest"
        //    /// </summary>
        //    /// <param name="IsSingleAxisSystem">
        //    /// If true than thread function is assumes that there is no need to select axis each time
        //    /// otherwise the regular Activate\Deactivate axis mode is applied
        //    /// </param>
        //    public void StartAxisPositionWatcher(Boolean IsSingleAxisSystem)
        //    {
        //        this.StartAxisPositionWatcher(IsSingleAxisSystem, ThreadPriority.Lowest);
        //    }

        //    /// <summary>
        //    /// Start the Axis Position watcher thread
        //    /// </summary>
        //    /// <param name="IsSingleAxisSystem">
        //    /// If true than thread function is assumes that there is no need to select axis each time
        //    /// otherwise the regular Activate\Deactivate axis mode is applied
        //    /// </param>
        //    /// <param name="PositionCheckThreadPriority">Specifies the scheduling priority of a AxisPositionWatcherThread</param>
        //    public void StartAxisPositionWatcher(Boolean IsSingleAxisSystem, ThreadPriority PositionCheckThreadPriority)
        //    {
        //        m_ReadPositionThreadActive = true;
        //        m_LastMotorPosition = ActualPositionIU;
        //        ThreadStart myThreadStart;
        //        if (IsSingleAxisSystem)
        //        {
        //            myThreadStart = new ThreadStart(SingleAxisPositionWatcher);
        //        }
        //        else
        //        {
        //            myThreadStart = new ThreadStart(MultIBaseAxisPositionWatcher);
        //        }

        //        m_AxisPositionWatcherThread = new Thread(myThreadStart);
        //        m_AxisPositionWatcherThread.IsBackground = true;
        //        m_AxisPositionWatcherThread.SetApartmentState(ApartmentState.STA);
        //        m_AxisPositionWatcherThread.Priority = PositionCheckThreadPriority;
        //        m_AxisPositionWatcherThread.Name = "Axis Position Watcher Thread";
        //        m_AxisPositionWatcherThread.Start();
        //    }

        //    /// <summary>
        //    /// Stop the Axis Axis Status Monitoring Thread thread
        //    /// </summary>
        //    public void StopAxisPositionWatcher()
        //    {
        //        m_ReadPositionThreadActive = false;
        //        System.Threading.Thread.Sleep(100);

        //        if (m_AxisPositionWatcherThread != null)
        //        {
        //            lock (m_AxisPositionWatcherThread)
        //            {
        //                if (m_AxisPositionWatcherThread.IsAlive)
        //                    if (m_AxisPositionWatcherThread.IsBackground)
        //                    {
        //                        m_AxisPositionWatcherThread.Abort();
        //                    }
        //                m_AxisPositionWatcherThread = null;
        //            }
        //        }
        //        System.Threading.Thread.Sleep(50);
        //    }


        //    public short CleanEventsHandlers()
        //    {
        //        if (this.NoMotionDelegate != null)
        //        {
        //            NoMotion -= this.NoMotionDelegate;
        //            this.NoMotionDelegate = null;
        //        }

        //        if (this.CurrentPositionChangedDelegate != null)
        //        {
        //            CurrentPositionChanged -= this.CurrentPositionChangedDelegate;
        //            this.CurrentPositionChangedDelegate = null;
        //        }

        //        if (this.FailedToReachTargetPositionDelegate != null)
        //        {
        //            FailedToReachTargetPosition -= this.FailedToReachTargetPositionDelegate;
        //            this.FailedToReachTargetPositionDelegate = null;
        //        }

        //        if (this.FailedToSelectAxisDelegate != null)
        //        {
        //            FailedToSelectAxis -= this.FailedToSelectAxisDelegate;
        //            this.FailedToSelectAxisDelegate = null;
        //        }
        //        return ErrorCodesList.OK;
        //    }



        //    #endregion Public Methods

        //    #region Private functions

        //    //Axis motion status reading thread watcher
        //    /// <summary>
        //    /// The thread that look after Motion status for current axis
        //    /// </summary>
        //    private void AxisPositionMonitoringThread()
        //    {
        //        try
        //        {
        //            System.Threading.Thread.Sleep(m_SuspendPosMonitoringThreadTimeout);
        //            long LastCurPosition = 0;
        //            Int16 NoChangeCounter = 0;
        //            Int16 FailedToSelectAxisCounter = 0;
        //            Boolean bInEvent = false;
        //            Single Delta = 0;

        //            while (true)
        //            {
        //                //	Define local variables
        //                ushort MotionFinished = 1;
        //                int aposition = 0;				// stores the actual position read from the drive

        //                //int i_minAPOS = -20;			// a position value below which an error is detected
        //                // bool event_status = false;				// variable storing the event indicator read from the drive

        //                System.Threading.Thread.Sleep(POSITION_CHECK_LOOP_TIME);
        //                if (null == m_PosMonitoringThread)
        //                    return;

        //                if (!m_IsAxisInSimulatorMode)
        //                {
        //                    if (!m_MotionController.ActivateAxis(this))
        //                    {
        //                        FailedToSelectAxisCounter++;
        //                        if (FailedToSelectAxisCounter++ > FAILED_TO_SELECT_AXIS_TIMES)
        //                        {
        //                            FailedToSelectAxisCounter = 0;

        //                            // OnFailedToSelectAxis(this);
        //                            continue;
        //                        }

        //                        continue;
        //                    }
        //                    else
        //                    {
        //                        try
        //                        {
        //                            FailedToSelectAxisCounter = 0;

        //                            //	Check the SRL.10 bit - Motion completed flag
        //                            //if (!TMLLib.TS_ReadStatus(TMLLib.REG_SRL, out MotionFinished))
        //                            //{
        //                            //    m_MotionController.DeactivateAxis();
        //                            //    continue;
        //                            //}

        //                            //MotionFinished = (UInt16)((MotionFinished & 1 << 10) == 0 ? 1 : 0);

        //                            if (!TMLLib.TS_ReadStatus(TMLLib.REG_SRH, out MotionFinished))
        //                            {
        //                                m_MotionController.DeactivateAxis();
        //                                continue;
        //                            }

        //                            MotionFinished = (UInt16)((MotionFinished & 1 << 9) == 0 ? 1 : 0);

        //                            //	Read the actual position from the drive
        //                            TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out aposition);
        //                        }
        //                        catch { }
        //                        finally
        //                        {
        //                            m_MotionController.DeactivateAxis();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    aposition = ActualPositionIU;
        //                    if (aposition == m_SimulatorDestinationPosition)
        //                        MotionFinished = 0;
        //                    else
        //                        MotionFinished = 1;
        //                }

        //                if (bInEvent) continue;

        //                if (m_LastMotorPosition != aposition)
        //                {
        //                    if (m_AxisType == 2)
        //                    {
        //                        if (Math.Abs(LastCurPosition - aposition) < m_TargetReachedThreshold)
        //                        {
        //                            bInEvent = true;
        //                            OnCurrentPositionChanged((Object)this, aposition);
        //                            bInEvent = false;
        //                            m_MotionController.StopAxisMotion(this, false);
        //                            OnNoMotion((Object)this);
        //                            return;
        //                        }
        //                    }

        //                    NoChangeCounter = 0;
        //                    Delta = aposition - m_LastMotorPosition;
        //                    m_LastMotorPosition = aposition;
        //                    bInEvent = true;
        //                    OnCurrentPositionChanged((Object)this, aposition, Delta);
        //                    bInEvent = false;
        //                    continue;
        //                }

        //                if (NoChangeCounter++ > NO_CHANGE_WAITING_TIME)
        //                {
        //                    NoChangeCounter = 0;
        //                    bInEvent = true;
        //                    OnFailedToReachTargetPosition(this);

        //                    //System.Threading.Thread.Sleep(1000);
        //                    bInEvent = false;
        //                    continue;
        //                }
        //                else
        //                    if (MotionFinished != 0)
        //                    {
        //                        System.Threading.Thread.Sleep(10);
        //                    }
        //                    else

        //                    //if (MotionFinished == 0)// && ActualPositionIU == m_TargetPosition) // && NoChangeCounter++ > NO_CHANGE_WAITING_TIME)
        //                    {
        //                        //tmrNoChangeTimer.Enabled = false;
        //                        m_MotionController.DeactivateAxis();
        //                        OnNoMotion((Object)this);
        //                        m_MotionController.DeactivateAxis();
        //                        return;
        //                    }
        //            }
        //        }
        //        catch (ThreadAbortException exc)
        //        {
        //            //m_MotionController.DeactivateAxis();
        //            Console.WriteLine("Thread aborting, code is " +
        //             exc.ExceptionState);
        //        }
        //        finally
        //        {
        //            m_MotionController.DeactivateAxis();
        //        }
        //    }

        //    /// <summary>
        //    /// Axis motion status reading thread watcher.
        //    /// Used in case there are more than one axis in the system
        //    /// DO NOT USE THIS OPTION WITH REGULAR PosMonitoring FUNCTION
        //    /// </summary>
        //    private void MultIBaseAxisPositionWatcher()
        //    {	
        //        try
        //        {
        //            System.Threading.Thread.Sleep(100);
        //            Int16 FailedToSelectAxisCounter = 0;
        //            Single Delta = 0;
        //            while (true)
        //            {
        //                //	Define local variables
        //                int aposition = 0;				// stores the actual position read from the drive

        //                System.Threading.Thread.Sleep(POSITION_CHECK_LOOP_TIME);
        //                if (null == m_AxisPositionWatcherThread)
        //                    return;

        //                if (!m_IsAxisInSimulatorMode)
        //                {
        //                    if (!m_MotionController.ActivateAxis(this))
        //                    {
        //                        FailedToSelectAxisCounter++;
        //                        if (FailedToSelectAxisCounter++ > FAILED_TO_SELECT_AXIS_TIMES)
        //                        {
        //                            FailedToSelectAxisCounter = 0;
        //                            continue;
        //                        }
        //                        continue;
        //                    }

        //                    FailedToSelectAxisCounter = 0;

        //                    //	Read the actual position from the drive
        //                    TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out aposition);

        //                    m_MotionController.DeactivateAxis();
        //                }
        //                else
        //                {
        //                    aposition = ActualPositionIU;
        //                }

        //                if (m_LastMotorPosition != aposition)
        //                {
        //                    Delta = aposition - m_LastMotorPosition;
        //                    m_LastMotorPosition = aposition;
        //                    OnCurrentPositionChanged((Object)this, aposition, Delta);
        //                    continue;
        //                }

        //                if (!m_ReadPositionThreadActive)
        //                {
        //                    //tmrNoChangeTimer.Enabled = false;
        //                    OnNoMotion((Object)this);
        //                    m_MotionController.DeactivateAxis();
        //                    return;
        //                }
        //            }
        //        }
        //        catch (ThreadAbortException exc)
        //        {
        //            Console.WriteLine("Thread aborting, code is " +
        //             exc.ExceptionState);
        //        }
        //        finally
        //        {
        //            m_MotionController.DeactivateAxis();
        //        }
        //    }

        //    /// <summary>
        //    /// Axis motion status reading thread watcher.
        //    /// Used in case there is only ONE axis in system in the system
        //    /// DO NOT USE THIS OPTION WITH REGULAR PosMonitoring FUNCTION
        //    /// </summary>
        //    private void SingleAxisPositionWatcher()
        //    {
        //        #region Select axis

        //        if (!m_IsAxisInSimulatorMode)
        //        {
        //            Int16 FailedToSelectAxisCounter = 0;
        //            while (FailedToSelectAxisCounter++ < FAILED_TO_SELECT_AXIS_TIMES)
        //            {
        //                if (!m_MotionController.ActivateAxis(this))
        //                    continue;
        //                break;
        //            }
        //            if (FailedToSelectAxisCounter >= FAILED_TO_SELECT_AXIS_TIMES)
        //            {
        //                OnFailedToSelectAxis(this);
        //                return;
        //            }
        //        }

        //        #endregion Select axis

        //        try
        //        {
        //            System.Threading.Thread.Sleep(100);
        //            int LocalPositionCheckTimeout = m_PositionCheckTimeout;

        //            //	Define local variables
        //            int aposition = 0;				// stores the actual position read from the drive
        //            Single Delta = 0;

        //            if (!m_IsAxisInSimulatorMode)
        //            {
        //                while (true)
        //                {
        //                    System.Threading.Thread.Sleep(LocalPositionCheckTimeout);
        //                    if (null == m_AxisPositionWatcherThread)
        //                        return;
        //                    //	Read the actual position from the drive
        //                    TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out aposition);

        //                    if (m_LastMotorPosition != aposition)
        //                    {
        //                        Delta = aposition - m_LastMotorPosition;
        //                        m_LastMotorPosition = aposition;
        //                        OnCurrentPositionChanged((Object)this, aposition, Delta);
        //                        continue;
        //                    }

        //                    if (!m_ReadPositionThreadActive)
        //                    {
        //                        //tmrNoChangeTimer.Enabled = false;
        //                        OnNoMotion((Object)this);
        //                        m_MotionController.DeactivateAxis();
        //                        return;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                while (true)
        //                {
        //                    aposition = ActualPositionIU;
        //                    if (m_LastMotorPosition != aposition)
        //                    {
        //                        Delta = aposition - m_LastMotorPosition;
        //                        m_LastMotorPosition = aposition;
        //                        OnCurrentPositionChanged((Object)this, aposition, Delta);
        //                        continue;
        //                    }

        //                    if (!m_ReadPositionThreadActive)
        //                    {
        //                        //tmrNoChangeTimer.Enabled = false;
        //                        OnNoMotion((Object)this);
        //                        m_MotionController.DeactivateAxis();
        //                        return;
        //                    }
        //                }
        //            }
        //        }
        //        catch (ThreadAbortException exc)
        //        {
        //            Console.WriteLine("Thread aborting, code is " +
        //             exc.ExceptionState);
        //        }
        //        finally
        //        {
        //            m_MotionController.DeactivateAxis();
        //        }
        //    }

        //    private Boolean AnalizeFaultReazon(IBaseAxis axis)
        //    {
        //        ushort SRL_Result;
        //        ushort SRH_Result;
        //        ushort MER_Result;
        //        if (m_MotionController.ActivateAxis(axis))
        //        {
        //            try
        //            {
        //                if (!TMLLib.TS_ReadStatus(TMLLib.REG_SRL, out SRL_Result))
        //                {
        //                    m_MotionController.DeactivateAxis();
        //                    return false;
        //                }
        //                if (!TMLLib.TS_ReadStatus(TMLLib.REG_SRH, out SRH_Result))
        //                {
        //                    m_MotionController.DeactivateAxis();
        //                    return false;
        //                }

        //                if (!TMLLib.TS_ReadStatus(TMLLib.REG_MER, out MER_Result))
        //                {
        //                    m_MotionController.DeactivateAxis();
        //                    return false;
        //                }
        //            }
        //            catch { }
        //            finally { m_MotionController.DeactivateAxis(); }
        //            return true;
        //        }
        //        else
        //            return false;
        //    }

        //    #endregion Private functions

        //    #region Simulator Mode Functions

        //    /// <summary>
        //    /// Used to start movement simulation routine
        //    /// </summary>
        //    /// <param name="inDestinationPosition">The destination to reach</param>
        //    /// <param name="SlewSpeed">Not in use</param>
        //    public void SimulateMotion(Int32 inDestinationPosition, Single SlewSpeed)
        //    {
        //        if (!this.IsAxisInSimulatorMode)
        //        {
        //            throw new Exception("The usage of Simulate Motion method enabled only in simulation mode.");
        //        }

        //        if (tmrMovementSimulation != null)
        //        {
        //            tmrMovementSimulation.Enabled = false;
        //            tmrMovementSimulation = null;
        //        }

        //        m_SimulatorDestinationPosition = inDestinationPosition;
        //        m_SimulatorStep = (inDestinationPosition - this.ActualPositionIU) / SIMULATION_STEPS_COUNT;
        //        m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;

        //        tmrMovementSimulation = new System.Timers.Timer(100);
        //        tmrMovementSimulation.Elapsed += new System.Timers.ElapsedEventHandler(tmrMovementSimulation_Elapsed);
        //        tmrMovementSimulation.Enabled = true;
        //    }

        //    private void tmrMovementSimulation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //    {
        //        if (m_SimulatorStepIndex == 0)
        //        {
        //            tmrMovementSimulation.Enabled = false;
        //            this.ActualPositionIU = m_SimulatorDestinationPosition;
        //        }
        //        else
        //        {
        //            this.ActualPositionIU += m_SimulatorStep;
        //            m_SimulatorStepIndex--;
        //        }
        //    }

        //    public void StopSimulationMotion()
        //    {
        //        if (!this.IsAxisInSimulatorMode)
        //        {
        //            throw new Exception("The usage of Simulate Stop method enabled only in simulation mode.");
        //        }

        //        if (tmrMovementSimulation != null)
        //        {
        //            tmrMovementSimulation.Enabled = false;
        //            tmrMovementSimulation = null;
        //        }
        //        StopPosMonitoring();
        //        OnNoMotion((Object)this);
        //    }

        //    public bool SimulationSetVariable(String pszName, Int16 value)
        //    {
        //        InitVariablesArrays();
        //        if (m_SimulatorINT16VariablesList.ContainsKey(pszName))
        //            m_SimulatorINT16VariablesList.Remove(pszName);
        //        m_SimulatorINT16VariablesList.Add(pszName, value);
        //        return true;
        //    }

        //    public bool SimulationSetVariable(String pszName, Double value)
        //    {
        //        InitVariablesArrays();
        //        if (m_SimulatorDoubleVariablesList.ContainsKey(pszName))
        //            m_SimulatorDoubleVariablesList.Remove(pszName);
        //        m_SimulatorDoubleVariablesList.Add(pszName, value);
        //        return true;
        //    }

        //    public Int16 SimulationGetIntVariable(String pszName)
        //    {
        //        InitVariablesArrays();
        //        if (m_SimulatorINT16VariablesList.ContainsKey(pszName))
        //            return m_SimulatorINT16VariablesList[pszName];
        //        else
        //            return -1;
        //    }

        //    public Double SimulationGetDoubleVariable(String pszName)
        //    {
        //        InitVariablesArrays();
        //        if (m_SimulatorDoubleVariablesList.ContainsKey(pszName))
        //            return m_SimulatorDoubleVariablesList[pszName];
        //        else
        //            return -1;
        //    }

        //    private void InitVariablesArrays()
        //    {
        //        if (IsAxisInSimulatorMode)
        //        {
        //            if (m_SimulatorINT16VariablesList == null)
        //                m_SimulatorINT16VariablesList = new Dictionary<string, Int16>();
        //            if (m_SimulatorDoubleVariablesList == null)
        //                m_SimulatorDoubleVariablesList = new Dictionary<string, Double>();
        //        }
        //    }

        //    #endregion Simulator Mode Functions

        //    public void SimulateContinuousMotion(int slewSpeed)
        //    {
        //        if (!this.IsAxisInSimulatorMode)
        //        {
        //            throw new Exception("The usage of Simulate Motion method enabled only in simulation mode.");
        //        }

        //        if (tmrMovementSimulation != null)
        //        {
        //            tmrMovementSimulation.Enabled = false;
        //            tmrMovementSimulation = null;
        //        }

        //        m_SimulatorDestinationPosition += CONTINUOUS_MOVE_STEP;
        //        m_SimulatorStep = (m_SimulatorDestinationPosition - this.ActualPositionIU) / SIMULATION_STEPS_COUNT;
        //        m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;

        //        tmrMovementSimulation = new System.Timers.Timer(100);
        //        tmrMovementSimulation.Elapsed += new System.Timers.ElapsedEventHandler(tmrMovementSimulation_Elapsed);
        //        tmrMovementSimulation.Enabled = true;
        //    }

        //    #region IComponent Members

        //    public string Name
        //    {
        //        get { return m_AxisName; }
        //    }

        //    #endregion IComponent Members

        //    #region IInputDevice Members

        //    public event DataEventHandler DataReceived;

        //    private long m_TargetReachedThreshold = 10;

        //    public long TargetReachedThreshold
        //    {
        //        get { return m_TargetReachedThreshold; }
        //        set { m_TargetReachedThreshold = value; }
        //    }

        //    #endregion IInputDevice Members

        //    #region IOutputDevice Members

        //    public void Send(string data)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    #endregion IOutputDevice Members
        //}

        //#endregion Single Axis description class
    // }
}