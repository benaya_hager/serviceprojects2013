﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motion_Controller.Axis
{
    #region Units converter Interface definition

    public class UnitsConverter
    {
        /// <summary>
        /// Convert Technosoft units to millimeters
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="IU_Value">The value to be converted in Technosoft IU units</param>
        /// <returns>Corresponding value in millimeters</returns>
        public static Double IU2MM(BaseAxis Axis, Double IU_Value)
        {
            return IU_Value / Axis.Millimeter_To_IU_Coeff;
        }

        /// <summary>
        /// Convert millimeters to Technosoft units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="MM_Value">The value to be converted in millimeters</param>
        /// <returns>Corresponding value in Technosoft units</returns>
        public static Int32 MM2IU(BaseAxis Axis, Double MM_Value)
        {
            return Convert.ToInt32( MM_Value * Axis.Millimeter_To_IU_Coeff);
        }

        /// <summary>
        /// Convert Technosoft Slew units to millimeters/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="SlewIU_Value">The value to be converted in Technosoft Slew IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Double SlewIU2MMS(BaseAxis Axis, Double SlewIU_Value)
        {
            return SlewIU_Value / Axis.MillimeterSecond_To_IU_SlewCoeff;
        }

        /// <summary>
        ///  Convert millimeters\Second to Technosoft Slew units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="SlewMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Technosoft Slew units</returns>
        public static Double MMS2SlewIU(BaseAxis Axis, Double SlewMMS_Value)
        {
            return SlewMMS_Value * Axis.MillimeterSecond_To_IU_SlewCoeff;
        }

        /// <summary>
        /// Convert Technosoft Acceleration units to millimeters/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="AccelerationIU_Value">The value to be converted in Technosoft Acceleration IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Double AccelerationIU2MMS(BaseAxis Axis, Double AccelerationIU_Value)
        {
            return AccelerationIU_Value / Axis.MillimeterSecond_To_IU_AccelerationCoeff;
        }

        /// <summary>
        ///  Convert millimeters\Second to Technosoft Acceleration units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="AccelerationMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Technosoft Acceleration units</returns>
        public static Double MMS2AccelerationIU(BaseAxis Axis, Double AccelerationMMS_Value)
        {
            return AccelerationMMS_Value * Axis.MillimeterSecond_To_IU_AccelerationCoeff;
        }

        /// <summary>
        /// Convert Technosoft Velocity units to millimeters/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="VelocityIU_Value">The value to be converted in Technosoft Velocity IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Double VelocityIU2MMS(BaseAxis Axis, Double VelocityIU_Value)
        {
            return VelocityIU_Value / Axis.MillimeterSecond_To_IU_VelocityCoeff;
        }

        /// <summary>
        ///  Convert millimeters\Second to Technosoft Velocity units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="VelocityMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Technosoft Velocity units</returns>
        public static Double MMS2VelocityIU(BaseAxis Axis, Double VelocityMMS_Value)
        {
            return VelocityMMS_Value * Axis.MillimeterSecond_To_IU_VelocityCoeff;
        }
    }

    #endregion Units convertor Interface definition
}
