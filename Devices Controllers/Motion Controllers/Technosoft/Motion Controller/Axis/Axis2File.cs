using Motion_Controller.Axis;
using Motion_Controller_Common.Interfaces;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace NS_MotionController.Axis
{
    #region Public Enumerators/Types/Structures definitions

    /// <summary>
    /// Color definition enumerator
    /// </summary>
    public enum ColorFormat
    {
        NAMED_COLOR,
        ARGB_COLOR
    }

    #endregion Public Enumerators/Types/Structures definitions

    // Axis list class which will be serialized
    [XmlRoot("AxisFile")]
    public class Axis2File
    {
        /// <summary>
        /// List of  Axis's
        /// </summary>
        private ArrayList ListcAxis;

        public Axis2File()
        {
            ListcAxis = new ArrayList();
        }

        [XmlElement("Axis")]
        public AxisXMLItem[] Items
        {
            get
            {
                AxisXMLItem[] items = new AxisXMLItem[ListcAxis.Count];
                ListcAxis.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                AxisXMLItem[] items = (AxisXMLItem[])value;
                ListcAxis.Clear();
                foreach (AxisXMLItem item in items)
                    ListcAxis.Add(item);
            }
        }

        public int AddItem(AxisXMLItem item)
        {
            return ListcAxis.Add(item);
        }
    }

    // Items in the shopping list
    public class AxisXMLItem
    {
        #region XML fields list

        [XmlAttribute("Axis_Name")]
        public String AxisName;
        [XmlAttribute("Axis_Network_Idetifier")]
        public Byte Axis_Network_Idetifier;
        [XmlAttribute("Axis_StartPosition")]
        public Int32 Axis_StartPosition;
        [XmlAttribute("Millimeter_To_IU_Coeff")]
        public Double Millimeter_To_IU_Coeff;
        [XmlAttribute("MillimeterSecond_To_IU_SlewCoeff")]
        public Double MillimeterSecond_To_IU_SlewCoeff;
        [XmlAttribute("MillimeterSecond_To_IU_AccelerationCoeff")]
        public Double MillimeterSecond_To_IU_AccelerationCoeff;
        [XmlAttribute("Setup_File_Name")]
        public String SetupFileName; 


        [XmlAttribute("ActualPositionVariableName")]
        public String ActualPositionVariableName;
        

        [XmlAttribute("Target_Position_Variable_Name")]
        public String Target_Position_Variable_Name;


        [XmlAttribute("Homing_Routine_Name")]
        public String HomingRoutineName;
        [XmlAttribute("Initial_Procedure_Name")]
        public String InitialRoutineName;
         
        #endregion XML fields list


        #region Constructors

        /// <summary>
        /// Base empty constructor, no of members will be initialized
        /// </summary>
        public AxisXMLItem()
        {
        }

        /// <summary>
        /// Base constructor, will initialize all the values corresponding to the list recived
        /// If new items added to the cAxis, necessary to update the internal and function parameters lists
        /// </summary>
        /// <param name="incAxisName">Optional name for current Axis</param>
        /// <param name="inAxis_Network_Idetifier">The EasyMotion Axis ID</param>
        /// <param name="inMillimeter_To_IU_Coeff">Position in millimeter to EasyMotion IU units coefficient</param>
        /// <param name="inMillimeterSecond_To_IU_SlewCoeff">Slew in millimeter per second to EasyMotion IU units coefficient</param>
        /// <param name="inMillimeterSecond_To_IU_AccelerationCoeff">Acceleration in millimeter per Second^2 to EasyMotion IU units coefficient</param>
        /// <param name="inSetupFileName">Current Axes Setup file</param>
        /// <param name="inDisplayPosition">The position of control on the control panel screen</param>
        ///<param name="inAxis_StartPosition">The initialize position value for axis</param>
        ///<param name="inDo_Homming">Indicate if there are homing routine defined for axis</param>
        ///<param name="inActualPositionVariableName">The long var name of actual position variable in Easy motion Studio</param>
        ///<param name="inSpeed_Variable_Name">The default variable to specify the motion slew speed</param>
        ///<param name="inTarget_Position_Variable_Name">The default variable to specify the motion target position</param>
        ///<param name="inStartMotionFunc">The function name to start automatic motion</param>
        ///<param name="inMANUAL_Function_Name">The function mode (Close loop) for manual motion</param>
        ///<param name="inSTOP_Function_Name">The Stop motion function name</param>
        ///<param name="inIsAxisInSimulatorMode">Value indicate if current axis was initialized in Simulation mode.</param>
        public AxisXMLItem(String inAxisName,
                            Byte inAxis_Network_Idetifier,
                            Int32 inAxis_StartPosition,
                            Double inMillimeter_To_IU_Coeff,
                            Double inMillimeterSecond_To_IU_SlewCoeff,
                            Double inMillimeterSecond_To_IU_AccelerationCoeff,
                            String inSetupFileName, 
                            String inActualPositionVariableName,
                            String inTarget_Position_Variable_Name,
                            String inHomingRoutineName,
                            String inInitialRoutineName,
                            Int32 inIsAxisInSimulatorMode)
        {
            AxisName = inAxisName;
            Axis_Network_Idetifier = inAxis_Network_Idetifier;
            Axis_StartPosition = inAxis_StartPosition;
            Millimeter_To_IU_Coeff = inMillimeter_To_IU_Coeff;
            MillimeterSecond_To_IU_SlewCoeff = inMillimeterSecond_To_IU_SlewCoeff;
            MillimeterSecond_To_IU_AccelerationCoeff = inMillimeterSecond_To_IU_AccelerationCoeff;
            SetupFileName = inSetupFileName;
     
            ActualPositionVariableName = inActualPositionVariableName;

            Target_Position_Variable_Name = inTarget_Position_Variable_Name;

            HomingRoutineName = inHomingRoutineName;
            InitialRoutineName = inInitialRoutineName;

        }

        /// <summary>
        /// Base constructor, will initialize all the values corresponding to the values stored
        /// in received cAxis object reference
        /// If new items added to the cAxis, necessary to update internal the list
        /// </summary>
        /// <param name="axis">The object reference to be stored</param>
        public AxisXMLItem(IBaseAxis axis)
        {


            AxisName = axis.AxisName;
            Axis_Network_Idetifier = axis.NetworkIdentifier;
            Axis_StartPosition = axis.Axis_StartPosition;
            Millimeter_To_IU_Coeff = axis.Millimeter_To_IU_Coeff;
            MillimeterSecond_To_IU_SlewCoeff = axis.MillimeterSecond_To_IU_SlewCoeff;
            MillimeterSecond_To_IU_AccelerationCoeff = axis.MillimeterSecond_To_IU_AccelerationCoeff;
            SetupFileName = axis.SetupFileName;
            ActualPositionVariableName = axis.ActualPositionVariableName;
                        
            Target_Position_Variable_Name = axis.Target_Position_Variable_Name;

            HomingRoutineName = axis.HomingRoutineName;
            InitialRoutineName = axis.InitialRoutineName;
        }

        #endregion Constructors
    }
}