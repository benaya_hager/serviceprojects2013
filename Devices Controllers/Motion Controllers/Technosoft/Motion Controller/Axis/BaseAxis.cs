﻿#if _WIN32
	using Motion_Controller.TML._32_BIT;
#else
	using Motion_Controller.TML._64_BIT;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NS_Common.ViewModels.Common;
using NS_Common.Error_Handling;
using SFW;
using NS_Common;
using Motion_Controller_Common.Interfaces;
using Motion_Controller_Common.Service;
using System.Threading.Tasks;


namespace Motion_Controller.Axis
{
	/// <summary>
	/// Base Technosoft Axis description class
	/// </summary>
	public class BaseAxis : ViewModelBase, IIODevice, IBaseAxis
	{
		#region Constants

			protected const int NO_CHANGE_WAITING_TIME = 15;
			protected const int FAILED_TO_SELECT_AXIS_TIMES = 15;
			protected const int POSITION_CHECK_LOOP_TIME = 50;
			protected const int SIMULATION_STEPS_COUNT = 25;
			protected const int CONTINUOUS_MOVE_STEP = 10000;

            protected const int UPDATE_AXIS_STATE_TIMEOUT = 100;
            protected const int TARGET_REACHED_IU_TOLERANCE = 10;
		#endregion

		#region Protected members

            protected IMotionController m_MotionController;


			protected CancellationTokenSource m_CTSAxisMonitoringThread;
			protected Thread m_AxisStateMonitoringThread = null;


			#region Simulator Definitions

			protected System.Timers.Timer tmrMovementSimulation = null;
			protected Int32 m_SimulatorDestinationPosition = 0;
			protected Int32 m_SimulatorStep = 0;
			protected Int32 m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;
			protected Dictionary<String, Int16> m_SimulatorINT16VariablesList = null;
			protected Dictionary<String, Double> m_SimulatorDoubleVariablesList = null;

			#endregion

		#endregion Protected members

        #region Public events And delegate functions

            #region Target Position Reached event

            /// <summary>
            /// The events delegate function. Occurs when there is no change for constant declared time on current position.
            /// </summary>
            /// <param name="Sender">The control that raised event handler</param>
            /// <param name="args">Not in use</param>
            public delegate void TargetPositionReachedHandler(Object Sender, EventArgs args);

            /// <summary>
            /// Occurs when there is no change for constant declared time on current position.
            /// </summary>
            private event TargetPositionReachedHandler TargetPositionReachedDelegate;

            /// <summary>
            /// Service function, used to check if there is handler for raised event declared.
            /// </summary>
            /// <param name="Sender">The Axis that raised the event</param>
            public void OnTargetPositionReached()
            {
                if (this.TargetPositionReachedDelegate != null)
                     Task.Factory.StartNew(() =>
                                    {
                                        this.TargetPositionReachedDelegate(this, new EventArgs());
                                    });
            }

            public event TargetPositionReachedHandler TargetPositionReached
            {
                // this is the equivalent of TargetPositionReached += new EventHandler(...)
                add
                {
                    if (this.TargetPositionReachedDelegate == null || !this.TargetPositionReachedDelegate.GetInvocationList().Contains(value))
                        this.TargetPositionReachedDelegate += value;
                }

                // this is the equivalent of TargetPositionReached -= new EventHandler(...)
                remove
                {
                    this.TargetPositionReachedDelegate -= value;
                }
            }

            #endregion Target Position Reached event

            #region Current Position Changed Event

            /// <summary>
            /// The events delegate function. Occurs when current axis position is changed position.
            /// </summary>
            /// <param name="Sender">The control that raised event handler</param>
            /// <param name="args">Not in use</param>
            public delegate void CurrentPositionChangedHandler(Object Sender, AxisEventArgs args);

            /// <summary>
            /// Occurs when there is no change for constant declared time on current position.
            /// </summary>
            private event CurrentPositionChangedHandler CurrentPositionChangedDelegate;

            /// <summary>
            /// Service function, used to check if there is handler for raised event declared.
            /// </summary>
            /// <param name="Sender">The Axis that raised the event</param>
            public void OnCurrentPositionChanged(Double cur_pos_mm)
            {
                if (this.CurrentPositionChangedDelegate != null)
                {
                    Task.Factory.StartNew(() =>
                    {
                        this.CurrentPositionChangedDelegate(this, new AxisEventArgs(cur_pos_mm));
                    });
                }
            }

            public void OnCurrentPositionChanged(Double cur_pos_mm, Double last_pos_mm)
            {
                if (this.CurrentPositionChangedDelegate != null)
                    Task.Factory.StartNew(() =>
                    {
                        this.CurrentPositionChangedDelegate(this, new AxisEventArgs(cur_pos_mm, (Math.Abs(last_pos_mm) - Math.Abs( cur_pos_mm)) ));
                    });
            }

            public event CurrentPositionChangedHandler CurrentPositionChanged
            {
                // this is the equivalent of CurrentPositionChanged += new EventHandler(...)
                add
                {
                    if (this.CurrentPositionChangedDelegate == null || !this.CurrentPositionChangedDelegate.GetInvocationList().Contains(value))
                    {
                        this.CurrentPositionChangedDelegate += value;
                    }
                }

                // this is the equivalent of CurrentPositionChanged -= new EventHandler(...)
                remove
                {
                    this.CurrentPositionChangedDelegate -= value;
                }
            }

            #endregion Current Position Changed Event

            #region Failed To Reach Target Position event

            /// <summary>
            /// Define an event handler delegate
            /// The events delegate function. Occurs when Easy Motion Studio Failed To Reach Target Position on selected axis.
            /// </summary>
            /// <param name="Sender">The control that raised event handler </param>
            /// <param name="args"></param>
            public delegate void FailedToReachTargetPositionHandler(Object Sender, EventArgs args);

            /// <summary>
            /// Declare an event handler delegate
            /// </summary>
            private FailedToReachTargetPositionHandler FailedToReachTargetPositionDelegate;

            // private static Int32 m_ExceptionsCounter = 0;
            public void OnFailedToReachTargetPosition(object Sender)
            {
                if (this.FailedToReachTargetPositionDelegate != null)
                {
                    this.FailedToReachTargetPositionDelegate(Sender, new EventArgs());
                }
                else
                {
                    m_MotionController.SetTargetPositionToActual(this);
                    return;
                }
            }

            /// <summary>
            /// re-define the FailedToReachTargetPosition event
            /// </summary>
            public event FailedToReachTargetPositionHandler FailedToReachTargetPosition
            {
                // this is the equivalent of FailedToReachTargetPosition += new EventHandler(...)
                add
                {
                    if (this.FailedToReachTargetPositionDelegate == null || !this.FailedToReachTargetPositionDelegate.GetInvocationList().Contains(value))
                    {
                        this.FailedToReachTargetPositionDelegate += value;
                    }
                }

                // this is the equivalent of FailedToReachTargetPosition -= new EventHandler(...)
                remove
                {
                    this.FailedToReachTargetPositionDelegate -= value;
                }
            }

            #endregion Failed To Reach Target Position event

            #region Failed To Enter Critical Section(Select Axis) event

            /// <summary>
            /// Define an event handler delegate
            /// The events delegate function. Occurs when Easy Motion Studio Failed To Enter Critical Section(Select Axis).
            /// </summary>
            /// <param name="Sender">The control that raised event handler </param>
            /// <param name="args"></param>
            public delegate void FailedToSelectAxisHandler(Object Sender, EventArgs args);

            /// <summary>
            /// Declare an event handler delegate
            /// </summary>
            private FailedToSelectAxisHandler FailedToSelectAxisDelegate;

            protected void OnFailedToSelectAxis(object Sender)
            {
                if (this.FailedToSelectAxisDelegate != null)
                {
                    this.FailedToSelectAxisDelegate(Sender, new EventArgs());
                }
                else
                {
                    ExceptionsList.FailedToEnterCriticalSectionException.MessageText = "The System Failed To Enter Critical Section(Select Axis) on Axis " + m_AxisName;
                    ExceptionsList.FailedToEnterCriticalSectionException.ErrorID = ErrorCodesList.FAILED_TO_ENTER_CRITICAL_SECTION;
                    throw ExceptionsList.FailedToEnterCriticalSectionException;
                }
            }

            /// <summary>
            /// re-define the FailedToSelectAxis event
            /// </summary>
            public event FailedToSelectAxisHandler FailedToSelectAxis
            {
                // this is the equivalent of FailedToSelectAxis += new EventHandler(...)
                add
                {
                    //if (this.FailedToSelectAxisDelegate != null)
                    //{
                    //    FailedToSelectAxis -= this.FailedToSelectAxisDelegate;
                    //    this.FailedToSelectAxisDelegate = null;
                    //}
                    if (this.FailedToSelectAxisDelegate == null || !this.FailedToSelectAxisDelegate.GetInvocationList().Contains(value))
                    {
                        this.FailedToSelectAxisDelegate += value;
                    }
                }

                // this is the equivalent of FailedToSelectAxis -= new EventHandler(...)
                remove
                {
                    this.FailedToSelectAxisDelegate -= value;
                }
            }

            #endregion Failed To Enter Critical Section(Select Axis) event

         #endregion Public events And delegate functions

		#region Public properties

			#region Axis Monitor Flags

				#region SRL

					protected ushort m_SRL_Result;
					/// <summary>
					/// Store SRL Register Data
					/// </summary>
					public ushort SRL_Result
					{
						get { return m_SRL_Result; }
						protected set 
						{
							if (m_SRL_Result != value)
							{
								m_SRL_Result = value;

								//Bit_0 = ((m_SRL_Result & 1) == 0 ? false : true);
								//Bit_1 = ((m_SRL_Result & 1 << 1) == 0 ? false : true);
								//Bit_2 = ((m_SRL_Result & 1 << 2) == 0 ? false : true);
								//Bit_3 = ((m_SRL_Result & 1 << 3) == 0 ? false : true);
								//Bit_4 = ((m_SRL_Result & 1 << 4) == 0 ? false : true);
								//Bit_5 = ((m_SRL_Result & 1 << 5) == 0 ? false : true);
								//Bit_6 = ((m_SRL_Result & 1 << 6) == 0 ? false : true);
								Bit7_HomingOrCallsWarning = ((m_SRL_Result & 1 << 7) == 0 ? false : true);
								Bit8_HomingOrCallsActive = ((m_SRL_Result & 1 << 8) == 0 ? false : true);
								//Bit_9 = ((m_SRL_Result & 1 << 9) == 0 ? false : true);
								Bit10_MotionIsCompleted = ((m_SRL_Result & 1 << 10) == 0 ? false : true);
								//Bit_11 = ((m_SRL_Result & 1 << 11) == 0 ? false : true);
								//Bit_12 = ((m_SRL_Result & 1 << 12) == 0 ? false : true);
								//Bit_13 = ((m_SRL_Result & 1 << 13) == 0 ? false : true);
								Bit14_EventSetHasOccured  = ((m_SRL_Result & 1 << 14) == 0 ? false : true);
								Bit15_AxisIsON = ((m_SRL_Result & 1 << 15) == 0 ? false : true);
					   
								//RaisePropertyChanged("SRL_Result");
							}
						}
					}

					protected Boolean m_Bit7_HomingOrCallsWarning;
					public Boolean Bit7_HomingOrCallsWarning
					{
						get { return m_Bit7_HomingOrCallsWarning; }
						protected set
						{
							if (m_Bit7_HomingOrCallsWarning != value)
							{
								m_Bit7_HomingOrCallsWarning = value;
								RaisePropertyChanged("Bit7_HomingOrCallsWarning");
							}
						}
					}

					protected Boolean m_Bit8_HomingOrCallsActive;
					public Boolean Bit8_HomingOrCallsActive
					{
						get { return m_Bit8_HomingOrCallsActive; }
						protected set
						{
							if (m_Bit8_HomingOrCallsActive != value)
							{
								m_Bit8_HomingOrCallsActive = value;
								RaisePropertyChanged("Bit8_HomingOrCallsActive");
							}
						}
					}

					protected Boolean m_Bit10_MotionIsCompleted;
					public Boolean Bit10_MotionIsCompleted
					{
						get { return m_Bit10_MotionIsCompleted; }
						protected set
						{
							if (m_Bit10_MotionIsCompleted != value)
							{
								m_Bit10_MotionIsCompleted = value;
								RaisePropertyChanged("Bit10_MotionIsCompleted");
							}
						}
					}

					protected Boolean m_Bit14_EventSetHasOccured;
					public Boolean Bit14_EventSetHasOccured
					{
						get { return m_Bit14_EventSetHasOccured; }
						protected set
						{
							if (m_Bit14_EventSetHasOccured != value)
							{
								m_Bit14_EventSetHasOccured = value;
								RaisePropertyChanged("Bit14_EventSetHasOccured");
							}
						}
					}

					protected Boolean m_Bit15_AxisIsON;
					public Boolean Bit15_AxisIsON
					{
						get { return m_Bit15_AxisIsON; }
						protected set
						{
							if (m_Bit15_AxisIsON != value)
							{
								m_Bit15_AxisIsON = value;
								RaisePropertyChanged("Bit15_AxisIsON");
							}
						}
					}

				#endregion

				#region SRH

					protected ushort m_SRH_Result;
					/// <summary>
					/// Store SRH (Status High) Register Data
					/// </summary>
					public ushort SRH_Result
					{
						get { return m_SRH_Result; }
						protected set 
						{
							if (m_SRH_Result != value)
							{
								m_SRH_Result = value;

								Bit0_ENDINItExecuted = ((m_SRH_Result & 1) == 0 ? false : true);
								Bit1_OverPositionTrigger1 = ((m_SRH_Result & 1 << 1) == 0 ? false : true);
								Bit2_OverPositionTrigger2 = ((m_SRH_Result & 1 << 2) == 0 ? false : true);
								Bit3_OverPositionTrigger3 = ((m_SRH_Result & 1 << 3) == 0 ? false : true);
								Bit4_OverPositionTrigger4 = ((m_SRH_Result & 1 << 4) == 0 ? false : true);
								Bit5_AutorunEnabled = ((m_SRH_Result & 1 << 5) == 0 ? false : true);
								Bit6_LSPEventOrInterrupt = ((m_SRH_Result & 1 << 6) == 0 ? false : true);
								Bit7_LSNEventOrInterrupt = ((m_SRH_Result & 1 << 7) == 0 ? false : true);
								Bit8_CaptureEventOrInterrupt = ((m_SRH_Result & 1 << 8) == 0 ? false : true);
								Bit9_TargetReached = ((m_SRH_Result & 1 << 9) == 0 ? false : true);
								Bit10_I2tMotorWarning = ((m_SRH_Result & 1 << 10) == 0 ? false : true);
								Bit11_I2tDriveWarning = ((m_SRH_Result & 1 << 11) == 0 ? false : true);
								Bit12_InGear = ((m_SRH_Result & 1 << 12) == 0 ? false : true);
								//Bit_13 = ((m_SRH_Result & 1 << 13) == 0 ? false : true);
								Bit14_InCam = ((m_SRH_Result & 1 << 14) == 0 ? false : true);
								Bit15_Fault = ((m_SRH_Result & 1 << 15) == 0 ? false : true);

								//RaisePropertyChanged("SRH_Result");
							}
						}
					}

					protected Boolean m_Bit0_ENDINItExecuted;
					public Boolean Bit0_ENDINItExecuted
					{
						get { return m_Bit0_ENDINItExecuted; }
						protected set
						{
							if (m_Bit0_ENDINItExecuted != value)
							{
								m_Bit0_ENDINItExecuted = value;
								RaisePropertyChanged("Bit0_ENDINItExecuted");
							}
						}
					}

					protected Boolean m_Bit1_OverPositionTrigger1;
					public Boolean Bit1_OverPositionTrigger1
					{
						get { return m_Bit1_OverPositionTrigger1; }
						protected set
						{
							if (m_Bit1_OverPositionTrigger1 != value)
							{
								m_Bit1_OverPositionTrigger1 = value;
								RaisePropertyChanged("Bit1_OverPositionTrigger1");
							}
						}
					}

					protected Boolean m_Bit2_OverPositionTrigger2;
					public Boolean Bit2_OverPositionTrigger2
					{
						get { return m_Bit2_OverPositionTrigger2; }
						protected set
						{
							if (m_Bit2_OverPositionTrigger2 != value)
							{
								m_Bit2_OverPositionTrigger2 = value;
								RaisePropertyChanged("Bit2_OverPositionTrigger2");
							}
						}
					}

					protected Boolean m_Bit3_OverPositionTrigger3;
					public Boolean Bit3_OverPositionTrigger3
					{
						get { return m_Bit3_OverPositionTrigger3; }
						protected set
						{
							if (m_Bit3_OverPositionTrigger3 != value)
							{
								m_Bit3_OverPositionTrigger3 = value;
								RaisePropertyChanged("Bit3_OverPositionTrigger3");
							}
						}
					}

					protected Boolean m_Bit4_OverPositionTrigger4;
					public Boolean Bit4_OverPositionTrigger4
					{
						get { return m_Bit4_OverPositionTrigger4; }
						protected set
						{
							if (m_Bit4_OverPositionTrigger4 != value)
							{
								m_Bit4_OverPositionTrigger4 = value;
								RaisePropertyChanged("Bit4_OverPositionTrigger4");
							}
						}
					}

					protected Boolean m_Bit5_AutorunEnabled;
					public Boolean Bit5_AutorunEnabled
					{
						get { return m_Bit5_AutorunEnabled; }
						protected set
						{
							if (m_Bit5_AutorunEnabled != value)
							{
								m_Bit5_AutorunEnabled = value;
								RaisePropertyChanged("Bit5_AutorunEnabled");
							}
						}
					}

					protected Boolean m_Bit6_LSPEventOrInterrupt;
					public Boolean Bit6_LSPEventOrInterrupt
					{
						get { return m_Bit6_LSPEventOrInterrupt; }
						protected set
						{
							if (m_Bit6_LSPEventOrInterrupt != value)
							{
								m_Bit6_LSPEventOrInterrupt = value;
								RaisePropertyChanged("Bit6_LSPEventOrInterrupt");
							}
						}
					}

					protected Boolean m_Bit7_LSNEventOrInterrupt;
					public Boolean Bit7_LSNEventOrInterrupt
					{
						get { return m_Bit7_LSNEventOrInterrupt; }
						protected set
						{
							if (m_Bit7_LSNEventOrInterrupt != value)
							{
								m_Bit7_LSNEventOrInterrupt = value;
								RaisePropertyChanged("Bit7_LSNEventOrInterrupt");
							}
						}
					}

					protected Boolean m_Bit8_CaptureEventOrInterrupt;
					public Boolean Bit8_CaptureEventOrInterrupt
					{
						get { return m_Bit8_CaptureEventOrInterrupt; }
						protected set
						{
							if (m_Bit8_CaptureEventOrInterrupt != value)
							{
								m_Bit8_CaptureEventOrInterrupt = value;
								RaisePropertyChanged("Bit8_CaptureEventOrInterrupt");
							}
						}
					}

					protected Boolean m_Bit9_TargetReached;
					public Boolean Bit9_TargetReached
					{
						get { return m_Bit9_TargetReached; }
						protected set
						{
							if (m_Bit9_TargetReached != value)
							{
								m_Bit9_TargetReached = value;
                                OnTargetPositionReached();
								RaisePropertyChanged("Bit9_TargetReached");
							}
						}
					}

					protected Boolean m_Bit10_I2tMotorWarning;
					public Boolean Bit10_I2tMotorWarning
					{
						get { return m_Bit10_I2tMotorWarning; }
						protected set
						{
							if (m_Bit10_I2tMotorWarning != value)
							{
								m_Bit10_I2tMotorWarning = value;
								RaisePropertyChanged("Bit10_I2tMotorWarning");
							}
						}
					}

					protected Boolean m_Bit11_I2tDriveWarning;
					public Boolean Bit11_I2tDriveWarning
					{
						get { return m_Bit11_I2tDriveWarning; }
						protected set
						{
							if (m_Bit11_I2tDriveWarning != value)
							{
								m_Bit11_I2tDriveWarning = value;
								RaisePropertyChanged("Bit11_I2tDriveWarning");
							}
						}
					}

					protected Boolean m_Bit12_InGear;
					public Boolean Bit12_InGear
					{
						get { return m_Bit12_InGear; }
						protected set
						{
							if (m_Bit12_InGear != value)
							{
								m_Bit12_InGear = value;
								RaisePropertyChanged("Bit12_InGear");
							}
						}
					}

					protected Boolean m_Bit14_InCam;
					public Boolean Bit14_InCam
					{
						get { return m_Bit14_InCam; }
						protected set
						{
							if (m_Bit14_InCam != value)
							{
								m_Bit14_InCam = value;
								RaisePropertyChanged("Bit14_InCam");
							}
						}
					}

					protected Boolean m_Bit15_Fault;
					public Boolean Bit15_Fault
					{
						get { return m_Bit15_Fault; }
						protected set
						{
							if (m_Bit15_Fault != value)
							{
								m_Bit15_Fault = value;
								RaisePropertyChanged("Bit15_Fault");
							}
						}
					}

				#endregion

				#region MER

					protected ushort m_MER_Result;
					/// <summary>
					/// Store MER(Critical error) Register Data 
					/// </summary>
					public ushort MER_Result
					{
						get { return m_MER_Result; }
						protected set 
						{
							if (m_MER_Result != value)
							{
								m_MER_Result = value;

								Bit0_CANbusError = ((m_MER_Result & 1) == 0 ? false : true);
								Bit1_ShortCircuit = ((m_MER_Result & 1 << 1) == 0 ? false : true);
								Bit2_InvalidSetupData = ((m_MER_Result & 1 << 2) == 0 ? false : true);
								Bit3_ControlError = ((m_MER_Result & 1 << 3) == 0 ? false : true);
								Bit4_SerialCommError = ((m_MER_Result & 1 << 4) == 0 ? false : true);
								Bit5_PositionWraparound = ((m_MER_Result & 1 << 5) == 0 ? false : true);
								Bit6_LSPActive = ((m_MER_Result & 1 << 6) == 0 ? false : true);
								Bit7_LSNActive = ((m_MER_Result & 1 << 7) == 0 ? false : true);
								Bit8_OverCurrent = ((m_MER_Result & 1 << 8) == 0 ? false : true);
								Bit9_I2TProtectionError = ((m_MER_Result & 1 << 9) == 0 ? false : true);
								Bit10_MotorOverTemp = ((m_MER_Result & 1 << 10) == 0 ? false : true);
								Bit11_DriveOverTemp = ((m_MER_Result & 1 << 11) == 0 ? false : true);
								Bit12_OverVoltage = ((m_MER_Result & 1 << 12) == 0 ? false : true);
								Bit13_UnderVoltage = ((m_MER_Result & 1 << 13) == 0 ? false : true);
								Bit14_CommandError = ((m_MER_Result & 1 << 14) == 0 ? false : true);
								Bit15_EnableInputIsInactive = ((m_MER_Result & 1 << 15) == 0 ? false : true);
								//RaisePropertyChanged("MER_Result");
							}
						}
					}

					protected Boolean m_Bit0_CANbusError;
					public Boolean Bit0_CANbusError
					{
						get { return m_Bit0_CANbusError; }
						protected set 
						{
							if (m_Bit0_CANbusError != value)
							{
								m_Bit0_CANbusError = value;
								RaisePropertyChanged("Bit0_CANbusError");
							}
						}
					}

					protected Boolean m_Bit1_ShortCircuit;
					public Boolean Bit1_ShortCircuit
					{
						get { return m_Bit1_ShortCircuit; }
						protected set
						{
							if (m_Bit1_ShortCircuit != value)
							{
								m_Bit1_ShortCircuit = value;
								RaisePropertyChanged("Bit1_ShortCircuit");
							}
						}
					}

					protected Boolean m_Bit2_InvalidSetupData;
					public Boolean Bit2_InvalidSetupData
					{
						get { return m_Bit2_InvalidSetupData; }
						protected set
						{
							if (m_Bit2_InvalidSetupData != value)
							{
								m_Bit2_InvalidSetupData = value;
								RaisePropertyChanged("Bit2_InvalidSetupData");
							}
						}
					}

					protected Boolean m_Bit3_ControlError;
					public Boolean Bit3_ControlError
					{
						get { return m_Bit3_ControlError; }
						protected set
						{
							if (m_Bit3_ControlError != value)
							{
								m_Bit3_ControlError = value;
								RaisePropertyChanged("Bit3_ControlError");
							}
						}
					}

					protected Boolean m_Bit4_SerialCommError;
					public Boolean Bit4_SerialCommError
					{
						get { return m_Bit4_SerialCommError; }
						protected set
						{
							if (m_Bit4_SerialCommError != value)
							{
								m_Bit4_SerialCommError = value;
								RaisePropertyChanged("Bit4_SerialCommError");
							}
						}
					}

					protected Boolean m_Bit5_PositionWraparound;
					public Boolean Bit5_PositionWraparound
					{
						get { return m_Bit5_PositionWraparound; }
						protected set
						{
							if (m_Bit5_PositionWraparound != value)
							{
								m_Bit5_PositionWraparound = value;
								RaisePropertyChanged("Bit5_PositionWraparound");
							}
						}
					}

					protected Boolean m_Bit6_LSPActive;
					public Boolean Bit6_LSPActive
					{
						get { return m_Bit6_LSPActive; }
						protected set
						{
							if (m_Bit6_LSPActive != value)
							{
								m_Bit6_LSPActive = value;
								RaisePropertyChanged("Bit6_LSPActive");
							}
						}
					}

					protected Boolean m_Bit7_LSNActive;
					public Boolean Bit7_LSNActive
					{
						get { return m_Bit7_LSNActive; }
						protected set
						{
							if (m_Bit7_LSNActive != value)
							{
								m_Bit7_LSNActive = value;
								RaisePropertyChanged("Bit7_LSNActive");
							}
						}
					}

					protected Boolean m_Bit8_OverCurrent;
					public Boolean Bit8_OverCurrent
					{
						get { return m_Bit8_OverCurrent; }
						protected set
						{
							if (m_Bit8_OverCurrent != value)
							{
								m_Bit8_OverCurrent = value;
								RaisePropertyChanged("Bit8_Overcurrent");
							}
						}
					}

					protected Boolean m_Bit9_I2TProtectionError;
					public Boolean Bit9_I2TProtectionError
					{
						get { return m_Bit9_I2TProtectionError; }
						protected set
						{
							if (m_Bit9_I2TProtectionError != value)
							{
								m_Bit9_I2TProtectionError = value;
								RaisePropertyChanged("Bit9_I2TProtectionError");
							}
						}
					}

					protected Boolean m_Bit10_MotorOverTemp;
					public Boolean Bit10_MotorOverTemp
					{
						get { return m_Bit10_MotorOverTemp; }
						protected set
						{
							if (m_Bit10_MotorOverTemp != value)
							{
								m_Bit10_MotorOverTemp = value;
								RaisePropertyChanged("Bit10_MotorOverTemp");
							}
						}
					}

					protected Boolean m_Bit11_DriveOverTemp;
					public Boolean Bit11_DriveOverTemp
					{
						get { return m_Bit11_DriveOverTemp; }
						protected set
						{
							if (m_Bit11_DriveOverTemp != value)
							{
								m_Bit11_DriveOverTemp = value;
								RaisePropertyChanged("Bit11_DriveOverTemp");
							}
						}
					}

					protected Boolean m_Bit12_OverVoltage;
					public Boolean Bit12_OverVoltage
					{
						get { return m_Bit12_OverVoltage; }
						protected set
						{
							if (m_Bit12_OverVoltage != value)
							{
								m_Bit12_OverVoltage = value;
								RaisePropertyChanged("Bit12_OverVoltage");
							}
						}
					}

					protected Boolean m_Bit13_UnderVoltage;
					public Boolean Bit13_UnderVoltage
					{
						get { return m_Bit13_UnderVoltage; }
						protected set
						{
							if (m_Bit13_UnderVoltage != value)
							{
								m_Bit13_UnderVoltage = value;
								RaisePropertyChanged("Bit13_UnderVoltage");
							}
						}
					}

					protected Boolean m_Bit14_CommandError;
					public Boolean Bit14_CommandError
					{
						get { return m_Bit14_CommandError; }
						protected set
						{
							if (m_Bit14_CommandError != value)
							{
								m_Bit14_CommandError = value;
								RaisePropertyChanged("Bit14_CommandError");
							}
						}
					}

					protected Boolean m_Bit15_EnableInputIsInactive;
					public Boolean Bit15_EnableInputIsInactive
					{
						get { return m_Bit15_EnableInputIsInactive; }
						protected set
						{
							if (m_Bit15_EnableInputIsInactive != value)
							{
								m_Bit15_EnableInputIsInactive = value;
								RaisePropertyChanged("Bit15_EnableInputIsInactive");
							}
						}
					}

				#endregion

			#endregion

			#region Axis Setup Parameters

			protected int m_LoadedSetupID = -1;
			public int LoadedSetupID
			{
				get { return m_LoadedSetupID; }
				set { m_LoadedSetupID = value; }
			}

			protected String m_SetupFileName = "";
			/// <summary>
			/// File name created in EasyMotion Studio, used to initialize the Axis basic information
			/// </summary>
			public String SetupFileName
			{
				get { return m_SetupFileName; }
				set { m_SetupFileName = value; }
			}

			/// <summary>
			/// Stores the value of the Status Register Low from Axis
			/// </summary>
			protected UInt16 m_AxisOn_flag = 0;
			/// <summary>
			/// Used to determinate if the power is on for current Axes
			/// </summary>
			public UInt16 AxisOn_flag
			{
				get { return m_AxisOn_flag; }
				set { m_AxisOn_flag = value; }
			}

			protected double m_ResetWaitPeriod = 0.1;
			/// <summary>
			/// Store the timeout for how long to keep the Axis power off state on reset event
			/// </summary>
			public double ResetWaitPeriod
			{
				get { return m_ResetWaitPeriod; }
				set { m_ResetWaitPeriod = value; }
			}

			protected Int32 m_Axis_StartPosition;
			/// <summary>
			/// Set/Get the home position for current Axis.
			/// May not be in use if the Home routine is working properly.
			/// </summary>
			public Int32 Axis_StartPosition
			{
				get { return m_Axis_StartPosition; }
				set { m_Axis_StartPosition = value; }
			}

			protected Byte m_NetworkIdentifier;
			/// <summary>
			/// The Axis Network Identifier
			/// </summary>
			public Byte NetworkIdentifier
			{
				get { return m_NetworkIdentifier; }
				set { m_NetworkIdentifier = value; }
			}

			protected String m_Target_Position_Variable_Name = "";
			/// <summary>
			/// Set/Get the value indicate if current motor target position parameter name
			/// </summary>
			public String Target_Position_Variable_Name
			{
				get { return m_Target_Position_Variable_Name; }
				set { m_Target_Position_Variable_Name = value; }
			}

			protected String m_ActualPositionVariableName = "APOS";
			/// <summary>
			/// Get/Set the name of technosoft variable name for current position check
			/// </summary>
			public String ActualPositionVariableName
			{
				get { return m_ActualPositionVariableName; }
				set { m_ActualPositionVariableName = value; }
			}

            private String  m_HomingRoutineName = "Homing_X";
            /// <summary>
            /// Get/Set the name of technosoft homing routine name. Till now it was allways "Homing_X"
            /// </summary>
            public String  HomingRoutineName
            {
                get { return m_HomingRoutineName; }
                set { m_HomingRoutineName = value; }
            }


            private String m_InitialRoutineName = "go_to_start_pos";
            /// <summary>
            /// Get/Set the name of technosoft routine name that will initialize all  necesarry axis values, 
            /// such as send axis to start position enable joystick etc.. Till now it was allways "go_to_start_pos"
            /// </summary>
            public String InitialRoutineName
            {
                get { return m_InitialRoutineName; }
                set { m_InitialRoutineName = value; }
            }

        

			#endregion

			#region Units Conversation Properties

			protected double m_Millimeter_To_IU_Coeff; // = TMLLibDefs.MILLIMETER_TO_IO_COEF;
			/// <summary>
			/// Get multiplier for current Axes translation from mm to drive/motor Internal Units.
			/// Used to calculate encoder index position.
			/// </summary>
			public double Millimeter_To_IU_Coeff
			{
				get { return m_Millimeter_To_IU_Coeff; }
				set { m_Millimeter_To_IU_Coeff = value; }
			}

			protected double m_MillimeterSecond_To_IU_SlewCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_SLEW_COEF;
			/// <summary>
			/// Get multiplier for current Axes translation from mm/s to drive/motor Internal Units.
			/// Used to calculate drive/motor slew speed.
			/// </summary>
			public double MillimeterSecond_To_IU_SlewCoeff
			{
				get { return m_MillimeterSecond_To_IU_SlewCoeff; }
				set { m_MillimeterSecond_To_IU_SlewCoeff = value; }
			}

			protected double m_MillimeterSecond_To_IU_AccelerationCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_ACCELERATION_COEF;
			/// <summary>
			/// Get multiplier for current Axes translation from mm/s^2 to drive/motor Internal Units.
			/// Used to calculate drive/motor Acceleration speed.
			/// </summary>
			public double MillimeterSecond_To_IU_AccelerationCoeff
			{
				get { return m_MillimeterSecond_To_IU_AccelerationCoeff; }
				set { m_MillimeterSecond_To_IU_AccelerationCoeff = value; }
			}

			protected double m_MillimeterSecond_To_IU_VelocityCoeff; // = TMLLibDefs.MILLIMETER_SECOND_TO_IU_VELOCITY_COEF;
			/// <summary>
			/// Get multiplier for current Axes translation from mm/s to drive/motor Internal Units.
			/// Used to calculate drive/motor velocity speed.
			/// </summary>
			public double MillimeterSecond_To_IU_VelocityCoeff
			{
				get { return m_MillimeterSecond_To_IU_VelocityCoeff; }
				set { m_MillimeterSecond_To_IU_VelocityCoeff = value; }
			}

			#endregion

			protected String m_AxisName = "";
			/// <summary>
			/// Optional parameter, that makes able to give names to each Axis
			/// By default is Empty String value
			/// </summary>
			public String AxisName
			{
				get { return m_AxisName; }
				set { m_AxisName = value; }
			}

			protected int m_ActualPositionIU = 0;
			/// <summary>
			/// Store the actual position of Axis in Technosoft Units
			/// </summary>
			public int ActualPositionIU
			{
				get
				{
					if (!this.IsAxisInSimulatorMode)
					{
						if (ErrorCodesList.OK != m_MotionController.ActivateAxis(this))
						    return m_ActualPositionIU;

						try
						{
							TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out m_ActualPositionIU);
						}
						catch { }
						finally
						{
							m_MotionController.DeactivateAxis();
						}
						
						return m_ActualPositionIU;
					}
					else
						return m_ActualPositionIU;
				}
				protected set
				{
                    if (m_ActualPositionIU != value)
                    {
                        m_ActualPositionIU = value;
                        ActualPositionMM = this.IU2MM(this.ActualPositionIU);
                    }
				}
			}

            protected Double m_ActualPositionMM;
            /// <summary>
            /// Get the actual Axis position converted to MM
            /// </summary>
			public Double ActualPositionMM
			{
                protected set 
                {
                    if (m_ActualPositionMM != value)
                    {
                        OnCurrentPositionChanged(value, m_ActualPositionMM);
                        m_ActualPositionMM = value;
                        RaisePropertyChanged("ActualPositionMM");
                    }
                }
                get { return m_ActualPositionMM; }
			}
			
			/// <summary>
			/// Indicate that currently the axis is in motion
			/// </summary>
			public bool InMotion
			{
                get { return !(m_Bit9_TargetReached && m_Bit10_MotionIsCompleted); }
			}

			/// <summary>
			/// Get/Set the value indicate if current axis was initialized in Simulation mode.
			/// </summary>
			public Boolean IsAxisInSimulatorMode
			{
                get { return m_MotionController.IsAxesInSimulatorMode; }
			}
        
			protected Single m_LastDestinationPositionIU = 0;
			public Single LastDestinationPositionIU
			{
				get { return m_LastDestinationPositionIU; }
				set { m_LastDestinationPositionIU = value; }
			}

            protected long m_TargetReachedIUTolerance = TARGET_REACHED_IU_TOLERANCE;
            public long TargetReachedIUTolerance
			{
				get { return m_TargetReachedIUTolerance; }
				set { m_TargetReachedIUTolerance = value; }
			}

            private Int32 m_UpdateAxisStatusTimeout = UPDATE_AXIS_STATE_TIMEOUT ;

            public Int32  UpdateAxisStatusTimeout
            {
                get { return m_UpdateAxisStatusTimeout; }
                set { m_UpdateAxisStatusTimeout = value; }
            }
            
		#endregion Public properties

		#region Constructors

		/// <summary>
		/// Base empty constructor, all the members initialize to their base values
		/// </summary>
		public BaseAxis(IMotionController motion_cntrl)
		{
			m_MotionController = motion_cntrl;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Start the main routines performance loop
		/// </summary>
		public void StartAxisMonitoring()
		{

			StopAxisMonitoring();
		   // while (null!=m_AxisStateMonitoringThread)
			

			m_CTSAxisMonitoringThread = new CancellationTokenSource();
			m_AxisStateMonitoringThread = new Thread(new ParameterizedThreadStart(AxisMonitoringThread));
			m_AxisStateMonitoringThread.IsBackground = true;
			m_AxisStateMonitoringThread.Name =  "Axis Status Monitoring Thread";
			m_AxisStateMonitoringThread.SetApartmentState(ApartmentState.STA);
			m_AxisStateMonitoringThread.Priority = ThreadPriority.Lowest;
			m_AxisStateMonitoringThread.Start(m_CTSAxisMonitoringThread.Token);
		}

		/// <summary>
		/// Stop the Routine performance loop without waiting for complete loop.
		/// Please be careful for memory leaks while using this method.
		/// </summary>
		public void StopAxisMonitoring()
		{
            if (m_AxisStateMonitoringThread != null)
			{
				try
				{
					if (!m_CTSAxisMonitoringThread.IsCancellationRequested)
					{
						m_CTSAxisMonitoringThread.Cancel();
						System.Threading.Thread.Sleep(100);
					}
				}
				catch { }
				finally
				{
					m_AxisStateMonitoringThread = null;
				}
			}
		}


        /// <summary>
		/// Updates drive status information.
		/// </summary>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
		public int ReadAxisStateFlags()
		{
            ushort tmpFlagHolder;
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(this))
                return m_MotionController.ErrorStatus; 
			try
			{
                //Returns drive status information.
                if (ErrorCodesList.OK != m_MotionController.ReadRegisterStatus(TMLLib.REG_SRL, out tmpFlagHolder))
                    return m_MotionController.ErrorStatus;
                SRL_Result = tmpFlagHolder;

                if (ErrorCodesList.OK != m_MotionController.ReadRegisterStatus(TMLLib.REG_SRH, out tmpFlagHolder))
                    return m_MotionController.ErrorStatus;
                SRH_Result = tmpFlagHolder;

                if (ErrorCodesList.OK != m_MotionController.ReadRegisterStatus(TMLLib.REG_MER, out tmpFlagHolder))
                    return m_MotionController.ErrorStatus;
                MER_Result = tmpFlagHolder;
			}
			catch { }
			finally { m_MotionController.DeactivateAxis(); }

            return m_MotionController.ErrorStatus;
			 
		}


        public void SetSimulatorPosition(Int32 position_iu)
        {
            ActualPositionIU = position_iu; 
        }

        #region Units Conversation methods

        /// <summary>
        /// Convert Technosoft units to millimeters
        /// </summary>
        /// <param name="IU_Value">The value to be converted in IU units</param>
        /// <returns>Corresponding value in millimeters</returns>
        public Double IU2MM(Double IU_Value)
        {
            return UnitsConverter.IU2MM(this, IU_Value);
        }

        /// <summary>
        /// Convert millimeters to Technosoft units
        /// </summary>
        /// <param name="MM_Value">The value to be converted in millimeters</param>
        /// <returns>Corresponding value in Technosoft units</returns>
        public Int32 MM2IU(Double MM_Value)
        {
            return UnitsConverter.MM2IU(this, MM_Value);
        }

        /// <summary>
        /// Convert Technosoft Slew units to Millimeters\Second
        /// </summary>
        /// <param name="SlewIU_Value">The value to be converted in Slew IU units</param>
        /// <returns>Corresponding value in Millimeters\Second</returns>
        public Double IUSlew2MMS(Double SlewIU_Value)
        {
            return UnitsConverter.SlewIU2MMS(this, SlewIU_Value);
        }

        /// <summary>
        /// Convert Millimeters\Second to Technosoft Slew IU units
        /// </summary>
        /// <param name="SlewMMS_Value">The value to be converted in Millimeters\Second</param>
        /// <returns>Corresponding value in Technosoft Slew IU units</returns>
        public Double MMS2SlewIU(Double SlewMMS_Value)
        {
            return UnitsConverter.MMS2SlewIU(this, SlewMMS_Value);
        }

        /// <summary>
        /// Convert Technosoft Acceleration units to Millimeters\Second
        /// </summary>
        /// <param name="AccelerationIU_Value">The value to be converted in Acceleration IU units</param>
        /// <returns>Corresponding value in Millimeters\Second</returns>
        public Double IUAcceleration2MMS(Double AccelerationIU_Value)
        {
            return UnitsConverter.AccelerationIU2MMS(this, AccelerationIU_Value);
        }

        /// <summary>
        /// Convert Millimeters\Second to Technosoft Acceleration IU units
        /// </summary>
        /// <param name="AccelerationMMS_Value">The value to be converted in Millimeters\Second</param>
        /// <returns>Corresponding value in Technosoft Slew IU units</returns>
        public Double MMS2AccelerationIU(Double AccelerationMMS_Value)
        {
            return UnitsConverter.MMS2AccelerationIU(this, AccelerationMMS_Value);
        }

        /// <summary>
        /// Convert Technosoft Velocity units to Millimeters\Second
        /// </summary>
        /// <param name="VelosityIU_Value">The value to be converted in Velocity IU units</param>
        /// <returns>Corresponding value in Millimeters\Second</returns>
        public Double IU2VelosityMMS(Double VelosityIU_Value)
        {
            return UnitsConverter.VelocityIU2MMS(this, VelosityIU_Value);
        }

        /// <summary>
        /// Convert Millimeters\Second to Technosoft Velocity IU units
        /// </summary>
        /// <param name="VelosityMMS_Value">The value to be converted in Millimeters\Second</param>
        /// <returns>Corresponding value in Technosoft Slew IU units</returns>
        public Double MMS2VelosityIU(Double VelosityMMS_Value)
        {
            return UnitsConverter.MMS2VelocityIU(this, VelosityMMS_Value);
        }

        #endregion

        #endregion

        #region Protected Methods

            protected void AxisMonitoringThread(Object cancel_token)
		    {
                int aposition;
			    while (true)
			    {
				    ReadAxisStateFlags();
                    if (((CancellationToken)cancel_token).IsCancellationRequested)
                        return;
                    
                    //	Read the actual position from the drive
                    TMLLib.TS_GetLongVariable(this.ActualPositionVariableName, out aposition);

                    ActualPositionIU = aposition;

                    System.Threading.Thread.Sleep(m_UpdateAxisStatusTimeout);
				    if (((CancellationToken)cancel_token).IsCancellationRequested)
					    return;
			    }
		    }

		#endregion

		#region Simulator Mode Functions

		public Int32 SimulatorDestinationPosition
		{ get { return m_SimulatorDestinationPosition; } }

		/// <summary>
		/// Used to start movement simulation routine
		/// </summary>
		/// <param name="inDestinationPosition">The destination to reach</param>
		/// <param name="SlewSpeed">Not in use</param>
		public void SimulateMotion(Int32 inDestinationPosition, Single SlewSpeed)
		{
			if (!this.IsAxisInSimulatorMode)
			{
				throw new Exception("The usage of Simulate Motion method enabled only in simulation mode.");
			}

			if (tmrMovementSimulation != null)
			{
				tmrMovementSimulation.Enabled = false;
				tmrMovementSimulation = null;
			}

			m_SimulatorDestinationPosition = inDestinationPosition;
			m_SimulatorStep = (inDestinationPosition - this.ActualPositionIU) / SIMULATION_STEPS_COUNT;
			m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;

			tmrMovementSimulation = new System.Timers.Timer(100);
			tmrMovementSimulation.Elapsed += new System.Timers.ElapsedEventHandler(tmrMovementSimulation_Elapsed);
			tmrMovementSimulation.Enabled = true;
		}

		protected void tmrMovementSimulation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (m_SimulatorStepIndex == 0)
			{
				tmrMovementSimulation.Enabled = false;
				this.ActualPositionIU = m_SimulatorDestinationPosition;
			}
			else
			{
				this.ActualPositionIU += m_SimulatorStep;
				m_SimulatorStepIndex--;
			}
		}

		public void StopSimulationMotion()
		{
			if (!this.IsAxisInSimulatorMode)
			{
				throw new Exception("The usage of Simulate Stop method enabled only in simulation mode.");
			}

			if (tmrMovementSimulation != null)
			{
				tmrMovementSimulation.Enabled = false;
				tmrMovementSimulation = null;
			}
			//StopPosMonitoring();
			OnTargetPositionReached();
		}

		public int SimulationSetVariable(String pszName, Int16 value)
		{
			InitSimulatorVariablesArrays();
			if (m_SimulatorINT16VariablesList.ContainsKey(pszName))
				m_SimulatorINT16VariablesList.Remove(pszName);
			m_SimulatorINT16VariablesList.Add(pszName, value);
			return ErrorCodesList.OK;
		}

		public int  SimulationSetVariable(String pszName, Double value)
		{
			InitSimulatorVariablesArrays();
			if (m_SimulatorDoubleVariablesList.ContainsKey(pszName))
				m_SimulatorDoubleVariablesList.Remove(pszName);
			m_SimulatorDoubleVariablesList.Add(pszName, value);
            return ErrorCodesList.OK;
		}

		public int SimulationGetIntVariable(String pszName, out Int16 result)
		{
			InitSimulatorVariablesArrays();
            if (m_SimulatorINT16VariablesList.ContainsKey(pszName))
                result = m_SimulatorINT16VariablesList[pszName];
            else
                result = -1;
            return ErrorCodesList.OK;
		}

		public int SimulationGetDoubleVariable(String pszName, out Double result)
		{
			InitSimulatorVariablesArrays();
			if (m_SimulatorDoubleVariablesList.ContainsKey(pszName))
				result= m_SimulatorDoubleVariablesList[pszName];
			else
                result = -1;

            return ErrorCodesList.OK;
		}

		public void SimulateContinuousMotion(int slewSpeed)
		{
			if (!this.IsAxisInSimulatorMode)
			{
				throw new Exception("The usage of Simulate Motion method enabled only in simulation mode.");
			}

			if (tmrMovementSimulation != null)
			{
				tmrMovementSimulation.Enabled = false;
				tmrMovementSimulation = null;
			}

			m_SimulatorDestinationPosition += CONTINUOUS_MOVE_STEP;
			m_SimulatorStep = (m_SimulatorDestinationPosition - this.ActualPositionIU) / SIMULATION_STEPS_COUNT;
			m_SimulatorStepIndex = SIMULATION_STEPS_COUNT;

			tmrMovementSimulation = new System.Timers.Timer(100);
			tmrMovementSimulation.Elapsed += new System.Timers.ElapsedEventHandler(tmrMovementSimulation_Elapsed);
			tmrMovementSimulation.Enabled = true;
		}

		protected void InitSimulatorVariablesArrays()
		{
			if (IsAxisInSimulatorMode)
			{
				if (m_SimulatorINT16VariablesList == null)
					m_SimulatorINT16VariablesList = new Dictionary<string, Int16>();
				if (m_SimulatorDoubleVariablesList == null)
					m_SimulatorDoubleVariablesList = new Dictionary<string, Double>();
			}
		}

		#endregion Simulator Mode Functions

		#region IComponent Members

		public string Name
		{
			get { return m_AxisName; }
		}

		#endregion IComponent Members

		#region IInputDevice Members

		public event DataEventHandler DataReceived;

		#endregion IInputDevice Members

		#region IOutputDevice Members

		public void Send(string data)
		{
			throw new NotImplementedException();
		}

		#endregion IOutputDevice Members
	}
}
