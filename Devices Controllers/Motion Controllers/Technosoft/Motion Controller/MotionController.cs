﻿#if _WIN32
	using Motion_Controller.TML._32_BIT;
#else
    using Motion_Controller.TML._64_BIT;
#endif

using log4net;
using Medinol.UserActivityMonitor;
using Motion_Controller_Common.Interfaces;
using Motion_Controller_Common.Service;
using Motion_Controller.TML;
using NS_Common;
using NS_Common.Error_Handling;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tools.CustomRegistry;
using NS_Common.ApplicationExtensions;
using System.Threading;
using System.Diagnostics;
using NS_MotionController.Axis;
using System.Xml.Serialization;
using Motion_Controller.Axis;



namespace Motion_Controller
{
    public class MotionController : IMotionController
    {
        #region Protected types and constant definitions

            protected const String AXIS_FILE_NAME = @"C:\ES2 Axis.xml";

            protected const Byte CLOSE_CHANNEL_TIMES = 5;

        #endregion

        #region Protected members
        
            protected static Object m_ObjAxisSynchronization = null;
            protected Queue m_ListOfHoldingThreads = new Queue();
            protected static Int32 m_AxisSelectionCounter = 0;


            protected Boolean m_isKeyboardHookEnable = true;
            protected Boolean ManualMotionXButtonPressed = false;
            protected Boolean ManualMotionYButtonPressed = false;


            protected Int32 m_AxisHommingTimeoutMS = 30000,
                            m_AxisInitialRoutineTimeoutMS = 30000;


            protected IBaseAxis m_SimulatorSelectedAxis = null;
        

            protected ILog m_SystemLog = LogManager.GetLogger("MotionController");

            protected AxisSetupHelper m_AxisSetupHelper;
            protected MotionHelper m_MotionHelper;




        #endregion

        #region Public events Delegator functions

            #region No Axis Data Available Event

                /// <summary>
                /// The delegate handle function for no Axis Loaded issue
                /// </summary>
                /// <param name="Sender"></param>
                /// <param name="args"></param>
                public delegate void NoAxisDataAvailableHandler(Object Sender, EventArgs args);
                /// <summary>
                /// Declare an event handler delegate
                /// </summary>
                protected NoAxisDataAvailableHandler NoAxisDataAvailableDelegate;

                public void OnNoAxisDataAvailable(object Sender)
                {
                    if (this.NoAxisDataAvailableDelegate != null)
                    {
                        this.NoAxisDataAvailableDelegate(Sender, new EventArgs());
                    }
                    else
                    {
                        throw new Exception("There are no Axis definitions available for Motion Controller and no Event handle available, please contact Administrator as soon as possible.");
                    }
                }

                /// <summary>
                /// The Event that indicate that this is the first creation of object, and needs to load
                /// all Axis's information from database
                /// </summary>
                public event NoAxisDataAvailableHandler NoAxisDataAvailable
                {
                    // this is the equivalent of NoAxisDataAvailable += new EventHandler(...)
                    add
                    {
                        if (this.NoAxisDataAvailableDelegate != null)
                        {
                            NoAxisDataAvailable -= this.NoAxisDataAvailableDelegate;
                            this.NoAxisDataAvailableDelegate = null;
                        }
                        this.NoAxisDataAvailableDelegate += value;
                    }
                    // this is the equivalent of NoAxisDataAvailable -= new EventHandler(...)
                    remove
                    {
                        this.NoAxisDataAvailableDelegate -= value;
                    }
                }

            #endregion No Axis Data Available Event

        #endregion

        #region Public properties

            protected Boolean m_IsAxesInSimulatorMode = false;
            /// <summary>
            /// Indicate if the system running in simulation mode,
            /// and not necessary  to raise error events
            /// </summary>
            public virtual Boolean IsAxesInSimulatorMode
            {
                get { return m_IsAxesInSimulatorMode; }
                set { m_IsAxesInSimulatorMode = value; }
            }
            

            protected static Int16 m_SelectedAxisNetworkIdetifier = -1;
            public static Int16 SelectedAxisNetworkIdetifier
            {
                get { return m_SelectedAxisNetworkIdetifier; }
                set { m_SelectedAxisNetworkIdetifier = value; }
            }

            /// <summary>
            /// The local reference to Axis xml file
            /// </summary>
            protected String m_XmlAxisFile = AXIS_FILE_NAME;
            /// <summary>
            /// Get/Set the path where axis definition and properties stored
            /// </summary>
            public virtual String XmlAxisFile
            {
                get
                {
                    return m_XmlAxisFile;
                }
                set
                {
                    if (File.Exists(value))
                        m_XmlAxisFile = value;
                    else
                        throw new Exception("File " + value + " Not Exists");
                }
            }

            
            protected AxisCollection m_AxisCol = null;
            /// <summary>
            /// Get/Set the reference to the AxisCollection 
            /// </summary>
            public virtual AxisCollection AxisCol
            {
                get { return m_AxisCol; }
                protected set { m_AxisCol = value; }
            }


            protected int m_ErrorStatus = ErrorCodesList.OK;
            /// <summary>
            /// Get/Set the error status for controller
            /// </summary>
            public virtual int ErrorStatus
            {
                get { return m_ErrorStatus; }
                set { m_ErrorStatus = value; }
            }

            protected String m_ErrorMessage = "";

            /// <summary>
            /// Get/Set the error status Message for controller
            /// </summary>
            public virtual String ErrorMessage
            {
                get { return m_ErrorMessage; }
                set { m_ErrorMessage = value; }
            }

            /// <summary>
            /// The window that Pseudo Joystick will be active on it
            ///
            /// </summary>
            protected Window m_KeybourdActivitiesForm;
            public virtual Window KeybourdActivitiesForm
            {
                get { return m_KeybourdActivitiesForm; }
                set { m_KeybourdActivitiesForm = value; }
            }

            #region Pseudo Joystick Area

            protected byte m_HorizontalJoystickAxisNetworkID = (byte)_DEFAULT_CAN_BUS_ADDRESS.X_CAN_ADDRESS;
            public virtual byte HorizontalJoystickAxisNetworkID
            {
                get { return m_HorizontalJoystickAxisNetworkID; }
                set { m_HorizontalJoystickAxisNetworkID = value; }
            }

            protected Single m_JoystickHorSpeedMM = 100;
            public virtual Single JoystickHorSpeedMM
            {
                get { return m_JoystickHorSpeedMM; }
                set { m_JoystickHorSpeedMM = value; }
            }

            protected Single m_JoystickHorAccelerationMM = 100;
            public virtual Single JoystickHorAccelerationMM
            {
                get { return m_JoystickHorAccelerationMM; }
                set { m_JoystickHorAccelerationMM = value; }
            }



            protected byte m_VericalJoystickAxisNetworkID = (Int32)_DEFAULT_CAN_BUS_ADDRESS.Y_CAN_ADDRESS;
            public virtual byte VericalJoystickAxisNetworkID
            {
                get { return m_VericalJoystickAxisNetworkID; }
                set { m_VericalJoystickAxisNetworkID = value; }
            }

            protected Single m_JoystickVerSpeedMM = 100;
            public virtual Single JoystickVerSpeedMM
            {
                get { return m_JoystickVerSpeedMM; }
                set { m_JoystickVerSpeedMM = value; }
            }

            protected Single m_JoystickVerAccelerationMM = 100;
            public virtual Single JoystickVerAccelerationMM
            {
                get { return m_JoystickVerAccelerationMM; }
                set { m_JoystickVerAccelerationMM = value; }
            }

            #endregion Pseudo Joistick Area


            #endregion Public properties

        #region Constructors

            /// <summary>
            /// protected member Constructor
            /// Initialize all the parameters for Connection/Motor/Drive/Axes's
            /// </summary>
            public MotionController()
            {
                m_ObjAxisSynchronization = new Object();


                ErrorStatus = InitRegistryData();

                //if (m_isKeyboardHookEnable)
                //{
                //    HookManager.KeyDown += HookManager_KeyDown;
                //    HookManager.KeyUp += HookManager_KeyUp;
                //}

                if (ErrorStatus != ErrorCodesList.OK)
                    return;

                m_AxisCol = new AxisCollection();

                m_AxisSetupHelper = new AxisSetupHelper(this,m_SystemLog);
                m_MotionHelper = new MotionHelper(this, m_AxisSetupHelper, m_SystemLog); 
            }


            public virtual void ReleaseController()
            {
                for (int i = 0; i < CLOSE_CHANNEL_TIMES; i++)
                {
                    CloseCommunicationChannel(false);
                    System.Threading.Thread.Sleep(50);;
                }
            }

        #endregion

        #region Protected functions

            #region RS232 Port manipulation functions

                /// <summary>
                /// Open communication port, in case failed to open return error code COMMUNICATION_ERROR
                /// </summary>
                /// <returns>
                /// If function successful the return value is MotionController.ErrorCodesList.OK, else return value is one of the
                /// errors codes from MotionController.ErrorCodesList
                /// </returns>
                protected virtual int InitPortConnection()
                {
                    TMLLibDefs defs = TMLLibDefs.GetInstance();
                    //	Open the communication channel
                   if (ErrorCodesList.OK != m_AxisSetupHelper.InitCommunicationChannel(ref defs))
                   {
                       ExceptionsList.CommunicationError.MessageText = m_ErrorMessage;
                       ExceptionsList.CommunicationError.ErrorID = m_ErrorStatus;
                       throw ExceptionsList.CommunicationError;
                    }
                   return m_ErrorStatus;
                }

                /// <summary>
                /// Close the communication channel.
                /// </summary>
                /// <returns>True if successful, otherwise false</returns>
                protected virtual int CloseCommunicationChannel(Boolean bCloseRS232Channel)
                {
                    return m_AxisSetupHelper.CloseCommunicationChannel(bCloseRS232Channel);
                }

            #endregion

            /// <summary>
            /// Get base data from registry
            /// </summary>
            protected virtual int InitRegistryData()
            {
                m_ErrorStatus = ErrorCodesList.OK; 
                try
                {
                    //Init application registry data
                    cRegistryFunc objRegistry = new cRegistryFunc(Application.Current.ProductName(),
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

                    m_IsAxesInSimulatorMode = objRegistry.GetRegKeyValue("Motion Controller Data", "SimulatePortController", m_IsAxesInSimulatorMode == true ? 1 : 0) == 0 ? false : true;
                    objRegistry.SetRegKeyValue("Motion Controller Data", "SimulatePortController", m_IsAxesInSimulatorMode == true ? 1 : 0);

                
                    m_isKeyboardHookEnable = objRegistry.GetRegKeyValue("Motion Controller Data", "Is Keyboard Hook Enable", m_isKeyboardHookEnable == true ? 1 : 0) == 0 ? false : true;
                    objRegistry.SetRegKeyValue("Motion Controller Data", "Is Keyboard Hook Enable", m_isKeyboardHookEnable == true ? 1 : 0);

                    m_AxisHommingTimeoutMS = objRegistry.GetRegKeyValue("Motion Controller Data", "Axis Homing Timeout MS", m_AxisHommingTimeoutMS);
                    objRegistry.SetRegKeyValue("Motion Controller Data", "Axis Homing Timeout MS", m_AxisHommingTimeoutMS);


                    m_AxisInitialRoutineTimeoutMS = objRegistry.GetRegKeyValue("Motion Controller Data", "Axis Initial Routine Timeout MS", m_AxisInitialRoutineTimeoutMS);
                    objRegistry.SetRegKeyValue("Motion Controller Data", "Axis Initial Routine Timeout MS", m_AxisInitialRoutineTimeoutMS);
                }
                catch { ErrorStatus = ErrorCodesList.FAILED; };
                return m_ErrorStatus;
            }

            /// <summary>
            /// Init all available axes's ,in case failed to open return error code INITIALIZATION_ERROR
            /// </summary>
            /// <returns>
            /// If function successful the return value is MotionController_64Bit.ErrorCodesList.OK, else return value is one of the
            /// errors codes from MotionController_64Bit.ErrorCodesList
            /// </returns>
            protected virtual Int32 InitAxes(CancellationToken ct, Boolean send_axes_home_after_init = true)
            {
                m_ErrorMessage = String.Empty;
                m_ErrorStatus = ErrorCodesList.OK; 


                if (m_AxisCol == null)
                {
                    OnNoAxisDataAvailable(this);
                }

                if (ErrorCodesList.OK != m_AxisSetupHelper.InitAxes(m_AxisCol, m_AxisHommingTimeoutMS, m_AxisInitialRoutineTimeoutMS, ct, send_axes_home_after_init))
               {
                   String error_message = m_ErrorMessage;
                   m_ErrorMessage = String.Empty;
                   int error_status = m_ErrorStatus;
                   m_AxisSetupHelper.CancelCallsAndStopMotion(m_AxisCol, true);

                   if (!m_ErrorMessage.Equals(String.Empty))
                       error_message += "\n\r" + m_ErrorMessage;
                   ExceptionsList.InitializationError.MessageText = String.Format("{0} \n\r Error Code number is {1} \n\r Look at System log window for more information" ,error_message , m_ErrorStatus);
                   ExceptionsList.InitializationError.ErrorID = m_ErrorStatus;
                   throw ExceptionsList.InitializationError;
                }

               m_AxisSetupHelper.CancelCallsAndStopMotion(m_AxisCol, false);

               return m_ErrorStatus;
            }

        #endregion

        #region Public functions

            #region Select active Axis Methods

                /// <summary>
                ///  Selects all axis on the active channel. 
                /// </summary>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int ActivateBroadcast()
                {
                    ErrorStatus = ErrorCodesList.OK;
                    ErrorMessage = "";

                    Boolean bSucc = false;
                    Int32 bCounter = 0;
                    if (!m_IsAxesInSimulatorMode)
                    {
                        while (!bSucc)
                        {
                            if (System.Threading.Monitor.TryEnter(m_ObjAxisSynchronization, 5000))
                            {
                                m_SelectedAxisNetworkIdetifier = -1;
                                Interlocked.Increment(ref m_AxisSelectionCounter);
                                m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);
                            
                                //	Select the destination axis of the TML commands
                                if (!TMLLib.TS_SelectBroadcast())
                                {
                                    m_AxisSetupHelper.UpdateLastError();
                                    ErrorStatus = ErrorCodesList.FAILED_TO_SELECT_BROADCAST;
                                    return ErrorStatus;
                                } 
                             
                                bSucc = true;
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(100);
                                if (++bCounter > 2)
                                {
                                    bCounter = 0;
                                    if (m_ListOfHoldingThreads.Count > 0)
                                    {
                                        ProcessThread myThread;
                                        try
                                        {
                                            myThread = (from ProcessThread entry in Process.GetCurrentProcess().Threads
                                                        where entry.Id == (Int32)m_ListOfHoldingThreads.Peek()
                                                        select entry).First();
                                        }
                                        catch (Exception)
                                        {
                                            myThread = null;
                                        }
                                        if (null == myThread ||
                                            myThread.ThreadState == System.Diagnostics.ThreadState.Terminated)
                                        {
                                            try
                                            {
                                                System.Threading.Monitor.PulseAll(m_ObjAxisSynchronization);
                                            }
                                            catch (Exception)
                                            {
                                                myThread = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (System.Threading.Monitor.TryEnter(m_ObjAxisSynchronization, 5000))
                        {
                            Interlocked.Increment(ref m_AxisSelectionCounter);
                            m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);

                            m_SimulatorSelectedAxis = null;
                            m_SelectedAxisNetworkIdetifier = -1;
                        }
                        else
                        {
                            ErrorStatus = ErrorCodesList.FAILED_TO_SELECT_BROADCAST;
                        }
                    }
                    return ErrorStatus;
                }

                /// <summary>
                /// Select the destination axis of the TML commands
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int ActivateAxis(IBaseAxis axis)
                {
                    ErrorStatus = ErrorCodesList.OK;
                    ErrorMessage = "";
 
                    Boolean bSucc = false;
                    Int32 bCounter = 0;
                    if (!m_IsAxesInSimulatorMode)
                    {
                        while (!bSucc)
                        {
                            if (System.Threading.Monitor.TryEnter(m_ObjAxisSynchronization, 5000))
                            {
                                //lock (m_ListOfHoldingThreads)
                                //{
                                Interlocked.Increment(ref m_AxisSelectionCounter);
                                m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);
                                //}

                                if (m_SelectedAxisNetworkIdetifier != axis.NetworkIdentifier)
                                {
                                    //	Select the destination axis of the TML commands
                                    if (!TMLLib.TS_SelectAxis(axis.NetworkIdentifier))
                                    {
                                        m_AxisSetupHelper.UpdateLastError();
                                        ErrorStatus = ErrorCodesList.FAILED_TO_SELECT_AXIS;
                                        return ErrorStatus; 
                                    }
                                    else
                                        m_SelectedAxisNetworkIdetifier = axis.NetworkIdentifier;
                                }
                                bSucc = true;
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(100);
                                if (++bCounter > 2)
                                {
                                    bCounter = 0;
                                    if (m_ListOfHoldingThreads.Count > 0)
                                    {
                                        ProcessThread myThread;
                                        try
                                        {
                                            myThread = (from ProcessThread entry in Process.GetCurrentProcess().Threads
                                                        where entry.Id == (Int32)m_ListOfHoldingThreads.Peek()
                                                        select entry).First();
                                        }
                                        catch (Exception)
                                        {
                                            myThread = null;
                                        }
                                        if (null == myThread ||
                                            myThread.ThreadState == System.Diagnostics.ThreadState.Terminated)
                                        {
                                            try
                                            {
                                                System.Threading.Monitor.PulseAll(m_ObjAxisSynchronization);
                                            }
                                            catch (Exception)
                                            {
                                                myThread = null;
                                            }
                                        }
                                    } 
                                }
                            }
                        }
                    }
                    else
                    {
                        if (System.Threading.Monitor.TryEnter(m_ObjAxisSynchronization, 5000))
                        {
                            Interlocked.Increment(ref m_AxisSelectionCounter);
                            m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);

                            if (m_SelectedAxisNetworkIdetifier != axis.NetworkIdentifier)
                            {
                                m_SimulatorSelectedAxis = axis;
                                m_SelectedAxisNetworkIdetifier = axis.NetworkIdentifier;
                            }
                        }
                        else
                        {
                            ErrorStatus = ErrorCodesList.FAILED_TO_SELECT_AXIS;
                        }

                   
                    }
                    return ErrorStatus;
                }

                /// <summary>
                /// Select the destination axis of the TML commands
                /// </summary>
                /// <param name="NetworkAxisIdentifier">The Axis Network Identifier </param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int ActivateAxis(Int32 NetworkAxisIdentifier)
                {
                    IBaseAxis tmpAxis = m_AxisCol.FindByNetworkID((Byte)NetworkAxisIdentifier);
                    return ActivateAxis(tmpAxis);
                }

                /// <summary>
                /// Select the destination axis of the TML commands
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <returns>TRUE if no error; FALSE if error</returns>
                public Boolean LockAxis(IBaseAxis axis)
                {
                    //#region AxisOn_flag check
                    if (!axis.IsAxisInSimulatorMode)
                    {
                        lock (m_ListOfHoldingThreads)
                        {
                            System.Threading.Monitor.Enter(m_ObjAxisSynchronization);
                            Interlocked.Increment(ref m_AxisSelectionCounter);
                            m_ListOfHoldingThreads.Enqueue(Thread.CurrentThread.ManagedThreadId);
                        }
                        //m_AxisSelectionCounter++;
                    }
                    return true;
                }

                /// <summary>
                /// Release the critical section lock
                /// </summary>
                public virtual void DeactivateAxis()
                {
                    try
                    {
                        int iCounter = m_ListOfHoldingThreads.Count;
                        while (iCounter-- > 0)
                        {
                            Object tmp = m_ListOfHoldingThreads.Peek();
                            if (Convert.ToInt32(tmp) != Thread.CurrentThread.ManagedThreadId)
                                break;
                            Interlocked.Decrement(ref m_AxisSelectionCounter);
                            Object HoldThreadID = m_ListOfHoldingThreads.Dequeue();
                            if (m_ListOfHoldingThreads.Count == 0)
                            {
                                System.Threading.Monitor.Exit(m_ObjAxisSynchronization);
                                break;
                            }
                            else
                                System.Threading.Monitor.Exit(m_ObjAxisSynchronization);
                        }
                    }
                    catch (SynchronizationLockException)
                    {
                    }
                }

            #endregion

            
            #region Motion Methods
        
                /// <summary>
                /// Move Absolute with trapezoidal speed profile. This function allows you to program a position profile
                /// with a trapezoidal shape of the speed. All parameters are predefined except the destination position.
                /// The function will wait till motion is completed.
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <param name="dest_pos_IU">The destination in IU units</param>
                /// <param name="slew_speed_IU">Slew speed in IU units</param>
                /// <param name="acc_IU">Acceleration in IU units</param>
                /// <param name="move_moment"> 
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ref_base">
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int AbsTrapezoidalMove(IBaseAxis axis,
                                                 Single dest_pos_IU,
                                                 Single slew_speed_IU,
                                                 Single acc_IU,
                                                 MOVE_START_TYPE move_moment,
                                                 MOVE_END_TYPE ref_base)
                {
                    if (!axis.IsAxisInSimulatorMode)
                    {
                        axis.LastDestinationPositionIU = dest_pos_IU;
                        //Move Axis for selected position
                        if (ErrorCodesList.OK != m_MotionHelper.TrapezoidalAbsoluteMove(axis,
                                                                    dest_pos_IU, slew_speed_IU, acc_IU,
                                                                    move_moment, ref_base))
                        {
                            return ErrorStatus;
                        }
                    }
                    else
                    {
                        if (ErrorCodesList.OK != ActivateAxis(axis))
                            return m_ErrorStatus;
                        try
                        {
                            m_SimulatorSelectedAxis.SimulateMotion(Convert.ToInt32(dest_pos_IU), 0);
                        }
                        catch { }
                        finally
                        {
                            DeactivateAxis();
                        }

                    }

                    return ErrorStatus; 
                }


                /// <summary>
                /// Move Absolute with trapezoidal speed profile. This function allows you to program a position profile
                /// with a trapezoidal shape of the speed. All parameters are predefined except the destination position.
                /// The function will wait till motion is completed.
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <param name="dest_pos_IU">The destination in IU units</param>
                /// <param name="slew_speed_IU">Slew speed in IU units</param>
                /// <param name="acc_IU">Acceleration in IU units</param>
                /// <param name="move_moment"> 
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ref_base">
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
                /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for infinity </param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int AbsTrapezoidalSyncMove(IBaseAxis axis,
                                                 Single dest_pos_IU,
                                                 Single slew_speed_IU,
                                                 Single acc_IU,
                                                 MOVE_START_TYPE move_moment,
                                                 MOVE_END_TYPE ref_base,
                                                 CancellationToken ct = new CancellationToken(),
                                                 Int32 timeout_ms = 60000)
                {   
                    if (ErrorCodesList.OK != AbsTrapezoidalMove(axis, dest_pos_IU, slew_speed_IU, acc_IU, move_moment, ref_base))
                        return ErrorStatus; 
                    else
                        return m_MotionHelper.WaitPositionReached(axis, ct, timeout_ms);
                }


                /// <summary>
                /// Move Relative with trapezoidal speed profile. This function allows you to program a position profile
                /// with a trapezoidal shape of the speed. All parameters are predefined except the destination position.
                /// The function will wait till motion is completed.
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <param name="offset_IU">The destination in IU units</param>
                /// <param name="slew_speed_IU">Slew speed in IU units</param>
                /// <param name="acc_IU">Acceleration in IU units</param>
                /// <param name="is_additive">
                /// TRUE -> Add the position increment to the position to reach set by the previous motion command
				/// FALSE -> No position increment is added to the target position
                /// </param>
                /// <param name="move_moment"> 
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ref_base">
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int RelTrapezoidalMove(IBaseAxis axis,
                                                 Single offset_IU,
                                                 Single slew_speed_IU,
                                                 Single acc_IU,
                                                 Boolean is_additive,
                                                 MOVE_START_TYPE move_moment,
                                                 MOVE_END_TYPE ref_base)
                {
                    if (!m_IsAxesInSimulatorMode)
                    {
                        axis.LastDestinationPositionIU = offset_IU;
                        //Move Axis for selected position
                        if (ErrorCodesList.OK != m_MotionHelper.TrapezoidalRelativeMove(axis,
                                                                    offset_IU, slew_speed_IU, acc_IU,
                                                                    is_additive, move_moment, ref_base))
                        {
                            return ErrorStatus;
                        }
                    }
                    else
                    {
                        if (ErrorCodesList.OK != ActivateAxis(axis))
                            return m_ErrorStatus;
                        try
                        {
                            m_SimulatorSelectedAxis.SimulateMotion(Convert.ToInt32(offset_IU), 0);
                        }
                        catch { }
                        finally
                        {
                            DeactivateAxis();
                        }

                    }

                    return ErrorStatus; 
                }


                /// <summary>
                /// Move Relative with trapezoidal speed profile. This function allows you to program a position profile
                /// with a trapezoidal shape of the speed. All parameters are predefined except the destination position.
                /// The function will wait till motion is completed.
                /// </summary>
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <param name="offset_IU">The destination in IU units</param>
                /// <param name="slew_speed_IU">Slew speed in IU units</param>
                /// <param name="acc_IU">Acceleration in IU units</param>
                /// <param name="is_additive">
                /// TRUE -> Add the position increment to the position to reach set by the previous motion command
                /// FALSE -> No position increment is added to the target position
                /// </param>
                /// <param name="move_moment"> 
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ref_base">
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
                /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing ; Set -1 for infinity </param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int RelTrapezoidalSyncMove(IBaseAxis axis,
                                                 Single offset_IU,
                                                 Single slew_speed_IU,
                                                 Single acc_IU,
                                                 MOVE_START_TYPE move_moment,
                                                 MOVE_END_TYPE ref_base,
                                                 Boolean is_additive,
                                                 CancellationToken ct = new CancellationToken(),
                                                 Int32 timeout_ms = 60000)
                {
                    if (ErrorCodesList.OK != RelTrapezoidalMove(axis, offset_IU, slew_speed_IU, acc_IU , is_additive, move_moment, ref_base))
                        return ErrorStatus;
                    else
                        return m_MotionHelper.WaitPositionReached(axis, ct, timeout_ms);
                }



                ///The function programs a trapezoidal speed profile. You specify the jog Speed. The
                ///load/motor accelerates until the jog speed is reached. The jog speed can be positive or negative;
                ///the sign gives the direction. The Acceleration can be only positive.
                /// <param name="axis">The Axis descriptor Item. Object reference from the AxisCol list</param>
                /// <param name="slew_speed_IU">Slew speed in IU units</param>
                /// <param name="acc_IU">Acceleration in IU units</param>
                /// <param name="move_moment"> 
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="ref_base">
                /// The position reference starts from the actual measured position value
                /// FROM_MEASURE = 0,
                /// The position reference starts from the actual reference position value
                /// FROM_REFERENCE = 1
                /// </param>
                /// <param name="positive">Indicate the direction</param>
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int ContinuesMove(IBaseAxis axis,
                                          Single slew_speed_IU,
                                          Single acc_IU,
                                          MOVE_START_TYPE move_moment,
                                          MOVE_END_TYPE ref_base,
                                          Boolean positive)
                {

                    if (!axis.IsAxisInSimulatorMode)
                    {
                        
                        //Move Axis for selected position
                        if (ErrorCodesList.OK != m_MotionHelper.ContinuesMove(axis,
                                                                              slew_speed_IU, acc_IU,
                                                                              move_moment,ref_base, positive))
                        {
                            return ErrorStatus;
                        }
                    }
                    else
                    {
                        if (ErrorCodesList.OK != ActivateAxis(axis))
                            return m_ErrorStatus;
                        try
                        {
                            m_SimulatorSelectedAxis.SimulateContinuousMotion(0);
                        }
                        catch { }
                        finally
                        {
                            DeactivateAxis();
                        }

                    }

                    return ErrorStatus;   
                }


                /// <summary>
                /// Perform Homing routine for single Axis
                /// </summary>
                /// <param name="axis">The axis on witch needed to perform homing routine</param>
                /// <param name="homming_timeout_ms">Time to wait till Homing routine complete. ; Set -1 for infinity </param> 
                /// <param name="initial_routine_timeout_ms"> Time to wait till initial routine complete. ; Set -1 for infinity </param> 
                /// <param name="ct">Set Cancel parameter to true for asynchronous. function stop</param> 
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int SendAxis2HomePosition(IBaseAxis axis,
                                                        Int32 homming_timeout_ms = -1,
                                                        Int32 initial_routine_timeout_ms = -1,
                                                        CancellationToken ct = new CancellationToken())
                {
                    if (!IsAxesInSimulatorMode)
                    {
                        if (ErrorCodesList.OK != m_AxisSetupHelper.SendAxisToHomePosition(axis, homming_timeout_ms, initial_routine_timeout_ms, ct))
                        {
                            return ErrorStatus;
                        }

                    }
                    else
                    {
                        axis.SetSimulatorPosition(axis.MM2IU(axis.Axis_StartPosition));
                    }
                    return ErrorStatus;
                }


                /// <summary>
                /// Perform Homing routine for single Axis
                /// </summary>
                /// <param name="axis">The axis on witch needed to perform homing routine</param>
                /// <param name="homming_timeout_ms">Time to wait till Homing routine complete. ; Set -1 for infinity </param> 
                /// <param name="initial_routine_timeout_ms"> Time to wait till initial routine complete. ; Set -1 for infinity </param> 
                /// <param name="ct">Set Cancel parameter to true for asynchronous. function stop</param> 
                /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
                public virtual int SendAxesHomePosition(AxisCollection axes_col,
                                                        Int32 homming_timeout_ms = -1,
                                                        Int32 initial_routine_timeout_ms = -1,
                                                        CancellationToken ct = new CancellationToken())
                {
                    ErrorStatus = ErrorCodesList.OK;
                    ErrorMessage = String.Empty;
                    if (!IsAxesInSimulatorMode)
                    {
                        if (ErrorCodesList.OK != m_AxisSetupHelper.SendAxesToHomePosition(axes_col, homming_timeout_ms, initial_routine_timeout_ms, ct))
                        {
                            return ErrorStatus;
                        }

                    }
                    else
                    {
                        foreach (IBaseAxis axis in axes_col)
                        {
                            axis.SetSimulatorPosition(axis.MM2IU(axis.Axis_StartPosition));
                        }
                    }
                    return ErrorStatus;
                }
            #endregion

            /// <summary>
            ///  Returns drive status information.
            /// </summary>
            /// <param name="sel_index">
            /// REG_MCR -> read MCR register
        	/// REG_MSR -> read MSR register
		    /// REG_ISR -> read ISR register 
			/// REG_SRL -> read SRL register 
		    /// REG_SRH -> read SRH register 
			/// REG_MER -> read MER register 
            /// </param>
            /// <param name="tmpFlagHolder">drive status information (value of the selected register)</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int ReadRegisterStatus(short sel_index, out ushort tmpFlagHolder)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                tmpFlagHolder = 0;

                if (m_IsAxesInSimulatorMode) return ErrorStatus;

                if (!TMLLib.TS_ReadStatus(sel_index, out tmpFlagHolder))
				{
                    ErrorStatus = ErrorCodesList.FAILED_TO_READ_CURRENT_STATE;
                    ErrorMessage += String.Format("Failed to read axis status registers \n\r");

                    m_AxisSetupHelper.UpdateLastError();
                    return ErrorStatus;
				}
                return ErrorStatus;
            }


            /// <summary>
            /// Load the list of Axis's descriptors from XML file
            /// </summary>
            /// <param name="AxisList">Out parameter. The list of Axis objects (AxisCollection)</param>
            /// <param name="FileName">The XML file name</param>
            /// <returns>
            /// ErrorCodesList.OK  if no error; Other if error
            /// </returns>
            public virtual int LoadAxisXmlFile(ref AxisCollection axis_col,
                                            String FileName)
            {
                if (!File.Exists(FileName))
                {
                    ExceptionsList.AxisXMLFileNotFound.MessageText = "The axis's file " + FileName + " not found";
                    ExceptionsList.AxisXMLFileNotFound.ErrorID = ErrorCodesList.AXIS_DEFINITION_FILE_NOT_FOUND_ERROR;
                    throw ExceptionsList.AxisXMLFileNotFound;
                }

                //Load data from XML Axis File
                Axis2File MyList = new Axis2File();
                TextReader Reader = new StreamReader(FileName);

                XmlSerializer Serializer = new XmlSerializer(typeof(Axis2File));
                //DeSerialization
                MyList = (Axis2File)Serializer.Deserialize(Reader);
                Reader.Close();

                //Create new reference to MotionController_64Bit.Axis.AxisCollection object
                if (null == axis_col)
                    axis_col = new AxisCollection();

                int iCont = MyList.Items.Length;
                for (int i = 0; i < iCont; i++)
                {
                    AxisXMLItem xmlItem = MyList.Items[i];
                    IBaseAxis newItem = new BaseAxis(this);
                    newItem.AxisName = xmlItem.AxisName;
                    newItem.NetworkIdentifier = xmlItem.Axis_Network_Idetifier;
                    newItem.Axis_StartPosition = xmlItem.Axis_StartPosition;
                    newItem.Millimeter_To_IU_Coeff = xmlItem.Millimeter_To_IU_Coeff;
                    newItem.MillimeterSecond_To_IU_SlewCoeff = xmlItem.MillimeterSecond_To_IU_SlewCoeff;
                    newItem.MillimeterSecond_To_IU_AccelerationCoeff = xmlItem.MillimeterSecond_To_IU_AccelerationCoeff;
                    newItem.SetupFileName = xmlItem.SetupFileName;


                    newItem.ActualPositionVariableName = xmlItem.ActualPositionVariableName;

                    newItem.Target_Position_Variable_Name = xmlItem.Target_Position_Variable_Name;

                    axis_col.Add(newItem);
                }

                return ErrorCodesList.OK;
            }




            /// <summary>
            /// Controls the power stage (ON/OFF).
            /// </summary>
            /// <param name="axis">Axis reference</param>
            /// <param name="timeout">How long to wait while axis state changed</param>
            /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
            /// <param name="target_power_state">TRUE -> Power ON the drive; FALSE -> Power OFF the drive</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int SetPowerState(IBaseAxis axis, Int32 timeout = -1, CancellationToken ct = new CancellationToken(), Boolean target_power_state = true)
            {
                return m_AxisSetupHelper.SetPowerState(axis, timeout,ct, target_power_state);
            }


            /// <summary>
            /// Reset selected axis
            /// </summary>
            /// <param name="axis">Axis description object to reset</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int ResetAxis(IBaseAxis axis)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (m_IsAxesInSimulatorMode) return ErrorStatus;

                return m_AxisSetupHelper.DriveReset(axis);
            }

            /// <summary>
            /// Reset all axes, by broadcast selection
            /// </summary>
            /// <param name="timeout">How long to wait while axis state changed</param>
            /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int ResetAllAxises(Int32 timeout = -1, CancellationToken ct = new CancellationToken())
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                if (m_IsAxesInSimulatorMode) return ErrorStatus;

                return m_AxisSetupHelper.ResetAllDrives(m_AxisCol, timeout,ct);
            }

            /// <summary>
            /// The function gets out the active axis from the FAULT status. A drive/motor enters in
            ///fault when an error occurs. After a TS_ResetFault execution, most of the errors bits from Motion
            ///Error Register are cleared (set to 0), the Ready output (if present) is set to the ready level, the
            ///Error output (if present) is set to the no error level and the drive/motor returns to normal operation.
            /// </summary>
            /// <param name="axis">Axis description object to reset</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int ResetAxisFault(IBaseAxis axis)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (m_IsAxesInSimulatorMode) return ErrorStatus;

                return m_AxisSetupHelper.ResetAxisFault(axis);
            }




            /// <summary>
            /// Stop axis motion
            /// </summary>
            /// <param name="axis">Axis description object to reset</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int StopAxisMotion(IBaseAxis axis)
            {
                m_ErrorStatus = ErrorCodesList.OK;
                m_ErrorMessage = String.Empty;

                if (null == axis) return m_ErrorStatus;
                if (!m_IsAxesInSimulatorMode)
                {
                    if (ErrorCodesList.OK != m_MotionHelper.StopAxisMotion(axis))
                    {
                        return ErrorStatus;
                    }
                }
                else
                {
                    if (ErrorCodesList.OK != ActivateAxis(axis))
                        return m_ErrorStatus;
                    try
                    {
                        m_SimulatorSelectedAxis.StopSimulationMotion();
                    }
                    catch { }
                    finally
                    {
                        DeactivateAxis();
                    }
                    
                }
                return m_ErrorStatus;
            }

            /// <summary>
            /// Stop axis motion
            /// </summary>
            /// <param name="axis">Axis network ID</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int StopAxisMotion(Int32 axis_network_id)
            {
                return StopAxisMotion(m_AxisCol.FindByNetworkID(axis_network_id));
            }

            /// <summary>
            /// Stop All axis motion
            /// </summary>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int StopAllAxisMotion()
            {   
                foreach (IBaseAxis tmpAxis in m_AxisCol)
                {
                    if (ErrorCodesList.OK != StopAxisMotion(tmpAxis))
                        return ErrorStatus; 
                        
                }
                return ErrorStatus; 
            }

            /// <summary>
            ///  Send stop command selected axis motion. And wait till motion really stopped
            /// </summary>
            /// <param name="axis">The axis on witch user wants to perform action</param>
            /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
            /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing ; Set -1 for infinity  </param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int SyncStopAxisMotion(IBaseAxis axis,
                                          CancellationToken ct = new CancellationToken(),
                                          Int32 timeout_ms = 2000)
            { 
                if (null == axis) return m_ErrorStatus;

                if (!m_IsAxesInSimulatorMode)
                {
                    if (ErrorCodesList.OK != m_MotionHelper.SyncStopAxisMotion(axis, ct, timeout_ms))
                    {
                        return ErrorStatus;
                    }
                }
                else
                {
                    StopAxisMotion(axis);
                }
                return ErrorStatus;
            }



            /// <summary>
            /// The function gets out the active axis from the FAULT status. A drive/motor enters in
            ///fault when an error occurs. After a TS_ResetFault execution, most of the errors bits from Motion
            ///Error Register are cleared (set to 0), the Ready output (if present) is set to the ready level, the
            ///Error output (if present) is set to the no error level and the drive/motor returns to normal operation.
            /// </summary>
            /// <param name="axis">Axis description object to reset</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int SetTargetPositionToActual(IBaseAxis axis)
            {
                m_ErrorStatus = ErrorCodesList.OK;
                m_ErrorMessage = String.Empty;

                if (null == axis) return m_ErrorStatus;
                if (!m_IsAxesInSimulatorMode)
                {
                    if (ErrorCodesList.OK != m_MotionHelper.SetTargetPositionToActual(axis))
                    {
                        return ErrorStatus;
                    }
                }
                return m_ErrorStatus;
            }

            /// <summary>
            /// The function gets out the active axis from the FAULT status. A drive/motor enters in
            ///fault when an error occurs. After a TS_ResetFault execution, most of the errors bits from Motion
            ///Error Register are cleared (set to 0), the Ready output (if present) is set to the ready level, the
            ///Error output (if present) is set to the no error level and the drive/motor returns to normal operation.
            /// </summary>
            /// <param name="axis_network_id">Axis network ID</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int SetTargetPositionToActual(Int32 axis_network_id)
            {
                return SetTargetPositionToActual(m_AxisCol.FindByNetworkID(axis_network_id));
            }


            /// <summary>
            /// Writes a fixed point type variable to the drive.
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="pszName">Name of the variable</param>
            /// <param name="value">Variable value</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public virtual int SetVariable(IBaseAxis axis, String pszName, Double value)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;
                

                try
                {
                    if (IsAxesInSimulatorMode)
                    {
                        return axis.SimulationSetVariable(pszName, value);
                    }
                    else
                    {
                        if (!TMLLib.TS_SetFixedVariable(pszName, value))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch { ErrorStatus = ErrorCodesList.FAILED; ;}
                finally { DeactivateAxis(); }

                return ErrorStatus;
            }

            /// <summary>
            ///  Writes a Int32 integer type variable to the drive.
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="pszName">Name of the variable</param>
            /// <param name="value">Variable value</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int SetVariable(IBaseAxis axis, String pszName, long value)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (IsAxesInSimulatorMode)
                    {
                        return axis.SimulationSetVariable(pszName, value);
                    }
                    else
                    {
                        if (!TMLLib.TS_SetLongVariable(pszName, (int)value))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch { ErrorStatus = ErrorCodesList.FAILED; ;}
                finally { DeactivateAxis(); }
                return ErrorStatus;
            }

            /// <summary>
            ///  Writes an integer type variable to the drive.
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="pszName">Name of the variable</param>
            /// <param name="value">Variable value</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int SetVariable(IBaseAxis axis, String pszName, Int16 value)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;
                try
                {
                    if (IsAxesInSimulatorMode)
                    {
                        return axis.SimulationSetVariable(pszName, value);
                    }
                    else
                    {
                        if (!TMLLib.TS_SetIntVariable(pszName, value))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch { ErrorStatus = ErrorCodesList.FAILED; ;}
                finally { DeactivateAxis(); }


                return ErrorStatus;
            }

            /// <summary>
            /// Reads an integer type variable from the drive.
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="pszName">Name of the variable</param>
            /// <param name="result">Variable value</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int GetIntVariable(IBaseAxis axis, String pszName, out Int16 result)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                result = -1;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;
                try
                {
                    if (IsAxesInSimulatorMode)
                    {
                        return axis.SimulationGetIntVariable(pszName, out result);
                    }
                    else
                    {
                        if (!TMLLib.TS_GetIntVariable(pszName, out result))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch{m_ErrorStatus = ErrorCodesList.FAILED;}
                finally { DeactivateAxis(); }
                 
                return m_ErrorStatus;
            }

            /// <summary>
            /// Get Input from port
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="nIO">Input port number to be read</param>
            /// <param name="Value">the input port status value (0 or 1)</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int GetInput(IBaseAxis axis, Byte nIO, out Byte result)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                result = 0;


                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (!IsAxesInSimulatorMode)
                    {
                        if (!TMLLib.TS_GetInput(nIO, out result))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                    else
                        result = 1;
                }
                catch{ErrorStatus = ErrorCodesList.FAILED; ;}
                finally{DeactivateAxis();}

                return ErrorStatus;
            }

            /// <summary>
            /// Set output port status.
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="nIO">Output port number to be written</param>
            /// <param name="value">Output port status value to be set (0 or 1)</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int SetOutput(IBaseAxis axis, Byte nIO, Byte value)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty; 

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (!IsAxesInSimulatorMode)
                    {
                        if (!TMLLib.TS_SetOutput(nIO, value))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch{m_ErrorStatus = ErrorCodesList.FAILED;}
                finally { DeactivateAxis(); }
                 
                return m_ErrorStatus;
            }

            /// <summary>
            /// Set output port status.
            /// </summary>
            /// <param name="axis_network_id">Axis Network Identifier</param>
            /// <param name="nIO">Output port number to be written</param>
            /// <param name="value">Output port status value to be set (0 or 1)</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int SetOutput(Int32 axis_network_id, Byte nIO, Byte value)
            {
                return SetOutput(m_AxisCol.FindByNetworkID(axis_network_id), nIO, value);
            }


            /// <summary>
            ///  Reads a fixed point type variable from the drive.
            ///  </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="pszName">Name of the variable</param>
            /// <param name="result">Variable value</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int GetDoubleVariable(IBaseAxis axis, String pszName, out Double result)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                result = -1;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (IsAxesInSimulatorMode)
                    {
                        result = axis.SimulationGetDoubleVariable(pszName, out result);
                    }
                    else
                    {
                        if (!TMLLib.TS_GetFixedVariable(pszName, out result))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED;
                            return ErrorStatus;
                        }
                    }
                }
                catch{ErrorStatus = ErrorCodesList.FAILED; }
                finally{DeactivateAxis();}
                 
                return ErrorStatus;
            }

            /// <summary>
            ///  Upload a data buffer from the drive (get it from motion chip's memory). 
            ///  </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="address">Start address where from to upload the data buffer</param>
            /// <param name="array_values">Buffer address where the uploaded data will be stored nSize: the number of words to upload</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int GetBuffer(IBaseAxis axis, ushort address, out ushort[] array_values)
            {
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;
                array_values = new ushort[1]; 

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (IsAxesInSimulatorMode)
                        return ErrorStatus;
                    else
                    {
                        if (!TMLLib.TS_GetBuffer(address, array_values, (ushort)(array_values.Length)))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED; ;
                            return ErrorStatus;
                        }
                    }
                }
                catch { ErrorStatus = ErrorCodesList.FAILED; ;}
                finally { DeactivateAxis(); }

                return ErrorStatus;
            }

            /// <summary>
            /// Download a data buffer to the drive's memory. 
            /// </summary>
            /// <param name="axis">Axis object</param>
            /// <param name="Address">Start address where to download the data buffer</param>
            /// <param name="ArrayValues">Buffer containing the data to be downloaded nSize: the number of words to download</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int SetBuffer(IBaseAxis axis, ushort Address, ushort[] ArrayValues)
            {
                
                ErrorStatus = ErrorCodesList.OK;
                ErrorMessage = String.Empty;

                if (ErrorCodesList.OK != ActivateAxis(axis))
                    return ErrorStatus;

                try
                {
                    if (IsAxesInSimulatorMode)
                        return ErrorStatus;
                    else
                    {
                        if (!TMLLib.TS_SetBuffer(Address, ArrayValues, (ushort)(ArrayValues.Length)))
                        {
                            m_AxisSetupHelper.UpdateLastError();
                            ErrorStatus = ErrorCodesList.FAILED;
                            return ErrorStatus;
                        }
                    }
                }
                catch{ErrorStatus = ErrorCodesList.FAILED;}
                finally { DeactivateAxis(); }

                return ErrorStatus;
            }




            /// <summary>
            /// Execute a cancelable call (CALLS) instruction on the drive.
            /// </summary>
            /// <param name="axis">Axis reference</param>
            /// <param name="func_name">name of the procedure to be executed</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
            public int CancelableCALL_Label(IBaseAxis axis, String func_name)
            {
                return m_AxisSetupHelper.CancelableCALL_Label(axis, func_name);
            }

            /// <summary>
            /// Execute a cancelable call (CALLS) instruction on the drive. And when wait while it complete.
            /// </summary>
            /// <param name="axis">Axis reference</param>
            /// <param name="func_name">name of the procedure to be executed</param>
            /// <param name="ct">Set Cancel request to asynchronous exit method</param> 
            /// <param name="timeout_ms">Set Timeout for this method in Milliseconds.  ; Set -1 for infinity </param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
            public int SyncCancelableCALL_Label(IBaseAxis axis, String func_name,
                                                CancellationToken ct = new CancellationToken(),
                                                Int32 timeout_ms = -1)
            {
                if (ErrorCodesList.OK != m_AxisSetupHelper.CancelableCALL_Label(axis, func_name))
                {
                    return ErrorStatus; 
                }

                if (ct.IsCancellationRequested) return ErrorStatus; 

                if (ErrorCodesList.OK != m_AxisSetupHelper.WaitHomingOrCallComplete(axis, timeout_ms, ct))
                {
                    return ErrorStatus; 
                }

                return ErrorStatus; 
            }


            /// <summary>
            /// Execute ABORT instruction on the drive (aborts execution of a procedure called with cancelable call instruction). And when  stops the motion.
            /// </summary>
            /// <param name="axis">Axis to stop label run on it</param>
            /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
            public int Abort_Call(IBaseAxis axis)
            {
                return m_AxisSetupHelper.CancelCallsAndStopMotion(axis, false);  
            }

        #endregion
   
    }
}
