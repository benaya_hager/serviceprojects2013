﻿using Motion_Controller_Common.Interfaces;
using NS_Common;
#if _WIN32
	using Motion_Controller.TML._32_BIT;
#else
    using Motion_Controller.TML._64_BIT;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Motion_Controller_Common.Service;
using Motion_Controller.Axis;
using NS_MotionController.Axis;
using System.Xml.Serialization;
using System.IO;
using NS_Common.Error_Handling;
using log4net;
using System.Threading;

namespace Motion_Controller.TML
{
    public class AxisSetupHelper
    {
        protected IMotionController m_MotionController;
        public ILog m_SystemLog;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="motion_controller">Reference to active motion controller</param>
        /// <param name="system_log">Reference to active system log manager</param>
        public AxisSetupHelper(IMotionController motion_controller, ILog system_log)
        {
            m_MotionController = motion_controller;
            m_SystemLog = system_log;
        }



        /// <summary>
        /// Check if all Axis got their power on
        /// </summary>
        /// <param name="AxisCol">Axis list (SingleAxisCollection)</param>
        /// <returns>Return true if all axis has power on, if one or more on power off returns false</returns>
        public  virtual Boolean CheckAllAxisON(AxisCollection axis_col)
        {
            bool IsAllActive = true;
            foreach (IBaseAxis axis in axis_col) //(IsAllActive && iIndex < AxisCol.Count)
            {
                if (axis.AxisOn_flag == 0)
                {
                    IsAllActive = false;
                    break;
                }
            }
            return IsAllActive;
        }

        /// <summary>
        /// Check if all Axis got their power on
        /// </summary>
        /// <param name="AxisCol">Axis list (SingleAxisCollection)</param>
        /// <returns>Return true if all axis has power on, if one or more on power off returns false</returns>
        public virtual Boolean CheckAxisON(IBaseAxis axis)
        {
            return (axis.AxisOn_flag != 0);
        }

        /// <summary>
        /// Set Technosoft properties for current position to specified in axis XML file values
        /// </summary>
        /// <param name="axis_col">Axis Collection</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int SetActualPositionToAxisStartValues(AxisCollection axis_col)
        {
            foreach (IBaseAxis axis in axis_col)
            {
                if (!axis.IsAxisInSimulatorMode)
                {
                    if (ErrorCodesList.OK != SetActualPositionToValue(axis, axis.Axis_StartPosition))
                        return m_MotionController.ErrorStatus;                      
                }
                else
                {
                    axis.SetSimulatorPosition(axis.Axis_StartPosition);
                }
            }

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Set Technosoft properties for current position to specified value
        /// </summary>
        /// <param name="axis">Axis to update</param>
        /// <param name="actual_position_value_IU">Position in IU</param>
        /// <returns></returns>
        public virtual int SetActualPositionToValue(IBaseAxis axis,
                                                   int actual_position_value_IU = 0)
        {   
            if (!axis.IsAxisInSimulatorMode)
            {
                if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;

                try
                {  
                     //	Set the actual motor position to 0 [position public units]
                    if (!TMLLib.TS_SetPosition(actual_position_value_IU))//;TMLLibDefs.AXIS_HOME_POSITION))
                    {
                        m_MotionController.ErrorMessage += String.Format ("Failed to set axis {0} actual position variable. As {1} position \n\r", axis.AxisName , actual_position_value_IU);

                        UpdateLastError();

                        m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_SET_ZERO_POSITION;
                        m_MotionController.DeactivateAxis();
                    }

                    if (!TMLLib.TS_SetLongVariable(axis.ActualPositionVariableName, actual_position_value_IU))
                    {
                        m_MotionController.ErrorMessage += String.Format("Failed Set actual axis position value to {0} ,  Axis {1} \n\r", actual_position_value_IU, axis.AxisName);

                        UpdateLastError();
                        m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_SET_POSITION;
                    }

                }
                catch { }
                finally
                {
                    m_MotionController.DeactivateAxis();
                }
            }
            else
            {
                axis.SetSimulatorPosition(axis.Axis_StartPosition);
            }
            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Wait while homing routine or call label complete complete. The SRL.8 bit should be updated 
        /// </summary>
        /// <param name="axis_col">axes collection to wait for</param>
        /// <param name="timeout_ms">Set Timeout for this method in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param> 
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int WaitHomingOrCallComplete(AxisCollection axis_col, Int32 timeout_ms, CancellationToken ct)
        {
            //	Wait until the "homing"\"label call" process is ended

            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();

            foreach (IBaseAxis axis in axis_col)
            {
                if (axis.IsAxisInSimulatorMode) continue;

                UInt16 HomingOrCall = 0;

                do
                {
                    if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                        return m_MotionController.ErrorStatus;
                    try
                    {

                        if (ErrorCodesList.OK !=  m_MotionController.ReadRegisterStatus  (TMLLib.REG_SRL, out HomingOrCall))
                            return m_MotionController.ErrorStatus;
                        
                    }
                    catch { }
                    finally { m_MotionController.DeactivateAxis(); }

                    HomingOrCall = (UInt16)((HomingOrCall & 1 << 8) == 0 ? 1 : 0);

                    //	If the homing procedure doesn't ends in MINUTES time then abort it
                    if ((timeout_ms >= 0) && (sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                    {
                        m_MotionController.ErrorMessage = String.Format("Timeout error on axis: {0};  \n\r The call not completed during {1} Milliseconds.", axis.AxisName, sw.Peek() / 10.0);
                        m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                        return ErrorCodesList.METHOD_TIMEOUT;
                    }
                }
                while (HomingOrCall == 0 && !ct.IsCancellationRequested); //	Wait for homing procedure to end
            }
            return m_MotionController.ErrorStatus;
        }


        /// <summary>
        /// Wait while homing routine or call label complete complete. The SRL.8 bit should be updated 
        /// </summary>
        /// <param name="axis">Axis to wait for</param>
        /// <param name="timeout_ms">Set Timeout for this method in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param> 
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int WaitHomingOrCallComplete(IBaseAxis axis, Int32 timeout_ms, CancellationToken ct)
        {
            if (axis.IsAxisInSimulatorMode) return ErrorCodesList.OK;

            //	Wait until the "homing"\"label call" process is ended
            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();

            UInt16 HomingOrCall = 0;

            do
            {
                if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
                try
                {
                    if (ErrorCodesList.OK != m_MotionController.ReadRegisterStatus(TMLLib.REG_SRL, out HomingOrCall))
                        return m_MotionController.ErrorStatus;
                }
                catch { }
                finally
                {
                    m_MotionController.DeactivateAxis();
                }

                HomingOrCall = (UInt16)((HomingOrCall & 1 << 8) == 0 ? 1 : 0);

                //	If the homing procedure doesn't ends in MINUTES time then abort it
                if ((timeout_ms >= 0) &&  (sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                {
                    m_MotionController.ErrorMessage = String.Format("Timeout error on axis: {0};  \n\r The call not completed during {1} Milliseconds.", axis.AxisName, sw.Peek() / 10.0);
                    m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                    return ErrorCodesList.METHOD_TIMEOUT;
                }
            }
            while (HomingOrCall == 0 && !ct.IsCancellationRequested); //	Wait for homing procedure to end

            return m_MotionController.ErrorStatus;
        }



        /// <summary>
        /// Check if axis power set to "ON". The SRL.15 bit should be updated 
        /// </summary>
        /// <param name="axis">Axis to wait for</param>
        /// <param name="timeout_ms">Set Timeout for this method in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param> 
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int CheckAxisPowerOnState(IBaseAxis axis, Int32 timeout_ms, CancellationToken ct)
        {
            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();

            UInt16 Status = 0; // stores the value of the Status Register Low from Axis
            Int32 iCounter = 0;
            
            while (axis.AxisOn_flag == 0)
            {
                if (iCounter > 0 || timeout_ms > 0)
                    System.Threading.Thread.Sleep(500);

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus; 

                
                // Check the status of the power stage
                if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    continue;
                else
                {
                    try
                    {
                        if (ErrorCodesList.OK !=  m_MotionController.ReadRegisterStatus(TMLLib.REG_SRL, out Status)  )
                        {
                            continue;
                        }
                    }
                    catch { }
                    finally { m_MotionController.DeactivateAxis(); }
                }

                axis.AxisOn_flag = Status;
                axis.AxisOn_flag = (UInt16)((axis.AxisOn_flag & 1 << 15) != 0 ? 1 : 0);



                if (timeout_ms > 0)
                {
                    if ((sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                    {
                        m_MotionController.ErrorMessage += String.Format("Failed to set axis {0} power state to 'ON' \n\r", axis.AxisName);

                        UpdateLastError();

                        m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                        break;
                    }
                }
                else
                {
                    iCounter++;
                    if (iCounter > 20)
                    {
                        m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_INIT_AXES;
                        break;
                    }
                }
            }
            return m_MotionController.ErrorStatus;
        }








        /// <summary>
        /// Initialize only Axis
        /// <summary>
        /// <param name="axis">Axis reference</param>
        /// <param name="homming_timeout_ms">Set Timeout for homing routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="initial_routine_timeout_ms">Set Timeout for Initialize routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param>
        /// <param name="isSendAxisHomeAfterInit"> Set true to perform homing and initial routines after axis power will be set to "ON"</param> 
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int InitAxis(IBaseAxis axis,
                            Int32 homming_timeout_ms,
                            Int32 initial_routine_timeout_ms,
                            CancellationToken ct = new CancellationToken(), 
                            Boolean isSendAxisHomeAfterInit = true)
        {
            if (axis == null)
                return ErrorCodesList.AXIS_DATA_NOT_SET_FAILED_TO_INIT;

            if (!axis.IsAxisInSimulatorMode)
            {
                //Load Setup file
                if (ErrorCodesList.OK != LoadAndSetAxisSetup(axis))
                    return m_MotionController.ErrorStatus;

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (ErrorCodesList.OK != DriveReset(axis))
                {
                    m_SystemLog.Error("Failed To reset Axis " + axis.AxisName);
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                //Set Power axis ON 
                if (ErrorCodesList.OK != SetPowerState(axis, 5000,ct,  true))
                    return m_MotionController.ErrorStatus;

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (ErrorCodesList.OK != CheckAxisPowerOnState(axis, homming_timeout_ms, ct))
                    return m_MotionController.ErrorStatus;
                
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (isSendAxisHomeAfterInit)
                {
                    System.Threading.Thread.Sleep(100);
                    
                    if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                    SendAxisToHomePosition(axis, homming_timeout_ms, initial_routine_timeout_ms, ct);
                }
                return m_MotionController.ErrorStatus;

            }
            else // Movement Simulation mode
            {
                axis.SetSimulatorPosition(axis.Axis_StartPosition);
            }
            return m_MotionController.ErrorStatus;
        }




        /// <summary>
        /// Init all Axis's with setup and sets power on
        /// </summary>
        /// <param name="axis_col">Axis list (SingleAxisCollection)</param>
        /// <param name="homming_timeout_ms">Set Timeout for homing routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="initial_routine_timeout_ms">Set Timeout for Initialize routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int InitAxes(AxisCollection axis_col,
                              Int32 homming_timeout_ms,
                              Int32 initial_routine_timeout_ms,
                              CancellationToken ct = new CancellationToken (),
                              Boolean isSendAllAxesHomeAfterInit = true)
        {
            if (axis_col == null) return ErrorCodesList.AXIS_DATA_NOT_SET_FAILED_TO_INIT;



            foreach (IBaseAxis axis in axis_col)
            {
                //Load Setup file
                if (ErrorCodesList.OK != LoadAndSetAxisSetup(axis))
                    return m_MotionController.ErrorStatus;
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
            }


            foreach (IBaseAxis axis in axis_col)
            {
                if (ErrorCodesList.OK != DriveReset(axis))
                {
                    m_SystemLog.Error("Failed To reset Axis " + axis.AxisName);
                }
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
            }

            if (ErrorCodesList.OK != SetPowerState(axis_col, axis_col.Count * 5000, ct, true))
                return m_MotionController.ErrorStatus;

            if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

            #region Send Axes To Home Position

            if (isSendAllAxesHomeAfterInit)
            {
                System.Threading.Thread.Sleep(100);

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                SendAxesToHomePosition(axis_col, homming_timeout_ms, initial_routine_timeout_ms, ct);
            }

            #endregion

            return m_MotionController.ErrorStatus;
        }


        /// <summary>
        /// Send selected axis to it's home position and when perform the initial function
        /// </summary>
        /// <param name="axis">Axis Object</param>
        /// <param name="homming_timeout_ms">Set Timeout for homing routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="initial_routine_timeout_ms">Set Timeout for Initialize routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int SendAxisToHomePosition(IBaseAxis axis,
                                          Int32 homming_timeout_ms,
                                          Int32 initial_routine_timeout_ms,
                                          CancellationToken ct)
        {
            if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
            if (ErrorCodesList.OK == CancelableCALL_Label(axis, axis.HomingRoutineName))
            {
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                if (ErrorCodesList.OK == WaitHomingOrCallComplete(axis, homming_timeout_ms, ct))
                {
                    if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                    if (ErrorCodesList.OK == SetActualPositionToValue(axis, axis.Axis_StartPosition ))
                    {
                        if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                        if (!String.Empty.Equals(axis.InitialRoutineName))
                            if (ErrorCodesList.OK == CancelableCALL_Label(axis, axis.InitialRoutineName))
                            {
                                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                                WaitHomingOrCallComplete(axis, initial_routine_timeout_ms, ct);
                            }
                    }
                }
            }
            
            if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
            CancelCallsAndStopMotion(axis, false);

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Send all axes in the list to their's home position and when perform the initial function
        /// </summary>
        /// <param name="axis_col">Axis list (AxisCollection)</param>
        /// <param name="homming_timeout_ms">Set Timeout for homing routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="initial_routine_timeout_ms">Set Timeout for Initialize routine in Milliseconds.  ; Set -1 for infinity </param>
        /// <param name="ct">Set Cancel request to asynchronous exit method</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int SendAxesToHomePosition(AxisCollection axis_col,
                                          Int32 homming_timeout_ms,
                                          Int32 initial_routine_timeout_ms,
                                          CancellationToken ct)
        {
            //Perform home routines for all loaded axes
            foreach (IBaseAxis axis in axis_col)
            {
                if (ErrorCodesList.OK != CancelableCALL_Label(axis, axis.HomingRoutineName))
                {
                    m_SystemLog.Error(String.Format("Failed To Send Axis {0} to Home Position \n\r", axis.AxisName));
                    return m_MotionController.ErrorStatus;
                }
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
            }

            //Wait axes motion completed and set current position initial values
            foreach (IBaseAxis axis in axis_col)
            {
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (ErrorCodesList.OK == WaitHomingOrCallComplete(axis, homming_timeout_ms, ct))
                {
                    if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                    if (ErrorCodesList.OK != SetActualPositionToValue(axis, axis.Axis_StartPosition))
                    {
                        m_SystemLog.Error(String.Format("Failed Set axis {0} start position value \n\r", axis.AxisName));
                        return m_MotionController.ErrorStatus;
                    }
                }
                else
                {
                    m_SystemLog.Error(String.Format("Failed while waiting Axis {0} Home routine complete \n\r", axis.AxisName));
                    return m_MotionController.ErrorStatus;
                }
            }

            if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

            //Perform Initial routine if exists
            foreach (IBaseAxis axis in axis_col)
            {
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (!String.Empty.Equals(axis.InitialRoutineName))
                    if (ErrorCodesList.OK != CancelableCALL_Label(axis, axis.InitialRoutineName))
                    {
                        return m_MotionController.ErrorStatus;
                    }
            }

            if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

            //Wait axes initial routine complete
            foreach (IBaseAxis axis in axis_col)
            {
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (ErrorCodesList.OK != WaitHomingOrCallComplete(axis, initial_routine_timeout_ms,ct))
                    return m_MotionController.ErrorStatus;
            }

            return m_MotionController.ErrorStatus;
        }



        /// <summary>
        /// Open the communication channel: COM1, RS232, 1, 115200
        /// </summary>
        /// <returns>True if successful, otherwise false</returns>
        public virtual int InitCommunicationChannel(ref TMLLibDefs defs)
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";

            if (m_MotionController.IsAxesInSimulatorMode) return m_MotionController.ErrorStatus;

            if (TMLLib.TS_OpenChannel(defs.ChannelName, defs.CHANNEL_TYPE, defs.HostID, defs.BAUDRATE) < 0)
            {
                UpdateLastError();
                m_MotionController.ErrorStatus = ErrorCodesList.COMMUNICATION_ERROR;
            }

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Close the communication channel.
        /// </summary>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int CloseCommunicationChannel(Boolean bCloseRS232Channel)
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";
            //	Select all the axes as the destination of the TML commands
            if (!TMLLib.TS_SelectBroadcast())
            {
                UpdateLastError();
                m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
            }

            if (!TMLLib.TS_Reset())
            {
                m_MotionController.ErrorMessage += String.Format("Failed to reset axes.  Broadcast mode selected! \n\r");

                UpdateLastError();

                m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
            }


            if (!TMLLib.TS_Power(TMLLib.POWER_OFF))
            {
                m_MotionController.ErrorMessage += String.Format("Failed to set axes power 'OFF'.  Broadcast mode selected! \n\r");

                UpdateLastError();

                m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
            }

            if (bCloseRS232Channel == true)
            {
                //	Close the communication channel
                TMLLib.TS_CloseChannel(-1);
            }
            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Save Axis list to Xml data file
        /// </summary>
        /// <param name="axis_col">The list of axis descriptors</param>
        /// <param name="FileName">Xml file name</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int SaveAxisXmlFile(AxisCollection axis_col, String FileName)
        {
            if (!File.Exists(FileName))
            {
                ExceptionsList.AxisXMLFileNotFound.MessageText = "The axis's file " + FileName + " not found";
                ExceptionsList.AxisXMLFileNotFound.ErrorID = ErrorCodesList.AXIS_DEFINITION_FILE_NOT_FOUND_ERROR;
                throw ExceptionsList.AxisXMLFileNotFound;
            }

            Axis2File MyList = new Axis2File();
            //Copy to data class good for serializing
            foreach (IBaseAxis tmpAxis in axis_col)
            {
                MyList.AddItem(new AxisXMLItem(tmpAxis));
            }
            //Serialization
            XmlSerializer Serializer = new XmlSerializer(typeof(Axis2File));
            TextWriter Writer = new StreamWriter(FileName);
            Serializer.Serialize(Writer, MyList);
            Writer.Close();
            MyList = null;

            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Loads and set axis setup file with Technosoft definitions and routines (Labels)
        /// </summary>
        /// <param name="axis">Axis to load</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int LoadAndSetAxisSetup(IBaseAxis axis)
        {
            //	Select the destination axis of the TML commands
            if (!m_MotionController.LockAxis(axis))
                return ErrorCodesList.FAILED_TO_SELECT_AXIS;
            try
            {
                //	Load the setup data generated for Axis 1. The setup data was previously created with EasySetUp/EasyMotion Studio
                axis.LoadedSetupID = TMLLib.TS_LoadSetup(axis.SetupFileName);
                if (axis.LoadedSetupID < 0)
                {
                    m_MotionController.DeactivateAxis();
                    return ErrorCodesList.FAILED_TO_LOAD_SETUP;
                }

                //	Setup the axis based on the setup data previously loaded
                if (!TMLLib.TS_SetupAxis(axis.NetworkIdentifier, axis.LoadedSetupID))
                {
                    m_MotionController.DeactivateAxis();
                    return ErrorCodesList.FAILED_TO_SET_SETUP;
                }

                TMLLib.TS_SelectAxis(axis.NetworkIdentifier);

                //	Execute the initialization of the drive (ENDINIT)
                if (!TMLLib.TS_DriveInitialisation())
                {
                    m_MotionController.DeactivateAxis();
                    return ErrorCodesList.FAILED_TO_INITIALIZE_DRIVE;
                }
            }
            catch { }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
            return ErrorCodesList.OK;
        }


        /// <summary>
        /// Execute ABORT instruction on the drive (aborts execution of a procedure called with cancelable call instruction). And when  stops the motion.
        /// </summary>
        /// <param name="axis">Axis to stop label run on it</param>
        /// <param name="reset_axis"> Indicate if needed to reset axis after stopping</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int CancelCallsAndStopMotion(IBaseAxis axis, Boolean reset_axis)
        {
            if (axis.IsAxisInSimulatorMode) return ErrorCodesList.OK; ;

            try
            {
                if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
                try
                {
                    //Execute ABORT instruction on the drive (aborts execution of a procedure called 
                    //with cancelable call instruction).
                    if (!TMLLib.TS_ABORT())
                    {
                        m_MotionController.ErrorMessage += String.Format("Failed to execute ABORT instruction on the drive {0} \n\r", axis.AxisName);

                        UpdateLastError();

                    }

                    if (!TMLLib.TS_Stop())
                    {
                        m_MotionController.ErrorMessage += String.Format("Failed to Stop the motion. On Axis {0} \n\r", axis.AxisName);

                        UpdateLastError();

                    }
                }
                catch { }
                finally { m_MotionController.DeactivateAxis(); }

                #region Reset Axis Fault's

                if (reset_axis && !axis.IsAxisInSimulatorMode)
                {
                    DriveReset(axis);
                }

                #endregion Reset Axis Fault's
            }
            catch
            {
            }
            finally { m_MotionController.DeactivateAxis(); }

            return ErrorCodesList.OK;
        }

        /// <summary>
        /// Execute ABORT instruction on the drives (aborts execution of a procedure called with cancelable call instruction). And when stops the motion.
        /// </summary>
        /// <param name="axis_col">List of axes</param>
        /// <param name="reset_axes">Indicate if needed to reset axes after stopping</param>
        /// <returns></returns>
        public virtual int CancelCallsAndStopMotion(AxisCollection axis_col, Boolean reset_axes)
        {
            int funcReturnCode = ErrorCodesList.OK;
            foreach (IBaseAxis axis in axis_col)
            {
                int tmpReturnCode = CancelCallsAndStopMotion(axis, reset_axes);
                if (ErrorCodesList.OK != tmpReturnCode)
                    funcReturnCode = tmpReturnCode;
            }
            return funcReturnCode;
        }

        /// <summary>
        /// Reset all setting s and initialize it from beginning.
        /// </summary>
        /// <param name="axis">Axis to reset</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int DriveReset(IBaseAxis axis, Int32 timeout = 5000, CancellationToken ct = new CancellationToken())
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";
            int CurPos = axis.ActualPositionIU;

            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus;

            try
            {
                //	Reset the drive
                if (!TMLLib.TS_Reset())
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to reset axis {0}  \n\r", axis.AxisName);

                    UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                    m_MotionController.DeactivateAxis();
                    return ErrorCodesList.FAILED;
                }

                System.Threading.Thread.Sleep(100);
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;  

                //	Set the serial communication baud rate to 9600. After reset the drive communicates using 9600 bps
                if (!TMLComm.MSK_SetBaudRate(9600))
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to set communication Baud Rate to 9600 on Axis {0} \n\r", axis.AxisName);

                    UpdateLastError();
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Restore the serial communication baud rate
                if (!TMLComm.MSK_SetBaudRate(TMLLibDefs.GetInstance().BAUDRATE))
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to set communication Baud Rate to {0} on Axis {1} \n\r", TMLLibDefs.GetInstance().BAUDRATE, axis.AxisName);

                    UpdateLastError();
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Execute the initialization of the drive (ENDINIT)
                if (!TMLLib.TS_DriveInitialisation())
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to execute initialization function (ENDINIT) on Axis {0} \n\r", axis.AxisName);

                    UpdateLastError();

                    m_MotionController.DeactivateAxis();
                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                    return ErrorCodesList.FAILED;
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Enable the power stage of the drive (AXISON)
                if (ErrorCodesList.OK != SetPowerState(axis, timeout, ct, true))
                    return m_MotionController.ErrorStatus;

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                //	Set the actual motor position to Previous stored position [position public units]
                if (ErrorCodesList.OK !=  SetActualPositionToValue(axis, CurPos))
                    return m_MotionController.ErrorStatus;
                
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally { m_MotionController.DeactivateAxis(); }


            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Reset All drives in the system, by selecting broadcast networking mode
        /// </summary>
        /// <param name="axis_col">All available axes</param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int ResetAllDrives(AxisCollection axis_col,  Int32 timeout = 5000, CancellationToken ct = new CancellationToken())
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = ""; 

            if (ErrorCodesList.OK != m_MotionController.ActivateBroadcast())
                return m_MotionController.ErrorStatus;

            try
            {
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Reset the drive
                if (!TMLLib.TS_Reset())
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to reset axis {0}  \n\r", "Broadcast");

                    UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                    m_MotionController.DeactivateAxis();
                    return ErrorCodesList.FAILED;
                }

                System.Threading.Thread.Sleep(100);
                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                //	Set the serial communication baud rate to 9600. After reset the drive communicates using 9600 bps
                if (!TMLComm.MSK_SetBaudRate(9600))
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to set communication Baud Rate to 9600 on Axis {0} \n\r", "Broadcast");

                    UpdateLastError();
                }


                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Restore the serial communication baud rate
                if (!TMLComm.MSK_SetBaudRate(TMLLibDefs.GetInstance().BAUDRATE))
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to set communication Baud Rate to {0} on Axis {1} \n\r", TMLLibDefs.GetInstance().BAUDRATE, "Broadcast");

                    UpdateLastError();
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
                //	Execute the initialization of the drive (ENDINIT)
                if (!TMLLib.TS_DriveInitialisation())
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to execute initialization function (ENDINIT) on Axis {0} \n\r", "Broadcast");

                    UpdateLastError();

                    m_MotionController.DeactivateAxis();
                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                    return ErrorCodesList.FAILED;
                }

                ////	Enable the power stage of the drive (AXISON)
                //if (!TMLLib.TS_Power(TMLLib.POWER_ON))
                //{
                //    m_MotionController.ErrorMessage += String.Format("Failed to Turn axis power 'On',  Axis {0} \n\r", "Broadcast");

                //    UpdateLastError();

                //    m_MotionController.DeactivateAxis();
                //    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                //    return ErrorCodesList.FAILED;
                //}

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;

                if (ErrorCodesList.OK != SetPowerState(axis_col, timeout, ct, true))
                    return m_MotionController.ErrorStatus;


            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally { m_MotionController.DeactivateAxis(); }


            return m_MotionController.ErrorStatus;
        }



        /// <summary>
        /// Controls the power stage (ON/OFF).
        /// </summary>
        /// <param name="axis">Axis reference</param>
        /// <param name="timeout">How long to wait while axis state changed</param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="target_power_state">TRUE -> Power ON the drive; FALSE -> Power OFF the drive</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public virtual int SetPowerState(IBaseAxis axis, Int32 timeout = 5000, CancellationToken ct = new CancellationToken(), Boolean target_power_state = true)
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";

            if (axis.IsAxisInSimulatorMode) return m_MotionController.ErrorStatus;

            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus;

            try
            {
                //	Enable the power stage of the drives (AXISON)
                if (!TMLLib.TS_Power(target_power_state == true ? TMLLib.POWER_ON : TMLLib.POWER_OFF))
                {
                    UpdateLastError();
                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_INIT_AXES;
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;  
                
                if (!target_power_state)
                {
                    axis.AxisOn_flag = 0;
                }
                else
                    m_MotionController.ErrorStatus = CheckAxisPowerOnState(axis,timeout, ct);
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_INIT_AXES; }
            finally { m_MotionController.DeactivateAxis(); }

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Sets all axes in collection power state to required value
        /// </summary>
        /// <param name="axis_col">Axis collection</param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="target_power_state">Set TRUE to set axis power "ON": false to set Axis power "OFF"</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int SetPowerState(AxisCollection axis_col, Int32 timeout = 5000,CancellationToken ct = new CancellationToken (), Boolean target_power_state = true)
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";

            if (m_MotionController.IsAxesInSimulatorMode) return m_MotionController.ErrorStatus;

            if (ErrorCodesList.OK != m_MotionController.ActivateBroadcast())
                return m_MotionController.ErrorStatus;

            try
            {
                //	Enable the power stage of the drives (AXISON)
                if (!TMLLib.TS_Power(target_power_state == true ? TMLLib.POWER_ON : TMLLib.POWER_OFF))
                {
                    UpdateLastError();
                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_INIT_AXES;
                    return ErrorCodesList.FAILED_TO_INIT_AXES;
                }

                foreach (IBaseAxis axis in axis_col)
                {
                    if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus;
  
                    if (!target_power_state)
                    {
                        axis.AxisOn_flag = 0;
                    }
                    else
                        m_MotionController.ErrorStatus = CheckAxisPowerOnState(axis, timeout,ct);
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_INIT_AXES; }
            finally { m_MotionController.DeactivateAxis(); }

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Execute a cancelable call (CALLS) instruction on the drive.
        /// </summary>
        /// <param name="axis">Axis reference</param>
        /// <param name="func_name">name of the procedure to be executed</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int CancelableCALL_Label(IBaseAxis axis, String func_name)
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";

            //Select Axis for TML commands
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus;

            try
            {
                if (!axis.IsAxisInSimulatorMode)
                {
                    if (!TMLLib.TS_CancelableCALL_Label(func_name)) //(0x406A))
                    {
                        m_MotionController.ErrorMessage += String.Format("Failed to execute a cancelable call ({0}) instruction on axis {1}  \n\r", func_name, axis.AxisName);
                        m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_RUN_ROUTINE_LABEL;
                        UpdateLastError();
                    }

                }
            }

            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_RUN_ROUTINE_LABEL; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
            return m_MotionController.ErrorStatus; ;
        }




        /// <summary>
        /// Description: The function gets out the active axis from the FAULT status. A drive/motor enters in
        ///fault when an error occurs. After a TS_ResetFault execution, most of the errors bits from Motion
        ///Error Register are cleared (set to 0), the Ready output (if present) is set to the ready level, the
        ///Error output (if present) is set to the no error level and the drive/motor returns to normal operation.
        /// </summary>
        /// <param name="axis">Axis reference</param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error </returns>
        public virtual int ResetAxisFault(IBaseAxis axis, Int32 timeout = 5000, CancellationToken ct = new CancellationToken())
        {
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";
            
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus;

            try
            {
                if (!TMLLib.TS_ResetFault())
                {
                    UpdateLastError();
                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED;
                    return m_MotionController.ErrorStatus;
                }
                
                if (ct.IsCancellationRequested )return m_MotionController.ErrorStatus;
 
                if (ErrorCodesList.OK != SetPowerState(axis, timeout , ct , true))
                    return m_MotionController.ErrorStatus;
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }

            return m_MotionController.ErrorStatus;

        }



        public virtual void UpdateLastError()
        {
#if _WIN32
            m_MotionController.ErrorMessage += TMLLib.TS_GetLastErrorText();
#else
            StringBuilder err = new StringBuilder(512);
            Int32 buffer_size = 512;

            TMLLib.TS_Basic_GetLastErrorText(err, buffer_size);
            Console.WriteLine(m_MotionController.ErrorMessage + err);

            m_MotionController.ErrorMessage += err.ToString();
#endif
        }
    }
}
