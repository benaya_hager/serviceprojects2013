using System;
using System.Text;
#if _WIN32
	using Motion_Controller.TML._32_BIT;
#else
    using Motion_Controller.TML._64_BIT;
#endif

using Motion_Controller_Common.Interfaces;
using Motion_Controller_Common.Service;
using NS_Common;
using log4net;
using System.Threading;



namespace Motion_Controller.TML
{
    public class MotionHelper
	{

        protected IMotionController m_MotionController;
        protected AxisSetupHelper m_AxisSetupHelper;
        protected ILog m_SystemLog;

        public MotionHelper(IMotionController motion_controller, AxisSetupHelper axis_setup_helper, ILog system_log)
        {
            m_MotionController = motion_controller;
            m_AxisSetupHelper = axis_setup_helper;
            m_SystemLog = system_log; 
        }


        #region Motion functions

        /// <summary>
        /// Move Relative with trapezoidal speed profile. This function allows you to program a position profile
        /// with a trapezoidal shape of the speed.
        /// </summary>
        /// <param name="Axis">The axis on witch user wants to perform movement</param>
        /// <param name="offset_IU">Destination position in Technosoft IU units</param>
        /// <param name="slew_speed_IU"> Slew Speed value in Technosoft Acceleration IU units</param>
        /// <param name="acc_IU">Acceleration rate in Technosoft Acceleration IU units</param>
        ///<param name="is_additive" ></param>
        /// <param name="move_moment"> Defines the moment when the position to reach</param>
        /// <param name="ref_base"></param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int TrapezoidalRelativeMove(IBaseAxis axis,
                                               Single offset_IU,
                                               Single slew_speed_IU,
                                               Single acc_IU,
                                               Boolean is_additive,
                                               MOVE_START_TYPE move_moment,
                                               MOVE_END_TYPE ref_base)
        {

            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus; 

            try
            {
                //Command the trapezoidal positioning using the prescribed parameters;
                if (!TMLLib.TS_MoveRelative(Convert.ToInt32(offset_IU),
                                            Math.Round(slew_speed_IU, 3),
                                            Math.Round(acc_IU, 4),
                                            is_additive,
                                            (short)move_moment,
                                            (short)ref_base))
                {
                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_MOVE_TO_REFERENCE;

                    m_MotionController.DeactivateAxis();
                    StopAxisMotion(axis);

                }
                else
                {
                    m_MotionController.ErrorStatus = ErrorCodesList.OK;
                    m_MotionController.ErrorMessage = "";
                }
            }
            catch {  m_MotionController.ErrorStatus =ErrorCodesList.FAILED; }
            finally { m_MotionController.DeactivateAxis(); }
            
            return m_MotionController.ErrorStatus;
        }


        /// <summary>
        /// Move Absolute with trapezoidal speed profile. This function allows you to program a position profile
        /// with a trapezoidal shape of the speed.
        /// </summary>
        /// <param name="axis">The axis on witch user wants to perform movement</param>
        /// <param name="dest_position_IU">Destination position in Technosoft IU units</param>
        /// <param name="slew_speed_IU"> Slew Speed value in Technosoft Acceleration IU units</param>
        /// <param name="acc_IU">Acceleration rate in Technosoft Acceleration IU units</param>
        /// <param name="move_moment"> Defines the moment when the position to reach</param>
        /// <param name="ref_base"></param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int TrapezoidalAbsoluteMove(IBaseAxis axis,
                                               Single dest_position_IU,
                                               Single slew_speed_IU,
                                               Single acc_IU,
                                               MOVE_START_TYPE move_moment,
                                               MOVE_END_TYPE ref_base)
        {
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                return m_MotionController.ErrorStatus;

            try
            {
                //Command the trapezoidal positioning using the prescribed parameters;
                if (!TMLLib.TS_MoveAbsolute(Convert.ToInt32(dest_position_IU),
                                            Math.Round(slew_speed_IU, 3),
                                            Math.Round(acc_IU, 4),
                                            (short)move_moment,
                                                (short)ref_base))
                {
                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_MOVE_TO_REFERENCE;

                    m_MotionController.DeactivateAxis();
                    StopAxisMotion(axis);
                }
                else
                {
                    m_MotionController.ErrorStatus = ErrorCodesList.OK;
                    m_MotionController.ErrorMessage = "";
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
            
            return m_MotionController.ErrorStatus;
        }


        /// <summary>
        ///  Function: Move at a given speed, with acceleration profile.
        /// </summary> 
        /// <param name="axis">The axis on witch user wants to perform movement</param>
        /// <param name="vel_speed_IU">Jogging speed</param>
        /// <param name="acc_IU">Acceleration  deceleration; if 0, use previously defined value</param>
        /// <param name="move_moment">UPDATE_NONE -> setup motion parameters, movement will start latter (on an Update command)
        ///		                    UPDATE_IMMEDIATE -> start moving immediate
        ///                 		UPDATE_ON_EVENT -> start moving on event</param>
        /// <param name="ref_ase">FROM_MEASURE -> the position reference starts from the actual measured position value
        /// 		                    FROM_REFERENCE -> the position reference starts from the actual reference position value</param>
        /// <param name="positive">Indicate the direction</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int ContinuesMove(IBaseAxis axis,
                                    Single vel_speed_IU,
                                    Single acc_IU,
                                    MOVE_START_TYPE move_moment,
                                    MOVE_END_TYPE ref_ase,
                                    Boolean positive)
        {
           if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;

            try
            {
                Double MovementSpeed = Math.Round((positive == true ? vel_speed_IU : vel_speed_IU * -1), 3);
                //Double MovementSpeed = (Positive == true ? 3.335 : 3.335 * -1);
                if (!TMLLib.TS_MoveVelocity(MovementSpeed,
                                            Math.Round(acc_IU, 4),
                                            (short)move_moment,
                                            (short)ref_ase))
                {
                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_MOVE_TO_REFERENCE;

                    m_MotionController.DeactivateAxis();
                    StopAxisMotion(axis);  
                }
                else
                {
                    m_MotionController.ErrorStatus = ErrorCodesList.OK;
                    m_MotionController.ErrorMessage = "";
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
             
            return m_MotionController.ErrorStatus;
        }



        /// <summary>
        /// Stop selected axis motion
        /// </summary>
        /// <param name="axis">The axis on witch user wants to perform action</param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int StopAxisMotion(IBaseAxis axis)
        {
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
            try
            {
                if (!TMLLib.TS_Stop())
                {

                    m_MotionController.ErrorMessage += String.Format("Failed to Stop the motion. On Axis {0} \n\r", axis.AxisName);

                    m_AxisSetupHelper.UpdateLastError();
                        
                    m_MotionController.ErrorStatus = ErrorCodesList.UNHANDLED_MOTION_CONTROLLER_ERROR;
                }
                else
                {
                    m_MotionController.ErrorStatus = ErrorCodesList.OK;
                    m_MotionController.ErrorMessage = "";
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally{m_MotionController.DeactivateAxis();}
             
            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        ///  Send stop command selected axis motion. And wait till motion really stopped
        /// </summary>
        /// <param name="axis">The axis on witch user wants to perform action</param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int SyncStopAxisMotion(IBaseAxis axis, CancellationToken ct, Int32 timeout_ms)
        {
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
            try
            {
                if (!TMLLib.TS_Stop())
                {
                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.UNHANDLED_MOTION_CONTROLLER_ERROR;

                    m_MotionController.DeactivateAxis();
                }
                else
                {
                    Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
                    sw.Reset();


                    ushort motion_completed;
                    do
                    {
                        //	Check the SRL.10 bit - Homing active flag
                        if (!TMLLib.TS_ReadStatus(TMLLib.REG_SRL, out motion_completed))
                        {
                            m_AxisSetupHelper.UpdateLastError();

                            m_MotionController.ErrorStatus = ErrorCodesList.UNHANDLED_MOTION_CONTROLLER_ERROR;
                            break;
                        }
                        System.Threading.Thread.Sleep(100);

                        //	If the homing procedure doesn't ends in MINUTES time then abort it
                        if ((timeout_ms >= 0) && (sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                        {
                            m_MotionController.ErrorMessage += String.Format("Axis {0} failed to stop \n\r", axis.AxisName);

                            m_AxisSetupHelper.UpdateLastError();

                            m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                            return ErrorCodesList.METHOD_TIMEOUT;
                        }

                    }
                    while (((UInt16)motion_completed & 1 << 10) == 0 || ct.IsCancellationRequested); //	Wait for motion procedure to end

                    if (!ct.IsCancellationRequested)
                    {
                        m_MotionController.ErrorStatus = ErrorCodesList.OK;
                        m_MotionController.ErrorMessage = "";
                    }
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            } 
            //Axis.AxisMemoryIncreased = false;
            return m_MotionController.ErrorStatus ;
        }

        /// <summary>
        /// Description: The function gets out the active axis from the FAULT status. A drive/motor enters in
        ///fault when an error occurs. After a TS_ResetFault execution, most of the errors bits from Motion
        ///Error Register are cleared (set to 0), the Ready output (if present) is set to the ready level, the
        ///Error output (if present) is set to the no error level and the drive/motor returns to normal operation.
        /// </summary>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int ResetAxisFault(IBaseAxis axis, CancellationToken ct, int timeout_ms)
        {
            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();
            m_MotionController.ErrorStatus = ErrorCodesList.OK;
            m_MotionController.ErrorMessage = "";

            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
            try
            {
                if (!TMLLib.TS_ResetFault())
                {
                    m_MotionController.ErrorMessage += String.Format("Failed to Reset axis {0} fault. \n\r", axis.AxisName);

                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.UNHANDLED_MOTION_CONTROLLER_ERROR;
                    return m_MotionController.ErrorStatus;
                }

                if (ct.IsCancellationRequested) return m_MotionController.ErrorStatus; 

                if (ErrorCodesList.OK != m_MotionController.SetPowerState(axis, timeout_ms, ct, true))
                    return m_MotionController.ErrorStatus;
               
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
             
            return m_MotionController.ErrorStatus ;
        }

        /// <summary>
        ///Description: The function sets the value of the target position (the position reference) to the
        ///value of the actual load position i.e. TPOS = APOS_LD. The command may be used in closed
        ///loop systems when the load/motor is still following a hard stop, to reposition the target position to
        ///the actual load position.
        /// </summary>
        /// <returns></returns>
        public int SetTargetPositionToActual(IBaseAxis axis)
        {
            if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;
            try
            {
                if (!TMLLib.TS_SetTargetPositionToActual())
                {
                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.FAILED_TO_SET_POSITION;

                    m_MotionController.DeactivateAxis();
                }
                else
                {
                    m_MotionController.ErrorStatus = ErrorCodesList.OK;
                    m_MotionController.ErrorMessage = "";
                }
            }
            catch { m_MotionController.ErrorStatus = ErrorCodesList.FAILED; }
            finally
            {
                m_MotionController.DeactivateAxis();
            }
         
            return m_MotionController.ErrorStatus;
        }

        #endregion Motion functions

        /// <summary>
        /// Waits axes position reached flag set to true. 
        /// </summary>
        /// <param name="axis_col">Axes Collection witch user want to wait motion complete</param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int WaitPositionReached(AxisCollection axis_col, CancellationToken ct, Int32 timeout_ms)
        {
            //	Wait until the "homing"\"label call" process is ended

            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();

            foreach (IBaseAxis axis in axis_col)
            {
                if (ct.IsCancellationRequested) break; 

                if (axis.IsAxisInSimulatorMode) continue;

                UInt16 position_reached ;

                do
                {
                    System.Threading.Thread.Sleep(100);
  
                    if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                        return m_MotionController.ErrorStatus;
                    
                    try
                    { 
                        if (ErrorCodesList.OK !=  m_MotionController.ReadRegisterStatus(TMLLib.REG_SRL, out position_reached) )
                            return m_MotionController.ErrorStatus;
                        
                    }
                    catch { return ErrorCodesList.FAILED_TO_READ_CURRENT_STATE; }
                    finally { m_MotionController.DeactivateAxis(); }

                    position_reached = (UInt16)((position_reached & 1 << 10) == 0 ? 1 : 0);

                    //	If the homing procedure doesn't ends in MINUTES time then abort it
                    if ((timeout_ms >= 0) && (sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                    {
                        m_MotionController.ErrorMessage += String.Format("Axis {0} failed to perform GoToStartPosition Method \n\r", axis.AxisName);

                        m_AxisSetupHelper.UpdateLastError();

                        m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                        return ErrorCodesList.METHOD_TIMEOUT;
                    }
                }
                while (position_reached != 0 || ct.IsCancellationRequested); //	Wait for homing procedure to end
            }

            return m_MotionController.ErrorStatus;
        }

        /// <summary>
        /// Waits axis position reached flag set to true. 
        /// </summary>
        /// <param name="axis">Axis that user wants to wait motion complete</param>
        /// <param name="ct">CancellationToken to stop waiting for method performing by request </param>
        /// <param name="timeout_ms"> Maximum waiting timeout in Milliseconds for method performing; Set -1 for Infinity </param>
        /// <returns>ErrorCodesList.OK  if no error; Other if error</returns>
        public int WaitPositionReached(IBaseAxis axis, CancellationToken ct, Int32 timeout_ms)
        {
            if (axis.IsAxisInSimulatorMode) return ErrorCodesList.OK;

            //	Wait until the "homing"\"label call" process is ended
            Tools.StopWatch.StopWatch sw = new Tools.StopWatch.StopWatch();
            sw.Reset();

            UInt16 motion_is_completed = 1;

            do
            {
                if (ErrorCodesList.OK != m_MotionController.ActivateAxis(axis))
                    return m_MotionController.ErrorStatus;

                try
                {
                    if (ErrorCodesList.OK !=  m_MotionController.ReadRegisterStatus (TMLLib.REG_SRL, out motion_is_completed))
                        return m_MotionController.ErrorStatus ;
                }
                catch { return ErrorCodesList.FAILED_TO_READ_CURRENT_STATE; }
                finally
                {
                    m_MotionController.DeactivateAxis();
                }

                motion_is_completed = (UInt16)((motion_is_completed & 1 << 10) == 0 ? 1 : 0);

                //	If the homing procedure doesn't ends in (timeout_ms) Milliseconds time then abort it
                if ((timeout_ms >= 0) && (sw.Peek() / 10.0)/*Milliseconds*/ >= timeout_ms)
                {
                    m_MotionController.ErrorMessage += String.Format("Timeout error on axis: {0}; Motion is Completed flag not set! \n\r The call not completed during {1} Milliseconds.", axis.AxisName, sw.Peek() / 10.0);

                    m_AxisSetupHelper.UpdateLastError();

                    m_MotionController.ErrorStatus = ErrorCodesList.METHOD_TIMEOUT;
                    return ErrorCodesList.METHOD_TIMEOUT;
                }
            }
            while (motion_is_completed != 0 || ct.IsCancellationRequested); //	Wait for homing procedure to end

            return ErrorCodesList.OK;
        }
    }
}