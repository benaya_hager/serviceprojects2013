#if _WIN32
    using Motion_Controller.TML._32_BIT;
#else
    using Motion_Controller.TML._64_BIT;
#endif

using Motion_Controller_Common.Interfaces;
using NS_Common;
using System;
using System.Xml.Serialization;
using Tools.CustomRegistry;



namespace Motion_Controller.TML
{
    [Serializable, XmlInclude(typeof(TMLLibDefs))]
    public class TMLLibDefs : ITMLLibDefs
    {
        #region Public Properties And Constants

        public const int FALSE = 0;
        public const int TRUE = 1;





        public const bool NO_ADDITIVE = false;
        public const bool ADDITIVE = true;

        public const bool WAIT_EVENT = true;
        public const bool NO_WAIT_EVENT = true;
        public const bool STOP_ON_EVENT = true;
        public const bool NO_STOP = false;

        public const bool OVER = true;
        public const bool UNDER = false;

        public const double NO_CHANGE_ACC = 0.0;
        public const double NO_CHANGE_SPD = 0.0;
         

        public const UInt16 SRL_ADDRESS = 0x090E;
        public const UInt16 MER_ADDRESS = 0x08FC;


        //Log Constants
        public const int LOG_SETS = 353;
        public const int NR_LOG_VARS = 4;
        public const UInt16 ADR_APOS_L = 0x228;
        public const UInt16 ADR_APOS_H = 0x229;
        public const UInt16 ADR_ASPD_L = 0x22C;
        public const UInt16 ADR_ASPD_H = 0x22D;


        public const double MILLIMETER_TO_IO_COEF = 1000;
        public const double MILLIMETER_SECOND_TO_IU_SLEW_COEF = 0.667;
        public const double MILLIMETER_SECOND_TO_IU_ACCELERATION_COEF = 0.0004;
        public const double MILLIMETER_SECOND_TO_IU_VELOCITY_COEF = 0.667;


        public const int AXIS_HOME_POSITION = 0;
        public const int MINIMUM_MOTION_POS = 0;
        public const int MAXIMUM_MOTION_POS = 20;
        public const int DEFAULT_SLEW_SPEED = 2;
        public const int DEFAULT_ACCELERATION_RATE = 2;
        public const int DEFAULT_MOVEMENT_VELOCITY_SPEED = 2;



        private int m_ErrorStatus = ErrorCodesList.OK;
        /// <summary>
        /// Get/Set the error status for controller
        /// </summary>
        public int ErrorStatus
        {
            get { return m_ErrorStatus; }
            set { m_ErrorStatus = value; }
        }

        [XmlIgnore]
        private String m_ChannelName = "COM1";
        /// <summary>
        /// Set/Get the Com channel name
        /// </summary>
        public String ChannelName
        {
            get { return m_ChannelName; }
            set { m_ChannelName = value; }
        }

        private byte m_HostID = 254;
        /// <summary>
        /// Set/Get the network Identifier for Master Microprocessor
        /// </summary>
        public byte HostID
        {
            get { return m_HostID; }
            set { m_HostID = value; }
        }

        public byte m_CHANNEL_TYPE = TMLLib.CHANNEL_RS232;
        /// <summary>
        /// Get the port connection type
        /// </summary>
        public byte CHANNEL_TYPE
        {
            get { return m_CHANNEL_TYPE; }
            set { m_CHANNEL_TYPE = value; }
        }

        public uint m_BAUDRATE = 115200;
        /// <summary>
        /// Get the port connection BAUD RATE
        /// </summary>
        public uint BAUDRATE
        {
            get { return m_BAUDRATE; }
            set { m_BAUDRATE = value; }
        }

        private int m_OnPinDefaultOffset = 10;
        /// <summary>
        /// The default offset from pin
        /// </summary>
        public int OnPinDefaultOffset
        {
            get { return m_OnPinDefaultOffset; }
            set { m_OnPinDefaultOffset = value; }
        }

        #endregion Public Properties And Constants

        #region Singleton implementation And Constructor

        private static TMLLibDefs m_Instance;

        /// <summary>
        /// Get the reference to the TMLLibDefs object.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>The reference to the TMLLibDefs object. There is only one Instance in the system </returns>
        public static TMLLibDefs GetInstance()
        {
            if (m_Instance == null)
                m_Instance = new TMLLibDefs();
            return m_Instance;
        }

        private TMLLibDefs()
        {
            ErrorStatus = InitRegistryData();
        }

        #endregion Singleton implimintation And Constructor

        #region Private methods

        /// <summary>
        /// Get base data from registry
        /// </summary>
        private int InitRegistryData()
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc("Motion Controller",
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

                HostID = objRegistry.GetRegKeyValue("Motion Controller Data", "HostID", m_HostID);
                objRegistry.SetRegKeyValue("Motion Controller Data", "HostID", HostID);

                ChannelName = objRegistry.GetRegKeyValue("Motion Controller Data", "ChannelName", m_ChannelName);
                objRegistry.SetRegKeyValue("Motion Controller Data", "ChannelName", ChannelName);

                OnPinDefaultOffset = objRegistry.GetRegKeyValue("Motion Controller Data", "OnPinDefaultOffset", m_OnPinDefaultOffset);
                objRegistry.SetRegKeyValue("Motion Controller Data", "OnPinDefaultOffset", OnPinDefaultOffset);
            }
            catch { };
            return ErrorCodesList.OK;
        }

        #endregion Private methods
    }
}