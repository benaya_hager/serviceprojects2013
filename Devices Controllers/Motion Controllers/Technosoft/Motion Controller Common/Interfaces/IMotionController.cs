﻿using Motion_Controller_Common.Service;
using NS_Common.Event_Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

namespace Motion_Controller_Common.Interfaces
{
    public interface IMotionController
    {
        int Abort_Call(IBaseAxis axis);
        int AbsTrapezoidalMove(IBaseAxis axis, float dest_pos_IU, float slew_speed_IU, float acc_IU, Motion_Controller_Common.Service.MOVE_START_TYPE move_moment, Motion_Controller_Common.Service.MOVE_END_TYPE ref_base);
        int AbsTrapezoidalSyncMove(IBaseAxis axis, float dest_pos_IU, float slew_speed_IU, float acc_IU, Motion_Controller_Common.Service.MOVE_START_TYPE move_moment, Motion_Controller_Common.Service.MOVE_END_TYPE ref_base, CancellationToken ct = new CancellationToken(), int timeout_ms = 60000);
        int ActivateAxis(IBaseAxis axis);
        int ActivateAxis(int NetworkAxisIdentifier);
        int ActivateBroadcast();
        Motion_Controller_Common.Service.AxisCollection AxisCol { get; }
        int CancelableCALL_Label(IBaseAxis axis, string func_name);
        int ContinuesMove(IBaseAxis axis, float slew_speed_IU, float acc_IU, Motion_Controller_Common.Service.MOVE_START_TYPE move_moment, Motion_Controller_Common.Service.MOVE_END_TYPE ref_base, bool positive);
        void DeactivateAxis();
        string ErrorMessage { get; set; }
        int ErrorStatus { get; set; }
        int GetBuffer(IBaseAxis axis, ushort address, out ushort[] array_values);
        int GetDoubleVariable(IBaseAxis axis, string pszName, out double result);
        int GetInput(IBaseAxis axis, byte nIO, out byte result);
        int GetIntVariable(IBaseAxis axis, string pszName, out short result);
        byte HorizontalJoystickAxisNetworkID { get; set; }
        bool IsAxesInSimulatorMode { get; set; }
        float JoystickHorAccelerationMM { get; set; }
        float JoystickHorSpeedMM { get; set; }
        float JoystickVerAccelerationMM { get; set; }
        float JoystickVerSpeedMM { get; set; }
        System.Windows.Window KeybourdActivitiesForm { get; set; }
        int LoadAxisXmlFile(ref Motion_Controller_Common.Service.AxisCollection axis_col, string FileName);
        bool LockAxis(IBaseAxis axis);

        void OnNoAxisDataAvailable(object Sender);
        int ReadRegisterStatus(short sel_index, out ushort tmpFlagHolder);
        void ReleaseController();
        int RelTrapezoidalMove(IBaseAxis axis, float offset_IU, float slew_speed_IU, float acc_IU, bool is_additive, Motion_Controller_Common.Service.MOVE_START_TYPE move_moment, Motion_Controller_Common.Service.MOVE_END_TYPE ref_base);
        int RelTrapezoidalSyncMove(IBaseAxis axis, float offset_IU, float slew_speed_IU, float acc_IU, Motion_Controller_Common.Service.MOVE_START_TYPE move_moment, Motion_Controller_Common.Service.MOVE_END_TYPE ref_base, bool is_additive, CancellationToken ct = new CancellationToken(), int timeout_ms = 60000);
        int ResetAllAxises(int timeout = -1, CancellationToken ct = new CancellationToken());
        int ResetAxis(IBaseAxis axis);
        int ResetAxisFault(IBaseAxis axis);
        int SendAxesHomePosition(Motion_Controller_Common.Service.AxisCollection axes_col, int homming_timeout_ms = -1, int initial_routine_timeout_ms = -1, CancellationToken ct = new CancellationToken());
        int SendAxis2HomePosition(IBaseAxis axis, int homming_timeout_ms = -1, int initial_routine_timeout_ms = -1, CancellationToken ct = new CancellationToken());
        int SetBuffer(IBaseAxis axis, ushort Address, ushort[] ArrayValues);
        int SetOutput(IBaseAxis axis, byte nIO, byte value);
        int SetOutput(int axis_network_id, byte nIO, byte value);
        int SetPowerState(IBaseAxis axis, int timeout = -1, CancellationToken ct = new CancellationToken(), bool target_power_state = true);
        int SetTargetPositionToActual(IBaseAxis axis);
        int SetTargetPositionToActual(int axis_network_id);
        int SetVariable(IBaseAxis axis, string pszName, double value);
        int SetVariable(IBaseAxis axis, string pszName, short value);
        int SetVariable(IBaseAxis axis, string pszName, long value);
        int StopAllAxisMotion();
        int StopAxisMotion(IBaseAxis axis);
        int StopAxisMotion(int axis_network_id);
        int SyncCancelableCALL_Label(IBaseAxis axis, string func_name, CancellationToken ct = new CancellationToken(), int timeout_ms = -1);
        int SyncStopAxisMotion(IBaseAxis axis, CancellationToken ct = new CancellationToken(), int timeout_ms = 2000);
        byte VericalJoystickAxisNetworkID { get; set; }
        string XmlAxisFile { get; set; }
    }
}
