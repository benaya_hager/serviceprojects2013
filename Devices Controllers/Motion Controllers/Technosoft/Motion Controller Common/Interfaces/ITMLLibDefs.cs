﻿using System;
namespace Motion_Controller_Common.Interfaces
{
    public interface ITMLLibDefs
    {
        uint BAUDRATE { get; set; }
        byte CHANNEL_TYPE { get; set; }
        string ChannelName { get; set; }
        int ErrorStatus { get; set; }
        byte HostID { get; set; }
        int OnPinDefaultOffset { get; set; }
    }
}
