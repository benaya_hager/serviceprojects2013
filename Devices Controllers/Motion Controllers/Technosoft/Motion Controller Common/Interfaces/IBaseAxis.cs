﻿using System;
using System.ComponentModel;
namespace Motion_Controller_Common.Interfaces
{
    public interface IBaseAxis : INotifyPropertyChanged, IDisposable
    {
        int ActualPositionIU { get; }
        double ActualPositionMM { get; }
        string ActualPositionVariableName { get; set; }
        int Axis_StartPosition { get; set; }
        string AxisName { get; set; }
        ushort AxisOn_flag { get; set; }
        bool Bit0_CANbusError { get; }
        bool Bit0_ENDINItExecuted { get; }
        bool Bit1_OverPositionTrigger1 { get; }
        bool Bit1_ShortCircuit { get; }
        bool Bit10_I2tMotorWarning { get; }
        bool Bit10_MotionIsCompleted { get; }
        bool Bit10_MotorOverTemp { get; }
        bool Bit11_DriveOverTemp { get; }
        bool Bit11_I2tDriveWarning { get; }
        bool Bit12_InGear { get; }
        bool Bit12_OverVoltage { get; }
        bool Bit13_UnderVoltage { get; }
        bool Bit14_CommandError { get; }
        bool Bit14_EventSetHasOccured { get; }
        bool Bit14_InCam { get; }
        bool Bit15_AxisIsON { get; }
        bool Bit15_EnableInputIsInactive { get; }
        bool Bit15_Fault { get; }
        bool Bit2_InvalidSetupData { get; }
        bool Bit2_OverPositionTrigger2 { get; }
        bool Bit3_ControlError { get; }
        bool Bit3_OverPositionTrigger3 { get; }
        bool Bit4_OverPositionTrigger4 { get; }
        bool Bit4_SerialCommError { get; }
        bool Bit5_AutorunEnabled { get; }
        bool Bit5_PositionWraparound { get; }
        bool Bit6_LSPActive { get; }
        bool Bit6_LSPEventOrInterrupt { get; }
        bool Bit7_HomingOrCallsWarning { get; }
        bool Bit7_LSNActive { get; }
        bool Bit7_LSNEventOrInterrupt { get; }
        bool Bit8_CaptureEventOrInterrupt { get; }
        bool Bit8_HomingOrCallsActive { get; }
        bool Bit8_OverCurrent { get; }
        bool Bit9_I2TProtectionError { get; }
        bool Bit9_TargetReached { get; }

        string HomingRoutineName { get; set; }
        string InitialRoutineName { get; set; }
        bool InMotion { get; }
        bool IsAxisInSimulatorMode { get; }
        double IU2MM(double IU_Value);
        double IU2VelosityMMS(double VelosityIU_Value);
        double IUAcceleration2MMS(double AccelerationIU_Value);
        double IUSlew2MMS(double SlewIU_Value);
        float LastDestinationPositionIU { get; set; }
        int LoadedSetupID { get; set; }
        ushort MER_Result { get; }
        double Millimeter_To_IU_Coeff { get; set; }
        double MillimeterSecond_To_IU_AccelerationCoeff { get; set; }
        double MillimeterSecond_To_IU_SlewCoeff { get; set; }
        double MillimeterSecond_To_IU_VelocityCoeff { get; set; }
        int MM2IU(double MM_Value);
        double MMS2AccelerationIU(double AccelerationMMS_Value);
        double MMS2SlewIU(double SlewMMS_Value);
        double MMS2VelosityIU(double VelosityMMS_Value);
        string Name { get; }
        byte NetworkIdentifier { get; set; }
        void OnCurrentPositionChanged(double cur_pos_mm);
        void OnCurrentPositionChanged(double cur_pos_mm, double last_pos_mm);
        void OnFailedToReachTargetPosition(object Sender);
        void OnTargetPositionReached();
        int ReadAxisStateFlags();
        double ResetWaitPeriod { get; set; }
        void Send(string data);
        void SetSimulatorPosition(int position_iu);
        string SetupFileName { get; set; }
        void SimulateContinuousMotion(int slewSpeed);
        void SimulateMotion(int inDestinationPosition, float SlewSpeed);
        int SimulationGetDoubleVariable(string pszName, out double result);
        int SimulationGetIntVariable(string pszName, out short result);
        int SimulationSetVariable(string pszName, double value);
        int SimulationSetVariable(string pszName, short value);
        int SimulatorDestinationPosition { get; }
        ushort SRH_Result { get; }
        ushort SRL_Result { get; }
        void StartAxisMonitoring();
        void StopAxisMonitoring();
        void StopSimulationMotion();
        string Target_Position_Variable_Name { get; set; }
        long TargetReachedIUTolerance { get; set; }
        int UpdateAxisStatusTimeout { get; set; }
    }
}
