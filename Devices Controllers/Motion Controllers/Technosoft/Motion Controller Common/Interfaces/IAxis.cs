﻿using System;
using System.ComponentModel;
namespace Motion_Controller_Common.Interfaces
{
    public interface IAxis: INotifyPropertyChanged, IDisposable
    {
        int ActualPositionIU { get; set; }
        double ActualPositionMM { get; }
        string ActualPositionVariableName { get; set; }
        int Axis_StartPosition { get; set; }
        ushort AxisOn_flag { get; set; }
        short CleanEventsHandlers();
        IAxis Clone();
        IAxis Clone(IAxis NewSingleAxis);
        int DataBaseID { get; set; }
        event SFW.DataEventHandler DataReceived;
        int DisplayPosition { get; set; }
        string DisplayPositionDescriptor { get; set; }
        int Do_Homming { get; set; }
        bool InMotion { get; set; }
        bool IsAxisInSimulatorMode { get; set; }
        double IU2MM(double IU_Value);
        double IU2VelosityMMS(double VelosityIU_Value);
        double IUAcceleration2MMS(double AccelerationIU_Value);
        double IUSlew2MMS(double SlewIU_Value);
        float LastDestinationPositionIU { get; set; }
        int LastMotorPosition { get; }
        int LoadedSetupID { get; set; }
        string MANUAL_Function_Name { get; set; }
        double Millimeter_To_IU_Coeff { get; set; }
        double MillimeterSecond_To_IU_AccelerationCoeff { get; set; }
        double MillimeterSecond_To_IU_SlewCoeff { get; set; }
        double MillimeterSecond_To_IU_VelocityCoeff { get; set; }
        double MM2IU(double MM_Value);
        double MMS2AccelerationIU(double AccelerationMMS_Value);
        double MMS2SlewIU(double SlewMMS_Value);
        double MMS2VelosityIU(double VelosityMMS_Value);
        string Name { get; }
        byte NetworkIdentifier { get; set; }
        void OnCurrentPositionChanged(object Sender, int CurPos);
        void OnCurrentPositionChanged(object Sender, int CurPos, float Delta);
        void OnFailedToReachTargetPosition(object Sender);
        void OnNoMotion(object Sender);
        int PositionCheckTimeout { get; set; }
        double ResetWaitPeriod { get; set; }
        void Send(string data);
        string Set_Manual_Param_Name { get; set; }
        bool SetTargetPositionAsActual(bool do_memmory_increase = true);
        string SetupFileName { get; set; } 
        void SimulateContinuousMotion(int slewSpeed);
        void SimulateMotion(int inDestinationPosition, float SlewSpeed);
        double SimulationGetDoubleVariable(string pszName);
        short SimulationGetIntVariable(string pszName);
        bool SimulationSetVariable(string pszName, double value);
        bool SimulationSetVariable(string pszName, short value);
        int SimulatorDestinationPosition { get; }
        string AxisName { get; set; }
        string Speed_Variable_Name { get; set; }
        void StartAxisPositionWatcher(bool IsSingleAxisSystem);
        void StartAxisPositionWatcher(bool IsSingleAxisSystem, System.Threading.ThreadPriority PositionCheckThreadPriority);
        void StartPosMonitoring();
        string StartMotionFunc { get; set; }
        string STOP_Function_Name { get; set; }
        void StopAxisPositionWatcher();
        void StopPosMonitoring();
        void StopSimulationMotion();
        void SuspendedStartPosMonitoring(int WaitTimeOut);
        string Target_Position_Variable_Name { get; set; }
        long TargetReachedThreshold { get; set; }
        string ToString();
    }
}
