﻿using Motion_Controller_Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Motion_Controller_Common.Service
{
    public class TrpMoveDesc
    {
        #region Public Properties
        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private Single m_DestinationIU = 0;
        public Single DestinationIU { get { return m_DestinationIU; } set { m_DestinationIU = value; } }

        private Single m_SlewSpeedIU = 0;
        public Single SlewSpeedIU { get { return m_SlewSpeedIU; } set { m_SlewSpeedIU = value; } }

        private Single m_AccelerationIU = 0;
        public Single AccelerationIU { get { return m_AccelerationIU; } set { m_AccelerationIU = value; } }

        private MOVE_START_TYPE m_MoveMoment = MOVE_START_TYPE.UPDATE_IMMEDIATE;
        public MOVE_START_TYPE MoveMoment { get { return m_MoveMoment; } set { m_MoveMoment = value; } }

        private MOVE_END_TYPE m_ReferenceBase = MOVE_END_TYPE.FROM_REFERENCE;
        public MOVE_END_TYPE ReferenceBase { get { return m_ReferenceBase; } set { m_ReferenceBase = value; } }
        #endregion

        #region Constructors
        public TrpMoveDesc() { }

        public TrpMoveDesc(IBaseAxis axis)
            : this()
        {
            m_Axis = axis;
        }

        public TrpMoveDesc(IBaseAxis axis, Single inDestinationIU, Single inSlewSpeedIU, Single inAccelerationIU)
            : this(axis)
        {
            m_DestinationIU = inDestinationIU;
            m_SlewSpeedIU = inSlewSpeedIU;
            m_AccelerationIU = inAccelerationIU;
        }

        public TrpMoveDesc(IBaseAxis axis, Single inDestinationIU, Single inSlewSpeedIU, Single inAccelerationIU, MOVE_START_TYPE inMoveMoment, MOVE_END_TYPE inReferenceBase)
            : this(axis, inDestinationIU, inSlewSpeedIU, inAccelerationIU)
        {
            m_MoveMoment = inMoveMoment;
            m_ReferenceBase = inReferenceBase;
        }
        #endregion
    }
}
