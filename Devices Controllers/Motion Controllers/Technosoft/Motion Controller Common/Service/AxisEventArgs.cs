﻿using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Motion_Controller_Common.Service
{
    public class AxisEventArgs : EventArgs, IEventData
    {
        private Double m_CurPositionMM;
        /// <summary>
        /// Get/Set Current Axis Index position value
        /// </summary>
        public Double CurPositionMM
        {
            get { return m_CurPositionMM; }
            set { m_CurPositionMM = value; }
        }

        private Double m_Delta; 
        /// <summary>
        /// Get Current Axis movement delta from last measured position value
        /// </summary>
        public Double Delta
        {
            get { return m_Delta; }
            set { m_Delta = value; }
        }

        public AxisEventArgs(Double cur_pos_mm)
        {
            m_CurPositionMM = cur_pos_mm;
        }

        public AxisEventArgs(Double cur_pos_mm, Double delta)
        {
            m_CurPositionMM = cur_pos_mm;
            m_Delta = delta;
        }
    }
}
