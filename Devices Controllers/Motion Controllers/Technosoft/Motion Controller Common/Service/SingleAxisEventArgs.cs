﻿using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motion_Controller_Common.Service
{
    public class SingleAxisEventArgs : EventArgs, IEventData
    {
        private int m_CurPosition;

        /// <summary>
        /// Get/Set Current Axis Index position value
        /// </summary>
        public int CurPosition
        {
            get { return m_CurPosition; }
            set { m_CurPosition = value; }
        }

        private Single m_Delta; 
        /// <summary>
        /// Get Current Axis movement delta from last measured position value
        /// </summary>
        public Single Delta
        {
            get { return m_Delta; }
            set { m_Delta = value; }
        }

        public SingleAxisEventArgs(Int32 cur_pos)
        {
            m_CurPosition = cur_pos;
        }

        public SingleAxisEventArgs(Int32 cur_pos, Single delta)
        {
            m_CurPosition = cur_pos;
            m_Delta = delta;
        }
    }
}
