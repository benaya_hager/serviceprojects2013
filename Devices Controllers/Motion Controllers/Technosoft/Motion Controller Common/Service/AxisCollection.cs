using Motion_Controller_Common.Interfaces;
using NS_Common.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Motion_Controller_Common.Service
{
    /// <summary>
    /// Store the list of Axis's available in the system
    /// </summary>
    public class AxisCollection : AsyncTrulyObservableCollection<IBaseAxis>
    {

        #region Private members



        private IBaseAxis GetAxisByKey(int lIndex)
        {
            foreach (IBaseAxis axis in this)
                if (axis.NetworkIdentifier == lIndex)
                    return axis;
            return null;
        }


        #endregion

        #region Public functions


        public bool ContainsKey(int lNetworkID)
        {
            foreach (IBaseAxis axis in this)
                if (axis.NetworkIdentifier == lNetworkID)
                    return true;
            return false;
        }


        //Get/Set the element at specific Index
        /// <summary>
        /// Get/Set the element at specific Index
        /// </summary>
        /// <param name="index">The zero-based index of the element</param>
        /// <returns>The element stored in the List</returns>
        public new IBaseAxis this[int index]
        {
            get
            {
                return GetAxisByKey(index);
            }
            set
            {
                Remove(GetAxisByKey(index));
                base.Add(value);
            }
        }

        
        public IBaseAxis FindByNetworkID(Int32 NetworkAxisId)
        {
            return this[NetworkAxisId];
        }


        public IBaseAxis FindByName(String name)
        {
            foreach (IBaseAxis axis in this)
                if (name == axis.AxisName)
                    return axis;
            return null;
        }






        //Adds an item to the IBaseAxisCollection.
        /// <summary>
        ///  Adds an item to the IBaseAxisCollection.
        /// </summary>
        /// <param name="NewIBaseAxis">Creates a shallow copy of the current NewIBaseAxis.</param>
        /// <returns>The position into which the new element was inserted.</returns>
        public new int Add(IBaseAxis NewIBaseAxis)
        {
            base.Add(NewIBaseAxis);
            return NewIBaseAxis.NetworkIdentifier;
        }




        //Insert New IBaseAxis to IBaseAxis List
        /// <summary>
        ///  Replace the IBaseAxisCollection item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to be replaced.</param>
        /// <param name="value">The new IBaseAxis to be placed over the old one in the IBaseAxisCollection.</param>
        public void Replace(int index, IBaseAxis value)
        {
            this[index] = value;
        }

        //Remove IBaseAxis from IBaseAxis list
        /// <summary>
        /// Removes the first occurrence of a specific IBaseAxis object from the IBaseAxisCollection.
        /// </summary>
        /// <param name="value">The IBaseAxis Object to remove from the IBaseAxisCollection.</param>
        public new void Remove(IBaseAxis value)
        {
            if (this.ContainsKey(value.NetworkIdentifier))
            {
                base.Remove(value);
            }
        }

        /// <summary>
        /// Find item from  SortedList collection by RoutineIndex created in Add process
        /// </summary>
        /// <param name="Index">Routine Index to remove</param>
        public void Remove(Int32 Index)
        {
            if (this.ContainsKey(Index))
            { 
                base.Remove(GetAxisByKey(Index));
            }
        }

        #endregion Public functions

        #region Private/Protected Functions

        ////Check if item inserted is of right type
        ///// <summary>
        ///// Check if item inserted is of right type
        ///// </summary>
        ///// <param name="value">Added Item, if the type is wrong throw exception</param>
        //protected override void OnValidate(Object value)
        //{
        //    if (value.GetType() != typeof(IBaseAxis))
        //        throw new ArgumentException("value must be of type IBaseAxis.", "value");
        //}

        #endregion Private/Protected Functions
    }
}