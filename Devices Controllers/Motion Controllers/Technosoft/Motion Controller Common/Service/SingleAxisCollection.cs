using Motion_Controller_Common.Interfaces;
using NS_Common.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Motion_Controller_Common.Service
{
    /// <summary>
    /// Store the list of Axis's available in the system
    /// </summary>
    public class AxisCollection : AsyncTrulyObservableCollection<IAxis>
    {

        #region Private members



        private IAxis GetAxisByKey(int lIndex)
        {
            foreach (IAxis axis in this)
                if (axis.NetworkIdentifier == lIndex)
                    return axis;
            return null;
        }


        #endregion

        #region Public functions


        public bool ContainsKey(int lNetworkID)
        {
            foreach (IAxis axis in this)
                if (axis.NetworkIdentifier == lNetworkID)
                    return true;
            return false;
        }


        //Get/Set the element at specific Index
        /// <summary>
        /// Get/Set the element at specific Index
        /// </summary>
        /// <param name="index">The zero-based index of the element</param>
        /// <returns>The element stored in the List</returns>
        public new IAxis this[int index]
        {
            get
            {
                return GetAxisByKey(index);
            }
            set
            {
                Remove(GetAxisByKey(index));
                base.Add(value);
            }
        }

        public IAxis FindByID(Int32 axis_id)
        {
            foreach (IAxis axis in this)
                if (axis_id == axis.DataBaseID)
                    return axis;
            return null;
        }


        public IAxis FindByNetworkID(Int32 NetworkAxisId)
        {
            return this[NetworkAxisId];
        }


        public IAxis FindByName(String name)
        {
            foreach (IAxis axis in this)
                if (name == axis.AxisName)
                    return axis;
            return null;
        }






        //Adds an item to the IAxisCollection.
        /// <summary>
        ///  Adds an item to the IAxisCollection.
        /// </summary>
        /// <param name="NewIAxis">Creates a shallow copy of the current NewIAxis.</param>
        /// <returns>The position into which the new element was inserted.</returns>
        public new int Add(IAxis NewIAxis)
        {
            base.Add(NewIAxis);
            return NewIAxis.NetworkIdentifier;
        }




        //Insert New IAxis to IAxis List
        /// <summary>
        ///  Replace the IAxisCollection item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to be replaced.</param>
        /// <param name="value">The new IAxis to be placed over the old one in the IAxisCollection.</param>
        public void Replace(int index, IAxis value)
        {
            this[index] = value;
        }

        //Remove IAxis from IAxis list
        /// <summary>
        /// Removes the first occurrence of a specific IAxis object from the IAxisCollection.
        /// </summary>
        /// <param name="value">The IAxis Object to remove from the IAxisCollection.</param>
        public new void Remove(IAxis value)
        {
            if (this.ContainsKey(value.NetworkIdentifier))
            {
                base.Remove(value);
            }
        }

        /// <summary>
        /// Find item from  SortedList collection by RoutineIndex created in Add process
        /// </summary>
        /// <param name="Index">Routine Index to remove</param>
        public void Remove(Int32 Index)
        {
            if (this.ContainsKey(Index))
            { 
                base.Remove(GetAxisByKey(Index));
            }
        }

        #endregion Public functions

        #region Private/Protected Functions

        ////Check if item inserted is of right type
        ///// <summary>
        ///// Check if item inserted is of right type
        ///// </summary>
        ///// <param name="value">Added Item, if the type is wrong throw exception</param>
        //protected override void OnValidate(Object value)
        //{
        //    if (value.GetType() != typeof(IAxis))
        //        throw new ArgumentException("value must be of type IAxis.", "value");
        //}

        #endregion Private/Protected Functions
    }
}