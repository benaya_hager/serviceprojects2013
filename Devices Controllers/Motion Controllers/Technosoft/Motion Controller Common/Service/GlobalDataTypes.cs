using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Service
{

    /*Constants used as values for 'MoveMoment' parameters*/
        //public const Int16 UPDATE_NONE = -1;
        //public const Int16 UPDATE_ON_EVENT = 0;
        //public const Int16 UPDATE_IMMEDIATE = 1;

        //    /*Constants used for ReferenceBase*/
        //public const Int16 FROM_MEASURE = 0;
        //public const Int16 FROM_REFERENCE = 1;
		 
        //    /*Constants used for DecelerationType*/
        //public const Int16 S_CURVE_SPEED_PROFILE = 0;
        //public const Int16 TRAPEZOIDAL_SPEED_PROFILE = 1;



    #region Public Data Types

    public enum MOTION_CONTROL_TYPES
    {
        PostProcess = 0x0,
        JogMod = 0x1
    }

    public enum _CONTROL_OPEN_TYPE
    {
        ROUTINE_BUILDING = 0x0,
        CALLIBRATION_ON_PIN = 0x1
    }

    public enum _DEFAULT_CAN_BUS_ADDRESS
    {
        X_CAN_ADDRESS = 255,
        Y_CAN_ADDRESS = 254,
        Z_CAN_ADDRESS = 229
    }

    #endregion Public Data Types


    #region Public Types/Enums/Structures 4 motion definitions


    /// <summary>
    /// Constants used as values for 'MoveMoment' parameters
    /// </summary>
    public enum MOVE_POS_CALC_TYPE
    {
        /// <summary>
        ///FALSE -> No position increment is added to the target position
        /// </summary>
        NO_ADDITIVE = 0,
        /// <summary>
        /// TRUE -> Add the position increment to the position to reach set by the previous motion command
        /// </summary>
        ADITIVE = 1
    }

    /// <summary>
    /// Constants used as values for 'MoveMoment' parameters
    /// </summary>
    public enum MOVE_START_TYPE
    {
        /// <summary>
        /// Setup motion parameters, movement will start latter (on an Update command)
        /// </summary>
        UPDATE_NONE = -1,
        /// <summary>
        /// Start moving immediate
        /// </summary>
        UPDATE_IMMEDIATE = 0,
        /// <summary>
        /// Start moving on event
        /// </summary>
        UPDATE_ON_EVENT =1
    }

    /// <summary>
    /// Constants used for ReferenceBase
    /// </summary>
    public enum MOVE_END_TYPE
    {
        /// <summary>
        /// The position reference starts from the actual measured position value
        /// </summary>
        FROM_MEASURE = 0,
        /// <summary>
        /// The position reference starts from the actual reference position value
        /// </summary>
        FROM_REFERENCE = 1
    }

    /// <summary>
    /// Constants used for relative S-Curve motion mode (DecelerationType).
    /// </summary>
    public enum DECELERATION_TYPE
    {
        /// <summary>
        /// Smooth, using an S-curve speed profile
        /// </summary>
        S_CURVE_SPEED_PROFILE = 0,
        /// <summary>
        /// Fast, using a trapezoidal speed profile
        /// </summary>
        TRAPEZOIDAL_SPEED_PROFILE = 1
    }

    #endregion Public Types/Enums/Structures 4 motion definitions


   
}