﻿namespace Motion_Controller_Common.Events
{
    public enum TMCCommandTypes
    {
        [Tools.String_Enum.StringValue("Send specific axis to home position")]
        CT_SEND_AXIS_HOME,
        [Tools.String_Enum.StringValue("Send all axes  to home position")]
        CT_SEND_ALL_AXES_HOME,


        [Tools.String_Enum.StringValue("Move RELATIVE with trapezoidal speed profile")]
        CT_TRAPEZOIDAL_MOVE_RELATIVE,
        [Tools.String_Enum.StringValue("Move List of Axes RELATIVE with trapezoidal speed profile")]
        CT_TRAPEZOIDAL_MOVE_AXES_RELATIVE,
        [Tools.String_Enum.StringValue("Synchronized Move RELATIVE with trapezoidal speed profile")]
        CT_SYNC_TRAPEZOIDAL_MOVE_RELATIVE,
        [Tools.String_Enum.StringValue("Synchronized Move RELATIVE list of AXES with trapezoidal speed profile")]
        CT_SYNC_TRAPEZOIDAL_MOVE_AXES_RELATIVE,


        [Tools.String_Enum.StringValue("Move ABSOLUTE with trapezoidal speed profile")]
        CT_TRAPEZOIDAL_MOVE_ABSOLUTE,
        [Tools.String_Enum.StringValue("Move List of Axes ABSOLUTE with trapezoidal speed profile")]
        CT_TRAPEZOIDAL_MOVE_AXES_AVSOLUTE,
        [Tools.String_Enum.StringValue("Synchronized Move ABSOLUTE with trapezoidal speed profile")]
        CT_SYNC_TRAPEZOIDAL_MOVE_ABSOLUTE,
        [Tools.String_Enum.StringValue("Synchronized Move ABSOLUTE list of AXES with trapezoidal speed profile")]
        CT_SYNC_TRAPEZOIDAL_MOVE_AXES_ABSOLUTE,
        [Tools.String_Enum.StringValue("wait motion completed, target reached flag set to 1")]
        CT_WAIT_MOTION_COMPLETED,

        [Tools.String_Enum.StringValue("Stop axis motion")]
        CT_STOP_AXIS_MOTION,
        [Tools.String_Enum.StringValue("Stop axis motion, And wait action Axis Reached target")]
        CT_SYNC_STOP_AXIS_MOTION,
        [Tools.String_Enum.StringValue("Stop List of Axes motion")]
        CT_STOP_AXES_MOTION,
        [Tools.String_Enum.StringValue("Stop all axes motion")]
        CT_STOP_ALL_AXES_MOTION,


        [Tools.String_Enum.StringValue("Set the target position value equal to the actual position value")]
        CT_SET_TARGET_POSITION_TO_ACTUAL,
        [Tools.String_Enum.StringValue("This function clears most of the errors bits from Motion Error Register (MER).")]
        CT_RESET_AXIS_FAULT,


        [Tools.String_Enum.StringValue("Set output port status")]
        CT_SET_CONTROLLER_OUTPUT,
        [Tools.String_Enum.StringValue("Writes an integer type variable to the drive")]
        CT_SET_CONTROLLER_OUTPUT_INTEGER_VARIABLE,
        [Tools.String_Enum.StringValue("Writes a Int32 integer type variable to the drive.")]
        CT_SET_CONTROLLER_OUTPUT_LONG_VARIABLE,
        [Tools.String_Enum.StringValue("Writes a fixed point type variable to the drive.")]
        CT_SET_CONTROLLER_OUTPUT_DOUBLE_VARIABLE,
        [Tools.String_Enum.StringValue("Download a data buffer to the drive's memory. ")]
        CT_SET_CONTROLLER_OUTPUT_USHORT_BUFFER,
        [Tools.String_Enum.StringValue("Get Input from port")]
        CT_READ_CONTROLLER_INPUT,


        [Tools.String_Enum.StringValue("Set Joystick Keyboard Activity Form")]
        CT_SET_JOYSTICK_KEYBOARD_ACTIVITY_FORM,
        [Tools.String_Enum.StringValue("Joystick Parameters Updated")]
        CT_JOYSTICK_AXIS_MOVEMENT_PARAMETERS_UPDATED,
        [Tools.String_Enum.StringValue("Set Joystick Horizontal Axis NetworkID, Speed and Acceleration ")]
        CT_SET_JOYSTICK_HORIZONTAL_AXIS_MOVEMENT_PARAMETERS,
        [Tools.String_Enum.StringValue("Set Joystick Vertical Axis NetworkID, Speed and Acceleration ")]
        CT_SET_JOYSTICK_VERTICAL_AXIS_MOVEMENT_PARAMETERS,



        [Tools.String_Enum.StringValue("Update Security Parameters")]
        CT_UPDATE_SECURITY_PARAMETERS,
        [Tools.String_Enum.StringValue("Send Axes to Spittoon")]
        CT_SEND_AXES_TO_SPITTOON,
        [Tools.String_Enum.StringValue("Set Keys Hook")]
        CT_SET_KEYS_HOOK,
        [Tools.String_Enum.StringValue("Remove Keys Hook")]
        CT_REMOVE_KEYS_HOOK,


        [Tools.String_Enum.StringValue("Send Axis Current Position")]
        CT_SEND_AXIS_CURRENT_POSITION,


        [Tools.String_Enum.StringValue("Update RFT Motion Parameters")]
        CT_UPDATE_MOTION_PARAMETERS,
        [Tools.String_Enum.StringValue("RFT Move to Target Position (Absolute)")]
        CT_MOVE_TO_PRESET_TARGET_POSITION,
        [Tools.String_Enum.StringValue("RFT Move for preset distance (Relative)")]
        CT_MOVE_FOR_PRESET_DISTANCE,
        [Tools.String_Enum.StringValue("RFT Move Axis To Test Start Position")]
        CT_MOVE_AXIS_TO_TEST_START_POSITION,

        [Tools.String_Enum.StringValue("RFT Move Axis To preset Distance From Bottom plate")]
        CT_MOVE_AXIS_TO_DISTANCE_FROM_BOTTOM_PLATE,

        
        [Tools.String_Enum.StringValue("Update Clients 4 actual axis position")]
        CT_UPDATE_CLIENTS
    }
}