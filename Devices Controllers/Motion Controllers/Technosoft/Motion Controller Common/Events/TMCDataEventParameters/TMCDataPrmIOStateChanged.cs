﻿using System;

namespace Motion_Controller_Common.Events.NS_TMCDataEventParameters
{
    public class TMCDataPrmIOStateChanged : TMCDataEventParameters
    {
        #region Private Members

        public enum AfectedFieldType
        {
            NOT_SET,
            BYTE,
            INT,
            LONG,
            DOUBLE,
            BUFFER
        }

        #endregion Private Members

        #region Public Properties

        private AfectedFieldType m_AfectedField = AfectedFieldType.NOT_SET;
        public AfectedFieldType AfectedField
        {
            get { return m_AfectedField; }
            private set { m_AfectedField = value; }
        }

        private Int32 m_AxisNetworkID = -1;
        /// <summary>
        ///
        /// </summary>
        public Int32 AxisNetworkID
        {
            get { return m_AxisNetworkID; }
        }

        private String m_AxisName = "";
        /// <summary>
        ///
        /// </summary>
        public String AxisName
        {
            get { return m_AxisName; }
        }

        private Byte m_nIO;
        public Byte nIO
        {
            get { return m_nIO; }
            set { m_nIO = value; }
        }

        private String m_Variable_Name;
        public String Variable_Name
        {
            get { return m_Variable_Name; }
            set { m_Variable_Name = value; }
        }

        private ushort m_Address;
        public ushort Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }

        private Byte m_ByteValue2Set;
        public Byte ByteValue2Set
        {
            get { return m_ByteValue2Set; }
            set { m_ByteValue2Set = value; }
        }


        private Int16 m_IntValue2Set;
        public Int16 IntValue2Set
        {
            get { return m_IntValue2Set; }
            set { m_IntValue2Set = value; }
        }

        private long m_LongValue2Set;
        public long LongValue2Set
        {
            get { return m_LongValue2Set; }
            set { m_LongValue2Set = value; }
        }

        private Double m_DoubleValue2Set;
        public Double DoubleValue2Set
        {
            get { return m_DoubleValue2Set; }
            set { m_DoubleValue2Set = value; }
        }

        private ushort[] m_UshortBufferValue2Set;
        public ushort[] UshortBufferValue2Set
        {
            get { return m_UshortBufferValue2Set; }
            set { m_UshortBufferValue2Set = value; }
        }


        #endregion Public Properties

        #region Constructors

        public TMCDataPrmIOStateChanged(Int32 axis_network_id)
            : base()
        { m_AxisNetworkID = axis_network_id; }

        public TMCDataPrmIOStateChanged(Int32 axis_network_id, Byte io_number, Byte value2set)
            : this(axis_network_id)
        {
            m_AfectedField = AfectedFieldType.BYTE;
            m_nIO = io_number;
            m_ByteValue2Set = value2set;
        }

        public TMCDataPrmIOStateChanged(Int32 axis_network_id, String variable_name, Int16 value2set)
            : this(axis_network_id)
        {
            m_AfectedField = AfectedFieldType.INT;
            m_Variable_Name = variable_name;
            m_IntValue2Set = value2set;
        }

        public TMCDataPrmIOStateChanged(Int32 axis_network_id, String variable_name, long value2set)
            : this(axis_network_id)
        {
            m_AfectedField = AfectedFieldType.LONG;
            m_Variable_Name = variable_name;
            m_LongValue2Set = value2set;
        }

        public TMCDataPrmIOStateChanged(Int32 axis_network_id, String variable_name, Double value2set)
            : this(axis_network_id)
        {
            m_AfectedField = AfectedFieldType.DOUBLE;
            m_Variable_Name = variable_name;
            m_DoubleValue2Set = value2set;
        }

        public TMCDataPrmIOStateChanged(Int32 axis_network_id, ushort address, ushort[] value2set)
            : this(axis_network_id)
        {
            m_AfectedField = AfectedFieldType.BUFFER;
            m_Address = address;
            m_UshortBufferValue2Set = value2set;
        }

        #endregion Constructors
    }
}