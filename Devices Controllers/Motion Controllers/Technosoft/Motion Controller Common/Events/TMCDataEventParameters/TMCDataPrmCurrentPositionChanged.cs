﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.NS_TMCDataEventParameters
{
    public class TMCDataPrmCurrentPositionChanged : TMCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private IBaseAxis m_Axis = null;
        /// <summary>
        ///
        /// </summary>
        public IBaseAxis Axis
        {
            get { return m_Axis; }
        }


        public Int32 NetworkID
        {
            get
            {
                return (null != m_Axis ? m_Axis.NetworkIdentifier : -1);
            }
        }

        private double m_EventPos;
        public double EventPos
        {
            get
            {
                return m_EventPos;
            }
        }


        public double RealPos
        {
            get
            {
                return m_Axis.IU2MM(m_Axis.ActualPositionIU);
            }
        }

        #endregion Public Properties

        #region Constructors

        public TMCDataPrmCurrentPositionChanged(IBaseAxis axis)
            : base()
        {
            m_EventPos = axis.IU2MM(axis.ActualPositionIU);
            m_Axis = axis;
        }

        #endregion Constructors
    }
}