﻿

using Motion_Controller_Common.Interfaces;
using System;
namespace Motion_Controller_Common.Events.NS_TMCDataEventParameters
{
    public class TMCDataPrmTargetPositionReached : TMCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private IBaseAxis m_Axis = null;
        /// <summary>
        ///
        /// </summary>
        public IBaseAxis Axis
        {
            get { return m_Axis; }
        }



        #endregion Public Properties

        #region Constructors

        public TMCDataPrmTargetPositionReached(IBaseAxis axis)
            : base()
        {
            m_Axis = axis;
        }

        #endregion Constructors
    }
}