﻿using System;

namespace Motion_Controller_Common.Events.NS_TMCDataEventParameters
{
    public class TMCDataPrmUpdateClientsAxisCurrentPosition : TMCDataEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Double m_CurrentPosition;
        public Double CurrentPosition
        {
            get { return m_CurrentPosition; }
            set { m_CurrentPosition = value; }
        }


        #endregion Public Properties

        #region Constructors

        public TMCDataPrmUpdateClientsAxisCurrentPosition(Double current_position)
            : base()
        {
            m_CurrentPosition = current_position;
        }

        #endregion Constructors
    }
}