﻿using SFW;
using Motion_Controller_Common.Events.NS_TMCDataEventParameters;

namespace Motion_Controller_Common.Events
{
    public class TMCDataEvent : IEventData
    {
        #region Public Properties

        private TMCDataEventTypes m_DataType;
        public TMCDataEventTypes DataType
        {
            get { return m_DataType; }
            private set { m_DataType = value; }
        }

        private TMCDataEventParameters m_DataParameters;
        public TMCDataEventParameters DataParameters
        {
            get { return m_DataParameters; }
            private set { m_DataParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCDataEvent(TMCDataEventTypes data_type, TMCDataEventParameters prms)
        {
            m_DataType = data_type;
            m_DataParameters = prms;
        }

        #endregion Constructors
    }
}