﻿namespace Motion_Controller_Common.Events
{
    public enum TMCDataEventTypes
    {
        [Tools.String_Enum.StringValue("Current axis position is changed position")]
        ET_CURRENT_POSITION_CHANGED,
        [Tools.String_Enum.StringValue("Occurs when Easy Motion Studio Failed To Reach Target Position on selected axis.")]
        ET_FAILED_TO_REACH_TARGET_POSITION,
        [Tools.String_Enum.StringValue("There is no change for constant declared time on current position.")]
        ET_TARGET_POSITION_REACHED,
        [Tools.String_Enum.StringValue("Occurs when there is any change in controller IO.")]
        ET_IO_STATE_CHANGED,
        [Tools.String_Enum.StringValue("Send clients current axis position")]
        ET_SEND_CLIENTS_AXIS_CURRENT_POSITION
    }
}