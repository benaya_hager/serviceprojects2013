﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetByteOutput : TMCCommandEventParameters
    {
        #region Public Properties

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Int32 m_AxisNetworkID = -1;
        public Int32 AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private Byte m_nIO;
        public Byte nIO
        {
            get { return m_nIO; }
            set { m_nIO = value; }
        }

        private Byte m_Value2Set;
        public Byte Value2Set
        {
            get { return m_Value2Set; }
            set { m_Value2Set = value; }
        }



        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetByteOutput() { }

        public TMCCommandPrmSetByteOutput(Byte IOnumber, byte value2set)
            : this()
        {
            m_nIO = IOnumber;
            m_Value2Set = value2set;
        }

        public TMCCommandPrmSetByteOutput(Int32 axis_network_id, Byte IOnumber, byte value2set)
            : this(IOnumber, value2set)
        {
            m_AxisNetworkID = axis_network_id;
        }


        public TMCCommandPrmSetByteOutput(string axis_name, Byte IOnumber, byte value2set)
            : this(IOnumber, value2set)
        {
            m_AxisName = axis_name;
        }

        #endregion Constructors
    }
}