﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmRFTMoveToTarget : TMCCommandEventParameters
    {
        #region Public Properties

        protected IBaseAxis m_Axis = null;
        public IBaseAxis Axis
        {
            get { return m_Axis; }
            set { m_Axis = value; }
        }


        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_SlewSpeedMM = 0;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = 0;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoringer = true;
        public Boolean StartPosMonitoringer { get { return m_StartPosMonitoringer; } set { m_StartPosMonitoringer = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmRFTMoveToTarget() { }

        public TMCCommandPrmRFTMoveToTarget(Boolean start_logger_watcher)
            : this()
        {
            m_StartPosMonitoringer = start_logger_watcher;
        }

        public TMCCommandPrmRFTMoveToTarget(Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmRFTMoveToTarget(byte inAxisNetworkID,
                                            Single inSlewSpeedMM,
                                            Single inAccelerationMM,
                                            Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisNetworkID = inAxisNetworkID;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }


        public TMCCommandPrmRFTMoveToTarget(String inAxisName,
                                            Single inSlewSpeedMM,
                                            Single inAccelerationMM,
                                            Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisName = inAxisName;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        #endregion Constructors
    }
}