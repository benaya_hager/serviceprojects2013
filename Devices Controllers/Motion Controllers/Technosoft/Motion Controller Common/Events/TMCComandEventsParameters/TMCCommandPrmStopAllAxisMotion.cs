﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmStopAllAxisMotion : TMCCommandEventParameters
    {
        #region Public Properties

        private Boolean m_StopPosMonitoring = true;
        public Boolean StopPosMonitoring { get { return m_StopPosMonitoring; } set { m_StopPosMonitoring = value; } }



        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmStopAllAxisMotion() { }

        public TMCCommandPrmStopAllAxisMotion(Boolean stop_logger_watcher)
            : this()
        {
            m_StopPosMonitoring = stop_logger_watcher;
        }

        #endregion Constructors
    }
}