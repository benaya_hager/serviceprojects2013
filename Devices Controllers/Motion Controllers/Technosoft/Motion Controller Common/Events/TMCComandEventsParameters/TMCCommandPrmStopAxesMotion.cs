﻿using System;
namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmStopAxesMotion : TMCCommandEventParameters
    {
        #region Public Properties

        private Object[] m_Axes = null;
        public Object[] Axes { get { return m_Axes; } set { m_Axes = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmStopAxesMotion() { }

        public TMCCommandPrmStopAxesMotion(Object[] inAxes)
            : this()
        {
            m_Axes = inAxes;
        }


        #endregion Constructors
    }
}