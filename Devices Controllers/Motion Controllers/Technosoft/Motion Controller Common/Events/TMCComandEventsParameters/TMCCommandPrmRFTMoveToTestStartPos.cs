﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmRFTMoveToTestStartPos : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID
        {
            get { return m_AxisNetworkID; }
            set { m_AxisNetworkID = value; }
        }


        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_SlewSpeedMM = -1;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = -1;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoring = true;
        public Boolean StartPosMonitoring { get { return m_StartPosMonitoring; } set { m_StartPosMonitoring = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmRFTMoveToTestStartPos() { }

        public TMCCommandPrmRFTMoveToTestStartPos(Boolean start_logger_watcher)
            : this()
        {
            m_StartPosMonitoring = start_logger_watcher;
        }

        public TMCCommandPrmRFTMoveToTestStartPos(IBaseAxis inAxis,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean start_logger_watcher = true)
            : this(start_logger_watcher)
        {
            m_Axis = inAxis;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmRFTMoveToTestStartPos(byte inAxisID,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisNetworkID = inAxisID;
            m_StartPosMonitoring = start_logger_watcher;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmRFTMoveToTestStartPos(String inAxisName,
                                              Single inSlewSpeedMM = -1,
                                              Single inAccelerationMM = -1,
                                              Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisName = inAxisName;
            m_StartPosMonitoring = start_logger_watcher;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        #endregion Constructors
    }
}