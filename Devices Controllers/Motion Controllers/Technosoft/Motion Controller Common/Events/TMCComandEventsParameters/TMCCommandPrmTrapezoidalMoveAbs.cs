﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmTrapezoidalMoveAbs : TMCCommandEventParameters
    {
        #region Public Properties

        protected IBaseAxis m_Axis = null;
        public IBaseAxis Axis
        {
            get { return m_Axis; }
            set { m_Axis = value; }
        }


        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_DestinationMM = 0;
        public Single DestinationMM { get { return m_DestinationMM; } set { m_DestinationMM = value; } }

        private Single m_SlewSpeedMM = 0;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = 0;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoringer = true;
        public Boolean StartPosMonitoringer { get { return m_StartPosMonitoringer; } set { m_StartPosMonitoringer = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmTrapezoidalMoveAbs() { }

        public TMCCommandPrmTrapezoidalMoveAbs(Boolean start_logger_watcher)
            : this()
        {
            m_StartPosMonitoringer = start_logger_watcher;
        }

        public TMCCommandPrmTrapezoidalMoveAbs(IBaseAxis inAxis,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_Axis = inAxis;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveAbs(byte inAxisNetworkID,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisNetworkID = inAxisNetworkID;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }


        public TMCCommandPrmTrapezoidalMoveAbs(String inAxisName,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisName = inAxisName;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        #endregion Constructors
    }
}