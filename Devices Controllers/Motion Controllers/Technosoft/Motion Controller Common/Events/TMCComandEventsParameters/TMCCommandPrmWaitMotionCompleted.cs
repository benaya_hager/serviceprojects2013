﻿using Motion_Controller_Common.Interfaces;
using System;
using System.Threading;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmWaitMotionCompleted : TMCCommandEventParameters
    {
        #region Public Properties

        private Int32 m_Timeout = 0;
        public Int32 Timeout
        {
            get { return m_Timeout; }
            set { m_Timeout = value; }
        }


        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private String m_AxisName = "";
        public String AxisName
        {
            get { return m_AxisName; }
            set { m_AxisName = value; }
        }

        private Int32 m_AxisNetworkID = -1;
        public Int32 AxisNetworkID
        {
            get { return m_AxisNetworkID; }
            set { m_AxisNetworkID = value; }
        }

        private CancellationTokenSource m_CancellationTokenSource;
        public CancellationTokenSource cancellationTokenSource
        {
            get { return m_CancellationTokenSource; }
            set { m_CancellationTokenSource = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmWaitMotionCompleted() { }

        public TMCCommandPrmWaitMotionCompleted(CancellationTokenSource cancellation_token_source)
            : this()
        { m_CancellationTokenSource = cancellation_token_source; }


        public TMCCommandPrmWaitMotionCompleted(IBaseAxis axis,
                                                Int32 timeout,
                                               CancellationTokenSource cancellation_token_source = null)
            : this(cancellation_token_source)
        {
            m_Axis = axis;
            m_Timeout = timeout;
        }

        public TMCCommandPrmWaitMotionCompleted(String axis_name,
                                                Int32 timeout,
                                               CancellationTokenSource cancellation_token_source = null)
            : this(cancellation_token_source)
        {
            m_AxisName = axis_name;
            m_Timeout = timeout;
        }

        public TMCCommandPrmWaitMotionCompleted(Int32 axis_id,
                                                Int32 timeout,
                                               CancellationTokenSource cancellation_token_source = null)
            : this(cancellation_token_source)
        {
            m_AxisNetworkID = axis_id;
            m_Timeout = timeout;
        }

        #endregion Constructors
    }
}