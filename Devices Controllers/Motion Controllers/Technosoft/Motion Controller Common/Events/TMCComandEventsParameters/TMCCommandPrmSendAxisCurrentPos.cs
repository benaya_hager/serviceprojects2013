﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSendAxisCurrentPos : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisID = 0;
        public byte AxisID
        {
            get { return m_AxisID; }
            set { m_AxisID = value; }
        }


        private String m_AxisName = "";
        public String AxisName
        {
            get { return m_AxisName; }
            set { m_AxisName = value; }
        }


        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSendAxisCurrentPos() { }

        public TMCCommandPrmSendAxisCurrentPos(byte axis_id)
        {
            m_AxisID = axis_id;
        }

        public TMCCommandPrmSendAxisCurrentPos(String axis_name)
        {
            m_AxisName = axis_name;
        }

        public TMCCommandPrmSendAxisCurrentPos(IBaseAxis inAxis)
            : this()
        {
            m_Axis = inAxis;
        }


        #endregion Constructors
    }
}