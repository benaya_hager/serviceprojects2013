﻿using Motion_Controller_Common.Interfaces;
using System;
using System.Threading;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmTrapezoidalMoveAbsMM : TMCCommandEventParameters
    {
        #region Public Properties

        protected IBaseAxis m_Axis = null;
        public IBaseAxis Axis
        {
            get { return m_Axis; }
            set { m_Axis = value; }
        }


        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_DestinationMM = 0;
        public Single DestinationMM { get { return m_DestinationMM; } set { m_DestinationMM = value; } }

        private Single m_SlewSpeedMM = 0;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = 0;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoring = true;
        public Boolean StartPosMonitoring { get { return m_StartPosMonitoring; } set { m_StartPosMonitoring = value; } }

        private CancellationTokenSource m_CancellationTokenSource;
        public CancellationTokenSource cancellationTokenSource
        {
            get { return m_CancellationTokenSource; }
            set { m_CancellationTokenSource = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmTrapezoidalMoveAbsMM() { }

        public TMCCommandPrmTrapezoidalMoveAbsMM(Boolean start_logger_watcher)
            : this()
        {
            m_StartPosMonitoring = start_logger_watcher;
        }

        public TMCCommandPrmTrapezoidalMoveAbsMM(IBaseAxis inAxis,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_Axis = inAxis;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveAbsMM(byte inAxisNetworkID,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisNetworkID = inAxisNetworkID;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }


        public TMCCommandPrmTrapezoidalMoveAbsMM(String inAxisName,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM,
                                               Single inAccelerationMM,
                                               Boolean start_logger_watcher)
            : this(start_logger_watcher)
        {
            m_AxisName = inAxisName;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveAbsMM(String axis_name,
                                              Single offset_mm = -1,
                                              Single slew_speed_mm = -1,
                                              Single acceleration_mm = -1,
                                              Boolean start_logger_watcher = true,
                                               CancellationTokenSource cancellation_token_source = null)

            : this(axis_name, offset_mm, slew_speed_mm, acceleration_mm, start_logger_watcher)
        {
            m_CancellationTokenSource = cancellation_token_source;
        }

        #endregion Constructors
    }
}