﻿using System;
using System.Drawing;
using System.Windows; 

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmUpdateSecurity : TMCCommandEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Single m_XZJetPosition;
        public Single XZJetPosition { get { return m_XZJetPosition; } set { m_XZJetPosition = value; } }

        private Single m_XZCameraPosition;
        public Single XZCameraPosition { get { return m_XZCameraPosition; } set { m_XZCameraPosition = value; } }

        //private PointF m_RefPoint;
        //public PointF RefPoint { get { return m_RefPoint; } set { m_RefPoint = value; } }

        private Single m_PrintHeight;
        public Single PrintHeight { get { return m_PrintHeight; } set { m_PrintHeight = value; } }

        private Single m_PurgeHeight;
        public Single PurgeHeight { get { return m_PurgeHeight; } set { m_PurgeHeight = value; } }

        private Single m_FreeTravelHeight;
        public Single FreeTravelHeight { get { return m_FreeTravelHeight; } set { m_FreeTravelHeight = value; } }

        private Point m_BathPosition;
        public Point BathPosition { get { return m_BathPosition; } set { m_BathPosition = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmUpdateSecurity(Single xz_jet_position,
                                           Single xz_camera_position,
            //PointF ref_point,
                                           Single print_height,
                                           Single purge_height,
                                           Single free_travel_height,
                                           Point bath_position)
            : base()
        {
            m_XZJetPosition = xz_jet_position;
            m_XZCameraPosition = xz_camera_position;
            //m_RefPoint = ref_point;
            m_PrintHeight = print_height;
            m_PurgeHeight = purge_height;
            m_FreeTravelHeight = free_travel_height;
            m_BathPosition = bath_position;
        }

        #endregion Constructors
    }
}