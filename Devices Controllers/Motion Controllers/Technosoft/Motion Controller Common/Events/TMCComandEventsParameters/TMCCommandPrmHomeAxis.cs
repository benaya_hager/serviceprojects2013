﻿

using Motion_Controller_Common.Interfaces;
using System;
namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmHomeAxis : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmHomeAxis() { }

        public TMCCommandPrmHomeAxis(IBaseAxis inAxis)
            : this()
        {
            m_Axis = inAxis;
        }


        #endregion Constructors
    }
}