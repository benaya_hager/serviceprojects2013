﻿using System.Windows; 

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetKeyboardActivityForm : TMCCommandEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Window m_UserKeyboardActivityForm;
        public Window UserKeyboardActivityForm
        {
            get { return m_UserKeyboardActivityForm; }
            private set { m_UserKeyboardActivityForm = value; }
        }


        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetKeyboardActivityForm(Window user_keyboard_activity_form)
            : base()
        {
            m_UserKeyboardActivityForm = user_keyboard_activity_form;
        }

        #endregion Constructors
    }
}