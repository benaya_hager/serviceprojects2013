﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetIntOutput : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private String m_Variable_Name;
        public String Variable_Name
        {
            get { return m_Variable_Name; }
            set { m_Variable_Name = value; }
        }

        private Int16 m_Value2Set;
        public Int16 Value2Set
        {
            get { return m_Value2Set; }
            set { m_Value2Set = value; }
        }



        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetIntOutput() { }

        public TMCCommandPrmSetIntOutput(IBaseAxis inAxis, String variable_name, Int16 value2set)
            : this()
        {
            m_Axis = inAxis;
            m_Variable_Name = variable_name;
            m_Value2Set = value2set;
        }


        public TMCCommandPrmSetIntOutput(string axis_name, String variable_name, Int16 value2set)
            : this()
        {
            m_AxisName = axis_name;
            m_Variable_Name = variable_name;
            m_Value2Set = value2set;
        }

        #endregion Constructors
    }
}