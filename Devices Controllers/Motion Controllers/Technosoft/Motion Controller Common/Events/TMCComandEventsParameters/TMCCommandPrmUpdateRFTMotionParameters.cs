﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmUpdateRFTMotionParameters : TMCCommandEventParameters
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        private Single m_TestTarget;
        public Single TestTarget
        {
            get { return m_TestTarget; }
            set { m_TestTarget = value; }
        }

        private Single m_TestDistance;
        public Single TestDistance
        {
            get { return m_TestDistance; }
            set { m_TestDistance = value; }
        }

        private Single m_TestStartPosition;
        public Single TestStartPosition
        {
            get { return m_TestStartPosition; }
            set { m_TestStartPosition = value; }
        }


        private Single m_DistanceFromBottomJigPlate = -5;
        public Single DistanceFromBottomJigPlate
        {
            get { return m_DistanceFromBottomJigPlate; }
            set  { m_DistanceFromBottomJigPlate= value;  }
        }



        private Single m_StagePositionOnJigClose;
        public Single StagePositionOnJigClose
        {
            get { return m_StagePositionOnJigClose; }
            set { m_StagePositionOnJigClose = value; }
        }
        
        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmUpdateRFTMotionParameters(Single test_distance,
                                                      Single test_target,
                                                      Single distance_from_bottom_jig_plate,
                                                      Single stage_position_on_jig_close  ,
                                                      Single test_strat_position)
            : base()
        {
            m_TestTarget = test_target;
            m_TestDistance = test_distance;
            m_DistanceFromBottomJigPlate = distance_from_bottom_jig_plate;
            m_StagePositionOnJigClose = stage_position_on_jig_close;
            m_TestStartPosition = test_strat_position;

        }

        #endregion Constructors
    }
}