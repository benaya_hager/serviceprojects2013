﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetDoubleOutput : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private String m_Variable_Name;
        public String Variable_Name
        {
            get { return m_Variable_Name; }
            set { m_Variable_Name = value; }
        }

        private Double m_Value2Set;
        public Double Value2Set
        {
            get { return m_Value2Set; }
            set { m_Value2Set = value; }
        }



        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetDoubleOutput() { }

        public TMCCommandPrmSetDoubleOutput(IBaseAxis inAxis, String variable_name, Double value2set)
            : this()
        {
            m_Axis = inAxis;
            m_Variable_Name = variable_name;
            m_Value2Set = value2set;
        }


        public TMCCommandPrmSetDoubleOutput(string axis_name, String variable_name, Double value2set)
            : this()
        {
            m_AxisName = axis_name;
            m_Variable_Name = variable_name;
            m_Value2Set = value2set;
        }

        #endregion Constructors
    }
}