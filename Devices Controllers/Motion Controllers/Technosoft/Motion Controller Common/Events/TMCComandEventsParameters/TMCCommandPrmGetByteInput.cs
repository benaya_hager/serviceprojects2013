﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmGetByteInput : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private Byte m_nIO;
        public Byte nIO
        {
            get { return m_nIO; }
            set { m_nIO = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmGetByteInput() { }

        public TMCCommandPrmGetByteInput(IBaseAxis inAxis, Byte IOnumber)
            : this()
        {
            m_Axis = inAxis;
            m_nIO = IOnumber;
        }


        #endregion Constructors
    }
}