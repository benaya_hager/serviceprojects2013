﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmTrapezoidalMoveRel : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }


        private Single m_OffsetIU = -1;
        public Single OffsetIU { get { return m_OffsetIU; } set { m_OffsetIU = value; } }

        private Single m_SlewSpeedIU = -1;
        public Single SlewSpeedIU { get { return m_SlewSpeedIU; } set { m_SlewSpeedIU = value; } }

        private Single m_AccelerationIU = -1;
        public Single AccelerationIU { get { return m_AccelerationIU; } set { m_AccelerationIU = value; } }


        private Boolean m_StartPosMonitoring = true;
        public Boolean StartPosMonitoring { get { return m_StartPosMonitoring; } set { m_StartPosMonitoring = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmTrapezoidalMoveRel() { }

        public TMCCommandPrmTrapezoidalMoveRel(IBaseAxis inAxis, Boolean start_logger_watcher)
            : this()
        {
            m_Axis = inAxis;
            m_StartPosMonitoring = start_logger_watcher;
        }

        public TMCCommandPrmTrapezoidalMoveRel(IBaseAxis inAxis,
                                               Single inOffsetIU = -1,
                                               Single inSlewSpeedIU = -1,
                                               Single inAccelerationIU = -1,
                                               Boolean start_logger_watcher = true)
            : this(inAxis, start_logger_watcher)
        {
            m_OffsetIU = inOffsetIU;
            m_SlewSpeedIU = inSlewSpeedIU;
            m_AccelerationIU = inAccelerationIU;
        }

        public TMCCommandPrmTrapezoidalMoveRel(String inAxisName,
                                              Single inOffsetIU = -1,
                                              Single inSlewSpeedIU = -1,
                                              Single inAccelerationIU = -1,
                                              Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisName = inAxisName;
            m_StartPosMonitoring = start_logger_watcher;
            m_OffsetIU = inOffsetIU;
            m_SlewSpeedIU = inSlewSpeedIU;
            m_AccelerationIU = inAccelerationIU;
        }


        public TMCCommandPrmTrapezoidalMoveRel(byte inAxisID,
                                              Single inOffsetIU = -1,
                                              Single inSlewSpeedIU = -1,
                                              Single inAccelerationIU = -1,
                                              Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisNetworkID = inAxisID;
            m_StartPosMonitoring = start_logger_watcher;
            m_OffsetIU = inOffsetIU;
            m_SlewSpeedIU = inSlewSpeedIU;
            m_AccelerationIU = inAccelerationIU;
        }
        #endregion Constructors
    }
}