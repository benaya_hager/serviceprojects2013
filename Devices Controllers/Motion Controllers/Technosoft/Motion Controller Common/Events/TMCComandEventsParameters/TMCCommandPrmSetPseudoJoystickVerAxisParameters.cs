﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetPseudoJoystickVerAxisParameters : TMCCommandEventParameters
    {
        #region Public Properties

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName
        {
            get { return m_AxisName; }
            set { m_AxisName = value; }
        }

        private Single m_JoystickVerSpeedMM = 100;
        public Single JoystickVerSpeedMM
        {
            get { return m_JoystickVerSpeedMM; }
            set { m_JoystickVerSpeedMM = value; }
        }

        private Single m_JoystickVerAccelerationMM = 100;
        public Single JoystickVerAccelerationMM
        {
            get { return m_JoystickVerAccelerationMM; }
            set { m_JoystickVerAccelerationMM = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetPseudoJoystickVerAxisParameters() { }

        public TMCCommandPrmSetPseudoJoystickVerAxisParameters(Single joystick_ver_Speed_mm,
                                                                Single Joystick_ver_acceleration_mm)
            : this()
        {
            m_JoystickVerSpeedMM = joystick_ver_Speed_mm;
            m_JoystickVerAccelerationMM = Joystick_ver_acceleration_mm;
        }

        public TMCCommandPrmSetPseudoJoystickVerAxisParameters(byte axis_network_id,
                                                               Single joystick_ver_Speed_mm,
                                                               Single Joystick_ver_acceleration_mm)
            : this(joystick_ver_Speed_mm, Joystick_ver_acceleration_mm)
        {
            m_AxisNetworkID = axis_network_id;
        }

        public TMCCommandPrmSetPseudoJoystickVerAxisParameters(String axis_name,
                                                       Single joystick_ver_Speed_mm,
                                                       Single Joystick_ver_acceleration_mm)
            : this(joystick_ver_Speed_mm, Joystick_ver_acceleration_mm)
        {
            m_AxisName = axis_name;
        }


        #endregion Constructors
    }
}