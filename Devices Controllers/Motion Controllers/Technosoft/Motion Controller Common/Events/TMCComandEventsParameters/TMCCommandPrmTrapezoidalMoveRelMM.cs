﻿using Motion_Controller_Common.Interfaces;
using System;
using System.Threading;



namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmTrapezoidalMoveRelMM : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID
        {
            get { return m_AxisNetworkID; }
            set { m_AxisNetworkID = value; }
        }


        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }


        private Single m_OffsetMM = -1;
        public Single OffsetMM { get { return m_OffsetMM; } set { m_OffsetMM = value; } }

        private Single m_SlewSpeedMM = -1;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = -1;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoring = true;
        public Boolean StartPosMonitoring { get { return m_StartPosMonitoring; } set { m_StartPosMonitoring = value; } }


        private CancellationTokenSource m_CancellationTokenSource;
        public CancellationTokenSource cancellationTokenSource
        {
            get { return m_CancellationTokenSource; }
            set { m_CancellationTokenSource = value; }
        }


        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmTrapezoidalMoveRelMM() { }

        public TMCCommandPrmTrapezoidalMoveRelMM(IBaseAxis inAxis, Boolean start_logger_watcher)
            : this()
        {
            m_Axis = inAxis;
            m_StartPosMonitoring = start_logger_watcher;
        }

        public TMCCommandPrmTrapezoidalMoveRelMM(IBaseAxis inAxis,
                                               Single inOffsetMM = -1,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean start_logger_watcher = true)
            : this(inAxis, start_logger_watcher)
        {
            m_OffsetMM = inOffsetMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveRelMM(byte inAxisID,
                                               Single inOffsetMM = -1,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisNetworkID = inAxisID;
            m_StartPosMonitoring = start_logger_watcher;
            m_OffsetMM = inOffsetMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveRelMM(String inAxisName,
                                              Single inOffsetMM = -1,
                                              Single inSlewSpeedMM = -1,
                                              Single inAccelerationMM = -1,
                                              Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisName = inAxisName;
            m_StartPosMonitoring = start_logger_watcher;
            m_OffsetMM = inOffsetMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmTrapezoidalMoveRelMM(String axis_name,
                                              Single offset_mm = -1,
                                              Single slew_speed_mm = -1,
                                              Single acceleration_mm = -1,
                                              Boolean start_logger_watcher = true,
                                               CancellationTokenSource cancellation_token_source = null)

            : this(axis_name, offset_mm, slew_speed_mm, acceleration_mm, start_logger_watcher)
        {
            m_CancellationTokenSource = cancellation_token_source;
        }

        #endregion Constructors
    }
}