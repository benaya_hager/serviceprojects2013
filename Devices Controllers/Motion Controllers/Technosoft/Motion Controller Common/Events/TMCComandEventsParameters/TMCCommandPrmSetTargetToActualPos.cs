﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetTargetToActualPos : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID
        {
            get { return m_AxisNetworkID; }
            set { m_AxisNetworkID = value; }
        }

        private String m_AxisName = "";
        public String AxisName
        {
            get { return m_AxisName; }
            set { m_AxisName = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetTargetToActualPos() { }

        public TMCCommandPrmSetTargetToActualPos(IBaseAxis inAxis)
            : this()
        {
            m_Axis = inAxis;
        }

        public TMCCommandPrmSetTargetToActualPos(byte axis_id)
            : this()
        {
            m_AxisNetworkID = axis_id;
        }

        public TMCCommandPrmSetTargetToActualPos(String axis_name)
            : this()
        {
            m_AxisName = axis_name;
        }

        #endregion Constructors
    }
}