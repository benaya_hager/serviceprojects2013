﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetPseudoJoystickHorAxisParameters : TMCCommandEventParameters
    {
        #region Public Properties

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID { get { return m_AxisNetworkID; } set { m_AxisNetworkID = value; } }

        private String m_AxisName = "";
        public String AxisName
        {
            get { return m_AxisName; }
            set { m_AxisName = value; }
        }


        private Single m_JoystickHorSpeedMM = 100;
        public Single JoystickHorSpeedMM
        {
            get { return m_JoystickHorSpeedMM; }
            set { m_JoystickHorSpeedMM = value; }
        }

        private Single m_JoystickHorAccelerationMM = 100;
        public Single JoystickHorAccelerationMM
        {
            get { return m_JoystickHorAccelerationMM; }
            set { m_JoystickHorAccelerationMM = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetPseudoJoystickHorAxisParameters() { }

        public TMCCommandPrmSetPseudoJoystickHorAxisParameters(Single joystick_hor_Speed_mm,
                                                                Single Joystick_hor_acceleration_mm)
            : this()
        {
            m_JoystickHorSpeedMM = joystick_hor_Speed_mm;
            m_JoystickHorAccelerationMM = Joystick_hor_acceleration_mm;
        }

        public TMCCommandPrmSetPseudoJoystickHorAxisParameters(byte axis_network_id,
                                                               Single joystick_hor_Speed_mm,
                                                               Single Joystick_hor_acceleration_mm)
            : this(joystick_hor_Speed_mm, Joystick_hor_acceleration_mm)
        {
            m_AxisNetworkID = axis_network_id;
        }

        public TMCCommandPrmSetPseudoJoystickHorAxisParameters(String axis_name,
                                                       Single joystick_hor_Speed_mm,
                                                       Single Joystick_hor_acceleration_mm)
            : this(joystick_hor_Speed_mm, Joystick_hor_acceleration_mm)
        {
            m_AxisName = axis_name;
        }

        #endregion Constructors
    }
}