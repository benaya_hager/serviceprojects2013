﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmSetUshortBufferOutput : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private ushort m_Address;
        public ushort Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }

        private ushort[] m_Value2Set;
        public ushort[] Value2Set
        {
            get { return m_Value2Set; }
            set { m_Value2Set = value; }
        }



        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmSetUshortBufferOutput() { }

        public TMCCommandPrmSetUshortBufferOutput(IBaseAxis inAxis, ushort address, ushort[] value2set)
            : this()
        {
            m_Axis = inAxis;
            m_Address = address;
            m_Value2Set = value2set;
        }


        public TMCCommandPrmSetUshortBufferOutput(string axis_name, ushort address, ushort[] value2set)
            : this()
        {
            m_AxisName = axis_name;
            m_Address = address;
            m_Value2Set = value2set;
        }

        #endregion Constructors
    }
}