﻿using System;

namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmTrapezoidalMoveAxesRel : TMCCommandEventParameters
    {
        #region Public Properties


        private int[] m_AxesNetworkID;
        public int[] AxesNetworkID { get { return m_AxesNetworkID; } set { m_AxesNetworkID = value; } }


        private Double[] m_OffsetsIU;
        public Double[] OffsetsIU
        {
            get { return m_OffsetsIU; }
            private set { m_OffsetsIU = value; }
        }


        private Single[] m_SlewSpeedsIU;
        public Single[] SlewSpeedIU { get { return m_SlewSpeedsIU; } set { m_SlewSpeedsIU = value; } }

        private Single[] m_AccelerationsIU;
        public Single[] AccelerationsIU { get { return m_AccelerationsIU; } set { m_AccelerationsIU = value; } }

        private Boolean[] m_StartPosMonitoring;
        public Boolean[] StartPosMonitoring { get { return m_StartPosMonitoring; } set { m_StartPosMonitoring = value; } }

        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmTrapezoidalMoveAxesRel() { }

        public TMCCommandPrmTrapezoidalMoveAxesRel(int[] inAxesNetworkID,
                                                    Boolean[] start_logger_watcher)
            : this()
        {
            m_StartPosMonitoring = start_logger_watcher;
            m_AxesNetworkID = inAxesNetworkID;
        }

        public TMCCommandPrmTrapezoidalMoveAxesRel(int[] inAxesNetworkID,
                                                Double[] inOffsetsIU,
                                               Single[] inSlewSpeedsIU,
                                               Single[] inAccelerationsIU,
                                               Boolean[] start_logger_watcher)
            : this(inAxesNetworkID, start_logger_watcher)
        {
            m_OffsetsIU = inOffsetsIU;
            m_SlewSpeedsIU = inSlewSpeedsIU;
            m_AccelerationsIU = inAccelerationsIU;
        }

        #endregion Constructors
    }
}