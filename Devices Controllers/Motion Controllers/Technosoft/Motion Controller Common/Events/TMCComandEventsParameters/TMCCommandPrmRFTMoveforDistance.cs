﻿using Motion_Controller_Common.Interfaces;
using System;


namespace Motion_Controller_Common.Events.TMCComandEventsParameters
{
    public class TMCCommandPrmRFTMoveforDistance : TMCCommandEventParameters
    {
        #region Public Properties

        private IBaseAxis m_Axis = null;
        public IBaseAxis Axis { get { return m_Axis; } set { m_Axis = value; } }

        private byte m_AxisNetworkID = 0;
        public byte AxisNetworkID
        {
            get { return m_AxisNetworkID; }
            set { m_AxisNetworkID = value; }
        }


        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_SlewSpeedMM = -1;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = -1;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

        private Boolean m_StartPosMonitoringer = true;
        public Boolean StartPosMonitoringer { get { return m_StartPosMonitoringer; } set { m_StartPosMonitoringer = value; } }

        private Boolean m_Is2Direction = true;
        public Boolean Is2Direction
        {
            get { return m_Is2Direction; }
            set
            {
                m_Is2Direction = value;
            }
        }


        #endregion Public Properties

        #region Constructors

        public TMCCommandPrmRFTMoveforDistance() { }

        public TMCCommandPrmRFTMoveforDistance(Boolean start_logger_watcher)
            : this()
        {
            m_StartPosMonitoringer = start_logger_watcher;
        }

        public TMCCommandPrmRFTMoveforDistance(IBaseAxis axis,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean inIs2Direction = true,
                                                Boolean start_logger_watcher = true)
            : this(start_logger_watcher)
        {
            m_Axis = axis;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
            m_Is2Direction = inIs2Direction;
        }

        public TMCCommandPrmRFTMoveforDistance(byte inAxisID,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1,
                                               Boolean inIs2Direction = true,
                                               Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisNetworkID = inAxisID;
            m_StartPosMonitoringer = start_logger_watcher;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_Is2Direction = inIs2Direction; 
            m_AccelerationMM = inAccelerationMM;
        }

        public TMCCommandPrmRFTMoveforDistance(String inAxisName,
                                              Single inSlewSpeedMM = -1,
                                              Single inAccelerationMM = -1,
                                              Boolean inIs2Direction= true,
                                              Boolean start_logger_watcher = true)
            : this()
        {
            m_AxisName = inAxisName;
            m_StartPosMonitoringer = start_logger_watcher;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
            m_Is2Direction = inIs2Direction; 

        }

        #endregion Constructors
    }
}