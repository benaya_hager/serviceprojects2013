﻿using Motion_Controller_Common.Events.TMCComandEventsParameters;
using SFW;

namespace Motion_Controller_Common.Events
{
    public class TMCCommandEvent : IEventData
    {
        #region Public Properties

        private TMCCommandTypes m_cmdType;
        public TMCCommandTypes CmdType
        {
            get { return m_cmdType; }
            private set { m_cmdType = value; }
        }

        private TMCCommandEventParameters m_CommandParameters;
        public TMCCommandEventParameters CommandParameters
        {
            get { return m_CommandParameters; }
            private set { m_CommandParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public TMCCommandEvent(TMCCommandTypes incmdType, TMCCommandEventParameters prms)
        {
            m_cmdType = incmdType;
            m_CommandParameters = prms;
        }


        #endregion Constructors
    }
}