﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NS_Common;
using NS_Common.ViewModels.Common;
using System.Xml.Serialization;
using Motion_Controller_Common.Service;
using Motion_Controller.Axis;
using Motion_Controller_Common.Interfaces;


namespace Motion_Controller_UI.Sample
{
    public class SampleMotionControllerViewModel : ViewModelBase
    {

        private IMotionController m_MotionController = null; 

        private AxisCollection m_SystemAxesCollection = new AxisCollection();
        public AxisCollection SystemAxesCollection
        {
            get { return m_SystemAxesCollection; }
            set 
            { 
                m_SystemAxesCollection = value; 
            }
        }

        public SampleMotionControllerViewModel()
        { 
            OnCollectionChange("en-US");                    // Default buttons from English
        }

        private void OnCollectionChange(object lang)
        {
            m_SystemAxesCollection.Add(new BaseAxis( m_MotionController));
            m_SystemAxesCollection.Add(new BaseAxis(m_MotionController));
        } 
       
    }
}
