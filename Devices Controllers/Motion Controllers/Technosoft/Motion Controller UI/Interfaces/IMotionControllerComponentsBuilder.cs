﻿using System;
namespace Motion_Controller_UI.Interfaces
{
    public interface IMotionControllerComponentsBuilder
    {
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; set; }
        Microsoft.Practices.Unity.IUnityContainer Container { get; }
        GUIFactory GuiFactory { get; }
    }
}
