﻿using System;
using System.Collections.Generic; 
using SFW;
using System.Windows.Controls;
using System.Windows;
using NS_Common.ApplicationExtensions;
using System.ComponentModel;

namespace Motion_Controller_UI
{
    public class GUIFactory : RuntimeObjectsFactory
    {
        public GUIFactory(Dictionary<string, SFW.IComponent> components)
            : base(components)
        {   
        }

        public override SFW.IComponent CreateItem(string type)
        {
            return CreateItem(type, null);
        }

        public SFW.IComponent CreateItem(string type, Page OwnerWindow)
        {
            SFW.IComponent res = null;
            if (m_Components.ContainsKey(type))
                res = m_Components[type];
            else
            {
                switch (type)
                {
                    default:
                        {
                            res = null;
                            break;
                        }
                }
            }
            return res;
        }

        void GUIFactory_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {  
            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }

        public override void ReleaseItem(string type)
        {
            switch (type)
            {
                case "frmSystemLog":
                    {
                        m_Components.Remove("frmSystemLog");
                        break;
                    }
            }
        }


         


        private void CreateChildWindows(SFW.IComponent component)
        {
            System.Reflection.MethodInfo mi = component.GetType().GetMethod("CreateChildWindows");
            if (mi != null)
                mi.Invoke(component, new object[] { this });
        }
    }
}