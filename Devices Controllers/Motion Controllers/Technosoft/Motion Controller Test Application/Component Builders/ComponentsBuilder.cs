﻿using Microsoft.Practices.Unity;
using Motion_Controller_UI;
using Motion_Controller_UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Motion_Controller_Test_Application.Component_Builders
{
    public class ComponentsBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public void Bulid(IUnityContainer container)
        {

            Container = container;

            Container.RegisterInstance<IUnityContainer>(Container);

            Container.RegisterType<IMotionControllerComponentsBuilder, MotionControllerComponentsBuilder>(); //"DBManagerComponentsBuilder", new InjectionConstructor(Container));

            Container.RegisterInstance<IMotionControllerComponentsBuilder>(Container.Resolve<IMotionControllerComponentsBuilder>());
        }

        public IUnityContainer Container { set; get; }
    }
}
