﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NS_Common;
using SFW;
using Zaber_Common;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using Zaber_Common.Interfaces;
using Zaber_Motion_Controller.Axis; 

namespace NS_Zaber_Motion_Controller
{
    public class ZaberControllerListener : Listener,IZaberControllerListener
    {
        #region Private Members

        protected ILog m_Log;
       
        #endregion


        #region Public Properties

        private short m_ErrorStatus = ErrorCodesList.OK;
        /// <summary>
        /// Get/Set the error status for controller
        /// </summary>
        public short ErrorStatus
        {
            get { return m_ErrorStatus; }
            set { m_ErrorStatus = value; }
        }

        private String m_ErrorMessage = "";
        /// <summary>
        /// Get/Set the error status Message for controller
        /// </summary>
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }

        #endregion


        #region Constructors


        public ZaberControllerListener(ZaberMotionController device)
            : base(device)
        {
            m_Name = "ZaberMotionControllerListener";

            m_Device = device;

            m_Log = LogManager.GetLogger("MotionController");

            if (null != ((ZaberMotionController)m_Device).ZaberAxesCollection)
            {
                foreach (BaseZaberDevice axis in ((ZaberMotionController)m_Device).ZaberAxesCollection)
                {
                    axis.PropertyChanged += (s, e) => { HandlePropertyChanged(s, e); };
                }
            }
        }

       
        #endregion

        private void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs property_data)
        {
            SingleAxis axis = (sender as SingleAxis);
            if (null != axis)
            {
                switch (property_data.PropertyName)
                {
                    case "CurrentPositionMM":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_CURRENT_POSITION_CHANGED, new ZaberDataPrmCurPosChanged(sender, axis.DeviceID, axis.CurrentPositionMM)));
                        break;
                    case "CurrentSpeedMM":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_CURRENT_SPEED_CHANGED, new ZaberDataPrmCurPosChanged(sender, axis.DeviceID, axis.CurrentSpeedMM)));
                        break;
                    case "SystemNotifications":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_NEW_SYSTEM_MESSAGE, new ZaberDataPrmNotification(sender, axis.DeviceID, axis.Notification)));
                        break;
                    case "DeviceStatus":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_DEVICE_STATUS_CHANGED, new ZaberDataPrmDeviceStatus(sender, axis.DeviceID, axis.DeviceStatus)));
                        break;
                    case "DeviceStatusMonitorigActive":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_DEVICE_MONITORING_THREAD_STATUS_CHANGED, new ZaberDataPrmDeviceMonitoringThreadStatus(sender, axis.DeviceID, axis.DeviceStatusMonitorigActive )));
                        break;
                    case "AxisHomed":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_DEVICE_HOMED_STATUS_CHANGED, new ZaberDataPrmBoolean(sender, axis.DeviceID, axis.AxisHomed )));
                        break; 
                    case "MotionComplete":
                        SendUpdateEvent(new ZaberDataEvent(ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE, new ZaberDataPrmBoolean(sender, axis.DeviceID, axis.MotionComplete)));
                        break;
                }
            }
        }

         

        protected void SendUpdateEvent(ZaberDataEvent Event, Boolean isSync = false)
        {
            if (isSync)
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue(Event);

                RaiseEventGroupReady();
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    m_EventGroupData = new EventGroupData();

                    m_EventGroupData.Enqueue(Event);

                    RaiseEventGroupReady();
                });
            }
        }


        protected override void OnInputDeviceDataReceived(object sender, SFW.EventArgs<object> e)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue((IEventData)e.Value);

                // m_Log.Info("Sending " + m_LastMeasure);

                RaiseEventGroupReady();
            });
        }

    }
}
