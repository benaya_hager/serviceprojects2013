﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using NS_Common;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using Zaber_Common.Interfaces;
using Zaber_Common.Service;
using Zaber_Common;
using System.IO;

namespace NS_Zaber_Motion_Controller
{
    public class ZaberControllerLogic : Logic, IZaberControllerLogic
    {
        #region Protected Members

        protected ILog m_Log;

        protected ZaberMotionController m_MotionContollerDevice;
        protected ZaberControllerListener m_MotionContollerListener;

        #endregion

        #region Public Properties

        protected short m_ErrorStatus = ErrorCodesList.OK;
        /// <summary>
        /// Get/Set the error status for controller
        /// </summary>
        public short ErrorStatus
        {
            get { return m_ErrorStatus; }
            set { m_ErrorStatus = value; }
        }

        protected String m_ErrorMessage = "";
        /// <summary>
        /// Get/Set the error status Message for controller
        /// </summary>
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }

        #endregion

        #region Constructors

        public ZaberControllerLogic(ZaberMotionController device, ZaberControllerListener listener)
            : base()
        {
            m_MotionContollerDevice = device;
            m_MotionContollerListener = listener;
            m_Log = LogManager.GetLogger("MotionController");
        }
        
        #endregion

        #region Protected Methods

        protected override void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberCommandEvent), HandleZaberCommandEvent);
        }

        protected override void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(ZaberCommandEvent), out res); 
            }
        }

        protected virtual void HandleZaberCommandEvent(IEventData data)
        {
            if (data.GetType() != typeof(ZaberCommandEvent))
            {
                throw new ArgumentException("value must be of type ZaberCommandEvent.", "value");
            }

            switch (((ZaberCommandEvent)data).CmdType)
            {
                #region Update
                case ZaberCommandEventTypes.CT_UPDATE_AXIS_STATUS:
                    {
                        UpdateAxisStatus(((ZaberCommandPrmUpdateAxis)((ZaberCommandEvent)data).CommandParameters));
                        break;
                    }
                #endregion
                #region Home Events
                case ZaberCommandEventTypes.CT_SEND_AXIS_TO_HOME_POSITION:
                    {
                        SendAxisHome(((ZaberCommandEventParameters)((ZaberCommandEvent)data).CommandParameters));
                        break;
                    }
                #endregion Home Events

                #region Move Axis to target

               case ZaberCommandEventTypes.CT_MOVE_AXIS_ABSOLUTE:
                   {
                       AbsMoveAxis(((ZaberCommandPrmMoveAbsMM)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
               case ZaberCommandEventTypes.CT_SYNC_MOVE_AXIS_ABSOLUTE:
                   {
                       SyncAbsMoveAxis(((ZaberCommandPrmMoveAbsMM)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
               case ZaberCommandEventTypes.CT_SYNC_MOVE_AXES_ABSOLUTE:
                   {
                       SyncAbsMoveAxes(((ZaberCommandPrmMoveAxesAbsMM)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }

               case ZaberCommandEventTypes.CT_MOVE_AXIS_RELATIVE:
                   {
                       RelMoveAxis(((ZaberCommandPrmMoveRelMM)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
               case ZaberCommandEventTypes.CT_SYNC_MOVE_AXIS_RELATIVE:
                   {
                       SyncRelMoveAxis(((ZaberCommandPrmMoveRelMM)((ZaberCommandEvent)data).CommandParameters));
                       break; 
                   }
               case ZaberCommandEventTypes.CT_SYNC_MOVE_AXES_RELATIVE:
                   {
                       SyncRelMoveAxes(((ZaberCommandPrmMoveAxesRelMM)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
                #endregion Move Axis to target

                #region Stop Functions

               case ZaberCommandEventTypes.CT_STOP_AXIS_MOTION:
                   {
                       StopAxisMotion(((ZaberCommandPrmStopAxisMotion)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
               case ZaberCommandEventTypes.CT_STOP_AXES_MOTION:
                   {
                       StopAxesMotion(((ZaberCommandPrmStopAxesMotion)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
               case ZaberCommandEventTypes.CT_STOP_ALL_AXES_MOTION :
                   {
                       StopAllAxesMotion(((ZaberCommandEventParameters)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }

                #endregion Stop Functions

                #region Special Events

                case ZaberCommandEventTypes.CT_START_AXIS_STATUS_MONITORING:
                   {
                       StartAxisMonitoringThread(((ZaberCommandPrmAxisMonitoringThread)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
                case ZaberCommandEventTypes.CT_STOP_AXIS_STATUS_MONITORING:
                   {
                       StopAxisMonitoringThread(((ZaberCommandEventParameters)((ZaberCommandEvent)data).CommandParameters));
                       break;
                   }
            //    case ZaberCommandEventTypes.CT_RESET_AXIS_FAULT:
            //        {
            //            ResetAxisFault(((TMCCommandPrmResetAxisFault)((TMCCommandEvent)data).CommandParameters));
            //            break;
            //        }

                     //    case ZaberCommandEventTypes.CT_UPDATE_CLIENTS:
            //        {
            //            UpdateListenerClients(((TMCCommandEventParameters)((TMCCommandEvent)data).CommandParameters));
            //            break;

            //        }
               
                #endregion Special Events
            }
        }

        #endregion


        protected virtual void StartAxisMonitoringThread(ZaberCommandPrmAxisMonitoringThread zaberCommandEventParameters)
        {
            m_MotionContollerDevice.StartAxisMonitoringThread(zaberCommandEventParameters.DeviceID, zaberCommandEventParameters.Timeout );
        }

        protected virtual  void StopAxisMonitoringThread(ZaberCommandEventParameters zaberCommandEventParameters)
        {
            m_MotionContollerDevice.StopAxisMonitoringThread(zaberCommandEventParameters.DeviceID);
        }

        protected virtual  void AbsMoveAxis(ZaberCommandPrmMoveAbsMM zaberCommandPrmMoveAbsMM)
        {
            ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[zaberCommandPrmMoveAbsMM.DeviceID] as ISingleAxis;
            if (null != axis)
                m_MotionContollerDevice.MoveAxisAbsolute(zaberCommandPrmMoveAbsMM.DeviceID,
                                                         UnitsConverter.MM2IU(axis, zaberCommandPrmMoveAbsMM.DestinationMM), 
                                                         UnitsConverter.MMS2SlewIU(axis , zaberCommandPrmMoveAbsMM.SlewSpeedMM), 
                                                         UnitsConverter.MMS2AccelerationIU(axis,  zaberCommandPrmMoveAbsMM.AccelerationMM)); 
        }

        protected virtual void UpdateAxisStatus(ZaberCommandPrmUpdateAxis zaberCommandPrmMoveAbsMM)
        {
            BaseZaberDevice axis = m_MotionContollerDevice.ZaberAxesCollection[zaberCommandPrmMoveAbsMM.DeviceID] as BaseZaberDevice;
            m_MotionContollerDevice.UpdateAllAxes(axis);
        }

        protected virtual Boolean SyncAbsMoveAxis(ZaberCommandPrmMoveAbsMM zaberCommandPrmMoveAbsMM)
        {
            ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[zaberCommandPrmMoveAbsMM.DeviceID] as ISingleAxis;
            if (null != axis)
                m_MotionContollerDevice.MoveAxisAbsolute(zaberCommandPrmMoveAbsMM.DeviceID,
                                                         UnitsConverter.MM2IU(axis, zaberCommandPrmMoveAbsMM.DestinationMM),
                                                         UnitsConverter.MMS2SlewIU(axis, zaberCommandPrmMoveAbsMM.SlewSpeedMM),
                                                         UnitsConverter.MMS2AccelerationIU(axis, zaberCommandPrmMoveAbsMM.AccelerationMM));
            System.Threading.Thread.Sleep(100);
            if (m_MotionContollerDevice.WaitMotionCompleted(zaberCommandPrmMoveAbsMM.DeviceID, UnitsConverter.MM2IU(axis, zaberCommandPrmMoveAbsMM.DestinationMM), zaberCommandPrmMoveAbsMM.cancellationToken) == ErrorCodesList.OK)
                return true;
            else
                return false;
        }

        protected virtual Boolean SyncAbsMoveAxes(ZaberCommandPrmMoveAxesAbsMM zaberCommandPrmMoveAxesAbsMM)
        {
            Boolean bResult = true;
            int i = 0;
            Dictionary<Byte, Int32> destinations_list = new Dictionary<byte,int>() ; 
            foreach (Byte device_id in zaberCommandPrmMoveAxesAbsMM.DeviceIDs)
            {
                ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[device_id] as ISingleAxis;
                if (null != axis)
                {
                    destinations_list.Add(device_id,  UnitsConverter.MM2IU(axis, zaberCommandPrmMoveAxesAbsMM.DestinationsMM[i]));
                    m_MotionContollerDevice.MoveAxisAbsolute(device_id,
                                                             destinations_list[device_id],
                                                             UnitsConverter.MMS2SlewIU(axis, zaberCommandPrmMoveAxesAbsMM.SlewSpeedMM[i]),
                                                             UnitsConverter.MMS2AccelerationIU(axis, zaberCommandPrmMoveAxesAbsMM.AccelerationMM[i]));
                
                }
                i++;
            }

            System.Threading.Thread.Sleep(100);
            foreach (Byte device_id in zaberCommandPrmMoveAxesAbsMM.DeviceIDs)
            {
                bResult &= (m_MotionContollerDevice.WaitMotionCompleted(device_id, destinations_list[device_id], zaberCommandPrmMoveAxesAbsMM.cancellationToken) == ErrorCodesList.OK);
                if (zaberCommandPrmMoveAxesAbsMM.cancellationToken.IsCancellationRequested) break;   
            }
            return bResult;
        }


        protected virtual void RelMoveAxis(ZaberCommandPrmMoveRelMM zaberCommandPrmMoveRelMM)
        {
            ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[zaberCommandPrmMoveRelMM.DeviceID] as ISingleAxis;
            if (null != axis)
                m_MotionContollerDevice.MoveAxisRelative(zaberCommandPrmMoveRelMM.DeviceID,
                                                         UnitsConverter.MM2IU(axis, zaberCommandPrmMoveRelMM.OffsetMM),
                                                         UnitsConverter.MMS2SlewIU(axis, zaberCommandPrmMoveRelMM.SlewSpeedMM),
                                                         UnitsConverter.MMS2AccelerationIU(axis, zaberCommandPrmMoveRelMM.AccelerationMM));
        }

        protected virtual Boolean SyncRelMoveAxis(ZaberCommandPrmMoveRelMM zaberCommandPrmMoveRelMM)
        {
            ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[zaberCommandPrmMoveRelMM.DeviceID] as ISingleAxis;
            if (null != axis)
            {
                Int32 axisTargetpos =Convert.ToInt32(  axis.CurrentPositionIU + UnitsConverter.MM2IU(axis, zaberCommandPrmMoveRelMM.OffsetMM)) ;
                m_MotionContollerDevice.MoveAxisRelative(zaberCommandPrmMoveRelMM.DeviceID,
                                                         UnitsConverter.MM2IU(axis, zaberCommandPrmMoveRelMM.OffsetMM),
                                                         UnitsConverter.MMS2SlewIU(axis, zaberCommandPrmMoveRelMM.SlewSpeedMM),
                                                         UnitsConverter.MMS2AccelerationIU(axis, zaberCommandPrmMoveRelMM.AccelerationMM));

                
                
                System.Threading.Thread.Sleep(100);
                if (m_MotionContollerDevice.WaitMotionCompleted(zaberCommandPrmMoveRelMM.DeviceID, axisTargetpos, zaberCommandPrmMoveRelMM.cancellationToken) == ErrorCodesList.OK)
                    return true;
                else
                    return false;
            }
            return false;
        }

        protected virtual Boolean SyncRelMoveAxes(ZaberCommandPrmMoveAxesRelMM zaberCommandPrmMoveAxesRelMM)
        {
            Boolean bResult = true;
            int i = 0;
            Dictionary<Byte, Int32> destinations_list = new Dictionary<byte, int>(); 
            foreach (Byte device_id in zaberCommandPrmMoveAxesRelMM.DeviceIDs)
            {
                ISingleAxis axis = m_MotionContollerDevice.ZaberAxesCollection[device_id] as ISingleAxis;
                if (null != axis)
                {
                    destinations_list.Add(device_id,   Convert.ToInt32(axis.CurrentPositionIU + UnitsConverter.MM2IU(axis, zaberCommandPrmMoveAxesRelMM.OffsetsMM[i])));
                    m_MotionContollerDevice.MoveAxisRelative(device_id,
                                                             destinations_list[device_id],
                                                             UnitsConverter.MMS2SlewIU(axis, zaberCommandPrmMoveAxesRelMM.SlewSpeedMM[i]),
                                                             UnitsConverter.MMS2AccelerationIU(axis, zaberCommandPrmMoveAxesRelMM.AccelerationMM[i]));

                }
                i++;

            }

            System.Threading.Thread.Sleep(100);
            foreach (Byte device_id in zaberCommandPrmMoveAxesRelMM.DeviceIDs)
            {
                bResult &= (m_MotionContollerDevice.WaitMotionCompleted(device_id, destinations_list[device_id], zaberCommandPrmMoveAxesRelMM.cancellationToken) == ErrorCodesList.OK);
                if (zaberCommandPrmMoveAxesRelMM.cancellationToken.IsCancellationRequested) break;
                   
            }
            return bResult;
        }


        protected virtual  void SendAxisHome(ZaberCommandEventParameters zaberCommandEventParameters)
        {
            m_MotionContollerDevice.SendAxisHome(zaberCommandEventParameters.DeviceID);
        }

        protected virtual   void StopAxisMotion(ZaberCommandPrmStopAxisMotion zaberCommandPrmStopAxisMotion)
        {
            m_MotionContollerDevice.StopAxisMotion(zaberCommandPrmStopAxisMotion.DeviceID); 
        }


        protected virtual void StopAllAxesMotion(ZaberCommandEventParameters zaberCommandEventParameters)
        {
            m_MotionContollerDevice.StopAllAxesMotion();
        }

        protected virtual void StopAxesMotion(ZaberCommandPrmStopAxesMotion zaberCommandPrmStopAxesMotion)
        {
            m_MotionContollerDevice.StopAxesMotion(zaberCommandPrmStopAxesMotion.DevicesID );
        }
    }

}
