﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using log4net;
using Microsoft.Practices.Unity;
using MVVM_Dialogs.Interfaces;
using NS_Common;
using NS_Common.Event_Arguments;
using SFW;
using Tools.CustomRegistry;
using Zaber;
using Zaber_Common;
using Zaber_Common.Interfaces;
using System.IO;
using Zaber_Motion_Controller.Axis;
using Zaber_Common.Service;
using Microsoft.Win32;

namespace NS_Zaber_Motion_Controller
{
	public class ZaberMotionController : IIODevice, IZaberMotionController
	{
		//private members
		private string ROOT_REGISTRY_AVI4;
		private bool isSimulateMode = false;

		#region protected Members

		protected IUnityContainer m_Container;
		//protected IManageFilesDialogsViewModel manageRoutinesFilesViewModel;


		protected  ILog m_SystemLog = LogManager.GetLogger("ZaberMotionController");


		protected Zaber.DeviceCollection m_ZaberDevicesCollection;


		protected ConversionTable conversions;



		//protected TSeriesPort MyPort = new TSeriesPort(new System.IO.Ports.SerialPort("COM2"), new PacketConverter());


		#endregion

		#region Public events delegate functions

		#region Update Splash Screen Event

		/// <summary>
		/// The events delegate function. Optional event, occurs when there is long operation and MotionController requests to update splash screen.
		/// </summary>
		/// <param name="Sender">The control that raised event handler</param>
		/// <param name="args">Not in use</param>
		public delegate void UpdateSplashScreenHandler(Object Sender, UpdateSplashEventArgs args);
		/// <summary>
		/// Optional event, occurs when there is long operation and MotionController requests to update splash screen.
		/// </summary>
		public static event UpdateSplashScreenHandler UpdateSplashScreen;

		/// <summary>
		/// Service function, used to check if there is handler for raised event declared.
		/// </summary>
		/// <param name="Sender">The Axis that raised the event</param>
		/// <param name="args"></param>
		public void OnUpdateSplashScreen(object sender, UpdateSplashEventArgs args)
		{
			if (UpdateSplashScreen != null)
				UpdateSplashScreen(sender, args);
		}

		#endregion Update Splash Screen Event
		
		#endregion

		#region Public Properties

		[XmlIgnore]
		protected String m_ChannelName = "COM1";
		/// <summary>
		/// Set/Get the Com channel name
		/// </summary>
		public String ChannelName
		{
			get { return m_ChannelName; }
			set { m_ChannelName = value; }
		}

		[XmlIgnore]
		public Boolean  m_isSimulationMode = false ;
		/// <summary>
		/// Indicate if the system running in simulation mode,
		/// and not necessary  to raise error events
		/// </summary>
		public Boolean isSimulationMode
		{
			get { return m_isSimulationMode; }
			protected set { m_isSimulationMode = value; }
		}

		[XmlIgnore]
		protected ZaberPortFacade m_PortFacade;
		/// <summary>
		/// Return the actual com port reference object
		/// </summary>
		public ZaberPortFacade PortFacade
		{
			get { return m_PortFacade; }
			protected set
			{
				m_PortFacade = value;
			}
		}

		[XmlIgnore]
		protected ZaberDevicesCollection m_ZaberAxesCollection;
		/// <summary>
		/// Return list of direct objects to all available axes  
		/// </summary>
		public ZaberDevicesCollection ZaberAxesCollection
		{
			get { return m_ZaberAxesCollection; }
			protected set { m_ZaberAxesCollection = value; }
		}
		 
		/// <summary>
		/// Get the list of axes id available on current system
		/// </summary>
		public List<Int32 > ListOfAvailableAxes
		{
			get 
			{
				List<Int32> tmplist = new List<Int32>();
				foreach (BaseZaberDevice device in m_ZaberAxesCollection)
					tmplist.Add (device.DeviceID);
				return tmplist; 
			} 
		}
		
		//public Dictionary<int, String> AxisIds2NamesList
		//{
		//    get { return m_ZaberAxesCollection; }
			 
		//}
		#endregion

		#region Constructors

		/// <summary>
		/// Base Empty constructor. protected for singleton implementation
		/// </summary>
		public ZaberMotionController(IUnityContainer container)
		{
			OnUpdateSplashScreen(this, new UpdateSplashEventArgs(25, "Starting Zaber Motion Controller initialization"));

			DataReceived = null;

			m_Container = container;

		   // manageRoutinesFilesViewModel =  m_Container.Resolve<IManageFilesDialogsViewModel>();

			OnUpdateSplashScreen(null, new UpdateSplashEventArgs(26, "Initialize registry data"));
			InitRegistryData();
			m_ZaberAxesCollection = new ZaberDevicesCollection();
            if (!m_isSimulationMode)
			{
				m_ZaberAxesCollection = EnumerateAxes();
				InitializeAxes(m_ZaberAxesCollection);
			}
			//m_Is_IO_Enable = false;
			OnUpdateSplashScreen(this, new UpdateSplashEventArgs(30, "Finished Zaber Motion Controller initialization"));
		}
 

		#endregion SingleTone implimintation

		#region Protected functions

		/// <summary>
		/// Get base data from registry
		/// </summary>
		protected int InitRegistryData()
		{
			try
			{
				//Initialize application registry data
				cRegistryFunc objRegistry = new cRegistryFunc("Zaber Motion Controller",
																"",
																cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

				//ChannelName = objRegistry.GetRegKeyValue("", "ChannelName", m_ChannelName);
				//objRegistry.SetRegKeyValue("", "ChannelName", ChannelName);
                ChannelName = AVI4S.Configuration.Configuration.GetValue("Zaber Motion Controller/ChannelName", m_ChannelName);
				isSimulationMode = objRegistry.GetRegKeyValue("", "SimulatePortController", m_isSimulationMode);
				objRegistry.SetRegKeyValue("", "SimulatePortController", m_isSimulationMode);

				cRegistryFunc objRegistry1 = new cRegistryFunc("AVI4S",
																"",
																cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

				//isSimulateMode = Convert.ToBoolean(objRegistry1.GetRegKeyValue("", "SIMULATION_MODE", 0));
                isSimulateMode = Convert.ToBoolean(AVI4S.Configuration.Configuration.GetValue("AVI4S/SIMULATION_MODE", 0));
				//string AVI4_ROOT_PATH = (string)Registry.GetValue(ROOT_REGISTRY_AVI4, "AVI4_ROOT_PATH", @"");
				//isSimulateMode = Convert.ToBoolean(Registry.GetValue(ROOT_REGISTRY_AVI4, "SIMULATION_MODE", 0));

			}
			catch { };
			return ErrorCodesList.OK;
		}

		/// <summary>
		/// Create a ZaberPortFacade object, and wire up all its dependencies.
		/// </summary>
		/// <remarks>
		/// This is what replaces the Spring configuration. You lose a couple 
		/// of things that are only in the Spring configuration: a list of
		/// all the device types with their names and ids, as well as a list
		/// of all the commands with help text.
		/// </remarks>
		protected void CreatePortFacade()
		{
			var packetConverter = new PacketConverter();
			packetConverter.MillisecondsTimeout = 50;
			var defaultDeviceType = new DeviceType();
			defaultDeviceType.Commands = new List<CommandInfo>();
			m_PortFacade = new ZaberPortFacade();
			m_PortFacade.DefaultDeviceType = defaultDeviceType;
			m_PortFacade.QueryTimeout = 1000;
			m_PortFacade.Port = new TSeriesPort(new SerialPort(), packetConverter);
			m_PortFacade.DeviceTypes = new List<DeviceType>();
			m_PortFacade.Opened += portFacade_Opened;
			m_PortFacade.Closed += portFacade_Closed;
			m_PortFacade.Invalidated += portFacade_Invalidated; 
		}

 


		void portFacade_Closed(object sender, EventArgs e)
		{
			//try
			//{
			   
				//log.InfoFormat("Close port.");

				//foreach (DeviceView view in deviceViewBindingSource)
				//{
				//    view.Dispose();
				//}
				//deviceViewBindingSource.Clear();
				//EnableControls();
			//}
			//catch (Exception ex)
			//{
			//    //DisplayError("Port closed", ex);
			//}
		}

		void portFacade_Opened(object sender, EventArgs e)
		{
			try
			{ 
				//log.InfoFormat("Open port {0}.", portNames.Text);

				//uomMap.Clear();
				foreach (var conversation in m_PortFacade.AllConversations)
				{
					//Single axis devices are handled differently to multi-axis.
					if (PortFacade.GetDevice(conversation.Device.DeviceNumber).Axes.Count == 1)
					{
						if (conversation.Device.AxisNumber == 0)
						{
							//working with the device, get the UOM for the AXIS.
							var uoms = conversation.Device.Axes[0].GetUnitsOfMeasure(
							MeasurementType.Position,
							ConversionMap.Common);

						}

						//The conversation for the axis isn't displayed.
					}
					else
					{
						var uoms = conversation.Device.GetUnitsOfMeasure(
							MeasurementType.Position,
							ConversionMap.Common);

					}
				}


				if (m_PortFacade.IsCollisionDetected)
				{
					var msg =
						"Some devices are using the same device number. " +
						"Would you like to renumber all devices?";
					var result = MessageBox.Show(
						msg,
						"Open Port",
						MessageBoxButtons.YesNo);

					//var result = Xceed.Wpf.Toolkit.MessageBox.Show("Fatal Windows Forms Error",
					//                                    "Fatal Windows Forms Error");
					//                                     //MessageBoxButton.OK,
					//                                     //MessageBoxImage.Stop);


					if (result == DialogResult.Yes)
					{
						if (m_PortFacade.Port.IsAsciiMode)
						{
							m_PortFacade.Port.Send("renumber");
						}
						else
						{
							m_PortFacade.Port.Send(0, Command.Renumber);
						}
					}
				}
				//  log.Debug("End of portFacade_Opened");
			}
			catch// (Exception ex)
			{
				// DisplayError("Port opened", ex);
			}
		}

		void portFacade_Invalidated(object sender, EventArgs e)
		{
			//try
			//{
				 
				// log.InfoFormat("Reopen port {0}.", portNames.Text);
				//openOrClose_Click(this, EventArgs.Empty);
				//openOrClose_Click(this, EventArgs.Empty);
			//}
			//catch (Exception ex)
			//{
				//  DisplayError("Port invalidated", ex);
			//}
		}



		protected ZaberDevicesCollection EnumerateAxes()
		{
			try
			{
				Int32 iIndex = -1;
				ZaberDevicesCollection axis_col = new ZaberDevicesCollection();
				CreatePortFacade();
				if(!isSimulateMode)
					m_PortFacade.Open(m_ChannelName);
				foreach (ZaberDevice device in m_PortFacade.Devices)
				{
					iIndex++;
					if (device.GetType() != typeof(Zaber.DeviceCollection) && device.DeviceType.DeviceId != 3003)
						axis_col.Add(new SingleAxis(device, PortFacade.Conversations.ToList()[iIndex]));
					else if (device.GetType() == typeof(Zaber.DeviceCollection))
						m_ZaberDevicesCollection = device as Zaber.DeviceCollection;
				}
				return axis_col; 
			}
			catch (Exception ex)
			{
			}
			return null;             
		}

		protected void InitializeAxes(ZaberDevicesCollection axes_list)
		{
			if (null!=axes_list)
				foreach (ISingleAxis axis in axes_list)
				{
					axis.UpdateActualPositionData();
					axis.UpdateAxisHomedStatus();
					axis.SetAxisTrackingMode(true, 50); 
				}
		}


		#endregion


		#region Public Methods


		public void UpdateAllAxes(BaseZaberDevice axis)
		{
			if (!isSimulateMode)
				(axis as ISingleAxis).UpdateActualPositionData();
		}

		/// <summary>
		/// Save Axis list to Xml data file
		/// </summary>
		/// <param name="axis_list">The list of axis descriptors</param>
		/// <param name="FileName">Xml file name</param>
		/// <returns>
		/// If function successful the return value is ErrorCodesList.OK, else return value is one of the
		/// errors codes from NS_Common.ErrorCodesList
		/// </returns>
		public int SaveAxisXmlFile(ZaberDevicesCollection device_list, String FileName)
		{
			Axis2File MyList = new Axis2File();
			//Copy to data class good for serializing
			foreach (ISingleAxis tmpAxis in device_list)
			{
				MyList.AddItem(new SingleAxisXMLItem(tmpAxis));
			}
			//Serialization
			XmlSerializer Serializer = new XmlSerializer(typeof(Axis2File));
			TextWriter Writer = new StreamWriter(FileName);
			Serializer.Serialize(Writer, MyList);
			Writer.Close();
			MyList = null;

			return ErrorCodesList.OK;
		}

		/// <summary>
		/// Load the list of Axis's descriptors from XML file
		/// </summary>
		/// <param name="FileName">The XML file name</param>
		/// <returns>
		/// If function successful the return value is ErrorCodesList.OK, else return value is one of the
		/// errors codes from NS_Common.ErrorCodesList
		/// </returns>
		public int LoadAxisXmlFile(String FileName)
		{
			if (!File.Exists(FileName))
			{
				throw new FileNotFoundException("The axis's file " + FileName + " not found", FileName);
			}

			//Load data from XML Axis File
			Axis2File MyList = new Axis2File();
			TextReader Reader = new StreamReader(FileName);

			XmlSerializer Serializer = new XmlSerializer(typeof(Axis2File));
			//DeSerialization
			MyList = (Axis2File)Serializer.Deserialize(Reader);
			Reader.Close();

			int iCont = MyList.Items.Length;
			for (int i = 0; i < iCont; i++)
			{
				SingleAxisXMLItem xmlItem = MyList.Items[i];
				if (m_ZaberAxesCollection.ContainsKey(xmlItem.Axis_Idetifier))
				{
					var Obj = m_ZaberAxesCollection[xmlItem.Axis_Idetifier] as ISingleAxis;
					if (Obj != null)
					{
						(m_ZaberAxesCollection[xmlItem.Axis_Idetifier] as ISingleAxis).UpdateConvertationDataValues(xmlItem);
					}
				}

			}

			return ErrorCodesList.OK;
		}


		/// <summary>
		/// Moves the device to the position. The device begins to move immediately, and sends a response when the move has finished.
		/// The target speed and acceleration during a move absolute instruction can be specified using Set Target Speed (Cmd 42) and Set Acceleration (Cmd 43).
		/// </summary>
		/// <param name="axis">device to move</param>
		/// <param name="target_position_IU">Motion target Position</param>
		/// <param name="target_speed_IU">Define the target speed using Set Target Speed (Cmd 42)</param>
		/// <param name="acceleration_IU">Define the target speed using Set Acceleration (Cmd 43)</param>
		public void MoveAxisAbsolute(BaseZaberDevice axis, Single target_position_IU, Single target_speed_IU, Single acceleration_IU)
		{
			if (target_speed_IU > 0)
				axis.SetTargetSpeed(target_speed_IU);
			if (acceleration_IU > 0)
				axis.SetAcceleration(acceleration_IU);
			(axis as ISingleAxis).SendAxisToTargetPosition(target_position_IU);
		}

		/// <summary>
		/// Moves the device to the position. The device begins to move immediately, and sends a response when the move has finished.
		/// The target speed and acceleration during a move absolute instruction can be specified using Set Target Speed (Cmd 42) and Set Acceleration (Cmd 43).
		/// </summary>
		/// <param name="deviceID">System device id to move</param>
		/// <param name="target_position_IU">Motion target Position</param>
		/// <param name="target_speed_IU">Define the target speed using Set Target Speed (Cmd 42)</param>
		/// <param name="acceleration_IU">Define the target speed using Set Acceleration (Cmd 43)</param>
		public void MoveAxisAbsolute(int deviceID, Single target_position_IU, Single target_speed_IU, Single acceleration_IU)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.MoveAxisAbsolute(m_ZaberAxesCollection[deviceID], target_position_IU,target_speed_IU,acceleration_IU); 
		}



		/// <summary>
		/// Moves the device by the positive or negative number of micro steps specified in the Command Data. 
		/// The device moves to a position given by its current position plus the value specified in the command data. 
		/// The relative move command data in micro-steps can be positive or negative. The device begins to move immediately,
		/// and sends a response when the move has finished.
		/// </summary>
		/// <param name="deviceID">device to move</param>
		/// <param name="offset_IU">Motion Step size</param>
		/// <param name="target_speed_IU">Define the target speed using Set Target Speed (Cmd 42)</param>
		/// <param name="acceleration_IU">Define the target speed using Set Acceleration (Cmd 43)</param>
		public void MoveAxisRelative(BaseZaberDevice axis, Single offset_IU, Single target_speed_IU, Single acceleration_IU)
		{
			if (target_speed_IU > 0)
				axis.SetTargetSpeed(target_speed_IU);
			if (acceleration_IU > 0)
				axis.SetAcceleration(acceleration_IU);
			(axis as ISingleAxis).MoveAxisRelative(offset_IU);
		}

		/// <summary>
		/// Moves the device by the positive or negative number of micro-steps specified in the Command Data. 
		/// The device moves to a position given by its current position plus the value specified in the command data. 
		/// The relative move command data in micro-steps can be positive or negative. The device begins to move immediately,
		/// and sends a response when the move has finished.
		/// </summary>
		/// <param name="deviceID">System device id to move</param>
		/// <param name="offset_IU">Motion Step size</param>
		/// <param name="target_speed_IU">Define the target speed using Set Target Speed (Cmd 42)</param>
		/// <param name="acceleration_IU">Define the target speed using Set Acceleration (Cmd 43)</param>
		public void MoveAxisRelative(int deviceID, Single offset_IU, Single target_speed_IU, Single acceleration_IU)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.MoveAxisRelative(m_ZaberAxesCollection[deviceID], offset_IU, target_speed_IU, acceleration_IU);
		}


		/// <summary>
		/// Stop Axis motion
		/// </summary>
		/// <param name="deviceID">Device Key</param>
		public void StopAxisMotion(int deviceID)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.StopAxisMotion((ISingleAxis)m_ZaberAxesCollection[deviceID]); 
		}

		/// <summary>
		/// Stop Axis motion
		/// </summary>
		/// <param name="axis">Device object</param>
		public void StopAxisMotion(ISingleAxis axis)
		{
			axis.StopAxisMotion(this); 
		}


		/// <summary>
		/// Stops all axes from the passed list
		/// </summary>
		/// <param name="list_of_devices_ids">List of axes to stop</param>
		public void StopAxesMotion(byte[] list_of_devices_ids)
		{
			foreach (Byte deviceID in list_of_devices_ids)
			{
				if (m_ZaberAxesCollection.ContainsKey(deviceID))
					this.StopAxisMotion((ISingleAxis)m_ZaberAxesCollection[deviceID]);
			}
		}

		/// <summary>
		/// Stops all axes available at current system
		/// </summary>
		public void StopAllAxesMotion()
		{
			foreach (ISingleAxis device in m_ZaberAxesCollection)
			{ 
				this.StopAxisMotion(device);
			}
		}

	   
		/// <summary>
		/// Send specified by Axis ID device to it's home positions
		/// </summary>
		/// <param name="deviceID"></param>
		public void SendAxisHome(int deviceID)
		{
            SingleAxis tempAxis = (SingleAxis)m_ZaberAxesCollection[deviceID];
			if (!isSimulateMode)
				tempAxis.UpdateAxisHomedStatus();
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
                if (!tempAxis.AxisHomed)
                {
                    this.SendAxisHome((ISingleAxis)m_ZaberAxesCollection[deviceID]);
                   
                }
		}

		/// <summary>
		/// Sends specific axis to it's home position
		/// </summary>
		/// <param name="axis"></param>
		public void SendAxisHome(ISingleAxis axis)
		{
			axis.SendAxisHome(); 
		}

		/// <summary>
		/// Starts specified by Axis ID device monitoring thread
		/// </summary>
		/// <param name="deviceID">Device ID to monitor</param>
		/// <param name="timeout">Status request timeout</param>
		public void StartAxisMonitoringThread(int deviceID, Int32 timeout)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.StartAxisMonitoringThread((ISingleAxis)m_ZaberAxesCollection[deviceID], timeout); 
		}
		/// <summary>
		/// Starts specific Axis monitoring thread
		/// </summary>
		/// <param name="SingleAxis">axis to monitor</param>
		/// <param name="timeout">Status request timeout</param>
		public void StartAxisMonitoringThread(ISingleAxis axis, Int32 timeout)
		{
			axis.ExecuteStartStatusReadTimer(timeout);
		}

		public void StopAxisMonitoringThread(int deviceID)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.StopAxisMonitoringThread((SingleAxis)m_ZaberAxesCollection[deviceID]); 
		}

		public void StopAxisMonitoringThread(ISingleAxis axis)
		{
			axis.ExecuteStopStatusReadTimer();
		}

		/// <summary>
		/// Enable Move Tracking messages. Enable Move Tracking to receive position responses during move commands. The device will return its position periodically when a move command is executed.
		/// *  The response interval can be configured via Set Move Tracking Period (Cmd 117).
		/// </summary>
		/// <param name="axis">Device object</param>
		/// <param name="trackingMode">True - Device Tracking Mode is Enabled, otherwise disable</param>
		/// <param name="trackingModeUpdateInterval">response interval, if less than 0, use preset interval value</param>
		public void SetAxisTrackingMode(ISingleAxis axis, Boolean trackingMode = true, Int32 trackingModeUpdateInterval = -1)
		{
			axis.SetAxisTrackingMode(trackingMode);
		}

		public void SetAxisTrackingMode(int deviceID)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				this.SetAxisTrackingMode((ISingleAxis)m_ZaberAxesCollection[deviceID]);
		}

		/// <summary>
		/// suspend thread performance while axis is in motion
		/// </summary>
		/// <param name="axis">Device object</param>
		/// <param name="ct">Parameter that able to stop performance in middle</param>
		/// <returns>ErrorCodesList.OK</returns>
		public short WaitMotionCompleted(ISingleAxis axis, Int32 motion_target_position, System.Threading.CancellationToken ct)          
        {
            int timeoutSec = 20;
            Tools.StopWatch.StopWatch StopWatchSample = new Tools.StopWatch.StopWatch();
            StopWatchSample.Reset();
            int iNoChangeCounter = 0;
            int icounter = 0;
            Int32 lastPos = axis.CurrentPositionIU;            
        
            /*In addition possible to add maximum wait timeout parameter (look At the sample below)*/
            while (!axis.MotionComplete && !ct.IsCancellationRequested)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();

                //if (Math.Abs(motion_target_position - axis.CurrentPositionIU) < 1)                    
                if (motion_target_position == axis.CurrentPositionIU)
                    if (++icounter > 10) return ErrorCodesList.OK;                             
                else if (lastPos == axis.CurrentPositionIU && ++iNoChangeCounter > 10) 
                    return ErrorCodesList.FAILED_TO_REACH_TARGET_POSITION;
                             
                if ((StopWatchSample.Peek() / 10000.0)/*Milliseconds*/ > timeoutSec)
                {
                    m_SystemLog.Error(String.Format("Failed to complete motion to target {0} for more than {1} Seconds", motion_target_position, timeoutSec));
                    return ErrorCodesList.FAILED_TO_REACH_TARGET_POSITION;
                }

                //Update last position value
                lastPos = axis.CurrentPositionIU;
                   
            }

            //In case internal axis flag set to “Motion Completed” , or external flag Cancelation requested set to true
            return ErrorCodesList.OK;
        }

        //Stop watch sample:
        //Tools.StopWatch.StopWatch StopWatchSample = new Tools.StopWatch.StopWatch();
        //StopWatchSample.Reset();
        
        //if ((StopWatchSample.Peek() / 10000.0)/*Milliseconds*/ > timeoutSec)
        //{
        //m_Log.Error(String.Format("Failed to complete motion to target {0} for more than {1} Seconds", motion_target_position, timeoutSec));
        //return ErrorCodesList.FAILED_TIMEOUT;
        //}


		public short WaitMotionCompleted(int deviceID, Int32 motion_target_position, System.Threading.CancellationToken ct)
		{
			if (m_ZaberAxesCollection.ContainsKey(deviceID))
				return this.WaitMotionCompleted((ISingleAxis)m_ZaberAxesCollection[deviceID] , motion_target_position,  ct);
			return ErrorCodesList.FAILED;  
		}

		#endregion

		#region IComponent Members

		public string Name
		{
			get { return "Zaber Motion Controller"; }
		}

		#endregion IComponent Members

		#region IInputDevice Members

		public event DataEventHandler DataReceived = null;

		#endregion IInputDevice Members

		#region IOutputDevice Members

		public void Send(string data)
		{
			throw new NotImplementedException();
		}

		#endregion IOutputDevice Members 
	}
}
