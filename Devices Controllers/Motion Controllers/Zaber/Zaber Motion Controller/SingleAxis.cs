﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NS_Common.ViewModels.Common;
using Zaber;
using Zaber_Common;
using Zaber_Common.Service;

namespace NS_Zaber_Motion_Controller
{
    public class SingleAxis : BaseZaberDevice // ZaberDevice
    {
        #region Protected members

        protected ZaberDevice m_Device;
        protected ConversionTable m_ConversionTable;
        protected Conversation m_Conversation;

        #region Monitor Motor Status Members
        
        protected System.Timers.Timer m_MotorStatusReadTimer = new System.Timers.Timer();
        

        #endregion

        #endregion 
         
        #region Constructors

        public SingleAxis(Int32 simulation_axis_id) 
            : base(simulation_axis_id )
        {
            m_Device = new ZaberDevice();

            m_MotorStatusReadTimer.Elapsed += m_MotorStatusReadTimer_Elapsed; ;

            StopMotion = new RelayCommand<object>(p => this.StopAxisMotion(p), p => true);

            SendToTarget = new RelayCommand<Single>(p => this.SendAxisToTargetPosition(p), p => true);

            StartStatusReadTimer = new RelayCommand<Int32>(timeout => this.ExecuteStartStatusReadTimer(timeout), timeout => true);

            StopStatusReadTimer = new RelayCommand<object>(p => this.ExecuteStopStatusReadTimer(), p => true);
        }
         

        public SingleAxis(ZaberDevice device)
            : this(device.AxisNumber)
        {
            // TODO: Complete member initialization
            m_Device = device;

            m_ConversionTable = ConversionMap.Common.Find(
                    MotionType.Linear,
                    MeasurementType.Position);

            m_Device.MessageReceived +=
                new EventHandler<DeviceMessageEventArgs>(myZaberDevice_MessageReceived);
            m_Device.MessageSent +=
                new EventHandler<DeviceMessageEventArgs>(myZaberDevice_MessageSent);

        }

        public    SingleAxis(ZaberDevice device,Conversation conversation):
            this(device)
        {
            m_Conversation  = conversation;
        }

        #endregion

        #region Public Properties
         
        private String m_ErrorMessage;
        /// <summary>
        /// Get last error message text
        /// </summary>
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            protected set
            {
                if (value != m_ErrorMessage)
                {
                    m_ErrorMessage = value;
                    RaisePropertyChanged("ErrorMessage");
                }
            }
        }

        //private String m_AxisName;
        public String AxisName
        {
            get 
            {
                if (String.Empty.Equals(m_Device.Description))
                    return "Simulation Device";
                else
                    return m_Device.Description;
            } 
        }


        private String m_Notification;
        public String Notification
        {
            get { return m_Notification; }
            set 
            {
                if (value != m_Notification)
                {
                    m_Notification = value;
                    RaisePropertyChanged("SystemNotifications");
                }
            }
        }

        private Decimal m_CurrentPositionMM;
        /// <summary>
        /// Get current axis position MM
        /// </summary>
        public Decimal CurrentPositionMM
        {
            get { return m_CurrentPositionMM; }
            protected set
            {
                if (value != m_CurrentPositionMM)
                {  
                    m_CurrentPositionMM = value;
                    RaisePropertyChanged("CurrentPositionMM");
                }
            }
        }

        private Decimal m_CurrentSpeedMM;
        /// <summary>
        /// Get current axis speed MM
        /// </summary>
        public Decimal CurrentSpeedMM
        {
            get { return m_CurrentSpeedMM; }
            protected set
            {
                if (value != m_CurrentSpeedMM)
                {
                    m_CurrentSpeedMM = value;
                    RaisePropertyChanged("CurrentSpeedMM");
                    UpdateActualPositionData();
                }
            }
        }


        private Decimal m_TargetAxisPosition;
        /// <summary>
        /// 
        /// </summary>
        public Decimal TargetAxisPosition
        {
            get { return m_TargetAxisPosition; }
            protected set
            {
                if (value != m_TargetAxisPosition)
                {
                    m_TargetAxisPosition = value;
                    RaisePropertyChanged("TargetAxisPosition");
                }
            }
        }

        private Boolean m_AxisHomed;
        /// <summary>
        /// Indicate if monitoring thread is active
        /// </summary>
        public Boolean AxisHomed
        {
            get { return m_AxisHomed; }
            protected set
            {
                if (value != m_AxisHomed)
                {
                    m_AxisHomed = value;
                    RaisePropertyChanged("AxisHomed");
                }
            }
        }

        private _DeviceStatus m_DeviceStatus = _DeviceStatus.UNKNOWN;
        /// <summary>
        /// 		* 0  - idle, not currently executing any instructions

        /// 		* 1  - executing a home instruction
        /// 		
        /// 		* 10 - executing a manual move (i.e. the manual control knob is turned)
        /// 		
        /// 		* 11 - executing a manual move in Displacement Mode (A-Series devices only)
        /// 		
        /// 		* 20 - executing a move absolute instruction
        /// 		
        /// 		* 21 - executing a move relative instruction
        /// 		
        /// 		* 22 - executing a move at constant speed instruction
        /// 		
        /// 		* 23 - executing a stop instruction (i.e. decelerating)
        /// 		
        /// 		* 65 - device is parked (A-Series devices with FW 6.02 and up only. FW 6.01 returns 0 when parked)
        /// 		
        /// <remarks >Use Tools.String_Enum.StringEnum.GetStringValue(DeviceStatus) to get string value </remarks>
        /// </summary>
        public _DeviceStatus DeviceStatus
        {
            get { return m_DeviceStatus; }
            protected set 
            {
                if (value != m_DeviceStatus)
                {
                    m_DeviceStatus = value;
                    RaisePropertyChanged("DeviceStatus");
                }
            }
        }

        private Boolean m_DeviceStatusMonitorigActive;
        /// <summary>
        /// Indicate if monitoring thread is active
        /// </summary>
        public Boolean DeviceStatusMonitorigActive
        {
            get { return m_DeviceStatusMonitorigActive; }
            protected set
            {  
                if (value != m_DeviceStatusMonitorigActive)
                {
                    m_DeviceStatusMonitorigActive = value;
                    if (m_DeviceStatusMonitorigActive == false) DeviceStatus = _DeviceStatus.UNKNOWN; 
                    RaisePropertyChanged("DeviceStatusMonitorigActive");
                }
            }
        }

        private Boolean  m_MotionComplete;
        /// <summary>
        /// Indicate if axis motion complete
        /// </summary>
        public Boolean  MotionComplete
        {
            get { return m_MotionComplete; }
            protected set 
            {
                if (value != m_MotionComplete)
                {
                    m_MotionComplete = value;
                    RaisePropertyChanged("MotionComplete");
                }
            }
        }
        


        #endregion

        #region Protected virtual Methods

        /// <summary>
        /// Sends the Motor status read command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void m_MotorStatusReadTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            m_Device.Send(Command.ReturnStatus);
        }

        /// <summary>
        /// Starts automatically status monitoring thread
        /// </summary>
        /// <param name="timeout">Read status flags timeout</param> 
        public override void ExecuteStartStatusReadTimer(int timeout)
        {
            m_MotorStatusReadTimer.Stop(); 
            m_MotorStatusReadTimer.Interval = timeout;
            m_MotorStatusReadTimer.Start();
            DeviceStatusMonitorigActive = true;
        }

        /// <summary>
        /// Stop automatically status monitoring thread
        /// </summary> 
        public override void ExecuteStopStatusReadTimer()
        {
            m_MotorStatusReadTimer.Stop();
            DeviceStatusMonitorigActive = false;
        }

        /// <summary>
        ///  Read Actual position using  ReturnCurrentPosition  (Cmd 60) 
        /// </summary>
        /// <param name="target_speed">Motion target speed</param>
        public override void UpdateActualPositionData()
        {
            m_Device.Send(Command.ReturnCurrentPosition, 0);
        }

        /// <summary>
        ///  Define the target speed using Set Target Speed (Cmd 42) 
        /// </summary>
        /// <param name="target_speed">Motion target speed</param>
        public override void SetTargetSpeed(float target_speed)
        {
            int speed = Convert.ToInt32(target_speed); // m_Device.CalculateData(new Measurement(target_speed, UnitOfMeasure.MillimetersPerSecond), MeasurementType.Velocity, ConversionMap.Common);
            m_Device.Send(Command.SetTargetSpeed, speed);
            //portFacade.Conversations.Last().Request(Command.SetTargetSpeed, speed);
        }

        /// <summary>
        ///  Define the Acceleration using Set Acceleration (Cmd 43)</param>
        /// </summary>
        /// <param name="acceleration">Motion Acceleration</param>
        public override void SetAcceleration(float acceleration)
        {
            int Acceleration = Convert.ToInt32(acceleration); // m_Device.CalculateData(new Measurement(acceleration, UnitOfMeasure.MillimetersPerSecond), MeasurementType.Acceleration, ConversionMap.Common);
            m_Device.Send(Command.SetAcceleration , Acceleration);
        }

        public override void SendAxisToTargetPosition(Single target_position)
        { 
            BackgroundWorker myback = new BackgroundWorker();
            myback.DoWork += new DoWorkEventHandler(myback_Abs_Motion_DoWork);


            myback.RunWorkerAsync(target_position);
        }

        protected void myback_Abs_Motion_DoWork(object sender, DoWorkEventArgs e)
        {
            Single targetpos = (Single)e.Argument;
            int pos = Convert.ToInt32(targetpos); //m_Device.CalculateData(new Measurement(targetpos, UnitOfMeasure.Millimeter), MeasurementType.Position, ConversionMap.Common);
            pos = Convert.ToInt32(targetpos); 
           
            m_Device.Send(Command.MoveAbsolute, pos);
            
        }

        public override void MoveAxisRelative(Single target_position)
        {
            BackgroundWorker myback = new BackgroundWorker();
            myback.DoWork += new DoWorkEventHandler(myback_Rel_Motion_DoWork);


            myback.RunWorkerAsync(target_position);
        }

        protected void myback_Rel_Motion_DoWork(object sender, DoWorkEventArgs e)
        { 
            int step_size = Convert.ToInt32((Single)e.Argument ); //m_Device.CalculateData(new Measurement(targetpos, UnitOfMeasure.Millimeter), MeasurementType.Position, ConversionMap.Common);
            m_Device.Send(Command.MoveRelative, step_size);
        }

        public virtual void StopAxisMotion(object sender)
        {
            m_Device.Send(Command.Stop);   
        }

        public virtual void SendAxisHome( )
        {
            m_Device.Send(Command.Home);   
        }

        public virtual void UpdateAxisHomedStatus()
        {
            m_Device.Send(Command.SetHomeStatus);   
        }

        public virtual void SetAxisTrackingMode(bool trackingMode, Int32 trackingModeUpdateInterval = -1)
        {
            if (trackingModeUpdateInterval > 0)
            {
                m_Device.Send(Command.SetMoveTrackingPeriod, trackingModeUpdateInterval);
            }
            m_Device.Send(Command.SetMoveTrackingMode, trackingMode == true ? 1 : 0);   
        }
        #endregion

        #region Events Handlers

        protected virtual void myZaberDevice_MessageSent(object sender, DeviceMessageEventArgs e)
        {
            switch (e.DeviceMessage.Command)
            {
                case Command.MoveAbsolute:
                case Command.MoveRelative:
                case Command.Home:
                case Command.ManualMove:
                    {
                        MotionComplete = false;
                        break;
                    } 
            
            }
        }

        protected virtual void myZaberDevice_MessageReceived(object sender, DeviceMessageEventArgs e)
        {
            switch (e.DeviceMessage.Command)
            {
                case Zaber.Command.Error:
                    {
                        int errorNumber = e.DeviceMessage.Data;
                        string errorMessage =
                            Enum.GetName(typeof(ZaberError), errorNumber);
                        if (errorMessage == null)
                        {
                            errorMessage = string.Format("code = {0}", errorNumber);
                        }
                        m_ErrorMessage = String.Format(
                            "Error response ({0}) from device {1}",
                            errorMessage,
                            e.DeviceMessage.DeviceNumber);
                        if (errorNumber == (int)ZaberError.Busy) // &&  isManualMoveRunning)
                        //&&
                        //    Conversation.Device.IsSingleDevice)
                        {
                            Notification =
                                "The manual control knob is turned.\n" +
                                "That command will not work until you center it.";
                        }
                        break;
                    }
                case Command.SetMoveTrackingPeriod:
                case Command.SetMoveTrackingMode:
                    {
                        UpdateTrackingMode((ZaberDevice)sender, e);
                        break;
                    }
                
                case Command.SetHomeStatus:
                    {
                        UpdateHomeStatus((ZaberDevice)sender, e);
                        break;
                    }
                case Command.ManualMoveTracking:
                case Command.ReturnCurrentPosition:
                case Command.MoveTracking:
                    {
                        UpdateCurrentPosition((ZaberDevice)sender, e);
                        break;
                    } 
                case Command.MoveAbsolute:
                case Command.MoveRelative:
                case Command.Home:
                case Command.ManualMove:
                    {
                        UpdateCurrentPosition((ZaberDevice )sender,e);
                        MotionComplete = true;
                        break;
                    }
                case Command.MoveAtConstantSpeed:
                    {
                        UpdateCurrentSpeed((ZaberDevice)sender, e);
                        break;
                    }
                case Command.ReturnStatus:
                    {
                        UpdateCurrentState((ZaberDevice)sender, e);
                        break;
                    }
                case Command.Stop:
                    {
                        MotionComplete = true;
                        break;
                    }
            }
        }

        private void UpdateTrackingMode(ZaberDevice zaberDevice, DeviceMessageEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void UpdateHomeStatus(ZaberDevice zaberDevice, DeviceMessageEventArgs e)
        {
            AxisHomed = e.DeviceMessage.Data == 1 ? true : false;  
        }

        private void UpdateCurrentState(ZaberDevice zaberDevice, DeviceMessageEventArgs e)
        {
            DeviceStatus =(_DeviceStatus) Convert.ToByte(e.DeviceMessage.Data); 
        }

        private void UpdateCurrentSpeed(ZaberDevice zaberDevice, DeviceMessageEventArgs e)
        {
            CurrentSpeedMM = e.DeviceMessage.Measurement.Value;
        }

        protected virtual void UpdateCurrentPosition(ZaberDevice sender, DeviceMessageEventArgs e)
        {
            //string tmp = sender.FormatData(e.DeviceMessage.Data, UnitOfMeasure.Millimeter, MeasurementType.Position, ConversionMap.Common);
            CurrentPositionMM =  e.DeviceMessage.Measurement.Value;
            //int decimals;
            //var measurement = sender.CalculateMeasurement(
            //   e.DeviceMessage.Data,
            //   UnitOfMeasure.Millimeter,
            //   MeasurementType.Position,
            //   ConversionMap.Common,
            //   out decimals);
            //CurrentPositionMM = decimals;
        }
        
        #endregion



        
    }
}
