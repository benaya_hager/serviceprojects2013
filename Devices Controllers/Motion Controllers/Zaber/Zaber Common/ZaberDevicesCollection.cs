﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Common.Service;
using Zaber_Common.Service;

namespace Zaber_Common
{
    public class ZaberDevicesCollection : AsyncTrulyObservableCollection<BaseZaberDevice>  // ObservableDictionary<Int32, BaseDevice>
    {
        #region Private members
            
            private BaseZaberDevice GetDeviceByKey(int lIndex)
            {
                foreach (BaseZaberDevice Device in this)
                    if (Device.DeviceID == lIndex)
                        return Device;
                return null;
            }
             

        #endregion

        #region Public Functions
            /// <summary>
            /// Return first free index from available in the list 
            /// </summary>
            /// <returns>First available position index in the list</returns>
            public int GetFreeIndex()
            {
                int lIndex = 1;
                while (true)
                {
                    if (!this.ContainsKey(lIndex))
                        break;
                    lIndex++;
                }
                return lIndex;
            }

            public bool ContainsKey(int lIndex)
            {
                foreach (BaseZaberDevice Device in this)
                    if (Device.DeviceID == lIndex)
                        return true;
                return false;
            }


            //Get/Set the element at specific Index
            /// <summary>
            /// Get/Set the element at specific Index
            /// </summary>
            /// <param name="index">The zero-based index of the element</param>
            /// <returns>The element stored in the List</returns>
            public new BaseZaberDevice this[int index]
            {
                get
                {
                    return GetDeviceByKey(index);
                }
                set
                {
                    //if (this.ContainsKey(index))
                        //this.Remove(index);
                    //var res = GetDeviceByKey(index);
                    //if (null != res)
                    Remove(GetDeviceByKey(index)); 
                    base.Add(value);
                }
            }

            /// <summary>
            /// Add Device based on  SortedList collection, 
            /// and store direct access parameter
            /// </summary>
            /// <param name="item">Handle of a SingleDevice to add</param>
            /// <returns>Return my direct access parameter</returns> 
            public new Int32 Add(BaseZaberDevice item)
            {
                Int32 m_Index = GetFreeIndex();
                item.DeviceID = m_Index;
                base.Add(item);
                return m_Index;
            }

            /// <summary>
            /// Find item from  SortedList collection by DeviceIndex created in Add process
            /// </summary>
            /// <param name="item">Device to remove</param>
            public new  void Remove(BaseZaberDevice item)
            {
                if (this.ContainsKey(item.DeviceID))
                {
                    OnBeforeDeviceRemove(this, new DeviceEventArguments( item)); 

                    base.Remove(item);
                }
            }

            /// <summary>
            /// Find item from  SortedList collection by DeviceIndex created in Add process
            /// </summary>
            /// <param name="Index">Device Index to remove</param>
            public void Remove(Int32 Index)
            {
                if (this.ContainsKey(Index))
                {
                    OnBeforeDeviceRemove(this, new DeviceEventArguments(this[Index]));
                    //this[Index].RemoveEventsHandlers();

                    base.Remove(GetDeviceByKey(Index));
                    //base.Remove(Index);
                }
            }

            public new void Clear()
            {
                foreach (BaseZaberDevice tmpDevice in this)
                {
                    OnBeforeDeviceRemove(this, new DeviceEventArguments(tmpDevice));
                }
                base.Clear(); 
            }
        #endregion
        
        #region Public events delegate functions
            #region Before Device Remove
                /// <summary>
                /// The events delegate function. Occurs when Device is going to be removed from performance loop.
                /// </summary>
                /// <param name="Sender">The control that raised event handler </param>
                /// <param name="args"></param>
                public delegate void BeforeDeviceRemoveHandler(Object Sender, DeviceEventArguments args);
                public event BeforeDeviceRemoveHandler BeforeDeviceRemove;
                protected void OnBeforeDeviceRemove(object Sender, DeviceEventArguments args)
                {
                    if (BeforeDeviceRemove != null)
                        BeforeDeviceRemove(Sender, args);
                }
            #endregion
        #endregion
    }
}
