﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber
{ 
    public enum ZaberDataEventType : byte
    {
        [Tools.String_Enum.StringValue("Not Set")]
        ET_NOT_SET = 255,

        [Tools.String_Enum.StringValue("Current Position Changed")]
        ET_CURRENT_POSITION_CHANGED = 1,
        [Tools.String_Enum.StringValue("Current Speed Changed")]
        ET_CURRENT_SPEED_CHANGED = 2,
        [Tools.String_Enum.StringValue("New System notification message received")]
        ET_NEW_SYSTEM_MESSAGE = 3,
        [Tools.String_Enum.StringValue("Device Monitoring thread state changed")]
        ET_DEVICE_MONITORING_THREAD_STATUS_CHANGED = 4,
        [Tools.String_Enum.StringValue("Device performance status changed")]
        ET_DEVICE_STATUS_CHANGED = 5,
        [Tools.String_Enum.StringValue("Device Homed status changed")]
        ET_DEVICE_HOMED_STATUS_CHANGED = 6,
        [Tools.String_Enum.StringValue("Device Motion complete either stopped")]
        ET_DEVICE_MOTION_COMPLETE = 7,
        //[Tools.String_Enum.StringValue("Data is ignored.")]
        //ET_Reset = 0,

        //[Tools.String_Enum.StringValue("Response data is Final Position")]
        //ET_Home = 1,

        //[Tools.String_Enum.StringValue("Request data is New Number. Response data is Device Id")]
        //ET_Renumber = 2,

        //[Tools.String_Enum.StringValue("Request data is Register Address. Response data is Data")]
        //ET_ReadRegister = 5,

        //[Tools.String_Enum.StringValue("Request data is Register Address. Response data is Register Address")]
        //ET_SetActiveRegister = 6,

        //[Tools.String_Enum.StringValue("Request data is Data. Response data is Data")]
        //ET_WriteRegister = 7,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_MoveTracking = 8,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_LimitActive = 9,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_ManualMoveTracking = 10,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_ManualMove = 11,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_SlipTracking = 12,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_UnexpectedPosition = 13,

        //[Tools.String_Enum.StringValue("Request data is Address. Response data is Address")]
        //ET_StoreCurrentPosition = 16,

        //[Tools.String_Enum.StringValue("Request data is Address. Response data is Stored Position")]
        //ET_ReturnStoredPosition = 17,

        //[Tools.String_Enum.StringValue("Request data is Address. Response data is Final Position")]
        //ET_MoveToStoredPosition = 18,

        //[Tools.String_Enum.StringValue("Request data is Absolute Position. Response data is Final position")]
        //ET_MoveAbsolute = 20,

        //[Tools.String_Enum.StringValue("Request data is Relative Position. Response data is Final Position")]
        //ET_MoveRelative = 21,

        //[Tools.String_Enum.StringValue("Request data is Speed. Response data is Speed")]
        //ET_MoveAtConstantSpeed = 22,

        //[Tools.String_Enum.StringValue("Response data is Final Position")]
        //ET_Stop = 23,

        //[Tools.String_Enum.StringValue("Request data is Axis. Response data is Axis")]
        //ET_SetActiveAxis = 25,

        //[Tools.String_Enum.StringValue("Request data is Device Number. Response data is Device Number")]
        //ET_SetAxisDeviceNumber = 26,

        //[Tools.String_Enum.StringValue("Request data is Invert Status. Response data is Invert Status")]
        //ET_SetAxisInversion = 27,

        //[Tools.String_Enum.StringValue("Request data is Profile Number. Response data is Profile Number")]
        //ET_SetAxisVelocityProfile = 28,

        //[Tools.String_Enum.StringValue("Request data is Maximum Velocity. Response data is Maximum Velocity")]
        //ET_SetAxisVelocityScale = 29,

        //[Tools.String_Enum.StringValue("Request data is Key Event. Response data is Key Event")]
        //ET_LoadEventInstruction = 30,

        //[Tools.String_Enum.StringValue("Request data is Key Event")]
        //ET_ReturnEventInstruction = 31,

        //[Tools.String_Enum.StringValue("Request data is Calibration Mode.Response data is Calibration Mode")]
        //ET_SetCalibrationMode = 33,

        //[Tools.String_Enum.StringValue("Request data is Data.Response data is Data")]
        //ET_ReadOrWriteMemory = 35,

        //[Tools.String_Enum.StringValue("Request data is Peripheral Id.Response data is Peripheral Id")]
        //ET_RestoreSettings = 36,

        //[Tools.String_Enum.StringValue("Request data is Microsteps.Response data is Microsteps")]
        //ET_SetMicrostepResolution = 37,

        //[Tools.String_Enum.StringValue("Request data is Value.Response data is Value")]
        //ET_SetRunningCurrent = 38,

        //[Tools.String_Enum.StringValue("Request data is Value.Response data is Value")]
        //ET_SetHoldCurrent = 39,

        //[Tools.String_Enum.StringValue("Request data is Mode.Response data is Mode")]
        //ET_SetDeviceMode = 40,

        //[Tools.String_Enum.StringValue("Request data is Speed.Response data is Speed")]
        //ET_SetHomeSpeed = 41,

        //[Tools.String_Enum.StringValue("Request data is Speed.Response data is Speed")]
        //ET_SetTargetSpeed = 42,

        //[Tools.String_Enum.StringValue("Request data is Acceleration.Response data is Acceleration")]
        //ET_SetAcceleration = 43,

        //[Tools.String_Enum.StringValue("Request data is Position.Response data is Position")]
        //ET_SetMaximumPosition = 44,

        //[Tools.String_Enum.StringValue("Identical to SetMaximumPosition, this is just for backward compatibility.Request data is Range.Response data is Range")]
        //ET_SetMaximumRange = 44,


        //[Tools.String_Enum.StringValue("Request data is New Position.Response data is New Position")]
        //ET_SetCurrentPosition = 45,

        //[Tools.String_Enum.StringValue("Request data is Range.Response data is Range")]
        //ET_SetMaximumRelativeMove = 46,

        //[Tools.String_Enum.StringValue("Request data is Offset.Response data is Offset")]
        //ET_SetHomeOffset = 47,

        //[Tools.String_Enum.StringValue("Request data is Alias Number.Response data is Alias Number")]
        //ET_SetAliasNumber = 48,

        //[Tools.String_Enum.StringValue("Request data is Lock State.Response data is Lock State")]
        //ET_SetLockState = 49,

        //[Tools.String_Enum.StringValue("Response data is Device Id")]
        //ET_ReturnDeviceId = 50,

        //[Tools.String_Enum.StringValue("Response data is Version")]
        //ET_ReturnFirmwareVersion = 51,

        //[Tools.String_Enum.StringValue("Response data is Voltage")]
        //ET_ReturnPowerSupplyVoltage = 52,

        //[Tools.String_Enum.StringValue("Request data is Setting Number.Response data is Setting Value")]
        //ET_ReturnSetting = 53,

        //[Tools.String_Enum.StringValue("Response data is Status")]
        //ET_ReturnStatus = 54,

        //[Tools.String_Enum.StringValue("Request data is Data.Response data is Data")]
        //ET_EchoData = 55,

        //[Tools.String_Enum.StringValue("Response data is Position")]
        //ET_ReturnCurrentPosition = 60,

        //[Tools.String_Enum.StringValue("Request data is Park State.Response data is Park State")]
        //ET_SetParkState = 65,

        //[Tools.String_Enum.StringValue("Request data is Peripheral Id.Response data is Peripheral Id")]
        //ET_SetPeripheralId = 66,

        //[Tools.String_Enum.StringValue("Request data is Auto-Reply Disabled Mode.Response data is Auto-Reply Disabled Mode")]
        //ET_SetAutoReplyDisabledMode = 101,

        //[Tools.String_Enum.StringValue("Request data is Message Id Mode.Response data is Message Id Mode")]
        //ET_SetMessageIdMode = 102,

        //[Tools.String_Enum.StringValue("Request data is Home Status. Response data is Home Status")]
        //ET_SetHomeStatus = 103,

        //[Tools.String_Enum.StringValue("Request data is Home Sensor Type. Response data is Home Sensor Type")]
        //ET_SetHomeSensorType = 104,

        //[Tools.String_Enum.StringValue("Request data is Auto-Home Disabled Mode. Response data is Auto-Home Disabled Mode")]
        //ET_SetAutoHomeDisabledMode = 105,

        //[Tools.String_Enum.StringValue("Request data is Minimum Position. Response data is Minimum Position")]
        //ET_SetMinimumPosition = 106,

        //[Tools.String_Enum.StringValue("Request data is Knob Disabled Mode. Response data is Knob Disabled Mode")]
        //ET_SetKnobDisabledMode = 107,

        //[Tools.String_Enum.StringValue("Request data is Knob Direction. Response data is Knob Direction")]
        //ET_SetKnobDirection = 108,

        //[Tools.String_Enum.StringValue("Request data is Movement Mode. Response data is Movement Mode")]
        //ET_SetKnobMovementMode = 109,

        //[Tools.String_Enum.StringValue("Request data is Jog Size. Response data is Jog Size")]
        //ET_SetKnobJogSize = 110,

        //[Tools.String_Enum.StringValue("Request data is Maximum Speed. Response data is Maximum Speed")]
        //ET_SetKnobVelocityScale = 111,

        //[Tools.String_Enum.StringValue("Request data is Profile Number. Response data is Profile Number")]
        //ET_SetKnobVelocityProfile = 112,

        //[Tools.String_Enum.StringValue("Request data is Acceleration. Response data is Acceleration")]
        //ET_SetAccelerationOnly = 113,

        //[Tools.String_Enum.StringValue("Request data is Deceleration. Response data is Deceleration")]
        //ET_SetDecelerationOnly = 114,

        //[Tools.String_Enum.StringValue("Request data is Move Tracking Mode. Response data is Move Tracking Mode")]
        //ET_SetMoveTrackingMode = 115,

        //[Tools.String_Enum.StringValue("Request data is Manual Move Tracking Disabled Mode. Response data is Manual Move Tracking Disabled Mode")]
        //ET_SetManualMoveTrackingDisabledMode = 116,

        //[Tools.String_Enum.StringValue("Request data is Tracking Period in ms. Response data is Tracking Period in ms")]
        //ET_SetMoveTrackingPeriod = 117,

        //[Tools.String_Enum.StringValue("Request data is Closed-Loop Mode. Response data is Closed-Loop Mode")]
        //ET_SetClosedLoopMode = 118,

        //[Tools.String_Enum.StringValue("Request data is Tracking Period in ms. Response data is Tracking Period in ms")]
        //ET_SetSlipTrackingPeriod = 119,

        //[Tools.String_Enum.StringValue("Request data is Stall Timeout in ms. Response data is Stall Timeout in ms")]
        //ET_SetStallTimeout = 120,

        //[Tools.String_Enum.StringValue("Request data is Device Direction. Response data is Device Direction")]
        //ET_SetDeviceDirection = 121,

        //[Tools.String_Enum.StringValue("Request data is Baudrate. Response data is Baudrate")]
        //ET_SetBaudrate = 122,

        //[Tools.String_Enum.StringValue("Request data is Protocol. Response data is Protocol")]
        //ET_SetProtocol = 123,

        //[Tools.String_Enum.StringValue("Request data is Baudrate. Response data is Baudrate")]
        //ET_ConvertToAscii = 124,

        //[Tools.String_Enum.StringValue("Response data is Error Code")]
        //ET_Error = 255

    }
}
