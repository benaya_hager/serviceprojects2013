﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber
{
    public enum ZaberCommandEventTypes
    {
        [Tools.String_Enum.StringValue("Not Set")]
        CT_NOT_SET,

        [Tools.String_Enum.StringValue("Send Axis to it's home position")]
        CT_SEND_AXIS_TO_HOME_POSITION,
        
        [Tools.String_Enum.StringValue("Move Axis Absolute")]
        CT_MOVE_AXIS_ABSOLUTE,
        [Tools.String_Enum.StringValue("Update Axis Status")]
        CT_UPDATE_AXIS_STATUS,
        [Tools.String_Enum.StringValue("Synchronized Move Axis Absolute")]
        CT_SYNC_MOVE_AXIS_ABSOLUTE,
        [Tools.String_Enum.StringValue("Synchronized Move Axes Absolute")]
        CT_SYNC_MOVE_AXES_ABSOLUTE,


        [Tools.String_Enum.StringValue("Move Axis Relative")]
        CT_MOVE_AXIS_RELATIVE,
        [Tools.String_Enum.StringValue("Synchronized Move Axis Relative")]
        CT_SYNC_MOVE_AXIS_RELATIVE,
        [Tools.String_Enum.StringValue("Synchronized Move Axes Relative")]
        CT_SYNC_MOVE_AXES_RELATIVE,

        [Tools.String_Enum.StringValue("Stop Axis Motion")]
        CT_STOP_AXIS_MOTION,
        [Tools.String_Enum.StringValue("Stop Specified Axes Motion")]
        CT_STOP_AXES_MOTION,
        [Tools.String_Enum.StringValue("Stop All Available In Current System Axes Motion")]
        CT_STOP_ALL_AXES_MOTION,


        [Tools.String_Enum.StringValue("Start Axis Status monitoring thread")]
        CT_START_AXIS_STATUS_MONITORING,
        [Tools.String_Enum.StringValue("Stop Axis Status monitoring thread")]
        CT_STOP_AXIS_STATUS_MONITORING
    }
}
