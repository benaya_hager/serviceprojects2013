﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataEventParameters
    {
        #region Public Properties

        protected Int32 m_DeviceID;
        /// <summary>
        /// The device ID
        /// </summary>
        public Int32 DeviceID
        {
            get { return m_DeviceID; }
            set { m_DeviceID = value; }
        }

        protected Object m_Sender;
        /// <summary>
        /// The object raised event
        /// </summary>
        public Object Sender
        {
            get { return m_Sender; }
            set { m_Sender = value; }
        }

        #endregion

        #region Constructors

        public ZaberDataEventParameters(Int32 device_ID)
        {
            m_DeviceID = device_ID;
        }

        public ZaberDataEventParameters(Object sender , Int32 device_ID)
            : this(device_ID)
        {
            m_Sender = sender;
        }
        #endregion
    }
}
