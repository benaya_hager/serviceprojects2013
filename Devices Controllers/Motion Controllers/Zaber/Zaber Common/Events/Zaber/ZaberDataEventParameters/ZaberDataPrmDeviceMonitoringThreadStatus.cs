﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Service;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataPrmDeviceMonitoringThreadStatus : ZaberDataEventParameters
    {
        #region Public Properties

        private Boolean m_DeviceMonitoringThreadStatus;
        /// <summary>
        /// New state of device monitoring thread.
        /// </summary> 
        public Boolean DeviceMonitoringThreadStatus
        {
            get { return m_DeviceMonitoringThreadStatus; }
            set { m_DeviceMonitoringThreadStatus = value; }
        }

         
        
        #endregion


        #region Constructors
         
        public ZaberDataPrmDeviceMonitoringThreadStatus(Object sender, Int32 device_id, Boolean device_monitoring_thread_status)
            : base(device_id)
        {
            m_Sender = sender; 
            m_DeviceMonitoringThreadStatus = device_monitoring_thread_status; 
        }
        
        #endregion
    }
}
