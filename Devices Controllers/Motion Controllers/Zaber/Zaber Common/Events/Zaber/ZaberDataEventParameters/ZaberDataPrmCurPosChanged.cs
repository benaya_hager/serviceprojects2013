﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataPrmCurPosChanged : ZaberDataEventParameters
    {
        #region Public Properties

        private Single m_CurrentPosition;
        /// <summary>
        /// The actual axis position
        /// </summary>
        public Single CurrentPosition
        {
            get { return m_CurrentPosition; }
            set { m_CurrentPosition = value; }
        }

         
        
        
        #endregion


        #region Constructors 
        public ZaberDataPrmCurPosChanged(Object sender, Int32 device_id, Single current_position)
            : base(device_id)
        {
            m_Sender = sender; 
            m_CurrentPosition = current_position; 
        }
        
        #endregion
    }
}
