﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Service;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataPrmBoolean : ZaberDataEventParameters
    {
        #region Public Properties

        private Boolean m_BooleanParameter;
        /// <summary>
        /// Boolean parameter flag
        /// </summary> 
        public Boolean BooleanParameter
        {
            get { return m_BooleanParameter; }
            set { m_BooleanParameter = value; }
        } 
        #endregion


        #region Constructors
         
        public ZaberDataPrmBoolean(Object sender, Int32 device_id, Boolean boolean_parameter)
            : base(device_id)
        {
            m_Sender = sender;
            m_BooleanParameter = boolean_parameter; 
        }
        
        #endregion
    }
}
