﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Service;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataPrmDeviceStatus : ZaberDataEventParameters
    {
        #region Public Properties

        private _DeviceStatus m_DeviceStatus;
        /// <summary>
        /// Current device performance status.
        /// </summary>
        /// <remarks>Use Tools.String_Enum.StringEnum.GetStringValue(DeviceStatus) to get string value</remarks> 
        public _DeviceStatus DeviceStatus
        {
            get { return m_DeviceStatus; }
            set { m_DeviceStatus = value; }
        }

         
        
        #endregion


        #region Constructors

        public ZaberDataPrmDeviceStatus(Object sender, Int32 device_id, _DeviceStatus device_status)
            : base(device_id)
        {
            m_Sender = sender; 
            m_DeviceStatus = device_status; 
        }
        
        #endregion
    }
}
