﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters
{
    public class ZaberDataPrmNotification : ZaberDataEventParameters
    {
        #region Public Properties

        private String m_NotificationMessage;
        /// <summary>
        /// Received notification message text
        /// </summary>
        public String NotificationMessage
        {
            get { return m_NotificationMessage; }
            set { m_NotificationMessage = value; }
        }

         
        
        #endregion


        #region Constructors
          
        public ZaberDataPrmNotification(Object sender, Int32 device_id, String notification_message)
            : base(device_id)
        {
            m_Sender = sender; 
            m_NotificationMessage = notification_message; 
        }
        
        #endregion
    }
}
