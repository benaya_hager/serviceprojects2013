﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;

namespace Zaber_Common.Events.Zaber
{
    public class ZaberCommandPrmUpdateAxis : ZaberCommandEventParameters
    {
        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        public ZaberCommandPrmUpdateAxis(byte deviceID) : base(deviceID) { }
    }
}
