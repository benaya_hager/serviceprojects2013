﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFW;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;

namespace Zaber_Common.Events.Zaber
{ 
    public class ZaberDataEvent : IEventData
    {
        #region Public Properties

        private ZaberDataEventType m_DataType;
        public ZaberDataEventType DataType
        {
            get { return m_DataType; }
            private set { m_DataType = value; }
        }

        private ZaberDataEventParameters m_DataParameters;
        public ZaberDataEventParameters DataParameters
        {
            get { return m_DataParameters; }
            private set { m_DataParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public ZaberDataEvent(ZaberDataEventType data_type, ZaberDataEventParameters prms)
        {
            m_DataType = data_type;
            m_DataParameters = prms;
        }

        #endregion Constructors
    }
}
