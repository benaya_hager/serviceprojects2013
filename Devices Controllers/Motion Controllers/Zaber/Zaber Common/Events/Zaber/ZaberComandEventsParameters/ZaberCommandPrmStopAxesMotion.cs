﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandPrmStopAxesMotion : ZaberCommandEventParameters
    {
         #region Public Properties

        protected Byte[] m_DevicesID;

        public Byte[]  DevicesID
        {
            get { return m_DevicesID; }
            set { m_DevicesID = value; }
        }

        #endregion Public Properties

        #region Constructors

        public ZaberCommandPrmStopAxesMotion(byte[] devicesID) : base(devicesID[0]) { }
         
 
        #endregion Constructors
    }
}
