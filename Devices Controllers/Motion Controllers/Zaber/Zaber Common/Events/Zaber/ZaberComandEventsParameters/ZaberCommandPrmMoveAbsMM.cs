﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandPrmMoveAbsMM : ZaberCommandEventParameters
     {
        #region Public Properties
         
        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Single m_DestinationMM = 0;
        public Single DestinationMM { get { return m_DestinationMM; } set { m_DestinationMM = value; } }

        private Single m_SlewSpeedMM = 0;
        public Single SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single m_AccelerationMM = 0;
        public Single AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

       
        private CancellationToken m_CancellationToken;
        public CancellationToken cancellationToken
        {
            get { return m_CancellationToken; }
            set { m_CancellationToken = value; }
        }

        #endregion Public Properties

        #region Constructors

        public ZaberCommandPrmMoveAbsMM(byte inDeviceID) :
            base(inDeviceID)
        { }

        /// <summary>
        /// Specify motion parameters to perform axis movement to absolute position
        /// </summary>
        /// <param name="inDeviceID">Axis Identifier</param>
        /// <param name="inDestinationMM">Motion Target Absolute position</param>
        /// <param name="inSlewSpeedMM">Optional parameter, specify the motion target speed, if not set, use the previously used speed</param>
        /// <param name="inAccelerationMM">Optional parameter, specify the motion acceleration and deceleration, if not set, use the previously used parameter</param>
        public ZaberCommandPrmMoveAbsMM(byte inDeviceID,
                                               Single inDestinationMM,
                                               Single inSlewSpeedMM = -1,
                                               Single inAccelerationMM = -1 ,
                                               CancellationToken cancellation_token = default(CancellationToken))
            : this(inDeviceID)
        {
            m_DeviceID = inDeviceID;
            m_DestinationMM = inDestinationMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
            m_CancellationToken = cancellation_token;
        }

       

        #endregion Constructors
    }
}
