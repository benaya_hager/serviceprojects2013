﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandPrmAxisMonitoringThread : ZaberCommandEventParameters
    {
        #region Public Properties
 
        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        private Int32 m_Timeout;
        /// <summary>
        /// Get/Set the read device state moitor thread.
        /// </summary>
        public Int32 Timeout
        {
            get { return m_Timeout; }
            set { m_Timeout = value; }
        }
        

        #endregion Public Properties

        #region Constructors

        public ZaberCommandPrmAxisMonitoringThread(byte deviceID, Int32 timeout) : base(deviceID) { m_Timeout = timeout; }

        
          

        #endregion Constructors
    }
}
