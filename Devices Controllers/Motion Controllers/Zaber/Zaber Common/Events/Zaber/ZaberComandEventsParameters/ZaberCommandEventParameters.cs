﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandEventParameters
    {
        #region Public Properties

        protected Byte   m_DeviceID;

        public Byte DeviceID
        {
            get { return  m_DeviceID; }
            set {  m_DeviceID = value; }
        }
        

        #endregion

        #region Constructors

        public ZaberCommandEventParameters(Byte deviceID)
        {
            m_DeviceID = deviceID;
        }

        #endregion
    }
}
