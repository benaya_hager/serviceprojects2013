﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandPrmMoveAxesRelMM : ZaberCommandEventParameters
     {
        #region Public Properties

         private Byte[] m_DeviceIDs;
         /// <summary>
         /// Get/Set the list of involved devices
         /// </summary>
         public Byte[] DeviceIDs
         {
             get { return m_DeviceIDs; }
             set { m_DeviceIDs = value; }
         }
         

        private Single[] m_OffsetsMM;
        /// <summary>
        /// Get/Set the list of offset from actual axes positions, the order based on DeviceIDs
        /// </summary>
        public Single[] OffsetsMM { get { return m_OffsetsMM; } set { m_OffsetsMM = value; } }

        private Single[] m_SlewSpeedMM;
        /// <summary>
        /// Get/Set the parameters specified target speed of each aces based on DeviceIDs order
        /// </summary>
        public Single[] SlewSpeedMM { get { return m_SlewSpeedMM; } set { m_SlewSpeedMM = value; } }

        private Single[] m_AccelerationMM;
        /// <summary>
        /// Get/Set the parameters specified target Acceleration of each aces based on DeviceIDs order
        /// </summary>
        public Single[] AccelerationMM { get { return m_AccelerationMM; } set { m_AccelerationMM = value; } }

       
        private CancellationToken m_CancellationToken;
        public CancellationToken cancellationToken
        {
            get { return m_CancellationToken; }
            set { m_CancellationToken = value; }
        }

        #endregion Public Properties

        #region Constructors

        public ZaberCommandPrmMoveAxesRelMM(Byte[] inDeviceIDs) :
            base(inDeviceIDs[0])
        { }

        /// <summary>
        /// Specify motion parameters to perform axis movement to absolute position
        /// </summary>
        /// <param name="inDeviceID">Axes Identifiers</param>
        /// <param name="inDestinationMM">Offsets from actual axes position to move</param>
        /// <param name="inSlewSpeedMM">Specify the motion target speed,if this parameter set below zero, use the previously used speed</param>
        /// <param name="inAccelerationMM">Specify the motion acceleration and deceleration, if this parameter set below zero,  use the previously used parameter</param>
        public ZaberCommandPrmMoveAxesRelMM(byte[] inDeviceIDs,
                                               Single[] offsetsMM,
                                               Single[] inSlewSpeedMM,
                                               Single[] inAccelerationMM,
                                               CancellationToken cancellation_token = default(CancellationToken))
            : this(inDeviceIDs)
        {
            m_DeviceIDs = inDeviceIDs;
            m_OffsetsMM = offsetsMM;
            m_SlewSpeedMM = inSlewSpeedMM;
            m_AccelerationMM = inAccelerationMM;
            m_CancellationToken = cancellation_token;
        }

       

        #endregion Constructors
    }
}
