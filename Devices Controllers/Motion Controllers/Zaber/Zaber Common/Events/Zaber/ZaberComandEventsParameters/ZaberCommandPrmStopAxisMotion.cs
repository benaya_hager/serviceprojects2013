﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters
{
    public class ZaberCommandPrmStopAxisMotion : ZaberCommandEventParameters
    {
         #region Public Properties
 
        private String m_AxisName = "";
        public String AxisName { get { return m_AxisName; } set { m_AxisName = value; } }

        

        #endregion Public Properties

        #region Constructors

        public ZaberCommandPrmStopAxisMotion(byte deviceID) : base (deviceID) { }

        
         

        //public ZaberCommandPrmStopAxisMotion(String axis_name)
        //    : this()
        //{
        //    m_AxisName = axis_name;
        //}

        #endregion Constructors
    }
}
