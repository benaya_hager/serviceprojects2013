﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFW;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;

namespace Zaber_Common.Events.Zaber
{
    public class ZaberCommandEvent : IEventData
    {
        #region Public Properties

        private ZaberCommandEventTypes m_cmdType;
        public ZaberCommandEventTypes CmdType
        {
            get { return m_cmdType; }
            private set { m_cmdType = value; }
        }

        private ZaberCommandEventParameters m_CommandParameters;
        public ZaberCommandEventParameters CommandParameters
        {
            get { return m_CommandParameters; }
            private set { m_CommandParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public ZaberCommandEvent(ZaberCommandEventTypes incmdType, ZaberCommandEventParameters prms)
        {
            m_cmdType = incmdType;
            m_CommandParameters = prms;
        }

        #endregion Constructors
    }
}
