﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NS_Common.ViewModels.Common;

namespace Zaber_Common
{
    public class BaseAxis : ViewModelBase
    {

        #region Public properties

    
        protected int m_AxisID = 0;
        public int AxisID
        {
            get { return m_AxisID; }
            set { m_AxisID = value; }
        }

        #region Binding Properties

        [XmlIgnore]
        public RelayCommand<Object> StopMotion { get; protected set; }
        [XmlIgnore]
        protected Boolean m_ButtonStopMotionVisible = true;
        [XmlIgnore]
        public Boolean ButtonStopMotionVisible
        {
            get { return m_ButtonStopMotionVisible; }
            set
            {
                m_ButtonStopMotionVisible = value;
                RaisePropertyChanged("ButtonStopMotionVisible");
            }
        }

        [XmlIgnore]
        public RelayCommand<Single> SendToTarget { get; protected set; }
        [XmlIgnore]
        protected Boolean m_ButtonSendToTargetVisible = true;
        [XmlIgnore]
        public Boolean ButtonSendToTargetVisible
        {
            get { return m_ButtonSendToTargetVisible; }
            set
            {
                m_ButtonSendToTargetVisible = value;
                RaisePropertyChanged("ButtonSendToTargetVisible");
            }
        }

        [XmlIgnore]
        public RelayCommand<Int32> StartStatusReadTimer { get; protected set; }
        [XmlIgnore]
        protected Boolean m_ButtonStartStatusReadTimerVisible = true;
        [XmlIgnore]
        public Boolean ButtonStartStatusReadTimerVisible
        {
            get { return m_ButtonStartStatusReadTimerVisible; }
            set
            {
                m_ButtonStartStatusReadTimerVisible = value;
                RaisePropertyChanged("ButtonStartStatusReadTimerVisible");
            }
        }

        [XmlIgnore]
        public RelayCommand<Object> StopStatusReadTimer { get; protected set; }
        [XmlIgnore]
        protected Boolean m_ButtonStopStatusReadTimerVisible = true;
        [XmlIgnore]
        public Boolean ButtonStopStatusReadTimerVisible
        {
            get { return m_ButtonStopStatusReadTimerVisible; }
            set
            {
                m_ButtonStopStatusReadTimerVisible = value;
                RaisePropertyChanged("ButtonStopStatusReadTimerVisible");
            }
        }


        #endregion

        #endregion

        #region Constructors

        public BaseAxis()
        {

        }

        public BaseAxis(Int32  axisID ) : 
            this()
        {
            m_AxisID = axisID;

           
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return String.Format("Axis {0}", m_AxisID);
        }

        /// <summary>
        /// Starts automaticaly status motitoring thread
        /// </summary>
        /// <param name="timeout">Read status flags timeot</param> 
        public virtual void ExecuteStartStatusReadTimer(int timeout)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stop automaticaly status motitoring thread
        /// </summary> 
        public virtual void ExecuteStopStatusReadTimer()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  Read Actual position using  ReturnCurrentPosition  (Cmd 60) 
        /// </summary>
        /// <param name="target_speed">Motion target speed</param>
        public virtual void UpdateActualPositionData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  Define the target speed using Set Target Speed (Cmd 42) 
        /// </summary>
        /// <param name="target_speed">Motion target speed</param>
        public virtual void SetTargetSpeed(float target_speed)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  Define the Acceleration using Set Acceleration (Cmd 43)</param>
        /// </summary>
        /// <param name="acceleration">Motion Acceleration</param>
        public virtual void SetAcceleration(float acceleration)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Send axis to absolute encoder position using MoveAbsolute (Cmd 20)
        /// </summary>
        /// <param name="target_position">target position in MM</param>
        public virtual void SendAxisToTargetPosition(Single target_position)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
