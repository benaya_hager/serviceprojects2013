﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Common.Service;
using Zaber_Common.Service;

namespace Zaber_Common
{
    public class AxesCollection : AsyncTrulyObservableCollection<BaseAxis>  // ObservableDictionary<Int32, BaseAxis>
    {
        #region Private members
            


            private BaseAxis GetAxisByKey(int lIndex)
            {
                foreach (BaseAxis Axis in this)
                    if (Axis.AxisID == lIndex)
                        return Axis;
                return null;
            }
             

        #endregion

        #region Public Functions
            /// <summary>
            /// Return first free index from available in the list 
            /// </summary>
            /// <returns>First available position index in the list</returns>
            public int GetFreeIndex()
            {
                int lIndex = 1;
                while (true)
                {
                    if (!this.ContainsKey(lIndex))
                        break;
                    lIndex++;
                }
                return lIndex;
            }

            public bool ContainsKey(int lIndex)
            {
                foreach (BaseAxis Axis in this)
                    if (Axis.AxisID == lIndex)
                        return true;
                return false;
            }


            //Get/Set the element at specific Index
            /// <summary>
            /// Get/Set the element at specific Index
            /// </summary>
            /// <param name="index">The zero-based index of the element</param>
            /// <returns>The element stored in the List</returns>
            public new BaseAxis this[int index]
            {
                get
                {
                    return GetAxisByKey(index);
                }
                set
                {
                    //if (this.ContainsKey(index))
                        //this.Remove(index);
                    //var res = GetAxisByKey(index);
                    //if (null != res)
                    Remove(GetAxisByKey(index)); 
                    base.Add(value);
                }
            }

            /// <summary>
            /// Add Axis based on  SortedList collection, 
            /// and store direct access parameter
            /// </summary>
            /// <param name="item">Handle of a SingleAxis to add</param>
            /// <returns>Return my direct access parameter</returns> 
            public new Int32 Add(BaseAxis item)
            {
                Int32 m_Index = GetFreeIndex();
                item.AxisID = m_Index;
                base.Add(item);
                return m_Index;
            }

            /// <summary>
            /// Find item from  SortedList collection by AxisIndex created in Add process
            /// </summary>
            /// <param name="item">Axis to remove</param>
            public new  void Remove(BaseAxis item)
            {
                if (this.ContainsKey(item.AxisID))
                {
                    OnBeforeAxisRemove(this, new AxisEventArguments( item)); 

                    base.Remove(item);
                }
            }

            /// <summary>
            /// Find item from  SortedList collection by AxisIndex created in Add process
            /// </summary>
            /// <param name="Index">Axis Index to remove</param>
            public void Remove(Int32 Index)
            {
                if (this.ContainsKey(Index))
                {
                    OnBeforeAxisRemove(this, new AxisEventArguments(this[Index]));
                    //this[Index].RemoveEventsHandlers();

                    base.Remove(GetAxisByKey(Index));
                    //base.Remove(Index);
                }
            }

            public new void Clear()
            {
                foreach (BaseAxis tmpAxis in this)
                {
                    OnBeforeAxisRemove(this, new AxisEventArguments(tmpAxis));
                }
                base.Clear(); 
            }
        #endregion
        
        #region Public events delegate functions
            #region Before Axis Remove
                /// <summary>
                /// The events delegate function. Occurs when Axis is going to be removed from performance loop.
                /// </summary>
                /// <param name="Sender">The control that raised event handler </param>
                /// <param name="args"></param>
                public delegate void BeforeAxisRemoveHandler(Object Sender, AxisEventArguments args);
                public event BeforeAxisRemoveHandler BeforeAxisRemove;
                protected void OnBeforeAxisRemove(object Sender, AxisEventArguments args)
                {
                    if (BeforeAxisRemove != null)
                        BeforeAxisRemove(Sender, args);
                }
            #endregion
        #endregion
    }
}
