﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Interfaces;

namespace Zaber_Common.Service
{
    #region Units converter Interface definition

    public class UnitsConverter
    {
        /// <summary>
        /// Convert Zaber units to millimeters
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="IU_Value">The value to be converted in Zaber IU units</param>
        /// <returns>Corresponding value in millimeters</returns>
        public static Single IU2MM(ISingleAxis Axis, Single IU_Value)
        {
            return IU_Value / Axis.Millimeter_To_IU_Coeff;
        }

        /// <summary>
        /// Convert millimeters to Zaber units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="MM_Value">The value to be converted in millimeters</param>
        /// <returns>Corresponding value in Zaber units</returns>
        public static Int32 MM2IU(ISingleAxis Axis, Single MM_Value)
        {
            return  Convert.ToInt32(  MM_Value * Axis.Millimeter_To_IU_Coeff);
        }

        /// <summary>
        /// Convert Zaber Slew units to millimeters/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="SlewIU_Value">The value to be converted in Zaber Slew IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Single SlewIU2MMS(ISingleAxis Axis, Single SlewIU_Value)
        {
            return SlewIU_Value / Axis.MillimeterSecond_To_IU_SlewCoeff;
        }

        /// <summary>
        ///  Convert milimetrs\Second to Zaber Slew units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="SlewMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Zaber Slew units</returns>
        public static Single MMS2SlewIU(ISingleAxis Axis, Single SlewMMS_Value)
        {
            return SlewMMS_Value * Axis.MillimeterSecond_To_IU_SlewCoeff;
        }

        /// <summary>
        /// Convert Zaber Acceleration units to milimetrs/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="AccelerationIU_Value">The value to be converted in Zaber Acceleration IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Single AccelerationIU2MMS(ISingleAxis Axis, Single AccelerationIU_Value)
        {
            return AccelerationIU_Value / Axis.MillimeterSecond_To_IU_AccelerationCoeff;
        }

        /// <summary>
        ///  Convert milimetrs\Second to Zaber Acceleration units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="AccelerationMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Zaber Acceleration units</returns>
        public static Single MMS2AccelerationIU(ISingleAxis Axis, Single AccelerationMMS_Value)
        {
            return AccelerationMMS_Value * Axis.MillimeterSecond_To_IU_AccelerationCoeff;
        }

        /// <summary>
        /// Convert Zaber Velocity units to milimetrs/Second
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="VelocityIU_Value">The value to be converted in Zaber Velocity IU units</param>
        /// <returns>Corresponding value in Millimeters/Second</returns>
        public static Single VelocityIU2MMS(ISingleAxis Axis, Single VelocityIU_Value)
        {
            return VelocityIU_Value / Axis.MillimeterSecond_To_IU_VelocityCoeff;
        }

        /// <summary>
        ///  Convert milimetrs\Second to Zaber Velocity units
        /// </summary>
        /// <param name="Axis">Current axis</param>
        /// <param name="VelocityMMS_Value">The value to be converted in Millimeters/Second</param>
        /// <returns>Corresponding value in Zaber Velocity units</returns>
        public static Single MMS2VelocityIU(ISingleAxis Axis, Single VelocityMMS_Value)
        {
            return VelocityMMS_Value * Axis.MillimeterSecond_To_IU_VelocityCoeff;
        }
    }

    #endregion Units convertor Interface definition
}
