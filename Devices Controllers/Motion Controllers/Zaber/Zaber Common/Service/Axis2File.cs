﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Zaber_Common.Interfaces;

namespace Zaber_Common.Service
{

    // Axis list class which will be serialized
    [XmlRoot("AxisFile")]
    public class Axis2File
    {
        /// <summary>
        /// List of  Axis's
        /// </summary>
        private ArrayList ListSingleAxis;

        public Axis2File()
        {
            ListSingleAxis = new ArrayList();
        }

        [XmlElement("Axis")]
        public SingleAxisXMLItem[] Items
        {
            get
            {
                SingleAxisXMLItem[] items = new SingleAxisXMLItem[ListSingleAxis.Count];
                ListSingleAxis.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                SingleAxisXMLItem[] items = (SingleAxisXMLItem[])value;
                ListSingleAxis.Clear();
                foreach (SingleAxisXMLItem item in items)
                    ListSingleAxis.Add(item);
            }
        }

        public int AddItem(SingleAxisXMLItem item)
        {
            return ListSingleAxis.Add(item);
        }
    }

    // Items in the shopping list
    public class SingleAxisXMLItem
    {
        #region XML fields list

        [XmlAttribute("Axis_Idetifier")]
        public Int32 Axis_Idetifier; 
        [XmlAttribute("Millimeter_To_IU_Coeff")]
        public Single Millimeter_To_IU_Coeff;
        [XmlAttribute("MillimeterSecond_To_IU_SlewCoeff")]
        public Single MillimeterSecond_To_IU_SlewCoeff;
        [XmlAttribute("MillimeterSecond_To_IU_AccelerationCoeff")]
        public Single MillimeterSecond_To_IU_AccelerationCoeff;
         



        #endregion XML fields list

        #region Constructors

        /// <summary>
        /// Base empty constructor, no of members will be initialized
        /// </summary>
        public SingleAxisXMLItem()
        {
        }

        /// <summary>
        /// Base constructor, will initialize all the values corresponding to the list recived
        /// If new items added to the SingleAxis, necessary to update the internal and function parameters lists
        /// </summary>
        /// <param name="inMillimeter_To_IU_Coeff">Position in millimeter to EasyMotion IU units coefficient</param>
        /// <param name="inMillimeterSecond_To_IU_SlewCoeff">Slew in millimeter per second to EasyMotion IU units coefficient</param>
        /// <param name="inMillimeterSecond_To_IU_AccelerationCoeff">Acceleratio in millimeter per Second^2 to EasyMotion IU units coefficient</param>
        public SingleAxisXMLItem(String inAxisName,
                            Byte inDevice_Idetifier,
                            Single inMillimeter_To_IU_Coeff,
                            Single inMillimeterSecond_To_IU_SlewCoeff,
                            Single inMillimeterSecond_To_IU_AccelerationCoeff)
        {
            Axis_Idetifier = inDevice_Idetifier ;
            
            Millimeter_To_IU_Coeff = inMillimeter_To_IU_Coeff;
            MillimeterSecond_To_IU_SlewCoeff = inMillimeterSecond_To_IU_SlewCoeff;
            MillimeterSecond_To_IU_AccelerationCoeff = inMillimeterSecond_To_IU_AccelerationCoeff;
            
        }

        /// <summary>
        /// Base constructor, will initialize all the values corresponding to the values stored
        /// in recived inSingleAxis object reference
        /// If new items added to the SingleAxis, necessary to update internal the list
        /// </summary>
        /// <param name="inSingleAxis">The object reference to be stored</param>
        public SingleAxisXMLItem(ISingleAxis inSingleAxis)
        { 
            Axis_Idetifier = inSingleAxis.DeviceIdentifier; 
            Millimeter_To_IU_Coeff = inSingleAxis.Millimeter_To_IU_Coeff;
            MillimeterSecond_To_IU_SlewCoeff = inSingleAxis.MillimeterSecond_To_IU_SlewCoeff;
            MillimeterSecond_To_IU_AccelerationCoeff = inSingleAxis.MillimeterSecond_To_IU_AccelerationCoeff;
            
        }

        #endregion Constructors
    }
}
