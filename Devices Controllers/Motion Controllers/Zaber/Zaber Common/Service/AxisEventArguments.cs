﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Service
{
    public class DeviceEventArguments : EventArgs
    {
        #region Constructor

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public DeviceEventArguments()
            : base()
        {
        }

        /// <summary>
        /// Constructor that initialize EvetArgument object
        /// </summary>
        /// <param name="inAxisDescriptor"></param>
        public DeviceEventArguments(BaseZaberDevice inAxisDescriptor)
            : this()
        {
            AxisDescriptor = inAxisDescriptor; //.Clone();
        }

        #endregion Constructor

        #region Public properties

        private BaseZaberDevice m_AxisDescriptor;

        /// <summary>
        /// The completed Axis descriptor
        /// </summary>
        public BaseZaberDevice AxisDescriptor
        {
            get { return m_AxisDescriptor; }
            set { m_AxisDescriptor = value; }
        }

        #endregion Public properties
    }
}
