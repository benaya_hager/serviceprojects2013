﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Service
{
    /// <summary>
    /// 		* 0  - idle, not currently executing any instructions

    /// 		* 1  - executing a home instruction
    /// 		
    /// 		* 10 - executing a manual move (i.e. the manual control knob is turned)
    /// 		
    /// 		* 11 - executing a manual move in Displacement Mode (A-Series devices only)
    /// 		
    /// 		* 20 - executing a move absolute instruction
    /// 		
    /// 		* 21 - executing a move relative instruction
    /// 		
    /// 		* 22 - executing a move at constant speed instruction
    /// 		
    /// 		* 23 - executing a stop instruction (i.e. decelerating)
    /// 		
    /// 		* 65 - device is parked (A-Series devices with FW 6.02 and up only. FW 6.01 returns 0 when parked)
    /// </summary>
    public enum _DeviceStatus : byte
    {
        [Tools.String_Enum.StringValue("Not Availible")]
        UNKNOWN =  255,  
        [Tools.String_Enum.StringValue("idle, not currently executing any instructions")]
        IDLE =  0  ,
        [Tools.String_Enum.StringValue("Homing, executing a home instruction")]
        HOMING = 1,
        [Tools.String_Enum.StringValue("Manual movement, executing a manual move (i.e. the manual control knob is turned)")]
        MANUAL_MOVEMENT = 10 ,
        [Tools.String_Enum.StringValue("Manual movement Displacement, executing a manual move in Displacement Mode (A-Series devices only)")]
        MANUAL_MOVEMENT_DISPLACEMENT = 11,
        [Tools.String_Enum.StringValue("Absolute movement, executing a move absolute instruction")]
        MOVES_ABSOLUTE = 20,
        [Tools.String_Enum.StringValue("Relative movement, executing a move relative instruction")]
        MOVES_RELATIVE = 21,
        [Tools.String_Enum.StringValue("Constant speed movement, executing a move at constant speed instruction")]
        MOVES_AT_CONSTANT_SPEED = 22,
        [Tools.String_Enum.StringValue("Stop Motion, executing a stop instruction (i.e. decelerating)")]
        STOP_IN_PROGRESS = 23,
        [Tools.String_Enum.StringValue("Parked,  device is parked (A-Series devices with FW 6.02 and up only. FW 6.01 returns 0 when parked)")]
        PARKED = 65 
    }
}
