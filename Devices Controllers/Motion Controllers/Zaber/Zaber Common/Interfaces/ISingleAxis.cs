﻿using System;
using Zaber_Common.Service;
namespace Zaber_Common.Interfaces
{
    public interface ISingleAxis
    {
        bool AxisHomed { get; }
        string AxisName { get; }
        Single CurrentPositionMM { get; }
        Int32 CurrentPositionIU { get; }
        Single CurrentSpeedMM { get; }
        Single Millimeter_To_IU_Coeff { get; }
        Single MillimeterSecond_To_IU_AccelerationCoeff { get;  }
        Single MillimeterSecond_To_IU_SlewCoeff { get;  }
        Single MillimeterSecond_To_IU_VelocityCoeff { get; }


        Zaber_Common.Service._DeviceStatus DeviceStatus { get; }
        bool DeviceStatusMonitorigActive { get; }
        string ErrorMessage { get; }
        void ExecuteStartStatusReadTimer(int timeout);
        void ExecuteStopStatusReadTimer();
        
        bool MotionComplete { get; }
        void MoveAxisRelative(float target_position);
        string Notification { get; set; }
        void SendAxisHome();
        void SendAxisToTargetPosition(float target_position);
        void SetAcceleration(float acceleration);
        void SetAxisTrackingMode(bool trackingMode, int trackingModeUpdateInterval = -1);
        void SetTargetSpeed(float target_speed);
        Int32 DeviceIdentifier { get; set; }
        void StopAxisMotion(object sender);
        decimal TargetAxisPosition { get; }
        void UpdateActualPositionData();
        void UpdateAxisHomedStatus();
        void UpdateConvertationDataValues(SingleAxisXMLItem xmlItem);
    }
}
