﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaber_Common.Interfaces
{
    public interface IZaberMotionController
    {
        string ChannelName { get; set; }
        event SFW.DataEventHandler DataReceived;
        bool isSimulationMode { get; }
        System.Collections.Generic.List<int> ListOfAvailableAxes { get; }
        int LoadAxisXmlFile(string FileName);
        void UpdateAllAxes(Zaber_Common.BaseZaberDevice axis);
        void MoveAxisAbsolute(int deviceID, float target_position, float target_speed, float acceleration);
        void MoveAxisAbsolute(Zaber_Common.BaseZaberDevice axis, float target_position, float target_speed, float acceleration);
        void MoveAxisRelative(int deviceID, float offset, float target_speed, float acceleration);
        void MoveAxisRelative(Zaber_Common.BaseZaberDevice axis, float offset, float target_speed, float acceleration);
        string Name { get; }
        void OnUpdateSplashScreen(object sender, NS_Common.Event_Arguments.UpdateSplashEventArgs args);
        int SaveAxisXmlFile(Zaber_Common.ZaberDevicesCollection device_list, string FileName);
        void Send(string data);
        void SendAxisHome(int deviceID);
        void SendAxisHome(ISingleAxis axis);
        void SetAxisTrackingMode(int deviceID);
        void SetAxisTrackingMode(ISingleAxis axis, bool trackingMode = true, int trackingModeUpdateInterval = -1);
        void StartAxisMonitoringThread(int deviceID, int timeout);
        void StartAxisMonitoringThread(ISingleAxis axis, int timeout);
        void StopAllAxesMotion();
        void StopAxesMotion(byte[] list_of_devices_ids);
        void StopAxisMonitoringThread(int deviceID);
        void StopAxisMonitoringThread(ISingleAxis axis);
        void StopAxisMotion(int deviceID);
        void StopAxisMotion(ISingleAxis axis);
        short WaitMotionCompleted(int deviceID, Int32 target_IU, System.Threading.CancellationToken ct);
        short WaitMotionCompleted(ISingleAxis axis, Int32 target_IU, System.Threading.CancellationToken ct);
        Zaber_Common.ZaberDevicesCollection ZaberAxesCollection { get; }
    }
}
