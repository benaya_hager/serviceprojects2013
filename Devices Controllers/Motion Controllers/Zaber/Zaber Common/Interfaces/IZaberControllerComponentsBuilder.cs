﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFW;

namespace Zaber_Common.Interfaces
{
    public interface IZaberControllerComponentsBuilder
    {
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; set; }
        Microsoft.Practices.Unity.IUnityContainer Container { get; }
        RuntimeObjectsFactory GuiFactory { get; }
    }
}
