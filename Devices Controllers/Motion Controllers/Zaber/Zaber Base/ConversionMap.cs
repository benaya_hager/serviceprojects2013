﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// A collection of conversion tables to use when converting units of 
    /// measure.
    /// </summary>
    public class ConversionMap
    {
        /// <summary>
        /// Gets the conversions for all the common units of measure included 
        /// with the Zaber library.
        /// </summary>
        public static ConversionMap Common { get; private set; }

        static ConversionMap()
        {
            Common = new ConversionMap();
            Common.Add(
                MotionType.Linear,
                MeasurementType.Position,
                ConversionTable.LinearPosition);
            Common.Add(
                MotionType.Rotary,
                MeasurementType.Position,
                ConversionTable.RotaryPosition);
            Common.Add(
                MotionType.Linear,
                MeasurementType.Velocity,
                ConversionTable.LinearVelocity);
            Common.Add(
                MotionType.Rotary,
                MeasurementType.Velocity,
                ConversionTable.RotaryVelocity);
            Common.Add(
                MotionType.Linear,
                MeasurementType.Acceleration,
                ConversionTable.LinearAcceleration);
            Common.Add(
                MotionType.Rotary,
                MeasurementType.Acceleration,
                ConversionTable.RotaryAcceleration);
        }

        private ConversionTable[,] tables;

        public ConversionMap()
        {
            int motionTypeCount = Enum.GetValues(typeof(MotionType)).Length;
            int measurementTypeCount = 
                Enum.GetValues(typeof(MeasurementType)).Length;
            tables = new ConversionTable[motionTypeCount,measurementTypeCount];
        }

        /// <summary>
        /// Add an entry to the map showing which conversion table to use for a 
        /// combination of motion and measurement types.
        /// </summary>
        /// <param name="motionType">The type of motion represented.</param>
        /// <param name="measurementType">The type of measurement represented.
        /// </param>
        /// <param name="table">The conversion table to use for the given
        /// types of motion and measurement.</param>
        public void Add(
            MotionType motionType, 
            MeasurementType measurementType, 
            ConversionTable table)
        {
            tables[(int)motionType, (int)measurementType] = table;
        }

        /// <summary>
        /// Find the conversion table to use for a combination of motion and 
        /// measurement types.
        /// </summary>
        /// <param name="motionType">The type of motion to find.</param>
        /// <param name="measurementType">The type of measurement to fine.
        /// </param>
        /// <returns>The conversion table </returns>
        public ConversionTable Find(
            MotionType motionType, 
            MeasurementType measurementType)
        {
            return tables[(int)motionType, (int)measurementType];
        }

        /// <summary>
        /// Find the conversion table that contains a specific unit of measure.
        /// </summary>
        /// <param name="unit">The unit of measure to find.</param>
        /// <returns>The conversion table </returns>
        public ConversionTable Find(UnitOfMeasure unit)
        {
            foreach (var table in tables)
            {
                if (table != null && table.UnitsOfMeasure.Contains(unit))
                {
                    return table;
                }
            }
            return null;
        }
    }
}
