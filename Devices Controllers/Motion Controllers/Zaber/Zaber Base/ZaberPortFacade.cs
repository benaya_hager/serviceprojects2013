using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Zaber
{
    /// <summary>
    /// This is the main class that most applications will interact with. 
    /// </summary>
    /// <remarks>
    /// The standard pattern is:
    /// <list type="number">
    /// <item>Call <see cref="GetPortNames"/> and display them to the user.</item>
    /// <item>When the user picks one, pass it to <see cref="Open"/>.</item>
    /// <item>Either call <see cref="GetDevice(byte)"/> to get a specific device, call
    ///     <see cref="Devices"/> to get the all of them, call 
    ///     <see cref="GetConversation(byte)"/> to get a specific conversation, or
    ///     call <see cref="Conversations"/> to get all of them.</item>
    /// <item>Either make requests from conversations or send commands to 
    ///     devices and register to receive the response events.</item>
    /// </list>
    /// See the ZaberDeviceDemo and ZaberConversationDemo for some example code.
    /// </remarks>
    public class ZaberPortFacade : IDisposable
    {
        public ZaberPortFacade()
        {
            DeviceCollection allDevices = new DeviceCollection();
            allDevices.DeviceNumber = 0;
            _devices[0] = allDevices;

            _selectedConversation = _conversations[0] = 
                new ConversationCollection(allDevices);
            IsInvalidateEnabled = true;
        }

        #region Public Methods
        /// <summary>
        /// Check to see if there is a conversation with the given device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device whose
        /// conversation you want.</param>
        /// <returns>true if the conversation exists.</returns>
        public bool ContainsConversation(byte deviceNumber)
        {
            lock (_locker)
            {
                return _conversations.ContainsKey(deviceNumber);
            }
        }

        /// <summary>
        /// Check to see if there is a device with the given device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device you want.</param>
        /// <returns>true if the device exists.</returns>
        public bool ContainsDevice(byte deviceNumber)
        {
            return ContainsConversation(deviceNumber);
        }

        /// <summary>
        /// Look up a conversation by device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device whose 
        /// conversation you want. Device number 0 or other alias numbers will
        /// return a ConversationCollection that represents all the devices
        /// using that alias number.</param>
        /// <returns>The conversation with the requested device number.</returns>
        /// <exception cref="KeyNotFoundException">If no conversation has the
        /// requested device number.</exception>
        /// <remarks>To avoid the exception, check <see cref="ContainsDevice"/>
        /// before calling this method. Every device has a matching 
        /// conversation.</remarks>
        public Conversation GetConversation(byte deviceNumber)
        {
            return GetConversation(deviceNumber, 0);
        }

        /// <summary>
        /// Look up a conversation by device number and axis number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device whose 
        /// conversation you want. Device number 0 or other alias numbers will
        /// return a ConversationCollection that represents all the devices
        /// using that alias number.
        /// </param>
        /// <param name="axisNumber">The number of the axis you want. Numbers
        /// 1 through 9 will return individual axes, and 0 will return the main
        /// conversation.</param>
        /// <returns>The requested conversation.</returns>
        /// <exception cref="KeyNotFoundException">If no conversation has the 
        /// requested device number and axis number.</exception>
        /// <seealso cref="ContainsDevice"/>
        public Conversation GetConversation(byte deviceNumber, int axisNumber)
        {
            lock (_locker)
            {
                Conversation conversation;
                if (!_conversations.TryGetValue(deviceNumber, out conversation))
                {
                    throw new KeyNotFoundException(String.Format(
                        CultureInfo.CurrentCulture,
                        "Device number {0} not found.",
                        deviceNumber));
                }
                if (axisNumber == 0)
                {
                    return conversation;
                }
                if (axisNumber > conversation.Axes.Count ||
                    axisNumber < 0)
                {
                    throw new KeyNotFoundException(String.Format(
                        CultureInfo.CurrentCulture,
                        "Axis number {0} not found on device number {1}.",
                        axisNumber,
                        deviceNumber));
                }
                return conversation.Axes[axisNumber - 1];
            }
        }

        /// <summary>
        /// Look up a conversation collection by device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device whose 
        /// conversation you want.</param>
        /// <returns>The conversation collection with the requested device number.</returns>
        /// <exception cref="KeyNotFoundException">If no conversation has the
        /// requested device number.</exception>
        /// <exception cref="InvalidCastException">If the conversation is a single
        /// conversation instead of a collection.</exception>
        /// <remarks>
        /// To avoid the exceptions, you can safely get a conversation collection
        /// as follows:
        /// <example>
        /// if (portFacade.ContainsDevice(deviceNumber))
        /// {
        ///     ConversationCollection conversations =
        ///         portFacade.GetConversation(deviceNumber)
        ///         as ConversationCollection;
        ///     if (conversations != null)
        ///     {
        ///         // Use conversations for something
        ///     }
        /// }
        /// </example>
        /// </remarks>
        public ConversationCollection GetConversationCollection(byte deviceNumber)
        {
            return (ConversationCollection)GetConversation(deviceNumber);
        }

        /// <summary>
        /// Look up a device by device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device you want. 
        /// Device number 0 or other alias numbers will return a 
        /// DeviceCollection that represents all the devices using that alias 
        /// number.</param>
        /// <returns>The device with the requested device number.</returns>
        /// <exception cref="KeyNotFoundException">If no device has the 
        /// requested device number.</exception>
        /// <seealso cref="ContainsDevice"/>
        public ZaberDevice GetDevice(byte deviceNumber)
        {
            return GetDevice(deviceNumber, 0);
        }

        /// <summary>
        /// Look up a device by device number and axis number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device you want. 
        /// Device number 0 or other alias numbers will return a 
        /// DeviceCollection that represents all the devices using that alias 
        /// number.</param>
        /// <param name="axisNumber">The number of the axis you want. Numbers
        /// 1 through 9 will return individual axes, and 0 will return the main
        /// device.</param>
        /// <returns>The requested device.</returns>
        /// <exception cref="KeyNotFoundException">If no device has the 
        /// requested device number and axis number.</exception>
        /// <seealso cref="ContainsDevice"/>
        public ZaberDevice GetDevice(byte deviceNumber, int axisNumber)
        {
            return GetConversation(deviceNumber, axisNumber).Device;
        }

        /// <summary>
        /// Look up a device collection by device number.
        /// </summary>
        /// <param name="deviceNumber">The number of the device you want.</param>
        /// <returns>The device with the requested device number.</returns>
        /// <exception cref="KeyNotFoundException">If no device has the 
        /// requested device number.</exception>
        /// <exception cref="InvalidCastException">If the device is a single
        /// device instead of a collection.</exception>
        /// <remarks>
        /// To avoid the exceptions, you can safely get a device collection
        /// as follows:
        /// <example>
        /// if (portFacade.ContainsDevice(deviceNumber))
        /// {
        ///     DeviceCollection devices =
        ///         portFacade.GetDevice(deviceNumber)
        ///         as DeviceCollection;
        ///     if (devices != null)
        ///     {
        ///         // Use devices for something
        ///     }
        /// }
        /// </example>
        /// </remarks>
        public DeviceCollection GetDeviceCollection(byte deviceNumber)
        {
            return (DeviceCollection)GetDevice(deviceNumber);
        }

        /// <summary>
        /// Open the port and check what devices are present.
        /// </summary>
        /// <param name="portName">One of the port names returned by 
        /// <see cref="GetPortNames"/>.</param>
        /// <remarks>Be sure to call <see cref="Close()"/> when you're finished.</remarks>
        /// <exception cref="LoopbackException">A loopback connection was 
        /// detected on the serial port. To recover from this exception, 
        /// either physically remove the loopback connection or call 
        /// <see cref="OpenWithoutQuery"/>.</exception>
        /// <exception cref="UnauthorizedAccessException">Access is denied to
        /// the port.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">An error occurred while
        /// querying devices.</exception>
        /// <exception cref="LoopbackException">A loopback connection was
        /// detected.</exception>
        public void Open(string portName)
        {
            IZaberPort port;
            lock (_locker)
            {
                port = _port;
            }
            port.Open(portName);

            TimeoutTimer queryTimeoutTimer;
            lock (_locker)
            {
                _currentState = ZaberPortFacadeState.QueryingDevices;
                _deviceIds = new Dictionary<byte, int>();
                _areMessageIdsEnabled = false;
                _isInvalidated = false;
                queryTimeoutTimer = _queryTimeoutTimer;
                _errorDuringQuery = ZaberPortError.None;
                _isVoltageBadDuringQuery = false;
                IsCollisionDetected = false;
            }
            try
            {
                if (port.IsAsciiMode)
                {
                    QueryAsciiDevices(port, queryTimeoutTimer);
                }
                else
                {
                    QueryBinaryDevices(port, queryTimeoutTimer); 
                }
                if (Opened != null)
                {
                    Opened(this, EventArgs.Empty);
                }
            }
            catch (Exception)
            {
                port.Close();
                lock (_locker)
                {
                    _currentState = ZaberPortFacadeState.Closed;
                }
                throw;
            }
        }

        /// <summary>
        /// Open the port without checking what devices are present and without
        /// building the <see cref="ZaberDevice"/> and 
        /// <see cref="Conversation"/> objects to represent those devices.
        /// </summary>
        /// <param name="portName">One of the port names returned by 
        ///     <see cref="GetPortNames"/>.</param>
        /// <remarks>There are always device and conversation objects for 
        /// device number 0 (all devices), but the conversation is empty and 
        /// will not support calls to the Request method. Be sure to call 
        /// <see cref="Close()"/> when you're finished.</remarks>
        /// <exception cref="UnauthorizedAccessException">Access is denied to
        /// the port.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void OpenWithoutQuery(string portName)
        {
            IZaberPort port = null;
            lock (_locker)
            {
                port = _port;
                IsCollisionDetected = false;
            }
            try
            {
                port.Open(portName);
            }
            catch (Exception)
            {
                if (port != null && port.IsOpen)
                {
                    port.Close();
                }
                lock (_locker)
                {
                    _currentState = ZaberPortFacadeState.Closed;
                }
                throw;
            }
            lock (_locker)
            {
                _devices[0].Port = port;
                _selectedConversation = _conversations[0];
                _currentState = ZaberPortFacadeState.Open;
            }
            if (Opened != null)
            {
                Opened(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Private Methods

        private void CheckMessageIdsMode(DataPacket dataPacket)
        {
            DeviceModes newMode = DeviceModes.None;
            IZaberPort port;

            lock (_locker)
            {
                if (!_areMessageIdsMonitored)
                {
                    return;
                }
                DeviceModes oldMode = (DeviceModes)dataPacket.Data;
                bool areMessageIdsEnabledOnDevice =
                    DeviceModes.None !=
                    (oldMode & DeviceModes.EnableMessageIdsMode);
                if (_areMessageIdsEnabled == areMessageIdsEnabledOnDevice)
                {
                    return;
                }
                newMode = oldMode ^ DeviceModes.EnableMessageIdsMode;
                port = _port;
            }

            port.Send(
                dataPacket.DeviceNumber,
                Command.SetDeviceMode,
                (int)newMode);
        }

        /// <summary>
        /// Wait for responses, and then create the collections of 
        /// conversations and devices, based on the entries in _deviceIds.
        /// </summary>
        /// <param name="port">The port to connect to the devices.</param>
        /// <param name="defaultDeviceType">Used for unknown devices.</param>
        /// <param name="devices">The collection of devices.</param>
        /// <param name="conversations">The collection of conversations.</param>
        /// <exception cref="ZaberPortErrorException">An error occurred while
        /// querying devices.</exception>
        /// <exception cref="LoopbackException">A loopback connection was
        /// detected.</exception>
        private void CreateConversations(
            IZaberPort port,
            out DeviceType defaultDeviceType,
            out SortedDictionary<byte, ZaberDevice> devices,
            out SortedDictionary<byte, Conversation> conversations)
        {
            // Copy some member variables so we can release the lock while we
            // query all devices for more details.
            var deviceIds = new Dictionary<byte, int>();
            IDictionary<int, DeviceType> deviceTypeMap;

            lock (_locker)
            {
                if (_errorDuringQuery != ZaberPortError.None)
                {
                    throw new ZaberPortErrorException(_errorDuringQuery);
                }
                if (_deviceIds.ContainsKey(0))
                {
                    throw new LoopbackException(String.Format(
                        CultureInfo.CurrentCulture,
                        "Loopback connection detected on {0}.",
                        port.PortName));
                }
                defaultDeviceType = _defaultDeviceType;

                // some DeviceType objects could be modified. e.g. ASCII command sets
                // Recover the original map.
                deviceTypeMap = new Dictionary<int, DeviceType>();
                foreach (KeyValuePair<int, DeviceType> pair in _deviceTypeMap)
                {
                    deviceTypeMap.Add(pair.Key, new DeviceType(pair.Value));
                }

                foreach (var entry in _deviceIds)
                {
                    deviceIds[entry.Key] = entry.Value;
                }
            }

            _log.Debug("Creating devices and conversations.");
            devices = new SortedDictionary<byte, ZaberDevice>();
            conversations = new SortedDictionary<byte, Conversation>();
            foreach (var entry in deviceIds)
            {
                byte deviceNumber = entry.Key;
                int deviceId = entry.Value;
                var device = new ZaberDevice();
                device.DeviceNumber = deviceNumber;
                device.Port = port;
                if (deviceTypeMap.ContainsKey(deviceId))
                {
                    device.DeviceType = deviceTypeMap[deviceId];
                }
                else
                {
                    device.DeviceType = new DeviceType(defaultDeviceType);
                    device.DeviceType.DeviceId = deviceId;
                }
                Conversation conversation = new Conversation(device);

                devices[entry.Key] = device;
                conversations[entry.Key] = conversation;
            }
        }

        /// <summary>
        /// Send the same request to all devices, and wait for a response.
        /// Yield the response for each request, so that calling code can
        /// follow up before the next request is sent.
        /// </summary>
        /// <param name="conversations">The conversations to send the request
        /// to.</param>
        /// <param name="command">The command to send if the port is in Binary
        /// mode.</param>
        /// <param name="data">The data to send if the port is in Binary 
        /// mode.</param>
        /// <param name="text">A text command to send if the port is in
        /// ASCII mode.</param>
        /// <returns>The responses in the same order as the conversations.
        /// Any device collection conversation (alias, or All device) will have null responses.
        /// If the command is not supported, response is null.</returns>
        private IEnumerable<DeviceMessage> EnumerateRequests(
            IEnumerable<Conversation> conversations,
            Command command,
            int data,
            String text)
        {
            foreach (var conversation in conversations)
            {
                if (!conversation.Device.IsSingleDevice)
                {
                    // skip device collections (all or alias)
                    yield return null;
                    continue;
                }
                if (text != null && conversation.Device.DeviceType.GetCommandByText(text) == null)
                {
                    // skip if command not supported
                    yield return null;
                    continue;
                }
                var topic = conversation.StartTopic();
                if (text != null)
                {
                    conversation.Device.Send(text);
                }
                else
                {
                    if (command == Command.ReturnSetting && data == (int)Command.SetMicrostepResolution
                        && conversation.Device.FirmwareVersion < 500)
                    {
                        // FW 2.93 has fixed resolution of 64.
                        // Also this version does not reply to ReturnSetting(SetMicrostepResolution)
                        // resulting in a Timeout exception when attempting to open port
                        conversation.Device.MicrostepResolution = 64;
                        yield return null;
                        continue;
                    }
                    conversation.Device.Send(command, data, topic.MessageId);
                }
                if (!topic.Wait(_queryTimeoutTimer))
                {
                    throw new RequestTimeoutException();
                }
                if (topic.IsValid)
                {
                    yield return topic.Response;
                }
                else if (IsUnsupportedSetting(topic.Response))
                {
                    yield return null;
                }
                if (_isVoltageBadDuringQuery)
                {
                    // allow low voltage error to go by.
                    _queryTimeoutTimer.Sleep();
                }
            }
            if (IsCollisionDetected)
            {
                // Wait for extra replies from devices with duplicate device numbers.
                QueryTimeoutTimer.Sleep();
            }
        }

        private void GetAliases(
            SortedDictionary<byte, ZaberDevice> devices,
            SortedDictionary<byte, Conversation> conversations,
            DeviceType defaultDeviceType)
        {
            List<Conversation> singleDeviceConversations =
                new List<Conversation>();
            singleDeviceConversations.AddRange(conversations.Values);
            foreach (var conversation in singleDeviceConversations)
            {
                var oldTimer = conversation.TimeoutTimer;
                conversation.TimeoutTimer = _queryTimeoutTimer;
                int aliasNumberAsInt;
                aliasNumberAsInt = conversation.Request(
                    Command.ReturnSetting,
                    (int)Command.SetAliasNumber).Data;
                conversation.TimeoutTimer = oldTimer;
                if (_isVoltageBadDuringQuery)
                {
                    // allow low voltage error to go by.
                    _queryTimeoutTimer.Sleep();
                }
                if (!IsValidByteValue(aliasNumberAsInt) || aliasNumberAsInt == 0)
                {
                    // ignore alias if it's an invalid value or the default.
                    continue;
                }
                byte aliasNumber = (byte)aliasNumberAsInt;
                DeviceCollection aliasDevice;
                ConversationCollection aliasConversation;
                if (devices.ContainsKey(aliasNumber))
                {
                    var deviceInAliasPosition = devices[aliasNumber];
                    if (deviceInAliasPosition.IsSingleDevice)
                    {
                        // ignore alias if it collides with a real device.
                        continue;
                    }
                    aliasDevice = (DeviceCollection)deviceInAliasPosition;
                    aliasConversation =
                        (ConversationCollection)conversations[aliasNumber];
                }
                else
                {
                    aliasDevice = new DeviceCollection();
                    aliasDevice.DeviceNumber = aliasNumber;
                    aliasDevice.DeviceType = new DeviceType(defaultDeviceType);
                    aliasDevice.Port = conversation.Device.Port;
                    aliasDevice.AreMessageIdsEnabled =
                        conversation.Device.AreMessageIdsEnabled;
                    devices[aliasNumber] = aliasDevice;

                    aliasConversation = new ConversationCollection(aliasDevice);
                    conversations[aliasNumber] = aliasConversation;
                }
                aliasDevice.Add(conversation.Device);
                aliasConversation.Add(conversation);
            }
            if (IsCollisionDetected)
            {
                // Wait for extra replies from devices with duplicate device numbers.
                QueryTimeoutTimer.Sleep();
            }
        }

        private bool GetDeviceModes(SortedDictionary<byte, Conversation> conversations)
        {
            bool isFirstDevice = true;
            bool areMessageIdsEnabled = false;
            var responses = EnumerateRequests(
                conversations.Values,
                Command.ReturnSetting,
                (int)Command.SetDeviceMode,
                null);
            foreach (var response in responses)
            {
                if (response == null)
                {
                    // skip alias devices
                    continue;
                }
                var conversation = conversations[response.DeviceNumber];

                DeviceModes deviceMode = (DeviceModes)response.Data;
                bool areMessageIdsEnabledOnDevice =
                    DeviceModes.None !=
                    (deviceMode & DeviceModes.EnableMessageIdsMode);
                if (isFirstDevice)
                {
                    areMessageIdsEnabled = areMessageIdsEnabledOnDevice;
                }
                else if (areMessageIdsEnabledOnDevice != areMessageIdsEnabled)
                {
                    DeviceModes newDeviceMode =
                        deviceMode ^ DeviceModes.EnableMessageIdsMode;
                    conversation.Request(
                        Command.SetDeviceMode,
                        (int)newDeviceMode);
                }
                isFirstDevice = false;
            }
            if (IsCollisionDetected)
            {
                // Wait for extra replies from devices with duplicate device numbers.
                QueryTimeoutTimer.Sleep();
            }
            foreach (var conversation in conversations.Values)
            {
                conversation.Device.AreMessageIdsEnabled = areMessageIdsEnabled;
            }
            return areMessageIdsEnabled;
        }

        /// <summary>
        /// Query each device for the list of text commands that it supports.
        /// </summary>
        /// <param name="conversations">The list of conversations to query.</param>
        /// <param name="queryTimeoutTimer">How long to wait for responses.</param>
        private static void GetTextCommands(
            SortedDictionary<byte, Conversation> conversations,
            TimeoutTimer queryTimeoutTimer)
        {
            foreach (Conversation conversation in conversations.Values)
            {
                if (!(conversation.Device.IsSingleDevice
                        && conversation.Device.AxisNumber == 0))
                {
                    continue; // skip device collections (All, alias) and individual axes
                }

                using (var listener = new DeviceListener(conversation.Device))
                {
                    conversation.Request("help commands list");

                    var device = conversation.Device;
                    var knownCommands = new Dictionary<String, CommandInfo>();
                    foreach (var cmd in device.DeviceType.Commands)
                    {
                        if (cmd.TextCommand != null)
                        {
                            knownCommands[cmd.TextCommand] = cmd;
                        }
                    }
                    device.DeviceType = new DeviceType(device.DeviceType);
                    var asciiCommands = new Dictionary<String, CommandInfo>();
                    bool isDone = false;
                    while (!isDone)
                    {
                        var infoResponse = listener.NextResponse(queryTimeoutTimer);
                        if (infoResponse.MessageType != MessageType.Comment)
                        {
                            continue;
                        }

                        if (infoResponse.Body.Length < 3)
                        {
                            isDone = true;  // empty line signifies end of list
                        }
                        else
                        {
                            CommandInfo commandInfo;
                            var commandParts = infoResponse.Body.Trim().Split();
                            var startPart = Math.Min(commandParts.Length, 1);
                            var partCount = startPart;
                            var hasParams = false;
                            while (
                                startPart + partCount < commandParts.Length &&
                                !commandParts[startPart + partCount].ToCharArray().Any(
                                    c => char.IsUpper(c)))
                            {
                                partCount++;
                            }

                            if (startPart + partCount < commandParts.Length)
                            {
                                hasParams = true;
                            }

                            if (commandParts[startPart] == "set")
                            {
                                startPart++;
                                partCount--;
                                commandInfo = new SettingInfo();
                            }
                            else if (commandParts[startPart] == "get")
                            {
                                startPart++;
                                partCount--;
                                commandInfo = null;
                            }
                            else
                            {
                                commandInfo = new CommandInfo();
                            }
                            var commandName =
                                String.Join(" ", commandParts, startPart, partCount);
                            if (commandInfo == null)
                            {
                                if (!asciiCommands.TryGetValue(
                                    commandName,
                                    out commandInfo))
                                {
                                    commandInfo = new ReadOnlySettingInfo();
                                }
                            }
                            commandInfo.HasParameters = hasParams;
                            commandInfo.TextCommand = commandName;
                            commandInfo.IsBasic =
                                commandParts.Length == 0 ||
                                commandParts[0].Length < 2 ||
                                commandParts[0][1] == 'a';//a=always,o=optional
                            commandInfo.IsAxisCommand =
                                commandParts.Length == 0 ||
                                commandParts[0].Length < 3 ||
                                commandParts[0][2] == 'a';//a=axis,d=device
                            CommandInfo knownCommand;
                            while (partCount >= 1 && commandInfo.HelpText == null)
                            {
                                var parentName = String.Join(
                                    " ",
                                    commandParts,
                                    startPart,
                                    partCount);
                                if (knownCommands.TryGetValue(parentName, out knownCommand))
                                {
                                    commandInfo.SetFormattedHelpText(knownCommand.HelpText);
                                    commandInfo.RequestDataType =
                                        knownCommand.RequestDataType;
                                    commandInfo.ResponseDataType =
                                        knownCommand.ResponseDataType;
                                    commandInfo.IsCurrentPositionReturned =
                                        knownCommand.IsCurrentPositionReturned;
                                }
                                partCount--;
                            }
                            asciiCommands[commandName] = commandInfo;
                        }
                    }

                    List<CommandInfo> newCommands = new List<CommandInfo>();
                    newCommands.AddRange(device.DeviceType.Commands.Where(c => c.TextCommand == null));  // preserve Binary commands
                    newCommands.AddRange(asciiCommands.Values);

                    device.DeviceType.Commands = newCommands;
                }
            }
        }

        private static bool IsUnsupportedSetting(DeviceMessage response)
        {
            return
                response != null &&
                response.Command == Command.Error &&
                response.Data == (int)ZaberError.SettingInvalid;
        }

        private static bool IsValidByteValue(int intValue)
        {
            return byte.MinValue <= intValue && intValue <= byte.MaxValue;
        }

        /// <summary>
        /// Query connected devices using the ASCII protocol.
        /// </summary>
        /// <param name="port">Serial port to use.</param>
        /// <param name="queryTimeoutTimer">Used to wait for responses.</param>
        /// <exception cref="ZaberPortErrorException">An error occurred while
        /// querying devices.</exception>
        /// <exception cref="LoopbackException">A loopback connection was
        /// detected.</exception>
        private void QueryAsciiDevices(IZaberPort port, TimeoutTimer queryTimeoutTimer)
        {
            DeviceType defaultDeviceType;
            SortedDictionary<byte, ZaberDevice> devices;
            SortedDictionary<byte, Conversation> conversations;

            port.Send("/get deviceid");

            _log.Debug("Waiting for device id responses.");
            queryTimeoutTimer.Sleep();
            _log.Debug("Finished waiting for device id responses.");

            CreateConversations(
                port,
                out defaultDeviceType,
                out devices,
                out conversations);

            lock (_locker)
            {
                _deviceIds = null;
            }

            GetTextCommands(conversations, queryTimeoutTimer);

            _log.Debug("Requesting firmware versions.");
            var versionResponses = EnumerateRequests(
                conversations.Values,
                0,
                0,
                "get version");
            foreach (var response in versionResponses)
            {
                if (response != null)
                {
                    var device = devices[response.DeviceNumber];
                    var versionPieces = response.TextData.Split('.');
                    var versionText = versionPieces[0] + versionPieces[1];
                    device.FirmwareVersion = Convert.ToInt32(
                        versionText,
                        CultureInfo.InvariantCulture);
                }
            }

            _log.Debug("Requesting axis count.");
            var axesResponses = EnumerateRequests(
                conversations.Values,
                0,
                0,
                "get system.axiscount");
            foreach (var response in axesResponses)
            {
                if (response != null)
                {
                    var conversation = conversations[response.DeviceNumber];
                    var device = conversation.Device;
                    var deviceType = new DeviceType(device.DeviceType);

                    List<CommandInfo> axisCommands = new List<CommandInfo>();
                    axisCommands.AddRange(device.DeviceType.Commands.Where(c => c.TextCommand == null)); // preserve Binary commands
                    axisCommands.AddRange(deviceType.Commands.Where(c => c.IsAxisCommand));
                    deviceType.Commands = axisCommands;

                    for (int i = 0; i < response.Data; i++)
                    {
                        var axisDevice = new ZaberDevice();
                        axisDevice.DeviceNumber = device.DeviceNumber;
                        axisDevice.AxisNumber = i + 1;
                        axisDevice.DeviceType = deviceType;
                        axisDevice.FirmwareVersion = device.FirmwareVersion;
                        axisDevice.Port = device.Port;
                        device.AddAxis(axisDevice);

                        var axisConversation = new Conversation(axisDevice);
                        conversation.AddAxis(axisConversation);
                    }
                }
            }

            _log.Debug("Requesting microstep resolution.");
            var resolutionResponses = EnumerateRequests(
                conversations.Values,
                0,
                0,
                "get resolution");
            foreach (var response in resolutionResponses)
            {
                if (response != null)
                {
                    var device = devices[response.DeviceNumber];
                    device.MicrostepResolution = 0;
                    for (int i = 0; i < response.DataValues.Count; i++)
                    {
                        device.Axes[i].MicrostepResolution =
                            response.DataValues[i];
                    }
                }
            }

            var peripheralResponses = EnumerateRequests(
                conversations.Values,
                0,
                0,
                "get peripheralid");
            foreach (var response in peripheralResponses)
            {
                if (response != null)
                {
                    var device = devices[response.DeviceNumber];
                    for (int i = 0; i < response.DataValues.Count; i++)
                    {
                        SetPeripheralDeviceType(
                            device.Axes[i],
                            response.DataValues[i]);
                    }
                }
            }

            lock (_locker)
            {
                _currentState = ZaberPortFacadeState.Open;
                _areMessageIdsMonitored = true;
                var allDevices = (DeviceCollection)_devices[0];
                foreach (var device in devices.Values)
                {
                    allDevices.Add(device);
                }
                devices[0] = allDevices;
                var allConversations = (ConversationCollection)_conversations[0];
                foreach (var conversation in conversations.Values)
                {
                    allConversations.Add(conversation);
                }
                _selectedConversation = conversations[0] = allConversations;
                _devices = devices;
                _conversations = conversations;
            }
        }

        /// <summary>
        /// Query connected devices using the binary protocol.
        /// </summary>
        /// <param name="port">Serial port to use.</param>
        /// <param name="queryTimeoutTimer">Used to wait for responses.</param>
        /// <exception cref="ZaberPortErrorException">An error occurred while
        /// querying devices.</exception>
        /// <exception cref="LoopbackException">A loopback connection was
        /// detected.</exception>
        private void QueryBinaryDevices(IZaberPort port, TimeoutTimer queryTimeoutTimer)
        {
            DeviceType defaultDeviceType;
            SortedDictionary<byte, ZaberDevice> devices;
            SortedDictionary<byte, Conversation> conversations;

            port.Send(0, Command.ReturnDeviceId);

            _log.Debug("Waiting for device id responses.");
            queryTimeoutTimer.Sleep();
            _log.Debug("Finished waiting for device id responses.");

            CreateConversations(
                port,
                out defaultDeviceType,
                out devices,
                out conversations);

            _log.Debug("Requesting alias numbers.");
            GetAliases(devices, conversations, defaultDeviceType);

            _log.Debug("Requesting firmware versions.");
            EnumerateRequests(
                conversations.Values,
                Command.ReturnFirmwareVersion,
                0,
                null).Count();

            _log.Debug("Requesting microstep resolution.");
            EnumerateRequests(
                conversations.Values,
                Command.ReturnSetting,
                (int)Command.SetMicrostepResolution,
                null).Count();

            var controllerConversations = new List<Conversation>();
            foreach (var conversation in conversations.Values)
            {
                var deviceType = conversation.Device.DeviceType;
                var peripheralCommandInfo =
                    deviceType.GetCommandByNumber(Command.SetPeripheralId);
                if (peripheralCommandInfo != null &&
                    conversation.Device.FirmwareVersion >=
                    peripheralCommandInfo.FirmwareVersionAdded)
                {
                    controllerConversations.Add(conversation);
                }
            }

            var peripheralResponses = EnumerateRequests(
                controllerConversations,
                Command.ReturnSetting,
                (int)Command.SetPeripheralId,
                null);
            foreach (var response in peripheralResponses)
            {
                if (response != null &&
                    response.Command == Command.SetPeripheralId)
                {
                    var device = devices[response.DeviceNumber];
                    SetPeripheralDeviceType(device, response.Data);
                }
            }

            _log.Debug("Requesting device modes.");
            bool areMessageIdsEnabled = GetDeviceModes(conversations);

            lock (_locker)
            {
                _currentState = ZaberPortFacadeState.Open;
                _deviceIds = null;
                _areMessageIdsEnabled = areMessageIdsEnabled;
                _areMessageIdsMonitored = true;
                var allDevices = (DeviceCollection)_devices[0];
                allDevices.AreMessageIdsEnabled = _areMessageIdsEnabled;
                foreach (var device in devices.Values)
                {
                    allDevices.Add(device);
                }
                devices[0] = allDevices;
                var allConversations = (ConversationCollection)_conversations[0];
                foreach (var conversation in conversations.Values)
                {
                    allConversations.Add(conversation);
                }
                _selectedConversation = conversations[0] = allConversations;
                _devices = devices;
                _conversations = conversations;
            }
        }

        /// <summary>
        /// Update a device's DeviceType based on a peripheral id.
        /// </summary>
        /// <param name="device">The device to update</param>
        /// <param name="peripheralId">The peripheral to use</param>
        private static void SetPeripheralDeviceType(ZaberDevice device, int peripheralId)
        {
            var controllerType = device.DeviceType;
            var peripheralType = controllerType.GetPeripheralById(
                peripheralId);
            if (peripheralType != null)
            {
                device.DeviceType = peripheralType;
            }
            else
            {
                // Unknown peripheral. Clone the controller and set
                // the peripheral id.
                device.DeviceType = new DeviceType(controllerType);
                device.DeviceType.PeripheralId = peripheralId;
                device.DeviceType.Name = String.Format(
                    CultureInfo.CurrentCulture,
                    "{0} + PeripheralId {1}",
                    controllerType.Name,
                    peripheralId);
            }
        }

        private bool ShouldInvalidate(DataPacketEventArgs e)
        {
            lock (_locker)
            {
                // Invalidation in ASCII is done through the "_shouldInvalidateNextResponse" flag
                // and some regex pattern-matching to make sure each device responds.
                if (_shouldInvalidateNextResponse)
                {
                    foreach (var pattern in _InvalidateNextResponsePatterns)
                    {
                        if (Regex.IsMatch(e.DataPacket.Text, @pattern))
                        {
                            _InvalidateNextResponsePatterns.Remove(pattern);
                            break;
                        }
                    }
                    if (_InvalidateNextResponsePatterns.Count == 0)
                    {
                        _shouldWaitBeforeReopening = true;
                        _shouldInvalidateNextResponse = false;
                        return true;
                    }
                }

                // No device exists with the number in the data packet we just received.
                // This happens when a "renumber" command has been sent out, 
                // and the device is replying with its new number instead of its old.
                if (!_devices.ContainsKey(e.DataPacket.DeviceNumber))
                {
                    // The device numbers have changed: invalidation patterns are incorrect. Trash them.
                    _InvalidateNextResponsePatterns.Clear();
                    _shouldWaitBeforeReopening = true;
                    return true;
                }

                // From here on down all has to do with Binary replies.
                ZaberDevice device = _devices[e.DataPacket.DeviceNumber];
                if (e.DataPacket.Command == Command.Renumber
                    || e.DataPacket.Command == Command.RestoreSettings
                    || e.DataPacket.Command == Command.ConvertToAscii
                    || (e.DataPacket.Command == Command.SetPeripheralId 
                        && device.DeviceType.PeripheralId != e.DataPacket.Data)
                    )
                {
                    _shouldWaitBeforeReopening = true;
                    return true;
                }

                // The rest of these checks are for alias commands.
                if (e.DataPacket.Command != Command.SetAliasNumber)
                {
                    // not an alias command!
                    return false;
                }
                if (!_devices.ContainsKey(e.DataPacket.DeviceNumber))
                {
                    // new device, must be new alias, too
                    return true;
                }
                // have to do this in case there's a message id in there.
                int aliasNumberAsInt =
                    _devices[0].CreateDeviceMessage(e.DataPacket).Data;
                if (!IsValidByteValue(aliasNumberAsInt))
                {
                    // invalid alias number, ignore it.
                    return false;
                }
                byte aliasNumber = (byte)aliasNumberAsInt;
                if (!_devices.ContainsKey(aliasNumber))
                {
                    // don't have that alias in the collection, must be new
                    return true;
                }
                DeviceCollection alias = _devices[aliasNumber] as DeviceCollection;

                if (alias == null)
                {
                    // there's a single device in that slot already. Ignore the alias.
                    return false;
                }

                if (aliasNumber != 0)
                {
                    // it's only a new alias if the alias collection doesn't already 
                    // have it.
                    return !alias.Contains(device);
                }
                else
                {
                    // new alias is 0 (all devices) so we have to
                    // check if the old alias is still out there
                    foreach (ZaberDevice otherDevice in _devices.Values)
                    {
                        if (otherDevice == alias || otherDevice == device)
                        {
                            continue;
                        }
                        DeviceCollection oldAlias = otherDevice as DeviceCollection;
                        if (oldAlias != null && oldAlias.Contains(device))
                        {
                            // There is another alias that contains this device.
                            // We need to reset.
                            return true;
                        }
                    }
                    // didn't find another alias that contains this device.
                    // We don't need to reset.
                    return false;
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a list of all conversations for all the alias 
        /// numbers, devices and individual axes.
        /// </summary>
        public IEnumerable<Conversation> AllConversations
        {
            get
            {
                foreach (var conversation in _conversations.Values)
                {
                    yield return conversation;
                    foreach (var axis in conversation.Axes)
                    {
                        yield return axis;
                    }
                }
            }
        }

        /// <summary>
        /// Get or set a flag that will raise a port error for an invalid 
        /// packet if a response packet has an unknown device number.
        /// </summary>
        public bool AreDeviceNumbersValidated { get; set; }

        /// <summary>
        /// Enables or disables message ids mode on all devices attached 
        /// to the port.
        /// </summary>
        /// <remarks>
        /// <para>Message ids mode uses id numbers to match each response to 
        /// the request that triggered it. Message ids are also known as
        /// logical channels.</para>
        /// <para>The Zaber port facade forces you to keep the message ids
        /// mode the same on all devices attached to the port, either all on
        /// or all off. This avoids major confusion after a renumber request.
        /// Setting this property will trigger a "Return Setting" command
        /// for the device mode. The Zaber port facade will automatically send 
        /// "Set Device Mode" commands to switch any devices that don't match
        /// the requested setting. It will continue to do this whenever it
        /// sees a "Set Device Mode" response with the wrong setting of
        /// message ids mode.</para>
        /// </remarks>
        public bool AreMessageIdsEnabled
        {
            get
            {
                lock (_locker)
                {
                    return _areMessageIdsEnabled;
                }
            }
            set
            {
                var conversations = new List<Conversation>();
                lock (_locker)
                {
                    _areMessageIdsMonitored = false;
                    conversations.AddRange(_conversations.Values);
                    foreach (var conversation in conversations)
                    {
                        conversation.Device.AreMessageIdsEnabled = false;
                    }
                }

                foreach (var conversation in conversations)
                {
                    if (conversation.Device.IsSingleDevice)
                    {
                        var topic1 = conversation.StartTopic();
                        conversation.Device.Send(
                            Command.ReturnSetting,
                            (int)Command.SetDeviceMode,
                            topic1.MessageId);
                        if (!topic1.Wait(QueryTimeoutTimer))
                        {
                            throw new RequestTimeoutException();
                        }
                        topic1.Validate();
                        var oldMode = (DeviceModes)topic1.Response.Data;
                        var newMode = oldMode | DeviceModes.EnableMessageIdsMode;
                        if (!value)
                        {
                            newMode ^= DeviceModes.EnableMessageIdsMode;
                        }
                        if (newMode != oldMode)
                        {
                            var topic2 = conversation.StartTopic();
                            conversation.Device.Send(
                                Command.SetDeviceMode,
                                (int)newMode,
                                topic2.MessageId);
                            if (!topic2.Wait(QueryTimeoutTimer))
                            {
                                throw new RequestTimeoutException();
                            }
                            topic2.Validate();
                        }
                    }
                }

                lock (_locker)
                {
                    foreach (var conversation in conversations)
                    {
                        conversation.Device.AreMessageIdsEnabled = value;
                    }
                    _areMessageIdsEnabled = value;
                    _areMessageIdsMonitored = true;
                }
            }
        }

        /// <summary>
        /// A collection of conversations for all the devices attached to the port.
        /// </summary>
        public ICollection<Conversation> Conversations
        {
            get
            {
                lock (_locker)
                {
                    var copy = new List<Conversation>();
                    copy.AddRange(_conversations.Values);
                    return copy;
                }
            }
        }

        /// <summary>
        /// Gets the current state of the facade.
        /// </summary>
        public ZaberPortFacadeState CurrentState
        {
            get
            {
                lock (_locker)
                {
                    return _currentState;
                }
            }
        }

        /// <summary>
        /// A device type that will be used for any device ids not found in 
        /// <see cref="DeviceTypes"/>. Effectively, this is just used to 
        /// specify a generic set of commands and settings.
        /// </summary>
        public DeviceType DefaultDeviceType
        {
            get
            {
                lock (_locker)
                {
                    return _defaultDeviceType;
                }
            }
            set
            {
                lock (_locker)
                {
                    _defaultDeviceType = value;

                    _devices[0].DeviceType = new DeviceType(_defaultDeviceType);
                    _devices[0].DeviceType.Name = "All Devices";
                }
            }
        }

        /// <summary>
        /// A collection of all the devices attached to the port.
        /// </summary>
        public ICollection<ZaberDevice> Devices
        {
            get
            {
                lock (_locker)
                {
                    var copy = new List<ZaberDevice>();
                    copy.AddRange(_devices.Values);
                    return copy;
                }
            }
        }

        /// <summary>
        /// A list of the known device types.
        /// </summary>
        [SuppressMessage("Microsoft.Usage",
            "CA2227:CollectionPropertiesShouldBeReadOnly",
            Justification = "Needed for Spring configuration")]
        public IList<DeviceType> DeviceTypes
        {
            get
            {
                lock (_locker)
                {
                    // already made read-only during set
                    return _deviceTypes;
                }
            }
            set
            {
                lock (_locker)
                {
                    // use read-only copy to avoid insertions
                    _deviceTypes = new List<DeviceType>(value).AsReadOnly();

                    _deviceTypeMap = new Dictionary<int, DeviceType>();
                    foreach (DeviceType deviceType in _deviceTypes)
                    {
                        _deviceTypeMap.Add(deviceType.DeviceId, deviceType);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a flag that shows whether two devices responded with the
        /// same device number.
        /// </summary>
        /// <remarks>
        /// If this is true, the client should send <see cref="Command.Renumber"/>
        /// to device number 0 to force all devices to choose unique device 
        /// numbers.
        /// </remarks>
        public bool IsCollisionDetected { get; private set; }

        /// <summary>
        /// Gets or sets a flag that enables or disables the 
        /// <see cref="Invalidated"/> event. Enabled by default.
        /// </summary>
        public bool IsInvalidateEnabled { get; set; }

        /// <summary>
        /// Gets or sets the port that this facade is attached to.
        /// </summary>
        public IZaberPort Port
        {
            get
            {
                lock (_locker)
                {
                    return _port;
                }
            }
            set
            {
                lock (_locker)
                {
                    if (_port != null)
                    {
                        _port.DataPacketReceived -=
                            new EventHandler<DataPacketEventArgs>(
                                port_DataPacketReceived);
                        _port.DataPacketSent -=
                            new EventHandler<DataPacketEventArgs>(
                                port_DataPacketSent);
                        _port.ErrorReceived -=
                            new EventHandler<ZaberPortErrorReceivedEventArgs>(
                                port_ErrorReceived);
                    }

                    _port = value;

                    if (_port != null)
                    {
                        _port.DataPacketReceived +=
                            new EventHandler<DataPacketEventArgs>(
                                port_DataPacketReceived);
                        _port.DataPacketSent +=
                            new EventHandler<DataPacketEventArgs>(
                                port_DataPacketSent);
                        _port.ErrorReceived +=
                            new EventHandler<ZaberPortErrorReceivedEventArgs>(
                                port_ErrorReceived);
                    }

                    _devices[0].Port = _port;
                }
            }
        }

        /// <summary>
        /// Gets or sets the time to wait in milliseconds for responses from 
        /// all devices.
        /// </summary>
        public int QueryTimeout
        {
            get
            {
                lock (_locker)
                {
                    return _queryTimeoutTimer.Timeout;
                }
            }
            set
            {
                lock (_locker)
                {
                    _queryTimeoutTimer.Timeout = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a timeout timer used to time how long we wait for
        /// responses from all devices.
        /// </summary>
        public TimeoutTimer QueryTimeoutTimer
        {
            get
            {
                lock (_locker)
                {
                    return _queryTimeoutTimer;
                }
            }
            set
            {
                lock (_locker)
                {
                    _queryTimeoutTimer = value;
                }
            }
        }

        /// <summary>
        /// Get or set the currently selected conversation.
        /// </summary>
        /// <remarks>
        /// The meaning of this property is entirely up to the client program.
        /// However, whenever this property changes, it will raise the
        /// <see cref="SelectedConversationChanged"/> event. Every time the
        /// port facade is opened or closed, this is set to the conversation 
        /// for device number 0, or all devices.
        /// </remarks>
        public Conversation SelectedConversation
        {
            get
            {
                lock (_locker)
                {
                    return _selectedConversation;
                }
            }
            set
            {
                bool isChanged;
                lock (_locker)
                {
                    isChanged = value != _selectedConversation;
                    if (isChanged)
                    {
                        _selectedConversation = value;
                    }
                }
                if (isChanged && SelectedConversationChanged != null)
                {
                    SelectedConversationChanged(this, EventArgs.Empty);
                }
            }
        }

        #endregion

        #region Private Properties

        private static ILog _log = LogManager.GetLogger(typeof(ZaberPortFacade));

        private object _locker = new object();
        private SortedDictionary<byte, ZaberDevice> _devices = new SortedDictionary<byte, ZaberDevice>();
        private SortedDictionary<byte, Conversation> _conversations = new SortedDictionary<byte, Conversation>();
        private IZaberPort _port;
        private DeviceType _defaultDeviceType;
        private IList<DeviceType> _deviceTypes;
        private IDictionary<int, DeviceType> _deviceTypeMap;
        private bool _areMessageIdsMonitored = false; // invalidate if device mode is wrong
        private bool _areMessageIdsEnabled = false;
        private TimeoutTimer _queryTimeoutTimer = new TimeoutTimer();
        private ZaberPortError _errorDuringQuery;
        private bool _isVoltageBadDuringQuery;
        private bool _shouldInvalidateNextResponse = false;
        private IList<String> _InvalidateNextResponsePatterns = new List<String>();
        private bool _isInvalidated = false;
        private bool _shouldWaitBeforeReopening = false;
        private ZaberPortFacadeState _currentState = ZaberPortFacadeState.Closed;
        private Dictionary<byte, int> _deviceIds; // only during query state
        private Conversation _selectedConversation;

        #endregion

        #region Events

        /// <summary>
        /// Raised when the current conversation has changed.
        /// </summary>
        /// <remarks>Get the <see cref="Conversation"/> property
        /// to see what the new one is.</remarks>
        public event EventHandler SelectedConversationChanged;

        /// <summary>
        /// Raised when the port facade has finished opening.
        /// </summary>
        public event EventHandler Opened;

        /// <summary>
        /// Raised when the port facade is closed.
        /// </summary>
        public event EventHandler Closed;

        /// <summary>
        /// Raised when the port facade is invalidated and should be reopened.
        /// </summary>
        /// <remarks>The port facade is invalidated when the list of connected
        /// devices may have become inaccurate. This happens after a response 
        /// to the Renumber command, the Convert to ASCII/Binary command,
        /// the RestoreSettings command, or the SetAlias command. 
        /// If client applications want to be sure the device list is
        /// accurate, they should call <see cref="Close"/> and then
        /// <see cref="Open"/> to reopen the port and requery the list of 
        /// connected devices. The event is only raised the first time the
        /// facade is invalidated, so if two renumber commands are sent without
        /// reopening the port facade in between, only the first one will raise
        /// this event.
        /// </remarks>
        public event EventHandler Invalidated;

        #endregion

        #region Event Handlers

        void port_DataPacketSent(object sender, DataPacketEventArgs e)
        {
            lock (_locker)
            {
                if (_port.IsAsciiMode && _currentState != ZaberPortFacadeState.QueryingDevices)
                {
                    var text = e.DataPacket.Text;
                    if (text != null
                        && Regex.IsMatch(text, @"renumber|system\s+restore|system\s+factoryreset|set\s+deviceid|set\s+peripheralid|set\s+system.access|tools\s+setcomm")
                        && !_isInvalidated)
                    {
                        _shouldInvalidateNextResponse = true;

                        if (e.DataPacket.DeviceNumber == 0)
                        {
                            foreach (var device in this.Devices)
                            {
                                if (device.DeviceNumber != 0) // Exclude the 0 device
                                {
                                    _InvalidateNextResponsePatterns.Add("@"
                                    + (device.DeviceNumber.ToString("00", CultureInfo.InvariantCulture)
                                    + " " + device.AxisNumber.ToString(CultureInfo.InvariantCulture))
                                    + " OK (IDLE|BUSY) (--|[A-Z][A-Z]) 0");
                                }
                            }
                        }
                        else
                        {
                            _InvalidateNextResponsePatterns.Add("@"
                                + (e.DataPacket.DeviceNumber.ToString("00", CultureInfo.InvariantCulture) + " "
                                + e.DataPacket.AxisNumber.ToString(CultureInfo.InvariantCulture))
                                + " OK (IDLE|BUSY) (--|[A-Z][A-Z]) 0");
                        }
                    }
                }
            }
        }

        private void port_DataPacketReceived(object sender, DataPacketEventArgs e)
        {
            _log.Debug("Receiving.");
            bool shouldInvalidate = false;
            lock (_locker)
            {
                _log.Debug("Receiving..");
                if (_currentState == ZaberPortFacadeState.Closed)
                {
                    _log.Debug("Closed.");
                    return;
                }
                if (_currentState == ZaberPortFacadeState.QueryingDevices)
                {
                    bool isDeviceId =
                        _deviceIds != null &&
                        (e.DataPacket.Command == Command.ReturnDeviceId ||
                        e.DataPacket.MessageType == MessageType.Response);
                    if (isDeviceId)
                    {
                        byte deviceNumber = e.DataPacket.DeviceNumber;
                        if (!_deviceIds.ContainsKey(deviceNumber))
                        {
                            _deviceIds[e.DataPacket.DeviceNumber] =
                                e.DataPacket.Data;
                        }
                        else
                        {
                            IsCollisionDetected = true;
                        }
                    }
                    else if (e.DataPacket.Command == Command.Error &&
                        (e.DataPacket.Data == (int)ZaberError.VoltageLow ||
                        e.DataPacket.Data == (int)ZaberError.VoltageHigh))
                    {
                        _isVoltageBadDuringQuery = true;
                    }
                }
                else if (AreDeviceNumbersValidated &&
                    !ContainsDevice(e.DataPacket.DeviceNumber))
                {
                    Port.ReportInvalidPacket();
                }
                else
                {
                    if (e.DataPacket.Command == Command.SetDeviceMode)
                    {
                        CheckMessageIdsMode(e.DataPacket);
                        return;
                    }
                    else if (ShouldInvalidate(e) && !_isInvalidated)
                    {
                        _isInvalidated = true;
                        shouldInvalidate = true;
                    }
                }
            }
            _log.Debug("Received.");

            if (IsInvalidateEnabled && shouldInvalidate && Invalidated != null)
            {
                Invalidated(this, EventArgs.Empty);
            }
        }

        void port_ErrorReceived(object sender, ZaberPortErrorReceivedEventArgs e)
        {
            lock (_locker)
            {
                if (_currentState == ZaberPortFacadeState.QueryingDevices)
                {
                    _errorDuringQuery = e.ErrorType;
                }
            }
        }
        #endregion

        #region IZaberPort delegation methods

        /// <summary>
        /// Get a list of all available ports.
        /// </summary>
        /// <returns>An array of port names, one of which should be passed to 
        /// <see cref="Open"/>.</returns>
        public string[] GetPortNames()
        {
            IZaberPort port;
            lock (_locker)
            {
                port = _port;
            }
            return port.GetPortNames();
        }

        /// <summary>
        /// Close and release the serial port. This must be called to avoid
        /// locking the port when you are finished with it.
        /// </summary>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Close()
        {
            TimeoutTimer queryTimeoutTimer;
            bool shouldWait;

            lock (_locker)
            {
                _currentState = ZaberPortFacadeState.Closed;
                shouldWait = _shouldWaitBeforeReopening;
                _shouldWaitBeforeReopening = false;
                queryTimeoutTimer = _queryTimeoutTimer;
            }

            if (shouldWait)
            {
                _log.Debug("Waiting for devices to finish executing commands.");
                queryTimeoutTimer.Sleep();
            }
            
            IZaberPort port;
            _log.Debug("Closing.");
            lock (_locker)
            {
                _log.Debug("Closing..");
                DeviceCollection allDevices = GetDeviceCollection(0);
                ConversationCollection allConversations = GetConversationCollection(0);
                foreach (var conversation in allConversations)
                {
                    conversation.Device.Port = null;
                    if (conversation.Device.DeviceNumber != 0)
                    {
                        conversation.Dispose();
                    }
                }
                allDevices.Clear();
                _devices.Clear();
                _devices[0] = allDevices;

                _conversations.Clear();
                allConversations.Clear();
                _selectedConversation = _conversations[0] = allConversations;

                allDevices.AreMessageIdsEnabled = false;
                _areMessageIdsEnabled = false;
                _areMessageIdsMonitored = false;
                port = _port;
            }
            _log.Debug("Closing...");
            try
            {
                port.Close();
                _log.Debug("Closed.");
            }
            finally
            {
                if (Closed != null)
                {
                    Closed(this, EventArgs.Empty);
                }
                _log.Debug("Closed..");
            }
        }

        /// <summary>
        /// Is the port open?
        /// </summary>
        /// <remarks>This doesn't always agree with <see cref="CurrentState"/>.
        /// For example, after a USB-to-serial adapter has been disconnected.</remarks>
        public bool IsOpen
        {
            get 
            {
                IZaberPort port;
                lock (_locker)
                {
                    port = _port;
                }
                return port.IsOpen;
            }
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                _port.Dispose();
                foreach (var conversation in _conversations.Values)
                {
                    conversation.Dispose();
                }
            }
        }

        #endregion
    }
}
