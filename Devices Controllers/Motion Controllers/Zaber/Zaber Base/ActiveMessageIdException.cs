using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
    /// <summary>
    /// This exception is thrown by a <see cref="Conversation"/> when
    /// the requested message id is already being used by an active topic.
    /// </summary>
    [Serializable]
    public class ActiveMessageIdException : ConversationException
    {
        private const string DEFAULT_MESSAGE = 
            "Message id is already active.";

        public ActiveMessageIdException()
            : base(DEFAULT_MESSAGE)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public ActiveMessageIdException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this one</param>
        public ActiveMessageIdException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected ActiveMessageIdException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Sets the System.Runtime.Serialization.SerializationInfo
        ///     with information about the exception.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info, 
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);
        }
    }
}
