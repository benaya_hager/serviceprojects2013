﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Describes a unit of measure used for converting measurements.
    /// </summary>
    public class UnitOfMeasure : IComparable<UnitOfMeasure>
    {
        /// <summary>
        /// Raw data value for any command.
        /// </summary>
        public static readonly UnitOfMeasure Data = new UnitOfMeasure("-- data --");

        // ---------- Acceleration ----------
        /// <summary>
        /// Measures acceleration in microsteps per second squared
        /// </summary>
        public static readonly UnitOfMeasure MicrostepsPerSecondSquared = 
            new UnitOfMeasure("µstep/s²");
        /// <summary>
        /// Measures acceleration in µm/s²
        /// </summary>
        public static readonly UnitOfMeasure MicrometersPerSecondSquared = 
            new UnitOfMeasure("µm/s²");
        /// <summary>
        /// Measures acceleration in mm/s²
        /// </summary>
        public static readonly UnitOfMeasure MillimetersPerSecondSquared = 
            new UnitOfMeasure("mm/s²");
        /// <summary>
        /// Measures acceleration in inches per second squared
        /// </summary>
        public static readonly UnitOfMeasure InchesPerSecondSquared = 
            new UnitOfMeasure("in/s²");
        /// <summary>
        /// Measures rotary acceleration in degrees per second squared
        /// </summary>
        public static readonly UnitOfMeasure DegreesPerSecondSquared =
            new UnitOfMeasure("°/s²");
        /// <summary>
        /// Measures rotary acceleration in radians per second squared
        /// </summary>
        public static readonly UnitOfMeasure RadiansPerSecondSquared =
            new UnitOfMeasure("rad/s²");

        // ---------- Velocity ----------
        /// <summary>
        /// Measures velocity in microsteps per second
        /// </summary>
        public static readonly UnitOfMeasure MicrostepsPerSecond = 
            new UnitOfMeasure("µstep/s", MicrostepsPerSecondSquared);
        /// <summary>
        /// Measures velocity in µm/s
        /// </summary>
        public static readonly UnitOfMeasure MicrometersPerSecond = 
            new UnitOfMeasure("µm/s", MicrometersPerSecondSquared);
        /// <summary>
        /// Measures velocity in mm/s
        /// </summary>
        public static readonly UnitOfMeasure MillimetersPerSecond = 
            new UnitOfMeasure("mm/s", MillimetersPerSecondSquared);
        /// <summary>
        /// Measures velocity in inches per second
        /// </summary>
        public static readonly UnitOfMeasure InchesPerSecond =
            new UnitOfMeasure("in/s", InchesPerSecondSquared);
        /// <summary>
        /// Measures rotary velocity in degrees per second
        /// </summary>
        public static readonly UnitOfMeasure DegreesPerSecond =
            new UnitOfMeasure("°/s", DegreesPerSecondSquared);
        /// <summary>
        /// Measures rotary velocity in radians per second
        /// </summary>
        public static readonly UnitOfMeasure RadiansPerSecond =
            new UnitOfMeasure("rad/s", RadiansPerSecondSquared);
        /// <summary>
        /// Measures rotary velocity in revolutions per minute
        /// </summary>
        public static readonly UnitOfMeasure RevolutionsPerMinute = 
            new UnitOfMeasure("RPM");
        
        // -------- Position -----------
        /// <summary>
        /// Measures position in micrometers
        /// </summary>
        public static readonly UnitOfMeasure Micrometer = 
            new UnitOfMeasure("µm", MicrometersPerSecond);
        /// <summary>
        /// Measures position in millimeters
        /// </summary>
        public static readonly UnitOfMeasure Millimeter = 
            new UnitOfMeasure("mm", MillimetersPerSecond);
        /// <summary>
        /// Measures position in inches
        /// </summary>
        public static readonly UnitOfMeasure Inch = 
            new UnitOfMeasure("in", InchesPerSecond);
        /// <summary>
        /// Measures rotary position in degrees
        /// </summary>
        public static readonly UnitOfMeasure Degree =
            new UnitOfMeasure("°", DegreesPerSecond);
        /// <summary>
        /// Measures rotary position in radians
        /// </summary>
        public static readonly UnitOfMeasure Radian =
            new UnitOfMeasure("rad", RadiansPerSecond);
        /// <summary>
        /// Measures rotary position in revolutions
        /// </summary>
        public static readonly UnitOfMeasure Revolution = 
            new UnitOfMeasure("rev");

        /// <summary>
        /// Initialize a new unit of measure object.
        /// </summary>
        /// <param name="abbreviation">Describes the unit of measure.</param>
        public UnitOfMeasure(string abbreviation)
        {
            Abbreviation = abbreviation;
        }

        /// <summary>
        /// Initialize a new unit of measure object.
        /// </summary>
        /// <param name="abbreviation">Describes the unit of measure.</param>
        /// <param name="derivative">The derivative for this unit of measure.
        /// </param>
        public UnitOfMeasure(string abbreviation, UnitOfMeasure derivative)
        {
            Abbreviation = abbreviation;
            Derivative = derivative;
        }

        /// <summary>
        /// Get an abbreviation describing the unit of measure.
        /// </summary>
        public string Abbreviation { get; private set; }

        /// <summary>
        /// The related unit of measure in the next measurement type.
        /// </summary>
        /// <remarks>The derivative of position is velocity, and the 
        /// derivative of velocity is acceleration.</remarks>
        public UnitOfMeasure Derivative { get; private set; }

        /// <summary>
        /// Create a string representation of the unit of measure.
        /// </summary>
        /// <returns>The abbreviation for this unit of measure.</returns>
        public override string ToString()
        {
            return Abbreviation;
        }

        /// <summary>
        /// Determines whether this instance and another object have the same 
        /// value.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the other object has the same abbreviation.</returns>
        public override bool Equals(object obj)
        {
            var other = obj as UnitOfMeasure;
            return 
                other != null && 
                String.Equals(Abbreviation, other.Abbreviation);
        }

        /// <summary>
        /// Determines whether two units of measure have the same 
        /// values.
        /// </summary>
        /// <param name="first">One unit of measure to compare.</param>
        /// <param name="second">The other unit of measure.</param>
        /// <returns>True if the two objects have the same abbreviation.</returns>
        public static bool operator ==(UnitOfMeasure first, UnitOfMeasure second)
        {
            return Equals(first, second);
        }

        /// <summary>
        /// Determines whether two units of measure have different
        /// values.
        /// </summary>
        /// <param name="first">One unit of measure to compare.</param>
        /// <param name="second">The other unit of measure.</param>
        /// <returns>True if the two objects have different abbreviations.</returns>
        public static bool operator !=(UnitOfMeasure first, UnitOfMeasure second)
        {
            return !Equals(first, second);
        }

        /// <summary>
        /// Determines the order of two units of measure.
        /// </summary>
        /// <param name="first">One unit of measure to compare.</param>
        /// <param name="second">The other unit of measure.</param>
        /// <returns>True if the first objects precedes the other.</returns>
        public static bool operator <(UnitOfMeasure first, UnitOfMeasure second)
        {
            return
                (first == null && second != null) ||
                first.CompareTo(second) < 0;
        }

        /// <summary>
        /// Determines the order of two units of measure.
        /// </summary>
        /// <param name="first">One unit of measure to compare.</param>
        /// <param name="second">The other unit of measure.</param>
        /// <returns>True if the first objects follows the other.</returns>
        public static bool operator >(UnitOfMeasure first, UnitOfMeasure second)
        {
            return
                first != null &&
                first.CompareTo(second) > 0;
        }

        /// <summary>
        /// Returns the hash code for this object.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return Abbreviation.GetHashCode();
        }

        #region IComparable<UnitOfMeasure> Members

        /// <summary>
        /// Compares this instance with another UnitOfMeasure object and 
        /// indicates whether this instance precedes, follows, or appears in 
        /// the same position in the sort order as other object.
        /// </summary>
        /// <param name="other">The object to compare to.</param>
        /// <returns>A 32-bit signed integer that indicates whether this 
        /// instance precedes, follows, or appears in the same position in the 
        /// sort order as the value parameter. Less than zero if this instance 
        /// precedes other. Zero if this instance has the same position in the 
        /// sort order as other. Greater than zero if this instance follows 
        /// other or other is null.</returns>
        public int CompareTo(UnitOfMeasure other)
        {
            return 
                other == null
                ? 1
                : String.Compare(
                    Abbreviation, 
                    other.Abbreviation, 
                    StringComparison.Ordinal);
        }

        #endregion
    }
}
