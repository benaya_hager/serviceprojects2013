using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Zaber
{
    /// <summary>
    /// Coordinates a request with a response 
    /// in the communications with a single device.
    /// </summary>
    [Serializable]
    public class ConversationTopic : IDisposable
    {
        private DeviceMessage response;
        private ZaberPortError zaberPortError;
        private DeviceMessage replacementResponse;

        [NonSerialized]
        private ManualResetEvent waitHandle = new ManualResetEvent(false);

        /// <summary>
        /// Gets or sets the port error that was detected while waiting
        /// for a response.
        /// </summary>
        /// <remarks>Setting this completes the topic.</remarks>
        public virtual ZaberPortError ZaberPortError 
        {
            get { return zaberPortError; }
            set
            {
                zaberPortError = value;
                Complete();
            }
        }

        /// <summary>
        /// Gets or sets the details of a response when this topic's request has
        /// been replaced by another.
        /// </summary>
        /// <remarks>Setting this completes the topic.</remarks>
        public virtual DeviceMessage ReplacementResponse
        {
            get { return replacementResponse; }
            set
            {
                replacementResponse = value;
                Complete();
            }
        }

        /// <summary>
        /// Gets or sets the command that was sent to the device.
        /// </summary>
        public virtual Command RequestCommand { get; set; }

        /// <summary>
        /// Gets or sets the data value that was sent to the device.
        /// </summary>
        public virtual int RequestData { get; set; }

        /// <summary>
        /// Gets or sets the number of retries remaining.
        /// </summary>
        public virtual int RetryCount { get; set; }

        /// <summary>
        /// Gets or sets the details of the response received from the device.
        /// </summary>
        /// <remarks>Setting this completes the topic.</remarks>
        public virtual DeviceMessage Response
        {
            get { return response; }
            set
            {
                response = value;
                Complete();
            }
        }

        /// <summary>
        /// Gets or sets a data packet that was used to retry the request after
        /// a port error.
        /// </summary>
        /// <remarks>This is recorded so that the retry can be cancelled if the
        /// original response is received before the retry request gets sent.
        /// </remarks>
        public DataPacket RetryPacket { get; set; }

        /// <summary>
        /// Gets or sets a flag showing if this topic is waiting for an alert
        /// message instead of a regular response.
        /// </summary>
        public bool IsAlert { get; set; }

        /// <summary>
        /// Get a flag showing whether this topic was canceled by a call to
        /// <see cref="Cancel"/>.
        /// </summary>
        public virtual bool IsCanceled { get; private set; }

        /// <summary>
        /// Stop waiting for a response.
        /// </summary>
        /// <remarks>Sets IsCanceled to true and marks the topic complete.</remarks>
        public virtual void Cancel()
        {
            IsCanceled = true;
            Complete();
        }

        /// <summary>
        /// Get a WaitHandle that will be released when the topic is completed.
        /// </summary>
        /// <remarks>
        /// This is useful when you want to use more sophisticated thread coordination.
        /// You can use this WaitHandle in the WaitHandle class's WaitAll() or WaitAny()
        /// methods. If you just want to do a simple wait, use the <see cref="Wait()"/>
        /// or <see cref="Wait(TimeoutTimer)"/> methods.
        /// </remarks>
        public WaitHandle WaitHandle 
        {
            get { return waitHandle; }
        }

        /// <summary>
        /// Mark the topic as complete, raise the event, and set the wait handle.
        /// </summary>
        protected virtual void Complete()
        {
            IsComplete = true;
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
            waitHandle.Set();
        }

        /// <summary>
        /// Gets or sets the message identifier used to coordinate requests 
        /// and responses.
        /// </summary>
        public byte MessageId { get; set; }

        /// <summary>
        /// Blocks the current thread until the topic is completed.
        /// </summary>
        /// <returns>This method always returns true.</returns>
        /// <remarks>
        /// The topic can be completed by setting any of these properties:
        /// <see cref="Response"/>, <see cref="ZaberPortError"/>,
        /// <see cref="ReplacementResponse"/>.
        /// </remarks>
        public virtual bool Wait()
        {
            return waitHandle.WaitOne();
        }

        /// <summary>
        /// Blocks the current thread until the topic is completed or the
        /// timer times out.
        /// </summary>
        /// <param name="timeoutTimer">The timer to wait with.</param>
        /// <returns>True if the topic completed before the timer expired, 
        /// otherwise false.</returns>
        /// <remarks>
        /// The topic can be completed by setting any of these properties:
        /// <see cref="Response"/>, <see cref="ZaberPortError"/>,
        /// <see cref="ReplacementResponse"/>.
        /// </remarks>
        public virtual bool Wait(TimeoutTimer timeoutTimer)
        {
            return timeoutTimer.WaitOne(waitHandle);
        }

        /// <summary>
        /// Raised when the topic is completed.
        /// </summary>
        /// <remarks>
        /// The topic can be completed by setting any of these properties:
        /// <see cref="Response"/>, <see cref="ZaberPortError"/>,
        /// <see cref="ReplacementResponse"/>.
        /// </remarks>
        public event EventHandler Completed;

        /// <summary>
        /// Gets a flag showing whether the request has been completed.
        /// </summary>
        public bool IsComplete { get; private set; }

        /// <summary>
        /// Validates that the request has been completed successfully.
        /// </summary>
        /// <remarks>
        /// If you want to check whether the response is valid without throwing
        /// an exception, use <see cref="IsValid"/> instead.
        /// </remarks>
        /// <exception cref="InvalidOperationException">The request has not
        /// been completed yet.</exception>
        /// <exception cref="RequestReplacedException">The request was replaced
        /// by another request and will not complete.</exception>
        /// <exception cref="ZaberPortErrorException">A port error occurred, so
        /// the status of the request is unknown.</exception>
        /// <exception cref="ErrorResponseException">The device responded with
        /// an error.</exception>
        /// <exception cref="RequestCollectionException">Multiple requests were
        /// made together, and some of them failed.</exception>
        public virtual void Validate()
        {
            if (!IsComplete)
            {
                throw new InvalidOperationException(
                    "Validate cannot be called until the request is complete.");
            }
            if (ReplacementResponse != null)
            {
                throw new RequestReplacedException(ReplacementResponse);
            }
            if (ZaberPortError != ZaberPortError.None)
            {
                throw new ZaberPortErrorException(ZaberPortError);
            }
            if (IsCanceled)
            {
                throw new RequestCanceledException();
            }
            if (Response.IsError)
            {
                throw new ErrorResponseException(Response);
            }
        }

        /// <summary>
        /// Gets a flag showing whether the request has been completed successfully.
        /// </summary>
        /// <seealso cref="Validate"/>
        public virtual bool IsValid
        {
            get
            {
                return IsComplete &&
                    !IsCanceled && 
                    ReplacementResponse == null &&
                    ZaberPortError == ZaberPortError.None &&
                    Response != null &&
                    ! Response.IsError;
            }
        }

        /// <summary>
        /// Gets a flag showing whether the request was rejected for flow control reasons and should be retried.
        /// </summary>
        public virtual bool IsFlowControlRejected
        {
            get
            {
                return IsComplete &&
                    !IsCanceled &&
                    ReplacementResponse == null &&
                    ZaberPortError == ZaberPortError.None &&
                    Response != null &&
                    Response.IsAgain;
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                waitHandle.Close();
            }
        }

        #endregion
    }
}
