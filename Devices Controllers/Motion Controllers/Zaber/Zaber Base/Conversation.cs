using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using log4net;
using System.Globalization;
using System.Collections.ObjectModel;

namespace Zaber
{
    /// <summary>
    /// Converts from the asynchronous, event-driven model of the 
    /// <see cref="ZaberDevice"/> to a synchronous, request/response model.
    /// </summary>
    /// <remarks>
    /// When you call the <see cref="Request(Command, int)"/> method, the conversation will
    /// call <see cref="ZaberDevice.Send(Command, int, byte)"/> on its device, and then sleep until
    /// a response is returned. See the <see cref="Request(Command, int)"/> method notes for 
    /// details on support for message ids and pitfalls to avoid.
    /// </remarks>
    public class Conversation : IDisposable
    {
        private static ILog log = LogManager.GetLogger(typeof(Conversation));

        private ZaberDevice device;
        private int lastId = 0;
        private Dictionary<byte, ConversationTopic> topicMap = 
            new Dictionary<byte, ConversationTopic>();
        private LinkedList<ConversationTopic> topicList =
            new LinkedList<ConversationTopic>();
        private bool isRegisteredForPortErrors = false;
        private bool areUnexpectedResponsesInvalid = false;
        private ManualResetEvent idleAlertEvent = new ManualResetEvent(false);
        private List<Conversation> axes = new List<Conversation>();

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="device">See <see cref="Device"/></param>
        public Conversation(ZaberDevice device)
        {
            this.device = device;
            device.MessageReceived +=
                new EventHandler<DeviceMessageEventArgs>(device_MessageReceived);
            TimeoutTimer = new TimeoutTimer();
            TimeoutTimer.Timeout = System.Threading.Timeout.Infinite;
            AlertTimeoutTimer = new TimeoutTimer();
            AlertTimeoutTimer.Timeout = System.Threading.Timeout.Infinite;
            PollTimeoutTimer = new TimeoutTimer();
            PollTimeoutTimer.Timeout = 100;
        }

        /// <summary>
        /// The device that this conversation will send requests through.
        /// </summary>
        public ZaberDevice Device
        {
            get { return device; }
        }

        /// <summary>
        /// Gets or sets the number of times to retry a request after a port error.
        /// Defaults to 0.
        /// </summary>
        /// <remarks>
        /// By default, when the port raises its 
        /// <see cref="IZaberPort.ErrorReceived"/> event, any waiting 
        /// conversation topics throw a <see cref="ZaberPortErrorException"/>.
        /// However, you can set this property to make the conversation retry
        /// the request a few times before throwing the exception. Only commands
        /// that are safe will be retried. For example, Move Relative cannot
        /// be retried, it will always throw an exception if a port error 
        /// occurs.
        /// </remarks>
        public virtual int RetryCount { get; set; }

        /// <summary>
        /// Gets or sets a flag for whether unexpected responses should be
        /// reported as invalid.
        /// </summary>
        /// <exception cref="InvalidOperationException">when this conversation's
        /// device does not have message ids enabled</exception>
        /// <remarks>If this flag is true, then only responses with a known
        /// message id will be processed. Any responses with an unknown message
        /// id or no message id will trigger 
        /// <see cref="ZaberPortError.InvalidPacket"/>. The advantage to 
        /// setting this is that corrupted responses won't accidentally trigger 
        /// a <see cref="RequestReplacedException"/>. If you do set this,
        /// you should probably use the <see cref="Timeout"/> property
        /// to avoid having a thread that hangs forever.</remarks>
        /// <seealso cref="RetryCount"/>
        public virtual bool AreUnexpectedResponsesInvalid
        {
            get { return areUnexpectedResponsesInvalid; }
            set
            {
                if (value && ! Device.AreMessageIdsEnabled)
                {
                    throw new InvalidOperationException(
                        "AreUnexpectedResponsesInvalid can only be set when " +
                        "message ids are enabled.");
                }
                areUnexpectedResponsesInvalid = value;
            }
        }

        void Port_ErrorReceived(object sender, ZaberPortErrorReceivedEventArgs e)
        {
            lock (topicMap)
            {
                // Copy list because releasing the topic will remove it from topicList.
                List<ConversationTopic> topics = new List<ConversationTopic>();
                topics.AddRange(topicList);
                foreach (ConversationTopic topic in topics)
                {
                    if (topic.RetryCount > 0)
                    {
                        topic.RetryCount--;
                        var port = Device.Port;
                        if (topic.RetryPacket != null && port != null)
                        {
                            port.CancelDelayedPacket(topic.RetryPacket);
                        }
                        topic.RetryPacket = Device.SendDelayed(
                            topic.RequestCommand,
                            topic.RequestData,
                            topic.MessageId);
                    }
                    else
                    {
                        // also releases the topic and removes it
                        topic.ZaberPortError = e.ErrorType;
                    }
                }
            }
        }

        /// <summary>
        /// Send a command to a Zaber device that doesn't require a data value
        /// and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <returns>The response message</returns>
        /// <exception cref="RequestReplacedException">
        /// Another request has replaced this request.
        /// </exception>
        /// <exception cref="ErrorResponseException">
        /// The response was <see cref="Command.Error"/>.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">The request did not
        /// complete before the timeout expired.</exception>
        /// <remarks>
        /// See more details in the other override of <see cref="Request(Command, int)"/>.
        /// </remarks>
        public virtual DeviceMessage Request(Command command)
        {
            return Request(command, 0);
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <returns>The response message</returns>
        /// <exception cref="RequestReplacedException">
        /// Another request has replaced this request.
        /// </exception>
        /// <exception cref="ErrorResponseException">
        /// The response was <see cref="Command.Error"/>.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">The request did not
        /// complete before the timeout expired.</exception>
        /// <remarks>
        /// <para>This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received or the
        /// <see cref="TimeoutTimer"/> times out. However,
        /// there are some situations where it can get confused about
        /// which response goes with which request.</para>
        /// <para>If message ids are enabled on the device, then coordinating
        /// requests and responses is reliable. There were some bugs in the 
        /// firmware related
        /// to message ids, so make sure your firmware is at least
        /// version 5.07.</para>
        /// <para>If message ids are disabled (the default), then the following
        /// scenarios can cause errors:
        /// <list type="bullet">
        /// <item>You request a pre-emptable command from the conversation, and
        /// while it is running, a joystick sends a command to the same device.
        /// The conversation didn't see the request, so it assumes that the
        /// response is for the message it sent and returns it normally.</item>
        /// <item>You request a command from the conversation, and just as it
        /// starts, another thread requests another command. Depending on the
        /// timing, the conversation may not record the requests in the same
        /// order they get sent to the device, and the conversation may return 
        /// your command's response to the other thread and the other command's 
        /// response to you.</item>
        /// <item>You request a command from the conversation, and just as
        /// it finishes, another thread requests another command. Depending
        /// on the timing, the conversation may return your command's response
        /// to the other thread and the other command's response to you.</item>
        /// </list></para>
        /// </remarks>
        public virtual DeviceMessage Request(Command command, int data)
        {
            return RequestInUnits(command, new Measurement(data, UnitOfMeasure.Data));
        }

        /// <summary>
        /// Send a text command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <returns>The response message</returns>
        /// <exception cref="ErrorResponseException">
        /// The response was an error.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">No response was received
        /// before the timeout expired.</exception>
        /// <remarks>
        /// <para>This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received or the
        /// <see cref="TimeoutTimer"/> times out.</para>
        /// </remarks>
        public DeviceMessage Request(string text)
        {
            return RequestInUnits(text, null);
        }

        /// <summary>
        /// Send a text command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="measurement">A data value to append to the command
        /// will be calculated from this. If it is null, then the command 
        /// will be sent without any extra data.</param>
        /// <returns>The response message</returns>
        /// <exception cref="ErrorResponseException">
        /// The response was an error.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">No response was received
        /// before the timeout expired.</exception>
        /// <remarks>
        /// This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received or the
        /// <see cref="TimeoutTimer"/> times out.
        /// </remarks>
        public DeviceMessage RequestInUnits(
            string text, 
            Measurement measurement)
        {
            byte messageId = 0; // text-mode protocol doesn't use message ids.
            ConversationTopic topic;

            do
            {
                topic = StartTopic(messageId);

                device.SendInUnits(text, measurement);

                if (!topic.Wait(TimeoutTimer))
                {
                    throw new RequestTimeoutException();
                }
            } while (topic.IsFlowControlRejected);

            topic.Validate();
            return topic.Response;
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(String, int)"/>.</remarks>
        public DeviceMessage RequestInUnits(
            string text,
            int value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(text, new Measurement(value, unit));
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(String, int)"/>.</remarks>
        public DeviceMessage RequestInUnits(
            string text,
            decimal value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(text, new Measurement(value, unit));
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.TextData"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(String, int)"/>.</remarks>
        public DeviceMessage RequestInUnits(
            string text,
            double value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(text, new Measurement(value, unit));
        }

        /// <summary>
        /// Send a text command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="data">A data value to be appended to the command.
        /// </param>
        /// <returns>The response message</returns>
        /// <exception cref="ErrorResponseException">
        /// The response was an error.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">No response was received
        /// before the timeout expired.</exception>
        /// <remarks>
        /// <para>This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received or the
        /// <see cref="TimeoutTimer"/> times out.</para>
        /// </remarks>
        public DeviceMessage Request(string text, int data)
        {
            return RequestInUnits(
                text, 
                new Measurement(data, UnitOfMeasure.Data));
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="measurement">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <exception cref="RequestReplacedException">
        /// Another request has replaced this request.
        /// </exception>
        /// <exception cref="ErrorResponseException">
        /// The response was <see cref="Command.Error"/>.
        /// </exception>
        /// <exception cref="ZaberPortErrorException">
        /// A port error occurred while waiting for a response.
        /// </exception>
        /// <exception cref="RequestTimeoutException">The request did not
        /// complete before the timeout expired.</exception>
        /// <remarks>
        /// <para>This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received or the
        /// <see cref="TimeoutTimer"/> times out. However,
        /// there are some situations where it can get confused about
        /// which response goes with which request.</para>
        /// <para>If message ids are enabled on the device, then coordinating
        /// requests and responses is reliable. There were some bugs in the 
        /// firmware related
        /// to message ids, so make sure your firmware is at least
        /// version 5.07.</para>
        /// <para>If message ids are disabled (the default), then the following
        /// scenarios can cause errors:
        /// <list type="bullet">
        /// <item>You request a pre-emptable command from the conversation, and
        /// while it is running, a joystick sends a command to the same device.
        /// The conversation didn't see the request, so it assumes that the
        /// response is for the message it sent and returns it normally.</item>
        /// <item>You request a command from the conversation, and just as it
        /// starts, another thread requests another command. Depending on the
        /// timing, the conversation may not record the requests in the same
        /// order they get sent to the device, and the conversation may return 
        /// your command's response to the other thread and the other command's 
        /// response to you.</item>
        /// <item>You request a command from the conversation, and just as
        /// it finishes, another thread requests another command. Depending
        /// on the timing, the conversation may return your command's response
        /// to the other thread and the other command's response to you.</item>
        /// </list></para>
        /// </remarks>
        public virtual DeviceMessage RequestInUnits(
            Command command, 
            Measurement measurement)
        {
            var topic = StartTopic();
            var data = Device.CalculateData(command, measurement);
            PrepareForRetry(command, data, topic);

            device.Send(command, data, topic.MessageId);

            if (!topic.Wait(TimeoutTimer))
            {
                throw new RequestTimeoutException();
            }
            topic.Validate();
            var response = topic.Response;
            var commandInfo = response.CommandInfo;
            var motionType = Device.DeviceType.MotionType;
            if (motionType != MotionType.Other &&
                measurement.Unit != response.Measurement.Unit &&
                measurement.Unit != UnitOfMeasure.Data &&
                commandInfo != null &&
                commandInfo.RequestDataType == commandInfo.ResponseDataType)
            {
                var conversions = ConversionMap.Common.Find(
                    motionType,
                    commandInfo.ResponseDataType);
                var convertedMeasurement = conversions.Convert(
                    response.Measurement,
                    measurement.Unit);
                response = new DeviceMessage(response);
                response.Measurement = convertedMeasurement;
            }
            return response;
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(Command, int)"/>.</remarks>
        public virtual DeviceMessage RequestInUnits(
            Command command,
            decimal value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(command, new Measurement(value, unit));
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(Command, int)"/>.</remarks>
        public virtual DeviceMessage RequestInUnits(
            Command command,
            int value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(command, new Measurement(value, unit));
        }

        /// <summary>
        /// Send a command to a Zaber device and wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <param name="unit">Calculate the <see cref="DataPacket.Data"/>
        /// value from this.</param>
        /// <returns>The response message</returns>
        /// <remarks>For more details and a list of possible exceptions, see
        /// <see cref="Request(Command, int)"/>.</remarks>
        public virtual DeviceMessage RequestInUnits(
            Command command,
            double value,
            UnitOfMeasure unit)
        {
            return RequestInUnits(command, new Measurement(value, unit));
        }

        /// <summary>
        /// Wait until the device sends an alert message.
        /// </summary>
        /// <returns>The details of the alert message</returns>
        public DeviceMessage WaitForAlert()
        {
            byte messageId = 0;
            var topic = StartTopic(messageId);
            topic.IsAlert = true;
            if (device.AxisNumber == 0)
            {
                throw (new InvalidOperationException("WaitForAlert on AxisNumber 0 Invalid"));
            }
            if (!topic.Wait(AlertTimeoutTimer))
            {
                throw new TimeoutException(
                    "Timed out while waiting for alert message.");
            }
            topic.Validate();
            return topic.Response;
        }

        /// <summary>
        /// Repeatedly check the device's status until it is idle, using
        /// text-mode requests.
        /// </summary>
        public virtual void PollUntilIdle()
        {
            PollUntilIdle(PollTimeoutTimer);
        }

        /// <summary>
        /// Repeatedly check the device's status until it is idle, using
        /// text-mode requests and the given timer.
        /// </summary>
        /// <param name="timer">How long to wait between polling requests.
        /// </param>
        /// <exception cref="DeviceFaultException">Device reply contains
        /// a warning flag beginning with an "F".
        /// Device has a fault condition.</exception>
        /// <exception cref="RequestReplacedException">Previous movement
        /// command has been interrupted and did not complete.
        /// The command was replaced by a new command, or interruped
        /// by manual movement control.</exception>
        public virtual void PollUntilIdle(TimeoutTimer timer)
        {
            idleAlertEvent.Reset();
            DeviceMessage resp;
            bool isIdle;
            do
            {
                resp = Request("");
                isIdle = resp.IsIdle;

                if (!isIdle)
                {
                    isIdle = timer.WaitOne(idleAlertEvent);
                }
                else
                {
                    //check if device has raised any fault
                    if (resp.HasFault)
                    {
                        throw new DeviceFaultException(resp);
                    }
                    
                    //check the flags to see if the command was replaced
                    if (resp.FlagText != "--")
                    {
                        //there are active flags, get all flags and see if NI is set
                        if (Request("warnings").Body.Contains("NI"))
                        {
                            throw (new RequestReplacedException(String.Format(
                                CultureInfo.CurrentCulture,
                                "{2}: Device {0}{1} Previous command was interrupted",
                                Device.DeviceNumber,
                                Device.AxisNumber == 0
                                ? "" : " axis " + Device.AxisNumber,
                                typeof(RequestReplacedException).Name)));
                        }
                    }
                }
            } while (!isIdle);
        }

        /// <summary>
        /// Record the request that started a topic so it can be retried if 
        /// necessary.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="topic">The topic to record this request on. It should
        /// have been returned by StartTopic.</param>
        protected void PrepareForRetry(
            Command command, 
            int data, 
            ConversationTopic topic)
        {
            topic.RequestCommand = command;
            topic.RequestData = data;
            var commandInfo = device.DeviceType.GetCommandByNumber(command);
            topic.RetryCount =
                commandInfo != null && commandInfo.IsRetrySafe
                ? RetryCount
                : 0;
        }

        /// <summary>
        /// Record that a request is about to be made.
        /// </summary>
        /// <returns>The topic that will monitor the request.</returns>
        /// <remarks>
        /// <para>
        /// You can then make the request using the device directly and call 
        /// <see cref="ConversationTopic.Wait()"/> to wait for the response. This is
        /// useful if you want to make several unrelated requests of several devices
        /// and then wait until they are all completed. If message ids are enabled,
        /// be sure to use the message id from the topic when sending the request.
        /// </para>
        /// <para>
        /// If message ids are disabled, then this technique is not thread-safe,
        /// because there's no guarantee that two requests from two threads will get 
        /// sent to the device in the same order that their topics were started.
        /// If you're sending requests to one device from more than one thread,
        /// either enable message ids, or implement your own locking at a higher level.
        /// </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">All message IDs are in
        /// use in other pending conversations.</exception>
        public virtual ConversationTopic StartTopic()
        {
            int maxMessageIds = 254; // 0 and 255 are not allowed.
            for (int i = 0; i < maxMessageIds; i++)
            {
                try
                {
                    return StartTopic(CalculateMessageId());
                }
                catch (ActiveMessageIdException)
                {
                    continue; // Try the next message ID.
                }
            }
            throw new InvalidOperationException("No available message ID found.");
        }

        /// <summary>
        /// Record that a request is about to be made.
        /// </summary>
        /// <returns>The topic that will monitor the request.</returns>
        /// <param name="messageId">The message id that the request will use.</param>
        /// <remarks>
        /// <para>
        /// You can then make the request using the device directly and call 
        /// <see cref="ConversationTopic.Wait()"/> to wait for the response. This is
        /// useful if you want to make several unrelated requests of several devices
        /// and then wait until they are all completed. If message ids are disabled,
        /// messageId will be ignored.
        /// </para>
        /// <para>
        /// If message ids are disabled, then this technique is not thread-safe,
        /// because there's no guarantee that two requests from two threads will get 
        /// sent to the device in the same order that their topics were started.
        /// If you're sending requests to one device from more than one thread,
        /// either enable message ids, or implement your own locking at a higher level.
        /// </para>
        /// </remarks>
        /// <exception cref="ActiveMessageIdException">A topic already exists
        /// with the specified message ID.</exception>
        public virtual ConversationTopic StartTopic(byte messageId)
        {
            var topic = new ConversationTopic();
            log.DebugFormat(CultureInfo.CurrentCulture, "Adding to topic map");
            lock (topicMap)
            {
                if (!isRegisteredForPortErrors)
                {
                    device.Port.ErrorReceived += 
                        new EventHandler<ZaberPortErrorReceivedEventArgs>(Port_ErrorReceived);
                    isRegisteredForPortErrors = true;
                }

                if (device.AreMessageIdsEnabled)
                {
                    if (topicMap.ContainsKey(messageId))
                    {
                        throw new ActiveMessageIdException();
                    }
                    topic.MessageId = messageId;
                    topicMap.Add(topic.MessageId, topic);
                }
                topicList.AddLast(topic);

                topic.Completed += new EventHandler(topic_Completed);
            }
            return topic;
        }

        private void topic_Completed(object sender, EventArgs e)
        {
            if (sender == null)
	        {
                throw new ArgumentNullException("sender");
	        }
            var topic = sender as ConversationTopic;
            if (topic == null)
	        {
                throw new ArgumentException(
                    "Sender must be a ConversationTopic.", 
                    "sender");
	        }

            lock (topicMap)
            {
                RemoveTopic(topic);
            }
        }

        /// <summary>
        /// Calculates the next message id.
        /// </summary>
        /// <returns>The message id.</returns>
        protected byte CalculateMessageId()
        {
            byte messageId = 0;
            lock (topicMap)
            {
                if (device.AreMessageIdsEnabled)
                {
                    messageId = (byte)(lastId = (lastId % 254) + 1);
                }
            }
            return messageId;
        }

        void device_MessageReceived(object sender, DeviceMessageEventArgs e)
        {
            if (e.DeviceMessage.MessageType == MessageType.Alert &&
                e.DeviceMessage.Body.Equals("status idle"))
            {
                idleAlertEvent.Set();
            }
            ConversationTopic topic;
            byte messageId = e.DeviceMessage.MessageId;
            log.DebugFormat(
                CultureInfo.CurrentCulture, 
                "Grabbing topic map for response to message id {0}", 
                messageId);
            lock (topicMap)
            {
                topic = FindTopic(messageId, e.DeviceMessage.MessageType);
                if (topic != null && topic.RetryPacket != null)
                {
                    device.Port.CancelDelayedPacket(topic.RetryPacket);
                }
                if (topic == null && 
                    AreUnexpectedResponsesInvalid &&
                    Device.AreMessageIdsEnabled)
                {
                    OnTopicNotFound();
                }
                else
                {
                    DeviceMessage response = e.DeviceMessage;
                    CheckReplacement(topic, response);
                }
                if (topic == null)
                {
                    return;
                }
                log.DebugFormat(
                    CultureInfo.CurrentCulture,
                    "Releasing response to message id {0}",
                    topic.MessageId);
                topic.Response = e.DeviceMessage;
            }
        }

        /// <summary>
        /// Handle the event when a response comes in that doesn't correspond
        /// to any known topic. This is only called when 
        /// <see cref="AreUnexpectedResponsesInvalid"/> is true.
        /// </summary>
        protected virtual void OnTopicNotFound()
        {
            device.Port.ReportInvalidPacket();
        }

        private void RemoveTopic(ConversationTopic topic)
        {
            lock (topicMap)
            {
                topicMap.Remove(topic.MessageId);
                topicList.Remove(topic); 
            }
        }

        /// <summary>
        /// Find the topic that is waiting for the response we just received.
        /// If there's no message id, just return the most recently started
        /// topic.
        /// </summary>
        /// <param name="messageId">Message id used to identify the topic,
        /// or zero.</param>
        /// <param name="messageType">Message type we just received. Some
        /// kinds of text responses get ignored.</param>
        /// <returns>The topic that is waiting for the response.</returns>
        private ConversationTopic FindTopic(
            byte messageId, 
            MessageType messageType)
        {
            if (messageType != MessageType.Binary && 
                messageType != MessageType.Response && 
                messageType != MessageType.Alert)
            {
                return null;
            }
            lock (topicMap)
            {
                ConversationTopic topic;
                if (topicMap.ContainsKey(messageId))
                {
                    topic = topicMap[messageId];
                }
                else if (messageId == 0 && topicList.Count > topicMap.Count)
                {
                    topic =
                        topicList.Count > 0
                        ? topicList.Last.Value
                        : null;
                }
                else
                {
                    topic = null;
                }
                if (topic != null &&
                    (messageType == MessageType.Alert) != (topic.IsAlert))
                {
                    topic = null;
                }
                return topic; 
            }
        }

        /// <summary>
        /// Checks if the current response replaces pre-empted commands.
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="response"></param>
        private void CheckReplacement(ConversationTopic topic, DeviceMessage response)
        {
            if (response.MessageType != MessageType.Binary)
            {
                // Only binary mode commands are preempted.
                return;
            }
            if ((Command.Home < response.Command && response.Command < Command.UnexpectedPosition) ||
                (Command.UnexpectedPosition < response.Command && response.Command < Command.MoveToStoredPosition) ||
                Command.Stop < response.Command)
            {
                // Commands beyond Stop don't replace the pre-empted command.
                // They pre-empt it and then return to it.
                // Same with commands between home and unexpected position
                // Same with commands between unexpected position and Move to stored position
                return;
            }
            lock (topicMap)
            {
                while (topicList.Count > 0 && topicList.First.Value != topic)
                {
                    var waitingTopic = topicList.First.Value;
                    topicList.RemoveFirst();
                    topicMap.Remove(waitingTopic.MessageId);

                    // also releases the topic
                    waitingTopic.ReplacementResponse = response;
                }
            }
        }

        /// <summary>
        /// This is really only used for testing purposes to make sure that no
        /// requests are left over in any of the internal data structures.
        /// </summary>
        public virtual bool IsWaiting
        {
            get 
            {
                lock (topicMap)
                {
                    return topicMap.Count > 0 || topicList.Count > 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets the length of time in milliseconds to wait for a response to
        /// any request. Use System.Threading.Timeout.Infinite (-1) to wait forever.
        /// Defaults to infinite.
        /// </summary>
        public int Timeout
        {
            get { return TimeoutTimer.Timeout; }
            set { TimeoutTimer.Timeout = value; }
        }

        /// <summary>
        /// Used to implement the <see cref="Timeout"/>. This is only really useful for
        /// testing, most clients can just use the <see cref="Timeout"/> property.
        /// </summary>
        public TimeoutTimer TimeoutTimer { get; set; }

        /// <summary>
        /// Gets or sets the length of time in milliseconds to wait for an alert message.
        /// Use System.Threading.Timeout.Infinite (-1) to wait forever.
        /// Defaults to infinite.
        /// </summary>
        public int AlertTimeout
        {
            get { return AlertTimeoutTimer.Timeout; }
            set { AlertTimeoutTimer.Timeout = value; }
        }

        /// <summary>
        /// Used to implement the <see cref="AlertTimeout"/>. This is only 
        /// really useful for testing, most clients can just use the 
        /// <see cref="AlertTimeout"/> property.
        /// </summary>
        public TimeoutTimer AlertTimeoutTimer { get; set; }

        /// <summary>
        /// Gets or sets the length of time in milliseconds to wait between
        /// polling requests.
        /// Defaults to 100.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">When the timeout
        /// value is negative or zero.</exception>
        public int PollTimeout
        {
            get { return PollTimeoutTimer.Timeout; }
            set 
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(
                        "value",
                        "PollTimeout must be positive.");
                }
                PollTimeoutTimer.Timeout = value; 
            }
        }

        /// <summary>
        /// Used to implement the <see cref="PollTimeout"/>. This is only 
        /// really useful for testing, most clients can just use the 
        /// <see cref="PollTimeout"/> property.
        /// </summary>
        public TimeoutTimer PollTimeoutTimer { get; set; }

        /// <summary>
        /// Send a command to this device that doesn't require a data value
        /// and don't wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        public void Send(Command command)
        {
            Device.Send(command);
        }

        /// <summary>
        /// Send a command to this device without using a message id and don't 
        /// wait for the response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        public void Send(Command command, int data)
        {
            Device.Send(command, data);
        }

        /// <summary>
        /// Send a command to this device and don't wait for a response.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="messageId">See <see cref="DeviceMessage.MessageId"/>.</param>
        public void Send(Command command, int data, byte messageId)
        {
            Device.Send(command, data, messageId);
        }

        /// <summary>
        /// Send a command to this device and don't wait for a response.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        public void Send(String text)
        {
            Device.Send(text);
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                idleAlertEvent.Close();
            }
        }

        #endregion

        /// <summary>
        /// Add a conversation that represents an axis for this controller.
        /// </summary>
        /// <param name="axis">The axis to add.</param>
        /// <remarks>All controllers have a collection of devices - one
        /// for each axis.</remarks>
        public void AddAxis(Conversation axis)
        {
            axes.Add(axis);
        }

        /// <summary>
        /// Get a list of axes for this device.
        /// </summary>
        public ReadOnlyCollection<Conversation> Axes
        {
            get
            {
                return axes.AsReadOnly();
            }
        }
    }
}
