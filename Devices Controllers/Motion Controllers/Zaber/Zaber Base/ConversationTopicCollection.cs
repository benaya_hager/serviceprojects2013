﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// A collection of <see cref="ConversationTopic"/> objects. It's used to represent
    /// a topic in a <see cref="ConversationCollection"/>.
    /// </summary>
    /// <remarks>
    /// This class is both a ConversationTopic and a collection of them. Waiting for it
    /// waits for all its items to complete. The class is almost completely thread-safe.
    /// The only exception is the <see cref="GetEnumerator"/> method. Enumerating through
    /// the collection while other threads are marking its items as completed or adding or
    /// removing items is not safe. A safe alternative would be to call <see cref="CopyTo"/>
    /// and then iterate through the array, but remember that you wouldn't see any changes to the
    /// collection that occurred after the call to CopyTo.
    /// </remarks>
    public class ConversationTopicCollection : ConversationTopic, IList<ConversationTopic>
    {
        private const String READ_ONLY_MESSAGE = 
            "Response properties are read-only in a ConversationTopicCollection.";

        private object locker = new object();
        private IList<ConversationTopic> items =
            new List<ConversationTopic>();

        /// <summary>
        /// Gets the details of the response received from the device.
        /// </summary>
        /// <remarks>Actually delegates to the first topic in the collection.
        /// Setting this is not supported.</remarks>
        public override DeviceMessage Response
        {
            get
            {
                return FirstItem.Response;
            }
            set
            {
                throw new NotSupportedException(READ_ONLY_MESSAGE);
            }
        }

        /// <summary>
        /// Gets the details of a response when this topic's request has
        /// been replaced by another.
        /// </summary>
        /// <remarks>Actually delegates to the first topic in the collection.
        /// Setting this is not supported.</remarks>
        public override DeviceMessage ReplacementResponse
        {
            get
            {
                return FirstItem.ReplacementResponse;
            }
            set
            {
                throw new NotSupportedException(READ_ONLY_MESSAGE);
            }
        }

        /// <summary>
        /// Gets the port error that was detected while waiting
        /// for a response.
        /// </summary>
        /// <remarks>Actually delegates to the first topic in the collection.
        /// Setting this is not supported.</remarks>
        public override ZaberPortError ZaberPortError
        {
            get
            {
                return FirstItem.ZaberPortError;
            }
            set
            {
                throw new NotSupportedException(READ_ONLY_MESSAGE);
            }
        }

        /// <summary>
        /// Blocks the current thread until the topic is completed.
        /// </summary>
        /// <returns>This method always returns true.</returns>
        /// <remarks>
        /// The topic can be completed by setting any of these properties:
        /// <see cref="Response"/>, <see cref="ZaberPortError"/>,
        /// <see cref="ReplacementResponse"/>. However, if the topic 
        /// collection is empty, this method immediately returns true
        /// without waiting.
        /// </remarks>
        public override bool Wait()
        {
            if (Count == 0)
            {
                return true;
            }
            return base.Wait();
        }

        /// <summary>
        /// Blocks the current thread until the topic is completed or the
        /// timer times out.
        /// </summary>
        /// <param name="timeoutTimer">The timer to wait with.</param>
        /// <returns>True if the topic completed before the timer expired, 
        /// otherwise false.</returns>
        /// <remarks>
        /// The topic can be completed by setting any of these properties:
        /// <see cref="Response"/>, <see cref="ZaberPortError"/>,
        /// <see cref="ReplacementResponse"/>. However, if the topic 
        /// collection is empty, this method immediately returns true
        /// without waiting.
        /// </remarks>
        public override bool Wait(TimeoutTimer timeoutTimer)
        {
            if (Count == 0)
            {
                return true;
            }
            return base.Wait(timeoutTimer);
        }

        /// <summary>
        /// Get a flag showing whether this topic was canceled by a call to
        /// <see cref="Cancel"/>.
        /// </summary>
        /// <remarks>Actually delegates to the first topic in the collection.
        /// </remarks>
        public override bool IsCanceled
        {
            get
            {
                return FirstItem.IsCanceled;
            }
        }
        /// <summary>
        /// Stop waiting for a response.
        /// </summary>
        /// <remarks>Sets IsCanceled to true and marks the topic complete.</remarks>
        public override void Cancel()
        {
            foreach (var item in items)
            {
                item.Cancel();
            }
        }

        private ConversationTopic FirstItem
        {
            get 
            {
                if (items.Count == 0)
                {
                    throw new InvalidOperationException(
                        "Response properties are undefined for an empty collection.");
                }
                return items[0]; 
            }
        }

        /// <summary>
        /// Validates that the request has been completed successfully.
        /// </summary>
        /// <remarks>
        /// If you want to check whether the response is valid without throwing
        /// an exception, use <see cref="IsValid"/> instead.
        /// </remarks>
        /// <exception cref="RequestCollectionException">Multiple requests were
        /// made together, and some of them failed.</exception>
        public override void Validate()
        {
            lock (locker)
            {
                if (!IsValid)
                {
                    throw new RequestCollectionException(items);
                }
            }
        }

        /// <summary>
        /// Gets a flag showing whether the request has been completed successfully.
        /// </summary>
        /// <seealso cref="Validate"/>
        public override bool IsValid
        {
            get
            {
                lock (locker)
                {
                    foreach (var item in items)
                    {
                        if (!item.IsValid)
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }

        #region ICollection<ConversationTopic> Members

        /// <summary>
        /// Adds a topic to the collection.
        /// </summary>
        /// <param name="item">The topic to add to the collection</param>
        public void Add(ConversationTopic item)
        {
            lock (locker)
            {
                items.Add(item);
                item.Completed += new EventHandler(item_Completed);
            }
        }

        private void item_Completed(object sender, EventArgs e)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    if (!item.IsComplete)
                    {
                        // still have some items incomplete, so do nothing.
                        return;
                    }
                }
            }
            // All items are complete, mark me complete.
            Complete();
        }

        /// <summary>
        /// Removes all topics from the collection.
        /// </summary>
        public void Clear()
        {
            lock (locker)
            {
                items.Clear();
            }
        }

        /// <summary>
        /// Determines whether the collection contains a specific topic.
        /// </summary>
        /// <param name="item">The topic to locate in the collection</param>
        /// <returns>true if the topic is found in the collection, otherwise false.</returns>
        public bool Contains(ConversationTopic item)
        {
            lock (locker)
            {
                return items.Contains(item);
            }
        }

        /// <summary>
        /// Copies the topics in the collection to a
        ///     System.Array, starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the topics
        ///     copied from the collection. The System.Array must
        ///     have zero-based indexing.
        ///</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
        /// <exception cref="ArgumentNullException">array is null.</exception>
        /// <exception cref="ArgumentException">arrayIndex is equal 
        /// to or greater than the
        ///     length of array.-or-The number of elements in the source collection
        ///     is greater than the available space from arrayIndex to the end of the destination
        ///     array.</exception>
        public void CopyTo(ConversationTopic[] array, int arrayIndex)
        {
            lock (locker)
            {
                items.CopyTo(array, arrayIndex);
            }
        }

        /// <summary>
        /// Gets the number of items contained in the collection.
        /// </summary>
        public int Count
        {
            get { return items.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return items.IsReadOnly; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific item from the collection.
        /// </summary>
        /// <param name="item">The item to remove from the collection.</param>
        /// <returns>true if the item was successfully removed from the collection,
        ///     otherwise false. This method also returns false if the item is not found in
        ///     the collection.</returns>
        public bool Remove(ConversationTopic item)
        {
            lock (locker)
            {
                return items.Remove(item);
            }
        }

        #endregion

        #region IEnumerable<ConversationTopic> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        /// <remarks>
        /// Iterating through the collection is not thread-safe. A safe 
        /// alternative would be to call <see cref="CopyTo"/>
        /// and then iterate through the array, but remember that you wouldn't 
        /// see any changes to the
        /// collection that occurred after the call to CopyTo.
        /// </remarks>
        public IEnumerator<ConversationTopic> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        /// <remarks>
        /// Iterating through the collection is not thread-safe. A safe 
        /// alternative would be to call <see cref="CopyTo"/>
        /// and then iterate through the array, but remember that you wouldn't 
        /// see any changes to the
        /// collection that occurred after the call to CopyTo.
        /// </remarks>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion

        #region IList<ConversationTopic> Members

        /// <summary>
        ///     Searches for the specified item and returns the zero-based index of the
        ///     first occurrence within the entire list.
        /// </summary>
        /// <param name="item">The item to locate in the list. The value
        ///     can be null.</param>
        /// <returns>The zero-based index of the first occurrence of item within the entire 
        /// list, if found; otherwise, –1.</returns>
        public int IndexOf(ConversationTopic item)
        {
            lock (locker)
            {
                return items.IndexOf(item);
            }
        }

        /// <summary>
        ///     Inserts an item into the list at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert. The value can be null.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is greater than <see cref="Count"/>.</exception>
        public void Insert(int index, ConversationTopic item)
        {
            lock (locker)
            {
                items.Insert(index, item);
            }
        }

        /// <summary>
        ///     Removes the item at the specified index of the list.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        public void RemoveAt(int index)
        {
            lock (locker)
            {
                items.RemoveAt(index);
            }
        }

        /// <summary>
        ///     Gets or sets the item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to get or set.</param>
        /// <returns>The item at the specified index.</returns>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        /// <remarks>
        /// Note that index is not necessarily the same as device number.
        /// </remarks>
        public ConversationTopic this[int index]
        {
            get
            {
                lock (locker)
                {
                    return items[index];
                }
            }
            set
            {
                lock (locker)
                {
                    items[index] = value;
                }
            }
        }

        #endregion
    }
}
