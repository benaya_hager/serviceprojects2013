﻿using System;
using System.Runtime.Serialization;

namespace Zaber
{
    /// <summary>
    /// A loopback connection was detected on a serial port.
    /// </summary>
    /// <remarks>A loopback connection
    /// is a connection from the serial port's send pin to the receive pin. 
    /// This makes it appear as though the serial port is receiving a copy of 
    /// every byte it sends. This can happen intentionally when a loopback dongle
    /// is connected to the serial port, or unintentionally when wires are
    /// connected incorrectly.</remarks>
    [Serializable]
    public class LoopbackException : Exception
    {
        public LoopbackException()
            : base("Loopback connection detected.")
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public LoopbackException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this exception</param>
        public LoopbackException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo 
        /// that holds the serialized object data about the exception being 
        /// thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext 
        /// that contains contextual information about the source or 
        /// destination.</param>
        protected LoopbackException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
