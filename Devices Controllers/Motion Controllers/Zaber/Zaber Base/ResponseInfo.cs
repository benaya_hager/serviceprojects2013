using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Descriptive information about a command that Zaber devices support.
    /// </summary>
    /// <remarks>This class is specific to response-only commands.</remarks>
    public class ResponseInfo : CommandInfo
    {
        /// <summary>
        /// Response-only commands are never sent to the devices. They come back as
        /// responses. The error response is a good example.
        /// </summary>
        public override bool IsResponseOnly { get { return true; } }
    }
}
