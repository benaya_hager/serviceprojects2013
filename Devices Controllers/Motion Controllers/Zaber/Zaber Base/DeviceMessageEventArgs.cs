using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Notification of a request or response from a <see cref="ZaberDevice"/>.
    /// </summary>
    public class DeviceMessageEventArgs : EventArgs
    {
        private DeviceMessage deviceMessage;

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="deviceMessage">See <see cref="DeviceMessage"/></param>
        public DeviceMessageEventArgs(DeviceMessage deviceMessage)
        {
            this.deviceMessage = deviceMessage;
        }

        /// <summary>
        /// The details of the device's request or response.
        /// </summary>
        public DeviceMessage DeviceMessage
        {
            get { return deviceMessage; }
            set { deviceMessage = value; }
        }
    }
}
