﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Zaber")]
[assembly: AssemblyDescription("Zaber device controller library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Zaber")]
[assembly: AssemblyProduct("Zaber")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Configure log4net using the .config file
// This will cause log4net to look for a configuration file
// called TestApp.exe.config in the application base
// directory (i.e. the directory containing TestApp.exe)
// The config file will be watched for changes.
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

// The Common Language Specification (CLS) defines naming restrictions, data 
// types, and rules to which assemblies must conform if they are to be used 
// across programming languages.
[assembly: CLSCompliant(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("116a0373-e7ad-4217-a386-9281cea7ef19")]
