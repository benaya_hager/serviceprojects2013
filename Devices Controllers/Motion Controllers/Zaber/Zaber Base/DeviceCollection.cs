using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// A collection of <see cref="ZaberDevice"/> objects. It's used to represent
    /// an Alias device number.
    /// </summary>
    /// <remarks>
    /// This class is both a ZaberDevice and a collection of them. Any requests
    /// you send to it get received by all its member devices. (The hardware handles
    /// that.) Any request or response events from its members will also be raised
    /// by the collection. (The software handles that.)
    /// </remarks>
    public class DeviceCollection : ZaberDevice, IList<ZaberDevice>
    {
        private List<ZaberDevice> members = new List<ZaberDevice>();
        private bool isDeviceTypeNameCalculated;

        /// <summary>
        /// True if this is a single device, and not a collection of devices.
        /// </summary>
        public override bool IsSingleDevice
        {
            get { return false; }
        }

        void item_MessageReceived(object sender, DeviceMessageEventArgs e)
        {
            if (isOriginalSender(sender, e))
            {
                OnMessageReceived(e);
            }
        }

        private static bool isOriginalSender(object sender, DeviceMessageEventArgs e)
        {
            ZaberDevice device = (ZaberDevice)sender;
            return device.DeviceNumber == e.DeviceMessage.DeviceNumber;
        }

        void item_MessageSent(object sender, DeviceMessageEventArgs e)
        {
            if (isOriginalSender(sender, e))
            {
                OnMessageSent(e); 
            }
        }

        private void UnregisterFrom(ZaberDevice item)
        {
            item.MessageReceived -=
                new EventHandler<DeviceMessageEventArgs>(item_MessageReceived);
            item.MessageSent -=
                new EventHandler<DeviceMessageEventArgs>(item_MessageSent);
        }

        #region ICollection<ZaberDevice> Members

        /// <summary>
        /// Adds a device to the collection.
        /// </summary>
        /// <param name="item">The device to add to the collection</param>
        public void Add(ZaberDevice item)
        {
            if (members.Count == 0)
            {
                isDeviceTypeNameCalculated = (DeviceType.Name == null);
                DeviceType = new DeviceType(DeviceType);
                DeviceType.Commands = item.DeviceType.Commands; // copy
            }
            else
            {
                HashSet<String> textCommands = new HashSet<string>(
                    DeviceType.Commands.Select(cmd => cmd.TextCommand).Where(
                    text => text != null));
                List<CommandInfo> newCommands = null;
                foreach (var command in item.DeviceType.Commands)
                {
                    bool isNew =
                        command.TextCommand != null
                        ? !textCommands.Contains(command.TextCommand)
                        : DeviceType.GetCommandByNumber(command.Command) == null;
                    if (isNew)
                    {
                        if (newCommands == null)
                        {
                            newCommands = new List<CommandInfo>(DeviceType.Commands);
                        }
                        newCommands.Add(command);
                    }
                }
                if (newCommands != null)
                {
                    DeviceType.Commands = newCommands;
                }
            }
            members.Add(item);

            item.MessageSent += 
                new EventHandler<DeviceMessageEventArgs>(item_MessageSent);
            item.MessageReceived +=
                new EventHandler<DeviceMessageEventArgs>(item_MessageReceived);

            if (isDeviceTypeNameCalculated)
            {
                List<string> deviceNumbers = new List<string>();
                foreach (ZaberDevice device in members)
                {
                    deviceNumbers.Add(device.DeviceNumber.ToString(
                        CultureInfo.CurrentCulture));
                }
                deviceNumbers.Sort();
                DeviceType.Name = String.Format(
                    CultureInfo.CurrentCulture,
                    "Alias ({0})",
                    String.Join(", ", deviceNumbers.ToArray()));
                
            }
        }

        /// <summary>
        /// Removes all devices from the collection.
        /// </summary>
        public void Clear()
        {
            foreach (ZaberDevice member in members)
            {
                UnregisterFrom(member);
            }
            members.Clear();
        }

        /// <summary>
        /// Determines whether the collection contains a specific device.
        /// </summary>
        /// <param name="item">The device to locate in the collection</param>
        /// <returns>true if the device is found in the collection, otherwise false.</returns>
        public bool Contains(ZaberDevice item)
        {
            return members.Contains(item);
        }

        /// <summary>
        /// Copies the devices in the collection to a
        ///     System.Array, starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the devices
        ///     copied from the collection. The System.Array must
        ///     have zero-based indexing.
        ///</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
        /// <exception cref="ArgumentNullException">array is null.</exception>
        /// <exception cref="ArgumentException">arrayIndex is equal 
        /// to or greater than the
        ///     length of array.-or-The number of elements in the source collection
        ///     is greater than the available space from arrayIndex to the end of the destination
        ///     array.</exception>
        public void CopyTo(ZaberDevice[] array, int arrayIndex)
        {
            members.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of devices contained in the collection.
        /// </summary>
        public int Count
        {
            get { return members.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific device from the collection.
        /// </summary>
        /// <param name="item">The device to remove from the collection.</param>
        /// <returns>true if the device was successfully removed from the collection,
        ///     otherwise false. This method also returns false if the device is not found in
        ///     the collection.</returns>
        public bool Remove(ZaberDevice item)
        {
            UnregisterFrom(item);
            return members.Remove(item);
        }

        #endregion

        #region IEnumerable<ZaberDevice> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        public IEnumerator<ZaberDevice> GetEnumerator()
        {
            return members.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return members.GetEnumerator(); ;
        }

        #endregion

        #region IList<ZaberDevice> Members

        /// <summary>
        ///     Searches for the specified device and returns the zero-based index of the
        ///     first occurrence within the entire list.
        /// </summary>
        /// <param name="item">The device to locate in the list. The value
        ///     can be null.</param>
        /// <returns>The zero-based index of the first occurrence of item within the entire 
        /// list, if found; otherwise, �1.</returns>
        public int IndexOf(ZaberDevice item)
        {
            return members.IndexOf(item);
        }

        /// <summary>
        ///     Inserts an element into the list at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert. The value can be null.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is greater than <see cref="Count"/>.</exception>
        public void Insert(int index, ZaberDevice item)
        {
            members.Insert(index, item);
        }

        /// <summary>
        ///     Removes the element at the specified index of the list.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        public void RemoveAt(int index)
        {
            members.RemoveAt(index);
        }

        /// <summary>
        ///     Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        public ZaberDevice this[int index]
        {
            get
            {
                return members[index];
            }
            set
            {
                members[index] = value;
            }
        }

        #endregion
    }
}
