﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;

namespace Zaber
{
    /// <summary>
    /// Registering for response events can be awkward, especially in scripts. 
    /// This class provides the base features for other classes to register
    /// for events and store them until you request them with the 
    /// <see cref="NextResponse()"/> method. Start and stop listening
    /// with the <see cref="Start"/> and <see cref="Stop"/> methods.
    /// </summary>
    /// <typeparam name="T">the type of response objects</typeparam>
    public abstract class ResponseListener<T> : IDisposable where T : class
    {
        private static ILog _log = LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Queue<T> _responses = new Queue<T>();
        private Semaphore _semaphore;
        private object _locker = new object();

        /// <summary>
        /// Gets a flag showing whether the listener is currently listening for
        /// responses.
        /// </summary>
        public bool IsListening
        {
            get
            {
                lock (_locker)
                {
                    return _semaphore != null;
                }
            }
        }

        /// <summary>
        /// Stop listening for responses.
        /// </summary>
        public void Stop()
        {
            lock (_locker)
            {
                if (_semaphore == null)
                {
                    throw new InvalidOperationException("Listener is already stopped.");
                }
                UnregisterEvent();

                _semaphore.Close();
                _semaphore = null;
            }
        }

        /// <summary>
        /// Derived classes need to override this method and register with
        /// whatever source of events they care about.
        /// </summary>
        /// <seealso cref="UnregisterEvent"/>
        /// <seealso cref="OnItemReceived"/>
        protected abstract void RegisterEvent();

        /// <summary>
        /// Derived classes need to override this method and unregister from
        /// whatever source of events they care about.
        /// </summary>
        /// <seealso cref="RegisterEvent"/>
        /// <seealso cref="OnItemReceived"/>
        protected abstract void UnregisterEvent();

        /// <summary>
        /// Start listening for responses.
        /// </summary>
        public void Start()
        {
            _log.Debug("starting listener");
            lock (_locker)
            {
                if (_semaphore != null)
                {
                    throw new InvalidOperationException("Listener is already started.");
                }
                _semaphore = new Semaphore(0, Int32.MaxValue);
                RegisterEvent();
            }
        }

        /// <summary>
        /// Handle a response event.
        /// </summary>
        /// <param name="response">The details of the response.</param>
        /// <seealso cref="RegisterEvent"/>
        /// <seealso cref="UnregisterEvent"/>
        protected void OnItemReceived(T response)
        {
            lock (_locker)
            {
                if (_semaphore != null)
                {
                    _responses.Enqueue(response);
                    _semaphore.Release();
                }
            }
        }

        /// <summary>
        /// Returns the next response or waits for one to be received.
        /// </summary>
        /// <returns>The response message</returns>
        /// <remarks>
        /// See <see cref="NextResponse(bool)"/> if you don't want to wait for
        /// a response to be received.
        /// </remarks>
        public T NextResponse()
        {
            return NextResponse(true);
        }

        /// <summary>
        /// Returns the next response or waits for one to be received.
        /// </summary>
        /// <param name="isBlocking">True if the method should wait for a 
        /// response if there are none already received.</param>
        /// <returns>The response message or null if no responses have
        /// been received.</returns>
        public T NextResponse(bool isBlocking)
        {
            var semaphore = _semaphore;
            CheckIsListening(semaphore);
            var timeout = isBlocking ? Timeout.Infinite : 0;
            if ( ! semaphore.WaitOne(timeout, false))
            {
                return null;
            }
            lock (_locker)
            {
                return _responses.Dequeue();
            }
        }

        /// <summary>
        /// Check that the listener is still listening, based on the current
        /// semaphore.
        /// </summary>
        /// <param name="semaphore">The value to check for null.</param>
        private static void CheckIsListening(Semaphore semaphore)
        {
            if (semaphore == null)
            {
                throw new InvalidOperationException(
                    "NextResponse() called when listener is not listening.");
            }
        }

        /// <summary>
        /// Returns the next response or waits for one to be received.
        /// </summary>
        /// <param name="timer">The timer that tells how long to wait
        /// for a response to be received.</param>
        /// <returns>The response message.</returns>
        /// <exception cref="RequestTimeoutException">When the timer expires 
        /// before any response is received.</exception>
        public T NextResponse(TimeoutTimer timer)
        {
            var semaphore = _semaphore;
            CheckIsListening(semaphore);
            if ( ! timer.WaitOne(semaphore))
            {
                throw new RequestTimeoutException();
            }
            lock (_locker)
            {
                return _responses.Dequeue();
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                lock (_locker)
                {
                    UnregisterEvent();
                    _semaphore.Close();
                    _semaphore = null;
                }
            }
        }

        #endregion
    }
}
