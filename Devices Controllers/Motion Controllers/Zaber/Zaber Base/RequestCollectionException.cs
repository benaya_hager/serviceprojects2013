using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Permissions;
using System.Runtime.Serialization;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// Exception thrown by a <see cref="ConversationCollection"/> when some
    /// requests in the list fail.
    /// </summary>
    [Serializable]
    public class RequestCollectionException : ConversationException
    {
        private List<ConversationTopic> topics;

        public RequestCollectionException()
            : base("Some requests failed.")
        {
            topics = new List<ConversationTopic>();
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public RequestCollectionException(string message)
            : base(message)
        {
            topics = new List<ConversationTopic>();
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this exception</param>
        public RequestCollectionException(string message, Exception ex)
            : base(message, ex)
        {
            topics = new List<ConversationTopic>();
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected RequestCollectionException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            int topicCount = info.GetInt32("topicCount");
            topics = new List<ConversationTopic>();
            for (int i = 0; i < topicCount; i++)
            {
                String name = String.Format(
                    CultureInfo.InvariantCulture,
                    "topic{0}", 
                    i++);
                topics.Add(
                    (ConversationTopic)info.GetValue(name, typeof(ConversationTopic)));
            }
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="topics">All the topics used in the request, including 
        /// the one or ones that caused the exception</param>
        public RequestCollectionException(ICollection<ConversationTopic> topics) : 
            base(BuildMessage(topics))
        {
            this.topics = new List<ConversationTopic>();
            this.topics.AddRange(topics);
        }

        /// <summary>
        /// Builds a text message that describes why some requests failed.
        /// </summary>
        /// <param name="topics">The topics to validate.</param>
        /// <returns>A text message if some topics were invalid, 
        /// otherwise an empty string.</returns>
        public static string BuildMessage(ICollection<ConversationTopic> topics)
        {
            var portErrors = new HashSet<ZaberPortError>();

            StringBuilder sb = new StringBuilder();
            int errorCount = 0;
            foreach (var topic in topics)
            {
                if (topic != null)
                {
                    if (topic.IsValid)
                    {
                        continue;
                    }
                    if (topic.ZaberPortError != ZaberPortError.None)
                    {
                        portErrors.Add(topic.ZaberPortError);
                        continue;
                    }
                }

                if (errorCount == 0)
                {
                    sb.Append("Some requests failed: ");
                }
                else
                {
                    sb.Append("; ");
                }
                errorCount++;

                if (topic == null)
                {
                    sb.Append("null topic");
                }
                else if (topic.ReplacementResponse != null)
                {
                    sb.AppendFormat(
                        "request replaced on device {0}",
                        topic.ReplacementResponse.DeviceNumber);
                }
                else if (topic.Response == null)
                {
                    sb.Append("null response");
                }
                else if (topic.Response.IsError)
                {
                    String errorMessage;
                    if (topic.Response.MessageType != MessageType.Binary)
                    {
                        errorMessage = topic.Response.TextData;
                    }
                    else
                    {
                        int errorNumber = topic.Response.Data;
                        string errorName =
                            Enum.GetName(typeof(ZaberError), errorNumber);
                        errorMessage =
                            errorName == null
                            ? string.Format(
                                CultureInfo.CurrentCulture,
                                "error {0}",
                                errorNumber)
                            : string.Format(
                                CultureInfo.CurrentCulture,
                                "{0} error",
                                errorName);
                    }
                    sb.AppendFormat(
                        "{1} on device {0}",
                        topic.Response.DeviceNumber,
                        errorMessage);
                }
            }
            if (errorCount > 0)
            {
                sb.Append(".");
            }
            foreach (var portError in portErrors)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" ");
                }
                sb.AppendFormat(
                    "Some requests failed because of a port error: {0}.",
                    portError);
            }
            return sb.ToString();
        }

        /// <summary>
        /// All the topics used in the request, including 
        /// the one or ones that caused the exception.
        /// </summary>
        public IList<ConversationTopic> Topics
        {
            get { return topics; }
        }

        /// <summary>
        /// Sets the System.Runtime.Serialization.SerializationInfo
        ///     with information about the exception.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info,
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);

            info.AddValue("topicCount", topics.Count);
            int i = 0;
            foreach (var topic in topics)
            {
                String name = String.Format(
                    CultureInfo.InvariantCulture,
                    "topic{0}", 
                    i++);
                info.AddValue(name, topic);
            }
        }
    }
}
