﻿<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.springframework.net"
	xmlns:p="http://www.springframework.net">
	<xsl:output method="text" version="4.0" indent="yes" 
		encoding="iso-8859-1" />

	<!-- This transforms the wiki content into a text file that
	contains a list of all error codes suitable for using in the ZaberError enumeration.
  -->

  <!-- This is the main template that matches the root node. It finds all the
  errors from both files, eliminates the duplicates, and then dumps a list in
  sorted order. -->
	<xsl:template match="/">
    <xsl:variable
      name="errors"
      select="/html/div/table[@id='error-codes']/tr"/>
    <xsl:variable
      name="error-codes"
      select="$errors/td[1]"/>

    <xsl:text>
      /// &lt;summary&gt;
      /// No error has occurred.
      /// &lt;/summary&gt;
      /// &lt;remarks&gt;
      /// This should never be returned by a device. It's only useful as a 
      /// default value in code.
      /// &lt;/remarks&gt;
      None = 0</xsl:text>
    
    <xsl:for-each select="$error-codes">
			<xsl:sort data-type="number"/>

      <xsl:variable name="error-code" select="."/>
      <xsl:variable 
        name="current-error" 
        select="$errors[td[1]=$error-code][1]"/>
      <xsl:variable name="command-name" select="$current-error/td[2]"/>
      <xsl:variable name="description" select="normalize-space($current-error/td[3])"/>
      <xsl:variable name="stripped-name" select="translate($command-name, ' -/', '')"/>

      <xsl:text>,

      /// &lt;summary&gt;
      /// </xsl:text>
      <xsl:value-of select="$description"/>
			<xsl:text>
      /// &lt;/summary&gt;
      </xsl:text>
			<xsl:value-of select="$stripped-name"/>
			<xsl:text> = </xsl:text>
			<xsl:value-of select="$error-code"/>
      <xsl:if test="$error-code = 44">
        <xsl:text>,

      /// &lt;summary&gt;
      /// Identical to MaximumPositionInvalid, this is just for backward 
      /// compatibility. 
      /// &lt;/summary&gt;
      MaximumRangeInvalid = 44</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:transform>