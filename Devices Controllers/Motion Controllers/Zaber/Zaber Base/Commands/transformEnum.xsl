﻿<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.springframework.net"
	xmlns:p="http://www.springframework.net">
	<xsl:output method="text" version="4.0" indent="yes" 
		encoding="iso-8859-1" />

	<!-- This transforms the Spring configuration file into a text file that
	contains a list of all commands suitable for using in the Command enumeration.
	-->
	

	<!-- This is the main template that matches the root node. It sets up each
	device type with a list of commands by using the command-list mode, and
	then it generates the commands themselves using the default mode.
	
	The skip-duplicates and command-list-skip-duplicates are used to generate
	any joystick commands that aren't duplicates of actuator commands. -->
	<xsl:template match="/">
		<xsl:for-each select="/p:objects/p:object">
			<xsl:sort select="p:property[@name='Number']/@value" data-type="number"/>

			<!-- Calculate how long the beginning would be if the name ends in 'Command'. -->
			<xsl:variable name="rootNameLength" select="string-length(@id) - 6"/>
			<xsl:variable name="suffix" select="substring(@id, $rootNameLength)"/>
			<xsl:if test="$suffix = 'Command'">
				<xsl:variable name="command-name" select="p:property[@name='Name']/@value"/>
				<xsl:variable name="stripped-name" select="translate($command-name, ' -/', '')"/>
				<xsl:variable name="number" select="p:property[@name='Number']/@value"/>
				<xsl:variable name="data-description" select="p:property[@name='DataDescription']/@value"/>
				<xsl:variable name="response-description" select="p:property[@name='ResponseDescription']/@value"/>
				<xsl:text>
        /// &lt;summary&gt;
        /// </xsl:text>
				<xsl:choose>
					<xsl:when test="$data-description | $response-description">
						<xsl:if test="$data-description">
							<xsl:text>Request data is </xsl:text>
							<xsl:value-of select="$data-description"/>
							<xsl:text>. </xsl:text>
						</xsl:if>
						<xsl:if test="$response-description">
							<xsl:text>Response data is </xsl:text>
							<xsl:value-of select="$response-description"/>
							<xsl:text>. </xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Data is ignored.</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>
        /// &lt;/summary&gt;
        </xsl:text>
				<xsl:value-of select="$stripped-name"/>
				<xsl:text> = </xsl:text>
				<xsl:value-of select="$number"/>
        <xsl:if test="$number != 255">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:if test="$number = 44">
          <xsl:text>
        /// &lt;summary&gt;
        /// Identical to SetMaximumPosition, this is just for backward 
        /// compatibility. Request data is Range. Response data is Range. 
        /// &lt;/summary&gt;
        SetMaximumRange = 44,
          </xsl:text>
        </xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:transform>