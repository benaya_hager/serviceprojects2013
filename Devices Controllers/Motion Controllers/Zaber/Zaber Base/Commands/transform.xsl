﻿<xsl:transform version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.springframework.net">
	<xsl:output method="xml" version="4.0" indent="yes" 
		encoding="iso-8859-1" />

	<!-- This transforms the wiki content into the Spring configuration file that
	lists all the commands. See the Readme.txt file for instructions on how to
	download the wiki content. -->
  <xsl:variable name="all-command-details" select="document('CommandDetails.xml')"/>
  <xsl:variable name="ascii-commands" select="document('wikiASCIICommands.html')" />

  <!-- This is the main template that matches the root node. It sets up each
	device type with a list of commands by using the command-list mode, and
	then it generates the commands themselves using the default mode.

	The skip-duplicates and command-list-skip-duplicates are used to generate
	any joystick commands that aren't duplicates of actuator commands. -->
	<xsl:template match="/">
		<objects>
			<object id="actuatorDeviceTypeParent" type="Zaber.DeviceType, Zaber">
				<property name="Commands">
					<list element-type="Zaber.CommandInfo, Zaber">
            <xsl:apply-templates mode="command-list" select="/html/h3">
              <xsl:with-param name="device" select="'motor'"/>
            </xsl:apply-templates>
            <xsl:apply-templates mode="ascii-command-list" select="$ascii-commands/html/h2" />
          </list>
				</property>
			</object>
			<object id="joystickDeviceTypeParent" type="Zaber.DeviceType, Zaber">
				<property name="Commands">
					<list element-type="Zaber.CommandInfo, Zaber">
            <xsl:apply-templates mode="command-list" select="/html/h3">
              <xsl:with-param name="device" select="'joy'"/>
            </xsl:apply-templates>
          </list>
				</property>
			</object>
      <object id="controllerDeviceTypeParent" type="Zaber.DeviceType, Zaber">
        <property name="Commands">
          <list element-type="Zaber.CommandInfo, Zaber">
            <xsl:apply-templates mode="command-list" select="/html/h3">
              <xsl:with-param name="device" select="'controller'"/>
            </xsl:apply-templates>
            <xsl:apply-templates mode="ascii-command-list" select="$ascii-commands/html/h2" />
          </list>
        </property>
      </object>
      <object id="defaultDeviceType" type="Zaber.DeviceType, Zaber">
        <property name="Commands">
          <list element-type="Zaber.CommandInfo, Zaber">
            <xsl:apply-templates mode="command-list" select="/html/h3">
              <xsl:with-param name="device" select="'all'"/>
            </xsl:apply-templates>
            <xsl:apply-templates mode="ascii-command-list" select="$ascii-commands/html/h2" />
          </list>
        </property>
      </object>
      <xsl:apply-templates select="/html/h3"/>
      <xsl:apply-templates mode="ascii-commands" select="$ascii-commands/html/h2" />
		</objects>
	</xsl:template>

	<!-- Do not copy text nodes to output -->
	<xsl:template match="text()"/>
	<xsl:template match="text()" mode="command-list"/>
	<xsl:template match="text()" mode="command-list-skip-duplicates"/>
	<xsl:template match="text()" mode="ascii-command-list"/>
	<xsl:template match="text()" mode="ascii-commands"/>

  <!-- Match command sections. They can be recognized as h3 headings that are 
	immediately followed by a table, so to exclude a section from the command list,
	just add an introductory paragraph before any tables. 
	Mode is command-list, so we just generate object references for all the commands. -->
	<xsl:template match="h3[following-sibling::*[1][name()='table']]" 
		mode="command-list">
    <xsl:param name="device"/>
		<xsl:variable name="summary" select="following-sibling::table[1]"/>
		<xsl:variable name="commandName">
			<xsl:call-template name="table-value">
				<xsl:with-param name="table" select="$summary"/>
				<xsl:with-param name="label" select="'Instruction Name'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="command-id">
			<xsl:call-template name="object-id">
				<xsl:with-param name="command-name" 
					select="$commandName"/>
			</xsl:call-template>
		</xsl:variable>
    <xsl:variable name="command-number">
      <xsl:call-template name="table-value">
        <xsl:with-param name="table" select="$summary"/>
        <xsl:with-param name="label" select="'Command Number'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable
      name="command-details"
      select="$all-command-details/commands/command[@number=$command-number]"/>
    <xsl:if test="not($command-details)">
      <xsl:message terminate="yes">
        <xsl:text>Command details not found for </xsl:text>
        <xsl:value-of select="$commandName"/>
        <xsl:text> - </xsl:text>
        <xsl:value-of select="$command-number"/>
      </xsl:message>
    </xsl:if>
    <xsl:variable name="command-device">
      <xsl:choose>
        <xsl:when test="$command-details/@device">
          <xsl:value-of select="$command-details/@device"/>
        </xsl:when>
        <!-- defaults to 'motor' -->
        <xsl:otherwise>motor</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="
            $command-device = $device or 
            $command-device = 'all' or
            ($command-device = 'motor' and $device = 'controller') or
            $device = 'all'">
      <ref object="{$command-id}"/>
    </xsl:if>
  </xsl:template>

	<!-- Match command sections (see previous explanation). This is the core 
	template that generates the object definition. Several values are pulled
	out of the summary table using the table-value or table-property 
	templates.-->
	<xsl:template match="h3[following-sibling::*[1][name()='table']]">
		<xsl:variable name="summary" select="following-sibling::table[1]"/>
    <xsl:variable name="command-number">
      <xsl:call-template name="table-value">
        <xsl:with-param name="table" select="$summary"/>
        <xsl:with-param name="label" select="'Command Number'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable
      name="command-details"
      select="$all-command-details/commands/command[@number=$command-number]"/>
    <xsl:variable name="commandName">
      <xsl:call-template name="table-value">
        <xsl:with-param name="table" select="$summary"/>
        <xsl:with-param name="label" select="'Instruction Name'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="object-id">
      <xsl:call-template name="object-id">
        <xsl:with-param name="command-name" select="$commandName"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="command-type" select="$command-details/@type"/>
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="$command-type = 'Setting'">Zaber.SettingInfo, Zaber</xsl:when>
        <xsl:when test="$command-type = 'Read-Only Setting'">Zaber.ReadOnlySettingInfo, Zaber</xsl:when>
        <xsl:when test="$command-type = 'Reply'">Zaber.ResponseInfo, Zaber</xsl:when>
        <xsl:when test="$command-type = 'Command'">Zaber.CommandInfo, Zaber</xsl:when>
        <xsl:otherwise>Zaber.CommandInfo, Zaber</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <object id="{$object-id}" type="{$type}">
      <property name="Name" value="{$commandName}"/>
      <property name="Number" value="{$command-number}"/>
      <xsl:call-template name="table-property">
        <xsl:with-param name="table" select="$summary"/>
        <xsl:with-param name="label" select="'Command Data'"/>
        <xsl:with-param name="property-name" select="'DataDescription'"/>
        <xsl:with-param name="is-optional" select="yes"/>
      </xsl:call-template>
      <xsl:call-template name="table-property">
        <xsl:with-param name="table" select="$summary"/>
        <xsl:with-param name="label" select="'Reply Data'"/>
        <xsl:with-param name="property-name" select="'ResponseDescription'"/>
        <xsl:with-param name="is-optional" select="yes"/>
      </xsl:call-template>
      <xsl:if test="$command-details/@returns-position = 'true'">
        <property name="IsCurrentPositionReturned" value="true"/>
      </xsl:if>
      <xsl:if 
        test="not($command-details/@safe-retry) or 
          $command-details/@safe-retry != 'false'">
        <property name="IsRetrySafe" value="true"/>
      </xsl:if>
      <xsl:if test="$command-details/@request-data-type">
        <property 
          name="RequestDataType" 
          value="{$command-details/@request-data-type}"/>
      </xsl:if>
      <xsl:if test="$command-details/@response-data-type">
        <property 
          name="ResponseDataType" 
          value="{$command-details/@response-data-type}"/>
      </xsl:if>
      <xsl:if test="$command-details/@basic">
        <property name="IsBasic" value="{$command-details/@basic}"/>
      </xsl:if>
      <xsl:if test="$command-details/@hide">
        <property name="IsHidden" value="{$command-details/@hide}"/>
      </xsl:if>
      <xsl:if test="$command-details/@fw-add">
        <property name="FirmwareVersionAdded" value="{$command-details/@fw-add}"/>
      </xsl:if>
      <xsl:if test="$command-details/@fw-drop">
        <property name="FirmwareVersionDropped" value="{$command-details/@fw-drop}"/>
      </xsl:if>
      <property name="HelpText">
        <value>
          <xsl:call-template name="table-value">
            <xsl:with-param name="table" select="$summary"/>
            <xsl:with-param name="label" select="'Summary'"/>
          </xsl:call-template>

          <xsl:text> </xsl:text>

          <!-- start processing extra help text. Skip over the summary table. -->
          <xsl:apply-templates mode="help-text" select="following-sibling::*[2]"/>

          <xsl:variable name="note-text">
            <xsl:call-template name="table-value">
              <xsl:with-param name="table" select="$summary"/>
              <xsl:with-param name="label" select="'Note'"/>
            </xsl:call-template>
          </xsl:variable>

          <xsl:if test="$note-text != ''">
            <xsl:text>Note: </xsl:text>
            <xsl:value-of select="$note-text"/>
          </xsl:if>
        </value>
      </property>
    </object>
	</xsl:template>

  <!-- 
  We must identify the sections that we're interested explicitly.
  The commands are started by an H2 that has a containing SPAN with an id of "Command_Refence"
  The settings are started by an H2 that has a containing SPAN with an id of "Device_Settings"
  The commands and settings are wrapped by a DIV with the class "zaber_cmd". Within each DIV,
  commands and settings are started with an H3 which has a SPAN containing the ID of the command 
  or setting.

  So first we find the start of the Commands or Settings, then we find the first h3 which is 
  the start of the command or setting documentation. At that point, all content to the next h3
  is considered data for the help text.

  For the ascii-command-list, all that's required are the IDs so return the list appropriately
  formatted and ignore the rest.
  -->
	<xsl:template match="h2[span[@id='Command_Reference']]" mode="ascii-command-list">
    <xsl:apply-templates mode="ascii-command-list-entries" select="following-sibling::div[1]/h3[1]">
      <xsl:with-param name="type" select="'Zaber.CommandInfo, Zaber'"/>
    </xsl:apply-templates>
  </xsl:template>

	<xsl:template match="h2[span[@id='Device_Settings']]" mode="ascii-command-list">
    <xsl:apply-templates mode="ascii-command-list-entries" select="following-sibling::div[1]/h3[1]">
      <xsl:with-param name="type" select="'Zaber.SettingInfo, Zaber'"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="h3" mode="ascii-command-list-entries">
    <xsl:variable name="object-id" select="normalize-space(span/@id)"/>
    <ref object="{$object-id}" />
    <xsl:for-each
      select="$all-command-details/commands/command[
        starts-with(@text, concat($object-id, ' '))]">
      
      <xsl:variable name="child-id" select="translate(@text, ' ', '-')"/>
      <ref object="{$child-id}" />
    </xsl:for-each>
    <xsl:apply-templates mode="ascii-command-list-entries" select="following-sibling::*[1]"/>
	</xsl:template>

  <xsl:template match="p|dl|ul|ol|h4" mode="ascii-command-list-entries">
		<xsl:apply-templates mode="ascii-command-list-entries" select="following-sibling::*[1]"/>
  </xsl:template>
  <xsl:template match="*" mode="ascii-command-list-entries"></xsl:template>

  <!-- 
  We must identify the sections that we're interested explicitly.
  The commands are started by an H2 that has a containing SPAN with an id of "Command_Refence"
  The settings are started by an H2 that has a containing SPAN with an id of "Device_Settings"
  The commands and settings are wrapped by a DIV with the class "zaber_cmd". Within each DIV,
  commands and settings are started with an H3 which has a SPAN containing the ID of the command 
  or setting.

  So first we find the start of the Commands or Settings, then we find the first h3 which is 
  the start of the command or setting documentation. At that point, all content to the next h3
  is considered data for the help text.

  For the ascii-command, ascii-command-entries does most of the heavy lifting aided by help-text
  to get the content for the help widget in Zaber Console.

  Walking of the siblings is unusual to non-functional programmers. Note that the last line of the
  H3/ascii-command-entries template calls the ascii-command-entries template again. That's effectively
  walking the tree by going to sibling to sibling in a recursive manner.
  -->
	<xsl:template match="h2[span[@id='Command_Reference']]" mode="ascii-commands">
    <xsl:apply-templates mode="ascii-command-entries" select="following-sibling::div[1]/h3[1]">
      <xsl:with-param name="type" select="'Zaber.CommandInfo, Zaber'"/>
    </xsl:apply-templates>
  </xsl:template>

	<xsl:template match="h2[span[@id='Device_Settings']]" mode="ascii-commands">
    <xsl:apply-templates mode="ascii-command-entries" select="following-sibling::div[1]/h3[1]">
      <xsl:with-param name="type" select="'Zaber.SettingInfo, Zaber'"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="h3" mode="ascii-command-entries">
    <xsl:param name="type" />
		<xsl:variable name="object-id" select="normalize-space(span/@id)"/>
    <xsl:variable name="command-text" select="normalize-space(span[@class='mw-headline'])"/>
    <xsl:variable
      name="command-details"
      select="$all-command-details/commands/command[@text=$command-text]"/>
    <object id="{$object-id}" type="{$type}">
      <property name="TextCommand" value="{$command-text}"/>
      <property name="IsBasic" value="true" />
      <xsl:if test="$command-details/@returns-position = 'true'">
        <property name="IsCurrentPositionReturned" value="true"/>
      </xsl:if>
      <xsl:if test="$command-details/@request-data-type">
        <property
          name="RequestDataType"
          value="{$command-details/@request-data-type}"/>
      </xsl:if>
      <xsl:if test="$command-details/@response-data-type">
        <property
          name="ResponseDataType"
          value="{$command-details/@response-data-type}"/>
      </xsl:if>
      <property name="HelpText">
        <value>
          <xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
        </value>
      </property>
    </object>
    <xsl:for-each
      select="$all-command-details/commands/command[
        starts-with(@text, concat($object-id, ' '))]">

      <xsl:variable name="child-id" select="translate(@text, ' ', '-')"/>
      <object id="{$child-id}" parent="{$object-id}">
        <property name="TextCommand" value="{@text}"/>
        <xsl:if test="@returns-position = 'true'">
          <property name="IsCurrentPositionReturned" value="true"/>
        </xsl:if>
        <xsl:if test="@request-data-type">
          <property
            name="RequestDataType"
            value="{@request-data-type}"/>
        </xsl:if>
        <xsl:if test="@response-data-type">
          <property
            name="ResponseDataType"
            value="{@response-data-type}"/>
        </xsl:if>
      </object>
    </xsl:for-each>
		<xsl:apply-templates mode="ascii-command-entries" select="following-sibling::*[1]">
      <xsl:with-param name="type" select="$type"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="p|dl|ul|ol|h4" mode="ascii-command-entries">
    <xsl:param name="type" />
		<xsl:apply-templates mode="ascii-command-entries" select="following-sibling::*[1]">
      <xsl:with-param name="type" select="$type"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*" mode="ascii-command-entries"></xsl:template>

	<!-- The next few sections copy various HTML tags into help text. They all
	end with a call to apply-templates on the following sibling, so that all
	the text until the next section heading will be included in this command's
	help text.
	Paragraphs we just copy straight and then add an extra line break. 
	Pre tags aren't handled very well and should be avoided. -->
	<xsl:template match="p|pre" mode="help-text">
		<xsl:value-of select="."/>
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

	<!-- Subheadings are copied as a separate paragraph with a colon added. -->
	<xsl:template match="h4|h5|h6|h7" mode="help-text">
		<xsl:value-of select="span[@class='mw-headline']"/>
		<xsl:text>:
		
		</xsl:text>
		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

	<!-- Bulleted and numbered lists have all their items copied with an asterisk added 
	before them and a blank line after. -->
	<xsl:template match="ul|ol" mode="help-text">
		<xsl:for-each select="li">
			<xsl:text>* </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text>
			</xsl:text>
		</xsl:for-each>

		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

	<!-- Definition lists have all their items copied with a blank line after. -->
	<xsl:template match="dl" mode="help-text">
    <xsl:apply-templates mode="definition" select="dt|dd"/>
		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

  <!-- Definition terms dump their text plus a colon and blank line. -->
  <xsl:template match="dt" mode="definition">
    <xsl:variable name="txt">
      <xsl:apply-templates mode="definition-text"/>
    </xsl:variable>
    <xsl:value-of select="normalize-space($txt)"/>
    <xsl:text>:

    </xsl:text>
  </xsl:template>
  
  <!-- Definition details just dump their text plus a blank line. -->
  <xsl:template match="dd" mode="definition">
    <xsl:apply-templates mode="definition-text"/>
    <xsl:text>

    </xsl:text>
    <xsl:apply-templates mode="help-text" select="dl"/>
  </xsl:template>

  <xsl:template match="dt|dd|a" mode="definition-text">
    <xsl:apply-templates mode="definition-text"/>
  </xsl:template>

  <xsl:template match="text()" mode="definition-text">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="dl" mode="definition-text">
    <xsl:apply-templates mode="help-text" select="."/>
  </xsl:template>

  <!-- Can't handle tables or images well, so just tell the user to see the wiki. -->
	<xsl:template match="table|div" mode="help-text">
		<!-- Uncomment below to produce warnings -->
		<!--<xsl:message terminate="no">		Warning: Unsupported table/div contents replaced by "For more details, see the wiki or your user manual."
			
			<xsl:value-of select="."/>
		</xsl:message>-->
		<xsl:text>For more details, see the wiki or your user manual.
		
		</xsl:text>
		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

	<!-- Top-level anchors are just used for section headings, so ignore it, 
	but keep looking for more help text. -->
  <xsl:template match="a" mode="help-text">
    <xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
  </xsl:template>
  
  <!-- <br> tags just get replaced with a blank line. -->
  <xsl:template match="br" mode="help-text">
    <xsl:text>
		
		</xsl:text>
    <xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
  </xsl:template>

  <!-- This is only really helpful for debugging. Anything that didn't hit any
	of the other templates will just get copied straight into the file, so you
	can see what got missed. Either change the wiki so it doesn't generate that 
	anymore, or add another template here to handle the new tag. -->
	<xsl:template match="*" mode="help-text">
		<xsl:copy-of select="."/>
		<xsl:text>

		</xsl:text>
		<xsl:apply-templates mode="help-text" select="following-sibling::*[1]"/>
	</xsl:template>

	<!-- You found the end of the command's section, so stop. -->
	<xsl:template match="h3|h2" mode="help-text">
		<!-- do nothing because the next h3 or h2 element breaks you out of 
		help-text mode. -->
	</xsl:template>

	<!-- These are some helper templates.
	This one generates a property tag based on a value from the summary table. -->
	<xsl:template name="table-property">
		<xsl:param name="table"/>
		<xsl:param name="label"/>
		<xsl:param name="property-name"/>
		<xsl:param name="is-optional" select="no"/>

		<xsl:variable name="value">
			<xsl:call-template name="table-value">
				<xsl:with-param name="table" select="$table"/>
				<xsl:with-param name="label" select="$label"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="is-wanted">
			<xsl:choose>
				<xsl:when test="$is-optional = 'no'">yes</xsl:when>
				<xsl:when test="not($value)">no</xsl:when>
				<xsl:when test="starts-with($value, 'Ignored')">no</xsl:when>
				<xsl:when test="starts-with($value, 'ignored')">no</xsl:when>
				<xsl:when test="starts-with($value, 'n/a')">no</xsl:when>
				<xsl:when test="starts-with($value, 'none')">no</xsl:when>
				<xsl:when test="starts-with($value, 'None')">no</xsl:when>
				<xsl:when test="$value = '0'">no</xsl:when>
				<xsl:when test="$value = ''">no</xsl:when>
				<xsl:otherwise>yes</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$is-wanted = 'yes'">
			<property name="{$property-name}" value="{$value}"/>
		</xsl:if>
	</xsl:template>

	<!-- Grab an entry from the summary table. -->
	<xsl:template name="table-value">
		<xsl:param name="table"/>
		<xsl:param name="label"/>

		<xsl:variable name="row" select="$table/tr[normalize-space(th)=$label]"/>
		<xsl:value-of select="normalize-space($row/td)"/>
	</xsl:template>

	<!-- Generate the object id based on the command name. -->
	<xsl:template name="object-id">
		<xsl:param name="command-name"/>

		<!-- strip spaces out of the name -->
		<xsl:variable name="stripped" select="translate($command-name, ' -/', '')"/>

		<!-- make the first letter lower case -->
		<xsl:variable name="first" select="substring($stripped, 1, 1)"/>
		<xsl:variable name="after-first" select="substring($stripped, 2)"/>
		<xsl:variable name="lowered-first" 
			select="translate($first, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
		<xsl:variable name="object-id" select="concat($lowered-first, $after-first)"/>

		<!-- Add "Command" to the end and return the result. -->
		<xsl:value-of select="$object-id"/>
		<xsl:text>Command</xsl:text>
	</xsl:template>
</xsl:transform>
