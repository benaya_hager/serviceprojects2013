﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Different kinds of messages that can be sent or received.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// Message in binary protocol, request or response.
        /// </summary>
        Binary,
        /// <summary>
        /// Message in text protocol being sent to the chain of devices.
        /// </summary>
        Request,
        /// <summary>
        /// Message in text protocol being sent from a device back to the 
        /// caller in reply to a request.
        /// </summary>
        Response,
        /// <summary>
        /// Message in text protocol being sent from a device back to the 
        /// caller without a request.
        /// </summary>
        Alert,
        /// <summary>
        /// Extra information in text protocol being sent from a device back 
        /// to the caller.
        /// </summary>
        Comment,
        /// <summary>
        /// Message in text protocol that started with an unexpected character.
        /// </summary>
        Unknown
    }
}
