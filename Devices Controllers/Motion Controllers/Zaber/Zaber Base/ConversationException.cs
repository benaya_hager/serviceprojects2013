using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Zaber
{
    /// <summary>
    /// This is a base class for all the exceptions that the 
    /// <see cref="Conversation"/> class throws. It lets you simplify your 
    /// catch blocks if you want to treat all exceptions the same.
    /// </summary>
    [Serializable]
    public class ConversationException : Exception
    {
        public ConversationException()
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public ConversationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this one.</param>
        public ConversationException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected ConversationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
