﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// Record ratios for converting between different units of measure.
    /// </summary>
    /// <remarks>
    /// As well as the usual conversion ratio, you can also include the step
    /// size of a Zaber device in the conversions.
    /// </remarks>
    public class ConversionTable
    {
        /// <summary>
        /// A conversion table that contains all the known linear position 
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable LinearPosition =
            new ConversionTable()
                .AddConversion(
                    UnitOfMeasure.Inch,
                    UnitOfMeasure.Millimeter,
                    25.4m)
                .AddConversion(
                    UnitOfMeasure.Millimeter,
                    UnitOfMeasure.Micrometer,
                    1000m);

        /// <summary>
        /// A conversion table that contains all the known linear position 
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable RotaryPosition =
            new ConversionTable()
                .AddConversion(
                    UnitOfMeasure.Revolution,
                    UnitOfMeasure.Degree,
                    360m)
                .AddConversion(
                    UnitOfMeasure.Revolution,
                    UnitOfMeasure.Radian,
                    2 * (decimal)Math.PI);

        /// <summary>
        /// A conversion table that contains all the known linear velocity
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable LinearVelocity =
            new ConversionTable(
                LinearPosition,
                UnitOfMeasure.Micrometer,
                UnitOfMeasure.MicrometersPerSecond,
                UnitOfMeasure.Millimeter,
                UnitOfMeasure.MillimetersPerSecond,
                UnitOfMeasure.Inch,
                UnitOfMeasure.InchesPerSecond);

        /// <summary>
        /// A conversion table that contains all the known rotary velocity
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable RotaryVelocity =
            new ConversionTable()
                .AddConversion(
                    UnitOfMeasure.RevolutionsPerMinute,
                    UnitOfMeasure.DegreesPerSecond,
                    6m)
                .AddConversion(
                    UnitOfMeasure.RadiansPerSecond,
                    UnitOfMeasure.DegreesPerSecond,
                    180 / (decimal)Math.PI);

        /// <summary>
        /// A conversion table that contains all the known linear acceleration
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable LinearAcceleration =
            new ConversionTable(
                LinearPosition,
                UnitOfMeasure.Micrometer,
                UnitOfMeasure.MicrometersPerSecondSquared,
                UnitOfMeasure.Millimeter,
                UnitOfMeasure.MillimetersPerSecondSquared,
                UnitOfMeasure.Inch,
                UnitOfMeasure.InchesPerSecondSquared);

        /// <summary>
        /// A conversion table that contains all the known rotary acceleration
        /// units of measure.
        /// </summary>
        public static readonly ConversionTable RotaryAcceleration =
            new ConversionTable().AddConversion(
                UnitOfMeasure.RadiansPerSecondSquared,
                UnitOfMeasure.DegreesPerSecondSquared,
                180 / (decimal)Math.PI);

        private readonly UnitOfMeasure[] units;
        private readonly decimal[,] ratios;

        /// <summary>
        /// Initialize a new, empty conversion table.
        /// </summary>
        public ConversionTable()
        {
            units = new UnitOfMeasure[0];
            ratios = new decimal[0, 0];
        }

        /// <summary>
        /// Initialize a new conversion table with only one unit in it.
        /// </summary>
        /// <param name="unit">The unit of measure to put in the table.</param>
        public ConversionTable(UnitOfMeasure unit)
        {
            units = new UnitOfMeasure[] { unit };
            ratios = new decimal[,] { { 1m } };
        }

        /// <summary>
        /// Initialize a new conversion table by copying another table, and
        /// mapping all the units of the old table to new units while keeping
        /// all the conversion ratios intact.
        /// </summary>
        /// <param name="source">The table to copy</param>
        /// <param name="unitMap">Pairs of units with the old unit followed by
        /// the new unit</param>
        /// <remarks>For example, this could be used to generate a conversion
        /// table for velocities with the same ratios as the positions. You
        /// could pass the units like this: mm, mm/s, in, in/s.</remarks>
        public ConversionTable(
            ConversionTable source,
            params UnitOfMeasure[] unitMap)
        {
            var expectedLength = source.units.Length * 2;
            if (unitMap.Length != expectedLength)
            {
                throw new ArgumentException(
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "Conversion table expected {0} units of measure, but got {1}.",
                        expectedLength,
                        unitMap.Length),
                    "unitMap");
            }
            ratios = source.ratios;
            units = new UnitOfMeasure[source.units.Length];
            for (int i = 0; i < units.Length; i++)
            {
                var sourceUnit = unitMap[2*i];
                var index = Array.IndexOf(source.units, sourceUnit);
                if (index < 0)
                {
                    throw new ArgumentException(
                        String.Format(
                            CultureInfo.CurrentCulture,
                            "Unknown unit of measure: {0}.",
                            sourceUnit),
                        "unitMap");
                }
                units[index] = unitMap[2 * i + 1];
            }
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i] == null)
                {
                    throw new ArgumentException(
                        String.Format(
                            CultureInfo.CurrentCulture,
                            "Unmapped unit of measure: {0}.",
                            source.units[i]),
                        "unitMap");
                }
            }
        }

        /// <summary>
        /// Initialize a new conversion table by copying another table, then
        /// adding new conversions.
        /// </summary>
        /// <param name="source">The table to copy</param>
        /// <param name="fromUnit">The unit of measure to convert from.</param>
        /// <param name="toUnit">The unit of measure to convert to.</param>
        /// <param name="ratio">One from unit is equal to this many to 
        /// units.</param>
        public ConversionTable(
            ConversionTable source,
            UnitOfMeasure fromUnit,
            UnitOfMeasure toUnit,
            decimal ratio)
        {
            if (ratio == 0)
            {
                throw new ArgumentOutOfRangeException(
                    "ratio",
                    "Conversion ratio cannot be zero.");
            }
            int unitCount = source.units.Length;
            int fromIndex = Array.IndexOf(source.units, fromUnit);
            int toIndex = Array.IndexOf(source.units, toUnit);
            if (fromIndex < 0)
            {
                unitCount++;
            }
            if (toIndex < 0)
            {
                unitCount++;
            }
            units = new UnitOfMeasure[unitCount];
            ratios = new decimal[unitCount, unitCount];
            int i;
            for (i = 0; i < source.units.Length; i++)
            {
                units[i] = source.units[i];
                for (int j = 0; j < source.units.Length; j++)
                {
                    ratios[i, j] = source.ratios[i, j];
                }
            }
            if (fromIndex < 0)
            {
                fromIndex = i++;
                units[fromIndex] = fromUnit;
            }
            if (toIndex < 0)
            {
                toIndex = i++;
                units[toIndex] = toUnit;
            }
            CopyRatios(source, fromIndex, toIndex, ratio);
        }

        /// <summary>
        /// Copy the ratios from the source table, and add any new conversions.
        /// </summary>
        /// <param name="source">The table to copy from.</param>
        /// <param name="fromIndex">The index of the from unit in the new 
        /// table.</param>
        /// <param name="toIndex">The index of the to unit in the new 
        /// table.</param>
        /// <param name="ratio">One from unit is equal to this many to 
        /// units.</param>
        private void CopyRatios(
            ConversionTable source, 
            int fromIndex, 
            int toIndex, 
            decimal ratio)
        {
            ratios[fromIndex, toIndex] = ratio;
            ratios[toIndex, fromIndex] = 1 / ratio;
            ratios[fromIndex, fromIndex] = ratios[toIndex, toIndex] = 1;
            if (fromIndex < source.units.Length)
            {
                for (int j = 0; j < source.units.Length; j++)
                {
                    ratios[j, toIndex] = ratios[j, fromIndex] * ratio;
                    ratios[toIndex, j] = 1 / ratios[j, toIndex];
                }
            }
            if (toIndex < source.units.Length)
            {
                for (int j = 0; j < source.units.Length; j++)
                {
                    ratios[fromIndex, j] = ratio * ratios[toIndex, j];
                    ratios[j, fromIndex] = 1 / ratios[fromIndex, j];
                }
            }
        }

        /// <summary>
        /// Create a new conversion table with a conversion added between two 
        /// units of measure.
        /// </summary>
        /// <param name="fromUnit">The unit to convert from.</param>
        /// <param name="toUnit">The unit to convert to.</param>
        /// <param name="ratio">Multiply one fromUnit by this ratio to convert
        /// it to toUnits.</param>
        /// <example>
        /// <code>
        /// conversionTable.AddConversion(inch, mm, 25.4);
        /// </code>
        /// <remarks>One inch is equal to 25.4 mm.</remarks>
        /// </example>
        /// <returns>A new conversion table with the conversion added.</returns>
        public ConversionTable AddConversion(
            UnitOfMeasure fromUnit, 
            UnitOfMeasure toUnit, 
            decimal ratio)
        {
            return new ConversionTable(this, fromUnit, toUnit, ratio);
        }

        /// <summary>
        /// Convert a measurement to another unit of measure.
        /// </summary>
        /// <param name="from">The existing measurement.</param>
        /// <param name="toUnit">The unit of measure to convert it to.</param>
        /// <returns>A new measurement with the requested unit of 
        /// measure.</returns>
        /// <exception cref="ArgumentException">If either unit of measure 
        /// isn't in the table.</exception>
        public Measurement Convert(
            Measurement from, 
            UnitOfMeasure toUnit)
        {
            int fromIndex = Array.IndexOf(units, from.Unit);
            int toIndex = Array.IndexOf(units, toUnit);
            if (fromIndex < 0)
            {
                throw new ArgumentException(
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "Unknown unit of measure: {0}.", 
                        from.Unit),
                    "from");
            }
            if (toIndex < 0)
            {
                throw new ArgumentException(
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "Unknown unit of measure: {0}.", 
                        toUnit),
                    "toUnit");
            }
            var ratio = ratios[fromIndex, toIndex];
            if (ratio == 0)
            {
                throw new InvalidOperationException(
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "Cannot convert from {0} to {1}.",
                        from.Unit,
                        toUnit));
            }
            return new Measurement(from.Value * ratio, toUnit);
        }

        /// <summary>
        /// Gets an unsorted list of the units of measure used in this table.
        /// </summary>
        public IEnumerable<UnitOfMeasure> UnitsOfMeasure
        {
            get
            {
                IEnumerable<UnitOfMeasure> foo = units;
                foo.Count();
                return units;
            }
        }

        /// <summary>
        /// Returns a System.String that represents the conversion table.
        /// </summary>
        /// <returns>A System.String that represents the unit of measure 
        /// abbreviations in this conversion table.</returns>
        public override string ToString()
        {
            return String.Join(
                ", ", 
                units.Select(u => u.Abbreviation).ToArray());
        }
    }
}
