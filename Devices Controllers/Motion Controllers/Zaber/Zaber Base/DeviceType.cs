using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Zaber
{
    /// <summary>
    /// Describes the type of a <see cref="ZaberDevice"/>.
    /// </summary>
    /// <remarks>The device type determines the list of supported commands.</remarks>
    public class DeviceType
    {
        private int deviceId;
        private IList<CommandInfo> commands;
        private IDictionary<Command, CommandInfo> commandMap;
        private IDictionary<String, CommandInfo> textMap;
        private string name;
        private IList<DeviceType> peripherals;
        private IDictionary<int, DeviceType> peripheralMap;
        private Measurement stepSize;
        private Measurement distanceToAxis;

        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        public DeviceType()
        {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">copy details from this device type</param>
        public DeviceType(DeviceType source)
        {
            deviceId = source.deviceId;
            PeripheralId = source.PeripheralId;
            peripherals = source.peripherals;
            peripheralMap = source.peripheralMap;
            Commands = source.Commands;
            name = source.name;
            MotionType = source.MotionType;
            if (source.StepSize != null)
            {
                StepSize = new Measurement(source.StepSize);
            }
            if (source.DistanceToAxis != null)
            {
                DistanceToAxis = new Measurement(source.DistanceToAxis);
            }
        }

        /// <summary>
        /// A custom name to display to the user.
        /// </summary>
        /// <seealso cref="ToString"/>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets a list of the commands that this device type supports.
        /// </summary>
        [SuppressMessage(
            "Microsoft.Usage",
            "CA2227:CollectionPropertiesShouldBeReadOnly",
            Justification = "Needed for Spring configuration.")]
        public IList<CommandInfo> Commands
        {
            get { return commands; }
            set
            {
                // use a read-only copy to avoid insertions or deletions
                commands = new List<CommandInfo>(value).AsReadOnly();

                commandMap = new Dictionary<Command, CommandInfo>();
                textMap = new Dictionary<String, CommandInfo>();
                foreach (CommandInfo command in commands)
                {
                    if (command.TextCommand == null)
                    {
                        if (commandMap.ContainsKey(command.Command))
                        {
                            throw new ArgumentException(
                                String.Format(
                                    CultureInfo.CurrentCulture,
                                    "More than one entry found for command {0}.",
                                    command.Command),
                                "value");
                        }
                        commandMap.Add(command.Command, command);
                    }
                    else
                    {
                        textMap.Add(command.TextCommand, command);
                    }
                }

                if (peripherals != null)
                {
                    foreach (DeviceType peripheralType in peripherals)
                    {
                        peripheralType.Commands = Commands;
                    }
                }
            }
        }

        /// <summary>
        /// A list of the peripherals that this device type supports.
        /// </summary>
        [SuppressMessage(
            "Microsoft.Usage",
            "CA2227:CollectionPropertiesShouldBeReadOnly",
            Justification = "Needed for Spring configuration.")]
        public IList<DeviceType> Peripherals
        {
            get { return peripherals; }
            set
            {
                if (value == null)
                {
                    peripherals = null;
                    peripheralMap = null;
                    return;
                }
                // use a read-only copy to avoid insertions or deletions
                peripherals = new List<DeviceType>(value).AsReadOnly();

                peripheralMap = new Dictionary<int, DeviceType>();
                foreach (DeviceType peripheralType in peripherals)
                {
                    if (peripheralMap.ContainsKey(peripheralType.PeripheralId))
                    {
                        throw new ArgumentException(
                            String.Format(
                                CultureInfo.CurrentCulture,
                                "More than one entry found for peripheral id {0}.",
                                peripheralType.PeripheralId),
                            "value");
                    }
                    peripheralMap.Add(peripheralType.PeripheralId, peripheralType);
                    peripheralType.DeviceId = deviceId;

                    // Modify peripheral type's details to match the controller.
                    peripheralType.Name = String.Format(
                        CultureInfo.CurrentCulture,
                        "{0} + {1}",
                        Name,
                        peripheralType.Name);
                    if (Commands != null)
                    {
                        peripheralType.Commands = Commands;
                    }
                }
            }
        }
        
        /// <summary>
        /// The identifier for this device type.
        /// </summary>
        public int DeviceId
        {
            get { return deviceId; }
            set { deviceId = value; }
        }

        /// <summary>
        /// The identifier for a specific peripheral if this device type is customized
        /// for a peripheral.
        /// </summary>
        public int PeripheralId { get; set; }

        /// <summary>
        /// Returns the <see cref="Name"/> or the <see cref="DeviceId"/>.
        /// </summary>
        /// <returns>A descriptive string that can be displayed to a user.</returns>
        public override string ToString()
        {
            return name != null 
                ? name 
                : String.Format(
                    CultureInfo.CurrentUICulture, 
                    "Zaber DeviceId {0}", 
                    deviceId);
        }

        /// <summary>
        /// Look up command details from the list of supported commands.
        /// </summary>
        /// <param name="command">The command number to look up</param>
        /// <returns>The command details, or null if the command is not
        /// supported by this device type.</returns>
        public CommandInfo GetCommandByNumber(Command command)
        {
            CommandInfo commandInfo;
            if (commandMap != null && 
                commandMap.TryGetValue(command, out commandInfo))
            {
                return commandInfo;
            }
            return null;
        }

        /// <summary>
        /// Look up command details from the list of supported commands.
        /// </summary>
        /// <param name="text">The command name to look up.</param>
        /// <returns>The command details, or null if the command is not
        /// supported by this device type.</returns>
        public CommandInfo GetCommandByText(string text)
        {
            CommandInfo commandInfo;
            String key =
                text.StartsWith("get ", StringComparison.Ordinal) || 
                text.StartsWith("set ", StringComparison.Ordinal)
                ? text.Substring(4)
                : text;
            if (textMap != null &&
                textMap.TryGetValue(key, out commandInfo))
            {
                return commandInfo;
            }
            return null;
        }

        /// <summary>
        /// Look up peripheral type from the list of supported peripherals.
        /// </summary>
        /// <param name="peripheralId">The peripheral id to look up</param>
        /// <returns>A device type to use.</returns>
        public DeviceType GetPeripheralById(int peripheralId)
        {
            if (peripheralMap == null || !peripheralMap.ContainsKey(peripheralId))
            {
                return null;
            }
            return peripheralMap[peripheralId];
        }

        /// <summary>
        /// Gets or sets the measured size of one full step for this device type.
        /// </summary>
        public Measurement StepSize 
        {
            get { return stepSize; }
            set
            {
                if (distanceToAxis != null && distanceToAxis.Unit != value.Unit)
                {
                    throw new ArgumentException(String.Format(
                        CultureInfo.CurrentUICulture,
                        "Step size units must match {0} on distance to axis.",
                        distanceToAxis.Unit));
                }
                stepSize = value;
            }
        }

        /// <summary>
        /// Get or sets the distance between the actuator's line of motion and
        /// the axis of rotation.
        /// </summary>
        /// <remarks>This is set on devices that use a linear actuator to cause
        /// rotation. This property and <see cref="StepSize"/> must use the 
        /// same unit of measure.</remarks>
        public Measurement DistanceToAxis
        {
            get { return distanceToAxis; }
            set
            {
                if (stepSize != null && stepSize.Unit != value.Unit)
                {
                    throw new ArgumentException(String.Format(
                        CultureInfo.CurrentUICulture,
                        "Distance to axis units must match {0} on step size.",
                        stepSize.Unit));
                }
                distanceToAxis = value;
            }
        }

        /// <summary>
        /// Gets or sets whether this device type uses linear, rotary, or some 
        /// other type of motion.
        /// </summary>
        public MotionType MotionType { get; set; }
    }
}
