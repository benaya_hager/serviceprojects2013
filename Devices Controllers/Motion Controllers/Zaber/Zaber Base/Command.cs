using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Named constants for the common commands that most T-Series devices 
    /// will support.
    /// </summary>
    /// <remarks>
    /// <c>Command</c> values and <c>byte</c> values are interchangeable,
    /// but you do need an explicit cast to convert from one to the other.
    /// </remarks>
    public enum Command : byte
    {
        /// <summary>
        /// Data is ignored.
        /// </summary>
        Reset = 0,
        /// <summary>
        /// Response data is Final Position. 
        /// </summary>
        Home = 1,
        /// <summary>
        /// Request data is New Number. Response data is Device Id. 
        /// </summary>
        Renumber = 2,
        /// <summary>
        /// Request data is Register Address. Response data is Data. 
        /// </summary>
        ReadRegister = 5,
        /// <summary>
        /// Request data is Register Address. Response data is Register Address. 
        /// </summary>
        SetActiveRegister = 6,
        /// <summary>
        /// Request data is Data. Response data is Data. 
        /// </summary>
        WriteRegister = 7,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        MoveTracking = 8,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        LimitActive = 9,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        ManualMoveTracking = 10,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        ManualMove = 11,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        SlipTracking = 12,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        UnexpectedPosition = 13,
        /// <summary>
        /// Request data is Address. Response data is Address. 
        /// </summary>
        StoreCurrentPosition = 16,
        /// <summary>
        /// Request data is Address. Response data is Stored Position. 
        /// </summary>
        ReturnStoredPosition = 17,
        /// <summary>
        /// Request data is Address. Response data is Final Position. 
        /// </summary>
        MoveToStoredPosition = 18,
        /// <summary>
        /// Request data is Absolute Position. Response data is Final position. 
        /// </summary>
        MoveAbsolute = 20,
        /// <summary>
        /// Request data is Relative Position. Response data is Final Position. 
        /// </summary>
        MoveRelative = 21,
        /// <summary>
        /// Request data is Speed. Response data is Speed. 
        /// </summary>
        MoveAtConstantSpeed = 22,
        /// <summary>
        /// Response data is Final Position. 
        /// </summary>
        Stop = 23,
        /// <summary>
        /// Request data is Axis. Response data is Axis. 
        /// </summary>
        SetActiveAxis = 25,
        /// <summary>
        /// Request data is Device Number. Response data is Device Number. 
        /// </summary>
        SetAxisDeviceNumber = 26,
        /// <summary>
        /// Request data is Invert Status. Response data is Invert Status. 
        /// </summary>
        SetAxisInversion = 27,
        /// <summary>
        /// Request data is Profile Number. Response data is Profile Number. 
        /// </summary>
        SetAxisVelocityProfile = 28,
        /// <summary>
        /// Request data is Maximum Velocity. Response data is Maximum Velocity. 
        /// </summary>
        SetAxisVelocityScale = 29,
        /// <summary>
        /// Request data is Key Event. Response data is Key Event. 
        /// </summary>
        LoadEventInstruction = 30,
        /// <summary>
        /// Request data is Key Event. 
        /// </summary>
        ReturnEventInstruction = 31,
        /// <summary>
        /// Request data is Calibration Mode. Response data is Calibration Mode. 
        /// </summary>
        SetCalibrationMode = 33,
        /// <summary>
        /// Request data is Data. Response data is Data. 
        /// </summary>
        ReadOrWriteMemory = 35,
        /// <summary>
        /// Request data is Peripheral Id. Response data is Peripheral Id. 
        /// </summary>
        RestoreSettings = 36,
        /// <summary>
        /// Request data is Microsteps. Response data is Microsteps. 
        /// </summary>
        SetMicrostepResolution = 37,
        /// <summary>
        /// Request data is Value. Response data is Value. 
        /// </summary>
        SetRunningCurrent = 38,
        /// <summary>
        /// Request data is Value. Response data is Value. 
        /// </summary>
        SetHoldCurrent = 39,
        /// <summary>
        /// Request data is Mode. Response data is Mode. 
        /// </summary>
        SetDeviceMode = 40,
        /// <summary>
        /// Request data is Speed. Response data is Speed. 
        /// </summary>
        SetHomeSpeed = 41,
        /// <summary>
        /// Request data is Speed. Response data is Speed. 
        /// </summary>
        SetTargetSpeed = 42,
        /// <summary>
        /// Request data is Acceleration. Response data is Acceleration. 
        /// </summary>
        SetAcceleration = 43,
        /// <summary>
        /// Request data is Position. Response data is Position. 
        /// </summary>
        SetMaximumPosition = 44,
        /// <summary>
        /// Identical to SetMaximumPosition, this is just for backward 
        /// compatibility. Request data is Range. Response data is Range. 
        /// </summary>
        SetMaximumRange = 44,

        /// <summary>
        /// Request data is New Position. Response data is New Position. 
        /// </summary>
        SetCurrentPosition = 45,
        /// <summary>
        /// Request data is Range. Response data is Range. 
        /// </summary>
        SetMaximumRelativeMove = 46,
        /// <summary>
        /// Request data is Offset. Response data is Offset. 
        /// </summary>
        SetHomeOffset = 47,
        /// <summary>
        /// Request data is Alias Number. Response data is Alias Number. 
        /// </summary>
        SetAliasNumber = 48,
        /// <summary>
        /// Request data is Lock State. Response data is Lock State. 
        /// </summary>
        SetLockState = 49,
        /// <summary>
        /// Response data is Device Id. 
        /// </summary>
        ReturnDeviceId = 50,
        /// <summary>
        /// Response data is Version. 
        /// </summary>
        ReturnFirmwareVersion = 51,
        /// <summary>
        /// Response data is Voltage. 
        /// </summary>
        ReturnPowerSupplyVoltage = 52,
        /// <summary>
        /// Request data is Setting Number. Response data is Setting Value. 
        /// </summary>
        ReturnSetting = 53,
        /// <summary>
        /// Response data is Status. 
        /// </summary>
        ReturnStatus = 54,
        /// <summary>
        /// Request data is Data. Response data is Data. 
        /// </summary>
        EchoData = 55,
        /// <summary>
        /// Response data is Position. 
        /// </summary>
        ReturnCurrentPosition = 60,
        /// <summary>
        /// Request data is Park State. Response data is Park State. 
        /// </summary>
        SetParkState = 65,
        /// <summary>
        /// Request data is Peripheral Id. Response data is Peripheral Id. 
        /// </summary>
        SetPeripheralId = 66,
        /// <summary>
        /// Request data is Auto-Reply Disabled Mode. Response data is Auto-Reply Disabled Mode. 
        /// </summary>
        SetAutoReplyDisabledMode = 101,
        /// <summary>
        /// Request data is Message Id Mode. Response data is Message Id Mode. 
        /// </summary>
        SetMessageIdMode = 102,
        /// <summary>
        /// Request data is Home Status. Response data is Home Status. 
        /// </summary>
        SetHomeStatus = 103,
        /// <summary>
        /// Request data is Home Sensor Type. Response data is Home Sensor Type. 
        /// </summary>
        SetHomeSensorType = 104,
        /// <summary>
        /// Request data is Auto-Home Disabled Mode. Response data is Auto-Home Disabled Mode. 
        /// </summary>
        SetAutoHomeDisabledMode = 105,
        /// <summary>
        /// Request data is Minimum Position. Response data is Minimum Position. 
        /// </summary>
        SetMinimumPosition = 106,
        /// <summary>
        /// Request data is Knob Disabled Mode. Response data is Knob Disabled Mode. 
        /// </summary>
        SetKnobDisabledMode = 107,
        /// <summary>
        /// Request data is Knob Direction. Response data is Knob Direction. 
        /// </summary>
        SetKnobDirection = 108,
        /// <summary>
        /// Request data is Movement Mode. Response data is Movement Mode. 
        /// </summary>
        SetKnobMovementMode = 109,
        /// <summary>
        /// Request data is Jog Size. Response data is Jog Size. 
        /// </summary>
        SetKnobJogSize = 110,
        /// <summary>
        /// Request data is Maximum Speed. Response data is Maximum Speed. 
        /// </summary>
        SetKnobVelocityScale = 111,
        /// <summary>
        /// Request data is Profile Number. Response data is Profile Number. 
        /// </summary>
        SetKnobVelocityProfile = 112,
        /// <summary>
        /// Request data is Acceleration. Response data is Acceleration. 
        /// </summary>
        SetAccelerationOnly = 113,
        /// <summary>
        /// Request data is Deceleration. Response data is Deceleration. 
        /// </summary>
        SetDecelerationOnly = 114,
        /// <summary>
        /// Request data is Move Tracking Mode. Response data is Move Tracking Mode. 
        /// </summary>
        SetMoveTrackingMode = 115,
        /// <summary>
        /// Request data is Manual Move Tracking Disabled Mode. Response data is Manual Move Tracking Disabled Mode. 
        /// </summary>
        SetManualMoveTrackingDisabledMode = 116,
        /// <summary>
        /// Request data is Tracking Period in ms. Response data is Tracking Period in ms. 
        /// </summary>
        SetMoveTrackingPeriod = 117,
        /// <summary>
        /// Request data is Closed-Loop Mode. Response data is Closed-Loop Mode. 
        /// </summary>
        SetClosedLoopMode = 118,
        /// <summary>
        /// Request data is Tracking Period in ms. Response data is Tracking Period in ms. 
        /// </summary>
        SetSlipTrackingPeriod = 119,
        /// <summary>
        /// Request data is Stall Timeout in ms. Response data is Stall Timeout in ms. 
        /// </summary>
        SetStallTimeout = 120,
        /// <summary>
        /// Request data is Device Direction. Response data is Device Direction. 
        /// </summary>
        SetDeviceDirection = 121,
        /// <summary>
        /// Request data is Baudrate. Response data is Baudrate. 
        /// </summary>
        SetBaudrate = 122,
        /// <summary>
        /// Request data is Protocol. Response data is Protocol. 
        /// </summary>
        SetProtocol = 123,
        /// <summary>
        /// Request data is Baudrate. Response data is Baudrate. 
        /// </summary>
        ConvertToAscii = 124,
        /// <summary>
        /// Response data is Error Code. 
        /// </summary>
        Error = 255
    }
}
