using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Descriptive information about a read-only setting that Zaber devices 
    /// support.
    /// </summary>
    /// <remarks>This class is specific to commands that return the value of a 
    /// read-only device setting.</remarks>
    public class ReadOnlySettingInfo : SettingInfo
    {
        /// <summary>
        /// Is this command actually a read-only setting?
        /// </summary>
        /// <remarks>
        /// Read-only settings are read differently from other settings. Just
        /// send the command and the value will be returned.
        /// </remarks>
        public override bool IsReadOnlySetting { get { return true; } }
    }
}
