using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// These error types are copied from <c>System.IO.Ports.SerialError</c>,
    /// with the addition of <see cref="PacketTimeout"/> and 
    /// <see cref="InvalidPacket"/>. They are used
    /// by <see cref="TSeriesPort"/> to either delegate the <c>SerialPort</c>'s
    /// <c>ErrorReceived</c> event, or to raise its own 
    /// <see cref="TSeriesPort.ErrorReceived"/> event when a partial packet
    /// times out, or someone calls 
    /// <see cref="TSeriesPort.ReportInvalidPacket"/>.
    /// </summary>
    public enum ZaberPortError
    {
        /// <summary>
        /// No error occurred.
        /// </summary>
        None,
        /// <summary>
        /// An input buffer overflow has occurred. There is either no room in 
        /// the input buffer, or a character was received after the end-of-file 
        /// (EOF) character. 
        /// </summary>
        RXOver = 1,
        /// <summary>
        /// A character-buffer overrun has occurred. The next character is lost. 
        /// </summary>
        Overrun = 2,
        /// <summary>
        /// The hardware detected a parity error. 
        /// </summary>
        RXParity = 4,
        /// <summary>
        /// The hardware detected a framing error.
        /// </summary>
        Frame = 8,
        /// <summary>
        /// The application tried to transmit a character, but the output 
        /// buffer was full. 
        /// </summary>
        TXFull = 0x100,
        /// <summary>
        /// A partial packet was received, but not completed before the timeout
        /// expired.
        /// </summary>
        PacketTimeout = 0x200,
        /// <summary>
        /// A packet was received, but it was reported as invalid.
        /// </summary>
        InvalidPacket = 0x400,
        /// <summary>
        /// The checksum value did not match the packet contents.
        /// </summary>
        InvalidChecksum = 0x800,
        /// <summary>
        /// The receiving thread threw an exception. See the application log
        /// for details.
        /// </summary>
        ReceiveException = 0x1000
    }
}
