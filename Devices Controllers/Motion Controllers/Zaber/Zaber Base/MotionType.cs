﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Different kinds of motion provided by Zaber devices.
    /// </summary>
    /// <remarks>This is used to help decide which units of measure are 
    /// supported for each device type.</remarks>
    public enum MotionType
    {
        /// <summary>
        /// Other kinds of motion that don't support unit of measure conversion.
        /// For example, the mirror mount.
        /// </summary>
        Other,
        /// <summary>
        /// Motion in a straight line
        /// </summary>
        Linear,
        /// <summary>
        /// Motion in a circle
        /// </summary>
        Rotary
    }
}
