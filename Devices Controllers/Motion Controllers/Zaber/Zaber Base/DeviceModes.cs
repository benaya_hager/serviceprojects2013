using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Bits used to set the flags in the <see cref="Command.SetDeviceMode"/>
    /// command.
    /// </summary>
    [Flags]
    public enum DeviceModes
    {
        /// <summary>
        /// No modes are enabled
        /// </summary>
        None = 0,
        /// <summary>
        /// Disables ALL replies except those to "return" commands (commands 50 and higher).
        /// </summary>
        DisableAutoReply = 0x1,
        /// <summary>
        /// Enables anti-backlash. On negative moves (retracting), the device 
        /// will overshoot the desired position by 640 microsteps (assuming 
        /// 64 microsteps/step), reverse direction and approach the requested 
        /// position from below. On positive moves (extending), the device 
        /// behaves normally. Care must be taken not to crash the moving 
        /// payload into a fixed object due to the 640 microsteps overshoot on 
        /// negative moves.
        /// </summary>
        EnableAntiBacklashRoutine = 0x2,
        /// <summary>
        /// Enables the anti-sticktion routine. On moves less than 640 
        /// microsteps (assuming 64 microsteps/step), the device will first 
        /// retract to a position 640 microsteps less than the requested 
        /// position and approach the requested position from below. Care must 
        /// be taken not to crash the moving payload into a fixed object due 
        /// to the 640 microsteps negative move.
        /// </summary>
        EnableAntiSticktionRoutine = 0x4,
        /// <summary>
        /// Disables the potentiometer preventing manual adjustment of the device.
        /// </summary>
        DisablePotentiometer = 0x8,
        /// <summary>
        /// Enables the Move Tracking response during move commands. The device
        /// will return its position periodically when a move
        /// command is executed. The Disable Auto-Reply option above takes 
        /// precedence over this option. The default is off on all devices.
        /// Before firmware version 5.14, only Move at Constant Speed commands 
        /// could generate tracking responses, now all move commands can.
        /// </summary>
        EnableMoveTracking = 0x10,
        /// <summary>
        /// Disables the Manual Move Tracking response during manual moves. The 
        /// Disable Auto-Reply option above takes precedence over this option.
        /// </summary>
        DisableManualMoveTracking = 0x20,
        /// <summary>
        /// Enables the Message Ids Mode. In this mode of communication, only 
        /// bytes 3 thru 5 are used for data. Byte 6 is used as an ID byte 
        /// that the user can set to any value they wish. It will be returned 
        /// unchanged in the reply. Logic Channel Mode allows the user 
        /// application to monitor communication packets individually to 
        /// implement error detection and recovery.
        /// </summary>
        EnableMessageIdsMode = 0x40,
        /// <summary>
        /// This bit is set to 0 automatically on power-up or reset. It is set 
        /// automatically when the device is homed or when the position is set 
        /// using command #45. It can be used to detect if a unit has a valid 
        /// position reference. It can also be set or cleared by the user.
        /// </summary>
        HomeStatus = 0x80,
        /// <summary>
        /// Disables auto-home checking. Checking for trigger of home sensor 
        /// is only done when home command is issued. This allows rotational 
        /// devices to move multiple revolutions without retriggering the 
        /// home sensor.
        /// </summary>
        DisableAutoHome = 0x100,
        /// <summary>
        /// Enables circular phase micro-stepping mode. Square phase 
        /// micro-stepping is employed by default. See your user manual or the
        /// wiki for the differences.
        /// </summary>
        EnableCircularPhaseMicrostepping = 0x800,
        /// <summary>
        /// Some devices have active high home limit switches. A value of 1 
        /// must be set for these devices for the device to home properly. On 
        /// devices with built in motors and home sensors, the factory default 
        /// settings will be correct. However, some devices, such as the T-CD 
        /// series can accept a variety of motors and home sensors. On these 
        /// devices, this bit may need to be changed from the default setting 
        /// in order for the home sensor to function correctly. See the 
        /// recommended settings for your particular device. Damage to the 
        /// home sensor or actuator may result if this bit is set improperly.
        /// </summary>
        SetHomeSwitchLogic = 0x1000,
        /// <summary>
        /// Turns off the green power LED. It will still blink briefly, 
        /// immediately after powerup.
        /// </summary>
        DisablePowerLed = 0x4000,
        /// <summary>
        /// Turns off the yellow serial LED.
        /// </summary>
        DisableSerialLed = 0x8000
    }
}
