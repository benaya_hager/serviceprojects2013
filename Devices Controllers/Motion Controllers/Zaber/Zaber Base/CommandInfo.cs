using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Zaber
{
    /// <summary>
    /// Descriptive information about a command that Zaber devices support.
    /// </summary>
    [Serializable]
    public class CommandInfo
    {
        private byte number;
        private string name;
        private string dataDescription;
        private string responseDescription;
        private string helpText;

        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        public CommandInfo()
        {
            FirmwareVersionDropped = Int32.MaxValue;
            HasParameters = true;
        }

        /// <summary>
        /// Is this command actually a setting that can also be retrieved?
        /// </summary>
        /// <remarks>
        /// A setting's value is set by sending the command with the new
        /// value as the data. A setting's value is read by sending the
        /// <see cref="Zaber.Command.ReturnSetting"/> command with the setting's
        /// command number as the data.
        /// </remarks>
        public virtual bool IsSetting
        {
            get { return false; }
        }

        /// <summary>
        /// Is this command actually a read-only setting?
        /// </summary>
        /// <remarks>
        /// Read-only settings are read differently from other settings. Just
        /// send the command and the value will be returned.
        /// </remarks>
        public virtual bool IsReadOnlySetting
        {
            get { return false; }
        }

        /// <summary>
        /// Response-only commands are never sent to the devices. They come back as
        /// responses. The error response is a good example.
        /// </summary>
        public virtual bool IsResponseOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets a flag showing whether it is safe to send the command 
        /// again when a response is lost.
        /// </summary>
        public bool IsRetrySafe { get; set; }

        /// <summary>
        /// Gets or sets a flag showing whether the command can be sent to one
        /// axis of a multi-axis device. (Only relevant for text commands.)
        /// </summary>
        public bool IsAxisCommand { get; set; }

        /// <summary>
        /// Does the command take parameters. (Only relevant for ASCII commands.)
        /// </summary>
        public bool HasParameters { get; set; }

        /// <summary>
        /// General descriptive text about the command.
        /// </summary>
        /// <remarks>
        /// When you set this property, it will be reformatted. Line breaks and
        /// whitespace are removed, except for blank lines.
        /// </remarks>
        public string HelpText
        {
            get { return helpText; }
            set {
                if (value == null)
                {
                    helpText = value;
                    return;
                }

                string trimmedValue = value.Trim();
                StringBuilder builder = new StringBuilder();
                MatchCollection matches = 
                    Regex.Matches(trimmedValue, @"\n(\s*\n)+");
                int consumedCharacters = 0;
                foreach (Match match in matches)
                {
                    builder.AppendLine(NormalizeSubstringWhitespace(
                        trimmedValue,
                        consumedCharacters,
                        match.Index - consumedCharacters));
                    consumedCharacters = match.Index + match.Length;
                }
                builder.AppendLine(NormalizeSubstringWhitespace(
                    trimmedValue,
                    consumedCharacters,
                    trimmedValue.Length - consumedCharacters));
                helpText = builder.ToString();
            }
        }

        /// <summary>
        /// Set the <see cref="HelpText"/> property without triggering the
        /// usual formatting.
        /// </summary>
        /// <param name="text">The formatted help text.</param>
        public void SetFormattedHelpText(string text)
        {
            helpText = text;
        }

        /// <summary>
        /// Gets or sets a flag showing whether the response to this command
        /// contains the device's current position.
        /// </summary>
        public bool IsCurrentPositionReturned { get; set; }

        private static string NormalizeSubstringWhitespace(
            string input, 
            int startIndex, 
            int length)
        {
            string rawParagraph = input.Substring(startIndex, length);
            return Regex.Replace(rawParagraph, @"\s+", " ").Trim();
        }

        /// <summary>
        /// Describes the meaning of the data value in a response packet.
        /// </summary>
        public string ResponseDescription
        {
            get { return responseDescription; }
            set { responseDescription = value; }
        }
	
        /// <summary>
        /// Describes the data value of this command.
        /// </summary>
        /// <value>
        /// Null if the data command is ignored.
        /// </value>
        public string DataDescription
        {
            get { return dataDescription; }
            set { dataDescription = value; }
        }

        /// <summary>
        /// A brief name for the command
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// The enumeration entry from <see cref="Zaber.Command"/> 
        /// that represents this command.
        /// </summary>
        public Command Command
        {
            get { return (Command)number; }
            set { number = (byte)value; }
        }

        /// <summary>
        /// The code number that represents this command.
        /// </summary>
        public byte Number
        {
            get { return number; }
            set { number = value; }
        }

        /// <summary>
        /// A text version of the command to use when the port is in ASCII
        /// mode.
        /// </summary>
        /// <remarks>Some commands will have a text version, some will have a
        /// binary version, some will have both.</remarks>
        public String TextCommand { get; set; }

        /// <summary>
        /// Gets or sets the kind of measurement represented by the data value
        /// for this command's requests.
        /// </summary>
        public MeasurementType RequestDataType { get; set; }

        /// <summary>
        /// Gets or sets the kind of measurement represented by the data value
        /// for this command's responses.
        /// </summary>
        public MeasurementType ResponseDataType { get; set; }

        /// <summary>
        /// The firmware version where this command was added.
        /// </summary>
        public int FirmwareVersionAdded { get; set; }

        /// <summary>
        /// Gets or sets the firmware version where this command was dropped.
        /// Defaults to MaxInt.
        /// </summary>
        public int FirmwareVersionDropped { get; set; }

        /// <summary>
        /// Gets or sets a flag marking this command as a simple one. 
        /// Defaults to false.
        /// </summary>
        public bool IsBasic { get; set; }

        /// <summary>
        /// Gets or sets a flag marking this command as one that should not
        /// be shown in the user interface. 
        /// Defaults to false.
        /// </summary>
        /// <remarks>Typically, these are either advanced commands or
        /// only kept for backward compatibility.</remarks>
        public bool IsHidden { get; set; }
    }
}
