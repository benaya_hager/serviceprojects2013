using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Notification of a request or response from one of the devices in the chain.
    /// </summary>
    public class DataPacketEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="data">See <see cref="DataPacket"/></param>
        public DataPacketEventArgs(DataPacket data)
        {
            DataPacket = data;
        }

        /// <summary>
        /// Gets the details of the device's request or response.
        /// </summary>
        public DataPacket DataPacket { get; private set; }
    }
}
