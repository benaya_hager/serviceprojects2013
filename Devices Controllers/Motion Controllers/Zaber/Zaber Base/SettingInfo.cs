using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Descriptive information about a setting that Zaber devices support.
    /// </summary>
    /// <remarks>This class is specific to commands that read or adjust a 
    /// device setting.</remarks>
    public class SettingInfo : CommandInfo
    {
        /// <summary>
        /// Is this command actually a setting that can also be retrieved?
        /// </summary>
        /// <remarks>
        /// A setting's value is set by sending the command with the new
        /// value as the data. A setting's value is read by sending the
        /// <see cref="Zaber.Command.ReturnSetting"/> command with the setting's
        /// command number as the data.
        /// </remarks>
        public override bool IsSetting { get { return true; } }
    }
}
