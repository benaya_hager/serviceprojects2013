using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// Exception thrown when a <see cref="Conversation"/>'s request is 
    /// interrupted by a <see cref="ZaberPortError"/>.
    /// </summary>
    [Serializable]
    public class ZaberPortErrorException : ConversationException
    {
        private ZaberPortError errorType;

        public ZaberPortErrorException()
            : base(BuildMessage(ZaberPortError.None))
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public ZaberPortErrorException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this one</param>
        public ZaberPortErrorException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected ZaberPortErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            errorType =
                (ZaberPortError)info.GetValue("errorType", typeof(ZaberPortError));
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="errorType">The type of error that caused this exception</param>
        public ZaberPortErrorException(ZaberPortError errorType)
            : base(BuildMessage(errorType))
        {
            this.errorType = errorType;
        }

        private static string BuildMessage(ZaberPortError errorType)
        {
            return String.Format(
                CultureInfo.CurrentCulture,
                "Port error: {0}", 
                errorType);
        }

        /// <summary>
        /// The type of error that caused the exception.
        /// </summary>
        public ZaberPortError ErrorType
        {
            get { return errorType; }
        }

        /// <summary>
        /// Sets the System.Runtime.Serialization.SerializationInfo
        ///     with information about the exception.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info, 
            StreamingContext context)
        {
            base.GetObjectData(info, context);
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("errorType", errorType);
        }
    }
}
