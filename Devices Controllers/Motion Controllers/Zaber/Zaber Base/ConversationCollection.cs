﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using log4net;

namespace Zaber
{
    /// <summary>
    /// Treats a collection of conversations as a single conversation. Useful 
    /// for making requests from aliases (including the "all devices" alias on 
    /// device number 0). When you make a request, it waits for a response from
    /// every conversation in the collection before it returns.
    /// </summary>
    public class ConversationCollection : Conversation, IList<Conversation>
    {
        private static ILog log = LogManager.GetLogger(typeof(ConversationCollection));

        private object locker = new object();
        private List<Conversation> members = new List<Conversation>();
        private List<ConversationTopicCollection> waitingTopics =
            new List<ConversationTopicCollection>();

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="device">The device that this conversation will send 
        /// single requests through.</param>
        public ConversationCollection(DeviceCollection device)
            : base(device)
        {
        }

        /// <summary>
        /// Gets or sets a flag for whether unexpected responses should be
        /// reported as invalid.
        /// </summary>
        /// <exception cref="InvalidOperationException">when this conversation's
        /// device does not have message ids enabled</exception>
        /// <remarks>If this flag is true, then only responses with a known
        /// message id will be processed. Any responses with an unknown message
        /// id or no message id will trigger 
        /// <see cref="ZaberPortError.InvalidPacket"/>. The advantage to 
        /// setting this is that corrupted responses won't accidentally trigger 
        /// a <see cref="RequestReplacedException"/>. If you do set this,
        /// you should probably use the <see cref="Timeout"/> property
        /// to avoid having a thread that hangs forever. Setting this on
        /// a conversation collection will set it on all members of the
        /// collection.</remarks>
        /// <seealso cref="RetryCount"/>
        public override bool AreUnexpectedResponsesInvalid
        {
            get
            {
                return base.AreUnexpectedResponsesInvalid;
            }
            set
            {
                base.AreUnexpectedResponsesInvalid = value;
                foreach (var member in members)
                {
                    member.AreUnexpectedResponsesInvalid = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of times to retry a request after a port error.
        /// Defaults to 0.
        /// </summary>
        /// <remarks>
        /// By default, when the port raises its 
        /// <see cref="IZaberPort.ErrorReceived"/> event, any waiting 
        /// conversation topics throw a <see cref="ZaberPortErrorException"/>.
        /// However, you can set this property to make the conversation retry
        /// the request a few times before throwing the exception. Only commands
        /// that are safe will be retried. For example, Move Relative cannot
        /// be retried, it will always throw an exception if a port error 
        /// occurs. Setting this on a conversation collection will set it
        /// on all members of the collection.
        /// </remarks>
        public override int RetryCount
        {
            get
            {
                return base.RetryCount;
            }
            set
            {
                base.RetryCount = value;
                foreach (var member in members)
                {
                    member.RetryCount = value;
                }
            }
        }

        /// <summary>
        /// Send a request to a Zaber device collection and wait for the responses
        /// from all its member devices.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <returns>The response message from the first conversation in the
        /// list.</returns>
        /// <exception cref="RequestCollectionException">
        /// At least one conversation in the list raised an exception. The detailed
        /// responses and exceptions are available from the exception as a list of
        /// <see cref="ConversationTopic"/> objects.
        /// </exception>
        /// <exception cref="RequestTimeoutException">Some of the requests did not
        /// complete before the timeout expired.</exception>
        /// <remarks>
        /// This method sends the request on to this conversation's device and then
        /// puts the thread to sleep until a response is received from every
        /// conversation in the list. However,
        /// there are some situations where it can get confused about
        /// which response goes with which request. See <see cref="Conversation.Request(Command, int)"/>
        /// for details.
        /// </remarks>
        /// <seealso cref="RequestCollection"/>
        public override DeviceMessage Request(Command command, int data)
        {
            return RequestCollection(command, data)[0];
        }

        /// <summary>
        /// Record that a request is about to be made.
        /// </summary>
        /// <param name="messageId">The message id that the request will use.</param>
        /// <returns>The topic that will monitor the request.</returns>
        /// <remarks>
        /// <para>
        /// You can then make the request using the device directly and call 
        /// <see cref="ConversationTopic.Wait()"/> to wait for the response. This is
        /// useful if you want to make several unrelated requests of several devices
        /// and then wait until they are all completed. If message ids are enabled,
        /// be sure to use the message id from the topic when sending the request.
        /// </para>
        /// <para>
        /// If message ids are disabled, then this technique is not thread-safe,
        /// because there's no guarantee that two requests from two threads will get 
        /// sent to the device in the same order that their topics were started.
        /// If you're sending requests to one device from more than one thread,
        /// either enable message ids, or implement your own locking at a higher level.
        /// </para>
        /// </remarks>
        public override ConversationTopic StartTopic(byte messageId)
        {
            return StartTopicCollection(messageId);
        }

        /// <summary>
        /// Record that a request is about to be made to several devices.
        /// </summary>
        /// <returns>The topic that will monitor the request.</returns>
        /// <remarks>
        /// <para>
        /// You can then make the request using the devices directly and call 
        /// <see cref="ConversationTopic.Wait()"/> to wait for the response. This is
        /// useful if you want to make several unrelated requests of several devices
        /// and then wait until they are all completed. If message ids are enabled,
        /// be sure to use the message id from the topic when sending the request.
        /// </para>
        /// <para>
        /// If message ids are disabled, then this technique is not thread-safe,
        /// because there's no guarantee that two requests from two threads will get 
        /// sent to the device in the same order that their topics were started.
        /// If you're sending requests to one device from more than one thread,
        /// either enable message ids, or implement your own locking at a higher level.
        /// </para>
        /// </remarks>
        public ConversationTopicCollection StartTopicCollection()
        {
            int maxMessageIds = 254; // 0 and 255 are not allowed.
            for (int i = 0; i < maxMessageIds; i++)
            {
                try
                {
                    return StartTopicCollection(CalculateMessageId());
                }
                catch (ActiveMessageIdException)
                {
                    // continue trying for all possible message ids
                }
            }
            throw new InvalidOperationException("All message ids are active.");
        }

        /// <summary>
        /// Record that a request is about to be made to several devices.
        /// </summary>
        /// <param name="messageId">The message id that the request will use.</param>
        /// <returns>The topic that will monitor the request.</returns>
        /// <remarks>
        /// <para>
        /// You can then make the request using the devices directly and call 
        /// <see cref="ConversationTopic.Wait()"/> to wait for the response. This is
        /// useful if you want to make several unrelated requests of several devices
        /// and then wait until they are all completed. If message ids are enabled,
        /// be sure to use the message id from the topic when sending the request.
        /// </para>
        /// <para>
        /// If message ids are disabled, then this technique is not thread-safe,
        /// because there's no guarantee that two requests from two threads will get 
        /// sent to the device in the same order that their topics were started.
        /// If you're sending requests to one device from more than one thread,
        /// either enable message ids, or implement your own locking at a higher level.
        /// </para>
        /// </remarks>
        public ConversationTopicCollection StartTopicCollection(byte messageId)
        {
            var topics = new ConversationTopicCollection();
            topics.MessageId = messageId;

            lock (locker)
            {
                foreach (var item in FindSingleConversations())
                {
                    topics.Add(item.StartTopic(messageId));
                }
                waitingTopics.Add(topics);
                topics.Completed += new EventHandler(topic_Completed);
            }
            return topics;
        }

        private List<Conversation> FindSingleConversations()
        {
            lock (locker)
            {
                var singleConversations = new List<Conversation>();
                var visitedCollections = new HashSet<ICollection<Conversation>>();
                AddSingleConversations(
                    members, 
                    singleConversations, 
                    visitedCollections);
                return singleConversations;
            }
        }

        private void AddSingleConversations(
            ICollection<Conversation> source,
            List<Conversation> singleConversations,
            HashSet<ICollection<Conversation>> visitedCollections)
        {
            lock (locker)
            {
                if (visitedCollections.Contains(source))
                {
                    return;
                }
                visitedCollections.Add(source);
                foreach (var item in source)
                {
                    if (item.Device.IsSingleDevice)
                    {
                        if (!singleConversations.Contains(item))
                        {
                            singleConversations.Add(item);
                        }
                    }
                    else
                    {
                        var collection = (ConversationCollection)item;
                        AddSingleConversations(
                            collection,
                            singleConversations,
                            visitedCollections);
                    }
                }
            }
        }

        private void topic_Completed(object sender, EventArgs e)
        {
            lock (locker)
            {
                waitingTopics.Remove((ConversationTopicCollection)sender);
            }
        }

        /// <summary>
        /// Handle the event when a response comes in that doesn't correspond
        /// to any known topic. This is only called when 
        /// <see cref="AreUnexpectedResponsesInvalid"/> is true.
        /// </summary>
        protected override void OnTopicNotFound()
        {
            // Do nothing, because conversation collections don't store
            // their topics the same way.
        }

        /// <summary>
        /// This is really only used for testing purposes to make sure that no
        /// requests are left over in any of the internal data structures.
        /// </summary>
        public override bool IsWaiting
        {
            get
            {
                lock (locker)
                {
                    return waitingTopics.Count != 0;
                }
            }
        }

        /// <summary>
        /// Send a request to several conversations and coordinate all the responses.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">An array of data values. One will be sent to each
        /// conversation in the list. There are two special cases: one entry and no entries.
        /// An array with one entry will be used to send a single request to this 
        /// conversation's device and then the thread will block until a response 
        /// is received from every conversation in the list. An array with no entries
        /// is the same as one entry with value 0. See <see cref="DataPacket.Data"/>.</param>
        /// <returns>A list of response messages in the same order that the
        /// conversations appear in the list.</returns>
        /// <exception cref="RequestCollectionException">
        /// At least one request in the list raised an exception. The detailed
        /// responses and exceptions are available from the exception as a list of
        /// <see cref="ConversationTopic"/> objects.
        /// </exception>
        /// <exception cref="RequestTimeoutException">Some of the requests did not
        /// complete before the timeout expired.</exception>
        /// <remarks>
        /// This method sends the request on to the device and then
        /// puts the thread to sleep until a response is received from every
        /// device in the collection. However,
        /// there are some situations where it can get confused about
        /// which response goes with which request. See 
        /// <see cref="Conversation.Request(Command, int)"/>
        /// for details.
        /// </remarks>
        /// <seealso cref="Request"/>
        public IList<DeviceMessage> RequestCollection(Command command, params int[] data)
        {
            ConversationTopicCollection topics;
            List<Conversation> currentMembers = new List<Conversation>();
            lock (locker)
            {
                topics = StartTopicCollection();

                // take a snapshot of the current members because new ones might
                // get added while you're sending requests.
                currentMembers.AddRange(members);
            }

            if (data.Length < 2)
            {
                int selectedData =
                    data.Length == 0
                    ? 0
                    : data[0];
                log.Debug("Sending to alias device.");
                foreach (var topic in topics)
                {
                    PrepareForRetry(command, selectedData, topic);
                }
                Device.Send(command, selectedData, topics.MessageId);
            }
            else
            {
                log.Debug("Sending to individual devices.");
                for (int i = 0; i < currentMembers.Count; i++)
                {
                    PrepareForRetry(command, data[i], topics[i]);
                    ZaberDevice device = currentMembers[i].Device;
                    device.Send(command, data[i], topics.MessageId);
                }
            }
            
            log.Debug("Waiting for members.");
            if (!topics.Wait(TimeoutTimer))
            {
                throw new RequestTimeoutException();
            }
            topics.Validate();
            var responses = new List<DeviceMessage>();
            foreach (var topic in topics)
            {
                responses.Add(topic.Response);
            }
            return responses;
        }

        /// <summary>
        /// Repeatedly check the device's status until it is idle, using
        /// text-mode requests.
        /// </summary>
        public override void PollUntilIdle()
        {
            PollUntilIdle(PollTimeoutTimer);
        }

        /// <summary>
        /// Repeatedly check the device's status until it is idle, using
        /// text-mode requests and the given timer.
        /// </summary>
        /// <param name="timer">How long to wait between polling requests.
        /// </param>
        public override void PollUntilIdle(TimeoutTimer timer)
        {
            foreach (var item in FindSingleConversations())
            {
                item.PollUntilIdle(timer);
            }
        }

        private class ConversationComparer : IComparer<Conversation>
        {
            public int Compare(Conversation x, Conversation y)
            {
                return x.Device.DeviceNumber.CompareTo(y.Device.DeviceNumber);
            }
        }

        /// <summary>
        /// Sorts the items in the list by their device numbers ascending.
        /// </summary>
        public void Sort()
        {
            members.Sort(new ConversationComparer());
        }

        #region IList<Conversation> Members

        /// <summary>
        ///     Searches for the specified conversation and returns the zero-based index of the
        ///     first occurrence within the entire list.
        /// </summary>
        /// <param name="item">The conversation to locate in the list. The value
        ///     can be null.</param>
        /// <returns>The zero-based index of the first occurrence of item within the entire 
        /// list, if found; otherwise, –1.</returns>
        public int IndexOf(Conversation item)
        {
            return members.IndexOf(item);
        }

        /// <summary>
        ///     Inserts an element into the list at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert. The value can be null.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is greater than <see cref="Count"/>.</exception>
        public void Insert(int index, Conversation item)
        {
            members.Insert(index, item);
        }

        /// <summary>
        ///     Removes the element at the specified index of the list.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        public void RemoveAt(int index)
        {
            members.RemoveAt(index);
        }

        /// <summary>
        ///     Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        /// <exception cref="ArgumentOutOfRangeException">index is less than 0.  
        /// -or- index is equal to or greater than <see cref="Count"/>.</exception>
        public Conversation this[int index]
        {
            get { return members[index]; }
            set { members[index] = value; }
        }

        #endregion

        #region ICollection<Conversation> Members

        /// <summary>
        /// Adds a conversation to the collection.
        /// </summary>
        /// <param name="item">The conversation to add to the collection</param>
        public void Add(Conversation item)
        {
            members.Add(item);
        }

        /// <summary>
        /// Removes all conversations from the collection.
        /// </summary>
        public void Clear()
        {
            members.Clear();
        }

        /// <summary>
        /// Determines whether the collection contains a specific conversation.
        /// </summary>
        /// <param name="item">The conversation to locate in the collection</param>
        /// <returns>true if the conversation is found in the collection, otherwise false.</returns>
        public bool Contains(Conversation item)
        {
            return members.Contains(item);
        }

        /// <summary>
        /// Copies the conversations in the collection to a
        ///     System.Array, starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the
        ///     conversations copied from the collection. The System.Array must
        ///     have zero-based indexing.
        ///</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
        /// <exception cref="ArgumentNullException">array is null.</exception>
        /// <exception cref="ArgumentException">arrayIndex is equal 
        /// to or greater than the
        ///     length of array.-or-The number of elements in the source collection
        ///     is greater than the available space from arrayIndex to the end of the destination
        ///     array.</exception>
        public void CopyTo(Conversation[] array, int arrayIndex)
        {
            members.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of conversations contained in the collection.
        /// </summary>
        public int Count
        {
            get { return members.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific conversation from the collection.
        /// </summary>
        /// <param name="item">The conversation to remove from the collection.</param>
        /// <returns>true if the conversation was successfully removed from the collection,
        ///     otherwise false. This method also returns false if the conversation is not found in
        ///     the collection.</returns>
        public bool Remove(Conversation item)
        {
            return members.Remove(item);
        }

        #endregion

        #region IEnumerable<Conversation> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        public IEnumerator<Conversation> GetEnumerator()
        {
            return members.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>an enumerator that iterates through the collection.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return members.GetEnumerator();
        }

        #endregion
    }
}
