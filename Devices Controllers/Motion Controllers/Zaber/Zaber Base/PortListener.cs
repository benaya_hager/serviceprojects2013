﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Zaber
{
    /// <summary>
    /// Registering for response events from a port can be 
    /// awkward, especially in scripts. This class registers for the response
    /// events and stores them until you request them with the 
    /// NextResponse() method. Listening is started automatically when you 
    /// create a listener, but you can stop and start listening with the 
    /// Stop() and Start() methods.
    /// </summary>
    public class PortListener : ResponseListener<DataPacket>
    {
        private IZaberPort _port;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="port">The port to listen to for responses.</param>
        public PortListener(IZaberPort port)
        {
            _port = port;
            Start();
        }

        /// <summary>
        /// Register with the port.
        /// </summary>
        protected override void RegisterEvent()
        {
            _port.DataPacketReceived +=
                new EventHandler<DataPacketEventArgs>(port_DataPacketReceived);
        }

        /// <summary>
        /// Unregister from the port.
        /// </summary>
        protected override void UnregisterEvent()
        {
            _port.DataPacketReceived -=
                new EventHandler<DataPacketEventArgs>(port_DataPacketReceived);
        }

        /// <summary>
        /// Handle a response event from the port.
        /// </summary>
        /// <param name="sender">The port sending the response.</param>
        /// <param name="e">The details of the response.</param>
        private void port_DataPacketReceived(object sender, DataPacketEventArgs e)
        {
            OnItemReceived(e.DataPacket);
        }
    }
}
