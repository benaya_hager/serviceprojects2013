﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Provides scripting services for statically-typed languages.
    /// </summary>
    public class CompiledScriptLanguage : ScriptLanguage
    {
        private CompilerInfo compilerInfo;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="compilerInfo">The compiler that this language 
        /// represents.</param>
        public CompiledScriptLanguage(CompilerInfo compilerInfo)
        {
            if (compilerInfo == null)
            {
                throw new ArgumentNullException("compilerInfo");
            }
            this.compilerInfo = compilerInfo;
            PickName(compilerInfo.GetLanguages());
        }

        /// <summary>
        /// The details of the compiler this language represents.
        /// </summary>
        public CompilerInfo CompilerInfo
        {
            get { return compilerInfo; }
        }

        /// <summary>
        /// Get a list of the file extensions this language supports.
        /// </summary>
        /// <returns>A collection of strings. The strings do not start with a 
        /// period.</returns>
        public override ICollection<string> UniqueExtensions
        {
            get
            {
                List<string> uniqueExtensions = new List<string>();

                string[] allExtensions = compilerInfo.GetExtensions();
                foreach (string extension in allExtensions)
                {
                    string trimmed = extension.TrimStart('.');
                    if (!uniqueExtensions.Contains(trimmed))
                    {
                        uniqueExtensions.Add(trimmed);
                    }
                }
                return uniqueExtensions;
            }
        }

        /// <summary>
        /// Convert a script to a plug in that can be executed.
        /// </summary>
        /// <param name="sourceCode">The source code for the script.</param>
        /// <returns>The plug in that can be executed.</returns>
        /// <exception cref="ArgumentException">The compiler generated errors.
        /// The error details can be found in the 
        /// <see cref="ScriptLanguage.CompilerErrors"/>
        /// property. Compiler warnings do not cause the exception, but they
        /// can also be found in the 
        /// <see cref="ScriptLanguage.CompilerErrors"/> property.
        /// </exception>
        public override IPlugIn CreatePlugIn(string sourceCode)
        {
            CompilerErrors.Clear();
            ScriptCompiler compiler = new ScriptCompiler(compilerInfo);
            try
            {
                return compiler.Compile(sourceCode);
            }
            finally
            {
                if (compiler.Errors != null)
                {
                    CompilerErrors.AddRange(compiler.Errors);
                }
            }
        }

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current System.Object.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current System.Object.</param>
        /// <returns>true if the specified System.Object is equal to the current System.Object;
        /// otherwise, false.</returns>
        /// <remarks>Just compares whether the two CompiledScriptLanguages use the 
        /// same compiler info.</remarks>
        public override bool Equals(object obj)
        {
            var other = obj as CompiledScriptLanguage;

            if (other == null)
            {
                return false;
            }

            return compilerInfo.Equals(other.compilerInfo);
        }

        /// <summary>
        /// Serves as a hash function for the CompiledScriptLanguage.
        /// </summary>
        /// <returns>A hash code for the CompiledScriptLanguage.</returns>
        /// <remarks>Just returns the compiler info's hash code.</remarks>
        public override int GetHashCode()
        {
            return compilerInfo.GetHashCode();
        }
    }
}
