﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Scripting.Hosting;
using System.CodeDom.Compiler;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Provides scripting services for dynamically-typed languages.
    /// </summary>
    public class DynamicScriptLanguage : ScriptLanguage
    {
        private ScriptEngine scriptEngine;

        /// <summary>
        /// Create an instance.
        /// </summary>
        /// <param name="scriptEngine">a script engine for the language</param>
        public DynamicScriptLanguage(ScriptEngine scriptEngine)
        {
            this.scriptEngine = scriptEngine;
            PickName(scriptEngine.Setup.Names);
        }

        /// <summary>
        /// Get a list of the file extensions this language supports.
        /// </summary>
        /// <returns>A collection of strings. The strings do not start with a 
        /// period.</returns>
        public override ICollection<string> UniqueExtensions
        {
            get
            {
                return new List<String>(
                    scriptEngine.Setup.FileExtensions.Select(
                        ext => ext.Substring(1))); // drop period at start.
            }
        }

        /// <summary>
        /// Convert a script to a plug in that can be executed.
        /// </summary>
        /// <param name="sourceCode">The source code for the script.</param>
        /// <returns>The plug in that can be executed.</returns>
        public override IPlugIn CreatePlugIn(string sourceCode)
        {
            CompilerErrors.Clear();

            return new DynamicPlugIn(sourceCode, scriptEngine);
        }

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current System.Object.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current System.Object.</param>
        /// <returns>true if the specified System.Object is equal to the current System.Object;
        /// otherwise, false.</returns>
        /// <remarks>Just compares whether the two DynamicScriptLanguages use the 
        /// same script engine.</remarks>
        public override bool Equals(object obj)
        {
            var other = obj as DynamicScriptLanguage;

            if (other == null)
            {
                return false;
            }

            return scriptEngine.Equals(other.scriptEngine);
        }

        /// <summary>
        /// Serves as a hash function for the DynamicScriptLanguage.
        /// </summary>
        /// <returns>A hash code for the DynamicScriptLanguage.</returns>
        /// <remarks>Just returns the script engine's hash code.</remarks>
        public override int GetHashCode()
        {
            return scriptEngine.GetHashCode();
        }
    }
}
