﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Specifies how 
    /// <see cref="PlugInBase.Sleep(int, SleepCancellationResponse)"/> should
    /// respond when <see cref="PlugInBase.Cancel"/> is called.
    /// </summary>
    public enum SleepCancellationResponse
    {
        /// <summary>
        /// Throw an <c>OperationCanceled</c> exception.
        /// </summary>
        Throw,

        /// <summary>
        /// Don't throw an exception. As soon as <see cref="PlugInBase.Cancel"/>
        /// is called, return normally from the call to 
        /// <see cref="PlugInBase.Sleep(int, SleepCancellationResponse)"/>.
        /// </summary>
        Wake,

        /// <summary>
        /// Ignore any calls to <see cref="PlugInBase.Cancel"/>. Always sleep
        /// the requested time before returning.
        /// </summary>
        Ignore
    }
}
