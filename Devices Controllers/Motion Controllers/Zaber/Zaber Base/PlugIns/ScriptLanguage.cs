using System;
using System.Collections.Generic;
using System.Text;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Globalization;
using System.IO;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Represents a language compiler that can be used for scripting.
    /// </summary>
    public abstract class ScriptLanguage
    {
        private static Dictionary<String, DynamicScriptLanguage> dynamicExtensions =
            LoadDynamicExtensions();

        private string name;

        protected ScriptLanguage()
        {
            CompilerErrors = new CompilerErrorCollection();
        }

        /// <summary>
        /// Load the dynamic language extensions map.
        /// </summary>
        /// <returns>A mapping from file extensions to the languages that use 
        /// them.</returns>
        private static Dictionary<string, DynamicScriptLanguage> LoadDynamicExtensions()
        {
            var extensions = new Dictionary<string, DynamicScriptLanguage>();
            try
            {
                var python = LoadPython();
                foreach (var extension in python.UniqueExtensions)
                {
                    extensions["." + extension] = python;
                }
            }
            catch (FileNotFoundException)
            {
                // Dlls for IronPython are missing, just don't include it.
            }
            return extensions;
        }

        /// <summary>
        /// Select the best language name to display.
        /// </summary>
        /// <param name="languageNames">Possible names for this language.</param>
        protected void PickName(IEnumerable<String> languageNames)
        {
            foreach (string languageName in languageNames)
            {
                if (name == null || languageName.Length > name.Length)
                {
                    name = languageName;
                }
            }
        }

        /// <summary>
        /// Find all languages installed on this computer.
        /// </summary>
        /// <returns>A collection of <see cref="ScriptLanguage"/> objects.</returns>
        public static ICollection<ScriptLanguage> FindAll()
        {
            Dictionary<String, ScriptLanguage> languages = 
                new Dictionary<String, ScriptLanguage>();

            CompilerInfo[] compilers = CodeDomProvider.GetAllCompilerInfo();
            foreach (CompilerInfo compiler in compilers)
            {
                if (compiler.IsCodeDomProviderTypeValid)
                {
                    var languageNames = String.Join(";", compiler.GetLanguages());
                    languages[languageNames] = new CompiledScriptLanguage(compiler);
                }
            }
            List<ScriptLanguage> languageList = 
                new List<ScriptLanguage>(languages.Values);
            foreach (var language in dynamicExtensions.Values)
            {
                if ( ! languageList.Contains(language))
                {
                    languageList.Add(language);
                }
            }
            languageList.Sort(delegate(ScriptLanguage a, ScriptLanguage b)
            {
                return String.Compare(
                    a.Name, 
                    b.Name, 
                    StringComparison.CurrentCultureIgnoreCase);
            });
            return languageList;
        }

        /// <summary>
        /// Try to load the Python language if the necessary dlls are present.
        /// </summary>
        private static DynamicScriptLanguage LoadPython()
        {
            return new DynamicScriptLanguage(Python.CreateEngine());
        }

        /// <summary>
        /// The name of this language
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// A file filter for use with file dialogs.
        /// </summary>
        public string FileFilter
        {
            get {
                StringBuilder extensionList = new StringBuilder();
                ICollection<string> extensions = UniqueExtensions;
                foreach (string extension in extensions)
                {
                    if (extensionList.Length > 0)
	                {
		                 extensionList.Append(";");
	                }
                    extensionList.AppendFormat("*.{0}", extension);
                }
                return String.Format(
                    CultureInfo.CurrentCulture,
                    "{0} files ({1})|{2}",
                    Name,
                    extensionList,
                    extensionList); 
            }
        }

        /// <summary>
        /// Get a list of the file extensions this language supports.
        /// </summary>
        /// <returns>A collection of strings. The strings do not start with a 
        /// period.</returns>
        public abstract ICollection<string> UniqueExtensions
        {
            get;
        }

        /// <summary>
        /// Convert a script to a plug in that can be executed.
        /// </summary>
        /// <param name="sourceCode">The source code for the script.</param>
        /// <returns>The plug in that can be executed.</returns>
        /// <exception cref="ArgumentException">The compiler generated errors.
        /// The error details can be found in the <see cref="CompilerErrors"/>
        /// property. Compiler warnings do not cause the exception, but they
        /// can also be found in the <see cref="CompilerErrors"/> property.
        /// </exception>
        public abstract IPlugIn CreatePlugIn(String sourceCode);

        /// <summary>
        /// Gets a collection of compiler errors from the last call to 
        /// <see cref="CreatePlugIn"/>.
        /// </summary>
        public CompilerErrorCollection CompilerErrors { get; private set; }

        /// <summary>
        /// Find the scripting language that compiles a given file type.
        /// </summary>
        /// <param name="file">The file whose extension will be used.</param>
        /// <returns>The scripting language that compiles the file type.</returns>
        public static ScriptLanguage FindByExtension(FileInfo file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }

            DynamicScriptLanguage language;
            if (dynamicExtensions.TryGetValue(file.Extension, out language))
            {
                return language;
            }
            else if (CodeDomProvider.IsDefinedExtension(file.Extension))
            {
                var languageName =
                    CodeDomProvider.GetLanguageFromExtension(file.Extension);
                CompilerInfo compiler =
                    CodeDomProvider.GetCompilerInfo(languageName);
                return new CompiledScriptLanguage(compiler);
            }
            else
            {
                throw new ArgumentException(
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "No compiler found for {0}", 
                        file.Name));
            }
        }
    }
}
