﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Holds the details about a script template
    /// </summary>
    public class ScriptTemplate
    {
        private FileInfo file;
        private String text;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="file">The file to read the template text from. Null is
        ///     equivalent to an empty file.</param>
        public ScriptTemplate(FileInfo file)
        {
            this.file = file;
            if (file == null)
            {
                text = "";
            }
        }

        /// <summary>
        /// The file that the template text came from.
        /// </summary>
        public FileInfo File
        {
            get { return file; }
        }

        /// <summary>
        /// Get the text from the template file.
        /// </summary>
        /// <exception cref="System.Security.SecurityException">The caller does not have the 
        ///     required permission to read the file.</exception>
        /// <exception cref="FileNotFoundException">The file is not found.</exception>
        /// <exception cref="UnauthorizedAccessException">The path is a directory.</exception>
        /// <exception cref="DirectoryNotFoundException">The specified path is invalid, 
        ///     such as being on an unmapped drive.</exception>
        public String Text
        {
            get {
                if (text == null)
                {
                    using (var reader = file.OpenText())
                    {
                        text = reader.ReadToEnd();
                    }
                }
                return text; 
            }
        }
    }
}
