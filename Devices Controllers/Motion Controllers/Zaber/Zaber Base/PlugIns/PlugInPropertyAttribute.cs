﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Mark a plug in's property to be automatically set by the host 
    /// program.
    /// </summary>
    /// <remarks>
    /// Only properties of certain types will be set by the host program:
    /// <see cref="ZaberPortFacade"/> will receive the active port facade,
    /// <see cref="Conversation"/> will receive the selected conversation from 
    /// the port facade, System.IO.TextReader will receive a user input stream 
    /// if the host program has one, System.IO.TextWriter will receive a 
    /// user output stream if the host has one. Other property types can be 
    /// used together with the <see cref="IPlugInManager"/>. This lets one plug
    /// in pass data to other plug ins.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class PlugInPropertyAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the name for this property. If a plug in needs more 
        /// than one property of the same type, you can distinguish between
        /// them with different names.
        /// </summary>
        /// <remarks>Named properties are only set when another plugin calls
        /// <see cref="IPlugInManager.SetProperty(object, Type, string)"/>.
        /// The default name of null counts as a separate name, so
        /// you could have one string property without a name, and another
        /// string property with a name, and they would be set independently.
        /// The name works in combination with the type, so if another plug in
        /// sets a property with the same name, but an incompatible type, then
        /// this property will not be set.
        /// </remarks>
        public string Name { get; set; }
    }
}
