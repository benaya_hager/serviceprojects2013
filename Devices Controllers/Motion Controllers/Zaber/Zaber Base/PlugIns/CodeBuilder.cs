using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Combines template code with a script's code to create source code for a
    /// class that can be compiled.
    /// </summary>
    public class CodeBuilder
    {
        private TextReader templateReader;
        private TextReader scriptReader;
        private String scriptText;
        private Match templateMatch;
        private int lineOffset;

        /// <summary>
        /// The input source for the script file that will be inserted into the template.
        /// </summary>
        public TextReader ScriptReader
        {
            get { return scriptReader; }
            set { scriptReader = value; }
        }

        /// <summary>
        /// The input source for the template file that will be combined with the script.
        /// </summary>
        /// <remarks>
        /// The template is searched for the first line that contains the marker text
        /// $INSERT-SCRIPT-HERE$ and that entire line is replaced by the contents
        /// of the script.
        /// </remarks>
        public TextReader TemplateReader
        {
            get { return templateReader; }
            set { templateReader = value; }
        }

        /// <summary>
        /// Searches the script for a #template directive and returns the template name if it
        /// finds one. Returns null if no template is found.
        /// </summary>
        /// <example>
        /// If the script contains a line like this:
        /// #template(MyTemplate) x, y, z
        /// this will return "MyTemplate". Anything after the closing parenthesis is ignored
        /// and excluded from the script.
        /// </example>
        public String TemplateName
        {
            get {
                CheckTemplate();
                return 
                    templateMatch.Success
                    ? templateMatch.Groups[1].Value
                    : null;
            }
        }

        /// <summary>
        /// Returns the number of lines that were found in the template before 
        /// the marker text.
        /// </summary>
        /// <remarks>This is useful for translating line numbers from the 
        /// combined source code back to line numbers in the script.</remarks>
        public int LineOffset
        {
            get { return lineOffset; }
        }

        private void CheckTemplate()
        {
            if (scriptText != null)
            {
                return;
            }
            scriptText = scriptReader.ReadToEnd();
            String pattern =
                @"^[ \t]*" // any leading space at start of line
                + @"#template\(" // macro marker and opening parenthesis
                + @"([^)\r\n]+)" // name of template until closing parenthesis. Outer 
                    // parentheses mark it as a capture.
                + @"\).*$" // closing parenthesis and any junk on the rest of the line
                + @"\r?\n?"; // the carriage return and/or line feed at the end of the line.
            Regex r = new Regex(pattern, RegexOptions.Multiline);
            templateMatch = r.Match(scriptText);
        }

        /// <summary>
        /// Build the source code from the <see cref="TemplateReader"/> and 
        /// <see cref="ScriptReader"/>.
        /// </summary>
        /// <returns>The combined source code</returns>
        public string Build()
        {
            CheckTemplate();
            StringBuilder sourceCode = new StringBuilder();

            const string SCRIPT_MARKER = "$INSERT-SCRIPT-HERE$";
            bool isMarkerFound = false;
            string line;
            lineOffset = 0;
            while (null != (line = templateReader.ReadLine()))
            {
                if (!line.Contains(SCRIPT_MARKER))
                {
                    sourceCode.AppendLine(line);
                    lineOffset++;
                }
                else
                {
                    isMarkerFound = true;
                    if (templateMatch.Success)
                    {
                        sourceCode.AppendLine(scriptText.Substring(0, templateMatch.Index));
                        sourceCode.AppendLine(scriptText.Substring(
                            templateMatch.Index + templateMatch.Length));

                    }
                    else
                    {
                        sourceCode.AppendLine(scriptText);
                    }
                    sourceCode.Append(templateReader.ReadToEnd());
                }
            }
            if (!isMarkerFound && sourceCode.Length == 0)
            {
                // empty template or no template, just return script
                return scriptText;
            }
            if (!isMarkerFound)
            {
                throw new ArgumentException("Template does not contain " + SCRIPT_MARKER);
            }
            return sourceCode.ToString();
        }
    }
}
