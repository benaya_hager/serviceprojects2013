﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.PlugIns
{
    /// <summary>
    /// An interface that plug ins can use to pass data between them.
    /// </summary>
    /// <remarks>
    /// If one plug in wants to send data to another one, it can declare a 
    /// property of type IPlugInManager. Then it calls manager.SetProperty()
    /// to pass data to any plug ins with properties of that type. If you want
    /// to have more than one property of the same type, use names for the
    /// properties.
    /// </remarks>
    public interface IPlugInManager
    {
        /// <summary>
        /// Set a property of the given type on any plug ins that have one.
        /// </summary>
        /// <param name="value">The value to pass to each plug in.</param>
        /// <param name="type">The type of property to set. Any properties of
        /// this type or its subtypes will be set.</param>
        void SetProperty(object value, Type type);

        /// <summary>
        /// Set a property of the given type and name on any plug ins that 
        /// have one.
        /// </summary>
        /// <param name="value">The value to pass to each plug in.</param>
        /// <param name="type">The type of property to set. Any properties of
        /// this type or its subtypes will be set.</param>
        /// <param name="name">The name of the property. This will match the
        /// name declared in the <see cref="PlugInPropertyAttribute"/>. The 
        /// default name is null.</param>
        void SetProperty(object value, Type type, String name);
    }
}
