using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using log4net;

namespace Zaber.PlugIns
{
    /// <summary>
    /// A basic implementation of the <see cref="IPlugIn"/> interface.
    /// </summary>
    /// <remarks>
    /// Plug ins and script templates do not have to derive from this base class!
    /// The only requirement is to implement the <see cref="IPlugIn"/> interface.
    /// This class just provides a simple implementation of all the required
    /// features except the <see cref="Run"/> method. It also implements helper
    /// methods like <see cref="Sleep(int)"/> and <see cref="CheckForCancellation"/>.
    /// </remarks>
    public abstract class PlugInBase : IPlugIn
    {
        private static ILog log = LogManager.GetLogger(typeof(PlugInBase));

        private ZaberPortFacade portFacade;
        private Conversation conversation;
        private TextReader input;
        private TextWriter output;
        private bool isCanceled;
        private ManualResetEvent canceledEvent = new ManualResetEvent(false);

        /// <summary>
        /// Run the plug in's operation.
        /// </summary>
        /// <remarks>
        /// The method returns when the operation has completed. Exceptions in the
        /// operation will be thrown from this method.
        /// </remarks>
        public abstract void Run();

        /// <summary>
        /// Ask the operation to stop running.
        /// </summary>
        /// <remarks>
        /// Sets the <see cref="IsCanceled"/> property to true. Derived 
        /// classes' <see cref="Run"/> methods can call 
        /// <see cref="CheckForCancellation"/> to throw an exception
        /// if the operation has been canceled.
        /// </remarks>
        public virtual void Cancel()
        {
            IsCanceled = true;
            if (portFacade.IsOpen)
            {
                // Use a topic instead of sending straight to the port so we don't
                // trigger a port error over an unexpected response.
                var allConversations = portFacade.GetConversation(0);
                var topic = allConversations.StartTopic();
                if (portFacade.Port.IsAsciiMode)
                {
                    allConversations.Device.Send("stop");
                }
                else
                {
                    allConversations.Device.Send(Command.Stop, 0, topic.MessageId);
                }
            }
        }

        /// <summary>
        /// Gets or sets a text writer that will receive output from the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this writer and will dispose it during 
        /// <see cref="Dispose()"/>.
        /// </remarks>
        public virtual TextWriter Output
        {
            get { return output; }
            set { output = value; }
        }

        /// <summary>
        /// Gets or sets a text reader that will provide input to the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this reader and will dispose it during 
        /// <see cref="Dispose()"/>.
        /// </remarks>
        public virtual TextReader Input
        {
            get { return input; }
            set { input = value; }
        }

        /// <summary>
        /// Write a message to the application log file.
        /// </summary>
        /// <param name="message">The object to write to the file.</param>
        /// <param name="exception">An exception to write to the log file,
        /// along with its stack trace.</param>
        /// <remarks>The message will be written to the application's log
        /// file at the Info level, coming from the Zaber.PlugIns.PlugInBase
        /// class.</remarks>
        public virtual void Log(object message, Exception exception)
        {
            log.Info(message, exception);
        }

        /// <summary>
        /// Write a message to the application log file.
        /// </summary>
        /// <param name="message">The object to write to the file.</param>
        /// <remarks>The message will be written to the application's log
        /// file at the Info level, coming from the Zaber.PlugIns.PlugInBase
        /// class.</remarks>
        public virtual void Log(object message)
        {
            log.Info(message);
        }

        /// <summary>
        /// Gets or sets the conversation that was selected by the user.
        /// </summary>
        /// <value>
        /// May be null if none was selected, or if the port hasn't been opened
        /// yet.
        /// </value>
        /// <remarks>
        /// The plug in does not own this conversation and will not dispose it 
        /// during <see cref="Dispose()"/>.
        /// </remarks>
        public virtual Conversation Conversation
        {
            get { return conversation; }
            set { conversation = value; }
        }

        /// <summary>
        /// Gets or sets the port facade that gives access to all the devices 
        /// and conversations.
        /// </summary>
        /// <remarks>
        /// The port facade may or may not be open when it is passed to this 
        /// property. Scripts can check by using the 
        /// <see cref="ZaberPortFacade.IsOpen"/> property.
        /// The plug in does not own this facade and will not dispose it 
        /// during <see cref="Dispose()"/>.
        /// </remarks>
        public virtual ZaberPortFacade PortFacade
        {
            get { return portFacade; }
            set { portFacade = value; }
        }

        /// <summary>
        /// Pause the operation for a number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The length of time to pause</param>
        /// <exception cref="OperationCanceledException">If the operation was 
        /// canceled before or during the sleep.</exception>
        public virtual void Sleep(int milliseconds)
        {
            Sleep(milliseconds, SleepCancellationResponse.Throw);
        }

        /// <summary>
        /// Pause the operation for a number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The length of time to pause</param>
        /// <param name="cancellationResponse">Specifies the desired response
        /// when <see cref="Cancel"/> is called before or during the sleep.</param>
        /// <exception cref="OperationCanceledException">If the operation was 
        /// canceled before or during the sleep.</exception>
        public virtual void Sleep(
            int milliseconds, 
            SleepCancellationResponse cancellationResponse)
        {
            switch (cancellationResponse)
            {   
                case SleepCancellationResponse.Throw:
                    CheckForCancellation();
                    canceledEvent.WaitOne(milliseconds, false);
                    CheckForCancellation();
                    break;
                case SleepCancellationResponse.Wake:
                    canceledEvent.WaitOne(milliseconds, false);
                    break;
                case SleepCancellationResponse.Ignore:
                    Thread.Sleep(milliseconds);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        "cancellationResponse",
                        cancellationResponse,
                        "Unknown cancellation response.");
            }
        }

        /// <summary>
        /// Throw an exception if the operation has been canceled. Otherwise,
        /// do nothing.
        /// </summary>
        /// <exception cref="OperationCanceledException">The operation was
        /// canceled by a call to <see cref="Cancel"/></exception>
        public virtual void CheckForCancellation()
        {
            if (IsCanceled)
            {
                throw new OperationCanceledException();
            }
        }

        /// <summary>
        /// A flag that is set to true when <see cref="Cancel"/> is called.
        /// </summary>
        public virtual bool IsCanceled
        {
            get { return isCanceled; }
            set
            {
                isCanceled = value;
                if (isCanceled)
                {
                    canceledEvent.Set();
                }
                else
                {
                    canceledEvent.Reset();
                }
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                canceledEvent.Close();
                input.Dispose();
                output.Dispose();
            }
        }

        #endregion
    }
}
