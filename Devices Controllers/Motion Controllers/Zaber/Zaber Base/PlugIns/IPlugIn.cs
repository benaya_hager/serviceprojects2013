using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Zaber.PlugIns
{
    /// <summary>
    /// This is the interface implemented by plug ins and scripts that
    /// the script runner can execute.
    /// </summary>
    public interface IPlugIn : IDisposable
    {
        /// <summary>
        /// Run the plug in's operation.
        /// </summary>
        /// <remarks>
        /// The method returns when the operation has completed. Exceptions in the
        /// operation will be thrown from this method.
        /// </remarks>
        void Run();

        /// <summary>
        /// Ask the operation to stop running.
        /// </summary>
        /// <remarks>
        /// <para>This is just a request, plug ins do not have to respect it. A common
        /// implementation is to set an IsCancelled flag and leave it up to
        /// the running code to check the flag at regular intervals.</para>
        /// <para>If a plug in does abort its operation it can either return
        /// normally, or throw an exception. 
        /// <see cref="System.OperationCanceledException"/> is a good exception
        /// to throw in this case.</para>
        /// </remarks>
        void Cancel();

        /// <summary>
        /// Gets or sets the port facade that gives access to all the devices 
        /// and conversations.
        /// </summary>
        /// <remarks>
        /// The port facade may or may not be open when it is passed to this 
        /// property. Scripts can check by using the 
        /// <see cref="ZaberPortFacade.IsOpen"/> property.
        /// The plug in does not own this facade and will not dispose it 
        /// during Dispose().
        /// </remarks>
        ZaberPortFacade PortFacade { get; set; }

        /// <summary>
        /// Gets or sets the conversation that was selected by the user.
        /// </summary>
        /// <value>
        /// May be null if none was selected, or if the port hasn't been opened
        /// yet.
        /// </value>
        /// <remarks>
        /// The plug in does not own this conversation and will not dispose it 
        /// during Dispose().
        /// </remarks>
        Conversation Conversation { get; set; }

        /// <summary>
        /// Gets or sets a text reader that will provide input to the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this reader and will dispose it during 
        /// Dispose().
        /// </remarks>
        TextReader Input { get; set; }

        /// <summary>
        /// Gets or sets a text writer that will receive output from the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this writer and will dispose it during 
        /// Dispose().
        /// </remarks>
        TextWriter Output { get; set; }
    }
}
