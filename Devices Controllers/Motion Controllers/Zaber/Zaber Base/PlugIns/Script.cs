﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.CodeDom.Compiler;
using System.Globalization;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Represents a script file with features for using templates and compiling.
    /// </summary>
    public class Script
    {
        private FileInfo file;
        private String text;
        private IFileFinder templateFinder;
        private ScriptTemplate template;
        private ScriptLanguage language;
        private bool isDirty;
        private CompilerErrorCollection compilerErrors;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="file">The file to load the script from.</param>
        /// <param name="templateFinder">Used to find template files.</param>
        public Script(FileInfo file, IFileFinder templateFinder)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            this.file = file;
            using (var reader = file.OpenText())
            {
                text = reader.ReadToEnd();
            }
            language = ScriptLanguage.FindByExtension(file);
            this.templateFinder = templateFinder;
            bool shouldThrow = false;
            FindTemplate(shouldThrow);
        }

        /// <summary>
        /// Create a new instance without a file.
        /// </summary>
        /// <param name="language">The language that this script will be written in.</param>
        /// <param name="templateFinder">Used to find template files.</param>
        public Script(ScriptLanguage language, IFileFinder templateFinder)
        {
            text = "";
            this.language = language;
            this.templateFinder = templateFinder;
            bool shouldThrow = false;
            FindTemplate(shouldThrow);
        }

        /// <summary>
        /// Find a template based on a #template declaration in the script text.
        /// </summary>
        /// <param name="shouldThrow">True if this should throw an exception
        ///     when it can't find the file named by the #template declaration.</param>
        /// <exception cref="FileNotFoundException">Thrown when it can't find the file 
        ///     named by the #template declaration and shouldThrow is true.</exception>
        private void FindTemplate(bool shouldThrow)
        {
            var builder = new CodeBuilder();
            builder.ScriptReader = new StringReader(text);
            string templateName = builder.TemplateName;
            if (IsTemplateLoaded(templateName))
            {
                // template hasn't changed
                return;
            }
            if (templateName == null)
            {
                template = new ScriptTemplate(null);
            }
            else
            {
                var templateFile = FindTemplateFile(templateName);
                if (templateFile == null && shouldThrow)
                {
                    string msg = String.Format(
                        CultureInfo.CurrentCulture,
                        "Template file not found: '{0}'.",
                        templateName);
                    throw new FileNotFoundException(
                        msg, 
                        templateName);
                }
                template = new ScriptTemplate(templateFile);
            }
        }

        private FileInfo FindTemplateFile(string templateName)
        {
            FileInfo templateFile = templateFinder.FindFile(templateName);
            if (templateFile != null)
            {
                return templateFile;
            }
            FileInfo templateNameInfo = new FileInfo(templateName);
            if (templateNameInfo.Extension.Length == 0)
            {
                foreach (var extension in language.UniqueExtensions)
                {
                    templateFile = templateFinder.FindFile(templateName + "." + extension);
                    if (templateFile != null)
                    {
                        return templateFile;
                    }
                }
            }
            return null;
        }

        private bool IsTemplateLoaded(string templateName)
        {
            if (template == null)
            {
                return false;
            }
            if (template.File == null)
            {
                return templateName == null;
            }
            if (templateName == null)
            {
                return false;
            }
            if (String.Equals(
                template.File.Name, 
                templateName, 
                StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            String trimmedExtension =
                template.File.Extension.Length > 0
                ? template.File.Extension.Substring(1)
                : "";
            if (!language.UniqueExtensions.Contains(trimmedExtension))
            {
                return false; // loaded template is for a different language.
            }
            FileInfo templateNameInfo = new FileInfo(templateName);
            return templateNameInfo.Extension.Length == 0 && String.Equals(
                templateName + template.File.Extension,
                template.File.Name,
                StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// The file this script was loaded from. May be null.
        /// </summary>
        public FileInfo File { get { return file; } }

        /// <summary>
        /// The template referenced by the script file. If there is no template
        /// declaration in the script file, then this will still return a valid
        /// template, but the template will have empty text and a null file.
        /// </summary>
        public ScriptTemplate Template
        {
            get { return template; }
        }

        /// <summary>
        /// The text from the script file.
        /// </summary>
        public String Text
        {
            get { return text; }
            set {
                if (text.Equals(value))
                {
                    return;
                }
                text = value;
                isDirty = true;
                bool shouldThrow = false;
                FindTemplate(shouldThrow);
            }
        }

        /// <summary>
        /// True if the text has been changed since the script was loaded or saved.
        /// </summary>
        public bool IsDirty
        {
            get { return isDirty; }
        }

        /// <summary>
        /// The .NET language that the script will be compiled with.
        /// </summary>
        /// <remarks>The choice is based on the script file's extension.</remarks>
        public ScriptLanguage Language
        {
            get { return language; }
        }

        /// <summary>
        /// Gets the compiler errors that were generated during <see cref="Build"/>
        /// </summary>
        public CompilerErrorCollection CompilerErrors
        {
            get { return compilerErrors; }
        }

        /// <summary>
        /// Write the current script text back to the file it was loaded from.
        /// </summary>
        public void Save()
        {
            using (var writer = new StreamWriter(file.FullName))
            {
                writer.Write(text);
            }
            isDirty = false;
        }

        /// <summary>
        /// Write the current script text to a new file.
        /// </summary>
        /// <param name="newFile">The file to write the script to</param>
        public void SaveAs(FileInfo newFile)
        {
            file = newFile;
            language = ScriptLanguage.FindByExtension(newFile);
            Save();
        }

        /// <summary>
        /// Compile a plug in assembly and find the plug in class within it.
        /// </summary>
        /// <returns>An instance of the plug in class</returns>
        /// <remarks>This takes the script's source code and compiles it in memory to 
        /// an assembly. It then searches the assembly for a class that 
        /// implements the <see cref="IPlugIn"/> interface. It creates an 
        /// instance and returns it.</remarks>
        /// <exception cref="ArgumentException">The compiler generated errors.
        /// The error details can be found in the <see cref="CompilerErrors"/> 
        /// property. Compiler warnings do not cause the exception, but they
        /// can also be found in the <see cref="CompilerErrors"/> property.
        /// </exception>
        /// <exception cref="FileNotFoundException">The #template declaration
        ///     in the script did not match any template file names.</exception>
        public IPlugIn Build()
        {
            compilerErrors = new CompilerErrorCollection();

            // Check that we don't have some unknown template.
            bool shouldThrow = true;
            FindTemplate(shouldThrow);

            var builder = new CodeBuilder();
            builder.ScriptReader = new StringReader(text);
            builder.TemplateReader = new StringReader(template.Text);
            string sourceCode = builder.Build();

            try
            {
                return language.CreatePlugIn(sourceCode);
            }
            finally
            {
                compilerErrors = language.CompilerErrors;
                foreach (CompilerError error in compilerErrors)
                {
                    error.Line -= builder.LineOffset;
                }
            }
        }
    }
}
