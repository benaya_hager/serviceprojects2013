using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Searches for files within a named folder, but looks up the directory
    /// tree to find that folder.
    /// </summary>
    public class FileFinder : IFileFinder
    {
        private DirectoryInfo directory;

        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="startDirectory">The directory to start searching from.</param>
        /// <param name="folderName">The folder to look for</param>
        public FileFinder(DirectoryInfo startDirectory, string folderName)
        {
            DirectoryInfo currentDirectory = startDirectory;
            do
            {
                DirectoryInfo[] matches =
                    currentDirectory.GetDirectories(folderName);
                if (matches.Length > 0)
                {
                    directory = matches[0];
                    break;
                }
                currentDirectory = currentDirectory.Parent;
            } while (currentDirectory != null);
        }

        /// <summary>
        /// Gets the directory that this finder is using.
        /// </summary>
        public DirectoryInfo Directory
        {
            get { return directory; }
        }

        /// <summary>
        /// Find the default file in the folder.
        /// </summary>
        /// <param name="language">Specifies the file extensions you want to
        /// look for</param>
        /// <returns>The requested file or null if none could be found.</returns>
        /// <remarks>We look for files that are named Default.* where .* is one
        /// of the extensions supported by the language.</remarks>
        public FileInfo FindDefaultFile(ScriptLanguage language)
        {
            if (directory == null)
            {
                return null;
            }

            List<FileInfo> allMatches = new List<FileInfo>();

            foreach (string extension in language.UniqueExtensions)
            {
                FileInfo[] matches = directory.GetFiles("Default." + extension);
                allMatches.AddRange(matches);
            }
            switch (allMatches.Count)
            {
                case 0:
                    return null;
                case 1:
                    return allMatches[0];
            }
            allMatches.Sort(delegate(FileInfo a, FileInfo b)
            {
                return String.Compare(
                    a.FullName, 
                    b.FullName, 
                    StringComparison.CurrentCultureIgnoreCase);
            });
            return allMatches[0];
        }

        /// <summary>
        /// Find the named file in the folder.
        /// </summary>
        /// <param name="fileName">The name of the file to find. It may contain wild cards.</param>
        /// <returns>The requested file, or null if it could not be found</returns>
        /// <remarks>We search the folder for the named file. If
        /// more than one file matches the request, the first one is returned.</remarks>
        public FileInfo FindFile(string fileName)
        {
            if (directory == null)
            {
                return null;
            }

            FileInfo[] matches = directory.GetFiles(fileName);
            return matches.Length > 0
                ? matches[0]
                : null;
        }

        /// <summary>
        /// Checks if a file could be found by this finder.
        /// </summary>
        /// <param name="fileName">The full path to the file.</param>
        /// <returns>True if the file is in the directory or one of
        /// its subdirectories, otherwise false.</returns>
        public bool IsFindable(string fileName)
        {
            if (directory == null)
            {
                throw new InvalidOperationException(
                    "No directory was found.");
            }

            return fileName.StartsWith(
                directory.FullName,
                StringComparison.OrdinalIgnoreCase);
        }
    }
}
