﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Mark a class as a plug in to be used in a Zaber library client program.
    /// </summary>
    /// <remarks>
    /// The class must also derive from System.Windows.Forms.Control. Deriving from 
    /// UserControl is the simplest.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PlugInAttribute : Attribute
    {
        /// <summary>
        /// Declare a name to be used for display in the client program. Defaults to
        /// the class name.
        /// </summary>
        public string Name { get; set; }
    }
}
