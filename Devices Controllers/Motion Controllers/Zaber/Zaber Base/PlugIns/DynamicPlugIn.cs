﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Scripting.Hosting;
using System.IO;
using Microsoft.Scripting;
using IronPython.Runtime.Exceptions;
using IronPython.Runtime.Operations;
using System.Globalization;

namespace Zaber.PlugIns
{
    /// <summary>
    /// An adapter from the IPlugIn interface to a dynamic language script.
    /// </summary>
    public class DynamicPlugIn : IPlugIn
    {
        private String sourceCode;
        private ScriptEngine scriptEngine;
        private ScriptScope scriptScope;
        private TextReader input;
        private TextWriter output;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="sourceCode">the source code to run</param>
        /// <param name="scriptEngine">the script engine to run the script
        /// with</param>
        public DynamicPlugIn(String sourceCode, ScriptEngine scriptEngine)
        {
            this.sourceCode = sourceCode;
            this.scriptEngine = scriptEngine;
        }

        #region IPlugIn Members

        /// <summary>
        /// Run the plug in's operation.
        /// </summary>
        /// <remarks>
        /// The method returns when the operation has completed. Exceptions in the
        /// operation will be thrown from this method.
        /// </remarks>
        public void Run()
        {
            var initSource = scriptEngine.CreateScriptSourceFromString(@"
import clr
import System.Threading
clr.AddReference('Zaber')
from Zaber import (Command, DeviceListener, DeviceModes, Measurement,
                   MeasurementType, UnitOfMeasure, ZaberError, ZaberPortError,
                   ZaberPortFacadeState)

def sleep(milliseconds):
    System.Threading.Thread.Sleep(milliseconds)

is_canceled = False
def cancel():
    global is_canceled
    is_canceled = True
    if port_facade.Port.IsAsciiMode:
        port_facade.Port.Send('stop')
    else:
        port_facade.Port.Send(0, Command.Stop, 0)
");
            var source = scriptEngine.CreateScriptSourceFromString(sourceCode);
            scriptScope = scriptEngine.CreateScope();
            scriptScope.SetVariable("conversation", Conversation);
            scriptScope.SetVariable("port_facade", PortFacade);

            initSource.Execute(scriptScope);
            try
            {
                source.Execute(scriptScope);
            }
            catch (SystemExitException)
            {
                // Nothing to do, this is a valid way to exit the script.
            }
            catch (SyntaxErrorException syntaxException)
            {
                throw new ArgumentException(String.Format(
                    CultureInfo.CurrentCulture,
                    "{0} (line {1}, column {2})",
                    syntaxException.Message,
                    syntaxException.Line,
                    syntaxException.Column));
            }
            catch (Exception ex)
            {
                var frames = PythonOps.GetDynamicStackFrames(ex);
                if (frames != null && frames.Length > 0)
                {
                throw new ArgumentException(String.Format(
                    CultureInfo.CurrentCulture,
                    "{0} (line {1})",
                    ex.Message,
                    frames[0].GetFileLineNumber()));
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Ask the operation to stop running.
        /// </summary>
        /// <remarks>
        /// <para>This is just a request, plug ins do not have to respect it. A common
        /// implementation is to set an IsCancelled flag and leave it up to
        /// the running code to check the flag at regular intervals.</para>
        /// <para>If a plug in does abort its operation it can either return
        /// normally, or throw an exception. 
        /// <see cref="System.OperationCanceledException"/> is a good exception
        /// to throw in this case.</para>
        /// </remarks>
        public void Cancel()
        {
            var cancelSource = scriptEngine.CreateScriptSourceFromString(@"cancel()");
            cancelSource.Execute(scriptScope);
        }

        /// <summary>
        /// Gets or sets the port facade that gives access to all the devices 
        /// and conversations.
        /// </summary>
        /// <remarks>
        /// The port facade may or may not be open when it is passed to this 
        /// property. Scripts can check by using the 
        /// <see cref="ZaberPortFacade.IsOpen"/> property.
        /// The plug in does not own this facade and will not dispose it 
        /// during Dispose().
        /// </remarks>
        public ZaberPortFacade PortFacade { get; set; }

        /// <summary>
        /// Gets or sets the conversation that was selected by the user.
        /// </summary>
        /// <value>
        /// May be null if none was selected, or if the port hasn't been opened
        /// yet.
        /// </value>
        /// <remarks>
        /// The plug in does not own this conversation and will not dispose it 
        /// during Dispose().
        /// </remarks>
        public Conversation Conversation { get; set; }

        /// <summary>
        /// Gets or sets a text reader that will provide input to the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this reader and will dispose it during 
        /// Dispose().
        /// </remarks>
        public TextReader Input
        {
            get
            {
                return input;
            }
            set
            {
                input = value;
                // This ignores any binary output and just displays text.
                var ignoredBinaryInput = new MemoryStream();
                scriptEngine.Runtime.IO.SetInput(
                    ignoredBinaryInput,
                    input,
                    Encoding.UTF8);
            }
        }

        /// <summary>
        /// Gets or sets a text writer that will receive output from the script.
        /// </summary>
        /// <remarks>
        /// The plug in owns this writer and will dispose it during 
        /// Dispose().
        /// </remarks>
        public TextWriter Output 
        {
            get
            {
                return output;
            }
            set
            {
                output = value;
                // This ignores any binary output and just displays text.
                var ignoredBinaryOutput = new MemoryStream();
                scriptEngine.Runtime.IO.SetOutput(
                    ignoredBinaryOutput, 
                    output);
                scriptEngine.Runtime.IO.SetErrorOutput(
                    ignoredBinaryOutput,
                    output);
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            // Nothing to dispose.
        }

        #endregion
    }
}
