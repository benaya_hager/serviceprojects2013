﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Zaber.PlugIns
{
    /// <summary>
    /// Search features for files in a specific folder.
    /// </summary>
    public interface IFileFinder
    {
        /// <summary>
        /// Find the default file in the folder.
        /// </summary>
        /// <param name="language">Specifies the file extensions you want to
        /// look for</param>
        /// <returns>The requested file or null if none could be found.</returns>
        /// <remarks>First, we find all files with an extension 
        /// supported by the language parameter. The default is determined using
        /// the first rule that applies:
        /// <list type="number">
        /// <item>If we only find one file, it's the default.</item>
        /// <item>If one of the files is named Default.*, it's the default.</item>
        /// <item>The file names are sorted alphabetically, and the first one 
        /// is the default.</item>
        /// </list></remarks>
        FileInfo FindDefaultFile(ScriptLanguage language);

        /// <summary>
        /// Find the named file in the folder.
        /// </summary>
        /// <param name="fileName">The name of the file to find. It may contain wild cards.</param>
        /// <returns>The requested file, or null if it could not be found</returns>
        /// <remarks>We search the folder for the named file. If
        /// more than one file matches the request, the first one is returned.</remarks>
        FileInfo FindFile(string fileName);

    }
}
