using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Named constants for the error codes that Zaber devices will return.
    /// </summary>
    public enum ZaberError
    {
        /// <summary>
        /// No error has occurred.
        /// </summary>
        /// <remarks>
        /// This should never be returned by a device. It's only useful as a 
        /// default value in code.
        /// </remarks>
        None = 0,

        /// <summary>
        /// Home - Device has traveled a long distance without triggering the home sensor. Device may be stalling or slipping.
        /// </summary>
        CannotHome = 1,

        /// <summary>
        /// Renumbering data out of range.
        /// </summary>
        DeviceNumberInvalid = 2,

        /// <summary>
        /// Read Register - Register address invalid.
        /// </summary>
        AddressInvalid = 5,

        /// <summary>
        /// Power supply voltage too low.
        /// </summary>
        VoltageLow = 14,

        /// <summary>
        /// Power supply voltage too high.
        /// </summary>
        VoltageHigh = 15,

        /// <summary>
        /// The position stored in the requested register is no longer valid. This is probably because the maximum range was reduced.
        /// </summary>
        StoredPositionInvalid = 18,

        /// <summary>
        /// Move Absolute - Target position out of range.
        /// </summary>
        AbsolutePositionInvalid = 20,

        /// <summary>
        /// Move Relative - Target position out of range.
        /// </summary>
        RelativePositionInvalid = 21,

        /// <summary>
        /// Constant velocity move. Velocity out of range.
        /// </summary>
        VelocityInvalid = 22,

        /// <summary>
        /// Set Active Axis - Data out of range. Must be 1, 2, or 3.
        /// </summary>
        AxisInvalid = 25,

        /// <summary>
        /// Set Axis Device Number - Data out of range. Must be between 0 and 254 inclusive.
        /// </summary>
        AxisDeviceNumberInvalid = 26,

        /// <summary>
        /// Set Axis Inversion - Data out of range. Must be 0, 1, or -1.
        /// </summary>
        InversionInvalid = 27,

        /// <summary>
        /// Set Axis Velocity Profile - Data out of range. Must be 0, 1, 2, or 3.
        /// </summary>
        VelocityProfileInvalid = 28,

        /// <summary>
        /// Set Axis Velocity Scale - Data out of range. Must be between 0 and 65535.
        /// </summary>
        VelocityScaleInvalid = 29,

        /// <summary>
        /// Load Event-Triggered Instruction - Data out of range. See command #30 for valid range.
        /// </summary>
        LoadEventInvalid = 30,

        /// <summary>
        /// Return Event-Triggered Instruction - Data out of range. See command #31 and #30 for valid range.
        /// </summary>
        ReturnEventInvalid = 31,

        /// <summary>
        /// Must be 0, 1, or 2.
        /// </summary>
        CalibrationModeInvalid = 33,

        /// <summary>
        /// Peripheral Id is invalid or not supported.
        /// </summary>
        PeripheralIdInvalid = 36,

        /// <summary>
        /// Microstep resolution not supported.
        /// </summary>
        ResolutionInvalid = 37,

        /// <summary>
        /// Run current out of range.
        /// </summary>
        RunCurrentInvalid = 38,

        /// <summary>
        /// Hold current out of range.
        /// </summary>
        HoldCurrentInvalid = 39,

        /// <summary>
        /// Set Device Mode - one or more of the mode bits is invalid.
        /// </summary>
        ModeInvalid = 40,

        /// <summary>
        /// Home speed out of range. The range of home speed is determined by the resolution.
        /// </summary>
        HomeSpeedInvalid = 41,

        /// <summary>
        /// Target speed out of range. The range of target speed is determined by the resolution.
        /// </summary>
        SpeedInvalid = 42,

        /// <summary>
        /// Target acceleration out of range.
        /// </summary>
        AccelerationInvalid = 43,

        /// <summary>
        /// Maximum position out of range.
        /// </summary>
        MaximumPositionInvalid = 44,

        /// <summary>
        /// Identical to MaximumPositionInvalid, this is just for backward 
        /// compatibility. 
        /// </summary>
        MaximumRangeInvalid = 44,

        /// <summary>
        /// Current position out of range.
        /// </summary>
        CurrentPositionInvalid = 45,

        /// <summary>
        /// Max relative move out of range. Must be between 0 and 16,777,215.
        /// </summary>
        MaximumRelativeMoveInvalid = 46,

        /// <summary>
        /// Home offset out of range.
        /// </summary>
        OffsetInvalid = 47,

        /// <summary>
        /// Alias out of range.
        /// </summary>
        AliasInvalid = 48,

        /// <summary>
        /// Lock state must be 1 (locked) or 0 (unlocked).
        /// </summary>
        LockStateInvalid = 49,

        /// <summary>
        /// The device id is not included in the firmware's list.
        /// </summary>
        DeviceIdUnknown = 50,

        /// <summary>
        /// Return Setting - data entered is not a valid setting command number. Valid data are the command numbers of any "Set..." or "Return..." instructions.
        /// </summary>
        SettingInvalid = 53,

        /// <summary>
        /// Command number not valid in this firmware version.
        /// </summary>
        CommandInvalid = 64,

        /// <summary>
        /// Set Park State - State must be 0 or 1, or device cannot park because it is in motion.
        /// </summary>
        ParkStateInvalid = 65,

        /// <summary>
        /// High temperature is detected inside device. Device may be overheating.
        /// </summary>
        TemperatureHigh = 67,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        AutoReplyDisabledModeInvalid = 101,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        MessageIdModeInvalid = 102,

        /// <summary>
        /// Status must be 0 or 1.
        /// </summary>
        HomeStatusInvalid = 103,

        /// <summary>
        /// Type must be 0 or 1.
        /// </summary>
        HomeSensorTypeInvalid = 104,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        AutoHomeDisabledModeInvalid = 105,

        /// <summary>
        /// Minimum position out of range.
        /// </summary>
        MinimumPositionInvalid = 106,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        KnobDisabledModeInvalid = 107,

        /// <summary>
        /// Direction must be 0 or 1.
        /// </summary>
        KnobDirectionInvalid = 108,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        KnobMovementModeInvalid = 109,

        /// <summary>
        /// Maximum knob speed out of range. The range of valid speed is determined by the resolution.
        /// </summary>
        KnobVelocityScaleInvalid = 111,

        /// <summary>
        /// Profile must be 1 (Linear), 2 (Quadratic), or 3 (Cubic).
        /// </summary>
        KnobVelocityProfileInvalid = 112,

        /// <summary>
        /// Acceleration out of range.
        /// </summary>
        AccelerationOnlyInvalid = 113,

        /// <summary>
        /// Deceleration out of range.
        /// </summary>
        DecelerationOnlyInvalid = 114,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        MoveTrackingModeInvalid = 115,

        /// <summary>
        /// Mode must be 0 or 1.
        /// </summary>
        ManualMoveTrackingDisabledModeInvalid = 116,

        /// <summary>
        /// Valid range is 10 - 65535.
        /// </summary>
        MoveTrackingPeriodInvalid = 117,

        /// <summary>
        /// Valid modes are 0-6.
        /// </summary>
        ClosedLoopModeInvalid = 118,

        /// <summary>
        /// Valid range is 0(Off), 10 - 65535.
        /// </summary>
        SlipTrackingPeriodInvalid = 119,

        /// <summary>
        /// Valid range is 0 - 65535.
        /// </summary>
        StallTimeoutInvalid = 120,

        /// <summary>
        /// Direction must be 0 or 1.
        /// </summary>
        DeviceDirectionInvalid = 121,

        /// <summary>
        /// Set Baudrate - Value not supported.
        /// </summary>
        BaudrateInvalid = 122,

        /// <summary>
        /// Set Protocol - Value not supported.
        /// </summary>
        ProtocolInvalid = 123,

        /// <summary>
        /// Set Baudrate and ASCII Protocol - Value not supported.
        /// </summary>
        BaudrateOrProtocolInvalid = 124,

        /// <summary>
        /// Another command is executing and cannot be pre-empted. Either stop the previous command or wait until it finishes before trying again.
        /// </summary>
        Busy = 255,

        /// <summary>
        /// Write Register - Register address invalid.
        /// </summary>
        RegisterAddressInvalid = 701,

        /// <summary>
        /// Write Register - Value out of range.
        /// </summary>
        RegisterValueInvalid = 702,

        /// <summary>
        /// Save Current Position register out of range (must be 0-15).
        /// </summary>
        SavePositionInvalid = 1600,

        /// <summary>
        /// Save Current Position is not allowed unless the device has been homed.
        /// </summary>
        SavePositionNotHomed = 1601,

        /// <summary>
        /// Return Stored Position register out of range (must be 0-15).
        /// </summary>
        ReturnPositionInvalid = 1700,

        /// <summary>
        /// Move to Stored Position register out of range (must be 0-15).
        /// </summary>
        MovePositionInvalid = 1800,

        /// <summary>
        /// Move to Stored Position is not allowed unless the device has been homed.
        /// </summary>
        MovePositionNotHomed = 1801,

        /// <summary>
        /// Move Relative (command 20) exceeded maximum relative move range. Either move a shorter distance, or change the maximum relative move (command 46).
        /// </summary>
        RelativePositionLimited = 2146,

        /// <summary>
        /// Must clear Lock State (command 49) first. See the Set Lock State command for details.
        /// </summary>
        SettingsLocked = 3600,

        /// <summary>
        /// Set Device Mode - bit 1 is reserved in this device and must be 0.
        /// </summary>
        Bit1Invalid = 4001,

        /// <summary>
        /// Set Device Mode - bit 2 is reserved in this device and must be 0.
        /// </summary>
        Bit2Invalid = 4002,

        /// <summary>
        /// Set Device Mode - this is a linear actuator; Disable Auto Home is used for rotary actuators only.
        /// </summary>
        DisableAutoHomeInvalid = 4008,

        /// <summary>
        /// Set Device Mode - bit 10 is reserved in this device and must be 0.
        /// </summary>
        Bit10Invalid = 4010,

        /// <summary>
        /// Set Device Mode - bit 11 is reserved in this device and must be 0.
        /// </summary>
        Bit11Invalid = 4011,

        /// <summary>
        /// Set Device Mode - this device has integrated home sensor with preset polarity; mode bit 12 cannot be changed by the user.
        /// </summary>
        HomeSwitchInvalid = 4012,

        /// <summary>
        /// Set Device Mode - bit 13 is reserved in this device and must be 0.
        /// </summary>
        Bit13Invalid = 4013,

        /// <summary>
        /// Set Device Mode - bit 14 is reserved in this device and must be 0.
        /// </summary>
        Bit14Invalid = 4014,

        /// <summary>
        /// Set Device Mode - bit 15 is reserved in this device and must be 0.
        /// </summary>
        Bit15Invalid = 4015,

        /// <summary>
        /// Device is currently parked. Use Set Park State or Home to unpark device before requesting a move.
        /// </summary>
        DeviceParked = 6501
    }
}
