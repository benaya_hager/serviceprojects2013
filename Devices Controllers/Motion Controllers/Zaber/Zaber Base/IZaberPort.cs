using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Zaber
{
    /// <summary>
    /// Represents a communication port with Zaber devices attached, and
    /// translates between the byte streams and the DataPacket structure.
    /// </summary>
    public interface IZaberPort : IDisposable
    {
        /// <summary>
        /// Get a list of all available ports.
        /// </summary>
        /// <returns>An array of port names, one of which should be passed to 
        /// <see cref="Open"/>.</returns>
        string[] GetPortNames();

        /// <summary>
        /// Raised when the port receives data back from the devices. When a complete
        /// response has been received, it's converted to a <see cref="DataPacket"/> object and
        /// sent through this event.
        /// </summary>
        /// <example>
        /// Register an event handler and print any received data to the 
        /// output console.
        /// <code>
        /// private void MyForm_Load(object sender, EventArgs e)
        /// {
        ///     port.DataPacketReceived += 
        ///         new EventHandler&lt;DataPacketEventArgs&gt;(port_DataPacketReceived);
        /// }
        ///
        /// void port_DataPacketReceived(object sender, DataPacketEventArgs e)
        /// {
        ///     System.Console.Out.WriteLine(
        ///         "Device {0}: {1}({2})",
        ///         e.Data.DeviceNumber,
        ///         e.Data.Command,
        ///         e.Data.Data);
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// <para>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. The simplest way to deal with that is not
        /// to register for this event at all. Use a <see cref="Conversation"/>
        /// to coordinate requests and responses instead. It handles all the
        /// threading issues and just returns the response as the return
        /// value from <see cref="Conversation.Request(Command, int)"/>.
        /// </para>
        /// <para>If you do want to handle this event safely in your user 
        /// interface, read the first two sections of this article:
        /// http://weblogs.asp.net/justin_rogers/articles/126345.aspx
        /// It shows how to use <c>Control.InvokeRequired</c> and 
        /// <c>Control.BeginInvoke</c> to move execution back onto the UI
        /// thread from a background thread.
        /// </para>
        /// </remarks>
        event EventHandler<DataPacketEventArgs> DataPacketReceived;

        /// <summary>
        /// Raised when the port sends data to the devices. 
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is occasionally 
        /// raised from a background thread. See 
        /// <see cref="DataPacketReceived"/> for details. Some scenarios where
        /// it can be raised from a background thread are: executing scripts 
        /// and automatically adjusting message id mode.
        /// </remarks>
        event EventHandler<DataPacketEventArgs> DataPacketSent;

        /// <summary>
        /// Raised when the underlying port raises its own <c>ErrorReceived</c> 
        /// event, or when a partial data packet is received because some bytes
        /// have been dropped. The type of error is described by
        /// <see cref="ZaberPortErrorReceivedEventArgs.ErrorType"/>.
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. See <see cref="DataPacketReceived"/> for 
        /// details.
        /// </remarks>
        event EventHandler<ZaberPortErrorReceivedEventArgs> ErrorReceived;

        /// <summary>
        /// Open the port to begin sending and receiving data. Be sure to call 
        /// <see cref="Close"/> or <see cref="IDisposable.Dispose()"/> when 
        /// you are finished with the port.
        /// </summary>
        /// <param name="portName">Should match one of the entries returned from
        /// <see cref="GetPortNames"/></param>
        /// <exception cref="UnauthorizedAccessException">Access is denied to
        /// the port.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Open(string portName);

        /// <summary>
        /// Is the port open?
        /// </summary>
        bool IsOpen
        {
            get;
        }

        /// <summary>
        /// Flag that gets or sets whether checksums are sent with each text 
        /// message. If true, the checksum will be calculated with the
        /// Longitudinal Redundancy Check (LRC) algorithm.
        /// </summary>
        bool AreChecksumsSent
        {
            get;
            set;
        }

        /// <summary>
        /// Flag that gets or sets whether the port is in ASCII mode.
        /// If it's not in ASCII mode, then all received data will be
        /// parsed as binary, 6-byte packets.
        /// </summary>
        bool IsAsciiMode { get; set; }

        /// <summary>
        /// Gets the port name that was last sent to <see cref="Open"/>.
        /// </summary>
        /// <value>The port name that was last sent to <see cref="Open"/>, or
        /// null if <see cref="Open"/> has not been called.</value>
        string PortName
        {
            get;
        }

        /// <summary>
        /// Convenience method for commands that ignore the data value. 
        /// It sends 0 as the data value.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Send(byte deviceNumber, Command command);

        /// <summary>
        /// Send a command to a device on the chain.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Send(byte deviceNumber, Command command, Int32 data);

        /// <summary>
        /// Send a command to a device on the chain.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="measurement">An optional measurement that the data 
        /// value was calculated from. May be null.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Send(
            byte deviceNumber,
            Command command,
            Int32 data,
            Measurement measurement);

        /// <summary>
        /// Send an ASCII command to a device on the chain.
        /// </summary>
        /// <param name="text">See <see cref="DataPacket.Text"/>.</param>
        /// <param name="measurement">An optional measurement that the 
        /// command's data value was calculated from. May be null.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Send(
            String text,
            Measurement measurement);

        /// <summary>
        /// Send a text-mode command to a device on the chain.
        /// </summary>
        /// <param name="text">The full command to send to the device.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Send(String text);

        /// <summary>
        /// Send a command to a device on the chain after a delay.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <returns>The data packet that will be sent. You can pass this object
        /// to <see cref="CancelDelayedPacket"/>.</returns>
        /// <remarks>Wait for <see cref="DelayMilliseconds"/> before sending
        /// the command. If you call this method several times, the commands
        /// get queued up and sent one at a time with a delay before each one.
        /// </remarks>
        DataPacket SendDelayed(byte deviceNumber, Command command, int data);

        /// <summary>
        /// Get or set the delay period to use with <see cref="SendDelayed"/>.
        /// Defaults to 500.
        /// </summary>
        int DelayMilliseconds { get; set; }

        /// <summary>
        /// Cancel a delayed packet that is waiting to be sent.
        /// </summary>
        /// <param name="packet">Returned by <see cref="SendDelayed"/>.</param>
        /// <returns>True if the packet will not be sent, false if it has 
        /// already been sent.</returns>
        bool CancelDelayedPacket(DataPacket packet);

        /// <summary>
        /// Close and release the serial port. This must be called to avoid
        /// locking the port when you are finished with it.
        /// </summary>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        void Close();

        /// <summary>
        /// Report that an invalid packet was received by raising the 
        /// <see cref="ErrorReceived"/> event.
        /// </summary>
        /// <remarks>This can be called by any higher-level code that decides
        /// a packet was invalid. An example would be if the device number
        /// doesn't match any known devices.</remarks>
        void ReportInvalidPacket();

        /// <summary>
        /// Synchronization lock that controls when data packets are sent to
        /// the port.
        /// </summary>
        /// <remarks>If you lock this, then no data packets will be sent until
        /// you release it.
        /// </remarks>
        object SendLock { get; }
    }
}
