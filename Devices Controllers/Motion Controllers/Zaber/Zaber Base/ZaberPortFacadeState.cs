﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Lists the possible states that the <see cref="ZaberPortFacade"/> can be in.
    /// </summary>
    public enum ZaberPortFacadeState
    {
        /// <summary>
        /// Starting state. The only things available are a device 
        /// collection and conversation collection for all devices
        /// (device number 0).
        /// </summary>
        Closed,
        /// <summary>
        /// COM port is open, but the devices are still being queried,
        /// still nothing available beyond device 0.
        /// </summary>
        QueryingDevices,
        /// <summary>
        /// Everything is now available.
        /// </summary>
        Open
    }

}
