using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Zaber
{
    /// <summary>
    /// This exception is thrown by a <see cref="Conversation"/> when
    /// a request gets replaced before responding.
    /// </summary>
    [Serializable]
    public class RequestReplacedException : ConversationException
    {
        private const string DEFAULT_MESSAGE = 
            "Current request was replaced by a new request.";
        private DeviceMessage replacementResponse;

        public RequestReplacedException()
            : base(DEFAULT_MESSAGE)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public RequestReplacedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this one</param>
        public RequestReplacedException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected RequestReplacedException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            replacementResponse = 
                (DeviceMessage)info.GetValue("replacementResponse", typeof(DeviceMessage));
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="replacementResponse">The response that replaced the expected one.</param>
        public RequestReplacedException(DeviceMessage replacementResponse) 
            : base(DEFAULT_MESSAGE)
        {
            this.replacementResponse = replacementResponse;
        }

        /// <summary>
        /// The response to the command that replaced the current command.
        /// </summary>
        public DeviceMessage ReplacementResponse
        {
            get { return replacementResponse; }
        }

        /// <summary>
        /// Sets the System.Runtime.Serialization.SerializationInfo
        ///     with information about the exception.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info, 
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);

            info.AddValue("replacementResponse", replacementResponse);
        }
    }
}
