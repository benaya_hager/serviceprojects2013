using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Permissions;
using System.Runtime.Serialization;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// Exception thrown by a <see cref="Conversation"/> when the response is
    /// an error.
    /// </summary>
    [Serializable]
    public class ErrorResponseException : ConversationException
    {
        private DeviceMessage response;

        public ErrorResponseException()
            : base("Error response received.")
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        public ErrorResponseException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="ex">The exception that caused this exception</param>
        public ErrorResponseException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance with serialized
        ///     data.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        protected ErrorResponseException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            response = 
                (DeviceMessage)info.GetValue("response", typeof(DeviceMessage));
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="response">The response that caused the exception</param>
        public ErrorResponseException(DeviceMessage response) : 
            base(BuildMessage(response))
        {
            this.response = response;
        }

        private static string BuildMessage(DeviceMessage response)
        {
            if (response.Text != null)
            {
                return String.Format(
                    CultureInfo.CurrentCulture,
                    "Error response: '{0}'.",
                    response.Text);
            }
            return String.Format(
                CultureInfo.CurrentCulture,
                "Error response {0} received from device number {1}.",
                (ZaberError)response.Data,
                response.DeviceNumber);
        }

        /// <summary>
        /// The response message from the device.
        /// </summary>
        public DeviceMessage Response
        {
            get { return response; }
        }

        /// <summary>
        /// Sets the System.Runtime.Serialization.SerializationInfo
        ///     with information about the exception.
        /// </summary>
        /// <param name="info">The System.Runtime.Serialization.SerializationInfo that holds the serialized
        ///     object data about the exception being thrown.</param>
        /// <param name="context">The System.Runtime.Serialization.StreamingContext that contains contextual
        ///     information about the source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is a null reference.</exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info,
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);

            info.AddValue("response", response);
        }
    }
}
