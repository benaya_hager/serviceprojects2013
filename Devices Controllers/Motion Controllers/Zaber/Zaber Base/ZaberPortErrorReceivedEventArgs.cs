using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Returns a response from one of the devices in the chain.
    /// </summary>
    public class ZaberPortErrorReceivedEventArgs : EventArgs
    {
        private ZaberPortError errorType;

        /// <summary>
        /// The type of error that raised the event.
        /// </summary>
        public ZaberPortError ErrorType
        {
            get { return errorType; }
            set { errorType = value; }
        }

        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="errorType">The type of error that raised this event</param>
        public ZaberPortErrorReceivedEventArgs(ZaberPortError errorType)
        {
            this.errorType = errorType;
        }
    }
}
