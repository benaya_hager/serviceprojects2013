﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zaber
{
    /// <summary>
    /// Different kinds of measurement represented by the data in different 
    /// commands. This is used to help convert units of measure.
    /// </summary>
    public enum MeasurementType
    {
        /// <summary>
        /// Other types of measurement that don't support unit of measure 
        /// conversion.
        /// </summary>
        Other,
        /// <summary>
        /// Position a device can move to.
        /// </summary>
        Position,
        /// <summary>
        /// Velocity a device can move at.
        /// </summary>
        Velocity,
        /// <summary>
        /// Acceleration a device can apply.
        /// </summary>
        Acceleration
    }
}
