using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// A collection of all the data that gets sent to and received from the
    /// T-Series devices.
    /// </summary>
    /// <remarks>
    /// This class is mostly used inside the Zaber library. It converts
    /// the commands to byte streams and back again. The place that client
    /// programs will see it is in the <see cref="TSeriesPort.DataPacketReceived"/>
    /// event handler.
    /// </remarks>
    /// <example>
    /// Receive a response
    /// <code>
    /// void port_DataPacketReceived(object sender, DataPacketReceivedEventArgs e)
    /// {
    ///     System.Console.Out.WriteLine(
    ///         "Device {0}: {1}({2})",
    ///         e.Data.DeviceNumber,
    ///         e.Data.Command,
    ///         e.Data.Data);
    /// }
    /// </code>
    /// </example>
    [Serializable]
    public class DataPacket
    {
        private byte deviceNumber;
        private Command command;
        private Int32 data;

        /// <summary>
        /// Initialize the data structure with all values set to zero.
        /// </summary>
        public DataPacket()
        {
        }

        /// <summary>
        /// Parse a text message into a data packet.
        /// </summary>
        /// <param name="text">parse details from this text</param>
        /// <remarks>
        /// Expected format is @dd a ff ssss ww xxxx where dd is the device
        /// number, a is the axis number, ff is a flag (OK or RJ for reject),
        /// ssss is the state (IDLE or BUSY), ww is the top warning, and
        /// xxxx is a list of one or more data values.
        /// </remarks>
        public DataPacket(String text)
        {
            Text = text;
            
            // Extract message header
            if (text.StartsWith("!", StringComparison.Ordinal))
            {
                MessageType = MessageType.Alert;
            }
            else if (text.StartsWith("#", StringComparison.Ordinal))
            {
                MessageType = MessageType.Comment;
            }
            else if (text.StartsWith("/", StringComparison.Ordinal))
            {
                MessageType = MessageType.Request;
            }
            else if (text.StartsWith("@", StringComparison.Ordinal))
            {
                MessageType = MessageType.Response;
            }
            else
            {
                MessageType = MessageType.Unknown;
            }

            string content;
            if (text.Length < 4 || text[text.Length-3] != ':')
            {
                content = text.Substring(1);
            }
            else
            {
                content = text.Substring(1, text.Length - 4);
                var checksumText = text.Substring(text.Length - 2);
                byte checksum;
                IsChecksumInvalid = ! Byte.TryParse(
                    checksumText,
                    NumberStyles.HexNumber,
                    CultureInfo.InvariantCulture,
                    out checksum);
                if ( ! IsChecksumInvalid)
                {
                    foreach (byte c in Encoding.ASCII.GetBytes(content))
                    {
                        checksum = (byte)((checksum + c) & 0xFF);
                    }
                    IsChecksumInvalid = checksum != 0;
                }
            }
            char[] delimiters = new char[] { ' ', '\r', '\n' };
            string[] tokens;

            // Extract device number
            tokens = content.Split(
                delimiters, 
                2, 
                StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length > 0
                && Byte.TryParse(tokens[0], out deviceNumber))
            {
                if (tokens.Length > 1)
                {
                    Body = tokens[1];
                }
                else
                {
                    Body = "";
                }
            }
            else
            {
                Body = text.Substring(1);
            }

            // Extract axis number
            int axisNumber;
            tokens = Body.Split(
                delimiters,
                2,
                StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length > 0
                && Int32.TryParse(tokens[0], out axisNumber))
            {
                AxisNumber = axisNumber;
                if (tokens.Length > 1)
                {
                    Body = tokens[1];
                }
                else
                {
                    Body = "";
                }
            }

            // Extract reply contents
            if (MessageType == MessageType.Response)
            {
                tokens = Body.Split(delimiters, 4, StringSplitOptions.RemoveEmptyEntries);

                var flag = tokens[0];
                IsError = flag.Equals("RJ");
                var status = tokens[1];
                IsIdle = status.Equals("IDLE");
                FlagText = tokens[2];
                HasFault = FlagText.StartsWith(
                    "F", 
                    StringComparison.OrdinalIgnoreCase);
                TextData = tokens[3];
                IsAgain = IsError && tokens[3].Equals("AGAIN");
                TextDataValues = TextData.Split(
                    delimiters, 
                    StringSplitOptions.RemoveEmptyEntries).ToList().AsReadOnly();
                var dataValuesList = new List<Int32>();
                for (int i = 0; i < TextDataValues.Count; i++)
                {
                    int dataValue;
                    Int32.TryParse(TextDataValues[i], out dataValue);
                    dataValuesList.Add(dataValue);
                }
                DataValues = dataValuesList.AsReadOnly();
                data = DataValues[0];
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">copy details from this packet</param>
        public DataPacket(DataPacket source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            deviceNumber = source.deviceNumber;
            AxisNumber = source.AxisNumber;
            command = source.command;
            data = source.data;
            Measurement = source.Measurement;
            Body = source.Body;
            IsError = source.IsError;
            IsAgain = source.IsAgain;
            MessageType = source.MessageType;
            Text = source.Text;
            FlagText = source.FlagText;
            HasFault = source.HasFault;
            TextData = source.TextData;
            TextDataValues = source.TextDataValues;
            DataValues = source.DataValues;
            IsIdle = source.IsIdle;
            IsChecksumInvalid = source.IsChecksumInvalid;
        }

        /// <summary>
        /// Data value to go with the command
        /// </summary>
        /// <value>
        /// Any integer value, positive or negative. Details depend on the
        /// different command values. See the user manual.
        /// </value>
        /// <remarks>For ASCII responses with multiple data values, this
        /// will hold the first data value. <see cref="DataValues"/> will hold
        /// all the data values. If the data value is text, then this will
        /// be zero.</remarks>
        public Int32 Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// An array of data values to go with the command
        /// </summary>
        /// <value>An array of integer values, positive or negative. Deatils
        /// depend on the different commands. See the user manual.</value>
        /// <remarks>If any of the data values are text, then the corresponding
        /// entry will be zero. <see cref="TextDataValues"/> will hold the
        /// text version of each data value.</remarks>
        public IList<Int32> DataValues { get; private set; }

        /// <summary>
        /// Gets or sets a measurement that represents the data value in 
        /// another unit of measure.
        /// </summary>
        public Measurement Measurement { get; set; }

        /// <summary>
        /// The specific command to execute. See the user manual for a list of 
        /// supported commands and the data values they support. Some common
        /// commands are listed in the <see cref="Zaber.Command"/> enumeration.
        /// </summary>
        /// <value>
        /// You can assign any byte value, but you have to cast it to a Command
        /// type first.
        /// </value>
        /// <example>
        /// Use one of the values from the Command enumeration.
        /// <code>
        /// DataPacket data = new DataPacket();
        /// data.DeviceNumber = 3;
        /// data.Command = Command.ConstantSpeed;
        /// data.Data = 100;
        /// </code>
        /// </example>
        /// <example>
        /// Cast a byte value to a command value.
        /// <code>
        /// byte commandFromUser = 43; // Maybe these get read from a form
        /// int dataFromUser = 100;
        /// 
        /// DataPacket data = new DataPacket();
        /// data.DeviceNumber = 3;
        /// data.Command = (Command)commandFromUser;
        /// data.Data = dataFromUser;
        /// </code>
        /// </example>
        public Command Command
        {
            get { return command; }
            set 
            { 
                command = value;
                IsError = command == Command.Error;
            }
        }

        /// <summary>
        /// The device number in the chain that this command should be sent to 
        /// or was received from.
        /// </summary>
        /// <remarks>
        /// You can send a command directly to a device, or send a command to 
        /// an alias device number. 0 is always an alias for all devices, and 
        /// each device can set another alias number. Several devices can use 
        /// the same alias, and all of them will respond to a command using
        /// the alias device number.
        /// </remarks>
        public byte DeviceNumber
        {
            get { return deviceNumber; }
            set { deviceNumber = value; }
        }

        /// <summary>
        /// Gets or sets the axis number on the device that this command
        /// should be sent to or was received from.
        /// </summary>
        /// <remarks>
        /// You can send a command directly to an axis, or send a command
        /// to all axes on the device. Axis number 0 represents all axes
        /// on the device.
        /// </remarks>
        public int AxisNumber { get; set; }

        /// <summary>
        /// Format the data packet as a string, assuming that it was sent as a 
        /// request.
        /// </summary>
        /// <returns>The message as a string</returns>
        public virtual string FormatRequest()
        {
            return Format("requests");
        }

        /// <summary>
        /// Format the message as a string, assuming that it was sent as a 
        /// response.
        /// </summary>
        /// <returns>The message as a string</returns>
        public virtual string FormatResponse()
        {
            return Format("responds");
        }

        /// <summary>
        /// Gets a flag showing if this packet is an error message.
        /// </summary>
        public bool IsError { get; private set; }

        /// <summary>
        /// Gets a flag showing if this packet is an error message with the AGAIN response.
        /// </summary>
        public bool IsAgain { get; private set; }

        /// <summary>
        /// Gets a flag showing if this packet has a warning flag that is a
        /// fault.
        /// </summary>
        public bool HasFault { get; private set; }

        /// <summary>
        /// Gets the data field from a text-mode response, otherwise null.
        /// </summary>
        public String TextData { get; private set; }

        /// <summary>
        /// Gets the flag field from a text-mode response, otherwise null.
        /// </summary>
        public String FlagText { get; private set; }

        /// <summary>
        /// Gets the individual data values from a text-mode response, 
        /// otherwise null.
        /// </summary>
        /// <value>The data values are split by spaces and stored here.</value>
        public IList<String> TextDataValues { get; private set; }

        /// <summary>
        /// Gets a flag showing if the device is idle.
        /// </summary>
        public bool IsIdle { get; private set; }

        /// <summary>
        /// Gets the type of message.
        /// </summary>
        public MessageType MessageType { get; private set; }

        /// <summary>
        /// Gets the original text of the message, or null for binary messages.
        /// </summary>
        public String Text { get; private set; }

        /// <summary>
        /// Gets part of the original text of the message, without the start
        /// marker, device number, or check sum. Only available for text 
        /// responses, null for everything else.
        /// </summary>
        public String Body { get; private set; }

        private string Format(
            string action)
        {
            if (Text != null)
            {
                return Text;
            }
            string deviceNumberText =
                DeviceNumber.ToString(CultureInfo.CurrentCulture).PadLeft(3);

            return String.Format(
                CultureInfo.CurrentCulture,
                "Device {0} {3} {1}({2})",
                deviceNumberText,
                command,
                data,
                action);
        }

        /// <summary>
        /// Gets a flag showing whether the message had a checksum that didn't
        /// match its content.
        /// </summary>
        public bool IsChecksumInvalid { get; private set; }

        /// <summary>
        /// Throws an exception if the checksum was invalid.
        /// </summary>
        /// <exception cref="ZaberPortErrorException">When the checksum was 
        /// invalid.</exception>
        public void ValidateChecksum()
        {
            if (IsChecksumInvalid)
            {
                throw new ZaberPortErrorException(
                        ZaberPortError.InvalidChecksum);
            }
        }
    }
}
