using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Globalization;
using log4net;
using System.Diagnostics;

namespace Zaber
{
    /// <summary>
    /// This class is used inside the library to convert between the serial port
    /// byte stream and the <see cref="DataPacket"/> data structure. It also
    /// times out when a byte gets 
    /// dropped and a partial packet is left in the serial port for too long.
    /// </summary>
    public class PacketConverter : IDisposable
    {
        /// <summary>
        /// The size of the byte array that the data gets converted from
        /// and to.
        /// </summary>
        public const int PacketSize = 6;

        private static ILog log = LogManager.GetLogger(typeof(PacketConverter));
        private const int Infinite = System.Threading.Timeout.Infinite;

        private Timer timer;
        private int millisecondsTimeout;
        private int bytesRead;
        private byte[] buffer = new byte[PacketSize];
        private StringBuilder textBuffer = new StringBuilder();
        private TraceInfo[] ringBuffer;
        private int ringIndex;
        private Stopwatch stopwatch;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        public PacketConverter() :
            this(log.IsDebugEnabled ? 10 : 0) // history of 10 when debugging
        {
        }

        /// <summary>
        /// Create a new instance and record packet history.
        /// </summary>
        /// <param name="historyCount">The number of packets to record.</param>
        /// <remarks>Use <see cref="FormatHistory"/> to retrieve the
        /// packet history.</remarks>
        public PacketConverter(int historyCount)
        {
            timer = new Timer(new TimerCallback(timer_Timeout));
            if (historyCount > 0)
            {
                ringBuffer = new TraceInfo[historyCount * PacketSize];
                stopwatch = new Stopwatch();
                stopwatch.Start();
            }
        }

        /// <summary>
        /// Raised when a partial packet is received but not completed before
        /// the <see cref="MillisecondsTimeout"/> expires.
        /// </summary>
        public event EventHandler Timeout;

        /// <summary>
        /// Raised when a port error is detected, including partial packet 
        /// timeouts.
        /// </summary>
        public event EventHandler<ZaberPortErrorReceivedEventArgs> PortError;

        /// <summary>
        /// Raised when a complete packet of <see cref="PacketSize"/> bytes is
        /// received. The packet is included in the event arguments.
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. See 
        /// <see cref="IZaberPort.DataPacketReceived"/> for details.
        /// </remarks>
        public event EventHandler<DataPacketEventArgs> DataPacketReceived;

        /// <summary>
        /// The length of time to wait after calling <see cref="StartTimer"/> 
        /// before raising the <see cref="Timeout"/> event.
        /// </summary>
        public int MillisecondsTimeout
        {
            get { return millisecondsTimeout; }
            set { millisecondsTimeout = value; }
        }

        /// <summary>
        /// Flag that gets or sets whether the converter is in ASCII mode.
        /// If it's not in ASCII mode, then all received data will be
        /// parsed as binary, 6-byte packets.
        /// </summary>
        public bool IsAsciiMode { get; set; }

        /// <summary>
        /// Add another byte to the incoming byte stream. If this byte 
        /// completes a packet, then the <see cref="DataPacketReceived"/> event
        /// will be raised.
        /// </summary>
        /// <remarks>
        /// Be sure to call <see cref="StartTimer"/> after you finish calling 
        /// this method for all incoming bytes.
        /// </remarks>
        /// <param name="value">The byte to receive</param>
        public void ReceiveByte(byte value)
        {
            if (ringBuffer != null)
            {
                RecordRingBuffer(value);
            }

            timer.Change(Infinite, Infinite); // Turn off timer

            lock (buffer)
            {
                DataPacket data = null;
                if (IsAsciiMode)
                {
                    if (value == '\r')
                    {
                        // ignore it
                    }
                    else if (value == '\n')
                    {
                        data = new DataPacket(textBuffer.ToString());
                        if (data.IsChecksumInvalid)
                        {
                            data = null;
                            if (PortError != null)
                            {
                                PortError(
                                    this, 
                                    new ZaberPortErrorReceivedEventArgs(
                                        ZaberPortError.InvalidChecksum));
                            }
                        }
                        textBuffer = new StringBuilder();
                    }
                    else
                    {
                        if (textBuffer.Length > 0 && 
                            "@#!".IndexOf((char)value) != -1)
                        {
                            textBuffer = new StringBuilder();
                        }
                        textBuffer.Append((char)value);
                    }
                }
                else
                {
                    buffer[bytesRead++] = value;
                    if (bytesRead == PacketSize)
                    {
                        data = BuildDataPacket();
                        bytesRead = 0;
                    }
                }
                if (data != null && DataPacketReceived != null)
                {
                    DataPacketReceived(this, new DataPacketEventArgs(data));
                }
            }
        }

        private void RecordRingBuffer(byte value)
        {
            ringBuffer[ringIndex].milliseconds = ReadStopwatch();
            ringBuffer[ringIndex].data = value;

            ringIndex = (ringIndex + 1) % ringBuffer.Length;
        }

        /// <summary>
        /// Call this when you've finished calling <see cref="ReceiveByte"/>.
        /// If there is a partial packet remaining, the timer will start. A
        /// <see cref="Timeout"/> event will get triggered if 
        /// <see cref="ReceiveByte"/> doesn't get called again before the timeout 
        /// period elapses.
        /// </summary>
        public void StartTimer()
        {
            lock (buffer)
            {
                if (bytesRead > 0 || textBuffer.Length > 0)
                {
                    timer.Change(millisecondsTimeout, Infinite);
                }
            }
        }

        private void timer_Timeout(Object context)
        {
            lock (buffer)
            {
                if (bytesRead == 0 && textBuffer.Length == 0)
                {
                    // sometimes the timer goes off just before
                    // more bytes come in.
                    return;
                }

                if (log.IsDebugEnabled && ringBuffer != null)
                {
                    log.Debug(
                        FormatPartialPacket() +
                        " after " +
                        FormatHistory());
                }
                bytesRead = 0;
                if (textBuffer.Length > 0)
                {
                    textBuffer = new StringBuilder();
                }
                if (Timeout != null)
                {
                    Timeout(this, new EventArgs());
                }
                if (PortError != null)
                {
                    PortError(
                        this, 
                        new ZaberPortErrorReceivedEventArgs(
                            ZaberPortError.PacketTimeout));
                }
            }
        }

        /// <summary>
        /// Format the contents of any partial packet as a string of 
        /// hexadecimal digits.
        /// </summary>
        /// <remarks>This is mostly useful for testing purposes.</remarks>
        /// <returns>A string of hexadecimal digits, possibly empty.</returns>
        public string FormatPartialPacket()
        {
            StringBuilder msg = new StringBuilder();
            msg.Append("Partial packet: 0x");
            for (int i = 0; i < bytesRead; i++)
            {
                msg.Append(buffer[i].ToString(
                    "X2", // 2 digit hexadecimal in caps
                    CultureInfo.CurrentCulture));
            }
            return msg.ToString();
        }

        /// <summary>
        /// Read the current time from the stop watch.
        /// </summary>
        /// <returns>The elapsed time in milliseconds</returns>
        /// <remarks>This method is just to facilitate testing.</remarks>
        protected virtual long ReadStopwatch()
        {
            return stopwatch.ElapsedMilliseconds;
        }

        /// <summary>
        /// Format the history of the last few bytes received along with
        /// the timing.
        /// </summary>
        /// <returns>The formatted history.</returns>
        /// <exception cref="InvalidOperationException">when no history count
        /// was specified for this object at construction.</exception>
        public string FormatHistory()
        {
            string history;
            if (ringBuffer == null)
            {
                throw new InvalidOperationException(
                    "History is not enabled.");
            }
            else
            {
                long elapsedTime = ReadStopwatch();
                long previousTime = ringBuffer[ringIndex].milliseconds;

                StringBuilder ringDump = new StringBuilder();
                ringDump.AppendFormat(
                    "{0}ms since last byte. The last {1} bytes and times(with delta) were:",
                    elapsedTime,
                    ringBuffer.Length);
                ringDump.AppendLine();
                for (int i = 0; i < ringBuffer.Length; i++)
                {
                    TraceInfo traceInfo =
                        ringBuffer[(ringIndex + i) % ringBuffer.Length];
                    ringDump.AppendFormat(
                        "{1:X2} {0}({2})",
                        traceInfo.milliseconds,
                        traceInfo.data,
                        traceInfo.milliseconds - previousTime);
                    ringDump.AppendLine();
                    if ((i + ringIndex + 1) % PacketSize == 0)
                    {
                        ringDump.AppendLine("---");
                    }
                    previousTime = traceInfo.milliseconds;
                }
                history = ringDump.ToString();
            }
            return history;
        }

        /// <summary>
        /// Convert a <see cref="DataPacket"/> to a byte stream.
        /// </summary>
        /// <param name="dataPacket">The data packet to convert</param>
        /// <returns>An array of <see cref="PacketSize"/> bytes.</returns>
        public static byte[] GetBytes(DataPacket dataPacket)
        {
            if (dataPacket == null)
            {
                throw new ArgumentNullException("dataPacket");
            }

            byte[] bytes = new byte[PacketSize];
            bytes[0] = dataPacket.DeviceNumber;
            bytes[1] = (byte)dataPacket.Command;
            int data = dataPacket.Data;
            bytes[2] = (byte)((data & 0x000000FF));
            bytes[3] = (byte)((data & 0x0000FF00) >> 8);
            bytes[4] = (byte)((data & 0x00FF0000) >> 16);
            bytes[5] = (byte)((data & 0xFF000000) >> 24);
            return bytes;
        }

        /// <summary>
        /// Initialize the data structure and read the values from the buffer.
        /// </summary>
        private DataPacket BuildDataPacket()
        {
            DataPacket dataPacket = new DataPacket();
            dataPacket.DeviceNumber = buffer[0];
            dataPacket.Command = (Command)buffer[1];

            int data = 0;
            for (int i = 5; i >= 2; i--)
            {
                data <<= 8;
                data += buffer[i];
            }
            dataPacket.Data = data;
            dataPacket.Measurement = new Measurement(data, UnitOfMeasure.Data);

            return dataPacket;
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        ///     resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                timer.Dispose();
            }
        }

        #endregion

        private struct TraceInfo
        {
            public byte data;
            public long milliseconds;
        }
    }
}
