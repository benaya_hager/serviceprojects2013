﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Zaber
{
    /// <summary>
    /// Registering for response events from a <see cref="ZaberDevice"/> can be 
    /// awkward, especially in scripts. This class registers for the response
    /// events and stores them until you request them with the 
    /// NextResponse() method. Listening is started automatically when you 
    /// create a listener, but you can stop and start listening with the 
    /// Stop() and Start() methods.
    /// </summary>
    public class DeviceListener : ResponseListener<DeviceMessage>
    {
        private ZaberDevice _device;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="device">The device to listen to for responses.</param>
        public DeviceListener(ZaberDevice device)
        {
            _device = device;
            Start();
        }

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="conversation">The conversation to listen to for 
        /// responses.</param>
        public DeviceListener(Conversation conversation) : 
            this(conversation.Device)
        {
        }

        /// <summary>
        /// Register with the device.
        /// </summary>
        protected override void RegisterEvent()
        {
            _device.MessageReceived +=
                new EventHandler<DeviceMessageEventArgs>(device_MessageReceived);
        }

        /// <summary>
        /// Unregister from the device.
        /// </summary>
        protected override void UnregisterEvent()
        {
            _device.MessageReceived -=
                new EventHandler<DeviceMessageEventArgs>(device_MessageReceived);
        }

        /// <summary>
        /// Handle a response event from the device.
        /// </summary>
        /// <param name="sender">The device sending the response.</param>
        /// <param name="e">The details of the response.</param>
        private void device_MessageReceived(object sender, DeviceMessageEventArgs e)
        {
            OnItemReceived(e.DeviceMessage);
        }
    }
}
