using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;
using System.Globalization;
using System.Collections.ObjectModel;

namespace Zaber
{
    /// <summary>
    /// Represents a device attached to the computer's port.
    /// </summary>
    /// <remarks>The ZaberDevice provides an event-driven interface to the device.
    /// Check out the <see cref="Conversation"/> class for request-driven 
    /// interface.</remarks>
    public class ZaberDevice
    {
        private static ILog log = LogManager.GetLogger(typeof(ZaberDevice));

        /// <summary>
        /// The default value for the <see cref="MicrostepResolution"/> 
        /// property.
        /// </summary>
        public const int DefaultMicrostepResolution = 64;

        private const string RawDataOnlyMessage = 
            "This measurement type only supports raw data.";
        private byte deviceNumber;
        private DeviceType deviceType;
        private IZaberPort port;
        private bool areMessageIdsEnabled;
        private List<ZaberDevice> axes = new List<ZaberDevice>();
        private DeviceMessage currentRequest;

        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        public ZaberDevice()
        {
            MicrostepResolution = DefaultMicrostepResolution;
        }

        /// <summary>
        /// Enables or disables message ids mode on this device.
        /// </summary>
        /// <remarks>
        /// <para>Message ids mode uses id numbers to match each response to 
        /// the request that triggered it. Message ids are also known as
        /// logical channels.</para>
        /// </remarks>
        /// <seealso cref="ZaberPortFacade.AreMessageIdsEnabled"/>
        /// <seealso cref="DeviceMessage.MessageId"/>
        public bool AreMessageIdsEnabled
        {
            get { return areMessageIdsEnabled; }
            set { areMessageIdsEnabled = value; }
        }

        /// <summary>
        /// The port to send requests to and receive responses from.
        /// </summary>
        public IZaberPort Port
        {
            get { return port; }
            set 
            { 
                if (port != null)
                {
                    port.DataPacketReceived -=
                        new EventHandler<DataPacketEventArgs>(OnDataPacketReceived);
                    port.DataPacketSent -=
                        new EventHandler<DataPacketEventArgs>(OnDataPacketSent);
                }

                port = value;
                IsTrackingRequests = port != null && port.IsAsciiMode;
                if (port != null)
                {
                    port.DataPacketReceived +=
                        new EventHandler<DataPacketEventArgs>(OnDataPacketReceived);
                    port.DataPacketSent +=
                        new EventHandler<DataPacketEventArgs>(OnDataPacketSent);
                }
            }
        }

        /// <summary>
        /// Gets a description of the device in a standard format.
        /// </summary>
        public string Description
        {
            get
            {
                return String.Format(
                      CultureInfo.CurrentUICulture,
                      "device {0}{1} ({2})",
                      DeviceNumber,
                      AxisNumber == 0
                      ? ""
                      : " axis " + AxisNumber,
                      DeviceType);
            }
        }

        /// <summary>
        /// Handle a data packet sent to the port and raise a corresponding event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataPacketSent(object sender, DataPacketEventArgs e)
        {
            RaiseDataPacketEvent(e, MessageSent, true);
        }

        /// <summary>
        /// Handle a data packet received from the port and raise a corresponding event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// This is internal so that the port facade can manually raise the first response
        /// received from this device.
        /// </remarks>
        internal void OnDataPacketReceived(object sender, DataPacketEventArgs e)
        {
            RaiseDataPacketEvent(e, MessageReceived, false);
        }

        /// <summary>
        /// Gets the most recently received position update for this device.
        /// </summary>
        public int? RecentPosition { get; private set; }

        private void RaiseDataPacketEvent(
            DataPacketEventArgs e,
            EventHandler<DeviceMessageEventArgs> handler,
            bool isRequest)
        {
            if (e.DataPacket.DeviceNumber != deviceNumber)
            {
                return;
            }
            if (AxisNumber != 0 &&
                e.DataPacket.AxisNumber != AxisNumber)
            {
                return;
            }
            DeviceMessage message = CreateDeviceMessage(e.DataPacket);
            if (isRequest)
            {
                if (IsTrackingRequests)
                {
                    if (currentRequest == null)
                    {
                        currentRequest = message;
                    }
                    else
                    {
                        currentRequest = null;
                        IsTrackingRequests = false;
                    }
                }
            }
            else
            {
                message.Request = currentRequest;
                if (message.Request != null)
                {
                    currentRequest = null;
                    message.CommandInfo = message.Request.CommandInfo;
                }
                var commandInfo = message.CommandInfo;
                if (commandInfo != null && 
                    commandInfo.IsCurrentPositionReturned)
                {
                    RecentPosition = message.Data;
                }
                if (e.DataPacket.Command == Command.SetMicrostepResolution)
                {
                    MicrostepResolution = e.DataPacket.Data;
                }
                if (e.DataPacket.Command == Command.ReturnFirmwareVersion)
                {
                    FirmwareVersion = e.DataPacket.Data;
                }
                message.Measurement = CalculateMeasurement(
                    commandInfo,
                    message.Data,
                    isRequest);
            }
            if (log.IsDebugEnabled)
            {
                if (isRequest)
                {
                    log.Debug(message.FormatRequest());
                }
                else
                {
                    log.Debug(message.FormatResponse());
                }
            }
            if (handler != null)
            {
                handler(this, new DeviceMessageEventArgs(message));
            }
        }

        /// <summary>
        /// Converts a data packet into a device message by populating the
        /// message id and command info fields.
        /// </summary>
        /// <param name="dataPacket">The data packet to copy</param>
        /// <returns>A device message with the new fields populated</returns>
        public DeviceMessage CreateDeviceMessage(DataPacket dataPacket)
        {
            if (dataPacket == null)
            {
                throw new ArgumentNullException("dataPacket");
            }

            DeviceMessage message = new DeviceMessage(dataPacket);
            if (areMessageIdsEnabled)
            {
                // Take most significant byte as Id.
                message.MessageId = (byte)(message.Data >> 24);
                // 255 or 0 means no Id.
                if (message.MessageId == 255)
                {
                    message.MessageId = 0;
                }
                message.Data = message.Data & 0xFFFFFF;
                // negative data has to be extended back into the most 
                // significant bit.
                if ((message.Data & 0x800000) != 0)
                {
                    message.Data = message.Data | -0x1000000;
                }
            }
            if (deviceType != null)
            {
                message.CommandInfo =
                    dataPacket.Body == null
                    ? deviceType.GetCommandByNumber(dataPacket.Command)
                    : DeviceType.GetCommandByText(dataPacket.Body); 
            }
            return message;
        }

        /// <summary>
        /// The device type determines the list of supported commands.
        /// </summary>
        public DeviceType DeviceType
        {
            get { return deviceType; }
            set 
            { 
                deviceType = value;
            }
        }
	
        /// <summary>
        /// The device number is used as an address to send requests to.
        /// </summary>
        public byte DeviceNumber
        {
            get { return deviceNumber; }
            set { deviceNumber = value; }
        }

        /// <summary>
        /// Convenience method for commands that ignore the data value. 
        /// It sends 0 as the data value.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>        
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="TimeoutException">The operation did not complete
        /// before the time-out period ended.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(Command command)
        {
            Send(command, 0, 0);
        }

        /// <summary>
        /// Send a command to this device without using a message id.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>        
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="TimeoutException">The operation did not complete
        /// before the time-out period ended.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(Command command, int data)
        {
            Send(command, data, 0);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="messageId">See <see cref="DeviceMessage.MessageId"/>.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="TimeoutException">The operation did not complete
        /// before the time-out period ended.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(Command command, int data, byte messageId)
        {
            bool isRequest = true;
            var commandInfo = DeviceType.GetCommandByNumber(command);
            Measurement measurement = 
                CalculateMeasurement(commandInfo, data, isRequest);
            var adjustedData = AdjustData(data, messageId);
            port.Send(
                deviceNumber,
                command,
                adjustedData,
                measurement);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="TimeoutException">The operation did not complete
        /// before the time-out period ended.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(String text)
        {
            port.Send(String.Format(
                CultureInfo.InvariantCulture,
                "{0:00} {1} {2}", 
                DeviceNumber, 
                AxisNumber,
                text));
        }

        /// <summary>
        /// Calculate a measurement for a given data value.
        /// </summary>
        /// <param name="commandInfo">The command that the data value is for.
        /// </param>
        /// <param name="data">The data value to convert.</param>
        /// <param name="isRequest">True if the data is part of a request, 
        /// false if it's part of a response.</param>
        /// <returns>A measurement in the appropriate unit of measure, or as 
        /// raw data if it couldn't be converted.</returns>
        private Measurement CalculateMeasurement(
            CommandInfo commandInfo, 
            int data, 
            bool isRequest)
        {
            Measurement measurement;
            var stepSize = DeviceType.StepSize;
            var measurementType =
                commandInfo == null
                ? MeasurementType.Other
                : isRequest
                ? commandInfo.RequestDataType
                : commandInfo.ResponseDataType;
            if (stepSize == null ||
                commandInfo == null ||
                measurementType == MeasurementType.Other)
            {
                measurement = new Measurement(data, UnitOfMeasure.Data);
            }
            else
            {
                var stepUnit =
                    DeviceType.DistanceToAxis == null
                    ? stepSize.Unit
                    : UnitOfMeasure.Degree;
                var toUnit =
                    measurementType == MeasurementType.Position
                    ? stepUnit
                    : measurementType == MeasurementType.Velocity
                    ? stepUnit.Derivative
                    : stepUnit.Derivative.Derivative;
                int decimals;
                measurement = CalculateMeasurement(
                    data,
                    toUnit,
                    measurementType,
                    ConversionMap.Common,
                    out decimals);
            }
            return measurement;
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="measurement">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        /// <param name="messageId">See <see cref="DeviceMessage.MessageId"/>.</param>
        public void SendInUnits(
            Command command,
            Measurement measurement,
            byte messageId)
        {
            port.Send(
                deviceNumber, 
                command, 
                AdjustData(CalculateData(command, measurement), messageId), 
                measurement);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="measurement">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        public void SendInUnits(Command command, Measurement measurement)
        {
            SendInUnits(command, measurement, 0);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        /// <param name="unit">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        public void SendInUnits(Command command, decimal value, UnitOfMeasure unit)
        {
            SendInUnits(command, new Measurement(value, unit), 0);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        /// <param name="unit">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        public void SendInUnits(Command command, int value, UnitOfMeasure unit)
        {
            SendInUnits(command, new Measurement(value, unit), 0);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="value">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        /// <param name="unit">The <see cref="DataPacket.Data"/> value 
        /// will be calculated from this.</param>
        public void SendInUnits(Command command, double value, UnitOfMeasure unit)
        {
            SendInUnits(command, new Measurement(value, unit), 0);
        }

        /// <summary>
        /// Send a command to this device.
        /// </summary>
        /// <param name="text">The command to send to the device without the 
        /// slash or device number.</param>
        /// <param name="measurement">A data value to append to the command
        /// will be calculated from this. If it is null, then the command 
        /// will be sent without any extra data.</param>
        public void SendInUnits(
            String text,
            Measurement measurement)
        {
            var format =
                measurement == null
                ? "{0:00} {1} {2}"
                : "{0:00} {1} {2} {3}";
            port.Send(String.Format(
                CultureInfo.InvariantCulture,
                format,
                DeviceNumber,
                AxisNumber,
                text,
                measurement == null ? 0 : CalculateData(text, measurement)),
                measurement);
        }

        private int AdjustData(int data, byte messageId)
        {
            int adjustedData =
                areMessageIdsEnabled
                ? ((int)messageId << 24) | (data & 0xFFFFFF)
                : data;
            return adjustedData;
        }

        /// <summary>
        /// Send a command to this device after a delay.
        /// </summary>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="messageId">See <see cref="DeviceMessage.MessageId"/>.</param>
        /// <remarks>See <see cref="IZaberPort.SendDelayed"/> for the details
        /// of how the delay works. This command may never be sent, if the port
        /// gets closed.
        /// </remarks>
        /// <returns>The data packet that will be sent, or null if the port is
        /// already closed.</returns>
        public DataPacket SendDelayed(Command command, int data, byte messageId)
        {
            var port = this.port;
            if (port == null)
            {
                return null;
            }
            return port.SendDelayed(deviceNumber, command, AdjustData(data, messageId));
        }

        /// <summary>
        /// Raised when the device receives a data packet back from the port.
        /// </summary>
        /// <example>
        /// Register an event handler and print any received data to the 
        /// output console.
        /// <code>
        /// private void Register(ZaberDevice device)
        /// {
        ///     device.MessageReceived += 
        ///         new EventHandler&lt;DeviceMessageEventArgs&gt;(device_MessageReceived);
        /// }
        ///
        /// void device_MessageReceived(object sender, DeviceMessageEventArgs e)
        /// {
        ///     System.Console.Out.WriteLine(
        ///         "Device {0}: {1}({2})",
        ///         e.DeviceMessage.DeviceNumber,
        ///         e.DeviceMessage.Command,
        ///         e.DeviceMessage.Data);
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. See 
        /// <see cref="IZaberPort.DataPacketReceived"/> for details on how to
        /// handle events from background threads and how to just avoid them.
        /// </remarks>
        public event EventHandler<DeviceMessageEventArgs> MessageReceived;

        /// <summary>
        /// Raised when the device sends a packet to the port.
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is occasionally raised 
        /// from a background thread. See 
        /// <see cref="IZaberPort.DataPacketReceived"/> for details on how to
        /// handle events from background threads and how to just avoid them.
        /// </remarks>
        public event EventHandler<DeviceMessageEventArgs> MessageSent;

        /// <summary>
        /// Gets a flag showing whether this device has any objects subscribed
        /// to its events. (Used for testing.)
        /// </summary>
        public bool HasSubscribers
        {
            get
            {
                return MessageReceived != null || MessageSent != null;
            }
        }

        /// <summary>
        /// Protected method to allow derived classes to raise the 
        /// MessageReceived event.
        /// </summary>
        /// <param name="e">The details of the message.</param>
        protected void OnMessageReceived(DeviceMessageEventArgs e)
        {
            if (MessageReceived != null)
            {
                MessageReceived(this, e);
            }
        }

        /// <summary>
        /// Protected method to allow derived classes to raise the 
        /// MessageSent event.
        /// </summary>
        /// <param name="e">The details of the message.</param>
        protected void OnMessageSent(DeviceMessageEventArgs e)
        {
            if (MessageSent != null)
            {
                MessageSent(this, e);
            }
        }

        /// <summary>
        /// True if this is a single device, and not a collection of devices.
        /// </summary>
        public virtual bool IsSingleDevice
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a flag indicating that this device is tracking the request 
        /// that generated each response.
        /// </summary>
        /// <remarks>This is only true when the port is in ASCII mode. It is
        /// disabled if a second request is sent before the response comes back
        /// for the first one. It will be enabled again when the port is
        /// reopened.</remarks>
        public bool IsTrackingRequests { get; private set; }

        /// <summary>
        /// Gets or sets the current microstep resolution of this device.
        /// </summary>
        /// <remarks>This is updated whenever a 
        /// <see cref="Command.SetMicrostepResolution"/> response is 
        /// received.</remarks>
        /// <value>The number of microsteps in a full step. Default value is 
        /// 64.</value>
        public int MicrostepResolution { get; set; }

        /// <summary>
        /// Gets or sets the firmware version of this device.
        /// </summary>
        /// <remarks>This is updated whenever a 
        /// <see cref="Command.ReturnFirmwareVersion"/> response is 
        /// received.</remarks>
        /// <value>The firmware version, multiplied by 100.</value>
        public int FirmwareVersion { get; set; }

        /// <summary>
        /// Calculate what the data value should be to represent the requested
        /// measurement on the requested command.
        /// </summary>
        /// <param name="command">The command that will use the data.</param>
        /// <param name="measurement">The measurement to send to this device.
        /// </param>
        /// <returns>A data value to send as part of a command.</returns>
        public int CalculateData(Command command, Measurement measurement)
        {
            if (measurement.Unit == UnitOfMeasure.Data)
            {
                return (int)measurement.Value;
            }
            var isMoveRelative = command == Command.MoveRelative;
            var commandInfo = DeviceType.GetCommandByNumber(command);

            return CalculateData(commandInfo, measurement, isMoveRelative);
        }

        /// <summary>
        /// Calculate what the data value should be to represent the requested
        /// measurement on the requested command.
        /// </summary>
        /// <param name="text">The command that will use the data.</param>
        /// <param name="measurement">The measurement to send to this device.
        /// </param>
        /// <returns>A data value to send as part of a command.</returns>
        public int CalculateData(String text, Measurement measurement)
        {
            if (measurement.Unit == UnitOfMeasure.Data)
            {
                return (int)measurement.Value;
            }
            var isMoveRelative = "move rel".Equals(
                text.Trim(), 
                StringComparison.OrdinalIgnoreCase);
            var commandInfo = DeviceType.GetCommandByText(text);

            return CalculateData(commandInfo, measurement, isMoveRelative);
        }

        /// <summary>
        /// Calculate what the data value should be to represent the requested
        /// measurement on the requested command.
        /// </summary>
        /// <param name="commandInfo">The command metadata that will use the data.</param>
        /// <param name="measurement">The measurement to send to this device.</param>
        /// <param name="isMoveRelative">Is the command a relative move?</param>
        /// <returns>A data value to send as part of a command.</returns>
        private int CalculateData(CommandInfo commandInfo, Measurement measurement, bool isMoveRelative)
        {
            if (commandInfo == null)
            {
                throw new InvalidOperationException(
                    "Unit of measure conversion is only allowed on " +
                    "known commands.");
            }

            var distanceToAxis = DeviceType.DistanceToAxis;
            var map = ConversionMap.Common;
            if ( ! isMoveRelative || distanceToAxis == null)
            {
                return CalculateData(
                    measurement,
                    commandInfo.RequestDataType,
                    map);
            }

            var recentValue = Decimal.Parse(
                FormatData(
                    RecentPosition.GetValueOrDefault(),
                    measurement.Unit,
                    commandInfo.RequestDataType,
                    map),
                CultureInfo.CurrentCulture);
            var absoluteMeasurement =
                new Measurement(
                    measurement.Value + recentValue,
                    measurement.Unit);
            var absoluteData = CalculateData(
                absoluteMeasurement,
                commandInfo.RequestDataType,
                map);
            return absoluteData - RecentPosition.GetValueOrDefault();
        }

        /// <summary>
        /// Calculate what the data value should be to represent the requested
        /// measurement.
        /// </summary>
        /// <param name="measurement">The measurement to send to this device.
        /// </param>
        /// <param name="measurementType">The type of measurement: position,
        /// velocity, etc.</param>
        /// <param name="map">Contains conversion ratios for all the different
        /// units of measure.</param>
        /// <returns>A data value to send as part of a command.</returns>
        /// <exception cref="InvalidOperationException">The device does not
        /// support units of measure.</exception>
        /// <exception cref="ArgumentException">The specified 
        /// <paramref name="measurementType"/> only supports raw data.
        /// </exception>
        public int CalculateData(
            Measurement measurement, 
            MeasurementType measurementType, 
            ConversionMap map)
        {
            var microstepSize = CalculateMicrostepSize();
            if (measurement.Unit == UnitOfMeasure.Data)
            {
                return (int)measurement.Value;
            }
            if (measurement.Unit == UnitOfMeasure.MicrostepsPerSecond
                && measurementType == MeasurementType.Velocity)
            {
                return (int)Math.Round(
                    measurement.Value / CalculateVelocityCoefficient());
            }
            if (measurement.Unit == UnitOfMeasure.MicrostepsPerSecondSquared
                && measurementType == MeasurementType.Acceleration)
            {
                return (int)Math.Round(
                    measurement.Value / CalculateAccelerationCoefficient());
            }
            if (microstepSize == null)
            {
                throw new InvalidOperationException(
                    "This device does not support units of measure.");
            }

            var table = map.Find(DeviceType.MotionType, measurementType);
            var distanceToAxis = DeviceType.DistanceToAxis;
            switch (measurementType)
            {
                case MeasurementType.Position:
                    Measurement convertedPosition;
                    if (distanceToAxis == null)
                    {
                        convertedPosition =
                            table.Convert(measurement, microstepSize.Unit);
                        return (int)Math.Round(
                            convertedPosition.Value /
                            microstepSize.Value);
                    }
                    convertedPosition =
                        table.Convert(measurement, UnitOfMeasure.Radian);
                    return (int)Math.Round(
                        (decimal)(Math.Tan((double)convertedPosition.Value)) *
                        distanceToAxis.Value / 
                        microstepSize.Value);
                case MeasurementType.Velocity:
                    var slowestSpeed = CalculateSlowestSpeed(
                        microstepSize,
                        distanceToAxis);
                    var convertedVelocity =
                        table.Convert(measurement, slowestSpeed.Unit);
                    return (int)Math.Round(
                        convertedVelocity.Value /
                        slowestSpeed.Value);
                case MeasurementType.Acceleration:
                    var lowestAcceleration = CalculateLowestAcceleration(
                        microstepSize,
                        distanceToAxis);
                    var convertedAcceleration =
                        table.Convert(measurement, lowestAcceleration.Unit);
                    return (int)Math.Round(
                        convertedAcceleration.Value /
                        lowestAcceleration.Value);
                default:
                    throw new ArgumentException(
                        RawDataOnlyMessage,
                        "measurement");
            }
        }

        private decimal CalculateVelocityCoefficient()
        {
            var coefficient =
                FirmwareVersion >= 600
                ? 10000m / (1 << 14)
                : 9.375m;
            return coefficient;
        }

        private decimal CalculateAccelerationCoefficient()
        {
            var coefficient =
                FirmwareVersion >= 600
                ? 100000000m / (1 << 14)
                : 11250m;
            return coefficient;
        }

        /// <summary>
        /// Display a data value in another unit of measure.
        /// </summary>
        /// <param name="rawData">The data value to display.</param>
        /// <param name="toUnit">The unit of measure to convert to.</param>
        /// <param name="measurementType">The type of measurement: position,
        /// velocity, etc.</param>
        /// <param name="map">Contains ratios for converting between different
        /// units of measure.</param>
        /// <returns>A string representation of the data value. Rounded to one
        /// decimal place beyond the device's microstep size.</returns>
        public string FormatData(
            int rawData, 
            UnitOfMeasure toUnit,
            MeasurementType measurementType, 
            ConversionMap map)
        {
            if (toUnit != UnitOfMeasure.Data &&
                measurementType == MeasurementType.Other)
            {
                throw new ArgumentException(
                    RawDataOnlyMessage,
                    "toUnit");
            }
            int decimals;
            var measurement = CalculateMeasurement(
                rawData, 
                toUnit, 
                measurementType, 
                map,
                out decimals);
            var format = String.Format(
                CultureInfo.InvariantCulture,
                "{{0:F{0}}}",
                decimals);
            return String.Format(
                CultureInfo.CurrentUICulture, 
                format, 
                measurement.Value);
        }

        /// <summary>
        /// Calculate a measurement for a given data value.
        /// </summary>
        /// <param name="rawData">The data value to convert.</param>
        /// <param name="toUnit">The unit of measure to convert to.</param>
        /// <param name="measurementType">The type of measurement: position,
        /// velocity, etc.</param>
        /// <param name="map">Contains ratios for converting between different 
        /// units of measure.</param>
        /// <param name="decimals">Returns the number of decimal places that are
        /// significant for the measurement.</param>
        /// <returns>A measurement in the requested unit of measure, or as raw
        /// data if it couldn't be converted.</returns>
        public Measurement CalculateMeasurement(
            int rawData, 
            UnitOfMeasure toUnit, 
            MeasurementType measurementType, 
            ConversionMap map, 
            out int decimals)
        {
            var microstepSize = CalculateMicrostepSize();
            var conversions = map.Find(DeviceType.MotionType, measurementType);
            if (toUnit == UnitOfMeasure.MicrostepsPerSecond &&
                measurementType == MeasurementType.Velocity)
            {
                decimal coefficient = CalculateVelocityCoefficient();
                decimals = 1 + (int)-Math.Floor(Math.Log10(
                    (double)coefficient));
                return new Measurement(
                    rawData * coefficient,
                    toUnit);
            }
            if (toUnit == UnitOfMeasure.MicrostepsPerSecondSquared &&
                measurementType == MeasurementType.Acceleration)
            {
                decimal coefficient = CalculateAccelerationCoefficient();
                decimals = 1 + (int)-Math.Floor(Math.Log10(
                    (double)coefficient));
                return new Measurement(
                    rawData * coefficient,
                    toUnit);
            }
            if (microstepSize == null || 
                toUnit == UnitOfMeasure.Data ||
                conversions == null)
            {
                decimals = 0;
                return new Measurement(rawData, UnitOfMeasure.Data);
            }
            decimal data;
            var distanceToAxis = DeviceType.DistanceToAxis;
            switch (measurementType)
            {
                case MeasurementType.Position:
                    if (distanceToAxis == null)
                    {
                        var convertedMicrostepSize = conversions.Convert(
                            microstepSize,
                            toUnit);
                        decimals = 1 + (int)-Math.Floor(Math.Log10(
                            (double)convertedMicrostepSize.Value));
                        data = rawData * convertedMicrostepSize.Value;
                    }
                    else
                    {
                        var convertedMicrostepSize = Math.Atan(
                            (double)(microstepSize.Value / distanceToAxis.Value));
                        decimals = 1 + (int)-Math.Floor(Math.Log10(
                            convertedMicrostepSize));
                        var radians = Math.Atan(
                            (double)(rawData * microstepSize.Value /
                                distanceToAxis.Value));
                        data = conversions.Convert(
                            new Measurement(
                                (decimal)radians,
                                UnitOfMeasure.Radian),
                            toUnit).Value;
                    }
                    break;
                case MeasurementType.Velocity:
                    var convertedSpeed = conversions.Convert(
                        CalculateSlowestSpeed(
                            microstepSize,
                            distanceToAxis),
                        toUnit);
                    decimals = 1 + (int)-Math.Floor(Math.Log10(
                        (double)convertedSpeed.Value));
                    data = rawData * convertedSpeed.Value;
                    break;
                case MeasurementType.Acceleration:
                    var convertedAcceleration = conversions.Convert(
                        CalculateLowestAcceleration(
                            microstepSize,
                            distanceToAxis),
                        toUnit);
                    decimals = 1 + (int)-Math.Floor(Math.Log10(
                        (double)convertedAcceleration.Value));
                    data = rawData * convertedAcceleration.Value;
                    break;
                default:
                    throw new ArgumentException(
                        String.Format(
                            CultureInfo.CurrentUICulture,
                            "Unknown measurement type '{0}'.",
                            measurementType),
                        "measurementType");
            }
            decimals = Math.Max(decimals, 0);
            return new Measurement(data, toUnit);
        }

        /// <summary>
        /// Calculate the microstep size based on the current resolution.
        /// </summary>
        /// <returns>A valid measurement, or null if no step size is configured
        /// for the device type.</returns>
        private Measurement CalculateMicrostepSize()
        {
            var stepSize = DeviceType.StepSize;
            if (stepSize == null || MicrostepResolution == 0)
            {
                return null;
            }
            return new Measurement(
                stepSize.Value / MicrostepResolution,
                stepSize.Unit);
        }

        /// <summary>
        /// Calculate the velocity of the device for a data value of 1.
        /// </summary>
        /// <param name="microstepSize">The size of a single microstep.</param>
        /// <param name="distanceToAxis">Read from the device type.</param>
        /// <returns>The calculated velocity.</returns>
        private Measurement CalculateSlowestSpeed(
            Measurement microstepSize, 
            Measurement distanceToAxis)
        {
            if (distanceToAxis == null)
            {
                return new Measurement(
                    microstepSize.Value *
                        CalculateVelocityCoefficient(),
                    microstepSize.Unit.Derivative);
            }
            return new Measurement(
                microstepSize.Value / distanceToAxis.Value *
                    CalculateVelocityCoefficient(),
                UnitOfMeasure.RadiansPerSecond);
        }

        /// <summary>
        /// Calculate the acceleration of the device for a data value of 1.
        /// </summary>
        /// <param name="microstepSize">The size of a single microstep.</param>
        /// <param name="distanceToAxis">Read from the device type.</param>
        /// <returns>The calculated acceleration.</returns>
        private Measurement CalculateLowestAcceleration(
            Measurement microstepSize,
            Measurement distanceToAxis)
        {
            if (distanceToAxis == null)
            {
                return new Measurement(
                    microstepSize.Value *
                        CalculateAccelerationCoefficient(),
                    microstepSize.Unit.Derivative.Derivative);
            }
            return new Measurement(
                microstepSize.Value / distanceToAxis.Value *
                    CalculateAccelerationCoefficient(),
                UnitOfMeasure.RadiansPerSecondSquared);
        }

        /// <summary>
        /// Get a list of the units of measure this device supports.
        /// </summary>
        /// <param name="measurementType">The type of measurement: position, 
        /// velocity, etc.</param>
        /// <param name="map">Contains ratios for converting different units
        /// of measure.</param>
        /// <returns>A list of units of measure, sorted with raw data as the
        /// first entry.</returns>
        public ICollection<UnitOfMeasure> GetUnitsOfMeasure(
            MeasurementType measurementType,
            ConversionMap map)
        {
            var table = map.Find(DeviceType.MotionType, measurementType);
            var units =
                table == null
                ? new List<UnitOfMeasure>()
                : new List<UnitOfMeasure>(table.UnitsOfMeasure);
            if (measurementType == MeasurementType.Velocity &&
                IsSingleDevice)
            {
                units.Add(UnitOfMeasure.MicrostepsPerSecond);
            }
            units.Sort();
            units.Insert(0, UnitOfMeasure.Data);
            return units;
        }

        /// <summary>
        /// Get a list of all the units of measure this device supports, 
        /// including all possible measurement types.
        /// </summary>
        /// <param name="map">Contains ratios for converting different units
        /// of measure.</param>
        /// <returns>A set of units of measure.</returns>
        public IEnumerable<UnitOfMeasure> GetAllUnitsOfMeasure(ConversionMap map)
        {
            var allUnits = new HashSet<UnitOfMeasure>();
            var measurementTypes = Enum.GetValues(typeof(MeasurementType));
            foreach (MeasurementType measurementType in measurementTypes)
            {
                foreach (var unit in GetUnitsOfMeasure(measurementType, map))
                {
                    allUnits.Add(unit);
                }
            }
            return allUnits;
        }

        /// <summary>
        /// Gets or sets the axis number for devices that represent an axis on 
        /// multi-axis controllers.
        /// </summary>
        /// <remarks>For the controller itself, this will be zero.</remarks>
        public int AxisNumber { get; set; }

        /// <summary>
        /// Add a device that represents an axis for this controller.
        /// </summary>
        /// <param name="axis">The axis to add.</param>
        /// <remarks>All controllers have a collection of devices - one
        /// for each axis.</remarks>
        public void AddAxis(ZaberDevice axis)
        {
            axes.Add(axis);
        }

        /// <summary>
        /// Get a list of axes for this device.
        /// </summary>
        public ReadOnlyCollection<ZaberDevice> Axes
        {
            get
            {
                return axes.AsReadOnly();
            }
        }
    }
}
