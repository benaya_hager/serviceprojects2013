using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using log4net;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Zaber
{
    /// <summary>
    /// Encapsulates serial port settings, and translates between the byte 
    /// streams and the DataPacket structure.
    /// </summary>
    public class TSeriesPort : IZaberPort
    {
        private static ILog _log = LogManager.GetLogger(typeof(TSeriesPort));
        private const int DEFAULT_READ_TIMEOUT = 100;

        private SerialPort _serialPort;
        private Stream _baseStream;
        private volatile bool _isClosing;
        private Thread _receiveThread;
        private String _lastPortName;
        private PacketConverter _packetConverter;
        private Timer _delayedSendTimer;
        private LinkedList<DataPacket> _delayedPackets = new LinkedList<DataPacket>();
        private readonly object _sendLock = new object();

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="serialPort">Serial port to communicate with.</param>
        /// <param name="packetConverter">Used to convert between the byte stream
        /// and the <see cref="DataPacket"/> structure.</param>
        public TSeriesPort(
            SerialPort serialPort,
            PacketConverter packetConverter)
        {
            if (serialPort.ReadTimeout == SerialPort.InfiniteTimeout)
            {
                serialPort.ReadTimeout = DEFAULT_READ_TIMEOUT;
            }
            serialPort.ErrorReceived +=
                new SerialErrorReceivedEventHandler(serialPort_ErrorReceived);
            _serialPort = serialPort;
            packetConverter.DataPacketReceived +=
                new EventHandler<DataPacketEventArgs>(packetConverter_DataPacketReceived);
            packetConverter.PortError +=
                new EventHandler<ZaberPortErrorReceivedEventArgs>(
                    packetConverter_PortError);
            _packetConverter = packetConverter;

            _delayedSendTimer = new Timer(OnDelayedSendTimer);
            DelayMilliseconds = 500;
            SubscriberTimer = new TimeoutTimer();
            SubscriberTimer.Timeout = 500;
        }

        /// <summary>
        /// This runs as a background thread that receives all data from the 
        /// serial port. We don't use the DataReceived event anymore because
        /// it's not supported on Mono.
        /// </summary>
        void ReceiveThread()
        {
            while ( ! _isClosing)
            {
                try
                {
                    if (_serialPort.BytesToRead > 0)
                        OnDataReceived((byte)_serialPort.ReadByte());
                }
                catch (TimeoutException)
                {
                    // Expected exception, ignore it.
                }
                catch (InvalidOperationException)
                {
                    // The port is closed. Stop trying to read.
                    break;
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(
                        CultureInfo.CurrentCulture,
                        "Receiving thread failed: {0}",
                        ex);
                    OnErrorReceived(ZaberPortError.ReceiveException);
                }
            }        
        }

        /// <summary>
        /// Synchronization lock that controls when data packets are sent to
        /// the port.
        /// </summary>
        /// <remarks>If you lock this, then no data packets will be sent until
        /// you release it.
        /// </remarks>
        public object SendLock { get { return _sendLock; } }

        /// <summary>
        /// Gets the packet converter used to convert between the byte stream 
        /// and the <see cref="DataPacket"/> structure.
        /// </summary>
        public PacketConverter PacketConverter
        {
            get 
            {
                return _packetConverter;
            }
        }

        /// <summary>
        /// Gets the serial port to communicate with.
        /// </summary>
        public SerialPort SerialPort
        {
            get 
            {
                return _serialPort;
            }
        }

        /// <summary>
        /// Get a list of all available serial ports.
        /// </summary>
        /// <returns>An array of port names, one of which should be passed to 
        /// <see cref="Open"/>. The names are sorted.</returns>
        public string[] GetPortNames()
        {
            var portNames = SerialPort.GetPortNames();
            Array.Sort(portNames);
            return portNames;
        }

        /// <summary>
        /// Process data that was received from the port.
        /// </summary>
        /// <param name="data">a byte of data that was received from the serial
        /// port</param>
        /// <remarks>This is protected so it can be triggered during
        /// testing.</remarks>
        protected void OnDataReceived(byte data)
        {
            _packetConverter.ReceiveByte(data);
            if ( ! IsAsciiMode)
            {
                _packetConverter.StartTimer();
            }
        }

        /// <summary>
        /// Receive an error from the port.
        /// </summary>
        /// <param name="sender">The port</param>
        /// <param name="e">Details of the error.</param>
        private void serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            OnErrorReceived((ZaberPortError)e.EventType);
        }

        private void OnErrorReceived(ZaberPortError errorType)
        {
            _log.DebugFormat(
                CultureInfo.CurrentCulture,
                "Port error: {0}",
                errorType);
            if (ErrorReceived != null && IsOpen)
            {
                ErrorReceived(this, new ZaberPortErrorReceivedEventArgs(errorType));
            }
            else if (!IsOpen)
            {
                _serialPort.Close();
            }
        }

        void packetConverter_PortError(
            object sender, 
            ZaberPortErrorReceivedEventArgs e)
        {
            OnErrorReceived(e.ErrorType);
        }

        void packetConverter_DataPacketReceived(object sender, DataPacketEventArgs e)
        {
            if (_log.IsDebugEnabled)
            {
                _log.Debug(e.DataPacket.FormatResponse());
            }
            if (DataPacketReceived != null)
            {
                var receivers = DataPacketReceived.GetInvocationList();
                foreach (EventHandler<DataPacketEventArgs> receiver in receivers)
                {
                    var result = receiver.BeginInvoke(this, e, null, null);
                    SubscriberTimer.WaitOne(result.AsyncWaitHandle);
                }
            }
        }

        /// <summary>
        /// Raised when the port receives data back from the devices. When a complete
        /// response has been received, it's converted to a <see cref="DataPacket"/> object and
        /// sent through this event.
        /// </summary>
        /// <example>
        /// Register an event handler and print any received data to the 
        /// output console.
        /// <code>
        /// private void MyForm_Load(object sender, EventArgs e)
        /// {
        ///     port.DataPacketReceived += 
        ///         new EventHandler&lt;DataPacketEventArgs&gt;(port_DataPacketReceived);
        /// }
        ///
        /// void port_DataPacketReceived(object sender, DataPacketEventArgs e)
        /// {
        ///     System.Console.Out.WriteLine(
        ///         "Device {0}: {1}({2})",
        ///         e.Data.DeviceNumber,
        ///         e.Data.Command,
        ///         e.Data.Data);
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// <para>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. The simplest way to deal with that is not
        /// to register for this event at all. Use a <see cref="Conversation"/>
        /// to coordinate requests and responses instead. It handles all the
        /// threading issues and just returns the response as the return
        /// value from <see cref="Conversation.Request(Command, int)"/>.
        /// </para>
        /// <para>If you do want to handle this event safely in your user 
        /// interface, read the first two sections of this article:
        /// http://weblogs.asp.net/justin_rogers/articles/126345.aspx
        /// It shows how to use <c>Control.InvokeRequired</c> and 
        /// <c>Control.BeginInvoke</c> to move execution back onto the UI
        /// thread from a background thread.
        /// </para>
        /// </remarks>
        public event EventHandler<DataPacketEventArgs> DataPacketReceived;

        /// <summary>
        /// Raised when the port sends data to the devices. 
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is occasionally 
        /// raised from a background thread. See 
        /// <see cref="DataPacketReceived"/> for details. Some scenarios where
        /// it can be raised from a background thread are: executing scripts 
        /// and automatically adjusting message id mode.
        /// </remarks>
        public event EventHandler<DataPacketEventArgs> DataPacketSent;

        /// <summary>
        /// Raised when the underlying port raises its own <c>ErrorReceived</c> 
        /// event, or when a partial data packet is received because some bytes
        /// have been dropped. The type of error is described by
        /// <see cref="ZaberPortErrorReceivedEventArgs.ErrorType"/>.
        /// </summary>
        /// <remarks>
        /// Be careful when handling this event, because it is usually raised 
        /// from a background thread. See <see cref="DataPacketReceived"/> for 
        /// details.
        /// </remarks>
        public event EventHandler<ZaberPortErrorReceivedEventArgs> ErrorReceived;

        /// <summary>
        /// Open the port to begin sending and receiving data. Be sure to call 
        /// <see cref="Dispose()"/> when you are finished with
        /// the port.
        /// </summary>
        /// <param name="portName">Should match one of the entries returned from
        /// <see cref="GetPortNames"/></param>
        [SuppressMessage(
            "Microsoft.Usage",
            "CA1816:CallGCSuppressFinalizeCorrectly",
            Justification = "Needed to make SerialPort class work properly.")]
        public void Open(string portName)
        {
            _log.Debug("Opening.");
            _lastPortName = portName;
            _serialPort.PortName = portName;
            Boolean  portExists = SerialPort.GetPortNames().Any(x => x == portName);
            if (portExists)
            {
                _serialPort.Open();
            }
            /* Hang onto the base stream ourselves so we can close it manually
                * if the serial port gets suddenly unplugged. This solves a
                * problem in the .NET framework where an unplugged serial port
                * will leave its stream open. */
            _baseStream = _serialPort.BaseStream;
            GC.SuppressFinalize(_baseStream);

            _receiveThread = new Thread(ReceiveThread);
            _receiveThread.IsBackground = true;
            _receiveThread.Start();
            _log.Debug("Opened.");
            
        }

        /// <summary>
        /// Gets the port name that was last sent to <see cref="Open"/>.
        /// </summary>
        /// <value>The port name that was last sent to <see cref="Open"/>, or
        /// null if <see cref="Open"/> has not been called.</value>
        public string PortName
        {
            get { return _lastPortName; }
        }

        /// <summary>
        /// Convenience method for commands that ignore the data value. 
        /// It sends 0 as the data value.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="Zaber.Command"/>.</param>
        public void Send(byte deviceNumber, Command command)
        {
            Send(deviceNumber, command, 0);
        }

        /// <summary>
        /// Is the port open?
        /// </summary>
        public virtual bool IsOpen
        {
            get { return _serialPort.IsOpen; }
        }

        /// <summary>
        /// Flag that gets or sets whether checksums are sent with each text 
        /// message. If true, the checksum will be calculated with the
        /// Longitudinal Redundancy Check (LRC) algorithm.
        /// </summary>
        public bool AreChecksumsSent { get; set; }

        /// <summary>
        /// Flag that gets or sets whether the port is in ASCII mode.
        /// If it's not in ASCII mode, then all received data will be
        /// parsed as binary, 6-byte packets.
        /// </summary>
        public bool IsAsciiMode 
        {
            get { return _packetConverter.IsAsciiMode; }
            set { _packetConverter.IsAsciiMode = value; }
        }

        /// <summary>
        /// Send a command to a device on the chain.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(byte deviceNumber, Command command, Int32 data)
        {
            Send(deviceNumber, command, data, null);
        }

        /// <summary>
        /// Send a command to a device on the chain.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <param name="measurement">An optional measurement that the data 
        /// value was calculated from. May be null.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(
            byte deviceNumber,
            Command command,
            Int32 data,
            Measurement measurement)
        {
            if (IsAsciiMode)
            {
                throw new InvalidOperationException(
                    "Binary commands cannot be sent when the port is in " +
                    "ASCII mode.");
            }
            DataPacket dataPacket = new DataPacket();
            dataPacket.DeviceNumber = deviceNumber;
            dataPacket.Command = command;
            dataPacket.Data = data;
            dataPacket.Measurement =
                measurement ?? new Measurement(data, UnitOfMeasure.Data);
            _log.Debug("Sending.");

            WriteData(PacketConverter.GetBytes(dataPacket));

            OnDataPacketSent(dataPacket);
        }

        /// <summary>
        /// Send a text-mode command to a device on the chain.
        /// </summary>
        /// <param name="text">The full command to send to the device.</param>
        /// <param name="measurement">An optional measurement that the data 
        /// value was calculated from. May be null.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(String text, Measurement measurement)
        {
            if ( ! IsAsciiMode)
            {
                throw new InvalidOperationException(
                    "ASCII commands cannot be sent when the port is in " +
                    "binary mode.");
            }
            String textToSend =
                text.StartsWith("/", StringComparison.Ordinal)
                ? text
                : "/" + text;

            if (AreChecksumsSent)
            {
                byte checksum = 0x00;
                var core = textToSend.Substring(1);
                foreach (byte c in Encoding.ASCII.GetBytes(core))
                {
                    checksum = (byte)((checksum + c) & 0xFF);
                }
                checksum = (byte)(((checksum ^ 0xFF) + 1) & 0xFF);
                textToSend += String.Format(
                    CultureInfo.InvariantCulture,
                    ":{0:X2}",
                    checksum);
            }
            DataPacket dataPacket = new DataPacket(textToSend);
            dataPacket.Measurement = measurement;
            _log.Debug("Sending.");

            WriteData(System.Text.Encoding.ASCII.GetBytes(textToSend + "\r\n"));

            OnDataPacketSent(dataPacket);
        }

        /// <summary>
        /// Send a text-mode command to a device on the chain.
        /// </summary>
        /// <param name="text">The full command to send to the device.</param>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="IOException">The port is in an invalid state.
        /// </exception>
        public void Send(String text)
        {
            Send(text, null);
        }

        private void OnDataPacketSent(DataPacket dataPacket)
        {
            if (_log.IsDebugEnabled)
            {
                _log.Debug(dataPacket.FormatRequest());
            }
            if (DataPacketSent != null)
            {
                var e = new DataPacketEventArgs(dataPacket);
                var receivers = DataPacketSent.GetInvocationList();
                foreach (EventHandler<DataPacketEventArgs> receiver in receivers)
                {
                    var result = receiver.BeginInvoke(this, e, null, null);
                    SubscriberTimer.WaitOne(result.AsyncWaitHandle);
                }
            }
        }

        /// <summary>
        /// Send a command to a device on the chain after a delay.
        /// </summary>
        /// <param name="deviceNumber">See <see cref="DataPacket.DeviceNumber"/>.</param>
        /// <param name="command">See <see cref="DataPacket.Command"/>.</param>
        /// <param name="data">See <see cref="DataPacket.Data"/>.</param>
        /// <returns>The data packet that will be sent. The data packet can be
        /// sent to <see cref="CancelDelayedPacket"/> to stop it from being
        /// sent.</returns>
        /// <remarks>Wait for <see cref="DelayMilliseconds"/> before sending
        /// the command. If you call this method several times, the commands
        /// get queued up and sent one at a time with a delay before each one.
        /// </remarks>
        public DataPacket SendDelayed(byte deviceNumber, Command command, int data)
        {
            DataPacket dataPacket = new DataPacket();
            dataPacket.DeviceNumber = deviceNumber;
            dataPacket.Command = command;
            dataPacket.Data = data;

            lock (_delayedPackets)
            {
                _delayedPackets.AddLast(dataPacket);
                EnableDelayedSendTimer(true);
            }
            return dataPacket;
        }

        /// <summary>
        /// Get or set the delay period to use with <see cref="SendDelayed"/>.
        /// </summary>
        public int DelayMilliseconds { get; set; }

        /// <summary>
        /// Gets or sets the timer that waits for an event subscriber to finish
        /// processing an event.
        /// </summary>
        /// <remarks>When a data packet is sent or received, this class
        /// will raise the event for each subscriber and wait for them to 
        /// finish processing it, so that all the events will be processed
        /// in order. However, a badly behaved subscriber could block the 
        /// thread and stop any more events from being raised. To avoid that,
        /// this class will wait for a subscriber to process an event until
        /// this timer times out. (The default is 500ms.) After that, it
        /// will stop waiting and raise the event to any other subscribers.
        /// </remarks>
        public TimeoutTimer SubscriberTimer { get; set; }

        /// <summary>
        /// Cancel a delayed packet that was requested by 
        /// <see cref="SendDelayed"/>.
        /// </summary>
        /// <param name="packet">The packet that was returned by 
        /// <see cref="SendDelayed"/>.</param>
        /// <returns>True if the packet was canceled before being sent, otherwise 
        /// false.</returns>
        public bool CancelDelayedPacket(DataPacket packet)
        {
            lock (_delayedPackets)
            {
                bool isFound = _delayedPackets.Remove(packet);
                if (_delayedPackets.Count == 0)
                {
                    EnableDelayedSendTimer(false);
                }
                return isFound;
            }
        }

        /// <summary>
        /// Write a stream of bytes to the port.
        /// </summary>
        /// <param name="data">The bytes to write</param>
        /// <remarks>This is protected so that it can be stubbed out during
        /// testing.</remarks>
        /// <exception cref="InvalidOperationException">The specified port is
        /// not open.</exception>
        /// <exception cref="TimeoutException">The operation did not complete
        /// before the time-out period ended.</exception>
        protected virtual void WriteData(byte[] data)
        {
            lock (_sendLock)
            {
                _serialPort.Write(
                        data,
                        0,
                        data.Length);
            }
        }

        /// <summary>
        /// Close and release the serial port. This must be called to avoid
        /// locking the port when you are finished with it.
        /// </summary>
        /// <exception cref="InvalidOperationException">The port is not open.
        /// </exception>
        /// <exception cref="IOException">The port does not exist, or a 
        /// sharing violation has occurred.</exception>
        public void Close()
        {
            lock (_delayedPackets)
            {
                _delayedPackets.Clear();
                EnableDelayedSendTimer(false);
            }
            _isClosing = true;
            if ( _receiveThread != null &&
                ! _receiveThread.Join(_serialPort.ReadTimeout * 2))
            {
                throw new TimeoutException("Receive thread could not be stopped.");
            }
            _isClosing = false;
            _receiveThread = null;
            _log.Debug("Closing.");
            _serialPort.Close();
            _log.Debug("Closed.");
        }

        /// <summary>
        /// Handle the timer event for sending delayed requests.
        /// </summary>
        /// <param name="ignored">State object for the timer. Not used.</param>
        protected virtual void OnDelayedSendTimer(object ignored)
        {
            DataPacket dataPacket;
            lock (_delayedPackets)
            {
                if (_delayedPackets.Count == 0)
                {
                    // port might have been closed during delay
                    return;
                }
                dataPacket = _delayedPackets.First.Value;
                _delayedPackets.RemoveFirst();
                if (_delayedPackets.Count == 0)
                {
                    EnableDelayedSendTimer(false);
                }
            }
            Send(dataPacket.DeviceNumber, dataPacket.Command, dataPacket.Data);
        }

        /// <summary>
        /// Turn the delayed send timer on or off.
        /// </summary>
        /// <remarks>The timer is used for the <see cref="SendDelayed"/> 
        /// method.</remarks>
        /// <param name="isEnabled">True to enable the timer, otherwise 
        /// false.</param>
        protected virtual void EnableDelayedSendTimer(bool isEnabled)
        {
            int milliseconds =
                isEnabled
                ? DelayMilliseconds
                : Timeout.Infinite;
            _delayedSendTimer.Change(milliseconds, milliseconds);
        }

        /// <summary>
        /// Report that an invalid packet was received by raising the 
        /// <see cref="ErrorReceived"/> event.
        /// </summary>
        /// <remarks>This can be called by any higher-level code that decides
        /// a packet was invalid. An example would be if the device number
        /// doesn't match any known devices.</remarks>
        public void ReportInvalidPacket()
        {
            OnErrorReceived(ZaberPortError.InvalidPacket);
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // not necessary here, but might be for derived types.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        /// <param name="isDisposing">True if the object is being disposed, and not
        /// garbage collected.</param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (_serialPort.Container != null) // Force the container to dispose too.
                    _serialPort.Container.Dispose();

                if (_baseStream != null && _baseStream.CanRead)
                {   // Force the base stream to close first before disposing, 
                    // even if _serialPort thinks it is closed.
                    _baseStream.Close();
                    GC.ReRegisterForFinalize(_baseStream);
                }

                _serialPort.Dispose();
                _packetConverter.Dispose();
                _delayedSendTimer.Dispose();
            }
        }

        #endregion
    }
}
