﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// Holds the combination of a measured value and its unit of measure.
    /// </summary>
    [Serializable]
    public class Measurement
    {
        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        /// <param name="measurement">The measurement to make a copy of.</param>
        public Measurement(Measurement measurement)
        {
            Value = measurement.Value;
            Unit = measurement.Unit;
        }
        
        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        /// <param name="value">The measured value.</param>
        /// <param name="unit">What units the measured value is in.</param>
        public Measurement(decimal value, UnitOfMeasure unit)
        {
            Value = value;
            Unit = unit;
        }

        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        /// <param name="value">The measured value.</param>
        /// <param name="unit">What units the measured value is in.</param>
        public Measurement(double value, UnitOfMeasure unit)
        {
            Value = (decimal)value;
            Unit = unit;
        }

        /// <summary>
        /// Initialize a new instance.
        /// </summary>
        /// <param name="value">The measured value.</param>
        /// <param name="unit">What units the measured value is in.</param>
        public Measurement(int value, UnitOfMeasure unit)
        {
            Value = (decimal)value;
            Unit = unit;
        }

        /// <summary>
        /// Gets the measured value.
        /// </summary>
        public decimal Value { get; private set; }

        /// <summary>
        /// Describes what units the measured value is in.
        /// </summary>
        public UnitOfMeasure Unit { get; private set; }

        /// <summary>
        /// Compare with another object.
        /// </summary>
        /// <param name="obj">the object to compare with</param>
        /// <returns>True if it is another Measurement with the same value and
        /// units, otherwise false.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            var other = obj as Measurement;
            if (other == null)
            {
                return false;
            }
            return Equals(Unit, other.Unit) && Equals(Value, other.Value);
        }

        /// <summary>
        /// Serves as a hash function for this type.
        /// </summary>
        /// <returns>A hash code for the current Measurement.</returns>
        public override int GetHashCode()
        {
            return Unit.GetHashCode() ^ Value.GetHashCode();
        }

        /// <summary>
        /// Convert the Measurement to a string.
        /// </summary>
        /// <returns>A string representation of the Measurement.</returns>
        public override string ToString()
        {
            return Value + (Unit == null ? "" : Unit.Abbreviation);
        }

        /// <summary>
        /// Adds two specified measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement.</param>
        /// <returns>The sum of the two measurements, using the second 
        /// parameter's units of measure.</returns>
        /// <exception cref="ArgumentException">The units of the parameters
        /// are incompatible.</exception>
        static public Measurement operator +(
            Measurement first, 
            Measurement second)
        {
            var aValue = first.Value;
            if (first.Unit != second.Unit)
            {
                var conversions = ConversionMap.Common.Find(first.Unit);
                if (conversions == null || 
                    ! conversions.UnitsOfMeasure.Contains(second.Unit))
                {
                    throw new ArgumentException(String.Format(
                        CultureInfo.CurrentUICulture,
                        "Units of measure {0} and {1} cannot be added together.",
                        first.Unit,
                        second.Unit));
                }
                aValue = conversions.Convert(
                    first,
                    second.Unit).Value;
            }
            return new Measurement(
                aValue + second.Value,
                second.Unit);
        }

        /// <summary>
        /// Adds two specified measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement.</param>
        /// <returns>The sum of the two measurements, using the second 
        /// parameter's units of measure.</returns>
        /// <exception cref="ArgumentException">The units of the parameters
        /// are incompatible.</exception>
        static public Measurement Add(Measurement first, Measurement second)
        {
            return first + second;
        }

        /// <summary>
        /// Subtracts two specified measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement.</param>
        /// <returns>The second measurement subtracted from the first, using 
        /// the second parameter's units of measure.</returns>
        /// <exception cref="ArgumentException">The units of the parameters
        /// are incompatible.</exception>
        static public Measurement operator -(
            Measurement first,
            Measurement second)
        {
            return first + (-second);
        }

        /// <summary>
        /// Subtracts two specified measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement.</param>
        /// <returns>The second measurement subtracted from the first, using 
        /// the second parameter's units of measure.</returns>
        /// <exception cref="ArgumentException">The units of the parameters
        /// are incompatible.</exception>
        static public Measurement Subtract(
            Measurement first,
            Measurement second)
        {
            return first - second;
        }

        /// <summary>
        /// Negates the value of a specified measurement.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <returns>The negative value of the measurement.</returns>
        static public Measurement operator -(Measurement measurement)
        {
            return new Measurement(-measurement.Value, measurement.Unit);
        }

        /// <summary>
        /// Negates the value of a specified measurement.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <returns>The negative value of the measurement.</returns>
        static public Measurement Negate(Measurement measurement)
        {
            return -measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(
            decimal ratio, 
            Measurement measurement)
        {
            return new Measurement(
                ratio * measurement.Value,
                measurement.Unit);
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(double ratio, Measurement measurement)
        {
            return ((decimal)ratio) * measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(int ratio, Measurement measurement)
        {
            return ((decimal)ratio) * measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(Measurement measurement, decimal ratio)
        {
            return ratio * measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(Measurement measurement, double ratio)
        {
            return ratio * measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement operator *(Measurement measurement, int ratio)
        {
            return ratio * measurement;
        }

        /// <summary>
        /// Multiplies a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement multiplied by the ratio.</returns>
        static public Measurement Multiply(Measurement measurement, double ratio)
        {
            return measurement * ratio;
        }

        /// <summary>
        /// Divides a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement divided by the ratio.</returns>
        static public Measurement operator /(Measurement measurement, decimal ratio)
        {
            return new Measurement(measurement.Value / ratio, measurement.Unit);
        }

        /// <summary>
        /// Divides a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement divided by the ratio.</returns>
        static public Measurement operator /(Measurement measurement, double ratio)
        {
            return measurement / (decimal)ratio;
        }

        /// <summary>
        /// Divides a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement divided by the ratio.</returns>
        static public Measurement operator /(Measurement measurement, int ratio)
        {
            return measurement / (decimal)ratio;
        }

        /// <summary>
        /// Divides a measurement by a ratio.
        /// </summary>
        /// <param name="measurement">A measurement.</param>
        /// <param name="ratio">A ratio.</param>
        /// <returns>The measurement divided by the ratio.</returns>
        static public Measurement Divide(Measurement measurement, double ratio)
        {
            return measurement / ratio;
        }

        /// <summary>
        /// Compare two measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement</param>
        /// <returns>True if the two measurements have the same values
        /// and units, otherwise false.</returns>
        static public bool operator ==(Measurement first, Measurement second)
        {
            return Object.Equals(first, second);
        }

        /// <summary>
        /// Compare two measurements.
        /// </summary>
        /// <param name="first">A measurement.</param>
        /// <param name="second">A measurement</param>
        /// <returns>False if the two measurements have the same values
        /// and units, otherwise true.</returns>
        static public bool operator !=(Measurement first, Measurement second)
        {
            return ! Object.Equals(first, second);
        }
    }
}
