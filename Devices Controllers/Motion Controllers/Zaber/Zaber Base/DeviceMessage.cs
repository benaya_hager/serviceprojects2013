using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Zaber
{
    /// <summary>
    /// A device message is a <see cref="DataPacket"/> with some added information.
    /// </summary>
    /// <remarks>
    /// When a <see cref="ZaberDevice"/> receives a response from the port, it
    /// adds context details before raising its own event.
    /// </remarks>
    [Serializable]
    public class DeviceMessage : DataPacket
    {
        private byte messageId;
        private bool isMessageIdSet = false;
        private CommandInfo commandInfo;

        public DeviceMessage()
        {
        }

        /// <summary>
        /// Parse a text message into a device message.
        /// </summary>
        /// <param name="text">parse details from this text</param>
        public DeviceMessage(String text)
            : base(text)
        {
        }

        /// <summary>
        /// Copy constructor that copies a DataPacket.
        /// </summary>
        /// <param name="source">copy details from this packet</param>
        public DeviceMessage(DataPacket source)
            : base(source)
        {
            var other = source as DeviceMessage;
            if (other != null)
            {
                messageId = other.messageId;
                isMessageIdSet = other.isMessageIdSet;
                commandInfo = other.commandInfo;
            }
        }

        /// <summary>
        /// More details about the command specified in <see cref="Command"/>.
        /// </summary>
        /// <value>
        /// May be null if the command is not recognized.
        /// </value>
        public CommandInfo CommandInfo
        {
            get { return commandInfo; }
            set { commandInfo = value; }
        }

        /// <summary>
        /// Gets or sets the request message that generated this response.
        /// </summary>
        /// <remarks>This is only set on response messages, and only when
        /// the request is known.</remarks>
        public DeviceMessage Request { get; set; }

        /// <summary>
        /// Message Ids allow you to coordinate responses with the requests 
        /// that triggered them.
        /// </summary>
        /// <remarks>
        /// Messages Ids are also known as logical channels.
        /// </remarks>
        /// <value>
        /// If Message has an Id, it will be between 1 and 254. Otherwise, this
        /// will be 0.
        /// </value>
        public byte MessageId
        {
            get { return messageId; }
            set { messageId = value; isMessageIdSet = true; }
        }

        /// <summary>
        /// Format the message as a string, assuming that it was sent as a 
        /// request.
        /// </summary>
        /// <returns>The message as a string</returns>
        public override string FormatRequest()
        {
            string paramDescription =
                commandInfo == null
                ? null
                : commandInfo.DataDescription;
            return Format("requests", paramDescription);
        }

        /// <summary>
        /// Format the message as a string, assuming that it was sent as a 
        /// response.
        /// </summary>
        /// <returns>The message as a string</returns>
        public override string FormatResponse()
        {
            string paramDescription =
                commandInfo == null
                ? null
                : commandInfo.ResponseDescription;
            return Format("responds", paramDescription);
        }

        private string Format(
            string action,
            string paramDescription)
        {
            if (Text != null)
            {
                return Text;
            }
            StringBuilder result = new StringBuilder();

            string deviceNumberText = 
                DeviceNumber.ToString(CultureInfo.CurrentCulture).PadLeft(3);
            string messageIdText =
                !isMessageIdSet
                ? string.Empty
                : messageId == 0
                ? "     "
                : String.Format(
                    CultureInfo.CurrentCulture,
                    "{0:000}: ",
                    messageId);
            if (commandInfo != null)
            {
                string param =
                    paramDescription == null
                    ? String.Empty
                    : Command == Command.Error && Enum.IsDefined(typeof(ZaberError), Data)
                    ? String.Format(
                        CultureInfo.CurrentCulture,
                        "({0})",
                        ((ZaberError)Data))
                    : String.Format(
                        CultureInfo.CurrentCulture,
                        "({0} = {1})",
                        paramDescription,
                        Data);
                result.AppendFormat(
                    CultureInfo.CurrentCulture,
                    "{4}Device {0} {3} {1}{2}",
                    deviceNumberText,
                    commandInfo.Name,
                    param,
                    action,
                    messageIdText);
            }
            else
            {
                result.AppendFormat(
                    CultureInfo.CurrentCulture,
                    "{4}Device {0} {3} {1}({2})",
                    deviceNumberText,
                    Command,
                    Data,
                    action,
                    messageIdText);
            }

            return result.ToString();
        }
    }
}
