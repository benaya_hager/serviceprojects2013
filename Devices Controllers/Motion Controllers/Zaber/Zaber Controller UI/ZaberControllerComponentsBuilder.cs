﻿using log4net;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using MVVM_Dialogs;
using MVVM_Dialogs.Interfaces; 
using SFW;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows; 
using Zaber_Common.Interfaces;
using NS_Zaber_Motion_Controller; 
namespace Zaber_Controller_UI
{
    public class ZaberControllerComponentsBuilder : IZaberControllerComponentsBuilder
    {
        #region Private Data Members

        private ILog m_SystemLog;
        private const String INITIAL_UNITS_CONVERSION_PARAMETERS_FILE = @"C:\Medinol\AVI IV\Motion Controller Data\Axes.xml";

        #endregion Private Data Members

        #region Public properties

        private GUIFactory m_GuiFactory;
        public GUIFactory GuiFactory
        {
            get { return m_GuiFactory; }
        }

        private IUnityContainer m_Container;
        public IUnityContainer Container
        {
            get { return m_Container; }
        }

        protected Dictionary<string, IComponent> m_Components;
        public Dictionary<string, IComponent> ComponentsList
        {
            get { return m_Components; }
            set { m_Components = value; }
        }



        #endregion Public properties

        public ZaberControllerComponentsBuilder(IUnityContainer container)
        {

            m_Container = container;

            SetApplicationUnhandledExeptionsHandle();

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"Zaber Motion Controller.config"));

            m_Components = new Dictionary<string, IComponent>();
            m_GuiFactory = new GUIFactory(m_Components);

            m_GuiFactory.CreateItem("frmSystemLog");

            m_SystemLog = LogManager.GetLogger("SystemLog");

            m_Container.RegisterType<IManageFilesDialogsViewModel, ManageFilesDialogsViewModel>();

            CreateZaberControllerObjects();

           
            //m_Container.RegisterType<ISelect_STP_Data_ViewModel, Select_STP_Data_ViewModel>(new InjectionConstructor(container, new NS_BLL.Logics.STP_Logic()));


        }

        #region Public Methods



        #endregion

        #region Private Methods

        #region Create Objects

        private void CreateZaberControllerObjects()
        {
            m_SystemLog.Info("Started Zaber Motion Controller Objects Initialization");

            #region Device

            m_Container.RegisterType<IZaberMotionController, ZaberMotionController>("ZaberMotionController");

            IZaberMotionController controller = m_Container.Resolve<IZaberMotionController>("ZaberMotionController");
            controller.LoadAxisXmlFile(INITIAL_UNITS_CONVERSION_PARAMETERS_FILE);
            //if (!m_Container.IsRegistered<IZaberMotionController>())
            //{
                m_Container.RegisterInstance<IZaberMotionController>(controller);
           // }
            lock (m_Components)
            {
                m_Components.Add("ZaberMotionController", (ZaberMotionController)controller);
            }

            #endregion

            #region Listener

            m_Container.RegisterType<IZaberControllerListener, ZaberControllerListener>(
                   "ZaberControllerListener",
                   new InjectionConstructor(controller));

            IZaberControllerListener listener = (IZaberControllerListener)m_Container.Resolve<IZaberControllerListener>("ZaberControllerListener");
            //if (!m_Container.IsRegistered<IZaberControllerListener>())
            //{
                m_Container.RegisterInstance<IZaberControllerListener>(listener);
            //}

            lock (m_Components)
            {
                m_Components.Add("ZaberControllerListener", (ZaberControllerListener)listener);
            }
            #endregion
            
            #region Logic

            m_Container.RegisterType<IZaberControllerLogic, ZaberControllerLogic>("ZaberControllerLogic",
                                                                     new InjectionConstructor(controller, listener));

            IZaberControllerLogic logic = (IZaberControllerLogic)m_Container.Resolve<IZaberControllerLogic>("ZaberControllerLogic");
            //if (!m_Container.IsRegistered<IZaberControllerLogic>())
            //{
                m_Container.RegisterInstance<IZaberControllerLogic>(logic);
            //}

            lock (m_Components)
            {
                m_Components.Add("ZaberControllerLogic", (ZaberControllerLogic)logic);
            }

            #endregion

            m_SystemLog.Info("Finished Zaber Motion Controller Objects Initialization");
        }

        #endregion

        #region Error handling

        private void SetApplicationUnhandledExeptionsHandle()
        {

            // Set the unhanded exception mode to force all Windows Forms errors to go through
            // our handler.
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            // Add the event handler for handling non-UI thread exceptions to the event.
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        //Handle the UI exceptions by showing a dialog box
        /// <summary>
        /// Handle the UI exceptions by showing a dialog box, and asking the user whether
        /// or not they wish to abort execution.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="t"></param>
        public static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            System.Windows.MessageBoxResult result = System.Windows.MessageBoxResult.Cancel;
            try
            {
                result = ShowThreadExceptionDialog("Windows Forms Error", e.Exception);
            }
            catch
            {
                try
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show("Fatal Windows Forms Error",
                                                        "Fatal Windows Forms Error",
                                                        System.Windows.MessageBoxButton.OK,
                                                        System.Windows.MessageBoxImage.Stop);
                }
                finally
                {
                    Application.Current.Shutdown();
                }
            }

            // Exits the program when the user clicks Abort.
            //if (result == DialogResult.Abort)
            Application.Current.Shutdown();
        }

        //Handle the Domain UI exceptions by showing a dialog box
        /// <summary>
        /// Handle the UI exceptions by showing a dialog box, and asking the user whether
        /// or not they wish to abort execution.
        /// NOTE: This exception cannot be kept from terminating the application - it can only
        /// log the event, and inform the user about it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                string errorMsg = "An application error occurred. Please contact the administrator " +
                    "with the following information:\n\n";

                // Since we can't prevent the app from terminating, log this to the event log.
                if (!EventLog.SourceExists("ThreadException"))
                {
                    EventLog.CreateEventSource("ThreadException", "Application");
                }

                // Create an EventLog instance and assign its source.
                EventLog myLog = new EventLog();
                myLog.Source = "ThreadException";
                myLog.WriteEntry(errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            }
            catch (Exception exc)
            {
                try
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show("Fatal Non-UI Error. Could not write the error to the event log. Reason: " + exc.Message,
                                    "Fatal Non-UI Error",
                                    System.Windows.MessageBoxButton.OK,
                                    System.Windows.MessageBoxImage.Stop);
                }
                finally
                {
                    Application.Current.Shutdown();
                }
            }
        }


        // Creates the error message and displays it.
        /// <summary>
        /// Creates the error message and displays it.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static System.Windows.MessageBoxResult ShowThreadExceptionDialog(string title, Exception e)
        {
            string errorMsg = "An application error occurred. Please contact the administrator " +
                    "with the following information:\n\n";

            errorMsg += e.Message + "\n\nStack Trace:\n" + e.StackTrace;

            return Xceed.Wpf.Toolkit.MessageBox.Show(errorMsg,
                                                     title,
                                                     System.Windows.MessageBoxButton.OK,
                                                    System.Windows.MessageBoxImage.Stop);
        }

        #endregion Error handling

        #endregion Private Methods




        RuntimeObjectsFactory IZaberControllerComponentsBuilder.GuiFactory
        {
            get { return m_GuiFactory; }
        }
    }
}
