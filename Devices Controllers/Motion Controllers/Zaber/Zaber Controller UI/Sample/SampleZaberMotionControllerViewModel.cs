﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NS_Common.ViewModels.Common;
using Zaber_Common;
using NS_Zaber_Motion_Controller;
using Zaber_Motion_Controller.Axis;

namespace Zaber_Controller_UI.Sample
{
    public class SampleZaberMotionControllerViewModel : ViewModelBase
    {

        [XmlIgnore]
        public RelayCommand<Object> UnloadMe { get; private set; }
        [XmlIgnore]
        public RelayCommand<Object> AddRoutine { get; private set; }


        private ZaberDevicesCollection m_ZaberAxesCollection = new ZaberDevicesCollection();
        public ZaberDevicesCollection ZaberAxesCollection  
        {
            get { return m_ZaberAxesCollection; }
            set
            {
                m_ZaberAxesCollection = value;
            }
        }

        public SampleZaberMotionControllerViewModel()
        {
            UnloadMe = new RelayCommand<Object>(p => this.UnloadMeFromTheList(p), p => true);
            AddRoutine = new RelayCommand<Object>(p => this.AddRoutineToTheList(p), p => true);


            OnCollectionChange("en-US");                    // Default buttons from English

           
        }

        private void OnCollectionChange(object lang)
        {
            m_ZaberAxesCollection.Add(new SingleAxis(1));
            m_ZaberAxesCollection.Add(new SingleAxis(2));
            m_ZaberAxesCollection.Add(new SingleAxis(3));
            //m_ZaberAxesCollection.Add(new SingleAxis(4));
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 1"));
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 2"));
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 6"));
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 24"));
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 44"));
        }


        protected virtual void AddRoutineToTheList(object routine)
        {
            //m_PerformanceRoutinesQueue.Add(new Routine("Test 1"));
        }

        protected virtual void UnloadMeFromTheList(object axis)
        { 
            //if (null != routine)
            //{
            //    System.Collections.IList items = (System.Collections.IList)routine;
            //    if (items.Count > 0)
            //        m_PerformanceRoutinesQueue.Remove((BaseRoutineDescriptor)items[0]); 
            //}
        }
    }
}
