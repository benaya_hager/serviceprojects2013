﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.Practices.Unity;
using SFW;
using NS_Zaber_Motion_Controller;

namespace ZaberTest.Component_Builders
{
    public class ComponentsBuilder : SFW.ComponentBuilder
    {
        #region Private Data Members

        private ILog m_SystemLog;
        private UnityContainer m_Container;
         
        #endregion Private Data Members

        #region Public properties

        private GUIFactory m_GuiFactory;
        public GUIFactory GuiFactory
        {
            get { return m_GuiFactory; }
        }

        #endregion Public properties

        public override void CreateComponents()
        {
            m_Components = new Dictionary<string, IComponent>();

            m_GuiFactory = new GUIFactory(m_Components);

            using (m_Container = new UnityContainer())
            {
                CreateZaberControllerObjects();
            }
        }

        public override void CreateMainWindow()
        {
            m_GuiFactory.CreateItem("frmMainForm");
        }

        protected override void RegisterLogics()
        {
            //throw new NotImplementedException();
        }

        protected override void RegisterListeners()
        {
            //throw new NotImplementedException();
        }

        protected override void RegisterDevices()
        {
           //
        }



        private void CreateZaberControllerObjects()
        {
            #region Device

            m_Container.RegisterType<IIODevice, ZaberMotionController>("ZaberMotionController",
                                                               new InjectionConstructor(m_Container));
            ZaberMotionController motion_cntrl = (ZaberMotionController)m_Container.Resolve<IIODevice>("ZaberMotionController");
             
            lock (m_Components)
            {
                m_Components.Add("ZaberMotionController", motion_cntrl);
            }

            #endregion

            #region Listener

            ZaberControllerListener listener;
            m_Container.RegisterType<Listener, ZaberControllerListener>(
                   "ZaberListener",
                   new InjectionConstructor(m_Components["ZaberMotionController"]));

            listener = (ZaberControllerListener)m_Container.Resolve<Listener>("ZaberListener");
            lock (m_Components)
            {
                m_Components.Add("ZaberListener", listener);
            }

            #endregion Listener

            #region Logic

            ZaberControllerLogic logic;
            m_Container.RegisterType<Logic, ZaberControllerLogic>("ZaberLogic",
                                                        new InjectionConstructor(m_Components["ZaberMotionController"], m_Components["ZaberListener"]));

            logic = (ZaberControllerLogic)m_Container.Resolve<Logic>("ZaberLogic");

            lock (m_Components)
            {
                m_Components.Add("ZaberLogic", logic);
            }

            #endregion Logic
        }

    }
}
