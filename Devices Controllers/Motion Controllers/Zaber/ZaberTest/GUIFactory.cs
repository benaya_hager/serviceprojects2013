﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFW;
using NS_Zaber_Motion_Controller;

namespace ZaberTest
{
    public class GUIFactory : RuntimeObjectsFactory
    {
        public GUIFactory(Dictionary<string, IComponent> components)
            : base(components)
        { }

        public override SFW.IComponent CreateItem(string type)
        {
            return CreateItem(type, null);
        }

        public SFW.IComponent CreateItem(string type, Form OwnerWindow)
        {
            SFW.IComponent res = null;
            if (m_Components.ContainsKey(type))
                res = m_Components[type];
            else
            {
                switch (type)
                {
                    #region Zaber Test Main Form

                    case "frmMainForm":
                        {
                            m_Components.Add("frmMainForm", new frmMainForm("frmMainForm", ((ZaberMotionController)m_Components["ZaberMotionController"]).ListOfAvailableAxes ));
                            ((System.Windows.Forms.Form)m_Components["frmMainForm"]).FormClosing += new System.Windows.Forms.FormClosingEventHandler(GUIFactory_FormClosing);

                            ((frmMainForm)m_Components["frmMainForm"]).EventGroupReady += ((Logic)m_Components["ZaberLogic"]).OnProcessEventGroup;
                            ((Listener)m_Components["ZaberListener"]).EventGroupReady += ((frmMainForm)m_Components["frmMainForm"]).OnProcessEventGroup;
 
                            CreateChildWindows(m_Components["frmMainForm"]);
                            res = m_Components["frmMainForm"];
                            break;
                        }

                    #endregion Reaction Force Tester Main Menu Form

                    default:
                        {
                            res = null;
                            break;
                        }
                }
            }
            return res;
        }

        public override void ReleaseItem(string type)
        {
            switch (type)
            {
                case "frmSystemLog":
                    {
                        m_Components.Remove("frmSystemLog");
                        break;
                    }
            }
        }


        private void GUIFactory_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.Cancel == true) return;

            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }


        private void CreateChildWindows(IComponent component)
        {
            System.Reflection.MethodInfo mi = component.GetType().GetMethod("CreateChildWindows");
            if (mi != null)
                mi.Invoke(component, new object[] { this });
        }
    }
}
