﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SFW;

namespace ZaberTest
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ZaberTest.Component_Builders.ComponentsBuilder builder = new ZaberTest.Component_Builders.ComponentsBuilder();
            builder.Build();

            builder.CreateMainWindow();

            Application.Run(((System.Windows.Forms.Form)(builder.ComponentsList["frmMainForm"])));
        }
    }
}
