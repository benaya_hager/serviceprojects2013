﻿namespace ZaberTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStop = new System.Windows.Forms.Button();
            this.btnGo = new System.Windows.Forms.Button();
            this.nudTargetPosition = new System.Windows.Forms.NumericUpDown();
            this.nudSpeed = new System.Windows.Forms.NumericUpDown();
            this.nudAcceleration = new System.Windows.Forms.NumericUpDown();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.lblAcceleration = new System.Windows.Forms.Label();
            this.lblTargetPosition = new System.Windows.Forms.Label();
            this.lblCurrentPosition = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = global::ZaberTest.Properties.Resources.stop_icon;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Location = new System.Drawing.Point(37, 12);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(159, 163);
            this.btnStop.TabIndex = 0;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnGo
            // 
            this.btnGo.BackgroundImage = global::ZaberTest.Properties.Resources.TM_Mot_ico;
            this.btnGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGo.Location = new System.Drawing.Point(564, 268);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(89, 83);
            this.btnGo.TabIndex = 1;
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // nudTargetPosition
            // 
            this.nudTargetPosition.Location = new System.Drawing.Point(626, 410);
            this.nudTargetPosition.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudTargetPosition.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudTargetPosition.Name = "nudTargetPosition";
            this.nudTargetPosition.Size = new System.Drawing.Size(81, 20);
            this.nudTargetPosition.TabIndex = 2;
            this.nudTargetPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudTargetPosition.Value = new decimal(new int[] {
            13000,
            0,
            0,
            0});
            // 
            // nudSpeed
            // 
            this.nudSpeed.Location = new System.Drawing.Point(520, 381);
            this.nudSpeed.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudSpeed.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudSpeed.Name = "nudSpeed";
            this.nudSpeed.Size = new System.Drawing.Size(81, 20);
            this.nudSpeed.TabIndex = 3;
            this.nudSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudSpeed.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudSpeed.ValueChanged += new System.EventHandler(this.nudSpeed_ValueChanged);
            // 
            // nudAcceleration
            // 
            this.nudAcceleration.Location = new System.Drawing.Point(520, 428);
            this.nudAcceleration.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudAcceleration.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudAcceleration.Name = "nudAcceleration";
            this.nudAcceleration.Size = new System.Drawing.Size(81, 20);
            this.nudAcceleration.TabIndex = 4;
            this.nudAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudAcceleration.Value = new decimal(new int[] {
            205,
            0,
            0,
            0});
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(520, 362);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(38, 13);
            this.lblSpeed.TabIndex = 5;
            this.lblSpeed.Text = "Speed";
            // 
            // lblAcceleration
            // 
            this.lblAcceleration.AutoSize = true;
            this.lblAcceleration.Location = new System.Drawing.Point(520, 412);
            this.lblAcceleration.Name = "lblAcceleration";
            this.lblAcceleration.Size = new System.Drawing.Size(66, 13);
            this.lblAcceleration.TabIndex = 6;
            this.lblAcceleration.Text = "Acceleration";
            // 
            // lblTargetPosition
            // 
            this.lblTargetPosition.AutoSize = true;
            this.lblTargetPosition.Location = new System.Drawing.Point(628, 394);
            this.lblTargetPosition.Name = "lblTargetPosition";
            this.lblTargetPosition.Size = new System.Drawing.Size(78, 13);
            this.lblTargetPosition.TabIndex = 7;
            this.lblTargetPosition.Text = "Target Position";
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.AutoSize = true;
            this.lblCurrentPosition.Location = new System.Drawing.Point(561, 231);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(84, 13);
            this.lblCurrentPosition.TabIndex = 8;
            this.lblCurrentPosition.Text = "Current Position:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 493);
            this.Controls.Add(this.lblCurrentPosition);
            this.Controls.Add(this.lblTargetPosition);
            this.Controls.Add(this.lblAcceleration);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.nudAcceleration);
            this.Controls.Add(this.nudSpeed);
            this.Controls.Add(this.nudTargetPosition);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.btnStop);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.NumericUpDown nudTargetPosition;
        private System.Windows.Forms.NumericUpDown nudSpeed;
        private System.Windows.Forms.NumericUpDown nudAcceleration;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Label lblAcceleration;
        private System.Windows.Forms.Label lblTargetPosition;
        private System.Windows.Forms.Label lblCurrentPosition;
    }
}

