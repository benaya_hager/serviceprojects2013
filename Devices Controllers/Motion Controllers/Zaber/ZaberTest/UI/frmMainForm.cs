﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using NS_Zaber_Motion_Controller;

namespace ZaberTest
{
    public partial class frmMainForm : Form, IDisposable, IEventGroupConsumer, SFW.IComponent
    {
        #region private members

            UnityContainer m_Container;

            Dictionary<int, String> m_AxisIds2NamesList = new Dictionary<int, string>(); 


            private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;


            private List<Int32> m_ListOfAvailibleAxesIds;
        #endregion

        #region Public members

        public event EventGroupHandler EventGroupReady;

        #endregion Public members

        #region public properties

        protected String m_MyComponentName;
        private string p;
        private List<int> list;
        public String MyComponentName
        {
            get { return m_MyComponentName; }
            protected set { m_MyComponentName = value; }
        }
 
        #endregion
        
        #region Constructors

        public frmMainForm(String component_name, List<Int32> list_of_availible_axes)
        {
            InitializeComponent();

            m_ListOfAvailibleAxesIds = list_of_availible_axes;

            foreach (Int32 axis_id in m_ListOfAvailibleAxesIds)
            {
                cmbxAxesList.Items.Add(axis_id); 
            }
            if (cmbxAxesList.Items.Count >=1 )
                cmbxAxesList.SelectedIndex = 0;// 1;


            m_MyComponentName = component_name;
             
            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapHandlers(); 
        }
         

        #endregion

        #region GUI events handlers

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClearHandlers();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            Byte selected_axis_id = Convert.ToByte( cmbxAxesList.SelectedItem);

            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_ABSOLUTE, new ZaberCommandPrmMoveAbsMM(selected_axis_id, 
                                                                                                                       Convert.ToSingle(nudTargetPosition.Value),
                                                                                                                       Convert.ToSingle(nudSpeed.Value),
                                                                                                                       Convert.ToSingle(nudAcceleration.Value))));
        }

        private void btnStartRelativeMotion_Click(object sender, EventArgs e)
        {
            Byte selected_axis_id = Convert.ToByte(cmbxAxesList.SelectedItem);

            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_RELATIVE, new ZaberCommandPrmMoveRelMM(selected_axis_id,
                                                                                                                       Convert.ToSingle(nudRelativeMotionOffset.Value),
                                                                                                                       Convert.ToSingle(nudSpeed.Value),
                                                                                                                       Convert.ToSingle(nudAcceleration.Value))));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {

            Byte selected_axis_id = Convert.ToByte(cmbxAxesList.SelectedItem);
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_MOTION, new ZaberCommandPrmStopAxisMotion(selected_axis_id)));
        } 

        private void btnHome_Click(object sender, EventArgs e)
        {
            Byte selected_axis_id = Convert.ToByte(cmbxAxesList.SelectedItem);
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SEND_AXIS_TO_HOME_POSITION, new ZaberCommandEventParameters(selected_axis_id)));
        }

        private void btnMonitoringState_Click(object sender, EventArgs e)
        {
            Byte selected_axis_id = Convert.ToByte(cmbxAxesList.SelectedItem);

            if (Convert.ToInt32(btnMonitoringState.Tag) == 0)
            {
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_START_AXIS_STATUS_MONITORING, new ZaberCommandPrmAxisMonitoringThread(selected_axis_id, Convert.ToInt32(nudDeviceMonitoringTimeout.Value))));
            }
            else
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_STATUS_MONITORING, new ZaberCommandEventParameters(selected_axis_id)));

        }

        #endregion

        #region Private methods 
      
      

        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
            }
        }

        #endregion

        #region IEventGroupConsumer Members

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
        }

        #endregion IEventGroupConsumer Members


        private delegate void ZaberDataFormUpdateHandler(IEventData data);
        private void HandleZaberDataEvent(IEventData data)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new ZaberDataFormUpdateHandler(HandleZaberDataEvent), new Object[] { data });
            }
            else
            {

                switch (((ZaberDataEvent)data).DataType)
                {
                    case ZaberDataEventType.ET_CURRENT_POSITION_CHANGED:
                        {
                            CurrentPositionChanged((ZaberDataPrmCurPosChanged)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_MONITORING_THREAD_STATUS_CHANGED:
                        {
                            UpdateCurrentStateOfMonitoringThread((ZaberDataPrmDeviceMonitoringThreadStatus)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_STATUS_CHANGED:
                        {
                            UpdateDeviceCurrentState((ZaberDataPrmDeviceStatus)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_HOMED_STATUS_CHANGED:
                        {
                            UpdateAxisHomedStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break; 
                        }
                    case ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE:
                        {
                            UpdateInMotionStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                }
            }
        }

        private void UpdateInMotionStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            lblInMotion.Text = Convert.ToString(zaberDataPrmBoolean.BooleanParameter); 
        }

        private void UpdateAxisHomedStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            lblAxisHomedStatus.Text = String.Format("Axis Homed: ({0})", zaberDataPrmBoolean.BooleanParameter);
        }

        private void UpdateDeviceCurrentState(ZaberDataPrmDeviceStatus zaberDataPrmDeviceStatus)
        {
            lblDeviceCurrentState.Text = Tools.String_Enum.StringEnum.GetStringValue(zaberDataPrmDeviceStatus.DeviceStatus); 
        }

        private void UpdateCurrentStateOfMonitoringThread(ZaberDataPrmDeviceMonitoringThreadStatus zaberDataPrmDeviceMonitoringThreadStatus)
        {
            if (zaberDataPrmDeviceMonitoringThreadStatus.DeviceMonitoringThreadStatus)
            {
                btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Stop_Monitoring;
                btnMonitoringState.Tag = 1;
            }
            else
            {
                btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Start_Monitoring;
                btnMonitoringState.Tag = 0;
            }
        }

        private void CurrentPositionChanged(ZaberDataPrmCurPosChanged zaberDataPrmCurPosChanged)
        {
            lblCurrentPosition.Text = String.Format("Current Position: {0}", zaberDataPrmCurPosChanged.CurrentPosition);
        }

        

        

    }
}
