﻿namespace ZaberTest
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudTargetPosition = new System.Windows.Forms.NumericUpDown();
            this.nudSpeed = new System.Windows.Forms.NumericUpDown();
            this.nudAcceleration = new System.Windows.Forms.NumericUpDown();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.lblAcceleration = new System.Windows.Forms.Label();
            this.lblCurrentPosition = new System.Windows.Forms.Label();
            this.grbxControlAxes = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblTargetPosition = new System.Windows.Forms.Label();
            this.btnStartAbsoluteMotion = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.nudRelativeMotionOffset = new System.Windows.Forms.NumericUpDown();
            this.lblRelativeMotionOffset = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMonitoringState = new System.Windows.Forms.Button();
            this.lblDeviceMonitoring = new System.Windows.Forms.Label();
            this.lblTimeout = new System.Windows.Forms.Label();
            this.nudDeviceMonitoringTimeout = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblListOfAxes = new System.Windows.Forms.Label();
            this.cmbxAxesList = new System.Windows.Forms.ComboBox();
            this.btnStartRelativeMotion = new System.Windows.Forms.Button();
            this.lblDeviceCurrentStateTitle = new System.Windows.Forms.Label();
            this.lblDeviceCurrentState = new System.Windows.Forms.Label();
            this.lblAxisHomedStatus = new System.Windows.Forms.Label();
            this.lblInMotionTitle = new System.Windows.Forms.Label();
            this.lblInMotion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).BeginInit();
            this.grbxControlAxes.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRelativeMotionOffset)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeviceMonitoringTimeout)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nudTargetPosition
            // 
            this.nudTargetPosition.Location = new System.Drawing.Point(9, 26);
            this.nudTargetPosition.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudTargetPosition.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudTargetPosition.Name = "nudTargetPosition";
            this.nudTargetPosition.Size = new System.Drawing.Size(81, 20);
            this.nudTargetPosition.TabIndex = 2;
            this.nudTargetPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudTargetPosition.Value = new decimal(new int[] {
            13000,
            0,
            0,
            0});
            // 
            // nudSpeed
            // 
            this.nudSpeed.Location = new System.Drawing.Point(60, 52);
            this.nudSpeed.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudSpeed.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudSpeed.Name = "nudSpeed";
            this.nudSpeed.Size = new System.Drawing.Size(81, 20);
            this.nudSpeed.TabIndex = 3;
            this.nudSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudSpeed.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            // 
            // nudAcceleration
            // 
            this.nudAcceleration.Location = new System.Drawing.Point(64, 93);
            this.nudAcceleration.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudAcceleration.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudAcceleration.Name = "nudAcceleration";
            this.nudAcceleration.Size = new System.Drawing.Size(81, 20);
            this.nudAcceleration.TabIndex = 4;
            this.nudAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudAcceleration.Value = new decimal(new int[] {
            205,
            0,
            0,
            0});
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(74, 36);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(38, 13);
            this.lblSpeed.TabIndex = 5;
            this.lblSpeed.Text = "Speed";
            // 
            // lblAcceleration
            // 
            this.lblAcceleration.AutoSize = true;
            this.lblAcceleration.Location = new System.Drawing.Point(64, 77);
            this.lblAcceleration.Name = "lblAcceleration";
            this.lblAcceleration.Size = new System.Drawing.Size(66, 13);
            this.lblAcceleration.TabIndex = 6;
            this.lblAcceleration.Text = "Acceleration";
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.AutoSize = true;
            this.lblCurrentPosition.Location = new System.Drawing.Point(43, 113);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(84, 13);
            this.lblCurrentPosition.TabIndex = 8;
            this.lblCurrentPosition.Text = "Current Position:";
            // 
            // grbxControlAxes
            // 
            this.grbxControlAxes.Controls.Add(this.tableLayoutPanel1);
            this.grbxControlAxes.Location = new System.Drawing.Point(262, 12);
            this.grbxControlAxes.Name = "grbxControlAxes";
            this.grbxControlAxes.Size = new System.Drawing.Size(722, 411);
            this.grbxControlAxes.TabIndex = 9;
            this.grbxControlAxes.TabStop = false;
            this.grbxControlAxes.Text = "Control Axes";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.82428F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.17572F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.Controls.Add(this.panel5, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnStartAbsoluteMotion, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnStartRelativeMotion, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnStop, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnHome, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 392);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = global::ZaberTest.Properties.Resources.Apps_Home_icon;
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHome.Location = new System.Drawing.Point(3, 3);
            this.btnHome.Name = "btnHome";
            this.tableLayoutPanel1.SetRowSpan(this.btnHome, 2);
            this.btnHome.Size = new System.Drawing.Size(145, 170);
            this.btnHome.TabIndex = 10;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = global::ZaberTest.Properties.Resources.stop_icon;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStop.Location = new System.Drawing.Point(262, 74);
            this.btnStop.Name = "btnStop";
            this.tableLayoutPanel1.SetRowSpan(this.btnStop, 2);
            this.btnStop.Size = new System.Drawing.Size(198, 170);
            this.btnStop.TabIndex = 0;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblTargetPosition);
            this.panel5.Controls.Add(this.nudTargetPosition);
            this.panel5.Location = new System.Drawing.Point(466, 250);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(106, 56);
            this.panel5.TabIndex = 19;
            // 
            // lblTargetPosition
            // 
            this.lblTargetPosition.AutoSize = true;
            this.lblTargetPosition.Location = new System.Drawing.Point(6, 10);
            this.lblTargetPosition.Name = "lblTargetPosition";
            this.lblTargetPosition.Size = new System.Drawing.Size(78, 13);
            this.lblTargetPosition.TabIndex = 7;
            this.lblTargetPosition.Text = "Target Position";
            // 
            // btnStartAbsoluteMotion
            // 
            this.btnStartAbsoluteMotion.BackgroundImage = global::ZaberTest.Properties.Resources.TM_Mot_ico;
            this.btnStartAbsoluteMotion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStartAbsoluteMotion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartAbsoluteMotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnStartAbsoluteMotion.Location = new System.Drawing.Point(578, 250);
            this.btnStartAbsoluteMotion.Name = "btnStartAbsoluteMotion";
            this.btnStartAbsoluteMotion.Size = new System.Drawing.Size(135, 139);
            this.btnStartAbsoluteMotion.TabIndex = 1;
            this.btnStartAbsoluteMotion.Text = "Absolute";
            this.btnStartAbsoluteMotion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStartAbsoluteMotion.UseVisualStyleBackColor = true;
            this.btnStartAbsoluteMotion.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.nudRelativeMotionOffset);
            this.panel4.Controls.Add(this.lblRelativeMotionOffset);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(154, 250);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(102, 139);
            this.panel4.TabIndex = 19;
            // 
            // nudRelativeMotionOffset
            // 
            this.nudRelativeMotionOffset.Location = new System.Drawing.Point(14, 23);
            this.nudRelativeMotionOffset.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudRelativeMotionOffset.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudRelativeMotionOffset.Name = "nudRelativeMotionOffset";
            this.nudRelativeMotionOffset.Size = new System.Drawing.Size(81, 20);
            this.nudRelativeMotionOffset.TabIndex = 16;
            this.nudRelativeMotionOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudRelativeMotionOffset.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // lblRelativeMotionOffset
            // 
            this.lblRelativeMotionOffset.AutoSize = true;
            this.lblRelativeMotionOffset.Location = new System.Drawing.Point(14, 7);
            this.lblRelativeMotionOffset.Name = "lblRelativeMotionOffset";
            this.lblRelativeMotionOffset.Size = new System.Drawing.Size(52, 13);
            this.lblRelativeMotionOffset.TabIndex = 17;
            this.lblRelativeMotionOffset.Text = "Step Size";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblSpeed);
            this.panel3.Controls.Add(this.nudAcceleration);
            this.panel3.Controls.Add(this.lblAcceleration);
            this.panel3.Controls.Add(this.nudSpeed);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(262, 250);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(198, 139);
            this.panel3.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnMonitoringState);
            this.panel2.Controls.Add(this.lblDeviceMonitoring);
            this.panel2.Controls.Add(this.lblTimeout);
            this.panel2.Controls.Add(this.nudDeviceMonitoringTimeout);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(578, 3);
            this.panel2.Name = "panel2";
            this.tableLayoutPanel1.SetRowSpan(this.panel2, 2);
            this.panel2.Size = new System.Drawing.Size(135, 170);
            this.panel2.TabIndex = 19;
            // 
            // btnMonitoringState
            // 
            this.btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Start_Monitoring;
            this.btnMonitoringState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMonitoringState.Location = new System.Drawing.Point(24, 28);
            this.btnMonitoringState.Name = "btnMonitoringState";
            this.btnMonitoringState.Size = new System.Drawing.Size(95, 95);
            this.btnMonitoringState.TabIndex = 11;
            this.btnMonitoringState.Tag = "0";
            this.btnMonitoringState.UseVisualStyleBackColor = true;
            this.btnMonitoringState.Click += new System.EventHandler(this.btnMonitoringState_Click);
            // 
            // lblDeviceMonitoring
            // 
            this.lblDeviceMonitoring.AutoSize = true;
            this.lblDeviceMonitoring.Location = new System.Drawing.Point(21, 12);
            this.lblDeviceMonitoring.Name = "lblDeviceMonitoring";
            this.lblDeviceMonitoring.Size = new System.Drawing.Size(93, 13);
            this.lblDeviceMonitoring.TabIndex = 12;
            this.lblDeviceMonitoring.Text = "Device Monitoring";
            // 
            // lblTimeout
            // 
            this.lblTimeout.AutoSize = true;
            this.lblTimeout.Location = new System.Drawing.Point(55, 126);
            this.lblTimeout.Name = "lblTimeout";
            this.lblTimeout.Size = new System.Drawing.Size(45, 13);
            this.lblTimeout.TabIndex = 13;
            this.lblTimeout.Text = "Timeout";
            // 
            // nudDeviceMonitoringTimeout
            // 
            this.nudDeviceMonitoringTimeout.Location = new System.Drawing.Point(47, 142);
            this.nudDeviceMonitoringTimeout.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudDeviceMonitoringTimeout.Name = "nudDeviceMonitoringTimeout";
            this.nudDeviceMonitoringTimeout.Size = new System.Drawing.Size(53, 20);
            this.nudDeviceMonitoringTimeout.TabIndex = 14;
            this.nudDeviceMonitoringTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudDeviceMonitoringTimeout.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblListOfAxes);
            this.panel1.Controls.Add(this.cmbxAxesList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(262, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(198, 65);
            this.panel1.TabIndex = 19;
            // 
            // lblListOfAxes
            // 
            this.lblListOfAxes.AutoSize = true;
            this.lblListOfAxes.Location = new System.Drawing.Point(45, 12);
            this.lblListOfAxes.Name = "lblListOfAxes";
            this.lblListOfAxes.Size = new System.Drawing.Size(110, 13);
            this.lblListOfAxes.TabIndex = 9;
            this.lblListOfAxes.Text = "Selecet active axis ID";
            // 
            // cmbxAxesList
            // 
            this.cmbxAxesList.FormattingEnabled = true;
            this.cmbxAxesList.Location = new System.Drawing.Point(34, 28);
            this.cmbxAxesList.Name = "cmbxAxesList";
            this.cmbxAxesList.Size = new System.Drawing.Size(132, 21);
            this.cmbxAxesList.TabIndex = 8;
            // 
            // btnStartRelativeMotion
            // 
            this.btnStartRelativeMotion.BackgroundImage = global::ZaberTest.Properties.Resources.StartRelative;
            this.btnStartRelativeMotion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStartRelativeMotion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartRelativeMotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnStartRelativeMotion.Location = new System.Drawing.Point(3, 250);
            this.btnStartRelativeMotion.Name = "btnStartRelativeMotion";
            this.btnStartRelativeMotion.Size = new System.Drawing.Size(145, 139);
            this.btnStartRelativeMotion.TabIndex = 15;
            this.btnStartRelativeMotion.Text = "Relative";
            this.btnStartRelativeMotion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStartRelativeMotion.UseVisualStyleBackColor = true;
            this.btnStartRelativeMotion.Click += new System.EventHandler(this.btnStartRelativeMotion_Click);
            // 
            // lblDeviceCurrentStateTitle
            // 
            this.lblDeviceCurrentStateTitle.AutoSize = true;
            this.lblDeviceCurrentStateTitle.Location = new System.Drawing.Point(43, 200);
            this.lblDeviceCurrentStateTitle.Name = "lblDeviceCurrentStateTitle";
            this.lblDeviceCurrentStateTitle.Size = new System.Drawing.Size(109, 13);
            this.lblDeviceCurrentStateTitle.TabIndex = 10;
            this.lblDeviceCurrentStateTitle.Text = "Device Current State:";
            // 
            // lblDeviceCurrentState
            // 
            this.lblDeviceCurrentState.AutoSize = true;
            this.lblDeviceCurrentState.Location = new System.Drawing.Point(59, 228);
            this.lblDeviceCurrentState.Name = "lblDeviceCurrentState";
            this.lblDeviceCurrentState.Size = new System.Drawing.Size(27, 13);
            this.lblDeviceCurrentState.TabIndex = 11;
            this.lblDeviceCurrentState.Text = "N/A";
            // 
            // lblAxisHomedStatus
            // 
            this.lblAxisHomedStatus.AutoSize = true;
            this.lblAxisHomedStatus.Location = new System.Drawing.Point(43, 259);
            this.lblAxisHomedStatus.Name = "lblAxisHomedStatus";
            this.lblAxisHomedStatus.Size = new System.Drawing.Size(95, 13);
            this.lblAxisHomedStatus.TabIndex = 12;
            this.lblAxisHomedStatus.Text = "Axis Homed: (N/A)";
            // 
            // lblInMotionTitle
            // 
            this.lblInMotionTitle.AutoSize = true;
            this.lblInMotionTitle.Location = new System.Drawing.Point(43, 67);
            this.lblInMotionTitle.Name = "lblInMotionTitle";
            this.lblInMotionTitle.Size = new System.Drawing.Size(89, 13);
            this.lblInMotionTitle.TabIndex = 13;
            this.lblInMotionTitle.Text = "Motion Complete:";
            // 
            // lblInMotion
            // 
            this.lblInMotion.AutoSize = true;
            this.lblInMotion.Location = new System.Drawing.Point(137, 69);
            this.lblInMotion.Name = "lblInMotion";
            this.lblInMotion.Size = new System.Drawing.Size(0, 13);
            this.lblInMotion.TabIndex = 14;
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 493);
            this.Controls.Add(this.lblInMotion);
            this.Controls.Add(this.lblInMotionTitle);
            this.Controls.Add(this.lblAxisHomedStatus);
            this.Controls.Add(this.lblDeviceCurrentState);
            this.Controls.Add(this.lblDeviceCurrentStateTitle);
            this.Controls.Add(this.grbxControlAxes);
            this.Controls.Add(this.lblCurrentPosition);
            this.Name = "frmMainForm";
            this.Text = "Main Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).EndInit();
            this.grbxControlAxes.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRelativeMotionOffset)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeviceMonitoringTimeout)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStartAbsoluteMotion;
        private System.Windows.Forms.NumericUpDown nudTargetPosition;
        private System.Windows.Forms.NumericUpDown nudSpeed;
        private System.Windows.Forms.NumericUpDown nudAcceleration;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Label lblAcceleration;
        private System.Windows.Forms.Label lblCurrentPosition;
        private System.Windows.Forms.GroupBox grbxControlAxes;
        private System.Windows.Forms.ComboBox cmbxAxesList;
        private System.Windows.Forms.Label lblListOfAxes;
        private System.Windows.Forms.Label lblTargetPosition;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnMonitoringState;
        private System.Windows.Forms.Label lblDeviceMonitoring;
        private System.Windows.Forms.NumericUpDown nudDeviceMonitoringTimeout;
        private System.Windows.Forms.Label lblTimeout;
        private System.Windows.Forms.Label lblDeviceCurrentStateTitle;
        private System.Windows.Forms.Label lblDeviceCurrentState;
        private System.Windows.Forms.Label lblAxisHomedStatus;
        private System.Windows.Forms.Button btnStartRelativeMotion;
        private System.Windows.Forms.NumericUpDown nudRelativeMotionOffset;
        private System.Windows.Forms.Label lblRelativeMotionOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblInMotionTitle;
        private System.Windows.Forms.Label lblInMotion;
    }
}

