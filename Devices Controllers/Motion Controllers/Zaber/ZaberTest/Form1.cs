﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using Zaber_Motion_Controller;

namespace ZaberTest
{
    public partial class Form1 : Form, IDisposable, IEventGroupConsumer, SFW.IComponent
    {
        #region private members

            UnityContainer m_Container;

            Dictionary<int, String> m_AxisIds2NamesList = new Dictionary<int, string>(); 


            private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;

        #endregion

        #region Public members

        public event EventGroupHandler EventGroupReady;

        #endregion Public members

        #region public properties

        protected Dictionary<string, SFW.IComponent> m_Components;
        public Dictionary<string, SFW.IComponent> ComponentsList
        {
            get { return m_Components; }
            set { m_Components = value; }
        }

        #endregion

        #region Constractors

        public Form1()
        {
            InitializeComponent();
             
           // m_Container = new UnityContainer();

            m_Components = new Dictionary<string, SFW.IComponent>();

            using (m_Container = new UnityContainer())
            {
                CreateZaberControllerObjects(m_Container);
            }


            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapObjects();

            MapHandlers();
            
        }

        private void MapObjects()
        {
            this.EventGroupReady += ((Logic)m_Components["ZaberLogic"]).OnProcessEventGroup;
            ((Listener)m_Components["ZaberListener"]).EventGroupReady += this.OnProcessEventGroup;
        }

        #endregion

        #region GUI events handlers

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClearHandlers();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_ABSOLUTE, new ZaberCommandPrmMoveAbsMM(2, 
                                                                                                                       Convert.ToSingle(nudTargetPosition.Value),
                                                                                                                       Convert.ToSingle(nudSpeed.Value),
                                                                                                                       Convert.ToSingle(nudAcceleration.Value))));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_MOTION, new ZaberCommandPrmStopAxisMotion(1)));
        }

        #endregion

        #region Private methods 
      
        private void CreateZaberControllerObjects(IUnityContainer m_Container)
        { 
            #region Device

            m_Container.RegisterType<IIODevice, ZaberMotionController>("ZaberMotionController",
                                                               new InjectionConstructor(m_Container));
            Zaber_Motion_Controller.ZaberMotionController motion_cntrl = (ZaberMotionController)m_Container.Resolve<IIODevice>("ZaberMotionController");

            //foreach (object axis in motion_cntrl.ZaberAxesCollection)
            //{
            
            //}


            lock (m_Components)
            {
                m_Components.Add("ZaberMotionController", motion_cntrl);
            }

            #endregion

            #region Listener

            ZaberListener listener;
            m_Container.RegisterType<Listener, ZaberListener>(
                   "ZaberListener",
                   new InjectionConstructor(m_Components["ZaberMotionController"]));

            listener = (ZaberListener)m_Container.Resolve<Listener>("ZaberListener");
            lock (m_Components)
            {
                m_Components.Add("ZaberListener", listener);
            }

            #endregion Listener

            #region Logic

            ZaberLogic logic;
            m_Container.RegisterType<Logic, ZaberLogic>("ZaberLogic",
                                                        new InjectionConstructor(m_Components["ZaberMotionController"], m_Components["ZaberListener"]));

            logic = (ZaberLogic)m_Container.Resolve<Logic>("ZaberLogic");

            lock (m_Components)
            {
                m_Components.Add("ZaberLogic", logic);
            }

            #endregion Logic
        }

        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
            }
        }

        #endregion

        #region IEventGroupConsumer Members

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
        }

        #endregion IEventGroupConsumer Members


        private delegate void ZaberDataFormUpdateHandler(IEventData data);
        private void HandleZaberDataEvent(IEventData data)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new ZaberDataFormUpdateHandler(HandleZaberDataEvent), new Object[] { data });
            }
            else
            {

                switch (((ZaberDataEvent)data).DataType)
                {
                    case ZaberDataEventType.ET_CURRENT_POSITION_CHANGED:
                        {
                            CurrentPositionChanged((ZaberDataPrmCurPosChanged)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                }
            }
        }

        private void CurrentPositionChanged(ZaberDataPrmCurPosChanged zaberDataPrmCurPosChanged)
        {
            lblCurrentPosition.Text = String.Format("Current Position: {0}", zaberDataPrmCurPosChanged.CurrentPosition); 
        }



        private void nudSpeed_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
