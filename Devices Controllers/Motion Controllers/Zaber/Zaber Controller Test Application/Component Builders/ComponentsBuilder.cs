﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Zaber_Common.Interfaces;
using Zaber_Controller_UI; 

namespace Zaber_Controller_Test_Application.Component_Builders
{
    public class ComponentsBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";

        public void Bulid(IUnityContainer container)
        {

            Container = container;

            Container.RegisterInstance<IUnityContainer>(Container);

            Container.RegisterType<IZaberControllerComponentsBuilder, ZaberControllerComponentsBuilder>(); //"DBManagerComponentsBuilder", new InjectionConstructor(Container));

            Container.RegisterInstance<IZaberControllerComponentsBuilder>(Container.Resolve<IZaberControllerComponentsBuilder>());
        }

        public IUnityContainer Container { set; get; }
    }
}
