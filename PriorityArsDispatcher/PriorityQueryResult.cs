﻿//using ExtensionMethods;
using System.ComponentModel;

namespace PriorityArsDispatcher
{
    public class PriorityQueryResult : INotifyPropertyChanged
    {
        #region Members

        private float m_Length;

        private float m_Diameter;

        private string m_lotNumber;

        public string LotNumber
        {
            get { return m_lotNumber; }
            set { NotifyPropertyChanged("LotNumber"); m_lotNumber = value; }
        }

        private string m_lotDescription;

        public string LotDescription
        {
            get { return m_lotDescription; }
            set { NotifyPropertyChanged("LotDescription"); m_lotDescription = value; }
        }

        private string m_catNumber;

        public string CatNumber
        {
            get { return m_catNumber; }
            set { NotifyPropertyChanged("CatNumber"); m_catNumber = value; }
        }

        private float m_quantity;

        public float Quantity
        {
            get { return m_quantity; }
            set { NotifyPropertyChanged("Quantity"); m_quantity = value; }
        }

        #endregion Members

        #region Constructors

        /// <summary>
        /// A constructor to be used when the lot exists in the priority db.
        /// </summary>
        /// <param name="lotNumber"></param>
        /// <param name="lotDescription"></param>
        /// <param name="catNumber"></param>
        /// <param name="quantity"></param>
        public PriorityQueryResult(string lotNumber, string lotDescription, string catNumber, float quantity)
        {
            LotNumber = lotNumber;
            LotDescription = ReverseString(lotDescription);
            CatNumber = catNumber;
            Quantity = quantity;

            Diameter = float.Parse(m_catNumber.Substring(3, 3)) / 100.0f ;
            Length = float.Parse(m_catNumber.Substring(6, 2));
        }

        /// <summary>
        /// A constructor to be used when the lot does not exist in the priority. the data is given by the user.
        /// </summary>
        /// <param name="lotNumber"></param>
        /// <param name="lotDescription"></param>
        /// <param name="catNumber"></param>
        /// <param name="quantity"></param>
        /// <param name="length"></param>
        /// <param name="diameter"></param>
        public PriorityQueryResult(string lotNumber, string lotDescription, string catNumber, float quantity, float length, float diameter, string familyName="", string familyDescription="")
        {
            LotNumber = lotNumber;
            LotDescription = ReverseString(lotDescription);
            CatNumber = catNumber;
            Quantity = quantity;

            Length = length;
            Diameter = diameter;

            FamilyName = familyName;
            FamilyDescription = familyDescription;
        }


        public PriorityQueryResult()
        {
            LotNumber = "";
            LotDescription = ReverseString("");
            CatNumber = "";
            Quantity = 0;
            Length = 0;
            Diameter = 0;
        }

        #endregion Constructors

        #region Prop's

        public float Length
        {
            //CatNumber	"MXC12250"	string
            //LotDescription	"Presillion 2.5 x 12 m"	string
            //LotNumber	"PR000360"	string
            //m_catNumber	"MXC12250"	string
            //m_lotDescription	"Presillion 2.5 x 12 m"	string
            //m_lotNumber	"PR000360"	string
            //m_quantity	53.0	float
            //Quantity	53.0	float

            get
            {
                return m_Length;
            }

            set
            {
                NotifyPropertyChanged("Length");
                m_Length = value;
            }
        }

        /// <summary>
        /// The system diameter in 1 mm resolution.
        /// </summary>
        public float Diameter
        {
            get
            {
                return m_Diameter;
            }
            set
            {
                NotifyPropertyChanged("Diameter");
                m_Diameter = value;
            }
        }

        private string _familyName;
        /// <summary>
        /// The Identifer of the family in the priority DB
        /// </summary>
        public string FamilyName
        {
            get { return _familyName; }
            set { _familyName = value; NotifyPropertyChanged("FamilyName"); }
        }

        private string _familyDescription;

        /// <summary>
        /// The description of the family in the priority DB
        /// </summary>
        public string FamilyDescription
        {
            get { return _familyDescription; }
            set { _familyDescription = value; NotifyPropertyChanged("FamilyDescription"); }
        }



        #endregion Prop's

        #region Private Methods

        private string ReverseString(string str)
        {
            string tempstr = "";
            char[] tempAr = str.ToCharArray();
            for (int i = tempAr.Length - 1; i > 0; i--)
                tempstr += tempAr[i].ToString();
            return tempstr;
        }

        #endregion Private Methods

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged Members
    }
}