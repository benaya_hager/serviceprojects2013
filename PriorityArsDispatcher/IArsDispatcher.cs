﻿namespace PriorityArsDispatcher
{
    public interface IArsDispatcher
    {
        PriorityQueryResult GetLotInfo(string lotNumber);
        PriorityQueryResult GetLotInfoWithFamilyInfo(string lotNumber);
        void InsertSingleShsArsRecord(ShsArsRecord record);
        bool IsConnectedToPriority { get; }
    }
}
