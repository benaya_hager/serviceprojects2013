﻿using ExtensionMethods;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace PriorityArsDispatcher
{
    public class ARSDispatcher : PriorityArsDispatcher.IArsDispatcher
    {
        private SqlConnection _connection;
        private bool _isConnectedToPriority;

        public ARSDispatcher(string connectionString)
        {
            _connection = new SqlConnection(connectionString);

            try
            {
                _connection.Open();
                IsConnectedToPriority = true;
            }
            catch (Exception)
            {

                IsConnectedToPriority = false;
            }
        }

        public bool IsConnectedToPriority
        {
            get { return _isConnectedToPriority; }
            private set { _isConnectedToPriority = value; }
        }

        public void InsertSingleShsArsRecord(ShsArsRecord record)
        {
            if (!IsConnectedToPriority)
                return;

            using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
            {
                dataAdapter.SelectCommand = new SqlCommand("select TOP 1 * from SHS_ARS", _connection);

                DataSet dataSet = new DataSet();

                dataAdapter.Fill(dataSet);

                Debug.WriteLine("There are {0} rows in the table", dataSet.Tables[0].Rows.Count);

                DataRow row = dataSet.Tables[0].NewRow();
                row["ACT"] = record.ACT;
                row["ANALYSISCODE"] = record.ANALYSISCODE;
                row["ANALYSISDES"] = record.ANALYSISDES;
                row["KEY1"] = record.KEY1;
                row["KEY2"] = record.KEY2;
                row["KEY3"] = record.KEY3;
                row["LOADED"] = record.LOADED;
                row["MDI_SYSTEMNUM"] = record.MDI_SYSTEMNUM;
                row["MDI_UDATE"] = record.MDI_UDATE;
                row["MDI_USERLOGIN"] = record.MDI_USERLOGIN;
                row["RECORDTYPE"] = record.RECORDTYPE;

                double result;

                if (double.TryParse(record.RESCODE, out result))
                {
                    row["RESCODE"] = record.RESCODE.ReverseString();
                }

                else
                {
                    row["RESCODE"] = record.RESCODE;
                }

                row["SERIALNAME"] = record.SERIALNAME;
                row["SHS_DATE"] = record.SHS_DATE;

                dataSet.Tables[0].Rows.Add(row);

                dataAdapter.InsertCommand =
                    new SqlCommand(
                        "insert into SHS_ARS (RECORDTYPE, LOADED,KEY1,KEY2,KEY3,MDI_SYSTEMNUM,ANALYSISCODE,ANALYSISDES,RESCODE,MDI_UDATE,MDI_USERLOGIN,ACT,SERIALNAME,SHS_DATE) values (@RECORDTYPE, @LOADED,@KEY1,@KEY2,@KEY3,@MDI_SYSTEMNUM,@ANALYSISCODE,@ANALYSISDES,@RESCODE,@MDI_UDATE,@MDI_USERLOGIN,@ACT,@SERIALNAME,@SHS_DATE)",
                        _connection);

                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("RECORDTYPE", row["RECORDTYPE"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("LOADED", row["LOADED"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("KEY1", row["KEY1"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("KEY2", row["KEY2"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("KEY3", row["KEY3"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("MDI_SYSTEMNUM", row["MDI_SYSTEMNUM"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("ANALYSISCODE", row["ANALYSISCODE"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("ANALYSISDES", row["ANALYSISDES"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("RESCODE", row["RESCODE"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("MDI_UDATE", row["MDI_UDATE"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("MDI_USERLOGIN", row["MDI_USERLOGIN"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("ACT", row["ACT"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("SERIALNAME", row["SERIALNAME"]));
                dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("SHS_DATE", row["SHS_DATE"]));

                dataAdapter.Update(dataSet);

                ////Just to prove we inserted
                using (DataSet newDataSet = new DataSet())
                {
                    dataAdapter.Fill(newDataSet);
                    Debug.WriteLine("There are {0} rows in the table", newDataSet.Tables[0].Rows.Count);
                }
            }
        }

        public PriorityQueryResult GetLotInfo(string lotNumber)
        {
            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
                {
                    dataAdapter.SelectCommand = new SqlCommand(string.Format(@"SELECT SERIAL.SERIALNAME, SERIAL.SERIALDES, PART.PARTNAME, SERIAL.QUANT 
                                                                                FROM SERIAL INNER JOIN PART ON SERIAL.PART = PART.PART 
                                                                                where SERIALNAME = '{0}'", lotNumber), _connection);

                    DataSet dataSet = new DataSet();

                    dataAdapter.Fill(dataSet);

                    Debug.WriteLine("There are {0} rows in the table", dataSet.Tables[0].Rows.Count);

                    var table = dataSet.Tables[0].Rows;

                    if (table.Count > 0)
                        return new PriorityQueryResult(
                            (string)table[0].ItemArray.GetValue(0),
                            (string)table[0].ItemArray.GetValue(1),
                           table[0].ItemArray.GetValue(2).ToString(),
                            ((float)((int)(table[0].ItemArray.GetValue(3))) / 1000));

                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PriorityQueryResult GetLotInfoWithFamilyInfo(string lotNumber)
        {
            try
            {
                using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
                {
                    dataAdapter.SelectCommand = 
                        new SqlCommand(string.Format(
                                            @"SELECT SERIAL.SERIALNAME, SERIAL.SERIALDES, PART.PARTNAME, SERIAL.QUANT, FAMILY.FAMILY, FAMILY.FAMILYDES, FAMILY.FAMILYNAME
                                            FROM SERIAL INNER JOIN
                                            PART ON SERIAL.PART = PART.PART INNER JOIN
                                            FAMILY ON PART.FAMILY = FAMILY.FAMILY
                                            WHERE  (SERIAL.SERIALNAME = '{0}')", lotNumber), _connection);

                    DataSet dataSet = new DataSet();

                    dataAdapter.Fill(dataSet);

                    Debug.WriteLine("There are {0} rows in the table", dataSet.Tables[0].Rows.Count);

                    var table = dataSet.Tables[0].Rows;

                    if (table.Count > 0)
                        return new PriorityQueryResult(
                            (string)table[0].ItemArray.GetValue(0),
                            (string)table[0].ItemArray.GetValue(1),
                           table[0].ItemArray.GetValue(2).ToString(),
                            ((float)((long)(table[0].ItemArray.GetValue(3))) / 1000), 
                            0, 
                            0, 
                            (string)table[0].ItemArray.GetValue(6), 
                            (string)table[0].ItemArray.GetValue(5));

                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool ConnectToPriority()
        {
            try
            {
                _connection.Open();
                IsConnectedToPriority = true;
            }
            catch (InvalidOperationException ioe)
            {
                //already connected
                IsConnectedToPriority = true;
            }
            catch (SqlException ex)
            {
                IsConnectedToPriority = false;
            }

            return IsConnectedToPriority;
        }
    }
}
