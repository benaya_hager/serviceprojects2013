﻿namespace PriorityArsDispatcher
{
    /// <summary>
    /// Represents a single SHS_ARS table record
    /// </summary>
    public class ShsArsRecord
    {
        public string RECORDTYPE { get; set; }
        public string LOADED { get; set; }
        public string KEY1 { get; set; }
        public string KEY2 { get; set; }
        public string KEY3 { get; set; }
        public string MDI_SYSTEMNUM { get; set; }
        public string ANALYSISCODE { get; set; }
        public string ANALYSISDES { get; set; }
        public string RESCODE { get; set; }
        public int MDI_UDATE { get; set; }
        public string MDI_USERLOGIN { get; set; }
        public int ACT { get; set; }
        public string SERIALNAME { get; set; }
        public int SHS_DATE { get; set; }
    }
}